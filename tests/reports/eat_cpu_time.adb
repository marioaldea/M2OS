----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
#if M2_TARGET = "arduino_uno" then
with Arduino;
#end if;
with M2.HAL;
with M2.Kernel.API;

package body Eat_CPU_Time is
   package HAL renames M2.HAL;
   
   use type HAL.HWTime;
   
   --------------
   -- Eat_Long --
   --------------

   procedure Eat_Long (D : Duration)
                       renames M2.Kernel.API.Eat;
   
   ----------------------
   -- Eat_Milliseconds --
   ----------------------

   procedure Eat_Milliseconds (MS : Natural) is
      Time_End : constant HAL.HWTime :=
        HAL.HWTime ((MS * HAL.HWTime_Hz) / 1_000) + HAL.Get_HWTime;
   begin
      while Time_End > HAL.Get_HWTime loop
         null;
      end loop;
   end Eat_Milliseconds;
   
   ----------------------
   -- Eat_Microseconds --
   ----------------------

   procedure Eat_Microseconds (US : Natural) is
   begin
#if M2_TARGET = "arduino_uno" then
      Arduino.Delay_Microseconds (Arduino.Time_US_16 (US));
#else
      Eat_Long (Duration (US) / 1_000_000);    
#end if;
      end Eat_Microseconds;  

   end Eat_CPU_Time;
