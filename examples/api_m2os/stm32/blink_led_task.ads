----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with LEDs;

with AdaX_Dispatching_Stack_Sharing;

generic
   LED : LEDs.User_LED;
   Period_Ms : Positive;
package Blink_Led_Task is

private
   procedure LED_Task_Init;

   procedure LED_Task_Body;

   LED_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (LED_Task_Init'Access, LED_Task_Body'Access, Priority => 4);

end Blink_Led_Task;
