pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package SHDefines_h is

   --  unsupported macro: SCL_BAS1 A5
   --  unsupported macro: SDA_BAS1 A4
   SCL_BAS2 : constant := 2;  --  SHDefines.h:72
   --  unsupported macro: SDA_BAS2 A0

   SCL_BBS1 : constant := 4;  --  SHDefines.h:74
   --  unsupported macro: SDA_BBS1 A1

   SCL_BBS2 : constant := 7;  --  SHDefines.h:76
   --  unsupported macro: SDA_BBS2 A2

   BTN_RIGHT : constant := 4;  --  SHDefines.h:80
   BTN_LEFT : constant := 1;  --  SHDefines.h:81

   BTN_GO : constant := 2;  --  SHDefines.h:84
   LED_RED : constant := 8;  --  SHDefines.h:85
   --  unsupported macro: LED_GREEN A3

   LED_BLUE : constant := 12;  --  SHDefines.h:87

   subtype SH_BankPort is unsigned;
   SH_BAS1 : constant SH_BankPort := 1;
   SH_BAS2 : constant SH_BankPort := 2;
   SH_BBS1 : constant SH_BankPort := 3;
   SH_BBS2 : constant SH_BankPort := 4;  -- SHDefines.h:57

   type SH_Protocols is 
     (SH_HardwareI2C,
      SH_SoftwareI2C);
   pragma Convention (C, SH_Protocols);  -- SHDefines.h:66

end SHDefines_h;
