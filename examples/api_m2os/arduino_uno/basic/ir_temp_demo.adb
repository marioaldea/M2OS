----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Grove - Infrared Temperature Sensor:
--  Measure the Surrounding temperature around the sensor
--  and the temperature of the target which is in front of the sensor.
--
--  Based on example in the "seeed" wiki:
--   https://wiki.seeedstudio.com/Grove-Infrared_Temperature_Sensor/
--
--     ---------------------                 ----------------------
--     |                   |                 |                    |
--     | Grove     SUR_TEMP|-----------------|A0      Arduino Uno |
--     | IR Temp   OBJ_TEMP|-----------------|A1                  |
--     | Sensor         VCC|-----------------|5V                  |
--     |                GND|-----------------|GND                 |
--     |                   |                 |                    |
--     ---------------------                 ----------------------
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino.IR_Temp_Sensor;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure IR_Temp_Demo is
   package DIO renames M2.Direct_IO;
   package IR_Temp renames Arduino.IR_Temp_Sensor;
   
   Temp : Float;
   Off : Float;
begin
   DIO.Put_Line ("IR Temp Demo");
   
   IR_Temp.Initialize;
   
   DIO.Put ("Offset Correction (x1000):    ");
   DIO.Put (Integer (IR_Temp.Get_Offset_Correction * 1000.0));
   DIO.New_Line;
 
   loop
      --  IR_Temp.Adjust_Offset;
      
      Temp := IR_Temp.Surronding_Temperature;       
      DIO.Put ("Surronding Temperature:");
      DIO.Put (Temp);
      DIO.New_Line;
      
      Temp := IR_Temp.Object_Temperature;       
      DIO.Put ("Object Temperature:    ");
      DIO.Put (Temp);
      DIO.New_Line;
      
      Off := IR_Temp.Get_Last_Offset;       
      DIO.Put ("Last Offset (x1000):    ");
      DIO.Put (Integer (Off * 1000.0));
      DIO.New_Line;

      Arduino.C_Delay (2_000);
   end loop;
   
end IR_Temp_Demo;
