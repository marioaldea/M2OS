----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of using Arduino.Servo package.
--  Arduino.Servo is a binding to the Servo.h standar Arduino library.
--  This example uses two servos simustaneously using two tasks.
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |          Pulse|-----------------|D3                   |
--     | Servo      VDD|-----------------|VDD                  |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 |         Arduino Uno |
--     -----------------                 |                     |
--     |               |                 |                     |
--     |          Pulse|-----------------|D5                   |
--     | Servo      VDD|-----------------|VDD                  |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--
--  --------------------------------------------------------------------------
with System;
with Ada.Real_Time;

with Arduino;

package Servos_Two_Tasks is

   --  Task servo 0 parameters
   
   Pin_Servo_0 : constant Arduino.PWM_Digital_Pin := Arduino.PWM_D3;
   Prio_Task_Servo_0 : constant System.Priority := 10;
   Period_Task_Servo_0 : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (100);
     
   -- Task servo 1 parameters
     
   Pin_Servo_1 : constant Arduino.PWM_Digital_Pin := Arduino.PWM_D5;
   Prio_Task_Servo_1 : constant System.Priority := 10;
   Period_Task_Servo_1 : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (400);
     
end Servos_Two_Tasks;
