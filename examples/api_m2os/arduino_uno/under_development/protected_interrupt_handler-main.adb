--  Task activated by an entry opened from a protected interrupt handler in
--  a Protected Object.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
with M2.HAL;
with Arduino;

with Protected_Interrupt_Handler.Server_Task;
with Protected_Interrupt_Handler.PO;

procedure Protected_Interrupt_Handler.Main is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put ("-Protected_Interrupt_Handler-");
   Arduino.Analog_Write (Pin   => Arduino.PWM_D5,
                         Value => 200);
   M2.HAL.Enable_Interrupts;
end Protected_Interrupt_Handler.Main;
