//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (mario.aldea@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Test of chained yield_to_higher() using the M2OS POSIX-like layer.
// Two yield_to_higher() nested.
//
// th_hp #.......................__#.....#....        ^ = thread activation
//       ^                       ^       ^            # = thread execution
// th_mp _#..............___#######_#...._##..        _ = thread active
//       ^               ^        y      ^y           . = thread suspended
// th_lp __################__________#.......#        y = yield_to_higher
//       ^                y                  ^
//       0       1       2       3       4
//
#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include "../reports/tests_reports.h"

extern int m2__hal__get_current_stack_size_in_bytes();
extern int m2__kernel__api__global_stack_base();
extern int m2__kernel__api__current_stack_top();

THREAD_POOL(3);

#define COUNT_LP_END_VALUE 2
int count_lp = 0;
int count_mp = 0;
int count_hp = 0;

int global_stack_base;

#define PERIOD_LP_NS 480000000
#define PERIOD_MP_NS 200000000
#define PERIOD_HP_NS 100000000
#define DELAY_ACTIVATION_LP_NS (PERIOD_HP_NS / 10)
const struct timespec period_lp = TS(0, PERIOD_LP_NS);
const struct timespec period_mp = TS(0, PERIOD_MP_NS);
const struct timespec period_hp = TS(0, PERIOD_HP_NS);
const struct timespec eat_lp = TS(0, PERIOD_MP_NS);
const struct timespec eat_mp = TS(0, PERIOD_HP_NS);

#define ARRAY_SIZE 10

void puts_now(char *msg) {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  putint(TS_SEC(now)); puts("s");
  putint(TS_NSEC(now) / 100000); puts("ms ");
  puts(msg);
}

//******************//
//  thread_body_lp  //
//******************//
struct timespec next_activation_time_lp = TS(0, 0);

void * thread_body_lp(void *arg) {
  puts_now("Thread LP\n");
  puts(" SPl=");
  putint(m2__kernel__api__current_stack_top());
  puts("\n");
  count_lp++;
  
  if (count_lp == COUNT_LP_END_VALUE) {
    tests_reports__assert(count_hp == 3 && count_mp == 3);
    tests_reports__test_ok();
  }
  
  const int value = 100 * count_hp +  10 * count_mp + count_lp;
  int a[ARRAY_SIZE];
  for (int i = 0; i < ARRAY_SIZE; i++) {
    a[i] = value;
    }
  
  for (int i = 0; i < ARRAY_SIZE; i++) {  
    putint(a[i]); puts(" ");
    tests_reports__assert (a[i] == value);
  }
  puts("\n");
  
  puts_now("Thread LP: eat\n");  
  tests_reports__eat(eat_lp);
  
  tests_reports__assert(count_hp == 1 && count_mp == 1 && count_lp == 1);
  
  puts_now("Thread LP: before yield_to_higher() \n");
  const int stack_before = m2__kernel__api__current_stack_top();
  yield_to_higher();
  tests_reports__assert(stack_before == m2__kernel__api__current_stack_top());
  puts_now("Thread LP: after yield_to_higher()\n");
  
  for (int i = 0; i < ARRAY_SIZE; i++) {  
    putint(a[i]); puts(" ");
    tests_reports__assert (a[i] == value);
  }
  puts("\n");
  
  tests_reports__assert(count_hp == 2 && count_mp == 2 && count_lp == 1);

  puts_now("Thread LP: clock_nanosleep()\n");
  TS_INC(next_activation_time_lp, period_lp);
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time_lp, NULL);
  
  return NULL;
}

//******************//
//  thread_body_mp  //
//******************//
struct timespec next_activation_time_mp = TS(0, 0);
int first_activation_mp = 1;

void * thread_body_mp(void *arg) {
  puts_now("Thread MP\n");
  puts(" SPm=");
  putint(m2__kernel__api__current_stack_top());
  puts("\n");
  count_mp++;
  
  if (first_activation_mp) {
    first_activation_mp = 0;
    tests_reports__assert(count_hp == 1 && count_lp == 0);
    puts_now("Thread MP: first clock_nanosleep()\n");
    TS_INC(next_activation_time_mp, period_mp);
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time_mp, NULL);
  }
  
  const int value = 100 * count_hp +  10 * count_mp + count_lp;
  int a[ARRAY_SIZE];
  for (int i = 0; i < ARRAY_SIZE; i++) {
    a[i] = value;
    }
  
  for (int i = 0; i < ARRAY_SIZE; i++) {  
    putint(a[i]); puts(" ");
    tests_reports__assert (a[i] == value);
  }
  puts("\n");
  
  if (count_mp == 2) { 
    puts_now("Thread MP: eat\n");  
    tests_reports__eat(eat_mp);
  }
  
  tests_reports__assert((count_mp == 2 && count_hp == 1 && count_lp == 1) ||
			(count_mp == 3 && count_hp == 3 && count_lp == 1));
  
  puts_now("Thread MP: before yield_to_higher() \n"); 
  const int stack_before = m2__kernel__api__current_stack_top();
  yield_to_higher();
  tests_reports__assert(stack_before == m2__kernel__api__current_stack_top());
  puts_now("Thread MP: after yield_to_higher()\n");
  
  tests_reports__assert((count_mp == 2 && count_hp == 2 && count_lp == 1) ||
			(count_mp == 3 && count_hp == 3 && count_lp == 1));
  
  for (int i = 0; i < ARRAY_SIZE; i++) {  
    putint(a[i]); puts(" ");
    tests_reports__assert (a[i] == value);
  }
  puts("\n");

  puts_now("Thread MP: clock_nanosleep()\n");
  TS_INC(next_activation_time_mp, period_mp);     // XXX lp
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time_mp, NULL);     // XXX lp
  
  return NULL;
}

//******************//
//  thread_body_hp  //
//******************//
struct timespec next_activation_time_hp = TS(0, PERIOD_MP_NS);
int first_activation_hp = 1;

void * thread_body_hp(void *arg) {
  puts_now("Thread HP\n");
  puts(" SPh=");
  putint(m2__kernel__api__current_stack_top());
  puts("\n");
  
  count_hp++;
  
  if (first_activation_hp) {
    first_activation_hp = 0;
    tests_reports__assert(count_hp == 1 && count_mp == 0 && count_lp == 0);
    puts_now("Thread HP: first clock_nanosleep()\n");
    TS_INC(next_activation_time_hp, period_hp);
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time_hp, NULL);
  }
  
  tests_reports__assert(count_mp == 2 && count_lp == 1);

  puts_now("Thread HP: clock_nanosleep()\n");
  TS_INC(next_activation_time_hp, period_hp);
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time_hp, NULL);
  
  return NULL;
} 


//**********//
//   main   //
//**********//
int main (int argc, char **argv) {
  m2osinit();
  global_stack_base = m2__kernel__api__global_stack_base();
  
  puts("Main\n");
  puts(" SPB=");
  putint(global_stack_base);
  puts("\n SPm=");
  putint(m2__kernel__api__current_stack_top());
  puts(" ");
  putint(m2__hal__get_current_stack_size_in_bytes());
  puts("\n");
  
  check_posix_api();
    
  pthread_t th_lp;    
  pthread_t th_mp;
  pthread_t th_hp;
  pthread_attr_t attr_lp;
  pthread_attr_t attr_mp;
  pthread_attr_t attr_hp;
  struct sched_param sch_param_lp = {.sched_priority = 10};
  struct sched_param sch_param_mp = {.sched_priority = 20};
  struct sched_param sch_param_hp = {.sched_priority = 30};

  pthread_attr_init(&attr_lp);
  pthread_attr_setschedparam(&attr_lp, &sch_param_lp);    
  pthread_create(&th_lp, &attr_lp, thread_body_lp, NULL);

  pthread_attr_init(&attr_mp);
  pthread_attr_setschedparam(&attr_mp, &sch_param_mp);    
  pthread_create(&th_mp, &attr_mp, thread_body_mp, NULL);
  
  pthread_attr_init(&attr_hp);
  pthread_attr_setschedparam(&attr_hp, &sch_param_hp);       
  pthread_create(&th_hp, &attr_hp, thread_body_hp, NULL);

  puts("Main ends\n");
  puts(" SPm=");
  putint(m2__kernel__api__current_stack_top());
  puts(" ");
  putint(m2__hal__get_current_stack_size_in_bytes());
  puts("\n");

  return 0;
}
