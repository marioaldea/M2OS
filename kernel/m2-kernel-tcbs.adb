----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Unchecked_Conversion;
with System;
with Interfaces;

with M2.Kernel.Scheduler;

with M2.Direct_IO;

package body M2.Kernel.TCBs is

   --pragma Compile_Time_Error (System.Thread_T_Size_In_Bytes * 8 /= TCB'Size,
   --                           "Unexpected size of TCB");
   pragma Compile_Time_Warning (System.Thread_T_Size_In_Bytes * 8 /= TCB'Size,
                                "Unexpected size of TCB");

   use type Interfaces.Unsigned_16;

   --------------------
   -- Initialize_TCB --
   --------------------

   procedure Initialize_TCB
     (Th_Ac : TCB_Ac;
      Id : Kernel.Thread_Id;
      Priority : M2.Kernel.Priority;
      Body_Ac : HAL.Code_Address;
      Args : System.Address) is
   begin
      Th_Ac.Id              := Id;
      Th_Ac.Ret_Addr := HAL.Code_Address (Scheduler.Thread_Body_Wrapper'Address);
      Th_Ac.Priority        := Priority;
      Th_Ac.Body_Ac         := Body_Ac;
      Th_Ac.Aux_Args        := Args;
      Th_Ac.Activation_Time := Initial_Activation_Time;
      Th_Ac.Stack_Base      := HAL.Global_Stack_Base;
      Th_Ac.Order_Counter   := 0;
      if M2.Account_CPU_Time'First then
         Th_Ac.CPU_Time := 0;
      end if;
   end Initialize_TCB;

   ------------------
   -- Set_Job_Body --
   ------------------

   procedure Set_Job_Body (Th : in out TCB;
                           Job_Body_Ac : HAL.Code_Address) is
   begin
      Th.Ret_Addr := Job_Body_Ac;
      Th.Body_Ac := Job_Body_Ac;
   end Set_Job_Body;

   ----------
   -- Show --
   ----------

   procedure Show  (Th : in TCB; Level : M2.Trace_Levels := M2.Trace) is
      use type HAL.HWTime;
   begin
      if not M2.Trace_Enabled (Level) then
         return;
      end if;

      M2.Direct_IO.Put ("(");
      M2.Direct_IO.Put (Integer (Th.Id));
      M2.Direct_IO.Put (",");
      M2.Direct_IO.Put (Integer (Th.Priority));
      M2.Direct_IO.Put (",");
      M2.Direct_IO.Put (Integer (Th.Activation_Time mod
                          HAL.HWTime (Integer'Last)));
--      M2.Direct_IO.Put (",");
--      M2.Direct_IO.Put (Integer (Th.CPU_Time));
--        M2.Direct_IO.Put (Integer (Th.Order_Counter));
--        M2.Direct_IO.Put (",");
--      M2.Direct_IO.Put (Integer (Th.Stack_Base), 16);
      if Th.Activation_Time = Blocked_Activation_Time then
         M2.Direct_IO.Put (",B)");
      elsif Th.Activation_Time <= HAL.Get_HWTime then
         M2.Direct_IO.Put (",R)");
      else
         M2.Direct_IO.Put (",S)");
      end if;
   end Show;

end M2.Kernel.TCBs;
