----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  RISC-V version of this package

with System;

with System.Storage_Elements;

with Ada.Unchecked_Conversion;
with System.Machine_Code; use System.Machine_Code;

with M2.Debug;

package body M2.HAL is

   package DBG renames M2.Debug;

   use type Interfaces.Unsigned_32;
   use type System.Address, Integer_Address;

   Initialized : Boolean := False;

   Stack_Base_Linker_Script : Interfaces.Unsigned_32;
   --  pragma Import (C, Stack_Base_Linker_Script, "__stack_end");
   pragma Compile_Time_Warning(True, "TODO");

   Stack_Base : Integer_Address;
   pragma Volatile (Stack_Base);
   
   procedure Push_Function_Status is
   begin
      null; -- TODO escribir codigo �ensamblador? �en m2_hal_regs.S?
   end Push_Function_Status;
   
   procedure Pop_Function_Status is
   begin
      null; -- TODO: escribir codigo �ensamblador? �en m2_hal_regs.S?
   end Pop_Function_Status;

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch (New_Context : Thread_Context;
                             New_Stack : System.Address) is
      procedure Jump_To_Context (Funct : Thread_Context;
                                 Stack_Top : System.Address) is
      begin
         null; -- TODO escribir codigo �ensamblador? �en m2_hal_regs.S?
      end Jump_To_Context;
        -- pragma Import (C, Jump_To_Context, "m2_hal_regs_jump_to_context");
        --  From m2_hal_regs.S
      pragma Compile_Time_Warning(True, "TODO");

   begin
      pragma Debug (Debug.Assert (New_Context /= null));
      pragma Debug (Debug.Assert (To_Integer (New_Stack) /= 0));

      --  Jump to the task body address (stored in New_Context)

      --  Jump_To_Context (New_Context, New_Stack);

      --  Never returns

      pragma Debug (Debug.Assert (False));
   end Context_Switch;

   -----------------------
   -- Global_Stack_Base --
   -----------------------

   function Global_Stack_Base return System.Address is
      (To_Address (Stack_Base));

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Natural is
   begin
      pragma Compile_Time_Warning
        (True, "Stack management functions not implemented");
      return 1000;
   end Get_Stack_Max_Size_In_Bytes;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Natural is
   begin
      return 100;
   end Get_Current_Stack_Size_In_Bytes;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Natural is
   begin
      return 900;
   end Get_Current_Stack_Margin_In_Bytes;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area is
   begin
      null;
   end Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Natural is
   begin
      return 234;
   end Get_Max_Stack_Usage_In_Bytes;

   -----------------------
   -- Enable_Interrupts --
   -----------------------

   procedure Enable_Interrupts is
   begin
      pragma Compile_Time_Warning(True, "TODO");
   end Enable_Interrupts;

   ------------------------
   -- Disable_Interrupts --
   ------------------------

   procedure Disable_Interrupts is
   begin
      pragma Compile_Time_Warning(True, "TODO");
   end Disable_Interrupts;

   ----------------------------
   -- Are_Interrupts_Enabled --
   ----------------------------

   function Are_Interrupts_Enabled return Boolean is
   begin
      pragma Compile_Time_Warning(True, "TODO");
      return True;
   end Are_Interrupts_Enabled;

   ----------
   -- Time --
   ----------

   Now : HWTime := 0; --  Current time

   ---------------------
   -- SysTick_Handler --
   ---------------------

   OS_Tick_Handler : OS_Tick_Handler_Ac;

   --  Handler for the timer interrupt.

   procedure Sys_Tick_Handler;
   pragma Export (C, Sys_Tick_Handler, "__gnat_irq_trap_rtc0");

   procedure Sys_Tick_Handler is
   begin
      Now := Now + 1;
      
      Events_Order_Counter := 0;

      if M2.Use_Timing_Events'First then
         OS_Tick_Handler.all (Now);
      end if;
   end Sys_Tick_Handler;

   ----------------
   -- Get_HWTime --
   ----------------

   function Get_HWTime return HWTime is
   begin
      pragma Debug (DBG.Assert (Initialized));

      return Now;
   end Get_HWTime;

   --------------------
   -- Initialization --
   --------------------

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac)is
   begin
      pragma Debug
        (Debug.Assert (System.Address'Size = Interfaces.Unsigned_32'Size));

       --  Initialize SDK
      pragma Compile_Time_Warning(True, "TODO");
      
      HAL.Disable_Interrupts;

      HAL.OS_Tick_Handler := OS_Tick_Handler;

      Stack_Base := To_Integer (Stack_Base_Linker_Script'Address);

      Initialized := True;
   end Initialization;

end M2.HAL;
