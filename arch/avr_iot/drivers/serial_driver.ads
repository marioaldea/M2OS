----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces.C;
use Interfaces.C;
with System;
with M2.Drivers;

package Serial_Driver is
   --  pragma Pure;
   pragma Preelaborate;

   -- Imported C functions from libAtmelStart
   -- Initialization functions
   -- Overall init

   procedure system_init
       with
       Import => true,
       Convention => C,
       External_Name => "system_init";
   -- Serial initialization functions
   function USART_ASYNC_init return Interfaces.Unsigned_8
     with
     Import => true,
     Convention => C,
     External_Name => "USART_ASYNC_init";

   procedure USART_ASYNC_enable
     with
     Import => true,
     Convention => C,
     External_Name => "USART_ASYNC_enable";

   procedure USART_ASYNC_enable_rx
     with
     Import => true,
     Convention => C,
     External_Name => "USART_ASYNC_enable_rx";

   procedure USART_ASYNC_enable_tx
     with
     Import => true,
     Convention => C,
     External_Name => "USART_ASYNC_enable_tx";

   procedure USART_ASYNC_disable
     with
     Import => true,
     Convention => C,
     External_Name => "USART_ASYNC_disable";

  -- Transmission functions

  function USART_ASYNC_is_rx_ready return Boolean
    with
    Import => true,
    Convention => C,
    External_Name => "USART_ASYNC_is_rx_ready";

  function USART_ASYNC_is_tx_ready return Boolean
    with
    Import => true,
    Convention => C,
    External_Name => "USART_ASYNC_is_tx_ready";

   procedure USART_ASYNC_write(data: Interfaces.Unsigned_8)
     with
     Import => true,
     Convention => C,
     External_Name => "USART_ASYNC_write";

   procedure Open (Mode : M2.Drivers.File_Access_Mode);
   --  Initialize USART0

   function Write (Buffer_Ptr : System.Address;
                   Bytes      : size_t)
                   return int;

end Serial_Driver;
