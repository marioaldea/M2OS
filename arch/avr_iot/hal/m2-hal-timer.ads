
package M2.HAL.Timer is
   --  pragma Pure;
   pragma Preelaborate;

   Now : HWTime := 0; --  Current time

   HWT_HZ : constant := 1_000;
   --  'HWT_HZ' is the number of 'HWTime' units per second.

   function Get_Time return HWTime;

   Microseconds_Per_Timer_Count : constant := 4;
   type Timer_Ticks is mod 250;
   for Timer_Ticks'Size use 8;
   --  These values depend on the timer programming in Initialize.

   function Get_Counter return Timer_Ticks;
   pragma Inline_Always (Get_Counter);

   procedure Initialize (OS_Tick_Handler : HAL.OS_Tick_Handler_Ac);
   --  Program Timer0 to spire every 1ms

end M2.HAL.Timer;
