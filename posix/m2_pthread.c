//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2023
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// M2OS POSIX-like layer implementation (thread_pool).
// Other POSIX wrappers are implemented as inline functions in the POSIX
// header files.
// The M2OS POSIX-like layer is in a preliminary stage.
#include <stddef.h>
#include <m2_api.h>
#include <pthread.h>

/*
 *  Pool of threads
 */
// Must be created by the user somewhere (probably in main.c) using the
// THREAD_POOL macro defined in m2_api.h.
extern const int max_num_threads;
extern pthread_t thread_pool[];
int num_of_threads_created = 0;

/*
 *  pthread_create
 */
int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
		   void *(*start_routine) (void *), void *arg) {
  if (num_of_threads_created >= max_num_threads) {
    return -1;
  }

  m2__kernel__api__create_thread(&thread_pool[num_of_threads_created],
				 start_routine,
				 attr == NULL?0:attr->param.sched_priority,
				 arg);
  num_of_threads_created++;
  return 0;
}


