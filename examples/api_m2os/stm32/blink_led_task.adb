----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

with M2.Direct_IO;

package body Blink_Led_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   --------------
   -- LED_Task --
   --------------

   Period : constant RT.Time_Span := RT.Milliseconds (Period_Ms);

   Next_Time : RT.Time;
   Switch_LED_On : Boolean := False;

   procedure LED_Task_Init is
   begin
      Next_Time := Ada.Real_Time.Clock;

      DIO.Put ("-Task-" & LED'Image);
      DIO.New_Line;
   end LED_Task_Init;


   procedure LED_Task_Body is
   begin
      if Switch_LED_On then
         LEDs.On (LED);
      else
         LEDs.Off (LED);
      end if;

      Switch_LED_On := not Switch_LED_On;

      DIO.Put ("-Task loop:" & LED'Image);
      DIO.New_Line;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end LED_Task_Body;

end Blink_Led_Task;
