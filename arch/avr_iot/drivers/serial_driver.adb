----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces;

with Ada.Unchecked_Conversion;

package body Serial_Driver is

   use type Interfaces.Unsigned_8;

   ----------------------
   -- USART0 registers --
   ----------------------

   --  USART 0 Baud Rate Registers (UBRR0L and UBRR0H)
-- UBRR0 : Interfaces.Unsigned_16;
-- for UBRR0'Address use 16#c4#;
-- pragma Volatile (UBRR0);

   --  USART 0 Control and Status Register A
-- UCSR0A : Interfaces.Unsigned_8;
-- for UCSR0A'Address use 16#c0#;
-- pragma Volatile (UCSR0A);

   --  USART 0 Control and Status Register B
-- UCSR0B : Interfaces.Unsigned_8;
-- for UCSR0B'Address use 16#c1#;
-- pragma Volatile (UCSR0B);

   --  USART 0 Control and Status Register C
-- UCSR0C : Interfaces.Unsigned_8;
-- for UCSR0C'Address use 16#c2#;
-- pragma Volatile (UCSR0C);

   --  USART 0 I/O Data Register
-- UDR0 : Interfaces.Unsigned_8;
-- for UDR0'Address use 16#c6#;
-- pragma Volatile (UDR0);

-- Baud_Rate_19200_16MHz : constant := 51;
   --  Clock_Hz/16/BAUD_RATE-1 = 16MHz/16/19200-1 = 51.08

   -----------------
   -- Buffer type --
   -----------------

   type Buffer is array (size_t) of Interfaces.Unsigned_8;
   type Buffer_Ac is access Buffer;
   function To_Buffer_Ac is
     new Ada.Unchecked_Conversion (System.Address, Buffer_Ac);

   ----------
   -- Open --
   ----------

   procedure Open (Mode : M2.Drivers.File_Access_Mode) is
--   err : Interfaces.Unsigned_8;
   begin
--   err:=USART_ASYNC_init;
--   USART_ASYNC_enable;
--   USART_ASYNC_enable_tx;
     system_init;

   end Open;

   -----------
   -- Write --
   -----------

   function Write (Buffer_Ptr : System.Address;
                   Bytes      : size_t)
                   return int is
      Buff_Ac : Buffer_Ac := To_Buffer_Ac (Buffer_Ptr);
      data : Interfaces.Unsigned_8;
   begin
      for I in size_t range 0 .. Bytes - 1 loop
         --  Wait for empty transmit buffer (when UDRE0 bit is set)

         loop
            exit when USART_ASYNC_is_tx_ready;
         end loop;

         --  Put data into buffer, sends the data
         data:=Buff_Ac(I);
         USART_ASYNC_write(data);
      end loop;

      return int (Bytes);
   end Write;

end Serial_Driver;
