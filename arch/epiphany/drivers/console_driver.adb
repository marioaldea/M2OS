----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Authors: David Garcia Villaescusa (garcia@unican.es)
--           Mario Aldea Rivas (aldeam@unican.es))
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces;

with Ada.Unchecked_Conversion;

package body Console_Driver is

   use type Interfaces.Unsigned_8;

   procedure Shared_Memory_Console_Open
     with
       Import => True,
       convention => C;
   -- Imports function 'shared_memory_console_open' from C.
   -- Can be called from Ada.

   procedure Shared_Memory_Console_Write
     (Buffer_Ptr : System.Address;
      Bytes      : size_t)
     with
       Import => True,
       convention => C;
   -- Imports function 'shared_memory_console_write' from C.
   -- Can be called from Ada.

   -----------------
   -- Buffer type --
   -----------------

   type Buffer is array (size_t) of Interfaces.Unsigned_8;
   type Buffer_Ac is access Buffer;
   function To_Buffer_Ac is
     new Ada.Unchecked_Conversion (System.Address, Buffer_Ac);

   ----------
   -- Open --
   ----------

   procedure Open (Mode : M2.Drivers.File_Access_Mode) is
   begin
      Shared_Memory_Console_Open;
   end Open;

   -----------
   -- Write --
   -----------

   function Write (Buffer_Ptr : System.Address;
                   Bytes      : size_t)
                   return int is
   begin
      Shared_Memory_Console_Write (Buffer_Ptr, Bytes);
      return int (Bytes);
   end Write;

end Console_Driver;
