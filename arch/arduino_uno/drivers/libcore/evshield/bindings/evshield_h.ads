pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with EVShieldI2C_h;
with stdint_h;
with Interfaces.C.Extensions;
with SHDefines_h;
with Interfaces.C.Strings;
with Arduino;

package EVShield_h is

   SH_CONTROL_SPEED : constant := 16#01#;  --  EVShield.h:67
   SH_CONTROL_RAMP : constant := 16#02#;  --  EVShield.h:68
   SH_CONTROL_RELATIVE : constant := 16#04#;  --  EVShield.h:69
   SH_CONTROL_TACHO : constant := 16#08#;  --  EVShield.h:70
   SH_CONTROL_BRK : constant := 16#10#;  --  EVShield.h:71
   SH_CONTROL_ON : constant := 16#20#;  --  EVShield.h:72
   SH_CONTROL_TIME : constant := 16#40#;  --  EVShield.h:73
   SH_CONTROL_GO : constant := 16#80#;  --  EVShield.h:74

   SH_STATUS_SPEED : constant := 16#01#;  --  EVShield.h:76
   SH_STATUS_RAMP : constant := 16#02#;  --  EVShield.h:77
   SH_STATUS_MOVING : constant := 16#04#;  --  EVShield.h:78
   SH_STATUS_TACHO : constant := 16#08#;  --  EVShield.h:79
   SH_STATUS_BREAK : constant := 16#10#;  --  EVShield.h:80
   SH_STATUS_OVERLOAD : constant := 16#20#;  --  EVShield.h:81
   SH_STATUS_TIME : constant := 16#40#;  --  EVShield.h:82
   SH_STATUS_STALL : constant := 16#80#;  --  EVShield.h:83

   SH_COMMAND : constant := 16#41#;  --  EVShield.h:85
   SH_VOLTAGE : constant := 16#6E#;  --  EVShield.h:86

   SH_SETPT_M1 : constant := 16#42#;  --  EVShield.h:88
   SH_SPEED_M1 : constant := 16#46#;  --  EVShield.h:89
   SH_TIME_M1 : constant := 16#47#;  --  EVShield.h:90
   SH_CMD_B_M1 : constant := 16#48#;  --  EVShield.h:91
   SH_CMD_A_M1 : constant := 16#49#;  --  EVShield.h:92

   SH_SETPT_M2 : constant := 16#4A#;  --  EVShield.h:94
   SH_SPEED_M2 : constant := 16#4E#;  --  EVShield.h:95
   SH_TIME_M2 : constant := 16#4F#;  --  EVShield.h:96
   SH_CMD_B_M2 : constant := 16#50#;  --  EVShield.h:97
   SH_CMD_A_M2 : constant := 16#51#;  --  EVShield.h:98

   SH_POSITION_M1 : constant := 16#52#;  --  EVShield.h:103
   SH_POSITION_M2 : constant := 16#56#;  --  EVShield.h:104
   SH_STATUS_M1 : constant := 16#5A#;  --  EVShield.h:105
   SH_STATUS_M2 : constant := 16#5B#;  --  EVShield.h:106
   SH_TASKS_M1 : constant := 16#5C#;  --  EVShield.h:107
   SH_TASKS_M2 : constant := 16#5D#;  --  EVShield.h:108

   SH_ENCODER_PID : constant := 16#5E#;  --  EVShield.h:110
   SH_SPEED_PID : constant := 16#64#;  --  EVShield.h:111
   SH_PASS_COUNT : constant := 16#6A#;  --  EVShield.h:112
   SH_TOLERANCE : constant := 16#6B#;  --  EVShield.h:113

   SH_S1_MODE : constant := 16#6F#;  --  EVShield.h:115
   SH_S1_EV3_MODE : constant := 16#6F#;  --  EVShield.h:116
   SH_S1_ANALOG : constant := 16#70#;  --  EVShield.h:117

   SH_S2_MODE : constant := 16#A3#;  --  EVShield.h:119
   SH_S2_EV3_MODE : constant := 16#6F#;  --  EVShield.h:120
   SH_S2_ANALOG : constant := 16#A4#;  --  EVShield.h:121

   SH_BTN_PRESS : constant := 16#DA#;  --  EVShield.h:123
   SH_RGB_LED : constant := 16#D7#;  --  EVShield.h:124
   SH_CENTER_RGB_LED : constant := 16#DE#;  --  EVShield.h:125

   SH_PS_TS_X : constant := 16#E3#;  --  EVShield.h:127
   SH_PS_TS_Y : constant := 16#E5#;  --  EVShield.h:128
   SH_PS_TS_RAWX : constant := 16#E7#;  --  EVShield.h:129
   SH_PS_TS_RAWY : constant := 16#E9#;  --  EVShield.h:130
   SH_PS_TS_CALIBRATION_DATA_READY : constant := 16#70#;  --  EVShield.h:131
   SH_PS_TS_CALIBRATION_DATA : constant := 16#71#;  --  EVShield.h:132
   SH_PS_TS_SAVE : constant := 16#77#;  --  EVShield.h:133
   SH_PS_TS_LOAD : constant := 16#6C#;  --  EVShield.h:134
   SH_PS_TS_UNLOCK : constant := 16#45#;  --  EVShield.h:135

   SH_Speed_Full : constant := 90;  --  EVShield.h:184
   SH_Speed_Medium : constant := 60;  --  EVShield.h:185
   SH_Speed_Slow : constant := 25;  --  EVShield.h:186

   SH_Bank_A : constant := 16#34#;  --  EVShield.h:197

   SH_Bank_B : constant := 16#36#;  --  EVShield.h:202

   SH_Type_NONE : constant := 16#00#;  --  EVShield.h:214

   SH_Type_SWITCH : constant := 16#01#;  --  EVShield.h:216

   SH_Type_ANALOG : constant := 16#02#;  --  EVShield.h:222

   SH_Type_LIGHT_REFLECTED : constant := 16#03#;  --  EVShield.h:229

   SH_Type_LIGHT_AMBIENT : constant := 16#04#;  --  EVShield.h:234

   SH_Type_I2C : constant := 16#09#;  --  EVShield.h:239

   SH_Type_COLORFULL : constant := 13;  --  EVShield.h:245

   SH_Type_COLORRED : constant := 14;  --  EVShield.h:250

   SH_Type_COLORGREEN : constant := 15;  --  EVShield.h:255

   SH_Type_COLORBLUE : constant := 16;  --  EVShield.h:260

   SH_Type_COLORNONE : constant := 17;  --  EVShield.h:265

   SH_Type_EV3_SWITCH : constant := 18;  --  EVShield.h:270

   SH_Type_EV3 : constant := 19;  --  EVShield.h:275

   SH_S1 : constant := 1;  --  EVShield.h:288

   SH_S2 : constant := 2;  --  EVShield.h:295

   subtype SH_Motor is unsigned;
   SH_Motor_1 : constant SH_Motor := 1;
   SH_Motor_2 : constant SH_Motor := 2;
   SH_Motor_Both : constant SH_Motor := 3;  -- EVShield.h:145

   type SH_Next_Action is 
     (SH_Next_Action_Float,
      SH_Next_Action_Brake,
      SH_Next_Action_BrakeHold);
   pragma Convention (C, SH_Next_Action);  -- EVShield.h:154

   type SH_Direction is 
     (SH_Direction_Reverse,
      SH_Direction_Forward);
   pragma Convention (C, SH_Direction);  -- EVShield.h:162

   type SH_Move is 
     (SH_Move_Absolute,
      SH_Move_Relative);
   pragma Convention (C, SH_Move);  -- EVShield.h:170

   type SH_Completion_Wait is 
     (SH_Completion_Dont_Wait,
      SH_Completion_Wait_For);
   pragma Convention (C, SH_Completion_Wait);  -- EVShield.h:178

   package Class_EVShieldBank is
      type EVShieldBank is limited record
         parent : aliased EVShieldI2C_h.Class_EVShieldI2C.EVShieldI2C;
      end record;
      pragma Import (CPP, EVShieldBank);

      function New_EVShieldBank (i2c_address : stdint_h.uint8_t) return EVShieldBank;  -- EVShield.h:310
      pragma CPP_Constructor (New_EVShieldBank, "_ZN12EVShieldBankC1Eh");

      function evshieldGetBatteryVoltage (this : access EVShieldBank) return int;  -- EVShield.h:319
      pragma Import (CPP, evshieldGetBatteryVoltage, "_ZN12EVShieldBank25evshieldGetBatteryVoltageEv");

      function nxshieldGetBatteryVoltage (this : access EVShieldBank) return int;  -- EVShield.h:323
      pragma Import (CPP, nxshieldGetBatteryVoltage, "_ZN12EVShieldBank25nxshieldGetBatteryVoltageEv");

      function EVShieldIssueCommand (this : access EVShieldBank; command : char) return stdint_h.uint8_t;  -- EVShield.h:329
      pragma Import (CPP, EVShieldIssueCommand, "_ZN12EVShieldBank20EVShieldIssueCommandEc");

      function motorSetEncoderTarget
        (this : access EVShieldBank;
         which_motor : SH_Motor;
         target : long) return Extensions.bool;  -- EVShield.h:338
      pragma Import (CPP, motorSetEncoderTarget, "_ZN12EVShieldBank21motorSetEncoderTargetE8SH_Motorl");

      function motorGetEncoderTarget (this : access EVShieldBank; which_motor : SH_Motor) return long;  -- EVShield.h:345
      pragma Import (CPP, motorGetEncoderTarget, "_ZN12EVShieldBank21motorGetEncoderTargetE8SH_Motor");

      function motorSetSpeed
        (this : access EVShieldBank;
         which_motor : SH_Motor;
         speed : int) return Extensions.bool;  -- EVShield.h:352
      pragma Import (CPP, motorSetSpeed, "_ZN12EVShieldBank13motorSetSpeedE8SH_Motori");

      function motorGetSpeed (this : access EVShieldBank; which_motor : SH_Motor) return stdint_h.int8_t;  -- EVShield.h:359
      pragma Import (CPP, motorGetSpeed, "_ZN12EVShieldBank13motorGetSpeedE8SH_Motor");

      function motorSetTimeToRun
        (this : access EVShieldBank;
         which_motor : SH_Motor;
         seconds : int) return Extensions.bool;  -- EVShield.h:366
      pragma Import (CPP, motorSetTimeToRun, "_ZN12EVShieldBank17motorSetTimeToRunE8SH_Motori");

      function motorGetTimeToRun (this : access EVShieldBank; which_motor : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:373
      pragma Import (CPP, motorGetTimeToRun, "_ZN12EVShieldBank17motorGetTimeToRunE8SH_Motor");

      function motorSetCommandRegB
        (this : access EVShieldBank;
         which_motor : SH_Motor;
         value : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:385
      pragma Import (CPP, motorSetCommandRegB, "_ZN12EVShieldBank19motorSetCommandRegBE8SH_Motorh");

      function motorGetCommandRegB (this : access EVShieldBank; which_motor : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:394
      pragma Import (CPP, motorGetCommandRegB, "_ZN12EVShieldBank19motorGetCommandRegBE8SH_Motor");

      function motorSetCommandRegA
        (this : access EVShieldBank;
         which_motor : SH_Motor;
         value : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:404
      pragma Import (CPP, motorSetCommandRegA, "_ZN12EVShieldBank19motorSetCommandRegAE8SH_Motorh");

      function motorGetCommandRegA (this : access EVShieldBank; which_motor : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:413
      pragma Import (CPP, motorGetCommandRegA, "_ZN12EVShieldBank19motorGetCommandRegAE8SH_Motor");

      function motorGetEncoderPosition (this : access EVShieldBank; which_motor : SH_Motor) return stdint_h.int32_t;  -- EVShield.h:420
      pragma Import (CPP, motorGetEncoderPosition, "_ZN12EVShieldBank23motorGetEncoderPositionE8SH_Motor");

      function motorGetStatusByte (this : access EVShieldBank; which_motor : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:429
      pragma Import (CPP, motorGetStatusByte, "_ZN12EVShieldBank18motorGetStatusByteE8SH_Motor");

      function motorGetTasksRunningByte (this : access EVShieldBank; which_motor : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:437
      pragma Import (CPP, motorGetTasksRunningByte, "_ZN12EVShieldBank24motorGetTasksRunningByteE8SH_Motor");

      function motorSetEncoderPID
        (this : access EVShieldBank;
         Kp : stdint_h.uint16_t;
         Ki : stdint_h.uint16_t;
         Kd : stdint_h.uint16_t) return Extensions.bool;  -- EVShield.h:446
      pragma Import (CPP, motorSetEncoderPID, "_ZN12EVShieldBank18motorSetEncoderPIDEjjj");

      function motorSetSpeedPID
        (this : access EVShieldBank;
         Kp : stdint_h.uint16_t;
         Ki : stdint_h.uint16_t;
         Kd : stdint_h.uint16_t) return Extensions.bool;  -- EVShield.h:455
      pragma Import (CPP, motorSetSpeedPID, "_ZN12EVShieldBank16motorSetSpeedPIDEjjj");

      function centerLedSetRGB
        (this : access EVShieldBank;
         R : stdint_h.uint8_t;
         G : stdint_h.uint8_t;
         B : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:457
      pragma Import (CPP, centerLedSetRGB, "_ZN12EVShieldBank15centerLedSetRGBEhhh");

      function ledSetRGB
        (this : access EVShieldBank;
         R : stdint_h.uint8_t;
         G : stdint_h.uint8_t;
         B : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:461
      pragma Import (CPP, ledSetRGB, "_ZN12EVShieldBank9ledSetRGBEhhh");

      function motorSetPassCount (this : access EVShieldBank; pass_count : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:470
      pragma Import (CPP, motorSetPassCount, "_ZN12EVShieldBank17motorSetPassCountEh");

      function motorSetTolerance (this : access EVShieldBank; tolerance : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:478
      pragma Import (CPP, motorSetTolerance, "_ZN12EVShieldBank17motorSetToleranceEh");

      function motorReset (this : access EVShieldBank) return Extensions.bool;  -- EVShield.h:484
      pragma Import (CPP, motorReset, "_ZN12EVShieldBank10motorResetEv");

      function motorStartBothInSync (this : access EVShieldBank) return Extensions.bool;  -- EVShield.h:490
      pragma Import (CPP, motorStartBothInSync, "_ZN12EVShieldBank20motorStartBothInSyncEv");

      function motorResetEncoder (this : access EVShieldBank; which_motor : SH_Motor) return Extensions.bool;  -- EVShield.h:496
      pragma Import (CPP, motorResetEncoder, "_ZN12EVShieldBank17motorResetEncoderE8SH_Motor");

      function motorSetSpeedTimeAndControl
        (this : access EVShieldBank;
         which_motors : SH_Motor;
         speed : int;
         duration : stdint_h.uint8_t;
         control : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:508
      pragma Import (CPP, motorSetSpeedTimeAndControl, "_ZN12EVShieldBank27motorSetSpeedTimeAndControlE8SH_Motorihh");

      function motorSetEncoderSpeedTimeAndControl
        (this : access EVShieldBank;
         which_motors : SH_Motor;
         encoder : long;
         speed : int;
         duration : stdint_h.uint8_t;
         control : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:523
      pragma Import (CPP, motorSetEncoderSpeedTimeAndControl, "_ZN12EVShieldBank34motorSetEncoderSpeedTimeAndControlE8SH_Motorlihh");

      function motorIsTimeDone (this : access EVShieldBank; which_motors : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:533
      pragma Import (CPP, motorIsTimeDone, "_ZN12EVShieldBank15motorIsTimeDoneE8SH_Motor");

      function motorWaitUntilTimeDone (this : access EVShieldBank; which_motors : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:541
      pragma Import (CPP, motorWaitUntilTimeDone, "_ZN12EVShieldBank22motorWaitUntilTimeDoneE8SH_Motor");

      function motorIsTachoDone (this : access EVShieldBank; which_motors : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:549
      pragma Import (CPP, motorIsTachoDone, "_ZN12EVShieldBank16motorIsTachoDoneE8SH_Motor");

      function motorWaitUntilTachoDone (this : access EVShieldBank; which_motors : SH_Motor) return stdint_h.uint8_t;  -- EVShield.h:557
      pragma Import (CPP, motorWaitUntilTachoDone, "_ZN12EVShieldBank23motorWaitUntilTachoDoneE8SH_Motor");

      procedure motorRunUnlimited
        (this : access EVShieldBank;
         which_motors : SH_Motor;
         direction : SH_Direction;
         speed : int);  -- EVShield.h:566
      pragma Import (CPP, motorRunUnlimited, "_ZN12EVShieldBank17motorRunUnlimitedE8SH_Motor12SH_Directioni");

      function motorRunSeconds
        (this : access EVShieldBank;
         which_motors : SH_Motor;
         direction : SH_Direction;
         speed : int;
         duration : stdint_h.uint8_t;
         wait_for_completion : SH_Completion_Wait;
         next_action : SH_Next_Action) return stdint_h.uint8_t;  -- EVShield.h:579
      pragma Import (CPP, motorRunSeconds, "_ZN12EVShieldBank15motorRunSecondsE8SH_Motor12SH_Directionih18SH_Completion_Wait14SH_Next_Action");

      function motorRunTachometer
        (this : access EVShieldBank;
         which_motors : SH_Motor;
         direction : SH_Direction;
         speed : int;
         tachometer : long;
         relative : SH_Move;
         wait_for_completion : SH_Completion_Wait;
         next_action : SH_Next_Action) return stdint_h.uint8_t;  -- EVShield.h:596
      pragma Import (CPP, motorRunTachometer, "_ZN12EVShieldBank18motorRunTachometerE8SH_Motor12SH_Directionil7SH_Move18SH_Completion_Wait14SH_Next_Action");

      function motorRunDegrees
        (this : access EVShieldBank;
         which_motors : SH_Motor;
         direction : SH_Direction;
         speed : int;
         degrees : long;
         wait_for_completion : SH_Completion_Wait;
         next_action : SH_Next_Action) return stdint_h.uint8_t;  -- EVShield.h:613
      pragma Import (CPP, motorRunDegrees, "_ZN12EVShieldBank15motorRunDegreesE8SH_Motor12SH_Directionil18SH_Completion_Wait14SH_Next_Action");

      function motorRunRotations
        (this : access EVShieldBank;
         which_motors : SH_Motor;
         direction : SH_Direction;
         speed : int;
         rotations : long;
         wait_for_completion : SH_Completion_Wait;
         next_action : SH_Next_Action) return stdint_h.uint8_t;  -- EVShield.h:629
      pragma Import (CPP, motorRunRotations, "_ZN12EVShieldBank17motorRunRotationsE8SH_Motor12SH_Directionil18SH_Completion_Wait14SH_Next_Action");

      function motorStop
        (this : access EVShieldBank;
         which_motors : SH_Motor;
         next_action : SH_Next_Action) return Extensions.bool;  -- EVShield.h:639
      pragma Import (CPP, motorStop, "_ZN12EVShieldBank9motorStopE8SH_Motor14SH_Next_Action");

      function sensorSetType
        (this : access EVShieldBank;
         which_sensor : stdint_h.uint8_t;
         sensor_type : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:652
      pragma Import (CPP, sensorSetType, "_ZN12EVShieldBank13sensorSetTypeEhh");

      function sensorReadRaw (this : access EVShieldBank; which_sensor : stdint_h.uint8_t) return int;  -- EVShield.h:659
      pragma Import (CPP, sensorReadRaw, "_ZN12EVShieldBank13sensorReadRawEh");
   end;
   use Class_EVShieldBank;
   package Class_EVShieldBankB is
      type EVShieldBankB is limited record
         parent : aliased EVShieldBank;
      end record;
      pragma Import (CPP, EVShieldBankB);

      function New_EVShieldBankB (i2c_address_b : stdint_h.uint8_t) return EVShieldBankB;  -- EVShield.h:673
      pragma CPP_Constructor (New_EVShieldBankB, "_ZN13EVShieldBankBC1Eh");

      function sensorReadRaw (this : access EVShieldBankB; which_sensor : stdint_h.uint8_t) return int;  -- EVShield.h:679
      pragma Import (CPP, sensorReadRaw, "_ZN13EVShieldBankB13sensorReadRawEh");

      function sensorSetType
        (this : access EVShieldBankB;
         which_sensor : stdint_h.uint8_t;
         sensor_type : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:687
      pragma Import (CPP, sensorSetType, "_ZN13EVShieldBankB13sensorSetTypeEhh");
   end;
   use Class_EVShieldBankB;
   package Class_EVShield is
      type EVShield is limited record
         m_protocol : aliased stdint_h.uint8_t;  -- EVShield.h:700
         bank_a : aliased EVShieldBank;  -- EVShield.h:703
         bank_b : aliased EVShieldBankB;  -- EVShield.h:706
         x1 : aliased stdint_h.uint16_t;  -- EVShield.h:818
         y1 : aliased stdint_h.uint16_t;  -- EVShield.h:818
         x2 : aliased stdint_h.uint16_t;  -- EVShield.h:818
         y2 : aliased stdint_h.uint16_t;  -- EVShield.h:818
         x3 : aliased stdint_h.uint16_t;  -- EVShield.h:818
         y3 : aliased stdint_h.uint16_t;  -- EVShield.h:818
         x4 : aliased stdint_h.uint16_t;  -- EVShield.h:818
         y4 : aliased stdint_h.uint16_t;  -- EVShield.h:818
         useOldTouchscreen : aliased Arduino.Boolean8;  -- EVShield.h:826
      end record;
      pragma Import (CPP, EVShield);

      function New_EVShield (i2c_address_a : stdint_h.uint8_t; i2c_address_b : stdint_h.uint8_t) return EVShield;  -- EVShield.h:709
      pragma CPP_Constructor (New_EVShield, "_ZN8EVShieldC1Ehh");

      procedure init (this : access EVShield; protocol : SHDefines_h.SH_Protocols);  -- EVShield.h:717
      pragma Import (CPP, init, "_ZN8EVShield4initE12SH_Protocols");

      procedure initLEDTimers (this : access EVShield);  -- EVShield.h:722
      pragma Import (CPP, initLEDTimers, "_ZN8EVShield13initLEDTimersEv");

      procedure I2CTimer (this : access EVShield);  -- EVShield.h:727
      pragma Import (CPP, I2CTimer, "_ZN8EVShield8I2CTimerEv");

      procedure initProtocols (this : access EVShield; protocol : SHDefines_h.SH_Protocols);  -- EVShield.h:733
      pragma Import (CPP, initProtocols, "_ZN8EVShield13initProtocolsE12SH_Protocols");

      function getButtonState (this : access EVShield; btn : stdint_h.uint8_t) return Extensions.bool;  -- EVShield.h:745
      pragma Import (CPP, getButtonState, "_ZN8EVShield14getButtonStateEh");

      procedure waitForButtonPress
        (this : access EVShield;
         btn : stdint_h.uint8_t;
         led_pattern : stdint_h.uint8_t);  -- EVShield.h:754
      pragma Import (CPP, waitForButtonPress, "_ZN8EVShield18waitForButtonPressEhh");

      procedure ledSetRGB
        (this : access EVShield;
         red : stdint_h.uint8_t;
         green : stdint_h.uint8_t;
         blue : stdint_h.uint8_t);  -- EVShield.h:764
      pragma Import (CPP, ledSetRGB, "_ZN8EVShield9ledSetRGBEhhh");

      procedure ledBreathingPattern (this : access EVShield);  -- EVShield.h:770
      pragma Import (CPP, ledBreathingPattern, "_ZN8EVShield19ledBreathingPatternEv");

      procedure ledHeartBeatPattern (this : access EVShield);  -- EVShield.h:776
      pragma Import (CPP, ledHeartBeatPattern, "_ZN8EVShield19ledHeartBeatPatternEv");

      procedure getTouchscreenValues
        (this : access EVShield;
         x : access stdint_h.uint16_t;
         y : access stdint_h.uint16_t);  -- EVShield.h:784
      pragma Import (CPP, getTouchscreenValues, "_ZN8EVShield20getTouchscreenValuesEPjS0_");

      function TS_X (this : access EVShield) return stdint_h.uint16_t;  -- EVShield.h:790
      pragma Import (CPP, TS_X, "_ZN8EVShield4TS_XEv");

      function TS_Y (this : access EVShield) return stdint_h.uint16_t;  -- EVShield.h:796
      pragma Import (CPP, TS_Y, "_ZN8EVShield4TS_YEv");

      function isTouched (this : access EVShield) return Extensions.bool;  -- EVShield.h:802
      pragma Import (CPP, isTouched, "_ZN8EVShield9isTouchedEv");

      function checkButton
        (this : access EVShield;
         x : stdint_h.uint16_t;
         y : stdint_h.uint16_t;
         width : stdint_h.uint16_t;
         height : stdint_h.uint16_t) return Extensions.bool;  -- EVShield.h:808
      pragma Import (CPP, checkButton, "_ZN8EVShield11checkButtonEjjjj");

      function getFunctionButton (this : access EVShield) return stdint_h.uint8_t;  -- EVShield.h:814
      pragma Import (CPP, getFunctionButton, "_ZN8EVShield17getFunctionButtonEv");

      function RAW_X (this : access EVShield) return stdint_h.uint16_t;  -- EVShield.h:821
      pragma Import (CPP, RAW_X, "_ZN8EVShield5RAW_XEv");

      function RAW_Y (this : access EVShield) return stdint_h.uint16_t;  -- EVShield.h:824
      pragma Import (CPP, RAW_Y, "_ZN8EVShield5RAW_YEv");

      procedure getReading
        (this : access EVShield;
         x : access stdint_h.uint16_t;
         y : access stdint_h.uint16_t);  -- EVShield.h:832
      pragma Import (CPP, getReading, "_ZN8EVShield10getReadingEPjS0_");
   end;
   use Class_EVShield;
   function format_bin (i : stdint_h.uint8_t; s : Interfaces.C.Strings.chars_ptr) return Extensions.bool;  -- EVShield.h:839
   pragma Import (CPP, format_bin, "_Z10format_binhPc");
   
   -----------------------
   -- EVShield object --
   -----------------------

   EVShield : aliased EVShield_h.Class_EVShield.EVShield := EVShield_h.Class_EVShield.New_EVShield (16#34#,16#36#);

end EVShield_h;
