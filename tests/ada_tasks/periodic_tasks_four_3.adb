----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Real_Time;

with M2.Direct_IO;
with M2.HAL;

package body Periodic_Tasks_Four_3 is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   ----------------
   -- Other_Task --
   ----------------
   task Other_Task;

   task body Other_Task is
      Next_Time : RT.Time;
      Period : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms*3);
   begin
      Next_Time := Ada.Real_Time.Clock + Period;
      delay until Next_Time;

      loop
         M2.HAL.Enable_Interrupts;

         DIO.Put ("-Other3-");
         Loop_Counter := Loop_Counter + 1;

         Next_Time := Next_Time + Period;
         delay until Next_Time;
      end loop;
   end Other_Task;

end Periodic_Tasks_Four_3;
