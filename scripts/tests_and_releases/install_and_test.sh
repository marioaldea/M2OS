#!/bin/bash
# Install and tests a M2OS release or repository.
# Use:
#   $ scripts/tests_and_releases/install_and_test.sh . (to install and tests the current directory)
#   $ scripts/tests_and_releases/install_and_test.sh .  arduino_uno (to install and tests only de Arduino Uno architecture in the current directory)
#   $ scripts/tests_and_releases/install_and_test.sh /path/M2OS_19-10-07
#set -o xtrace

if [ "$#" -gt 2 ] ; then
  echo "Usage: $0 M2OS_DIR [arch]" >&2
  exit 1
fi

if [ "$#" -gt 1 ] ; then
  ARCHS_TO_TEST=$2
fi

cd $1
readonly M2OS_DIR=$(pwd)
readonly LOG_FILE=${M2OS_DIR}/"tests_"$(date +%y-%m-%d)".log"
readonly GNAT_FOR_TOOL_PATH="${HOME}/opt/GNAT/2019"

cd $M2OS_DIR

# remove tests log
rm -f $LOG_FILE

# test each architecture
declare -A paths=( ["arduino_uno"]="${HOME}/opt/avr-gcc-gnat-7.3.0/bin:${HOME}/opt/GNAT/2019/bin:$PATH" \
                   ["stm32"]="${HOME}/opt/GNAT/2018-arm-elf/bin:${HOME}/opt/GNAT/2019/bin:$PATH" \
                   ["epiphany"]="${HOME}/opt/epiphany-gcc-7.4.0/bin:${HOME}/opt/GNAT/2019/bin:$PATH" \
                   ["microbit"]="${HOME}/opt/GNAT/2018-arm-elf/bin:${HOME}/opt/GNAT/2021/bin:$PATH")
declare -A config_arch=( ["arduino_uno"]="CONFIG_BOARD_ARDUINO" \
                         ["stm32"]="CONFIG_BOARD_STM32F4" \
                         ["epiphany"]="CONFIG_BOARD_EPIPHANY" \
                         ["microbit"]="CONFIG_BOARD_MICROBIT")
declare -A pre_commandsiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii=( ["arduino_uno"]=":" \
                          ["stm32"]="rm -rf ${HOME}/opt/GNAT/2018-arm-elf/arm-eabi/lib/gnat/m2os-stm32f4" \
                          ["epiphany"]="ln -s ${HOME}/opt/e-lib/ arch/epiphany/" \
                          ["microbit"]=":")
declare -A example_dirs=( ["arduino_uno"]="examples/api_m2os examples/api_m2os/arduino_uno/basic examples/api_m2os/arduino_uno/advanced examples/api_m2os/arduino_uno/evshield examples/posix examples/posix/arduino_uno" \
                          ["stm32"]="examples/api_m2os examples/api_m2os/stm32 examples/posix" \
                          ["epiphany"]="" \
                          ["microbit"]="examples/api_m2os examples/posix")
declare -A test_dirs=( ["arduino_uno"]="tests/api_m2os tests/ada_tasks tests/posix" \
                       ["stm32"]="tests/api_m2os tests/ada_tasks tests/posix" \
                       ["epiphany"]="tests/api_m2os tests/ada_tasks" \
                       ["microbit"]="tests/api_m2os tests/ada_tasks tests/posix")

cp config_params.mk config_params.mk.orig

if [ -z "$ARCHS_TO_TEST" ] ; then
    #ARCHS_TO_TEST="${!paths[@]}"
    ARCHS_TO_TEST="arduino_uno stm32 epiphany"
    echo "Archs to test: $ARCHS_TO_TEST"
fi

for arch in $ARCHS_TO_TEST ; do
    cd $M2OS_DIR
    echo "Processing architecture $arch..." | tee -a $LOG_FILE

    # create config_params.mk
    echo "${config_arch[${arch}]}=y"     > config_params.mk
    echo "CONFIG_SIZEOPTIMIZATION_RTS=y" >> config_params.mk
    echo "CONFIG_SIZEOPTIMIZATION_M2=y"  >> config_params.mk
    echo "CONFIG_SIZEOPTIMIZATION_APP=y" >> config_params.mk
    echo "CONFIG_HOSTGNATFORTOOL=${GNAT_FOR_TOOL_PATH}"  >> config_params.mk

    # set path and install M2OS
    export PATH=${paths[${arch}]}
    make clean    
    ${pre_commands[${arch}]}
    make install

    # execute tests
    for dir in ${test_dirs[${arch}]}; do
	echo -e "Running tests in ${dir} " | tee -a $LOG_FILE
	cd ${dir}
        make clean
	make

	# store summary in log
	../summary.sh >> $LOG_FILE
           
        cd $M2OS_DIR
    done
    
    # compile examples
    for exam_dir in ${example_dirs[${arch}]}; do
       echo "Compiling examples in ${exam_dir}..." | tee -a $LOG_FILE
       cd $M2OS_DIR/$exam_dir
       gpr_file=$(find -maxdepth 1 -name "*_${arch}*.gpr")
       PATH=${paths[${arch}]} gprbuild -P $gpr_file
       if [ $? -ne 0 ]; then
          echo "Error in examples for arch ${arch}" | tee -a $LOG_FILE
          exit 1
       fi
       echo "   ... examples in ${exam_dir} compiled OK." | tee -a $LOG_FILE
    done
     
    echo "Done architecture $arch." | tee -a $LOG_FILE   
    echo -e "\n======================================" | tee -a $LOG_FILE
    
    cd $M2OS_DIR
    PATH=${paths[${arch}]} make clean
done
cp config_params.mk.orig config_params.mk

echo "Done archs: $ARCHS_TO_TEST :)" | tee -a $LOG_FILE
echo "   Tests results in $LOG_FILE"


