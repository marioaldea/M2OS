#!/bin/bash

# Changes files in GNAT 2018 for ARM to create the M2OS RTS

#set -o xtrace  # show commands

# Get SRC PATH
cd rts-m2os-files/
RTS_SRC_PATH="`pwd`"
readonly RTS_SRC_PATH

# GNAT PATH
GNAT_PATH=`which arm-eabi-gnat`
GNAT_PATH="${GNAT_PATH/\/bin\/arm-eabi-gnat/}"
readonly GNAT_PATH
printf "\nGNAT 2018 in dir: $GNAT_PATH\n\n"

# Create RTS directories
declare -a rts_paths=("arm-eabi/BSPs/cortex-m/armv7-m/stm32/stm32f4/" \
                      "arm-eabi/BSPs/cortex-m/microbit/")

for i in "${rts_paths[@]}"
do
	if [ ! -e ${GNAT_PATH}/${i}/m2os/ ]; then
	   printf "\nCreating ${GNAT_PATH}/${i}/m2os/ directory\n"
	   cd ${GNAT_PATH}/${i}
	   mkdir m2os/
	   cp -r ravenscar-sfp/* m2os/
	   cd m2os
	   rm -f adalib/* obj/*
	fi
done
printf "\nFalta modificar ${GNAT_PATH}/arm-eabi/BSPs/cortex-m/armv7-m/stm32/stm32f4/m2os/ada_source_path\n"

cd ${RTS_SRC_PATH}
# Link each entry in the RTS src directory
for entry in *; do 
	
    # skip security copies   
    if [[ ${entry} = *"~" ]]; then
	printf "Skipping ${entry}\n\n"
	continue
    fi
    
    # Get entry name and entry path
    entry_full_path=${entry//__//} # replace __ with /
    entry_path="$(dirname $entry_full_path)"
    entry_name="$(basename $entry_full_path)"
    printf "$entry -> \n   $GNAT_PATH/$entry_path\n   $entry_name\n"

    # create copy of the original file or directory (only if it does not exists)
     if [ ! -e $GNAT_PATH/${entry_full_path}.orig ]; then
 	mv $GNAT_PATH/$entry_full_path $GNAT_PATH/${entry_full_path}.orig
     fi

    # remove link (if it exists)
    rm -f $GNAT_PATH/$entry_full_path
    
    # Create link in the gnat directory
    cd $GNAT_PATH/$entry_path && ln -s ${RTS_SRC_PATH}/${entry}  ${entry_name}
    
    #echo "`pwd`"

#    if [ -d "${entry}" ]; then
#	printf "Directory\n\n"
#    else
#	printf "File\n\n"
#    fi		      
done

printf "\nLinks created in $GNAT_PATH GNAT installation\n"
printf "To create and install RTS run: gprclean, gprbuild y gprinstall\n"
#printf "gprbuild -P $GNAT_PATH/arm-eabi/BSPs/ravenscar_sfp_stm32f4.gpr"
