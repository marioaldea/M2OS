----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Wrapper to the Zowi library Zowi.h
--    Can be downloaded from https://github.com/bq/zowiLibs
--  Zowi is an educational smart robot for children from BQ
--    https://www.bq.com/en/zowi
with Interfaces.C;

package Arduino.Zowi is

   Sizeof_Zowi : Integer with
     Import, Convention => C,
     External_Name => "sizeof_Zowi";
   --  Defined in 'arch/arduino_uno/drivers/libcore/sizes_arduino_types.cpp'
   --  Take the value of -1 if the library is not included.

   procedure Init;
   pragma Import (C, Init, "zowi_init");

   -- HOME = Zowi at rest position
   -- void home();
   procedure Home;
   pragma Import (C, Home, "zowi_home");

   -- Predetermined Motion Functions

   type Direction is (Direction_Backward, Direction_Forward);
   for Direction'Size use Interfaces.C.Int'Size;
   for Direction use (Direction_Backward => -1,
                      Direction_Forward  => 1);

   type Turn_Direction is (Turn_Right, Turn_Left);
   for Turn_Direction'Size use Interfaces.C.Int'Size;
   for Turn_Direction use (Turn_Right => -1,
                           Turn_Left  => 1);

--      void jump(float steps=1, int T = 2000);

   -- void walk(float steps=4, int T=1000, int dir = FORWARD);
   procedure Walk (Steps : Interfaces.C.C_Float := 4.0;
                   T : Time_MS_16 := 1_000;
                   Dir : Direction := Direction_Forward);
   pragma Import (C, Walk, "zowi_walk");

   --      void turn(float steps=4, int T=2000, int dir = LEFT);
   procedure Turn (Steps : Interfaces.C.C_Float := 4.0;
                   T : Time_MS_16 := 2_000;
                   Dir :Turn_Direction := Turn_Left);
   pragma Import (C, Turn, "zowi_turn");

--      void bend (int steps=1, int T=1400, int dir=LEFT);
--      void shakeLeg (int steps=1, int T = 2000, int dir=RIGHT);
--
--      void updown(float steps=1, int T=1000, int h = 20);
--      void swing(float steps=1, int T=1000, int h=20);
--      void tiptoeSwing(float steps=1, int T=900, int h=20);
--      void jitter(float steps=1, int T=500, int h=20);
--      void ascendingTurn(float steps=1, int T=900, int h=20);
--
--      void moonwalker(float steps=1, int T=900, int h=20, int dir=LEFT);
--      void crusaito(float steps=1, int T=900, int h=20, int dir=FORWARD);
--      void flapping(float steps=1, int T=1000, int h=20, int dir=FORWARD);

    -- Sensors functions
   function Get_Distance return Float; --  Ultrasonic Sensor
   pragma Import (C, Get_Distance, "zowi_getDistance");
--      int getNoise();      //Noise Sensor

    -- Battery
--      double getBatteryLevel();
--      double getBatteryVoltage();

    -- Mouth & Animations

   type Mouth_Shape is (Mouth_Zero,
                        Mouth_One,
                        Mouth_Two,
                        Mouth_Three,
                        Mouth_Four,
                        Mouth_Five,
                        Mouth_Six,
                        Mouth_Seven,
                        Mouth_Eight,
                        Mouth_Nine,
                        Mouth_Smile,
                        Mouth_HappyOpen,
                        Mouth_HappyClosed,
                        Mouth_Heart,
                        Mouth_BigSurprise,
                        Mouth_SmallSurprise,
                        Mouth_TongueOut,
                        Mouth_Vamp1,
                        Mouth_Vamp2,
                        Mouth_LineMouth,
                        Mouth_Confused,
                        Mouth_Diagonal,
                        Mouth_Sad,
                        Mouth_SadOpen,
                        Mouth_SadClosed,
                        Mouth_OkMouth,
                        Mouth_XMouth,
                        Mouth_Interrogation,
                        Mouth_Thunder,
                        Mouth_Culito,
                        Mouth_Angry);
   for Mouth_Shape'Size use Interfaces.C.Unsigned_Long'Size;

   type Mouth_Animation is (Mouth_Animation_LittleUuh,
                            Mouth_Animation_DreamMouth,
                            Mouth_Animation_Adivinawi,
                            Mouth_Animation_Wave);
   for Mouth_Animation'Size use Interfaces.C.Unsigned_Long'Size;

   type Mouth_Animation_Step is new Interfaces.C.int range 0 .. 7;
   for Mouth_Animation_Step'Size use Interfaces.C.int'Size;

   --  void putMouth(unsigned long int mouth, bool predefined = true);
   procedure Put_Mouth (Mouth : Mouth_Shape);
   pragma Import (C,  Put_Mouth, "zowi_putMouth");

   --      void putAnimationMouth(unsigned long int anim, int index);
   procedure Put_Animation_Mouth(Animation : Mouth_Animation;
                                 Index : Mouth_Animation_Step);
   pragma Import (C, Put_Animation_Mouth, "zowi_putAnimationMouth");

   --      void clearMouth();

    -- Sounds
--      void _tone (float noteFrequency, long noteDuration, int silentDuration);
--      void bendTones (float initFrequency, float finalFrequency, float prop, long noteDuration, int silentDuration);
--      void sing(int songName);

   -- Gestures

   type Gestures is (Gesture_ZowiHappy,
                     Gesture_ZowiSuperHappy,
                     Gesture_ZowiSad,
                     Gesture_ZowiSleeping,
                     Gesture_ZowiFart,
                     Gesture_ZowiConfused,
                     Gesture_ZowiLove,
                     Gesture_ZowiAngry,
                     Gesture_ZowiFretful,
                     Gesture_ZowiMagic,
                     Gesture_ZowiWave,
                     Gesture_ZowiVictory,
                     Gesture_ZowiFail);
   for Gestures'Size use Interfaces.C.Int'Size;

   --      void playGesture(int gesture);
   procedure PLay_Gesture (Gesture : Gestures);
   pragma Import (C,  PLay_Gesture, "zowi_playGesture");

end Arduino.Zowi;
