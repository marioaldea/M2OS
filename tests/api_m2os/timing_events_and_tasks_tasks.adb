----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Synchronous_Task_Control;
with Ada.Real_Time;

with Timing_Events_And_Tasks_PO;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;

with Tests_Reports;
with M2.HAL;

package body Timing_Events_And_Tasks_Tasks is
   package DIO renames M2.Direct_IO;

   ----------------
   -- Task1_Init --
   ----------------

   procedure Task1_Init is
   begin
      DIO.Put_Line ("T1 Init" & Timing_Events_And_Tasks_PO.Step'Img);
      Tests_Reports.Assert (Timing_Events_And_Tasks_PO.Step = 1);
      Timing_Events_And_Tasks_PO.Step := Timing_Events_And_Tasks_PO.Step + 1;
   end Task1_Init;

   ----------------
   -- Task1_Body --
   ----------------

   procedure Task1_Body is
   begin
      DIO.Put_Line ("T1" & Timing_Events_And_Tasks_PO.Step'Img);
      Tests_Reports.Assert (M2.HAL.Are_Interrupts_Enabled);
      Tests_Reports.Assert (Timing_Events_And_Tasks_PO.Step = 2 or
                              Timing_Events_And_Tasks_PO.Step = 6 or
                              Timing_Events_And_Tasks_PO.Step = 11 or
                              Timing_Events_And_Tasks_PO.Step = 12);
      Timing_Events_And_Tasks_PO.Step := Timing_Events_And_Tasks_PO.Step + 1;

      Ada.Synchronous_Task_Control.Suspend_Until_True
        (Timing_Events_And_Tasks_PO.Susp_Object);
   end Task1_Body;

   ----------------
   -- Task2_Init --
   ----------------

   procedure Task2_Init is
   begin
      DIO.Put_Line ("T2 Init" & Timing_Events_And_Tasks_PO.Step'Img);
      Tests_Reports.Assert (Timing_Events_And_Tasks_PO.Step = 3);
      Timing_Events_And_Tasks_PO.Step := Timing_Events_And_Tasks_PO.Step + 1;
   end Task2_Init;

   ----------------
   -- Task2_Body --
   ----------------

   procedure Task2_Body is
   begin
      DIO.Put_Line ("T2" & Timing_Events_And_Tasks_PO.Step'Img);
      Tests_Reports.Assert (M2.HAL.Are_Interrupts_Enabled);
      Tests_Reports.Assert (Timing_Events_And_Tasks_PO.Step = 4 or
                              Timing_Events_And_Tasks_PO.Step = 8 or
                              Timing_Events_And_Tasks_PO.Step = 12);
      Timing_Events_And_Tasks_PO.Step := Timing_Events_And_Tasks_PO.Step + 1;

      if Timing_Events_And_Tasks_PO.Step = 13 then
         --  End test
         Tests_Reports.Test_OK;
      end if;

      --Timing_Events_And_Tasks_PO.PO.Wait2;
      Ada.Synchronous_Task_Control.Suspend_Until_True
        (Timing_Events_And_Tasks_PO.Susp_Object2);
   end Task2_Body;

   ------------
   -- Task_1 --
   ------------

   Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task1_Init'Access,
      Body_Ac  => Task1_Body'Access,
      Priority => 5);

   ------------
   -- Task_2 --
   ------------

   Task_2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task2_Init'Access,
      Body_Ac  => Task2_Body'Access,
      Priority => 4);

end Timing_Events_And_Tasks_Tasks;
