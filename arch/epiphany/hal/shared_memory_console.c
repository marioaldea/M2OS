/****************************************************************************
*                                  M2OS
*
*                           Copyright (C) 2019
*                    Universidad de Cantabria, SPAIN
*
*  Author: David Garcia Villaescusa (garciavd@unican.es)
*
*  This file is part of M2OS.
*
*  M2OS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  M2OS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "e_lib.h"

#define CONSOLE_SIZE 256
#define N_CORES 16

int cursor_pos = 0;
char volatile *monitor_init;
char volatile *monitor_pos;

/*
 * Load char by char to a destination from the origin.
 */
void load_char(volatile char *dest, char *orig, unsigned size) {
  int i = 0;
  for (i = 0; i < size; ++i) {
    dest[i] = orig[i];
  }
}

/*
 * Initialization of the console
 */
void shared_memory_console_open () {
  monitor_init = (volatile char *)0x7500;
  monitor_pos = monitor_init;
}

/*
 * Writting into the console memory space
 */
void shared_memory_console_write (char * buffer, unsigned bytes) {

  if (bytes <= CONSOLE_SIZE) {

    if (cursor_pos + bytes*sizeof(char) > CONSOLE_SIZE) {
      cursor_pos = 0;
      monitor_pos = monitor_init;
    }

    load_char (monitor_pos, buffer, bytes);
    monitor_pos = monitor_pos + sizeof(char)*bytes;
    cursor_pos += bytes*sizeof(char);
  }
}
