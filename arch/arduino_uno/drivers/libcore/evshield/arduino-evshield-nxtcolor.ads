----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of evs_nxtcolor.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with stdint_h;
with Arduino.EVShield;


package Arduino.EVShield.NXTColor is
   generic
      bp : Arduino.EVShield.SH_BankPort;
   package Color is

     SH_Type_COLORFULL : constant := 13;  --  EVShield.h:245
     SH_Type_COLORRED : constant := 14;  --  EVShield.h:250
     SH_Type_COLORGREEN : constant := 15;  --  EVShield.h:255
     SH_Type_COLORBLUE : constant := 16;  --  EVShield.h:260
     SH_Type_COLORNONE : constant := 17;  --  EVShield.h:265

     -- set the color of the device on this port of the EVShield
     function Set_Color ( SH_TYPE : stdint_h.uint8_t ) return Extensions.bool;

     function Read_Value return stdint_h.uint8_t;

     function Read_Color return stdint_h.uint8_t;

   end Color;

end Arduino.EVShield.NXTColor;
