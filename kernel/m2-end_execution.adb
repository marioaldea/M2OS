----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with M2.Direct_IO;
--  with MaRTE.Kernel.Dispatcher;
--  with MaRTE.Kernel.TCBs;
with M2.HAL;

package body M2.End_Execution is

   procedure Os_Exit (Status : Interfaces.C.Int);
   pragma No_Return (Os_Exit);
#if M2_TARGET = "stm32f4" or M2_TARGET = "epiphany" or M2_TARGET = "rp2040" or M2_TARGET = "microbit" or M2_TARGET = "riscv" then
   pragma Import (Ada, Os_Exit, "_exit_gnat");
   --  Shutdown or restart the board (Defined in System.Machine_Reset)
#elsif M2_TARGET = "arduino_uno" or M2_TARGET = "avr_iot" then
   pragma Import (Ada, Os_Exit, "_exit");
   --  _exit defined in linker script ??
#end if;

   procedure End_Execution (Status : Interfaces.C.Int) is
   begin
      HAL.Disable_Interrupts;

      Direct_IO.Put ("M2 exit:");
      Direct_IO.Put (Integer (Status), 10);
      Direct_IO.Put (" MxStack:");
      Direct_IO.Put (Integer (HAL.Get_Max_Stack_Usage_In_Bytes));
      Direct_IO.New_Line;

      --  Reset the sysem

      Os_Exit (Status);
   end End_Execution;

end M2.End_Execution;
