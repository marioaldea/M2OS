----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

package M2.HAL.Timer is
   --  pragma Pure;
   pragma Preelaborate;

   subtype Timer_Ticks is M2.HAL.HiResTime;

   Now : HWTime := 0 with Volatile; --  Current time

   HWT_HZ : constant := 1_000;
   --  'HWT_HZ' is the number of 'HWTime' units per second.

   Microseconds_Per_Timer_Count : constant := 4;
   --  These values depend on the timer programming in Initialize.

   function Get_Counter return HAL.HiResTime;
   pragma Inline_Always (Get_Counter);

   procedure Initialize (OS_Tick_Handler : HAL.OS_Tick_Handler_Ac);
   --  Program Timer0 to spire every 1ms

end M2.HAL.Timer;
