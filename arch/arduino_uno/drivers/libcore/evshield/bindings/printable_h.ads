pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
limited with Print_h;
with stddef_h;

package Printable_h is

   package Class_Printable is
      type Printable is limited interface;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Printable.h:33
      pragma Import (CPP, Printable);

      function printTo (this : access constant Printable; p : access Print_h.Class_Print.Print'Class) return stddef_h.size_t is abstract;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Printable.h:36
   end;
   use Class_Printable;
end Printable_h;
