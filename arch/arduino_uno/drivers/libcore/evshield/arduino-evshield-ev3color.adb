----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of evs_ev3color.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with SHDefines_h;
with stdint_h;
with EVs_EV3Color_h;
with Arduino.EVShield;
with EVShieldUART_h;


package body Arduino.EVShield.EV3Color is
   package body Color is
      -------------------------
      -- EVs_EV3Color object --
      -------------------------
      Color : aliased EVs_EV3Color_h.Class_EVs_EV3Color.EVs_EV3Color := EVs_EV3Color_h.Class_EVs_EV3Color.New_EV3Color;

      Aux : Extensions.bool:= EVs_EV3Color_h.Class_EVs_EV3Color.init(color'Access,Arduino.EVShield.EVShield_obj'Access, bp);

      function Get_Val return float is
      begin
       return EVs_Ev3Color_h.Class_EVs_EV3Color.getVal(color'Access);
      end Get_Val;

      function Set_Mode ( New_Mode : char ) return stdint_h.uint8_t is
      begin
         return EVShieldUART_h.Class_EVShieldUART.setMode(color.parent'Access, New_Mode);
      end Set_Mode;

   end Color;

end Arduino.EVShield.EV3Color;
