pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

with M2.Direct_IO;
with M2.HAL;

package body CS_Delay_Past_Time_2 is
   package DIO renames M2.Direct_IO;

   ----------------
   -- Other_Task --
   ----------------

   task body Other_Task is
   begin
      loop
         M2.HAL.Enable_Interrupts;
         --DIO.Put ("-Other-");
--           DIO.Put (Integer
--                    (Ada.Real_Time.To_Duration (Ada.Real_Time.Clock)));

         Loop_Counter := Loop_Counter + 1;

         delay until Ada.Real_Time.Time_First;
      end loop;
   end Other_Task;

end CS_Delay_Past_Time_2;
