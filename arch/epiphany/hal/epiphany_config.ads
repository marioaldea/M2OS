----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: David Garcia Villaescusa (garciavd@unican.es)
--          Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with System;
with Interfaces;
with E_Lib;

package Epiphany_Config is

   ----------------------
   -- Get_Core_Address --
   ----------------------

   function Get_Core_Address (C : in E_Lib.Core) return System.Address;

   -------------------
   -- Add_Addresses --
   -------------------

   function Add_Addresses (A : System.Address; B : System.Address)return System.Address;
end Epiphany_Config;
