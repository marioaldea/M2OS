pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with BaseI2CDevice_h;
with SoftI2cMaster_h;
with System;
with stdint_h;
with SHDefines_h;
with Interfaces.C.Strings;
with Interfaces.C.Extensions;

package EVShieldI2C_h is

   package Class_EVShieldI2C is
      type EVShieldI2C is limited record
         parent : aliased BaseI2CDevice_h.Class_BaseI2CDevice.BaseI2CDevice;
         field_2 : aliased SoftI2cMaster_h.Class_SoftI2cMaster.SoftI2cMaster;
         mp_shield : System.Address;  -- EVShieldI2C.h:37
         u_i2c_buffer : access stdint_h.uint8_t;  -- EVShieldI2C.h:40
         m_protocol : aliased stdint_h.uint8_t;  -- EVShieldI2C.h:47
      end record;
      pragma Import (CPP, EVShieldI2C);

      function New_EVShieldI2C (i2c_address : stdint_h.uint8_t) return EVShieldI2C;  -- EVShieldI2C.h:44
      pragma CPP_Constructor (New_EVShieldI2C, "_ZN11EVShieldI2CC1Eh");

      procedure init
        (this : access EVShieldI2C;
         shield : System.Address;
         bp : SHDefines_h.SH_BankPort);  -- EVShieldI2C.h:50
      pragma Import (CPP, init, "_ZN11EVShieldI2C4initEPv11SH_BankPort");

      function readByte (this : access EVShieldI2C; location : stdint_h.uint8_t) return stdint_h.uint8_t;  -- EVShieldI2C.h:56
      pragma Import (CPP, readByte, "_ZN11EVShieldI2C8readByteEh");

      function readInteger (this : access EVShieldI2C; location : stdint_h.uint8_t) return stdint_h.uint16_t;  -- EVShieldI2C.h:62
      pragma Import (CPP, readInteger, "_ZN11EVShieldI2C11readIntegerEh");

      function readLong (this : access EVShieldI2C; location : stdint_h.uint8_t) return stdint_h.uint32_t;  -- EVShieldI2C.h:68
      pragma Import (CPP, readLong, "_ZN11EVShieldI2C8readLongEh");

      function readRegisters
        (this : access EVShieldI2C;
         start_register : stdint_h.uint8_t;
         bytes : stdint_h.uint8_t;
         buf : access stdint_h.uint8_t) return access stdint_h.uint8_t;  -- EVShieldI2C.h:76
      pragma Import (CPP, readRegisters, "_ZN11EVShieldI2C13readRegistersEhhPh");

      function readString
        (this : access EVShieldI2C;
         location : stdint_h.uint8_t;
         bytes_to_read : stdint_h.uint8_t;
         buffer : access stdint_h.uint8_t;
         buffer_length : stdint_h.uint8_t) return Interfaces.C.Strings.chars_ptr;  -- EVShieldI2C.h:85
      pragma Import (CPP, readString, "_ZN11EVShieldI2C10readStringEhhPhh");

      function writeRegisters
        (this : access EVShieldI2C;
         start_register : stdint_h.uint8_t;
         bytes_to_write : stdint_h.uint8_t;
         buffer : access stdint_h.uint8_t) return Extensions.bool;  -- EVShieldI2C.h:93
      pragma Import (CPP, writeRegisters, "_ZN11EVShieldI2C14writeRegistersEhhPh");

      function writeByte
        (this : access EVShieldI2C;
         location : stdint_h.uint8_t;
         data : stdint_h.uint8_t) return Extensions.bool;  -- EVShieldI2C.h:100
      pragma Import (CPP, writeByte, "_ZN11EVShieldI2C9writeByteEhh");

      function writeInteger
        (this : access EVShieldI2C;
         location : stdint_h.uint8_t;
         data : stdint_h.uint16_t) return Extensions.bool;  -- EVShieldI2C.h:106
      pragma Import (CPP, writeInteger, "_ZN11EVShieldI2C12writeIntegerEhj");

      function writeLong
        (this : access EVShieldI2C;
         location : stdint_h.uint8_t;
         data : stdint_h.uint32_t) return Extensions.bool;  -- EVShieldI2C.h:112
      pragma Import (CPP, writeLong, "_ZN11EVShieldI2C9writeLongEhm");

      function getFirmwareVersion (this : access EVShieldI2C) return Interfaces.C.Strings.chars_ptr;  -- EVShieldI2C.h:115
      pragma Import (CPP, getFirmwareVersion, "_ZN11EVShieldI2C18getFirmwareVersionEv");

      function getVendorID (this : access EVShieldI2C) return Interfaces.C.Strings.chars_ptr;  -- EVShieldI2C.h:118
      pragma Import (CPP, getVendorID, "_ZN11EVShieldI2C11getVendorIDEv");

      function getDeviceID (this : access EVShieldI2C) return Interfaces.C.Strings.chars_ptr;  -- EVShieldI2C.h:121
      pragma Import (CPP, getDeviceID, "_ZN11EVShieldI2C11getDeviceIDEv");

      function getFeatureSet (this : access EVShieldI2C) return Interfaces.C.Strings.chars_ptr;  -- EVShieldI2C.h:124
      pragma Import (CPP, getFeatureSet, "_ZN11EVShieldI2C13getFeatureSetEv");

      function getErrorCode (this : access EVShieldI2C) return stdint_h.uint8_t;  -- EVShieldI2C.h:127
      pragma Import (CPP, getErrorCode, "_ZN11EVShieldI2C12getErrorCodeEv");

      function checkAddress (this : access EVShieldI2C) return Extensions.bool;  -- EVShieldI2C.h:129
      pragma Import (CPP, checkAddress, "_ZN11EVShieldI2C12checkAddressEv");

      function setAddress (this : access EVShieldI2C; address : stdint_h.uint8_t) return Extensions.bool;  -- EVShieldI2C.h:134
      pragma Import (CPP, setAddress, "_ZN11EVShieldI2C10setAddressEh");
   end;
   use Class_EVShieldI2C;
end EVShieldI2C_h;
