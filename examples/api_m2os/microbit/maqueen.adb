--  Sending data to McQueen example

with MicroBit.IOs;
with Microbit.Music; Use Microbit.Music;
with MicroBit.I2C; Use MicroBit.I2C;
with HAL.I2C; Use HAL.I2C;
with HAL;
with nRF.Device;
with nRF.GPIO; use nRF.GPIO;

package body Maqueen is

   MotorsSpeed : Speed := 0;
   Running : Boolean := False;

   -------------------
   -- Turn_On_Light --
   -------------------

   procedure Turn_On_Light (FrontLight : Light)
   is

   begin
      case FrontLight is
        when Left_Light =>
           --  Turn on the LED connected to pin 8
            MicroBit.IOs.Set (8, True);

         when Right_Light =>
           --  Turn on the LED connected to pin 12
           MicroBit.IOs.Set (12, True);
        when others =>
           raise Program_Error;
     end case;
   end Turn_On_Light;

   -------------------
   -- Turn_Off_Light --
   -------------------

   procedure Turn_Off_Light (FrontLight : Light)
   is

   begin
      case FrontLight is
        when Left_Light =>
           --  Turn on the LED connected to pin 8
            MicroBit.IOs.Set (8, False);

         when Right_Light =>
           --  Turn on the LED connected to pin 12
           MicroBit.IOs.Set (12, False);
        when others =>
           raise Program_Error;
     end case;
   end Turn_Off_Light;

   -------------------
   --   Play_Sound  --
   -------------------
   procedure Play_Sound (Snd : Melody)
   is
   begin
      MicroBit.Music.Play (0, Snd);
   end Play_Sound;

   --------------------------
   --      Initialize      --
   --------------------------
   procedure Initialize
   is
      Config   : GPIO_Configuration := (Mode => Mode_In,
                                        Resistors => Pull_Up,
                                        Input_Buffer => Input_Buffer_Connect,
                                        Drive => Drive_S0D1,
                                        Sense => Sense_Disabled);

      GPIO_Pin_13 : GPIO_Point := nRF.Device.P23;
      GPIO_Pin_14 : GPIO_Point := nRF.Device.P22;
   begin
      MicroBit.I2C.Initialize (MicroBit.I2C.S250kbps);

      --Inicialize the GPIO Pins 13 & 14
      Configure_IO(GPIO_Pin_13, Config);
      Configure_IO(GPIO_Pin_14, Config);

      if MicroBit.I2C.Initialized then
         --  Successfully initialized I2C
         null;
   else
         --  Error initializing I2C
         null;
   end if;
   end Initialize;

   --------------------------
   --   Maqueen_SetSpeed   --
   --------------------------
   procedure SetSpeed(Spd : Speed)
   is
   begin
      MotorsSpeed := Spd;
   end SetSpeed;

   --------------------------
   --   Maqueen_Forward  --
   --------------------------
   procedure Forward
   is
      Ctrl   : constant Any_I2C_Port := MicroBit.I2C.Controller;
      Addr   : constant I2C_Address := 16#20#;
      Data   : I2C_Data (0 .. 2);
      Status : I2C_Status;
   begin
      if not MicroBit.I2C.Initialized then
         --  I2C is not initialized
         Raise Program_Error with "Motors has not been initialized!";
      end if;

      Data := (0, 0, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);
      Data := (16#2#, 0, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);

      Running := True;
   end Forward;

   --------------------------
   --   Maqueen_Backward   --
   --------------------------
   procedure Backward
   is
      Ctrl   : constant Any_I2C_Port := MicroBit.I2C.Controller;
      Addr   : constant I2C_Address := 16#20#;
      Data   : I2C_Data (0 .. 2);
      Status : I2C_Status;
   begin
      if not MicroBit.I2C.Initialized then
         --  I2C is not initialized
         Raise Program_Error with "Motors has not been initialized!";
      end if;

      Data := (0, 1, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);
      Data := (16#2#, 1, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);

      Running := True;
   end Backward;

   --------------------------
   --   Maqueen_Turn_Left  --
   --------------------------
   procedure Turn_Left
   is
      Ctrl   : constant Any_I2C_Port := MicroBit.I2C.Controller;
      Addr   : constant I2C_Address := 16#20#;
      Data   : I2C_Data (0 .. 2);
      Status : I2C_Status;
   begin
      if not MicroBit.I2C.Initialized then
         --  I2C is not initialized
         Raise Program_Error with "Motors has not been initialized!";
      end if;

      Data := (0, 0, 0);
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);
      Data := (16#2#, 0, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);

      Running := True;
   end Turn_Left;

   --------------------------
   --   Maqueen_Spin_Left  --
   --------------------------
   procedure Spin_Left
   is
      Ctrl   : constant Any_I2C_Port := MicroBit.I2C.Controller;
      Addr   : constant I2C_Address := 16#20#;
      Data   : I2C_Data (0 .. 2);
      Status : I2C_Status;
   begin
      if not MicroBit.I2C.Initialized then
         --  I2C is not initialized
         Raise Program_Error with "Motors has not been initialized!";
      end if;

      Data := (0, 1, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);
      Data := (16#2#, 0, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);

      Running := True;
   end Spin_Left;

   --------------------------
   --  Maqueen_Turn_Right  --
   --------------------------
   procedure Turn_Right
   is
      Ctrl   : constant Any_I2C_Port := MicroBit.I2C.Controller;
      Addr   : constant I2C_Address := 16#20#;
      Data   : I2C_Data (0 .. 2);
      Status : I2C_Status;
   begin
      if not MicroBit.I2C.Initialized then
         --  I2C is not initialized
         Raise Program_Error with "Motors has not been initialized!";
      end if;

      Data := (0, 0, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);
      Data := (16#2#, 0, 0);
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);

      Running := True;
   end Turn_Right;

   --------------------------
   --  Maqueen_Spin_Right  --
   --------------------------
   procedure Spin_Right
   is
      Ctrl   : constant Any_I2C_Port := MicroBit.I2C.Controller;
      Addr   : constant I2C_Address := 16#20#;
      Data   : I2C_Data (0 .. 2);
      Status : I2C_Status;
   begin
      if not MicroBit.I2C.Initialized then
         --  I2C is not initialized
         Raise Program_Error with "Motors has not been initialized!";
      end if;

      Data := (0, 0, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);
      Data := (16#2#, 1, Hal.Uint8(MotorsSpeed));
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);

      Running := True;
   end Spin_Right;

   --------------------------
   --     Maqueen_Stop     --
   --------------------------
   procedure Stop
   is
      Ctrl   : constant Any_I2C_Port := MicroBit.I2C.Controller;
      Addr   : constant I2C_Address := 16#20#;
      Data   : I2C_Data (0 .. 2);
      Status : I2C_Status;
   begin
      if not MicroBit.I2C.Initialized then
         --  I2C is not initialized
         Raise Program_Error with "Motors has not been initialized!";
      end if;

      Data := (0, 0, 0);
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);
      Data := (16#2#, 0, 0);
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);

      Running := False;
   end Stop;

   --------------------------
   --       Motor_Run      --
   --------------------------
   procedure Motor_Run(MotorToMove : Motor; WhichDirection : Direction; Spd :
                       Speed)
   is
      Ctrl   : constant Any_I2C_Port := MicroBit.I2C.Controller;
      Addr   : constant I2C_Address := 16#20#;
      Data   : I2C_Data (0 .. 2);
      Status : I2C_Status;
   begin
      if not MicroBit.I2C.Initialized then
         --  I2C is not initialized
         Raise Program_Error with "Motors has not been initialized!";
      end if;

      case MotorToMove is
         when Left_Motor  => Data(0) := 0;
         when Right_Motor => Data(0) := 16#2#;
      when others       => null;
      end case;

      case WhichDirection is
         when Forwards  => Data(1) := 0;
         when Backwards => Data(1) := 1;
      when others       => null;
      end case;

      Data(2) := HAL.Uint8(Spd);
      Ctrl.Master_Transmit(Addr => Addr, Data => Data, Status =>Status);

      Running := True;
   end Motor_Run;

   -------------------------------
   --     Maqueen_Is_Running    --
   -------------------------------
   function Is_Running return Boolean
   is
   begin
      return Running;
   end Is_Running;

   -------------------------------
   --     Read_Patrol_Right     --
   -------------------------------
   function Read_Patrol_Right return Boolean
   is
   begin
      return MicroBit.IOs.Set (13);
   end Read_Patrol_Right;

   -------------------------------
   --     Read_Patrol_Left      --
   -------------------------------
   function Read_Patrol_Left return Boolean
   is
   begin
      return MicroBit.IOs.Set (14);
   end Read_Patrol_Left;


   -------------------------------
   --     Left_Light_Status     --
   -------------------------------
   function Left_Light_Status return Boolean
   is
   begin
      return MicroBit.IOs.Set (8);
   end Left_Light_Status;

   -------------------------------
   --     Right_Light_Status     --
   -------------------------------
   function Right_Light_Status return Boolean
   is
   begin
      return MicroBit.IOs.Set (12);
   end Right_Light_Status;

begin
   null;
end Maqueen;
