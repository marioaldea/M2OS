pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with stdint_h;

package pins_arduino_h is

   NUM_DIGITAL_PINS : constant := 20;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:28
   NUM_ANALOG_INPUTS : constant := 6;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:29
   --  arg-macro: function analogInputToDigitalPin (p)
   --    return (p < 6) ? (p) + 14 : -1;
   --  arg-macro: function digitalPinHasPWM (p)
   --    return (p) = 3  or else  (p) = 5  or else  (p) = 6  or else  (p) = 9  or else  (p) = 10  or else  (p) = 11;

   PIN_SPI_SS : constant := (10);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:38
   PIN_SPI_MOSI : constant := (11);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:39
   PIN_SPI_MISO : constant := (12);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:40
   PIN_SPI_SCK : constant := (13);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:41

   PIN_WIRE_SDA : constant := (18);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:48
   PIN_WIRE_SCL : constant := (19);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:49

   LED_BUILTIN : constant := 13;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:54

   PIN_A0 : constant := (14);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:56
   PIN_A1 : constant := (15);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:57
   PIN_A2 : constant := (16);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:58
   PIN_A3 : constant := (17);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:59
   PIN_A4 : constant := (18);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:60
   PIN_A5 : constant := (19);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:61
   PIN_A6 : constant := (20);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:62
   PIN_A7 : constant := (21);  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:63
   --  arg-macro: function digitalPinToPCICR (p)
   --    return ((p) >= 0  and then  (p) <= 21) ? (andPCICR) : ((uint8_t *)0);
   --  arg-macro: function digitalPinToPCICRbit (p)
   --    return ((p) <= 7) ? 2 : (((p) <= 13) ? 0 : 1);
   --  arg-macro: function digitalPinToPCMSK (p)
   --    return ((p) <= 7) ? (andPCMSK2) : (((p) <= 13) ? (andPCMSK0) : (((p) <= 21) ? (andPCMSK1) : ((uint8_t *)0)));
   --  arg-macro: function digitalPinToPCMSKbit (p)
   --    return ((p) <= 7) ? (p) : (((p) <= 13) ? ((p) - 8) : ((p) - 14));
   --  arg-macro: function digitalPinToInterrupt (p)
   --    return (p) = 2 ? 0 : ((p) = 3 ? 1 : NOT_AN_INTERRUPT);
   --  unsupported macro: SERIAL_PORT_MONITOR Serial
   --  unsupported macro: SERIAL_PORT_HARDWARE Serial

   SS : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:43
   pragma Import (CPP, SS, "_ZL2SS");

   MOSI : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:44
   pragma Import (CPP, MOSI, "_ZL4MOSI");

   MISO : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:45
   pragma Import (CPP, MISO, "_ZL4MISO");

   SCK : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:46
   pragma Import (CPP, SCK, "_ZL3SCK");

   SDA : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:51
   pragma Import (CPP, SDA, "_ZL3SDA");

   SCL : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:52
   pragma Import (CPP, SCL, "_ZL3SCL");

   A0 : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:65
   pragma Import (CPP, A0, "_ZL2A0");

   A1 : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:66
   pragma Import (CPP, A1, "_ZL2A1");

   A2 : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:67
   pragma Import (CPP, A2, "_ZL2A2");

   A3 : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:68
   pragma Import (CPP, A3, "_ZL2A3");

   A4 : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:69
   pragma Import (CPP, A4, "_ZL2A4");

   A5 : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:70
   pragma Import (CPP, A5, "_ZL2A5");

   A6 : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:71
   pragma Import (CPP, A6, "_ZL2A6");

   A7 : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/variants/standard/pins_arduino.h:72
   pragma Import (CPP, A7, "_ZL2A7");

end pins_arduino_h;
