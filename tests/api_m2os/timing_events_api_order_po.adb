----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Direct_IO;
with M2.HAL;
with Tests_Reports;

package body Timing_Events_API_Order_PO is
   package DIO renames M2.Direct_IO;

   use type API.Time;

   Handler1_Period : constant := 50;
   Handler2_Period : constant := 100;
   Handler3_Period : constant := 150;

   protected body PO is

      procedure PO_Handler1 (Event : in out API.Timing_Event) is
      begin
         DIO.Put_Line ("In PO_Handler1 step" & Step'Img);
         Tests_Reports.Assert (not M2.HAL.Are_Interrupts_Enabled);
         Tests_Reports.Assert (Step = 3 or Step = 6 or Step = 7 or
                                 Step = 9 or Step = 11);
         Step := Step + 1;

         API.Set_Handler (Event'Unrestricted_Access,
                    API.Get_Time + Handler1_Period,
                    PO_Handler1'Access);
      end PO_Handler1;

      procedure PO_Handler2 (Event : in out API.Timing_Event) is
      begin
         DIO.Put_Line ("In PO_Handler2 step" & Step'Img);
         Tests_Reports.Assert (not M2.HAL.Are_Interrupts_Enabled);
         Tests_Reports.Assert (Step = 2 or Step = 5 or Step = 8);
         Step := Step + 1;

         API.Set_Handler (Event'Unrestricted_Access,
                    API.Get_Time + Handler2_Period,
                    PO_Handler2'Access);
      end PO_Handler2;

      procedure PO_Handler3 (Event : in out API.Timing_Event) is
      begin
         DIO.Put_Line ("In PO_Handler3 step" & Step'Img);
         Tests_Reports.Assert (not M2.HAL.Are_Interrupts_Enabled);
         Tests_Reports.Assert (Step = 1 or Step = 4 or Step = 10);
         Step := Step + 1;

         API.Set_Handler (Event'Unrestricted_Access,
                    API.Get_Time + Handler3_Period,
                    PO_Handler3'Access);
      end PO_Handler3;

   end PO;

end Timing_Events_API_Order_PO;
