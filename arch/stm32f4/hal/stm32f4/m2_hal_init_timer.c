
#include "ARMCM4_FP.h"

void m2_hal_init_timer (uint32_t period_in_ticks)
{
  SysTick_Config(period_in_ticks);
}
