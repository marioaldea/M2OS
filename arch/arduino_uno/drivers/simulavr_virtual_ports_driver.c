/* This port correponds to the "-W 0x20,-" command line option. */
#define special_output_port (*((volatile char *)0x20))

/* This port correponds to the "-R 0x22,-" command line option. */
#define special_input_port  (*((volatile char *)0x22))

/* Open */
void simulavr_virtual_ports_open(int mode) {
  char *c = "Driver simulavr_virtual_ports\n";

  for(c = str; *c; c++) {
    special_output_port = *c;
  }
}

/* write */
int simulavr_virtual_ports_write(char *addr, int bytes) {
  int i;
  for(i=0; i<bytes; i++) {
    special_output_port = addr[i];
  }

  return bytes;
}
