----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Synchronous_Task_Control;
with Ada.Real_Time;

with AdaX_Dispatching_Stack_Sharing;
with M2.Direct_IO;

with Tests_Reports;

package body Suspension_Object_Open_Tasks is
   package DIO renames M2.Direct_IO;
   package STC renames Ada.Synchronous_Task_Control;

   Step_Counter : Natural := 0 with Volatile;
   Num_Of_Loops : constant := 3;

   Suspension_Object : Ada.Synchronous_Task_Control.Suspension_Object;

   ----------------
   -- Task1_Body --
   ----------------

   Loop_Counter_1 : Integer := 0;

   procedure Task1_Body is

      use type Ada.Real_Time.Time;

      Next_Time : Ada.Real_Time.Time;
      Period : constant Ada.Real_Time.Time_Span :=
        Ada.Real_Time.Milliseconds (50);

   begin
      Next_Time := Ada.Real_Time.Clock;

      loop
         DIO.Put ("Task1:");
         DIO.Put (Integer (Step_Counter));
         DIO.Put (Loop_Counter_1);

         if Step_Counter mod 2 = 0 then
            Tests_Reports.Assert (Step_Counter = Loop_Counter_1 * 2);
         else
            Tests_Reports.Assert (Step_Counter = Loop_Counter_1 * 2 + 1);
         end if;
         Step_Counter := Step_Counter + 1;

         Loop_Counter_1 := Loop_Counter_1 + 1;

         if Loop_Counter_1 = Num_Of_Loops then
            Tests_Reports.Test_OK;
         end if;

         DIO.Put ("Task1:A");
         STC.Set_True (Suspension_Object);

         DIO.Put ("Task1:B");
         Next_Time := Next_Time + Period;
         delay until Next_Time;
      end loop;
   end Task1_Body;

   ----------------
   -- Task2_Body --
   ----------------

   Loop_Counter_2 : Integer := 0;

   procedure Task2_Body is
   begin
      loop
         DIO.Put ("Task2:");
         DIO.Put (Integer (Step_Counter));
         DIO.Put (Loop_Counter_2);

         if Step_Counter mod 2 = 0 then
            Tests_Reports.Assert (Step_Counter = Loop_Counter_2 * 2);
         else
            Tests_Reports.Assert (Step_Counter = Loop_Counter_2 * 2 + 1);
         end if;
         Step_Counter := Step_Counter + 1;

         Loop_Counter_2 := Loop_Counter_2 + 1;
         Tests_Reports.Assert (Loop_Counter_2 <= Num_Of_Loops);

         DIO.Put ("Task2:A");
         STC.Suspend_Until_True (Suspension_Object);
      end loop;
   end Task2_Body;

   Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => null,
      Body_Ac  => Task1_Body'Access,
      Priority => 8);

   Task_2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => null,
      Body_Ac  => Task2_Body'Access,
      Priority => 5);

end Suspension_Object_Open_Tasks;
