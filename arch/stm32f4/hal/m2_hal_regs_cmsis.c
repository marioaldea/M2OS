#include <stdint.h>

#ifdef M2_TARGET_microbit
//#include "ARMCM0.h"
#include "nrf.h"
#include "core_cmFunc.h"

#elif defined(M2_TARGET_stm32f4)
#include "ARMCM4_FP.h"
#include "cmsis_gcc.h"

#else
#error Unexpected M2_TARGET value
#endif

uint32_t get_systick_val() {
  return SysTick->VAL;
}

uint32_t get_control(void) {
  return __get_CONTROL();
}

uint32_t get_primask(void) {
  return __get_PRIMASK();
}

void set_primask(uint32_t priMask) {
  __set_PRIMASK(priMask);
}

#ifdef M2_TARGET_stm34f4
uint32_t get_basepri(void) {
  return __get_BASEPRI();
}
#endif
