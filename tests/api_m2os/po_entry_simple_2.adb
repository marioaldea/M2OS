----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Direct_IO;

with Tests_Reports;

package body PO_Entry_Simple_2 is
   package DIO renames M2.Direct_IO;

   --------
   -- PO --
   --------

   protected body PO is

      entry Wait when Is_Open is
      begin
         DIO.Put ("-PO.Wait-");
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         DIO.Put ("-PO.Open-");
         Is_Open := True;
      end Open;
   end PO;

   ---------------------
   -- Other_Task_Init --
   ---------------------

   procedure Other_Task_Init is
   begin
      null;
   end Other_Task_Init;

   Loop_Counter : Integer := 0;

   ---------------------
   -- Other_Task_Body --
   ---------------------

   procedure Other_Task_Body is
   begin
      DIO.Put ("-Other-");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (Loop_Counter);

      case Loop_Counter is
         when 0 =>
            Tests_Reports.Assert (Step_Counter = 1);

         when 1 =>
            Tests_Reports.Assert (Step_Counter = 2);

         when 2 =>
            Tests_Reports.Assert (Step_Counter = 4);

         when 3 =>
            Tests_Reports.Assert (Step_Counter = 6);

         when 4 =>
            Tests_Reports.Assert (Step_Counter = 8);

         when others =>
            Tests_Reports.Assert (False);
      end case;

      Step_Counter := Step_Counter + 1;
      Loop_Counter := Loop_Counter + 1;

      DIO.Put (">wait");
      PO.Wait;
   end Other_Task_Body;

end PO_Entry_Simple_2;
