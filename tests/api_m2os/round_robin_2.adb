----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;

with Tests_Reports;

package body Round_Robin_2 is
   package DIO renames M2.Direct_IO;
   package STC renames Ada.Synchronous_Task_Control;

   Loop_Counter : Integer := 0;

   ---------------
   -- Task_Init --
   ---------------

   procedure Task_Init is
   begin
      DIO.Put ("-TB Init-");
      STC.Suspend_Until_True (Suspension_Object_2);
   end Task_Init;

   ---------------
   -- Task_Body --
   ---------------

   procedure Task_Body is
   begin
      DIO.Put ("-TB-");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (Loop_Counter);

      if Loop_Counter mod 2 = 0 then
         Tests_Reports.Assert (Step_Counter = Loop_Counter * 3 + 2);
      else
         Tests_Reports.Assert (Step_Counter = Loop_Counter * 3 + 1);
      end if;

      Step_Counter := Step_Counter + 1;

      Loop_Counter := Loop_Counter + 1;
      Tests_Reports.Assert (Loop_Counter <= Num_Of_Loops);

      STC.Suspend_Until_True (Suspension_Object_2);
   end Task_Body;

   ------------
   -- Task_2 --
   ------------

   Task_2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_Init'Access,
      Body_Ac  => Task_Body'Access,
      Priority => 5);

end Round_Robin_2;
