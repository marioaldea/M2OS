----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
------------------------------------------------------------------------
--
--  "Freeze!" children game.
--  Details in comments in file 'freeze_game.ads'.
--
------------------------------------------------------------------------
with Freeze_Game.Buzzer;
with Freeze_Game.PIR;

with Arduino.Melodies;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

package body Freeze_Game.Game_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span, Arduino.Time_MS_16;

   Detected_Once : Boolean;

   --  Next activation time

   Next_Time : RT.Time;

   --------------------
   -- Game_Task_Init --
   --------------------

   procedure Game_Task_Init is
   begin
      DIO.Put_Line ("Game_Task_Init");

      Next_Time := RT.Clock;
   end Game_Task_Init;

   --------------------
   -- Game_Task_Body --
   --------------------

   type Freeze_Periods is mod Freeze_Phase_Periods;
   Freeze_Periods_Count : Freeze_Periods := 0;

   Current_Phase : Game_Phase := Move;
   Next_Phase : Game_Phase := Move;

   procedure Game_Task_Body is
      Detections_Count : Natural;
   begin
      if Current_Phase /= Freeze then
         PIR.Reset;
      end if;

      Current_Phase := Next_Phase;
      case Current_Phase is
         when Move =>
            DIO.Put (" Move ");
            Buzzer.Play_Melody
              (Melody   => Arduino.Melodies.Twinkle_Twinkle_Little_Star'Access,
               Tempo_Ms => Arduino.Melodies.Moderato,
               Repeat   => True);

            Next_Phase := Pre_Freeze;

         when Pre_Freeze =>
            DIO.Put (" Pre_Freeze ");
            Buzzer.Tone (Frequency_Hz  => Arduino.Melodies.Beep_Frecquency_Hz,
                         Duration_Ms => 100,
                         Repeat_Period_Ms => 200);

            Next_Phase := Freeze;

            Freeze_Periods_Count := 0;
            Detected_Once := False;
            PIR.Reset;

         when Freeze =>
            DIO.Put (" Freeze ");
            Buzzer.Silent;
            Detections_Count := PIR.Get_Num_Of_Detections_And_Reset;
            --DIO.Put ("PIR detections:");
            DIO.Put (Detections_Count);

            Freeze_Periods_Count := Freeze_Periods_Count + 1;

            if Freeze_Periods_Count = 0 then
               Next_Phase := Move;

            elsif Detections_Count >= Positive_Detections_In_Polling_Period then
               if Detected_Once then
                  --  Second movement detected
                  Next_Phase := Detected;
               else
                  Detected_Once := True;
                  --  First movement detected
                  Next_Phase := Warning;
               end if;
            end if;

         when Warning =>
            DIO.Put (" Warning ");

            Buzzer.Tone (Frequency_Hz  => Arduino.Melodies.Note_D1,
                         Duration_Ms => 2 * Waring_Phase_Period_Ms / 9,
                         Repeat_Period_Ms => Waring_Phase_Period_Ms / 3);

            Next_Phase := Freeze;
            PIR.Reset;

         when Detected =>
            DIO.Put (" Detected ");
            --  Second movement detected

            Buzzer.Tone (Frequency_Hz  => Arduino.Melodies.Note_D2,
                         Duration_Ms => Detect_Phase_Period_Ms);

            Next_Phase := Move;

      end case;

      Next_Time := Next_Time + Phase_Period (Current_Phase);
      delay until Next_Time;
   end Game_Task_Body;

   ---------------
   -- Game Task --
   ---------------

   Game_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Game_Task_Init'Unrestricted_Access,
      Body_Ac  => Game_Task_Body'Unrestricted_Access,
      Priority => Freeze_Game.Game_Task_Prio);

end Freeze_Game.Game_Task;
