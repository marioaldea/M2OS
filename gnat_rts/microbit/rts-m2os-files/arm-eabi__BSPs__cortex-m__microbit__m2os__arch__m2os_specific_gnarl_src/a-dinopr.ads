------------------------------------------------------------------------------
--                                                                          --
--                         GNAT RUN-TIME COMPONENTS                         --
--                                                                          --
--       A D A . D I S P A T C H I N G . N O N _ P R E E M P T I V E        --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
-- This specification is derived from the Ada Reference Manual for use with --
-- GNAT.  In accordance with the copyright of that document, you can freely --
-- copy and modify this specification,  provided that if you redistribute a --
-- modified version,  any changes that you have made are clearly indicated. --
--                                                                          --
------------------------------------------------------------------------------
with System.OS_Interface;

package Ada.Dispatching.Non_Preemptive is
   pragma Preelaborate (Non_Preemptive);

   procedure Yield_To_Higher
     renames System.OS_Interface.Yield_To_Higher;

   --  procedure Yield_To_Same_Or_Higher renames Yield;
   --  M2OS: not supported in M2OS
end Ada.Dispatching.Non_Preemptive;
