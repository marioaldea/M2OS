----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Arduino.CCS811;

package CO2_Sensor.LCD_Tasks is

   procedure Set_Temperature (Temp : Float);
   
   procedure Set_Humidity (Hum : Float);
   
   procedure Set_Humidity_Level (Hum : Humidity_Level);
   
   procedure Set_CO2_PPM (CO2_PPM : Arduino.CCS811.CO2_PPM_T);
   
   procedure Set_TVOC_PPB (TVOC_PPB: Arduino.CCS811.TVOC_PPB_T);
   
   procedure Set_IDA (IDA : IDA_Level);

end CO2_Sensor.LCD_Tasks;
