----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
pragma Profile (Ravenscar);
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);
with M2.Direct_IO;

with System;

with Tests_Reports;

procedure Hello_World_Test is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put ("Main Task");
   DIO.New_Line;
   loop
      for D in 0 .. 10 loop
         for U in 0 .. 9 loop
            DIO.Put (D*10 + U);
            DIO.Put (" ");
         end loop;
         DIO.New_Line;
      end loop;
      DIO.Put ("M2 Hello M2");
      DIO.New_Line;
      DIO.Put ("Integer'size ");
      DIO.Put (Integer'Size, 10);
      DIO.New_Line;
      DIO.Put ("Positive'size ");
      DIO.Put (Positive'Size, 10);
      DIO.New_Line;
      DIO.Put ("Natural'size ");
      DIO.Put (Natural'Size, 10);
      DIO.New_Line;
      DIO.Put ("Address'size ");
      DIO.Put (System.Address'Size, 10);
      DIO.New_Line;

      for I in -101 .. 101 loop
         DIO.Put (I);
         DIO.Put (" ");
      end loop;

      Tests_Reports.Test_OK;
   end loop;
   DIO.Put ("Main ends");
end Hello_World_Test;
