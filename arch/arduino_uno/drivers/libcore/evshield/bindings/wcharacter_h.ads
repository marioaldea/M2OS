pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Arduino_h;

package WCharacter_h is

   function isAlphaNumeric (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:46
   pragma Import (CPP, isAlphaNumeric, "_Z14isAlphaNumerici");

   function isAlpha (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:54
   pragma Import (CPP, isAlpha, "_Z7isAlphai");

   function isAscii (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:62
   pragma Import (CPP, isAscii, "_Z7isAsciii");

   function isWhitespace (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:69
   pragma Import (CPP, isWhitespace, "_Z12isWhitespacei");

   function isControl (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:76
   pragma Import (CPP, isControl, "_Z9isControli");

   function isDigit (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:83
   pragma Import (CPP, isDigit, "_Z7isDigiti");

   function isGraph (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:90
   pragma Import (CPP, isGraph, "_Z7isGraphi");

   function isLowerCase (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:97
   pragma Import (CPP, isLowerCase, "_Z11isLowerCasei");

   function isPrintable (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:104
   pragma Import (CPP, isPrintable, "_Z11isPrintablei");

   function isPunct (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:112
   pragma Import (CPP, isPunct, "_Z7isPuncti");

   function isSpace (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:121
   pragma Import (CPP, isSpace, "_Z7isSpacei");

   function isUpperCase (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:128
   pragma Import (CPP, isUpperCase, "_Z11isUpperCasei");

   function isHexadecimalDigit (c : int) return Arduino_h.boolean;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:136
   pragma Import (CPP, isHexadecimalDigit, "_Z18isHexadecimalDigiti");

   function toAscii (c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:144
   pragma Import (CPP, toAscii, "_Z7toAsciii");

   function toLowerCase (c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:156
   pragma Import (CPP, toLowerCase, "_Z11toLowerCasei");

   function toUpperCase (c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WCharacter.h:163
   pragma Import (CPP, toUpperCase, "_Z11toUpperCasei");

end WCharacter_h;
