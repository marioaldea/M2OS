----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Demo program that moves two coordinated servos following a trayectory.
--  See coordinated_servos.ads for details.

with Ada.Real_Time;
with Arduino;
with Coordinated_Servos_Demo.Servos;

package Coordinated_Servos_Demo.Trayectory_Data is

   Servo_Pins : constant Servos.Servo_Pins :=
     (Arduino.PWM_D3, Arduino.PWM_D5);

   Trayectory_Cicle_Time : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (1_000);

   Trayectory : aliased Servos.Trayectory :=
     ((10, 10), (20, 50), (40, 100), (60, 100), (40, 100), (20, 50));
end Coordinated_Servos_Demo.Trayectory_Data;
