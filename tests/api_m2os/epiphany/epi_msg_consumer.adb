----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Authors: David Garc�a Villaescusa (garciavd@unican.es)
--           Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Interfaces;
with E_Lib;
with M2.Direct_IO;
with Tests_Reports;
with Epiphany_Messages;
use Epiphany_Messages;
with Ada.Real_Time;
use Ada.Real_Time;

package body Epi_Msg_Consumer is
   package DIO renames M2.Direct_IO;
   
   Next_Time : Ada.Real_Time.Time;

   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);
   
   
   SP_Acc : Epiphany_Messages.SP_Id;
   SP_Num : Epiphany_Messages.SP_Index := 0;
   Num : Integer;
   
   Value : Integer;
   
   --------------------
   -- Main_Task_Init --
   --------------------
      
   procedure Main_Task_Init is
      Core_Id : Interfaces.Unsigned_32;
      Core : E_Lib.Core;
      Success : Boolean;
   begin
      DIO.Put_Line ("-Epi_Msg_Consumer-Init");
      
      Core_Id := E_Lib.E_Get_Coreid;
   
      E_Lib.E_Coords_From_Coreid (Core_Id, Core.Row, Core.Col);
   
      Num := 3;
      
      Success := Epiphany_Messages.Init_Sampling_Port (Core, SP_Num, Num'Address, Num'Size, SP_Acc);
      Next_Time := Ada.Real_Time.Clock;
      
   end Main_Task_Init;
   
   --------------------
   -- Main_Task_Body --
   --------------------
      
   procedure Main_Task_Body is
      Is_New : Boolean := False;
   begin
      while Is_New = False loop
         Is_New := Epiphany_Messages.Read_Sampling_Port (SP_Acc, Value'Address, Value'Size);
      end loop;
      
      if Value = 5 then
         DIO.Put_Line ("Final value: " & Value'Img);
         Tests_Reports.Test_OK;
      end if;
      
      Next_Time := Next_Time + Period;
      delay until Next_Time; 

   end Main_Task_Body;

end Epi_Msg_Consumer;
