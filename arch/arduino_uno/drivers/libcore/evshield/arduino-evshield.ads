----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  EVShield is a shield that enables the use of NXT and EV3 motors and Sensors
--  from the Arduino Uno
--  (http://www.mindsensors.com/arduino/16-evshield-for-arduino-duemilanove-or-uno).
--
--  This file contains the parent package of the binding of the EVSHield
--  library.
--
--  To use this library download it from the GIT repository
--  (https://github.com/mindsensors/EVShield), copy the 'EVShield/ directory in
--  '~/Arduino/Libraries/' and "make" the libcore library.
----------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with EVShieldI2C_h;
with stdint_h;
with Interfaces.C.Extensions;
with SHDefines_h;
with Interfaces.C.Strings;
with Arduino;
with EVShield_h;

package Arduino.EVShield is
   -- EVShield botons
   BTN_RIGHT : constant := 4;
   BTN_LEFT : constant := 1;
   BTN_GO : constant := 2;
   -- Bank A sensors
   subtype SH_BankPort is unsigned;
   SH_BAS1 : constant SH_BankPort := 1;
   SH_BAS2 : constant SH_BankPort := 2;
   -- Bank B sensors
   SH_BBS1 : constant SH_BankPort := 3;
   SH_BBS2 : constant SH_BankPort := 4;

   --  the initialization of the EVShield;
   --	This function initializes the LED related timers, and communication protocols.
   --  @param protocol optional, specify the i2c protocol to use for the EVShield and highspeed i2c port
   procedure Init;

   --  Get the button state of the specific button on EVShield.
   --  @param btn      Button to get state for (BTN_GO, BTN_LEFT, BTN_RIGHT)
   --  @return true or false for specified button on the EVShield
   function Get_Button_State ( Btn : stdint_h.uint8_t ) return Extensions.bool;

   -- Methods for EVShield Bank A
   package Bank_A is

      --  Set the colors of LED on the EVShield;
      --  The values of red, green, blue are between 0 to 255.
      --  @param red      Intensity for red color (between 0 and 255)
      --  @param green      Intensity for green color (between 0 and 255)
      --  @param blue      Intensity for blue color (between 0 and 255)
      function Led_Set_RGB
        (R : stdint_h.uint8_t;
         G : stdint_h.uint8_t;
         B : stdint_h.uint8_t) return Extensions.bool;

   end Bank_A;

   -- Methods for EVShield Bank b
   package Bank_B is

      --  Set the colors of LED on the EVShield;
      --  The values of red, green, blue are between 0 to 255.
      --  @param red      Intensity for red color (between 0 and 255)
      --  @param green    Intensity for green color (between 0 and 255)
      --  @param blue     Intensity for blue color (between 0 and 255)
      function Led_Set_RGB
        (R : stdint_h.uint8_t;
         G : stdint_h.uint8_t;
         B : stdint_h.uint8_t) return Extensions.bool;

   end Bank_B;

private
   ---------------------
   -- EVShield object --
   ---------------------
   EVShield_obj : aliased EVShield_h.Class_EVShield.EVShield := EVShield_h.Class_EVShield.New_EVShield (16#34#,16#36#);

end Arduino.EVShield;
