----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of Grove-16x2 LCD library
--  https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/
--
--  IMPORTANT: change line of 74 of rgb_lcd.cpp replacing:
--      delayMicroseconds(50000);
--  by:
--      delay(50);
--  Arduino reference page for delayMicroseconds() says: "Currently, the largest
--  value that will produce an accurate delay is 16383". In fact, the use of
--  delayMicroseconds() with a long time causes a short delay that, some times,
--  avoids the proper initialization of the LCD.
----------------------------------------------------------------------------
with Interfaces.C.Strings;

package Arduino.LCD_I2C is
   pragma Elaborate_Body;

   type Cols_T is new Interfaces.Unsigned_8 range 0 .. 15;
   for Cols_T'Size use Interfaces.Unsigned_8'Size;

   type Rows_T is new Interfaces.Unsigned_8 range 0 .. 1;
   for Rows_T'Size use Interfaces.Unsigned_8'Size;

   procedure Begin_LCD (Cols : Interfaces.Unsigned_8;
                        Rows : Interfaces.Unsigned_8)
     with Inline_Always;

   procedure Clear
     with Inline_Always;

   procedure Set_Cursor (Cols : Cols_T;
                         Rows : Rows_T)
     with Inline_Always;

   procedure Print_Integer (I : Integer;
                            Base : Integer := 10)
     with Inline_Always;

   procedure Print_Null_Terminated_Char_Array
     (Str : Interfaces.C.Strings.char_array_access)
     with Inline_Always;

   procedure Print_Char (Char : Character)
     with Inline_Always;

   LCD_Size_In_Bytes : constant := 10;

   Sizeof_LCD : Integer with
     Import, Convention => C,
     External_Name => "sizeof_rgb_lcd";
   --  Defined in 'sizes_arduino_types.cpp' (created by Makefile)
   --  Takes the value of -1 if the library is not included.

private

   type LCD_Filling is
     array(1 .. LCD_Size_In_Bytes) of Interfaces.Unsigned_8;

   type LCD is limited record
      Filling: LCD_Filling;
   end record;
   pragma Import (CPP, LCD);
   for LCD'Size use LCD_Size_In_Bytes * 8;
   for LCD'Alignment use Standard'Maximum_Alignment;

   function New_LCD return LCD;
   pragma CPP_Constructor (New_LCD, "_ZN7rgb_lcdC1Ev");

   -----------------------
   -- Screen LCD object --
   -----------------------

   Screen : aliased LCD := New_LCD;

end Arduino.LCD_I2C;
