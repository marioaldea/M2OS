pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

with M2.Direct_IO;
with M2.HAL;

with Measurements;

package body CS_Suspensions_2 is
   package DIO renames M2.Direct_IO;

   --------
   -- PO --
   --------

   protected body PO is

      entry Wait when Is_Open is
      begin
         --DIO.Put ("-PO.Wait-");
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         --DIO.Put ("-PO.Open-");
         Is_Open := True;
      end Open;
   end PO;

   --------------
   -- PO_Param --
   --------------

   protected body PO_Param is

      entry Wait (Data_Out : Access Integer) when Is_Open is
      begin
         --DIO.Put ("-PO.Wait-");

         Data_Out.All := Counter;
         --DIO.Put (Data_Out.All);
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         --DIO.Put ("-PO.Open-");
         --DIO.Put (Counter);

         Counter := Counter + 1;
         Is_Open := True;
      end Open;
   end PO_Param;

   ----------------
   -- Other_Task --
   ----------------

   task body Other_Task is
   begin
      loop
         M2.HAL.Enable_Interrupts;

         --DIO.Put ("-Other-");

         Measurements.End_Measurement;

         Loop_Counter := Loop_Counter + 1;

         case Suspension_Primitive is
         when Suspension_Object =>
            Ada.Synchronous_Task_Control.Set_True (SO);
         when PO_Entry =>
            PO.Open;
         when PO_Entry_Param =>
            PO_Param.Open;
         when others =>
            null;
         end case;

         System.OS_Interface.Running_Thread_Ends_Job_Without_Blocking;
      end loop;
   end Other_Task;

end CS_Suspensions_2;
