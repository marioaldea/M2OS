----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  The main tasks waits on a suspension object and a periodic task sets it
--  to true.
--  The main task always find the Suspension Object closed and is activated
--  when the periodic task sets it to true.
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
with Ada.Synchronous_Task_Control;

with Suspension_Object_Closed_2;

with Tests_Reports;

procedure Suspension_Object_Closed_Test  is
   package DIO renames M2.Direct_IO;
   package S2 renames Suspension_Object_Closed_2;
   package STC renames Ada.Synchronous_Task_Control;

   Loop_Counter : Integer := 0;

begin
   DIO.Put ("-Suspension_Object_Closed_Test-");

   loop
      DIO.Put ("-Main-");

      Tests_Reports.Assert (S2.Step_Counter = Loop_Counter * 2);
      S2.Step_Counter := S2.Step_Counter + 1;

      Loop_Counter := Loop_Counter + 1;

      if Loop_Counter = S2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      STC.Suspend_Until_True (S2.Suspension_Object);
   end loop;

end Suspension_Object_Closed_Test;
