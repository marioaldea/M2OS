with System;

package Protected_Interrupt_Handler.PO is

   --------
   -- PO --
   --------

   protected PO
     with Interrupt_Priority => System.Interrupt_Priority'Last is

      entry Wait;

      procedure Open
        with Attach_Handler => Protected_Interrupt_Handler.Interrupt_Num;

   private
      Is_Open : Boolean := False;
   end PO;

end Protected_Interrupt_Handler.PO;
