----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of using Arduino.DHT and Arduino.LCD_I2C packages.
--  Reads temperature and humidity from a DHT11 sensor and displays them on
--  an LCD (accessed by I2C).
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |           DATA|-----------------|D4                   |
--     | DHT11      VDD|-----------------|VDD      Arduino Uno |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 |                     |
--                                       |                     |
--     -----------------                 |                     |
--     |               |                 |                     |
--     |            SCL|-----------------|A5                   |
--     |            SDA|-----------------|A4                   |
--     | LCD        VDD|-----------------|VDD                  |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino;
with Arduino.DHT.DHT11_Sensor;
with Arduino.LCD_I2C;
with Arduino.LCD_I2C.Advanced;

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure Weather_Station_No_Task is
   package DIO renames M2.Direct_IO;
   package LCD renames Arduino.LCD_I2C;

   use type Arduino.Digital_Pin_Out_Value;
   
   package DHT11 is new Arduino.DHT.DHT11_Sensor (Pin => 4);

   Measurement : aliased Arduino.DHT.Measurement := (11.0, 12.0);
   Ret : Integer := 0;
   
--     Sensor : aliased Arduino.DHT11.DHT11 := DHT11.New_DHT11 (DHT11_Pin);
   
   type Progress_Range is mod 2;
   Progress : constant array (Progress_Range) of Character :=
     ('.', ':');
   Progress_Index : Progress_Range := 0;
   
   --  Strings definition
   
   NUL : constant Interfaces.C.Char := Interfaces.C.Char'Val(0);
   T_Str : aliased Interfaces.C.Char_Array := ('T', ':', NUL);
   Cel_Str : aliased Interfaces.C.Char_Array := ('C', NUL);
   H_Str : aliased Interfaces.C.Char_Array := ('H', ':', NUL); 
   Percent_Str : aliased Interfaces.C.Char_Array := ('%', NUL);
   
begin
   LCD.Begin_LCD (16, 2);
   
   loop   
      --  Read sensor
      
      Ret := DHT11.Read (Measurement'Access);
      if Ret = 0 then
         DIO.Put ("Humidity:" & Integer (Measurement.Humidity)'Img);
         DIO.Put (" Temperature:" & Integer (Measurement.Temperature)'Img);
         DIO.New_Line;
      else
         DIO.Put ("DHT11 error:");
         DIO.Put (Ret);
         Measurement.Temperature := 0.0;
         Measurement.Humidity := 0.0;
      end if;
      
      --  Write on LCD
      
      LCD.Clear;
      LCD.Set_Cursor (0, 0);
      LCD.Print_Null_Terminated_Char_Array (T_Str'Unrestricted_Access);
      LCD.Advanced.Print_Double (Interfaces.C.double (Measurement.Temperature), 1);      
      LCD.Print_Null_Terminated_Char_Array (Cel_Str'Unrestricted_Access);
      LCD.Set_Cursor (0, 1);
      LCD.Print_Null_Terminated_Char_Array (H_Str'Unrestricted_Access);
      LCD.Advanced.Print_Double (Interfaces.C.double (Measurement.Humidity), 1);
      LCD.Print_Null_Terminated_Char_Array (Percent_Str'Unrestricted_Access);
  
      --  Show progress
      
      LCD.Set_Cursor (10, 0);
      LCD.Print_Char (Progress (Progress_Index));
      Progress_Index := Progress_Index + 1;
      
      Arduino.C_Delay (2_000);
   end loop;
end Weather_Station_No_Task;
