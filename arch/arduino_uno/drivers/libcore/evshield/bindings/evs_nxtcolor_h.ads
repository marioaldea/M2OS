pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
limited with EVShield_h;
with SHDefines_h;
with stdint_h;
with Interfaces.C.Extensions;

package EVs_NXTColor_h is

   package Class_EVs_NXTColor is
      type EVs_NXTColor is limited record
         mp_shield : access EVShield_h.Class_EVShield.EVShield;  -- EVs_NXTColor.h:34
         m_bp : aliased SHDefines_h.SH_BankPort;  -- EVs_NXTColor.h:37
         m_offset : aliased int;  -- EVs_NXTColor.h:41
      end record;
      pragma Import (CPP, EVs_NXTColor);

      function New_EVs_NXTColor return EVs_NXTColor;  -- EVs_NXTColor.h:44
      pragma CPP_Constructor (New_EVs_NXTColor, "_ZN12EVs_NXTColorC1Ev");

      function New_EVs_NXTColor (shield : access EVShield_h.Class_EVShield.EVShield; bp : SHDefines_h.SH_BankPort) return EVs_NXTColor;  -- EVs_NXTColor.h:47
      pragma CPP_Constructor (New_EVs_NXTColor, "_ZN12EVs_NXTColorC1EP8EVShield11SH_BankPort");

      function setType (this : access EVs_NXTColor; c_type : stdint_h.uint8_t) return Extensions.bool;  -- EVs_NXTColor.h:50
      pragma Import (CPP, setType, "_ZN12EVs_NXTColor7setTypeEh");

      function init
        (this : access EVs_NXTColor;
         shield : access EVShield_h.Class_EVShield.EVShield;
         bp : SHDefines_h.SH_BankPort) return Extensions.bool;  -- EVs_NXTColor.h:56
      pragma Import (CPP, init, "_ZN12EVs_NXTColor4initEP8EVShield11SH_BankPort");

      function readValue (this : access EVs_NXTColor) return stdint_h.uint8_t;  -- EVs_NXTColor.h:58
      pragma Import (CPP, readValue, "_ZN12EVs_NXTColor9readValueEv");

      function readColor (this : access EVs_NXTColor) return stdint_h.uint8_t;  -- EVs_NXTColor.h:60
      pragma Import (CPP, readColor, "_ZN12EVs_NXTColor9readColorEv");
   end;
   use Class_EVs_NXTColor;
end EVs_NXTColor_h;
