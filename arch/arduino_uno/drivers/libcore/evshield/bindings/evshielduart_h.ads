pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
limited with EVShield_h;
with SHDefines_h;
with stdint_h;
with Interfaces.C.Extensions;

package EVShieldUART_h is

   package Class_EVShieldUART is
      type EVShieldUART is limited record
         mp_shield : access EVShield_h.Class_EVShield.EVShield;  -- EVShieldUART.h:36
         m_bp : aliased SHDefines_h.SH_BankPort;  -- EVShieldUART.h:39
         m_offset : aliased int;  -- EVShieldUART.h:43
      end record;
      pragma Import (CPP, EVShieldUART);

      function New_EVShieldUART return EVShieldUART;  -- EVShieldUART.h:46
      pragma CPP_Constructor (New_EVShieldUART, "_ZN12EVShieldUARTC1Ev");

      function New_EVShieldUART (shield : access EVShield_h.Class_EVShield.EVShield; bp : SHDefines_h.SH_BankPort) return EVShieldUART;  -- EVShieldUART.h:49
      pragma CPP_Constructor (New_EVShieldUART, "_ZN12EVShieldUARTC1EP8EVShield11SH_BankPort");

      function getMode (this : access EVShieldUART) return stdint_h.uint8_t;  -- EVShieldUART.h:52
      pragma Import (CPP, getMode, "_ZN12EVShieldUART7getModeEv");

      function isDeviceReady (this : access EVShieldUART) return Extensions.bool;  -- EVShieldUART.h:55
      pragma Import (CPP, isDeviceReady, "_ZN12EVShieldUART13isDeviceReadyEv");

      function setType (this : access EVShieldUART; c_type : stdint_h.uint8_t) return Extensions.bool;  -- EVShieldUART.h:58
      pragma Import (CPP, setType, "_ZN12EVShieldUART7setTypeEh");

      function writeLocation
        (this : access EVShieldUART;
         loc : stdint_h.uint8_t;
         data : stdint_h.uint8_t) return Extensions.bool;  -- EVShieldUART.h:61
      pragma Import (CPP, writeLocation, "_ZN12EVShieldUART13writeLocationEhh");

      function readLocationInt (this : access EVShieldUART; loc : stdint_h.uint8_t) return stdint_h.int16_t;  -- EVShieldUART.h:64
      pragma Import (CPP, readLocationInt, "_ZN12EVShieldUART15readLocationIntEh");

      function readLocationByte (this : access EVShieldUART; loc : stdint_h.uint8_t) return stdint_h.uint8_t;  -- EVShieldUART.h:67
      pragma Import (CPP, readLocationByte, "_ZN12EVShieldUART16readLocationByteEh");

      function init
        (this : access EVShieldUART;
         shield : access EVShield_h.Class_EVShield.EVShield;
         bp : SHDefines_h.SH_BankPort) return Extensions.bool;  -- EVShieldUART.h:70
      pragma Import (CPP, init, "_ZN12EVShieldUART4initEP8EVShield11SH_BankPort");

      function setMode (this : access EVShieldUART; newMode : char) return stdint_h.uint8_t;  -- EVShieldUART.h:73
      pragma Import (CPP, setMode, "_ZN12EVShieldUART7setModeEc");

      function readValue (this : access EVShieldUART) return stdint_h.uint16_t;  -- EVShieldUART.h:76
      pragma Import (CPP, readValue, "_ZN12EVShieldUART9readValueEv");

      function readAndPrint
        (this : access EVShieldUART;
         loc : stdint_h.uint8_t;
         len : stdint_h.uint8_t) return Extensions.bool;  -- EVShieldUART.h:79
      pragma Import (CPP, readAndPrint, "_ZN12EVShieldUART12readAndPrintEhh");
   end;
   use Class_EVShieldUART;
end EVShieldUART_h;
