----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Reads temperature and humidity from a DHT11 sensor.
--  Uses the binding of the DTH library DTH.h (Grove Temperature And Humidity
--  Sensor)
--  https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |           DATA|-----------------|D4                   |
--     | DHT11      VDD|-----------------|VDD      Arduino Uno |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--
------------------------------------------------------------------------
--   Uses the generic package (slightly higher footprint than dht11_demo2 that
--   uses binding directly).
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino.DHT.DHT11_Sensor;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure DHT11_Demo is
   package DIO renames M2.Direct_IO;
   
   package DHT11 is new Arduino.DHT.DHT11_Sensor (Pin => 4);
   Measurement : aliased Arduino.DHT.Measurement;
   Ret : Integer;
begin
   DIO.Put_Line ("DHT11 Demo with generic package");

   loop
      DIO.Put_Line ("Read sensor");
      
      Ret := DHT11.Read (Measurement'Access);
      if Ret = 0 then
         DIO.Put ("Humidity:" & Integer (Measurement.Humidity)'Img);
         DIO.Put (" Temperature:" & Integer (Measurement.Temperature)'Img);
         DIO.New_Line;
      else
         DIO.Put ("DHT11 error:");
         DIO.Put (Ret);
         Measurement.Temperature := 0.0;
         Measurement.Humidity := 0.0;
      end if;

      Arduino.C_Delay (MS => 1_000);
   end loop;
   
end DHT11_Demo;
