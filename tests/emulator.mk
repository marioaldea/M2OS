# Emulator command for each target architecture

ifeq ($(M2_TARGET),stm32f4)
  # STM32F4 target
  RUN_COMMAND = arm-eabi-gnatemu --board=stm32f4 $(EXE)
else ifeq ($(M2_TARGET),arduino_uno)
  # Arduino Uno target
  RUN_COMMAND = simulavr -d atmega328 \
                -W 0xc6,- -T m2__end_execution__end_execution -f
else ifeq ($(M2_TARGET),rpi)
  # RPi target
  RUN_COMMAND = qemu-system-arm -M raspi -serial stdio -kernel $(EXE)
else ifeq ($(M2_TARGET),microbit)
  # Microbit target
  #RUN_COMMAND = qemu-system-arm -M microbit -serial stdio -display none -device loader,file=
  RUN_COMMAND = qemu-system-arm -M microbit -nographic -device loader,file=$(EXE)
else ifeq ($(M2_TARGET),$(filter $(M2_TARGET), epiphany))
  # epiphany target
  RUN_COMMAND = NO_EMULATOR_AVAILABLE_FOR_$(M2_TARGET)
else
  # any other target
  $(error Target $(M2_TARGET) is not supported)
endif
