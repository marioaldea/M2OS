with Ada.Real_Time;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;

with CS_Delay_Past_Time_2;

with Measurements_LoRes;

package body CS_Delay_Past_Time_1 is
   package DIO renames M2.Direct_IO;
   package S2 renames CS_Delay_Past_Time_2;
   package Measurements renames Measurements_LoRes;

   Loop_Counter : Integer := 0;
      
   -----------------
   -- Task_1_Init --
   -----------------
   
   procedure Task_1_Init is
   begin
      Measurements.Start_Measurement;
   end Task_1_Init;
      
   -----------------
   -- Task_1_Body --
   -----------------

   procedure Task_1_Body is
   begin
      --  DIO.Put ("-Task1-");
      
      if Loop_Counter = S2.Num_Of_Loops then
         Measurements.End_Measurement;

--           DIO.Put ("L1=");
--           DIO.Put (Loop_Counter);
--           DIO.Put (" L2=");
--           DIO.Put (S2.Loop_Counter);
         Measurements.Assert_Equal (S2.Num_Of_Loops, S2.Loop_Counter, 1);

         DIO.Put (" CSTime(us)=");
         DIO.Put (Integer (Measurements.Measurement_Time) * 100 /
                  (Loop_Counter + S2.Loop_Counter) * 10);
         Measurements.Finish;
      end if;

      Loop_Counter := Loop_Counter + 1;

      delay until Ada.Real_Time.Time_First;
   end Task_1_Body;
   
   ------------
   -- Task_2 --
   ------------

   Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => null,
      Body_Ac  => Task_1_Body'Access,
      Priority => S2.Task_2_Prio);
   
end CS_Delay_Past_Time_1;
