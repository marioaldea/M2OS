with Ada.Text_IO; use Ada.Text_IO;
with Ada.Directories;

with Libadalang.Iterators;
with Libadalang.Common;    use Libadalang.Common;
with Libadalang.Rewriting;
with LAL_Extensions;

with Langkit_Support.Text;

with GNATCOLL.Utils;

with M2OS_IO;
with M2OS_Utils;
with M2OS_Processing.Helper;

package body M2OS_Processing is

   package LAL_RW renames Libadalang.Rewriting;
   package LAL_It renames Libadalang.Iterators;
   package M2OS_Helper renames M2OS_Processing.Helper;

   -------------------
   -- Get_Proc_Name --
   -------------------

   function Get_Proc_Name (Node : Subp_Body'Class) return String is
      Proc_Name : constant End_Name := F_End_Name (Node);
   begin
      return M2OS_IO.Trim (M2OS_Helper.Get_Node_As_String (Proc_Name.As_Ada_Node));
   end Get_Proc_Name;

   ----------------------------------
   -- Get_Main_Task_Init_Procedure --
   ----------------------------------

   function Get_Main_Task_Init_Procedure (Node : Subp_Body'Class) return String is
      Task_Name : constant String := Get_Proc_Name (Node);
   begin
      return Task_Name & "_Init";
   end Get_Main_Task_Init_Procedure;

   ---------------------------------------
   -- Get_Main_Task_Main_Loop_Procedure --
   ---------------------------------------

   function Get_Main_Task_Main_Loop_Procedure (Node : Subp_Body'Class) return String is
      Task_Name : constant String := Get_Proc_Name (Node);
   begin
      return Task_Name & "_Body";
   end Get_Main_Task_Main_Loop_Procedure;

   -------------------
   -- Get_Task_Name --
   -------------------

   function Get_Task_Name (Node : Task_Body'Class) return String is
      Task_Name : constant Defining_Name := F_Name (Node);
   begin
      return M2OS_IO.Trim (M2OS_Helper.Get_Node_As_String (Task_Name.As_Ada_Node));
   end Get_Task_Name;

   -------------------
   -- Get_Task_Name --
   -------------------

   function Get_Task_Name (Node : Single_Task_Decl) return String is
      Task_Name : constant Defining_Name := P_Defining_Name (Node);
   begin
      return M2OS_IO.Trim (M2OS_Helper.Get_Node_As_String (Task_Name.As_Ada_Node));
   end Get_Task_Name;

   -----------------------------
   -- Get_Task_Init_Procedure --
   -----------------------------

   function Get_Task_Init_Procedure (Node : Task_Body'Class) return String is
      Task_Name : constant String := Get_Task_Name (Node);
   begin
      return Task_Name & "_Init";
   end Get_Task_Init_Procedure;

   ----------------------------------
   -- Get_Task_Main_Loop_Procedure --
   ----------------------------------

   function Get_Task_Main_Loop_Procedure (Node : Task_Body'Class) return String is
      Task_Name : constant String := Get_Task_Name (Node);
   begin
      return Task_Name & "_Body";
   end Get_Task_Main_Loop_Procedure;

   -------------
   -- Is_Loop --
   -------------

   function Is_Loop (Statement : Stmt'Class) return Boolean is
   begin
      return Kind (Statement) = Ada_Loop_Stmt;
   end Is_Loop;

   -----------------
   -- Is_Var_Decl --
   -----------------

--     function Is_Var_Decl (Node : Ada_Node) return Boolean is
--     begin
--        return Kind (Node) = Ada_Object_Decl;
--     end Is_Var_Decl;

   ------------------------
   -- Contains_Task_Body --
   ------------------------

   function Contains_Task_Body (Node : Ada_Node) return Boolean is
   begin
      return LAL_Extensions.Contains_Kind (Node, Ada_Task_Body);
   end Contains_Task_Body;

   ------------------------
   -- Contains_Task_Spec --
   ------------------------

   function Contains_Task_Spec (Node : Ada_Node) return Boolean is
   begin
      return LAL_Extensions.Contains_Kind (Node, Ada_Single_Task_Decl);
   end Contains_Task_Spec;


   ----------------------------------
   -- Process_Package_Dependencies --
   ----------------------------------
--     -- Write with dependecies associated with the compilation unit
--     procedure Process_Package_Dependencies (Root_Node : Ada_Node; Trace : Boolean := False) is
--        pragma Unreferenced (Trace);
--        Dependencies : constant Ada_Node_Array := LAL_Extensions.Find_All (Root_Node, Ada_With_Clause);
--     begin
--        for Dependency of Dependencies loop
--           M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Dependency));
--        end loop;
--
--        -- Add extra dependency
--        M2OS_IO.Write_Line ("with AdaX_Dispatching_Stack_Sharing;");
--
--     end Process_Package_Dependencies;

   -----------------------------------
   -- Process_Configuration_Pragmas --
   -----------------------------------

   procedure Process_Configuration_Pragmas (Root_Node : Ada_Node; Trace : Boolean := False) is
      pragma Unreferenced (Trace);
      -- Get the pragmas in the prelude part (with, use, pragma). Ignore other pragmas inside the package
      Prelude      : constant Ada_Node_List := Libadalang.Analysis.F_Prelude (Root_Node.As_Compilation_Unit);
      Dependencies : constant Ada_Node_Array := LAL_Extensions.Find_All (Prelude, Ada_Pragma_Node);
   begin
      for Dependency of Dependencies loop
         if (M2OS_Helper.Get_Node_As_String (F_Id(Dependency.As_Pragma_Node)) = "Task_Dispatching_Policy")
           or (M2OS_Helper.Get_Node_As_String (F_Id(Dependency.As_Pragma_Node)) = "Locking_Policy")
           or (M2OS_Helper.Get_Node_As_String (F_Id(Dependency.As_Pragma_Node)) = "Queuing_Policy")
           or (M2OS_Helper.Get_Node_As_String (F_Id(Dependency.As_Pragma_Node)) = "Ravenscar")
           or (M2OS_Helper.Get_Node_As_String (F_Id(Dependency.As_Pragma_Node)) = "Profile")
         then
            M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Dependency));
         end if;

      end loop;

   end Process_Configuration_Pragmas;

   -----------------------------
   -- Process_Package_Prelude --
   ----------------------------

   procedure Process_Package_Prelude (Root_Node : Ada_Node; Trace : Boolean := False) is
      pragma Unreferenced (Trace);
      -- Get the pragmas in the prelude part (with, use, pragma). Ignore other pragmas inside the package
      Prelude      : constant Ada_Node_List := Libadalang.Analysis.F_Prelude (Root_Node.As_Compilation_Unit);
   begin
      for Item of Prelude loop
         M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Item));
      end loop;

      -- Add extra dependency
      M2OS_IO.Write_Line ("with AdaX_Dispatching_Stack_Sharing;");

   end Process_Package_Prelude;

   ----------------------------
   -- Process_Task_Variables --
   ----------------------------

   procedure Process_Task_Variables (Node : Task_Body'Class; Task_Name : String) is
      Variables : constant Ada_Node_Array := LAL_Extensions.Find_All (F_Decls (Node), Ada_Object_Decl);
   begin
      for Variable of Variables loop
         -- Put_Line ("Processing var "& Get_Node_As_String (Variable));
         M2OS_Helper.Write_Global_Variable (Variable, Task_Name);
      end loop;
   end Process_Task_Variables;

   --------------------------
   -- Rename_Task_Variable --
   --------------------------

   procedure Rename_Variable (Variable : Object_Decl'Class; Prefix_Name : String) is
      Variable_Type : constant String :=
                        LAL_It.Find_First (Variable, LAL_It.Kind_Is (Ada_Subtype_Indication)).Debug_Text;
      Variable_Name : constant String :=  Variable.F_Ids.Debug_Text;
   begin
      -- Put_Line ("Renaming var "& Variable_Name);
      M2OS_Helper.Write_Rename_Variable (The_Name  => Variable_Name,
                                         The_Type  => Variable_Type,
                                         Task_Name => Prefix_Name);

   end Rename_Variable;

   ---------------------------
   -- Rename_Task_Variables --
   ---------------------------

   procedure Rename_Task_Variables (Node : Task_Body'Class) is
      Variables : constant Ada_Node_Array := LAL_Extensions.Find_All (F_Decls (Node), Ada_Object_Decl);
      Task_Name : constant String := Get_Task_Name (Node);
   begin
      for Variable of Variables loop
         Rename_Variable (Variable    => Variable.As_Object_Decl,
                          Prefix_Name => Task_Name);
      end loop;
   end Rename_Task_Variables;

   ----------------------------------
   -- Process_Task_Init_Operations --
   ----------------------------------

   function Process_Task_Init_Operations (Node : Task_Body'Class) return Natural is
      Statements     : constant Stmt_List := F_Stmts (Node).F_Stmts;
      Next_Statement : Stmt;
      Init_Procedure : constant String := Get_Task_Init_Procedure (Node);
   begin
      -- Write header for init procedure
      M2OS_Helper.Write_Procedure_Header (Init_Procedure);
      M2OS_Helper.Write_Procedure_Body_Header (Init_Procedure);
      Rename_Task_Variables (Node);
      M2OS_IO.Write_Line ("begin");
      M2OS_IO.Write_Indentation;
      -- Null is required if no init operations are provided, otherwise no damage is done
      M2OS_IO.Write_Line ("null;");

      for Index in 1..Statements.Children_Count loop
         Next_Statement := Statements.Child (Index).As_Stmt;
         if Is_Loop (Next_Statement) then
            -- The main loop is identified, so finish to process it
            -- Known limitation: No loop is allowed within init operations
            -- Write end for init procedure
            M2OS_IO.Remove_Indentation;
            M2OS_Helper.Write_End_Procedure_Body (Init_Procedure);
            M2OS_IO.New_Line;
            return Index;
         else
            M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Next_Statement.As_Ada_Node));
         end if;
      end loop;

      return 0;
   end Process_Task_Init_Operations;

   ----------------------------
   -- Process_Task_Main_Loop --
   ----------------------------

   procedure Process_Task_Main_Loop (Node : Task_Body'Class; Loop_Index: Natural) is
      Statements          : constant Stmt_List := F_Stmts (Node).F_Stmts;
      Loop_Statements     : Stmt_List;
      Loop_Statement      : Ada_Node;
      Main_Loop_Procedure : constant String := Get_Task_Main_Loop_Procedure (Node);
   begin
      -- Write header for main loop procedure
      M2OS_Helper.Write_Procedure_Header (Main_Loop_Procedure);
      M2OS_Helper.Write_Procedure_Body_Header (Main_Loop_Procedure);
      Rename_Task_Variables (Node);
      M2OS_IO.Write_Line ("begin");
      M2OS_IO.Write_Indentation;

      Loop_Statements := F_Stmts(Statements.Child (Loop_Index).As_Loop_Stmt);
      for Index in 1..Loop_Statements.Children_Count loop
         Loop_Statement := Loop_Statements.Child (Index);
         M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Loop_Statement));
      end loop;

      -- Write end for init procedure
      M2OS_IO.Remove_Indentation;
      M2OS_Helper.Write_End_Procedure_Body (Main_Loop_Procedure);

   end Process_Task_Main_Loop;

   -------------------------
   -- Get_Priority_Aspect --
   -------------------------

   function Get_Priority_Aspect (Task_Node : Ada_Node) return Natural is
      Aspects : constant Aspect_Spec := LAL_Extensions.Get_Aspects (Task_Node.As_Basic_Decl);
      Result  : Natural := 0;
   begin
      if not Aspects.Is_Null then
         declare
            Assocs : constant Aspect_Assoc_List := F_Aspect_Assocs (Aspects);
            Assoc  : Aspect_Assoc;
         begin
            for I in 1 .. Children_Count (Assocs) loop
               Assoc := LAL_Extensions.Childx (Assocs, I).As_Aspect_Assoc;
               if M2OS_Helper.Get_Node_As_String (Assoc.F_Id.As_Ada_Node) = "Priority" then
                  Result := Integer'Value (M2OS_Helper.Get_Node_As_String (Assoc.F_Expr.As_Ada_Node));
               end if;
            end loop;
         end;
      end if;
      return Result;
   end Get_Priority_Aspect;


   -----------------------
   -- Get_Task_Priority --
   -----------------------

   function Get_Task_Priority (Node : Task_Body'Class) return Natural is
      Ada_Unit      : constant Analysis_Unit := Unit (Node);
      Body_Filename : constant String := Get_Filename (Ada_Unit);
      Task_Name     : constant String := Get_Task_Name (Node);
      Tasks_Decl    : constant Ada_Node_Array := LAL_Extensions.Find_All (Ada_Unit.Root, Ada_Single_Task_Decl);
      Result        : Natural := 0;

      function Search_Priority (Tasks_Decl: Ada_Node_Array; Task_Name: String) return Natural is
         Result : Natural := 0;
      begin
         if Tasks_Decl'Length > 0 then
            for Task_Decl of Tasks_Decl loop
               if Task_Name = Get_Task_Name (Task_Decl.As_Single_Task_Decl) then
                  Result := Get_Priority_Aspect (Task_Decl);
                  return Result; -- Only one task name could match, so return the result
               end if;
            end loop;
         end if;
         return Result;
      end Search_Priority;

   begin
      -- Search task decl in this same unit
      Result := Search_Priority (Tasks_Decl, Task_Name);
      if Result /= 0 then
         return Result;
      end if;

      -- Search task decl in the corresponding spec
      declare
         Spec_Filename : constant String := M2OS_Utils.Corresponding_Spec (Body_Filename);
         Ada_Spec      : constant Analysis_Unit := Get_From_File (Context  => Context (Ada_Unit),
                                                                  Filename => Spec_Filename);
         Tasks_Decl    : constant Ada_Node_Array := LAL_Extensions.Find_All (Ada_Spec.Root, Ada_Single_Task_Decl);
      begin
         M2OS_Utils.Show_Diagnostics (Ada_Spec);
         Result := Search_Priority (Tasks_Decl, Task_Name);
      end;

      return Result;
   end Get_Task_Priority;

   -----------------------
   -- Process_Task_Body --
   -----------------------

   procedure Process_Task_Body (Node      : Task_Body'Class;
                                Trace     : Boolean := False) is
      pragma Unreferenced (Trace);
      Loop_Index         : Natural;
      Task_Name          : constant String := Get_Task_Name (Node);
      Task_Priority      : Natural := 0;

   begin
      Put_Line ("  - Processing task " & Task_Name);

      Process_Task_Variables (Node, Task_Name);
      Loop_Index := Process_Task_Init_Operations (Node);
      Process_Task_Main_Loop (Node       => Node,
                              Loop_Index => Loop_Index);

      --  Get priority aspect
      Task_Priority := Get_Task_Priority (Node);

      -- Last step: instantiate generic task
      M2OS_IO.New_Line;
      M2OS_IO.Write_Line (Task_Name & " : AdaX_Dispatching_Stack_Sharing.One_Shot_Task");
      M2OS_IO.Write_Indentation;
      --  Write args (including priority although its value is 0)
      M2OS_IO.Write_Line ("(" & Get_Task_Init_Procedure (Node) & "'Access, " &
                            Get_Task_Main_Loop_Procedure (Node) & "'Access, " &
                            Task_Priority'Img & ");");
      M2OS_IO.New_Line;
      M2OS_IO.Remove_Indentation;
   end Process_Task_Body;

   -----------------------------------
   -- Process_Package_Body_Contents --
   -----------------------------------

   procedure Process_Package_Body_Contents (Node   : Ada_Node;
                                            Trace  : Boolean := False) is
      pragma Unreferenced (Trace);
      Package_Content : constant Declarative_Part := LAL_Extensions.Body_Decls (Node);
      Content         : constant Ada_Node :=
                          LAL_It.Find_First (Package_Content, LAL_It.Kind_Is (Ada_Ada_Node_List));
   begin
      for Element of Content.Children loop
         -- Filter task body and task spec, as they will be handled after
         if not Contains_Task_Body (Element) and not Contains_Task_Spec (Element) then
            M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Element));
            M2OS_IO.New_Line;
         end if;
      end loop;
   end Process_Package_Body_Contents;

   --------------------------
   -- Process_Package_Body --
   --------------------------

   procedure Process_Package_Body (Root_Node   : Ada_Node;
                                   Trace       : Boolean := False) is
      pragma Unreferenced (Trace);
      Package_Node     : constant Ada_Node :=
                           LAL_It.Find_First (Root_Node, LAL_It.Kind_Is (Ada_Package_Body));
      Package_Element  : constant String :=  F_Package_Name (Package_Node.As_Package_Body).Debug_Text;
      Tasks_Nodes      : constant Ada_Node_Array := LAL_Extensions.Find_All (Node      => Root_Node,
                                                                             Node_Kind => Ada_Task_Body);
      The_File         : M2OS_IO.File_Type;
      Filename         : constant String := Get_Filename (Unit (Root_Node));

   begin

      --  There is a task, so we need to make a copy of file with updated code
      --  Rename local file and open new file
      if (not M2OS_IO.Backup_File (Filename)) then
         raise Storage_Error with "File cannot be backed up"; -- File not renamed
      end if;
      The_File := M2OS_IO.Create_File (Filename);

      --  Dependencies and pragmas: we need to process together as the order is important
      --  for pragmas with arguments (i.e., Elaborate_Body)
      Process_Package_Prelude (Root_Node);
      M2OS_IO.New_Line;

      M2OS_Helper.Write_Package_Body_Header (Package_Element);
      M2OS_IO.Write_Indentation;

      --  Body content which is not a task
      Process_Package_Body_Contents (Package_Node);

      for Node of Tasks_Nodes loop
         M2OS_Processing.Process_Task_Body (Node.As_Task_Body);
      end loop;

      M2OS_IO.Remove_Indentation;
      M2OS_IO.New_Line;
      M2OS_Helper.Write_End_Package_Header (Package_Element);
      M2OS_IO.Close_File (The_File);
   end Process_Package_Body;

   ----------------------
   -- Remove_Task_Decl --
   ----------------------

   procedure Remove_Task_Decl (Node_List: Ada_Node_List) is
      Id : LAL_RW.Node_Rewriting_Handle := LAL_RW.No_Node_Rewriting_Handle;
   begin
      for Index in Node_List.First_Child_Index..Node_List.Last_Child_Index loop
         if Kind (Node_List.Child(Index)) = Ada_Single_Task_Decl then
            Put_Line ("  - Processing task " &
                        Get_Task_Name (Node_List.Child(Index).As_Single_Task_Decl));
            Id := LAL_RW.Handle (Node_List);
            LAL_RW.Remove_Child (Handle => Id,
                                 Index  => Index);
         end if;
      end loop;
   end Remove_Task_Decl;

   ---------------------
   -- Write_Diff_Tree --
   ---------------------

   procedure Write_Diff_Tree (Diff : in out LAL_RW.Rewriting_Handle) is
      Units  : constant LAL_RW.Unit_Rewriting_Handle_Array := LAL_RW.Unit_Handles (Diff);
      Result : constant LAL_RW.Apply_Result := LAL_RW.Apply (Diff);
   begin
      if not Result.Success then
         raise Program_Error with ("Error during rewriting context...");
      end if;

      --  Go through all rewritten units and generate a new source file to
      --  contain the rewritten sources.
      for Unit_Handle of Units loop
         declare
            U            : constant Analysis_Unit := LAL_RW.Unit (Unit_Handle);
            Filename     : constant String := Get_Filename (U);
            Charset      : constant String := Get_Charset (U);

            --  Retreive rewritten text, and encode it using the same encoding as in the original file.
            Content_Text  : constant Langkit_Support.Text.Text_Type := Text (U);
            Content_Bytes : constant String := Langkit_Support.Text.Encode (Content_Text, Charset);

            The_File      : M2OS_IO.File_Type;
         begin

            if (not M2OS_IO.Backup_File (Filename)) then
               raise Storage_Error with "File cannot be backed up"; -- File not renamed
            end if;

            The_File := M2OS_IO.Create_File (Filename);
            M2OS_IO.Write_Line (Content_Bytes);
            M2OS_IO.Close_File (The_File);
         end;
      end loop;
   end Write_Diff_Tree;

   --------------------------
   -- Process_Package_Spec --
   --------------------------

   procedure Process_Package_Spec (Root_Node   : Ada_Node;
                                   Trace       : Boolean := False) is
      pragma Unreferenced (Trace);
      Unit_Context     : constant Analysis_Context := Context (Unit (Root_Node));
      Diff             : LAL_RW.Rewriting_Handle;
      Node_Lists       : constant Ada_Node_Array := LAL_Extensions.Find_All (Node      => Root_Node,
                                                                             Node_Kind => Ada_Ada_Node_List);

   begin
      -- Start a rewritting context, as this Node safely contains a task decl from main app
      Diff := LAL_RW.Start_Rewriting (Context => Unit_Context);

      --  Remove task decl, as it is not further needed in the M2OS one-shot task model
      for Node_List of Node_Lists loop
         Remove_Task_Decl (Node_List.As_Ada_Node_List);
      end loop;

      -- Apply changes and write results
      Write_Diff_Tree (Diff);
   end Process_Package_Spec;

   ------------------------------
   -- Process_Main_Contents --
   ------------------------------

   procedure Process_Main_Contents (Node   : Ada_Node;
                                    Trace  : Boolean := False) is
      pragma Unreferenced (Trace);
      Main_Content    : constant Declarative_Part := LAL_Extensions.Body_Decls (Node);
      Content         : constant Ada_Node :=
                          LAL_It.Find_First (Main_Content, LAL_It.Kind_Is (Ada_Ada_Node_List));
   begin
      for Element of Content.Children loop
         -- In the main procedure, all the data should be handled and written as is
         M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Element));
         M2OS_IO.New_Line;
      end loop;
   end Process_Main_Contents;

   ----------------------------------
   -- Process_Main_Init_Operations --
   ----------------------------------

   function Process_Main_Init_Operations (Node : Subp_Body'Class) return Natural is
      Statements     : constant Stmt_List := F_Stmts (Node).F_Stmts;
      Next_Statement : Stmt;
      Main_Proc_Name : constant String := Get_Proc_Name (Node);
      Init_Procedure : constant String := Main_Proc_Name&"_Init";
   begin
      -- Write header for init procedure
      M2OS_Helper.Write_Procedure_Header (Init_Procedure);
      M2OS_Helper.Write_Procedure_Body_Header (Init_Procedure);
      --Rename_Main_Variables (Node);
      M2OS_IO.Write_Line ("begin");
      M2OS_IO.Write_Indentation;

      for Index in 1..Statements.Children_Count loop
         Next_Statement := Statements.Child (Index).As_Stmt;
         if Is_Loop (Next_Statement) then
            -- The main loop is identified, so finish to process it
            -- Known limitation: No loop is allowed within init operations
            -- Write end for init procedure
            M2OS_IO.Remove_Indentation;
            M2OS_Helper.Write_End_Procedure_Body (Init_Procedure);
            M2OS_IO.New_Line;
            return Index;
         else
            M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Next_Statement.As_Ada_Node));
         end if;
      end loop;
      -- No loop detected, finish the procedure
      M2OS_IO.Remove_Indentation;
      M2OS_Helper.Write_End_Procedure_Body (Init_Procedure);
      M2OS_IO.New_Line;
      return 0;
   end Process_Main_Init_Operations;

   ----------------------------
   -- Process_Task_Main_Loop --
   ----------------------------

   procedure Process_Main_Task_Main_Loop (Node : Subp_Body'Class; Loop_Index: Natural) is
      Statements          : constant Stmt_List := F_Stmts (Node).F_Stmts;
      Loop_Statements     : Stmt_List;
      Loop_Statement      : Ada_Node;
      Main_Loop_Procedure : constant String := Get_Main_Task_Main_Loop_Procedure (Node);
   begin
      -- Write header for main loop procedure
      M2OS_Helper.Write_Procedure_Header (Main_Loop_Procedure);
      M2OS_Helper.Write_Procedure_Body_Header (Main_Loop_Procedure);
      --Rename_Main_Variables (Node);
      M2OS_IO.Write_Line ("begin");
      M2OS_IO.Write_Indentation;

      if (Loop_Index > 0) then
         Loop_Statements := F_Stmts(Statements.Child (Loop_Index).As_Loop_Stmt);
         for Index in 1..Loop_Statements.Children_Count loop
            Loop_Statement := Loop_Statements.Child (Index);
            M2OS_IO.Write_Line (M2OS_Helper.Get_Node_As_String (Loop_Statement));
         end loop;
      else
         M2OS_IO.Write_Line ("null;");
      end if;

      -- Write end for init procedure
      M2OS_IO.Remove_Indentation;
      M2OS_Helper.Write_End_Procedure_Body (Main_Loop_Procedure);

   end Process_Main_Task_Main_Loop;

   -----------------------
   -- Process_Main_Body --
   -----------------------

   procedure Process_Main_Body (Node      : Subp_Body'Class;
                                Trace     : Boolean := False) is
      pragma Unreferenced (Trace);
      Loop_Index         : Natural;
      Main_Proc_Name     : constant String := Get_Proc_Name (Node);
      Main_Task_Priority : Natural := 0;

   begin

      Put_Line ("  - Processing main procedure "& Main_Proc_Name);
      Process_Main_Contents (Node.As_Ada_Node);
      --Process_Proc_Variables (Node, Main_Proc_Name);

      --  Separate init and main loop
      Loop_Index := Process_Main_Init_Operations (Node);
      Process_Main_Task_Main_Loop (Node       => Node,
                                   Loop_Index => Loop_Index);

      --  Get priority aspect
      Main_Task_Priority := Get_Priority_Aspect (Node.As_Ada_Node);

      -- Last step: instantiate generic task
      M2OS_IO.New_Line;
      M2OS_IO.Write_Line ("Task_"&Main_Proc_Name & " : AdaX_Dispatching_Stack_Sharing.One_Shot_Task");
      M2OS_IO.Write_Indentation;
      --  Write args (including priority although its value is 0)
      M2OS_IO.Write_Line ("(" & Get_Main_Task_Init_Procedure (Node) & "'Access, " &
                            Get_Main_Task_Main_Loop_Procedure (Node) & "'Access, " &
                            Main_Task_Priority'Img & ");");
      M2OS_IO.New_Line;
      M2OS_IO.Remove_Indentation;
   end Process_Main_Body;

   ----------------------------
   -- Process_Main_Procedure --
   ----------------------------

   procedure Process_Main_Procedure (Root_Node   : Ada_Node;
                                     Trace       : Boolean := False) is
      pragma Unreferenced (Trace);
      Subprogram_Node     : constant Ada_Node :=
                              LAL_It.Find_First (Root_Node, LAL_It.Kind_Is (Ada_Subp_Body));
      Proc_Element           : constant String :=  Get_Proc_Name (Subprogram_Node.As_Subp_Body);--F_Package_Name (Subprogram_Node.As_Subp_Body).Debug_Text;
      Secondary_Proc_Element : constant String :=  "Main_Task_"&Proc_Element;

      --        Loop_Nodes      : constant Ada_Node_Array := LAL_Extensions.Find_All (Node      => Root_Node,
      --                                                                              Node_Kind => Ada_Loop_Stmt);
      The_File            : M2OS_IO.File_Type;
      Filename               : constant String := Get_Filename (Unit (Root_Node));
      --  Filename is absolute path so to easily modify the name convert to relative path
      Secondary_Filename     : constant String := "main_task_"&Ada.Directories.Simple_Name (Filename);
      Secondary_Filename_Spec: constant String := GNATCOLL.Utils.Replace(S           => Secondary_Filename,
                                                                         Pattern     => ".adb",
                                                                         Replacement => ".ads");

      --        Main_Loop       : constant Ada_Node := Loop_Nodes(Loop_Nodes'Last);

   begin

      --  There is a task, so we need to make a copy of file with updated code
      --  Rename local file and open new file
      if (not M2OS_IO.Backup_File (Filename)) then
         raise Storage_Error with "File cannot be backed up"; -- File not renamed
      end if;
      -- The main file is converted to three different files:
      -- A) A new main file to redirect calls. It also includes original pragmas
      The_File := M2OS_IO.Create_File (Filename);
      --  Retrive configuration pragmas
      Process_Configuration_Pragmas (Root_Node);
      M2OS_IO.New_Line;
      -- Dependency to new package
      M2OS_IO.Write_Line ("with "&Secondary_Proc_Element&";");
      M2OS_Helper.Write_Procedure_Body_Header (Name => Proc_Element);
      M2OS_IO.Write_Line ("begin");
      M2OS_IO.Write_Indentation;
      -- Main Task is now launched with withed package, so null is enough
      M2OS_IO.Write_Line ("null;");
      M2OS_IO.Remove_Indentation;
      M2OS_Helper.Write_End_Procedure_Body (Proc_Element);
      M2OS_IO.Close_File (The_File);

      -- B) A new .adb file with the main task and their operations
      Put_Line ("  Creating "&Secondary_Filename);
      The_File := M2OS_IO.Create_File (Secondary_Filename);
      --  Dependencies
      Process_Package_Prelude (Root_Node);
      M2OS_IO.New_Line;

      M2OS_Helper.Write_Package_Body_Header (Secondary_Proc_Element);
      M2OS_IO.Write_Indentation;

      M2OS_Processing.Process_Main_Body (Subprogram_Node.As_Subp_Body);

      M2OS_IO.Remove_Indentation;
      M2OS_IO.New_Line;
      M2OS_Helper.Write_End_Package_Header (Secondary_Proc_Element);
      M2OS_IO.Close_File (The_File);

      -- C) a new .ads file to complete the previous adb file
      Put_Line ("  Creating "&Secondary_Filename_Spec);
      The_File := M2OS_IO.Create_File (Secondary_Filename_Spec);

      M2OS_Helper.Write_Package_Spec_Header (Secondary_Proc_Element);
      M2OS_IO.Write_Indentation;
      M2OS_IO.New_Line;
      -- Add pragma
      M2OS_IO.Write_Line("pragma Elaborate_Body;");
      M2OS_IO.Remove_Indentation;
      M2OS_IO.New_Line;
      M2OS_Helper.Write_End_Package_Header (Secondary_Proc_Element);
      M2OS_IO.Close_File (The_File);

   end Process_Main_Procedure;

end M2os_Processing;
