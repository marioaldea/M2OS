----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on motor_and_nxttouch.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- setup for this example:
-- attach external power to EVShield.
-- attach a touch sensor to BAS2
-- attach motors to motor ports (any number of motors is fine).
----------------------------------------------------------------------------

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Arduino.EVShield.Motors; use Arduino.EVShield.Motors;
with Arduino.EVShield.EV3Touch;
with SHDefines_h; use SHDefines_h;
with Interfaces.C.Extensions;


procedure evshield_motor_and_nxttouch is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, Interfaces.C.Extensions.bool;

   -- Program variables
   time : Arduino.Time_MS;
   touch : Interfaces.C.Extensions.bool;
   brake : Interfaces.C.Extensions.bool;
   count : Interfaces.C.int;
   aux   : Interfaces.C.Extensions.bool;

   package Touch_sensor is new Arduino.EVShield.EV3Touch.Touch (SHDefines_h.SH_BAS1);
   package BAM1 is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM1);

begin

   DIO.Put_Line ("--------------------------------------");
   DIO.Put_Line ("Initializing the devices ...");

   DIO.Put_Line ("setup done");

   -- Wait until user presses GO button to continue the program
   while not Arduino.EVShield.Get_Button_State(SHDefines_h.BTN_GO) loop
      time := Arduino.Millis;
      if time mod 1000 < 3 then
         DIO.Put_Line (" Press GO button to continue");
      end if;
   end loop;

   -- reset motors.
   aux := BAM1.Motor_Reset;

   loop
      -- get the reading(s) form sensor
      touch := Touch_sensor.Is_Pressed ;
      count := Touch_sensor.Get_Bump_Count;

      if touch then
         DIO.Put_Line ("Button is pressed!");
      end if;

      -- print the sensor value(s)
      if touch then
         DIO.Put_Line ("run unlimited");
         BAM1.Motor_Run_Unlimited(BAM1.SH_Direction_Forward, 50);
      elsif not touch then
         DIO.Put_Line ( "stop (float)");
         brake := BAM1.Motor_Stop(BAM1.SH_Next_Action_Float);
      end if;

      -- wait for some time
      Arduino.C_Delay (1000);

   end loop;
end evshield_motor_and_nxttouch;
