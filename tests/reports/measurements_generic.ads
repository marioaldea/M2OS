
with Tests_Reports;
with GNAT.Source_Info;

generic
   type Time_T is mod <>;
   type Time_Long_T is mod <>;
   with function Get_Time return Time_T;
   Upward : Boolean := True; -- Time counts upward or downward
   type Time_Units_T is mod <>;
   Time_Unit_Char : Character;   
   Time_Unit_T_Hz : Time_Units_T; -- time units per second
   Time_T_Hz : Time_Units_T; -- time ticks per second
   
package Measurements_Generic is
   subtype Time is Time_T;
   subtype Time_Long is Time_Long_T;
   subtype Time_Units is Time_Units_T;
   
   Time_Hz : constant Time_Units := Time_T_Hz;
   Time_Unit_Hz : constant Time_Units := Time_Unit_T_Hz;
   Time_Unit_Symbol : constant Character := Time_Unit_Char;
 
   procedure Init;
   
   procedure Reset;
   
   procedure Start_Measurement;
   
   procedure End_Measurement;
   
   function Measurement_Time return Time_Long;
   
   function Measurement_Avg return Time;
   
   function Measurement_Avg_In_Time_Units return Time_Units;
   
   function To_Time_Units (T : Time_Long) return Time_Units;
   
   procedure Assert
     (Condition : Boolean;
      Msg       : String := GNAT.Source_Info.Source_Location)
     renames Tests_Reports.Assert;
   
   procedure Assert_Equal
     (Val1 : Integer; Val2 : Integer; Margin : Integer;
      Msg       : String := GNAT.Source_Info.Source_Location)
     renames Tests_Reports.Assert_Equal;
   
   procedure Print_Measures_Data;
   
   procedure Finish;  
   
end Measurements_Generic;
