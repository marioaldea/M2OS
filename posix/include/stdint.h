#ifndef	_M2_STDINT_H_
#define _M2_STDINT_H_

// Signed

typedef signed char		int8_t;

#if __SIZEOF_POINTER__ == 2
typedef int		int16_t;
typedef long int	int32_t;
typedef long long int	int64_t;
#elif __SIZEOF_POINTER__ == 4
typedef short		int16_t;
typedef int	        int32_t;
typedef long long int	int64_t;
#elif __SIZEOF_POINTER__ == 8
typedef short		int16_t;
typedef int	        int32_t;
typedef long long int	int64_t;
#else
# error Unexpected architecture
#endif

// Unsigned
typedef unsigned char		uint8_t;


#if __SIZEOF_POINTER__ == 2
typedef unsigned int		uint16_t;
typedef unsigned long int	uint32_t;
typedef unsigned long long int	uint64_t;
#elif __SIZEOF_POINTER__ == 4
typedef unsigned short		uint16_t;
typedef unsigned int    	uint32_t;
typedef unsigned long long int	uint64_t;
#elif __SIZEOF_POINTER__ == 8
typedef unsigned short		uint16_t;
typedef unsigned int    	uint32_t;
typedef unsigned long long int  uint64_t;
#else
# error Unexpected architecture
#endif

#endif // _M2_STDINT_H_
