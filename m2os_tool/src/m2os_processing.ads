--  This package contains routines for high-level processing of
--  (terating through) loop code

with Libadalang.Analysis;  use Libadalang.Analysis;

package M2os_Processing is

   procedure Process_Package_Body (Root_Node   : Ada_Node;
                                   Trace       : Boolean := False);
   --  This procedure iterates through the whole content of its argument
   --  Ada_Node. If Trace parameter is set ON, it generate the
   --  simple trace of the element processing

   procedure Process_Package_Spec (Root_Node   : Ada_Node;
                                   Trace       : Boolean := False);
   --  This procedure iterates through the whole content of its argument
   --  Ada_Node. If Trace parameter is set ON, it generate the
   --  simple trace of the element processing

   procedure Process_Task_Body (Node  : Task_Body'Class;
                                Trace : Boolean := False);
   --  This procedure iterates through the whole content of its argument
   --  Node. If Trace parameter is set ON, it generate the
   --  simple trace of the element processing

   procedure Process_Main_Procedure (Root_Node   : Ada_Node;
                                     Trace       : Boolean := False);
   --  This procedure iterates through the whole content of its argument
   --  Ada_Node. If Trace parameter is set ON, it generate the
   --  simple trace of the element processing

end M2os_Processing;
