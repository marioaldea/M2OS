----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Direct_IO;
with M2.Kernel.API;
with Interfaces.C;

package body Tests_Reports is

   use type Interfaces.C.int;

   ------------
   -- Assert --
   ------------

   procedure Assert
     (Condition : Boolean;
      Msg       : String := GNAT.Source_Info.Source_Location) is
   begin
      if not Condition then
         M2.Direct_IO.Put ("ASSERT FAILED ");
         M2.Direct_IO.Put (Msg);
         M2.Direct_IO.New_Line;

         M2.Kernel.API.End_Execution (-1);
      end if;
   end Assert;

   ------------------
   -- Assert_Equal --
   ------------------

   procedure Assert_Equal
     (Val1 : Integer; Val2 : Integer; Margin : Integer;
      Msg       : String := GNAT.Source_Info.Source_Location) is
   begin
      if (Val1 > Val2 and then Val1 - Val2 > Margin) or
        (Val2 > Val1 and then Val2 - Val1 > Margin)
      then
         M2.Direct_IO.Put ("ASSERT FAILED ");
         M2.Direct_IO.Put (Msg);
         M2.Direct_IO.New_Line;

         M2.Kernel.API.End_Execution (-1);
      end if;
   end Assert_Equal;

   -------------
   -- Test_OK --
   -------------

   procedure Test_OK is
   begin
      M2.Direct_IO.Put ("Test OK");
      M2.Direct_IO.New_Line;

      M2.Kernel.API.End_Execution (0);
   end Test_OK;

   ---------------
   -- Known_Bug --
   ---------------

   procedure Known_Bug is
   begin
      M2.Direct_IO.Put ("Known Bug");
      M2.Direct_IO.New_Line;
   end Known_Bug;

end Tests_Reports;
