----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  ARM version of this package

pragma Restrictions (No_Elaboration_Code);

with System;
with Interfaces;
with Ada.Unchecked_Conversion;
with System.Storage_Elements;

package M2.HAL is
   --  pragma Pure;
   pragma Preelaborate;

   type Code_Address is new System.Address;
   type Stack_Address is new System.Storage_Elements.Integer_Address;
   subtype Stack_Size is System.Storage_Elements.Integer_Address;
   pragma Compile_Time_Error (Stack_Size'Size /= 32, "Stack_Size'Size /= 32");
   pragma Compile_Time_Error (Stack_Size'Size /= System.Address'Size,
                             "Stack_Size'Size /= System.Address'Size");

   Events_Order_Counter : Interfaces.Unsigned_8 := 0;
   --  To keep FIFO order when threads have equal priority and Activation_Time.
   --  Set to 0 in the timer handler.

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch (New_Addr  : Code_Address;
                             New_Stack : Stack_Address);

   --------------------------
   -- Push_Function_Status --
   --------------------------

   procedure Push_Function_Status;
   pragma Import (Asm, Push_Function_Status, "m2_hal_regs_push_status");
   --  defined in m2_hal_regs.S

   -------------------------
   -- Pop_Function_Status --
   -------------------------

   procedure Pop_Function_Status;
   pragma Import (Asm, Pop_Function_Status, "m2_hal_regs_pop_status");
   --  defined in m2_hal_regs.S

   -----------------------
   -- Current_Stack_Top --
   -----------------------

   function Current_Stack_Top return Stack_Address;
   pragma Import (Asm, Current_Stack_Top, "m2_hal_regs_get_sp_reg");
   --  From m2_hal_regs.S

   -----------------------
   -- Global_Stack_Base --
   -----------------------

   function Global_Stack_Base return Stack_Address with Inline_Always;

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Stack_Size;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Stack_Size;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Stack_Size;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Stack_Size;

   -------------------------------
   -- Enable/disable interrupts --
   -------------------------------

   procedure Enable_Interrupts;
   --  pragma Inline_Always (Enable_Interrupts);

   procedure Disable_Interrupts;
   --  pragma Inline_Always (Disable_Interrupts);

   function Are_Interrupts_Enabled return Boolean;

   ------------
   -- HWTime --
   ------------

   HWTime_Hz : constant := 10_000;
   --  Number of HWTime ticks per second.
   --  The duration of a HWTime tick is 1/HWTime_Hz = 0.000_1s = 100us
   --  Should have the same value than OSI.Ticks_Per_Second

   type HWTime is new Interfaces.Unsigned_64;

   function Get_HWTime return HWTime;

   ---------------
   -- HiResTime --
   ---------------

   HiResTime_Hz : constant := 168_000_000;
   --  Number of HiResTime ticks per second.
   --  The duration of a HiResTime tick is 1/HWTime_Hz = 6ns

   HiResTime_Max_Value : constant := HiResTime_Hz / HWTime_Hz - 1;
   -- Maximum value of HiResTime = 16799 (100us)

   type HiResTime is mod HiResTime_Max_Value + 1; -- mod 16_800
   for HiResTime'Size use Interfaces.Unsigned_32'Size;
   --  HiResTime is the current value of the SysTick Timer.

   function Get_HiResTime return HiResTime
     with Import => True, Convention => C, External_Name => "get_systick_val";
   --  Reads the VAL register of the "SysTick Timer".
   --  Function get_systick_val() defined in 'm2_hal_regs_cmsis.c'.

   ------------------------
   -- OS_Tick_Handler_Ac --
   ------------------------

   type OS_Tick_Handler_Ac is access procedure (Now : HWTime);

   --------------------
   -- Initialization --
   --------------------

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac);

end M2.HAL;
