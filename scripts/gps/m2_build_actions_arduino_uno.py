import GPS
import os
import os.path
import sys


def on_gps_started(hook_name):
    build_targets = GPS.Project.get_attribute_as_list(GPS.Project.root(),
                                                      "main")
    # Redefine the build actions for each build target.
    # They are named "Build Main Number 1", "Build Main Number 2", ... One for
    # each main file in the project.
    i = 1
    for target in build_targets:
        action_xml = (
            "<action name='Build Main Number " + str(i) +
            "' category='Build'> " +
            "<shell lang='python' output='Messages'>" +
            "m2_build_actions_arduino_uno.build_target(" + str(i-1) +
            ")</shell>  </action>")
        print "action_xml:", action_xml
        GPS.parse_xml(action_xml)
        i = i + 1


GPS.Hook("gps_started").add(on_gps_started)
GPS.Hook("project_changed").add(on_gps_started)


def get_tool_mode():
    """ Return [No_Use_Tool, Use_Tool] """
    tool_options = ["No_Use_Tool", "Use_Tool"]

    print "GPS.Project.scenario_variables():", GPS.Project.scenario_variables()
    if (not GPS.Project.scenario_variables() or
            ("use_tool" not in GPS.Project.scenario_variables().keys())):
            raise AssertionError(
                "Error 'use_tool' scenario variable not set\n" +
                "   BUILD ABORTED")

    use_code_processing_tool = GPS.Project.scenario_variables()["use_tool"]
    if use_code_processing_tool not in tool_options:
            raise AssertionError(
                "Error 'use_tool' scenario variable should have value:\n" +
                str(tool_options))

    return use_code_processing_tool


def exec_command(command, console_name):
    f = os.popen(command)
    console = GPS.Console(console_name)
    for l in f.readlines():
        console.write(l)


def build_target(target_num):
    target = GPS.Project.get_attribute_as_list(GPS.Project.root(),
                                               "main")[target_num]
    if get_tool_mode() == "Use_Tool":
        # Execute code processing tool
        print "Running the code processing tool on", target
        m2_dir = GPS.Project("global_switches").get_attribute_as_string(
            "Project_Dir")
        # Old code used for ASIS based tool
        # sources_spc_sep = '-I' + ' -I'.join(
        #     GPS.Project.root().get_attribute_as_list("Source_Dirs"))
        # make_command = (
        #     'make -f ' + m2_dir + 'examples/Makefile' +
        #     ' APP=' + target +
        #     ' EXTRA_GNAT_INC="' + sources_spc_sep + '"' +
        #     ' recovery tree_files asis 2>&1')
        os.popen("rename -f 's/\.orig$//' *.orig 2>/dev/null")
        make_command = ('cd ' +
                        GPS.Project.root().get_attribute_as_string(
                            "Project_Dir") + ' && ' +
                        m2_dir + 'm2os_tool/bin/ada_tasks_to_m2 ' +
                        GPS.Project.root().get_attribute_as_string("Name") +
                        ".gpr")
        print make_command
        f = os.popen(make_command)
        console = GPS.Console("Code transf. tool")
        console.write('\n--- Running Code transformation tool on ' +
                      target + '\n')
        console.write(make_command + '\n')
        for l in f.readlines():
            console.write('   ' + l)

    # Build the target
    build_main_target = GPS.BuildTarget("Build Main")
    build_main_target.execute(target)

    # Show size
    exec_command("avr-size " + os.path.splitext(target)[0],
                 "avr-size " + os.path.splitext(target)[0])
