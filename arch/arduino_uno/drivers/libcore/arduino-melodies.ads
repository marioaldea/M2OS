----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  The above copyright notice does not apply to some of the melodies
--  which have the appropriate copyright notice beside their definition.
----------------------------------------------------------------------------
with Interfaces;

package Arduino.Melodies is

   type Note is Record
      Frequency_Hz : Frequency_In_Hz;

      Rel_Duration  : Interfaces.Unsigned_8;
      --  Note duration relative to the Tempo: Duration := Tempo/Rel_Duration
      --  1 = redonda, 2 = blanca, 4 = negra, 8 = corchea, ...
   end record;

   type Melody_Notes is array (Interfaces.Unsigned_8 range <>) of Note;

   ------------------------------
   -- Tempos (in milliseconds) --
   ------------------------------

   --  60_000 / beats per minute

   Adagio  : constant := 60_000 / 66;
   Andante : constant := 60_000 / 76;
   Moderato : constant := 60_000 / 100;

   ------------------------------------
   -- Musical Note's frequency in Hz --
   ------------------------------------

   --  Taken from http://www.arduino.cc/en/Tutorial/Tone

   Note_Rest : constant := 1;
   Note_B0 : constant := 31;
   Note_C1 : constant := 33;
   Note_CS1 : constant := 35;
   Note_D1 : constant := 37;
   Note_DS1 : constant :=39;
   Note_E1 : constant := 41;
   Note_F1 : constant := 44;
   Note_FS1 : constant := 46;
   Note_G1 : constant := 49;
   Note_GS1 : constant := 52;
   Note_A1 : constant := 55;
   Note_AS1 : constant := 58;
   Note_B1 : constant := 62;
   Note_C2 : constant := 65;
   Note_CS2 : constant := 69;
   Note_D2 : constant := 73;
   Note_DS2 : constant :=78;
   Note_E2 : constant := 82;
   Note_F2 : constant := 87;
   Note_FS2 : constant :=93;
   Note_G2 : constant := 98;
   Note_GS2 : constant :=104;
   Note_A2 : constant := 110;
   Note_AS2 : constant :=117;
   Note_B2 : constant := 123;
   Note_C3 : constant := 131;
   Note_CS3 : constant :=139;
   Note_D3 : constant := 147;
   Note_DS3 : constant :=156;
   Note_E3 : constant := 165;
   Note_F3 : constant := 175;
   Note_FS3 : constant :=185;
   Note_G3 : constant := 196;
   Note_GS3 : constant :=208;
   Note_A3 : constant := 220;
   Note_AS3 : constant :=233;
   Note_B3 : constant := 247;
   Note_C4 : constant := 262;  -- Do
   Note_CS4 : constant :=277;
   Note_D4 : constant := 294;  -- Re
   Note_DS4 : constant :=311;
   Note_E4 : constant := 330;  -- Mi
   Note_F4 : constant := 349;  -- Fa
   Note_FS4 : constant :=370;
   Note_G4 : constant := 392;  -- Sol
   Note_GS4 : constant :=415;
   Note_A4 : constant := 440;  -- La
   Note_AS4 : constant :=466;
   Note_B4 : constant := 494;  -- Si
   Note_C5 : constant := 523;  --  Do
   Note_CS5 : constant :=554;
   Note_D5 : constant := 587;
   Note_DS5 : constant :=622;
   Note_E5 : constant := 659;
   Note_F5 : constant := 698;
   Note_FS5 : constant :=740;
   Note_G5 : constant := 784;
   Note_GS5 : constant :=831;
   Note_A5 : constant := 880;
   Note_AS5 : constant :=932;
   Note_B5 : constant := 988;
   Note_C6 : constant := 1047;
   Note_CS6 : constant :=1109;
   Note_D6 : constant := 1175;
   Note_DS6 : constant :=1245;
   Note_E6 : constant := 1319;
   Note_F6 : constant := 1397;
   Note_FS6 : constant :=1480;
   Note_G6 : constant := 1568;
   Note_GS6 : constant :=1661;
   Note_A6 : constant := 1760;
   Note_AS6 : constant :=1865;
   Note_B6 : constant := 1976;
   Note_C7: constant := 2093;
   Note_CS7 : constant :=2217;
   Note_D7 : constant := 2349;
   Note_DS7 : constant :=2489;
   Note_E7 : constant := 2637;
   Note_F7 : constant := 2794;
   Note_FS7 : constant :=2960;
   Note_G7 : constant := 3136;
   Note_GS7 : constant :=3322;
   Note_A7 : constant := 3520;
   Note_AS7 : constant :=3729;
   Note_B7 : constant := 3951;
   Note_C8 : constant := 4186;
   Note_CS8 : constant := 4435;
   Note_D8 : constant := 4699;
   Note_DS8 : constant := 4978;

   Note_Do_4 : constant := Note_C4;   -- Do
   Note_Re_4 : constant := Note_D4;   -- Re
   Note_Mi_4 : constant := Note_E4;   -- Mi
   Note_Fa_4 : constant := Note_F4;   -- Fa
   Note_Sol_4 : constant := Note_G4;  -- Sol
   Note_La_4 : constant := Note_A4;   -- La
   Note_Sib_4 : constant := Note_AS4; -- Si bemol
   Note_Si_4 : constant := Note_B4;   -- Si
   Note_Do_5 : constant := Note_C5;   --  DO
   Note_Re_5 : constant := Note_D4;   -- RE
   Note_Mi_5 : constant := Note_E5;   -- MI
   Note_Fa_5 : constant := Note_F5;   -- FA
   Note_Sol_5 : constant := Note_G5;  -- SOL
   Note_La_5 : constant := Note_A5;   -- LA
   Note_Si_5 : constant := Note_B5;   -- SI

   ------------------------
   -- Beep_Frecquency_Hz --
   ------------------------

   Beep_Frecquency_Hz : constant := Note_A4;

   -----------
   -- Scale --
   -----------

   Scale : aliased constant Melody_Notes :=
     ((Note_C4, 1),
      (Note_D4, 1),
      (Note_E4, 1),
      (Note_F4, 1),
      (Note_G4, 1),
      (Note_A4, 1),
      (Note_B4, 1),
      (Note_C5, 1));

   -------------
   -- Melody1 --
   -------------

   --  by Tom Igoe
   --  Taken from http://www.arduino.cc/en/Tutorial/Tone

   Melody1 : aliased constant Melody_Notes :=
     ((Note_C4, 1),
      (Note_G3, 2),
      (Note_G3, 2),
      (Note_A3, 1),
      (Note_G3, 1),
      (Note_Rest, 1),
      (Note_B3, 1),
      (Note_C4, 1),
      (Note_Rest, 1));

   -------------
   -- Melody2 --
   -------------

   --  (cleft) 2005 D. Cuartielles for K3
   --  Refactoring and comments 2006 clay.shirky@nyu.edu
   --  Taken from https://www.arduino.cc/en/Tutorial/PlayMelody

   Melody2 : aliased constant Melody_Notes :=
     ((Note_C5, 2),
      (Note_B4, 2),
      (Note_G4, 2),
      (Note_C5, 1),
      (Note_B4, 1),
      (Note_E4, 2),
      (Note_Rest, 4),

      (Note_C5, 2),
      (Note_C4, 2),
      (Note_G4, 2),
      (Note_A4, 1),
      (Note_C5, 1),
      (Note_Rest, 1));

   --------------------
   -- Happy_Birthday --
   --------------------

   Happy_Birthday : aliased constant Melody_Notes :=
     ((Note_Do_4, 2),
      (Note_Do_4, 2),
      (Note_Re_4, 2),
      (Note_Do_4, 2),
      (Note_Fa_4, 2),
      (Note_Mi_4, 1),
      (Note_Rest, 2),

      (Note_Do_4, 2),
      (Note_Do_4, 2),
      (Note_Re_4, 2),
      (Note_Do_4, 2),
      (Note_Sol_4, 2),
      (Note_Fa_4, 2),
      (Note_Rest, 2),

      (Note_Do_4, 2),
      (Note_Do_4, 2),
      (Note_Do_5, 2),
      (Note_La_4, 2),
      (Note_Fa_4, 1),
      (Note_Mi_4, 2),
      (Note_Re_4, 2),
      (Note_Rest, 4),

      (Note_Sib_4, 2),
      (Note_Sib_4, 2),
      (Note_La_4, 2),
      (Note_Fa_4, 1),
      (Note_Sol_4, 2),
      (Note_Fa_4, 2),
      (Note_Rest, 2),

      (Note_Do_4, 2),
      (Note_Do_4, 2),
      (Note_Re_4, 2),
      (Note_Do_4, 2),
      (Note_Fa_4, 2),
      (Note_Mi_4, 1),
      (Note_Rest, 2),

      (Note_Do_4, 2),
      (Note_Do_4, 2),
      (Note_Re_4, 2),
      (Note_Do_4, 2),
      (Note_Sol_4, 2),
      (Note_Fa_4, 2),
      (Note_Rest, 2),

      (Note_Do_4, 2),
      (Note_Do_4, 2),
      (Note_Do_5, 2),
      (Note_La_4, 2),
      (Note_Fa_4, 1),
      (Note_Mi_4, 2),
      (Note_Re_4, 2),
      (Note_Sib_4, 2),
      (Note_Sib_4, 2),
      (Note_La_4, 2),
      (Note_Fa_4, 1),
      (Note_Sol_4, 2),
      (Note_Fa_4, 1),
      (Note_Rest, 2));

   ---------------------------------
   -- Twinkle_Twinkle_Little_Star --
   ---------------------------------

   Twinkle_Twinkle_Little_Star : aliased constant Melody_Notes :=
     ((Note_Do_4, 2),
      (Note_Do_4, 2),
      (Note_Sol_4, 2),
      (Note_Sol_4, 2),
      (Note_La_4, 2),
      (Note_La_4, 2),
      (Note_Sol_4, 1),
      (Note_Rest, 4),

      (Note_Fa_4, 2),
      (Note_Fa_4, 2),
      (Note_Mi_4, 2),
      (Note_Mi_4, 2),
      (Note_Re_4, 2),
      (Note_Re_4, 2),
      (Note_Do_4, 1),
      (Note_Rest, 4),

      (Note_Sol_4, 2),
      (Note_Sol_4, 2),
      (Note_Fa_4, 2),
      (Note_Fa_4, 2),
      (Note_Mi_4, 2),
      (Note_Mi_4, 2),
      (Note_Re_4, 1),
      (Note_Rest, 4),

      (Note_Sol_4, 2),
      (Note_Sol_4, 2),
      (Note_Fa_4, 2),
      (Note_Fa_4, 2),
      (Note_Mi_4, 2),
      (Note_Mi_4, 2),
      (Note_Re_4, 1),
      (Note_Rest, 4),

      (Note_Do_4, 2),
      (Note_Do_4, 2),
      (Note_Sol_4, 2),
      (Note_Sol_4, 2),
      (Note_La_4, 2),
      (Note_La_4, 2),
      (Note_Sol_4, 1),
      (Note_Rest, 2),

      (Note_Fa_4, 2),
      (Note_Fa_4, 2),
      (Note_Mi_4, 2),
      (Note_Mi_4, 2),
      (Note_Re_4, 2),
      (Note_Re_4, 2),
      (Note_Do_4, 1),
      (Note_Rest, 4));

   ----------------
   -- Bella Ciao --
   ----------------

   Bella_Ciao : aliased constant Melody_Notes :=
     ((Note_Mi_4, 2),
      (Note_La_4, 2),
      (Note_Si_4, 2),
      (Note_Do_5, 2),
      (Note_La_4, 1),
      (Note_Rest, 4),
      (Note_Mi_4, 2),
      (Note_La_4, 2),
      (Note_Si_4, 2),
      (Note_Do_5, 2),
      (Note_La_4, 1),
      (Note_Rest, 4),

      (Note_Mi_4, 2),
      (Note_La_4, 2),
      (Note_Si_4, 2),
      (Note_Do_5, 2),
      (Note_Rest, 4),
      (Note_Si_4, 2),
      (Note_La_4, 2),
      (Note_Do_5, 2),
      (Note_Rest, 4),
      (Note_Si_4, 2),
      (Note_La_4, 2),
      (Note_Mi_5, 1),
      (Note_Rest, 32),
      (Note_Mi_5, 1),
      (Note_Rest, 32),
      (Note_Mi_5, 2),
      (Note_Rest, 32),

      (Note_Mi_5, 2),
      (Note_Re_5, 2),
      (Note_Mi_5, 2),
      (Note_Fa_5, 2),
      (Note_Fa_5, 1),
      (Note_Rest, 4),
      (Note_Fa_5, 2),
      (Note_Mi_5, 2),
      (Note_Re_5, 2),
      (Note_Fa_5, 2),
      (Note_Mi_5, 1),
      (Note_Rest, 4),

      (Note_Mi_5, 2),
      (Note_Re_5, 2),
      (Note_Do_5, 2),
      (Note_Si_4, 1),
      (Note_Mi_5, 1),
      (Note_Do_5, 1),
      (Note_Si_4, 1),
      (Note_La_4, 1),
      (Note_Rest, 4)
     );

end Arduino.Melodies;
