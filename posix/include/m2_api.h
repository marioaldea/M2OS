//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// M2OS POSIX-like layer.
// The M2OS POSIX-like layer is in a preliminary stage.
#ifndef	_M2_API_H_
#define _M2_API_H_

#include <stddef_m2.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/*
 * TCBs, pthread_t and suspension_t
 */
#if defined(M2_TARGET_Arduino_Uno)
#  define TCB_SIZE 22
#  define SUSP_OBJ_SIZE 3
#elif defined(M2_TARGET_stm32f4) || defined(M2_TARGET_microbit) || defined(M2_TARGET_riscv)
#  define TCB_SIZE 48
#  define SUSP_OBJ_SIZE 8
#else
#  error Unexpected architecture
#endif
// This size must be equal to System.Thread_T_Size_In_Bytes and to
// M2.Kernel.TCBs.TCB'Size / 8

typedef struct {
  char filling[TCB_SIZE];
} pthread_t;

typedef struct {
  char filling[SUSP_OBJ_SIZE];
} suspension_t;

/*
 *  THREAD_POOL macro
 */
// Creates un array of TCBs to be initialized as pthread_create() is called.
// This macro, with the desired number of threads, must be invoked by the user
// somewhere (probably in main.c)
#define THREAD_POOL(num_threads) \
   const int max_num_threads=num_threads; \
   pthread_t thread_pool[num_threads];

/*
 *  HWTime_Hz
 */
// Measured in (clock ticks / seg)
// Must have the same value than M2.HAL.HWTime_Hz
#if defined(M2_TARGET_Arduino_Uno) || defined(M2_TARGET_microbit)
#  define HWTIME_HZ 1000
#elif defined(M2_TARGET_stm32f4)
#  define HWTIME_HZ 10000
#elif defined(M2_TARGET_riscv)
#  define HWTIME_HZ  32768000
#else
#  error Unexpected architecture
#endif

/*
 *  hwtime_t
 */
#if defined(M2_TARGET_Arduino_Uno)
typedef uint32_t hwtime_t;
#elif defined(M2_TARGET_stm32f4) || defined(M2_TARGET_microbit) || defined(M2_TARGET_riscv)
typedef uint64_t hwtime_t;
# define HWTIME_64
#else
#  error Unexpected architecture
#endif

/*
 * M2 functions
 */
extern void m2osinit(); // kernel initialization
extern void m2__end_execution__end_execution(int status);
extern void m2__debug__assert(uint8_t assertion);
extern void m2__kernel__api__create_thread(pthread_t *th,
					   void *(*th_body)(void *),
					   int prio,
					   void *args);
extern hwtime_t m2__kernel__api__get_time();
extern void m2__kernel__scheduler__suspend_running_thread(hwtime_t until_abs_time);
extern void m2_dispatch_after_yield_to_higher();
extern void m2__kernel__api__set_job_body_of_running_thread(void *(*th_body)(void *));
extern void ada__synchronous_task_control__set_true(suspension_t *so);
extern void ada__synchronous_task_control__set_false(suspension_t *so);
extern bool ada__synchronous_task_control__current_state(suspension_t *so);
extern void suspend_until_true(suspension_t *so);

/*
 * check_posix_api
 */
// Check if the objects have the same size in the kernel and in this POSIX API.
extern uint8_t m2__kernel__tcbs__tcb_size_in_bytes;
extern uint8_t m2__kernel__api__so_size_in_bytes;
#define CHECK(c) \
 if (!(c)) { \
  puts("Failed check "#c"\n");  m2__end_execution__end_execution(-1); \
 }
static inline void check_posix_api() {
  CHECK(TCB_SIZE == m2__kernel__tcbs__tcb_size_in_bytes);
  CHECK(SUSP_OBJ_SIZE == m2__kernel__api__so_size_in_bytes);
  CHECK(sizeof(int16_t) == 2);
  CHECK(sizeof(int32_t) == 4);
  CHECK(sizeof(int64_t) == 8);
  CHECK(sizeof(uint16_t) == 2);
  CHECK(sizeof(uint32_t) == 4);
  CHECK(sizeof(uint64_t) == 8);
}

#endif // _M2_API_H_
