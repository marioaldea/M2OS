----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Secondary task activated by a simple entry.
--  The entry is always open when the secoundary task
--  calls it.
--  The entry has 2 parameters: an "in" parameter and an "access" parameter.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;

with Tests_Reports;
with M2.Direct_IO;

with PO_Entry_Open_Param_2;

procedure PO_Entry_Open_Param_Test is
   package DIO renames M2.Direct_IO;
   package P2 renames PO_Entry_Open_Param_2;

   Loop_Counter : Integer := 0;

begin
   DIO.Put ("-PO_Entry_Open_Param_Test-");

   loop
      DIO.Put ("-Main-");
      DIO.Put (Integer (P2.Step_Counter));
      DIO.Put (Loop_Counter);

      Tests_Reports.Assert (P2.Step_Counter = Loop_Counter * 2);


      P2.Step_Counter := P2.Step_Counter + 1;
      Loop_Counter := Loop_Counter + 1;

      P2.PO.Open;

      if Loop_Counter = P2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      --  End current activation without blocking but yielding the CPU
      --  to other task with the same priority.

      delay until Ada.Real_Time.Time_First;
   end loop;

end PO_Entry_Open_Param_Test;
