--  To measure the size of an application with SIX periodic tasks that
--  use delay until.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

#if not MEASURE_SIZE then
with M2.Direct_IO;
#end if;
with Size_Periodic_Tasks_Six_Tasks;

procedure Size_Periodic_Tasks_Six_Test is
#if not MEASURE_SIZE then
   package DIO renames M2.Direct_IO;
#end if;

begin
#if not MEASURE_SIZE then
   DIO.Put ("-Size_Periodic_Tasks_Six_Test-");
#end if;
   null;
end Size_Periodic_Tasks_Six_Test;
