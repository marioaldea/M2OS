----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces.C; use Interfaces.C;
with Ada.Unchecked_Conversion;

with M2.Drivers;
with M2.HAL;

with Console_Driver;

package body M2.Direct_IO is
   use type Interfaces.Integer_32;
   
   --  Write : Drivers.Write_Function_Ac;
   --  This pointer is not assiged when using size optimization in ARM

   ---------
   -- Put --
   ---------

   procedure Put (Str : String) is
      Ret : Int;
   begin
      Ret := Console_Driver.Write (Str'Address, size_t (Str'Length));
   end Put;

   --------------
   -- Put_Line --
   --------------

   procedure Put_Line (Str : String) is
   begin
      Put (Str);
      New_Line;
   end Put_Line;

   ---------
   -- Put --
   ---------

   procedure Put (Num : Integer; Base : Interfaces.Unsigned_8 := 10) is
      C : Character;
      Ret : Int;
      Mag : Positive;  -- Magnitude of the number
      Num_Abs : Natural;
      Is_Negative : Boolean := False;
      Digit : Natural;
   begin
      if Integer'Size = Interfaces.Integer_32'Size then
         Put (Interfaces.Integer_32 (Num), Base);
         return;
      end if;
      
      if Num < 0 then
         Num_Abs := -Num;
         Is_Negative := True;
      else
         Num_Abs := Num;
      end if;
      
      Mag := 1;
      while Num_Abs / Mag >= Integer (Base) loop
         Mag := Mag * Integer (Base);
      end loop;

      if Is_Negative then
         C := '-';
         Ret := Console_Driver.Write (C'Address, 1);
      end if;

      loop
         Digit := Num_Abs / Mag;
         if Digit <= 9 then
            C := Character'Val (Character'Pos ('0') + Digit);
         else
            C := Character'Val (Character'Pos ('a') + Digit - 10);
         end if;

         Ret := Console_Driver.Write (C'Address, 1);
         
         exit when Mag = 1;        

         Num_Abs := Num_Abs - Digit * Mag;
         Mag := Mag / Integer (Base);
      end loop;
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (Num : Interfaces.Integer_32;
                  Base : Interfaces.Unsigned_8 := 10) is
      C : Character;
      Ret : Int;
      Mag : Interfaces.Integer_32;  -- Magnitude of the number
      Num_Abs : Interfaces.Integer_32;
      Is_Negative : Boolean := False;
      Digit : Interfaces.Integer_32;
   begin
      if Num < 0 then
         Num_Abs := -Num;
         Is_Negative := True;
      else
         Num_Abs := Num;
      end if;
      
      Mag := 1;
      while Num_Abs / Mag >= Interfaces.Integer_32 (Base) loop
         Mag := Mag * Interfaces.Integer_32 (Base);
      end loop;

      if Is_Negative then
         C := '-';
         Ret := Console_Driver.Write (C'Address, 1);
      end if;

      loop
         Digit := Num_Abs / Mag;
         if Digit <= 9 then
            C := Character'Val (Character'Pos ('0') + Digit);
         else
            C := Character'Val (Character'Pos ('a') + Digit - 10);
         end if;

         Ret := Console_Driver.Write (C'Address, 1);
         
         exit when Mag = 1;        

         Num_Abs := Num_Abs - Digit * Mag;
         Mag := Mag / Interfaces.Integer_32 (Base);
      end loop;
   end Put;

   -------------------------
   -- Put_Null_Terminated --
   -------------------------

   procedure Put_Null_Terminated (Str_Ad : System.Address) is
      function Str_Len (Str_Ac : System.Address) return Interfaces.C.size_t;
      pragma Import(C, Str_Len, "strlen");
      --  Imported from libc

      Ret : Int;
   begin
      Ret := Console_Driver.Write (Str_Ad, Str_Len (Str_Ad));
   end Put_Null_Terminated;

   ---------
   -- Put --
   ---------

   procedure Put (D : Duration) is
      use type HAL.HWTime;
      use Interfaces;
      type Num_Of_NS is range -(2 ** (Duration'Size-1)) .. 
        2 ** (Duration'Size-1) - 1;  -- OJO!!
      
      function To_NS is new Ada.Unchecked_Conversion (Duration,  Num_Of_NS);

      T :  Num_Of_NS := To_NS (D);
   begin
      Put (Interfaces.Integer_32 (T / 1_000_000_000), 10);
      T := T - (T / 1_000_000_000) * 1_000_000_000;
      Put (".");
      Put (Interfaces.Integer_32 (T / 1_000), 10);
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (F : Float) is
   begin
      Put (Interfaces.Integer_32 (Float'Floor (F)));
      PutChar ('.');
      Put (Interfaces.Integer_32 ((F - Float'Floor (F)) * 10.0));
   end Put;

   ---------
   -- Put --
   ---------

   procedure PutChar (C : Character) is
      Ret : Int;
   begin
      Ret := Console_Driver.Write (C'Address, 1);
   end PutChar;


   --------------
   -- New_Line --
   --------------
   
   CR : constant Character := Standard.ASCII.LF;

   procedure New_Line is
      Ret : Int;
   begin
      Ret := Console_Driver.Write (CR'Address, 1);
   end New_Line;

--     procedure Simulavr_Virtual_Ports_Open
--       (Mode : MaRTE.Drivers.File_Access_Mode);
--     pragma Import (C, Simulavr_Virtual_Ports_Open,
--                    "simulavr_virtual_ports_driver");
--
--     function Simulavr_Virtual_Ports_Write (Buffer_Ptr : System.Address;
--                                            Bytes      : Size_T)
--                                            return Int;
--     pragma Import (C, Simulavr_Virtual_Ports_Write,
--                    "simulavr_virtual_ports_write");

   
   -------------------
   -- Change_Device --
   -------------------

--     procedure Change_Device (Write_Function : Drivers.Write_Function_Ac) is
--     begin
--        Write := Write_Function;
--     end Change_Device;
   
   --------------------
   -- Initialization --
   --------------------

   procedure Initialization is
   begin
      Console_Driver.Open (0);
      --  Write := Console_Driver.Write'Access;
   end Initialization;

end M2.Direct_IO;
