----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Real_Time;

with M2.Direct_IO;

with Tests_Reports;

package body Simple_Tasks_Delay_Until_2 is
   package DIO renames M2.Direct_IO;

   use type Ada.Real_Time.Time;

   Next_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);

   Loop_Counter : Integer := 0;

   ---------------------
   -- Other_Task_Init --
   ---------------------

   procedure Other_Task_Init is
   begin
      DIO.Put ("-PLInit-");

      Next_Time := Ada.Real_Time.Clock;
   end Other_Task_Init;

   ---------------------
   -- Other_Task_Body --
   ---------------------

   procedure Other_Task_Body is
   begin
      DIO.Put ("-PL-");

      Tests_Reports.Assert (Step_Counter = Loop_Counter * 2 + 1);
      Step_Counter := Step_Counter + 1;

      Loop_Counter := Loop_Counter + 1;
      Tests_Reports.Assert (Loop_Counter <= Num_Of_Loops);

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Other_Task_Body;

end Simple_Tasks_Delay_Until_2;
