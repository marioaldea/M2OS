----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Concurrent version of light demo
----------------------------------------------------------------------------
-- setup for this example:
-- attach demo robot
----------------------------------------------------------------------------

with Interfaces.C;

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Arduino.EVShield.EV3Touch;
with Arduino.EVShield.EV3Color;
with Arduino.EVShield.Motors;
with Arduino.EVShield.NXTColor;
with SHDefines_H; use SHDefines_H;
with Interfaces.C.Extensions;
with Stdint_H;

procedure EVShield_Demo_Light_Concurrent is
   package DIO renames M2.Direct_IO;

   -- Program variables
   Value_Right : Stdint_H.Uint8_T;
   Value_Left : Float;
   Count : Interfaces.C.Int;
   Aux : Stdint_H.Uint8_T;
   Aux2 : Interfaces.C.Extensions.Bool;

   package Touch_Sensor is new Arduino.EVShield.EV3Touch.Touch (SHDefines_H.SH_BAS2);
   -- Bank A Motor 1
   package Motor_Left is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM1);
   --     -- Bank A Motor 2
   package Motor_Right is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM2);
   package Light_Right is new Arduino.EVShield.NXTColor.Color (SHDefines_H.SH_BBS2);
   package Light_Left is new Arduino.EVShield.EV3Color.Color (Arduino.EVShield.SH_BAS1);
   use type Interfaces.C.Int;
   use type Interfaces.C.Unsigned_Char;
begin
   DIO.Put_Line ("-----------------------------");
   DIO.Put_Line ("Starting EVShield light demo.");
   DIO.Put_Line ("-----------------------------");

   Aux := Light_Left.Set_Mode(Light_Left.MODE_Color_AmbientLight);
   Aux2 := Light_Right.Set_Color(Light_Right.SH_Type_COLORNONE);
   loop
      Count := Touch_Sensor.Get_Bump_Count;

      if Count mod 2 = 1 then
         -- read the value from color sensors
         Value_Right := Light_Right.Read_Value;
         Value_Left := Light_Left.Get_Val;
         -- print the value on serial terminal
         DIO.Put ("Light right: "); DIO.Put_Line (Value_Right'Img);
         DIO.Put ("Light left: "); DIO.Put (Value_Left); DIO.New_Line;


         -- Sensir derecha (nxt) Luz normal 20 -- 40
         --                      Luz directa mas  de 40
         -- Sensor izq (Ev3) Luz directa mas de 30
         --           value1 := value1 - value2;
         if Value_Right >= 45 then
            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            15);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           50);
         elsif Value_Left >= 35.0 then
            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            50);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           15);
         elsif Value_Right >= 30 and Value_Right < 45 and Value_Left >= 10.0 and Value_Left < 35.0 then
            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            30);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           30);
         else
            Aux2 := Motor_Right.Motor_Stop(Motor_Right.SH_Next_Action_Brake);
            Aux2 := Motor_Left.Motor_Stop(Motor_Left.SH_Next_Action_Brake);
         end if;

      else
         DIO.Put_Line ("Demo is STOP. Press touch sensor to start.");
         Aux2 := Motor_Right.Motor_Stop(Motor_Right.SH_Next_Action_Brake);
         Aux2 := Motor_Left.Motor_Stop(Motor_Left.SH_Next_Action_Brake);
      end if;

      -- wait for some time
      Arduino.C_Delay (1000);

   end loop;

end EVShield_Demo_Light_Concurrent;
