-- pragma Profile (Ravenscar);
-- pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
-- pragma Locking_Policy(Ceiling_Locking);
-- pragma Queuing_Policy(Priority_Queuing);

with MicroBit.Display;
with Ada.Text_IO;

with AdaX_Dispatching_Stack_Sharing;

procedure Leds_Text_Scrolling is
begin
   Ada.Text_IO.Put_Line ("Text Scrolling working in M2OS!");
   loop
      null;
      MicroBit.Display.Display ("M2OS ");
   end loop;
end Leds_Text_Scrolling;
