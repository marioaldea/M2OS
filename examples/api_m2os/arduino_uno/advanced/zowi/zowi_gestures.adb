----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  BQ Zowi robot.
--  Show all the Zowi mouth animations.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
with Arduino.Zowi;
with AdaX_Dispatching_Stack_Sharing;
with Interfaces.C;

procedure Zowi_Gestures  is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put_Line ("Zowi_Gestures");

   Arduino.Zowi.Init;
   Arduino.Zowi.Home;

   loop

      for Gesture in Arduino.Zowi.Gestures loop
         DIO.Put ("Mouth Gestures:");
         DIO.Put (Arduino.Zowi.Gestures'Pos(Gesture));
         DIO.New_Line;

         Arduino.Zowi.Play_Gesture (Gesture);

         Arduino.Zowi.Home;
         Arduino.C_Delay (MS => 2_000);
      end loop;

   end loop;
end Zowi_Gestures;
