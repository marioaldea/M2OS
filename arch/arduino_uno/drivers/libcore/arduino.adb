----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of the Arduino library
--  Header file:
--    - hardware/arduino/avr/cores/arduino/Arduino.h
--  C and Cpp files:
--    - hardware/arduino/avr/cores/arduino/wiring_analog.c
--
with M2.HAL;
with M2.HAL.Timer;
with M2.Direct_IO;

package body Arduino is
   package HT renames M2.HAL.Timer;

   use type M2.HAL.Timer.Timer_Ticks;
   use type Interfaces.C.unsigned_long;

   ------------
   -- Millis --
   ------------

   function Millis return Time_MS is
   begin
      pragma Compile_Time_Error (M2.HAL.HWTime_Hz /= 1_000,
                                 "HWTime_Hz /= 1_000");
      return Time_MS (M2.HAL.Get_HWTime);
   end Millis;

   ------------
   -- Micros --
   ------------

   function Micros return Time_US is
      MS : Time_MS := Millis;
      US : Time_US;
   begin
      US := Time_US (M2.HAL.Timer.Get_Counter *
                       M2.HAL.Timer.Microseconds_Per_Timer_Count);
      if MS /= Millis then
         --  A millisecond has pass during the calculation

         MS := MS + 1;
         US := Time_US
           (M2.HAL.Timer.Get_Counter *
              M2.HAL.Timer.Microseconds_Per_Timer_Count);

      end if;

      return Time_US (MS) * 1_000 + US;
   end Micros;

   -------------
   -- C_Delay --
   -------------

   procedure C_Delay (MS : Time_MS) is
      MS_End : constant Time_MS := Millis + MS;
   begin
      pragma Assert (M2.HAL.Are_Interrupts_Enabled);

      while MS_End > Millis loop
         null;
      end loop;
   end C_Delay;

   ------------------------
   -- Delay_Microseconds --
   ------------------------

   --  Use delayMicroseconds from wiring.c since it implements
   --  the delays without reading the clock (simply does a active loop)

   procedure Delay_Microseconds (US : Time_US_16) is
      procedure Delay_Microseconds_C (US : Time_US_16);
      pragma Import (C, Delay_Microseconds_C, "delayMicroseconds_c");
   begin
      pragma Assert (US <= 16#3FFF#);
      --  Limitation stated in Arduino reference page for delayMicroseconds()

      Delay_Microseconds_C (US);
--        pragma Assert (M2.HAL.Are_Interrupts_Enabled);
--
--        if US < 1_000 - HT.Microseconds_Per_Timer_Count * 2 then
--           --  The timer counter can be used to measure the interval
--           declare
--              Ticks_End : constant HT.Timer_Ticks :=
--                HT.Timer_Ticks ((US + HT.Microseconds_Per_Timer_Count - 1) /
--                                    HT.Microseconds_Per_Timer_Count) +
--                  HT.Get_Counter;
--           begin
--              while Ticks_End - HT.Get_Counter > 1 loop
--                 null;
--              end loop;
--           end;
--
--        else
--           declare
--              US_End : constant Time_US := Micros + Time_US (US);
--           begin
--              while US_End > Micros loop
--                 null;
--              end loop;
--           end;
--        end if;
   end Delay_Microseconds;

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:128
   procedure Init;
   pragma Import (C, Init, "init_arduino_in_m2os");

begin
   Init;
end Arduino;
