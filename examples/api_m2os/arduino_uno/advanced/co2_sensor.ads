----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Reads Temperature and Humidity and CO2 Concentration.
--  See 'CO2_Sensor.Main' for details.
--
----------------------------------------------------------------------------
package CO2_Sensor is

   DHT11_Pin : constant := 4;   
   DHT11_Task_Prio : constant := 3;
   DHT11_Task_T_In_Ms : constant := 5_500;
   
   Update_CCS811_Env_Task_Prio : constant := 1;
   Update_CCS811_Env_Task_T_In_Ms : constant := 20_000;
   
   CCS811_Task_Prio : constant := 3;
   CCS811_Task_T_In_Ms : constant := 5_500;
   
   LCD_Task_Prio : constant := 5;
   LCD_Task_T_In_Ms : constant := 4_000;
   
   LCD_Blink_Task_Prio : constant := 6;
   LCD_Blink_Task_T_In_Ms : constant := 600;
   
   type IDA_Level is (IDA1, IDA2, IDA3, IDA4);
   
   type Humidity_Level is (Hum_Very_Dry, Hum_Dry, Hum_Normal, Hum_Wet, Hum_Very_Wet);

end CO2_Sensor;
