pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with EVShieldUART_h;
limited with EVShield_h;
with SHDefines_h;
with Interfaces.C.Extensions;

package EVs_EV3Color_h is

   type MODE_Color is 
     (MODE_Color_ReflectedLight,
      MODE_Color_AmbientLight,
      MODE_Color_MeasureColor);
   pragma Convention (C, MODE_Color);  -- EVs_EV3Color.h:33

   package Class_EVs_EV3Color is
      type EVs_EV3Color is limited record
         parent : aliased EVShieldUART_h.Class_EVShieldUART.EVShieldUART;
      end record;
      pragma Import (CPP, EVs_EV3Color);
      
      function New_EV3Color return EVs_EV3Color;  -- EVShieldUART.h:46
      pragma CPP_Constructor (New_EV3Color, "_ZN12EVShieldUARTC1Ev");

      function init
        (this : access EVs_EV3Color;
         shield : access EVShield_h.Class_EVShield.EVShield;
         bp : SHDefines_h.SH_BankPort) return Extensions.bool;  -- EVs_EV3Color.h:42
      pragma Import (CPP, init, "_ZN12EVs_EV3Color4initEP8EVShield11SH_BankPort");

      function getVal (this : access EVs_EV3Color) return float;  -- EVs_EV3Color.h:45
      pragma Import (CPP, getVal, "_ZN12EVs_EV3Color6getValEv");
   end;
   use Class_EVs_EV3Color;
end EVs_EV3Color_h;
