#!/bin/sh

echo "\n == Measurements summary =="

for f in *.out ; do
  if egrep -q 'Measurement|Total|TIME OF' "$f" ; then
     echo "=$f"
     egrep 'Measurement|Total|TIME OF' "$f"
  fi
done

exit 0
