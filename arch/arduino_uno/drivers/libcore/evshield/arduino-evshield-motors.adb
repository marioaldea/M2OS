----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of motors functions EVSHield.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with EVShieldI2C_h;
with stdint_h;
with Interfaces.C.Extensions;
with SHDefines_h;
with Interfaces.C.Strings;
with Arduino;
with EVShield_h;

package body Arduino.EVShield.Motors is

   package body Motor is
      Which_Motor : EVShield_h.SH_Motor;
      Bank : access EVShield_h.Class_EVShieldBank.EVShieldBank;
      function Motor_Reset
        return Extensions.bool is
      begin
         return EVShield_h.Class_EVShieldBank.motorReset(bank);
      end Motor_Reset;

      function Motor_Get_Encoder_Position
        return stdint_h.int32_t is
      begin
         return EVShield_h.Class_EVShieldBank.motorGetEncoderPosition(bank,which_motor);
      end Motor_Get_Encoder_Position;

      function Motor_Run_Rotations
        (Direction : SH_Direction;
         Speed : int;
         Rotations : long;
         Wait_For_Completion : SH_Completion_Wait;
         Next_Action : SH_Next_Action) return stdint_h.uint8_t is
      begin
         return EVShield_h.Class_EVShieldBank.motorRunRotations(bank,
                                                                Which_Motor,
                                                                EVShield_h.SH_Direction(Direction),
                                                                Speed,
                                                                Rotations,
                                                                EVShield_h.SH_Completion_Wait(Wait_For_Completion),
                                                                EVShield_h.SH_Next_Action(Next_Action));
      end Motor_Run_Rotations;

      procedure Motor_Run_Unlimited
        (Direction : SH_Direction;
         Speed : int) is
      begin
         EVShield_h.Class_EVShieldBank.motorRunUnlimited(Bank,
                                                         Which_Motor,
                                                         EVShield_h.SH_Direction(Direction),
                                                         Speed);
      end Motor_Run_Unlimited;

      function Motor_Stop
        (Next_Action : SH_Next_Action) return Extensions.bool is
      begin
         return EVShield_h.Class_EVShieldBank.motorStop(Bank,
                                                        Which_Motor,
                                                        EVShield_h.SH_Next_Action(Next_Action));
      end Motor_Stop;
   begin


      if Bp = Arduino.EVShield.Motors.SH_BAM1 then
         Which_Motor := EVShield_h.SH_Motor_1;
         Bank := EVShield_Obj.Bank_A'Access;
      elsif Bp = Arduino.EVShield.Motors.SH_BAM2 then
         Which_Motor := EVShield_h.SH_Motor_2;
         Bank := EVShield_Obj.Bank_A'Access;
      elsif Bp = Arduino.EVShield.Motors.SH_BBM1 then
         Which_Motor := EVShield_h.SH_Motor_1;
         Bank := EVShield_Obj.Bank_B.Parent'Access;
      elsif Bp = Arduino.EVShield.Motors.SH_BBM2 then
         Which_Motor := EVShield_h.SH_Motor_2;
         Bank := EVShield_Obj.Bank_B.Parent'Access;
      end if;
   end Motor;


   --

   --     package body Bank_A is
   --        function motorReset
   --          return Extensions.bool is
   --        begin
   --           return EVShield_h.Class_EVShieldBank.motorReset(EVShield_obj.bank_a'Access);
   --        end motorReset;
   --
   --        function motorGetEncoderPosition
   --          (which_motor : SH_Motor) return stdint_h.int32_t is
   --        begin
   --           return EVShield_h.Class_EVShieldBank.motorGetEncoderPosition(EVShield_obj.bank_a'Access, which_motor);
   --        end motorGetEncoderPosition;
   --
   --        function motorRunRotations
   --          (which_motors : SH_Motor;
   --           direction : SH_Direction;
   --           speed : int;
   --           rotations : long;
   --           wait_for_completion : SH_Completion_Wait;
   --           next_action : SH_Next_Action) return stdint_h.uint8_t is
   --        begin
   --           return EVShield_h.Class_EVShieldBank.motorRunRotations(EVShield_obj.bank_a'Access,
   --                                    which_motors,
   --                                    EVShield_h.SH_Direction(direction),
   --                                    speed,
   --                                    rotations,
   --                                    EVShield_h.SH_Completion_Wait(wait_for_completion),
   --                                    EVShield_h.SH_Next_Action(next_action));
   --        end motorRunRotations;
   --
   --        procedure motorRunUnlimited
   --          (which_motors : SH_Motor;
   --           direction : SH_Direction;
   --           speed : int) is
   --        begin
   --           EVShield_h.Class_EVShieldBank.motorRunUnlimited(EVShield_obj.bank_a'Access,
   --                             which_motors,
   --                             EVShield_h.SH_Direction(direction),
   --                             speed);
   --        end motorRunUnlimited;
   --
   --        function motorStop
   --          (which_motors : SH_Motor;
   --           next_action : SH_Next_Action) return Extensions.bool is
   --        begin
   --           return EVShield_h.Class_EVShieldBank.motorStop(EVShield_obj.bank_a'Access,
   --                            which_motors,
   --                            EVShield_h.SH_Next_Action(next_action));
   --        end motorStop;
   --
   --     end Bank_A;
   --
   --     package body Bank_B is
   --        function motorReset
   --          return Extensions.bool is
   --        begin
   --           return EVShield_h.Class_EVShieldBank.motorReset(EVShield_obj.bank_b.parent'Access);
   --        end motorReset;
   --
   --        function motorGetEncoderPosition
   --          (which_motor : SH_Motor) return stdint_h.int32_t is
   --        begin
   --           return EVShield_h.Class_EVShieldBank.motorGetEncoderPosition(EVShield_obj.bank_a'Access, which_motor);
   --        end motorGetEncoderPosition;
   --
   --        function motorRunRotations
   --          (which_motors : SH_Motor;
   --           direction : SH_Direction;
   --           speed : int;
   --           rotations : long;
   --           wait_for_completion : SH_Completion_Wait;
   --           next_action : SH_Next_Action) return stdint_h.uint8_t is
   --        begin
   --           return EVShield_h.Class_EVShieldBank.motorRunRotations(EVShield_obj.bank_b.parent'Access,
   --                                    which_motors,
   --                                    EVShield_h.SH_Direction(direction),
   --                                    speed,
   --                                    rotations,
   --                                    EVShield_h.SH_Completion_Wait(wait_for_completion),
   --                                    EVShield_h.SH_Next_Action(next_action));
   --        end motorRunRotations;
   --
   --        procedure motorRunUnlimited
   --          (which_motors : SH_Motor;
   --           direction : SH_Direction;
   --           speed : int) is
   --        begin
   --           EVShield_h.Class_EVShieldBank.motorRunUnlimited(EVShield_obj.bank_b.parent'Access,
   --                             which_motors,
   --                             EVShield_h.SH_Direction(direction),
   --                             speed);
   --        end motorRunUnlimited;
   --
   --        function motorStop
   --          (which_motors : SH_Motor;
   --           next_action : SH_Next_Action) return Extensions.bool is
   --        begin
   --           return EVShield_h.Class_EVShieldBank.motorStop(EVShield_obj.bank_b.parent'Access,
   --                            which_motors,
   --                            EVShield_h.SH_Next_Action(next_action));
   --        end motorStop;
   --
   --     end Bank_B;

end Arduino.EVShield.Motors;
