----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Reads distance using the "Sharp IR Range Finder GP2D120"  
--
--     -----------------                 ----------------------
--     |               |                 |        Arduino Uno |
--     | GP2D120   Vout|-----------------|A4                  |
--     |            VCC|-----------------|5V                  |
--     |            GND|-----------------|GND                 |
--     |               |                 |                    |
--     -----------------                 ----------------------
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure Distance_IR_Range_Finder_SHARP_GP2D120_Demo is
   package DIO renames M2.Direct_IO;
   
   Sensor_Pin : constant := 4;
   
   ------------------
   -- Read_GP2D120 --
   ------------------
   
   --  Distance in centimeters.

   function Read_GP2D120 (Sensor_Pin : Arduino.ADC_Chanel)
                          return Float is
      Val : Float := 0.0;
      Num_Measurements : constant := 5;
   begin
      for I in 1 .. Num_Measurements loop
         Val := Val + Float (Arduino.Analog_Read (Sensor_Pin));
      end loop;
      Val := Val / Float (Num_Measurements);
      return (2914.0 / (Val + 5.0)) - 1.0;
   end Read_GP2D120;
   
begin
   DIO.Put_Line ("Distance IR Range Finder SHARP GP2D120 Demo");
   
   loop
      DIO.Put ("Dist:");
      DIO.Put (Read_GP2D120 (Sensor_Pin));
      DIO.Put_Line ("cm");
                 
      Arduino.C_Delay (500);
   end loop;
   
end Distance_IR_Range_Finder_SHARP_GP2D120_Demo;
