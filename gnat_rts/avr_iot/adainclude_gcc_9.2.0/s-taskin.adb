------------------------------------------------------------------------------
--                                                                          --
--                 GNAT RUN-TIME LIBRARY (GNARL) COMPONENTS                 --
--                                                                          --
--                        S Y S T E M . T A S K I N G                       --
--                                                                          --
--                                  B o d y                                 --
--                                                                          --
--                     Copyright (C) 2001-2018, AdaCore                     --
--                                                                          --
-- GNARL is free software; you can  redistribute it  and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 3,  or (at your option) any later ver- --
-- sion. GNARL is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.                                     --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
-- GNARL was developed by the GNARL team at Florida State University.       --
-- Extensive contributions were provided by Ada Core Technologies, Inc.     --
--                                                                          --
------------------------------------------------------------------------------

--  M2OS: stack sharing version of this package.

--  This is the Ravenscar/HI-E and Ravenscar/full version of this package

--  Simplified version of the GNAT System.Tasking package.

with System.Task_Primitives.Operations;

package body System.Tasking is

   ------------------------
   -- Local Declarations --
   ------------------------

   Main_Priority : Integer := Unspecified_Priority;
   pragma Export (C, Main_Priority, "__gl_main_priority");
   --  Priority associated with the environment task. By default, its value is
   --  undefined, and can be set by using pragma Priority in the main program.

   Main_CPU : Integer := Unspecified_CPU;
   pragma Export (C, Main_CPU, "__gl_main_cpu");
   --  Affinity associated with the environment task. By default, its value is
   --  undefined, and can be set by using pragma CPU in the main program.
   --  Switching the environment task to the right CPU is left to the user.

   ---------------------
   -- Initialize_ATCB --
   ---------------------

--     procedure Initialize_ATCB (Thread_Ac : OS_Interface.Thread_Ac;
--                                T         : Task_Id) is
--     begin
--        T.Thread_Ac := Thread_Ac;
--     end Initialize_ATCB;

   ----------------
   -- Initialize --
   ----------------

   Initialized : Boolean := False;
   --  Used to prevent multiple calls to Initialize

   procedure Initialize is
--      Base_Priority : Any_Priority;

--      Success : Boolean;
--      pragma Warnings (Off, Success);

   begin
      if Initialized then
         return;
      end if;

      Initialized := True;

      --  Compute priority

      if Main_Priority = Unspecified_Priority then
         Main_Priority := Default_Priority;  -- M2OS
      end if;

      Task_Primitives.Operations.Initialize;
   end Initialize;

   -----------
   -- Self --
   ----------

   function Self return Task_Id renames System.Task_Primitives.Operations.Self;

end System.Tasking;
