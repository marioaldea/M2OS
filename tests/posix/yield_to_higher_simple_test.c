//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (mario.aldea@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Simple test of yield_to_higher() using the M2OS POSIX-like layer.
// The high priority thread (thread_body_hp) is always ready when thread_body_lp
// calls yield_to_higher().
//
// th_hp #......._#......#......._#......#..   ���       ^ = thread activation
//       ^       ^       ^       ^       ^               # = thread execution
// th_lp .########.#......########.#......##   ���       _ = thread active
//        ^       y       ^      y        ^              . = thread suspended
//       0       1       2       3       4               y = yield_to_higher
//
#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include "../reports/tests_reports.h"

extern int m2__hal__get_current_stack_size_in_bytes();
extern int m2__kernel__api__global_stack_base();
extern int m2__kernel__api__current_stack_top();

THREAD_POOL(2);

#define COUNTER_HP_END_VALUE 11
int counter_lp = 0;
int counter_hp = 0;


#define PERIOD_LP_NS 200000000
#define PERIOD_HP_NS 100000000
#define DELAY_ACTIVATION_LP_NS (PERIOD_HP_NS / 10)
const struct timespec period_lp = TS(0, PERIOD_LP_NS);
const struct timespec period_hp = TS(0, PERIOD_HP_NS);
const struct timespec eat_lp = TS(0, PERIOD_HP_NS);

#define ARRAY_SIZE 10

void puts_now(char *msg) {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  putint(TS_SEC(now)); puts("s");
  putint(TS_NSEC(now) / 100000); puts("ms ");
  puts(msg);
}

//******************//
//  thread_body_lp  //
//******************//
struct timespec next_activation_time_lp = TS(0, 0);

void * thread_body_lp(void *arg) {
  puts_now("Thread LP\n");
  // puts(" SP1=");
  // putint(m2_hal_regs_get_sp_reg());
  // puts(" ");
  // putint(m2__hal__get_current_stack_size_in_bytes());
  // puts("\n");
  tests_reports__assert(counter_hp == counter_lp * 2 + 1);
  counter_lp++;
  
  const int value = 10 * counter_hp + counter_lp;
  int a[ARRAY_SIZE];
  for (int i = 0; i < ARRAY_SIZE; i++) {
    a[i] = value;
    }
  
  for (int i = 0; i < ARRAY_SIZE; i++) {  
    putint(a[i]); puts(" ");
    tests_reports__assert (a[i] == value);
  }
  puts("\n");
  
  puts_now("Thread LP: eat\n");  
  tests_reports__eat(eat_lp);
  
  puts_now("Thread LP: before yield_to_higher() \n");  
  yield_to_higher(); 
  puts_now("Thread LP: after yield_to_higher()\n");
  tests_reports__assert(counter_hp == counter_lp * 2);
  
  for (int i = 0; i < ARRAY_SIZE; i++) {  
    putint(a[i]); puts(" ");
    tests_reports__assert (a[i] == value);
  }
  puts("\n");

  puts_now("Thread LP: clock_nanosleep()\n");
  TS_INC(next_activation_time_lp, period_lp);
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time_lp, NULL);
  
  return NULL;
}

//******************//
//  thread_body_hp  //
//******************//
struct timespec next_activation_time_hp = TS(0, 0);

void * thread_body_hp(void *arg) {
  puts_now("Thread HP\n");
  
  counter_hp++;
  
  tests_reports__assert(counter_hp / 2 == counter_lp);
  
  if (counter_hp == COUNTER_HP_END_VALUE) {
    tests_reports__test_ok();
  }

  puts_now("Thread HP: clock_nanosleep()\n");
  TS_INC(next_activation_time_hp, period_hp);
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time_hp, NULL);
  
  return NULL;
} 


//**********//
//   main   //
//**********//
int main (int argc, char **argv) {
  m2osinit();
  
  puts("Main\n");
  puts(" SPB=");
  putint(m2__kernel__api__global_stack_base());
  puts("\n SPm=");
  putint(m2__kernel__api__current_stack_top());
  puts(" ");
  putint(m2__hal__get_current_stack_size_in_bytes());
  puts("\n");
  
  check_posix_api();
    
  pthread_t th_lp;
  pthread_t th_hp;
  pthread_attr_t attr_lp;
  pthread_attr_t attr_hp;
  struct sched_param sch_param_lp = {.sched_priority = 10};
  struct sched_param sch_param_hp = {.sched_priority = 20};
  
  clock_gettime(CLOCK_MONOTONIC, &next_activation_time_hp);
  const struct timespec delay_activation_lp = TS(0, DELAY_ACTIVATION_LP_NS);
  TS_ADD(next_activation_time_lp, next_activation_time_hp, delay_activation_lp);

  pthread_attr_init(&attr_lp);
  pthread_attr_setschedparam(&attr_lp, &sch_param_lp);    
  pthread_create(&th_lp, &attr_lp, thread_body_lp, NULL);
  
  pthread_attr_init(&attr_hp);
  pthread_attr_setschedparam(&attr_hp, &sch_param_hp);       
  pthread_create(&th_hp, &attr_hp, thread_body_hp, NULL);

  puts("Main ends\n");
  puts(" SPm=");
  putint(m2__kernel__api__current_stack_top());
  puts(" ");
  putint(m2__hal__get_current_stack_size_in_bytes());
  puts("\n");

  return 0;
}
