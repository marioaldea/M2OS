----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Authors: David Garcia Villaescusa (garciavd@unican.es)
--           Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Epiphany_Config;
use Epiphany_Config;
with System;
with Interfaces;
use Interfaces;
with E_Lib;
with Ada.Unchecked_Conversion;
with Ada.Synchronous_Task_Control;

package Epiphany_Messages is
   Sampling_Ports_Per_Core : constant Unsigned_32 := 4;
   type Sampling_Port is private;
   
   type SP_Index is range 0 .. Sampling_Ports_Per_Core-1;
   type SP_Id is private;
   Null_SP_Id : constant SP_Id;
   
   ------------------------
   -- Get_Sampling_Ports --
   ------------------------
   
   function Get_Sampling_Port (C : in E_Lib.Core;
                               Id : in SP_Index)
                               return SP_Id;
   
   -------------------------
   -- Init_Sampling_Ports --
   -------------------------
   
   function Init_Sampling_Port (C : in E_Lib.Core; 
                                Id : in SP_Index;
                                Addr : in System.Address;
                                Size : in Unsigned_32)
                                return SP_Id;
   
   --------------------------
   -- Write_Sampling_Ports --
   --------------------------
   
   function Write_Sampling_Port (SP : in SP_Id;
                                 Orig : in System.Address;
                                 Orig_Size : in Unsigned_32)
                                 return Boolean;

   -------------------------
   -- Read_Sampling_Ports --
   -------------------------
   
   procedure Read_Sampling_Port (SP : in SP_Id;
                                 Dest : in System.Address;
                                 Dest_Size : in Unsigned_32;
                                 Successful : out Boolean;
                                 Is_New : out Boolean);
   
   -------------------
   -- Take_SP_Mutex --
   -------------------
   
   procedure Take_SP_Mutex (SP_Acc : in SP_Id);
   
   ----------------------
   -- Release_SP_Mutex --
   ----------------------
   
   procedure Release_SP_Mutex (SP_Acc : in SP_Id);
private
   type SP_Id is access Sampling_Port;
   Null_SP_Id : constant SP_Id := null;
   
   type Sampling_Port is record
      Initialized : Boolean;
      Mutex : Integer;
      Size : Unsigned_32;
      Content_Address : System.Address;
      Is_New : Boolean;
      Core : E_Lib.Core;
   end record with Alignment => Standard'Maximum_Alignment;

end Epiphany_Messages;
