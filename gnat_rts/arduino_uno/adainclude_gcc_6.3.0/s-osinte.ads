--  M2OS Arduino Uno version of this package.

--  Imports from M2.Kernel.API

with Interfaces;
with Interfaces.C;
with Ada.Unchecked_Conversion;

package System.OS_Interface is
   pragma Preelaborate;

   ----------
   -- Time --
   ----------

   type Time is new Interfaces.Unsigned_32;
   --  Representation of the time in the underlying tasking system

   type Time_Span is new Interfaces.Integer_32;
   --  Represents the length of time intervals in the underlying tasking
   --  system.

   Ticks_Per_Second : constant := 1_000;
   --  Number of clock ticks per second
   --  M2OS: Should have the same value than M2.HAL.HWT_HZ

   function Clock return Time;
   pragma Import (C, Clock, "m2__hal__get_hwtime");
   --  Get the number of ticks elapsed since startup

   procedure Delay_Until (T : Time);
   pragma Import (C, Delay_Until,
                  "m2__kernel__scheduler__suspend_running_thread");
   --  Suspend the calling task until the absolute time specified by T

   -------------
   -- Threads --
   -------------

   type Thread_Ac is new System.Address;

   Null_Thread_Ac : constant Thread_Ac := Thread_Ac (System.Null_Address);

   subtype Thread_Body_Ac is System.Address;
--     pragma Convention (C, Thread_Body_Ac);

--     type Procedure_Ac is access procedure;
--     function To_Thread_Body_Ac is
--       new Ada.Unchecked_Conversion (Procedure_Ac, Thread_Body_Ac);
--     function To_Thread_Body_Ac is
--       new Ada.Unchecked_Conversion (System.Address, Thread_Body_Ac);

   ----------
   -- Self --
   ----------

   function Self return Thread_Ac;
   pragma Import (C, Self, "m2_self");

   ------------------
   -- Set_Job_Body --
   ------------------

   procedure Set_Job_Body (Body_Ac : Thread_Body_Ac);
   pragma Import (C, Set_Job_Body,
                  "m2__kernel__api__set_job_body_of_running_thread");
   pragma Import_Procedure (Set_Job_Body,
                            "m2__kernel__api__set_job_body_of_running_thread",
                            (Thread_Body_Ac),
                            Mechanism => Value);

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

   procedure Set_Global_Stack_Limit;
   pragma Import (C, Set_Global_Stack_Limit,
                  "m2__kernel__api__set_global_stack_limit");

   -------------------
   -- Create_Thread --
   -------------------

   procedure Create_Thread (Th_Ac : out Thread_Ac;
                            Th_Body : Thread_Body_Ac;
                            Prio : Priority;
                            Args : System.Address);
   pragma Import (C, Create_Thread, "m2__kernel__api__create_thread");
--     pragma Import_Procedure (
--       (Internal        => Create_Thread,
--        External        => "marte__kernel__api__create_thread",
--        Parameter_Types => (Thread_Ac,
--                            Thread_Body_Ac,
--                            Priority),
--        Mechanism       => (Th_Ac   => Reference,
--                            Th_Body => Value,
--                            Prio    => Value)));

   ----------------------------------------------
   -- Running_Thread_Ends_Job_Without_Blocking --
   ----------------------------------------------

   procedure Running_Thread_Ends_Job_Without_Blocking;
   pragma Import (C, Running_Thread_Ends_Job_Without_Blocking,
                  "m2_running_thread_ends_job_without_blocking");

   --------------------------
   -- Block_Running_Thread --
   --------------------------

   procedure Block_Running_Thread;
   pragma Import (C, Block_Running_Thread, "m2_block_running_thread");

   -------------------------------
   -- Activate_Suspended_Thread --
   -------------------------------

   procedure Activate_Suspended_Thread (Th_Ac : Thread_Ac);
   pragma Import (C, Activate_Suspended_Thread,
                  "m2_activate_suspended_thread");

   -----------------
   --  Interrupts --
   -----------------

   type Interrupt_Range is new Integer range 0 .. 1;

   --  Arduino Uno pin 2 produces interrupt 0 and pin 3 produces interrupt 1

   type Interrupt_Mode is
     (Interrupt_When_Low, -- trigger the interrupt whenever the pin is low
      Interrupt_When_Change, -- trigger interrupt whenever the pin changes
      Interrupt_When_Falling, -- trigger when the pin goes from high to low.
      Interrupt_When_Rising); -- trigger when the pin goes from low to high
   for Interrupt_Mode use
     (Interrupt_When_Low => 0,
      Interrupt_When_Change => 1,
      Interrupt_When_Falling => 2,
      Interrupt_When_Rising => 3);
   for Interrupt_Mode'Size use Interfaces.C.int'Size;

   type Parameterless_Handler is access protected procedure;
   pragma Convention (Convention => C,
                      Entity => Parameterless_Handler);

   procedure Enable_Interrupts;
   pragma Inline_Always (Enable_Interrupts);
   pragma Import (Intrinsic, Enable_Interrupts, "__builtin_avr_sei");

   procedure Disable_Interrupts;
   pragma Inline_Always (Disable_Interrupts);
   pragma Import (Intrinsic, Disable_Interrupts, "__builtin_avr_cli");

   procedure Attach_Interrupt
     (Int_Num : Interrupt_Range;
      Handler  : Parameterless_Handler;
      Mode : Interrupt_Mode);
   pragma Import (C, Attach_Interrupt, "attachInterrupt");

   ---------------------
   -- Multiprocessors --
   ---------------------

--     function Get_Affinity (Id : Thread_Id) return Multiprocessors.CPU_Range
--       renames System.BB.Threads.Get_Affinity;
   --  Return CPU affinity of the given thread (maybe Not_A_Specific_CPU)

--     function Get_CPU  (Id : Thread_Id) return Multiprocessors.CPU is
--       (Multiprocessors.CPU'First);
--       renames System.BB.Threads.Get_CPU;
   --  Return the CPU in charge of the given thread (always a valid CPU)

--     function Current_Priority
--       (CPU_Id : Multiprocessors.CPU) return System.Any_Priority
--       renames System.BB.Threads.Queues.Current_Priority;
   --  Return the active priority of the current thread or
   --  System.Any_Priority'First if no threads are running.

   --  function Current_CPU return Multiprocessors.CPU is
   --   (Multiprocessors.CPU'First);
--       renames System.BB.Board_Support.Multiprocessors.Current_CPU;

   procedure Initialize;
   --  To be called from System.Tasking.Initialize
   pragma Import (C, Initialize, "m2osinit");
   --  M2OS library (libm2os.a) initializacion
end System.OS_Interface;
