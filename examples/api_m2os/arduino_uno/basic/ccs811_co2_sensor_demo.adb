----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Reads air quality: CO2 concentration (ppm) and Total Volatile Organic
--  Compounds (ppb) using the "KS0457 keyestudio CCS811 Carbon Dioxide Air
--  Quality Sensor".
--
--  Based on example in the CCS811 library:
--  https://wiki.keyestudio.com/KS0457_keyestudio_CCS811_Carbon_Dioxide_Air_Quality_Sensor
--
--     -----------------                 ----------------------
--     |               |                 |                    |
--     |           Wake|-----------------|GND                 |
--     | KS0457     SCL|-----------------|A5      Arduino Uno |
--     | (CCS811)   SDA|-----------------|A4                  |
--     |            VCC|-----------------|5V                  |
--     |            GND|-----------------|GND                 |
--     |               |                 |                    |
--     -----------------                 ----------------------
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino.CCS811;
with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Interfaces.C;

procedure CCS811_CO2_Sensor_Demo is
   package DIO renames M2.Direct_IO;
   package CCS811 renames Arduino.CCS811;
   Ret : Integer;
begin
   DIO.Put_Line ("CCS811 Demo");
   
   loop
      Ret := CCS811.Begin_CCS811;     
      exit when Ret = 0;
      
      Arduino.C_Delay (1_000);
   end loop;
   
   CCS811.Set_Meas_Cycle (CCS811.ECycle_250ms);
 
   loop
      DIO.Put_Line ("Read sensor");
      
      Arduino.C_Delay (1_000);
--        
      if CCS811.Check_Data_Ready then
         DIO.Put ("CO2 (ppm):" &
                    Integer (CCS811.Get_CO2_PPM)'Img);
         DIO.Put (" TVOC (ppb):" &
                    Integer (CCS811.Get_TVOC_PPB)'Img);
         DIO.New_Line;
      else
         DIO.Put_Line ("Data not ready");
      end if;

      CCS811.Write_Base_Line (CCS811.Base_Line_Value);
   end loop;
   
end CCS811_CO2_Sensor_Demo;
