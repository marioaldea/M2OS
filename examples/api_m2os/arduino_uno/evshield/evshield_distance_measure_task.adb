----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Task that gets the distances measures.
----------------------------------------------------------------------------

pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

with M2.Direct_IO;
with M2.HAL;
with M2.Kernel.API;
with Interfaces.C.Extensions;
with Arduino.EVShield.EV3Ultrasonic;
with Stdint_H;



package body EVShield_Distance_Measure_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;
   ----------------
   -- Other_Task --
   ----------------

   Next_Time : RT.Time;
   Period : constant RT.Time_Span := RT.Milliseconds (250);

   Aux : Stdint_H.Uint8_T;
   Aux1 : Float;
   Aux2 : Float;

   -- Ultrasonic sensors
   package Ultrasonic_Right is new Arduino.EVShield.EV3Ultrasonic.Ultrasonic (Arduino.EVShield.SH_BBS2);
   package Ultrasonic_Left is new Arduino.EVShield.EV3Ultrasonic.Ultrasonic (Arduino.EVShield.SH_BAS2);

   procedure Other_Task_Init is
   begin
      Next_Time := Ada.Real_Time.Clock;

      -- initialize the sensor mode.
      Aux := Ultrasonic_Right.Set_Mode(Ultrasonic_Right.MODE_Sonar_CM);
      Aux := Ultrasonic_Left.Set_Mode(Ultrasonic_Left.MODE_Sonar_CM);
   end Other_Task_Init;


   procedure Other_Task_Body is
   begin
      Aux1 := 0.0;
      Aux2 := 0.0;

      -- get the reading(s) from sensor
      Distance_Right := Ultrasonic_Right.Get_Dist;
      Distance_Left := Ultrasonic_Left.Get_Dist;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Other_Task_Body;

   Other_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Other_Task_Init'Access,
      Body_Ac  => Other_Task_Body'Access,
      Priority => 4);

end EVShield_Distance_Measure_Task;
