----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of EVSHieldI2C.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with EVShieldI2C_h;
with stdint_h;
with Interfaces.C.Extensions;
with SHDefines_h;
with Interfaces.C.Strings;
with Arduino;
with Arduino.EVShield;
with EVShieldI2C_h;

package body Arduino.EVShield.EVShieldI2C is
   package body Bank_A is

      function Get_Firmware_Version return Interfaces.C.Strings.chars_ptr is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getFirmwareVersion(EVShield_Obj.Bank_A.parent'Access);
      end Get_Firmware_Version;

      function Get_Vendor_ID return Interfaces.C.Strings.chars_ptr is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getVendorID(EVShield_Obj.Bank_A.parent'Access);
      end Get_Vendor_ID;

      function Get_Device_ID return Interfaces.C.Strings.chars_ptr is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getDeviceID(EVShield_Obj.Bank_A.parent'Access);
      end Get_Device_ID;

      function Get_Feature_Set return Interfaces.C.Strings.chars_ptr is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getFeatureSet(EVShield_Obj.Bank_A.parent'Access);
      end Get_Feature_Set;

      function Get_Error_Code return stdint_h.uint8_t is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getErrorCode(EVShield_Obj.Bank_A.parent'Access);
      end Get_Error_Code;

      function Check_Address return Extensions.bool is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.checkAddress(EVShield_obj.Bank_A.parent'Access);
      end Check_Address;

      function Set_Address (Address : stdint_h.uint8_t) return Extensions.bool is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.setAddress(EVShield_Obj.Bank_A.parent'Access, Address);
      end Set_Address;

   end Bank_A;

   package body Bank_B is
      function Get_Firmware_Version return Interfaces.C.Strings.chars_ptr is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getFirmwareVersion(EVShield_Obj.bank_b.parent.parent'Access);
      end Get_Firmware_Version;

      function Get_Vendor_ID return Interfaces.C.Strings.chars_ptr is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getVendorID(EVShield_obj.bank_b.parent.parent'Access);
      end Get_Vendor_ID;

      function Get_Device_ID return Interfaces.C.Strings.chars_ptr is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getDeviceID(EVShield_obj.bank_b.parent.parent'Access);
      end Get_Device_ID;

      function Get_Feature_Set return Interfaces.C.Strings.chars_ptr is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getFeatureSet(EVShield_obj.bank_b.parent.parent'Access);
      end Get_Feature_Set;

      function Get_Error_Code return stdint_h.uint8_t is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.getErrorCode(EVShield_obj.bank_b.parent.parent'Access);
      end Get_Error_Code;

      function Check_Address return Extensions.bool is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.checkAddress(EVShield_obj.bank_b.parent.parent'Access);
      end Check_Address;

      function Set_Address (address : stdint_h.uint8_t) return Extensions.bool is
      begin
         return EVShieldI2C_h.Class_EVShieldI2C.setAddress(EVShield_obj.bank_b.parent.parent'Access, address);
      end Set_Address;

   end Bank_B;

end Arduino.EVShield.EVShieldI2C;
