
with Ada.Real_Time;

#if not MEASURE_SIZE then
  with M2.Direct_IO;
with Tests_Reports;
#end if;

  with AdaX_Dispatching_Stack_Sharing.Periodic_Task;

package body Size_Periodic_Tasks_Two_Tasks is
#if not MEASURE_SIZE then
   package DIO renames M2.Direct_IO;
#end if;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;
   
   Priority : constant := 3;
   Period_In_Ms : constant := 100;

   ------------
   -- Task_1 --
   ------------
 
#if not MEASURE_SIZE then  
   Task_1_Executed : Boolean := False;
#end if;
   Next_Time_1 : RT.Time;
   Period_1 : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms);
   
   procedure Task_1_Init is
   begin
      Next_Time_1 := RT.Clock;
   end Task_1_Init;

   procedure Task_1_Body is
   begin
#if not MEASURE_SIZE then
      DIO.Put ("Task1");
      Task_1_Executed := True;
#end if;
      
      Next_Time_1 := Next_Time_1 + Period_1;
      delay until Next_Time_1;
   end Task_1_Body;
     
   Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_1_Init'Access,
      Body_Ac  => Task_1_Body'Access,
      Priority => Priority);

   ------------
   -- Task_2 --
   ------------
 
#if not MEASURE_SIZE then  
   Task_2_Executed : Boolean := False;
#end if;
   Next_Time_2 : RT.Time;
   Period_2 : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms);
   
   procedure Task_2_Init is
   begin
      Next_Time_2 := RT.Clock;
   end Task_2_Init;

   procedure Task_2_Body is
   begin
#if not MEASURE_SIZE then
      DIO.Put ("Task2");
      if Task_2_Executed then
         -- Second activation
             
         Tests_Reports.Assert (Task_1_Executed);
         Tests_Reports.Test_OK; 
 
      else
         --  First activation
       
         Tests_Reports.Assert (not Task_1_Executed);

      end if;
      
      Task_2_Executed := True;
#end if;
      
      Next_Time_2 := Next_Time_2 + Period_2;
      delay until Next_Time_2;
   end Task_2_Body;
     
   Task_2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_2_Init'Access,
      Body_Ac  => Task_2_Body'Access,
      Priority => Priority + 1);

end Size_Periodic_Tasks_Two_Tasks;
