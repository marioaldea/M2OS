
with Libadalang.Common;
with Langkit_Support.Text; use Langkit_Support.Text;

with M2OS_IO;

package body M2os_Processing.Helper is

   ------------------------
   -- Get_Node_As_String --
   ------------------------

   function Get_Node_As_String (N : Ada_Node'Class) return String is
      Content_Bytes : constant String := Langkit_Support.Text.Encode (Text(Node => N),
                                                                      Libadalang.Common.Default_Charset);
   begin
      return M2OS_IO.Trim (Content_Bytes);
   end Get_Node_As_String;

   ---------------------------
   -- Write_Global_Variable --
   ---------------------------
   -- Procedure to write task's local variables as global variables

   procedure Write_Global_Variable (Variable : Ada_Node'Class; Task_Name : String) is
      Variable_Text : constant String := Get_Node_As_String (Variable);
   begin
      --M2OS_IO.Write_Indentation;
      M2OS_IO.Write_Line (Task_Name&"_"&M2OS_IO.Trim(Variable_Text));
      --M2OS_IO.Remove_Indentation;
   end Write_Global_Variable;

   ---------------------------
   -- Write_Rename_Variable --
   ---------------------------
   -- Procedure to rename a variable inside a task
   -- Task's variables defined in the same line is not supported (one variable should be defined per line)

   procedure Write_Rename_Variable (The_Name : String; The_Type : String; Task_Name : String) is
   begin
      M2OS_IO.Write_Indentation;
      M2OS_IO.Write_Line (The_Name &" : " & The_Type & " renames " & Task_Name&"_" & The_Name & ";");
      M2OS_IO.Remove_Indentation;
   end Write_Rename_Variable;

   -------------------------------
   -- Write_Package_Spec_Header --
   -------------------------------
   -- Write package spec header

   procedure Write_Package_Spec_Header (Name : String) is
   begin
      M2OS_IO.Write_Line ("package " & Name & " is");
   end Write_Package_Spec_Header;

   -------------------------------
   -- Write_Package_Body_Header --
   -------------------------------
   -- Write package body header

   procedure Write_Package_Body_Header (Name : String) is
   begin
      M2OS_IO.Write_Line ("package body " & Name & " is");
   end Write_Package_Body_Header;

   ------------------------------
   -- Write_End_Package_Header --
   ------------------------------
   -- Write end package header

   procedure Write_End_Package_Header (Name : String) is
   begin
      M2OS_IO.Write_Line ("end " & Name & ";");
   end Write_End_Package_Header;

   ----------------------------
   -- Write_Procedure_Header --
   ----------------------------
   -- Write procedure header

   procedure Write_Procedure_Header (Name : String) is
   begin
      M2OS_IO.New_Line;
      --M2OS_IO.Write_Indentation;
      M2OS_IO.Write_Line ("procedure " & Name & ";");
      --M2OS_IO.Remove_Indentation;
   end Write_Procedure_Header;

   ---------------------------------
   -- Write_Procedure_Body_Header --
   ---------------------------------
   -- Write procedure body header

   procedure Write_Procedure_Body_Header (Name : String) is
   begin
      M2OS_IO.New_Line;
      --M2OS_IO.Write_Indentation;
      M2OS_IO.Write_Line ("procedure " & Name & " is");
   end Write_Procedure_Body_Header;

   ------------------------------
   -- Write_End_Procedure_Body --
   ------------------------------
   -- Write end procedure header for a task

   procedure Write_End_Procedure_Body (Name : String) is
   begin
      M2OS_IO.Write_Line ("end " & Name &";");
      --M2OS_IO.Remove_Indentation;
   end Write_End_Procedure_Body;

end M2os_Processing.Helper;
