pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with stdint_h;
with System;
with stdarg_h;
with stddef_h;

package stdio_h is

   --  unsupported macro: stdin (__iob[0])
   --  unsupported macro: stdout (__iob[1])
   --  unsupported macro: stderr (__iob[2])
   EOF : constant := (-1);  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:314
   --  arg-macro: procedure fdev_set_udata (stream, u)
   --    do { (stream).udata := u; } while(0)
   --  arg-macro: function fdev_get_udata (stream)
   --    return (stream).udata;
   --  arg-macro: procedure fdev_setup_stream (stream, p, g, f)
   --    do { (stream).put := p; (stream).get := g; (stream).flags := f; (stream).udata := 0; } while(0)
   --  arg-macro: procedure FDEV_SETUP_STREAM (p, g, f)
   --    { .put := p, .get := g, .flags := f, .udata := 0, }
   --  arg-macro: function fdev_close ()
   --    return (void)0;
   --  arg-macro: procedure putc (__c, __stream)
   --    fputc(__c, __stream)
   --  arg-macro: procedure putchar (__c)
   --    fputc(__c, stdout)
   --  arg-macro: procedure getc (__stream)
   --    fgetc(__stream)
   --  arg-macro: procedure getchar ()
   --    fgetc(stdin)
   --  arg-macro: procedure clearerror (s)
   --    do { (s).flags &= ~(__SERR or __SEOF); } while(0)
   --  arg-macro: function feof (s)
   --    return (s).flags and __SEOF;
   --  arg-macro: function ferror (s)
   --    return (s).flags and __SERR;

   BUFSIZ : constant := 1024;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:948

   SEEK_SET : constant := 0;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:981
   SEEK_CUR : constant := 1;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:982
   SEEK_END : constant := 2;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:983

   type uu_file is record
      buf : Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:245
      unget : aliased unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:246
      flags : aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:247
      size : aliased int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:263
      len : aliased int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:264
      put : access function (arg1 : char; arg2 : access uu_file) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:265
      get : access function (arg1 : access uu_file) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:266
      udata : System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:267
   end record;
   pragma Convention (C_Pass_By_Copy, uu_file);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:244

   subtype FILE is uu_file;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:277

   function fdevopen (uu_put : access function (arg1 : char; arg2 : access FILE) return int; uu_get : access function (arg1 : access FILE) return int) return access FILE;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:419
   pragma Import (C, fdevopen, "fdevopen");

   function fclose (uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:436
   pragma Import (C, fclose, "fclose");

   function vfprintf
     (uu_stream : access FILE;
      uu_fmt : Interfaces.C.Strings.chars_ptr;
      uu_ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:610
   pragma Import (C, vfprintf, "vfprintf");

   function vfprintf_P
     (uu_stream : access FILE;
      uu_fmt : Interfaces.C.Strings.chars_ptr;
      uu_ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:616
   pragma Import (C, vfprintf_P, "vfprintf_P");

   function fputc (uu_c : int; uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:623
   pragma Import (C, fputc, "fputc");

   function putc (uu_c : int; uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:628
   pragma Import (C, putc, "putc");

   function putchar (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:631
   pragma Import (C, putchar, "putchar");

   function printf (uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:651
   pragma Import (C, printf, "printf");

   function printf_P (uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:657
   pragma Import (C, printf_P, "printf_P");

   function vprintf (uu_fmt : Interfaces.C.Strings.chars_ptr; uu_ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:665
   pragma Import (C, vprintf, "vprintf");

   function sprintf (uu_s : Interfaces.C.Strings.chars_ptr; uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:671
   pragma Import (C, sprintf, "sprintf");

   function sprintf_P (uu_s : Interfaces.C.Strings.chars_ptr; uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:677
   pragma Import (C, sprintf_P, "sprintf_P");

   function snprintf
     (uu_s : Interfaces.C.Strings.chars_ptr;
      uu_n : stddef_h.size_t;
      uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:687
   pragma Import (C, snprintf, "snprintf");

   function snprintf_P
     (uu_s : Interfaces.C.Strings.chars_ptr;
      uu_n : stddef_h.size_t;
      uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:693
   pragma Import (C, snprintf_P, "snprintf_P");

   function vsprintf
     (uu_s : Interfaces.C.Strings.chars_ptr;
      uu_fmt : Interfaces.C.Strings.chars_ptr;
      ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:699
   pragma Import (C, vsprintf, "vsprintf");

   function vsprintf_P
     (uu_s : Interfaces.C.Strings.chars_ptr;
      uu_fmt : Interfaces.C.Strings.chars_ptr;
      ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:705
   pragma Import (C, vsprintf_P, "vsprintf_P");

   function vsnprintf
     (uu_s : Interfaces.C.Strings.chars_ptr;
      uu_n : stddef_h.size_t;
      uu_fmt : Interfaces.C.Strings.chars_ptr;
      ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:715
   pragma Import (C, vsnprintf, "vsnprintf");

   function vsnprintf_P
     (uu_s : Interfaces.C.Strings.chars_ptr;
      uu_n : stddef_h.size_t;
      uu_fmt : Interfaces.C.Strings.chars_ptr;
      ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:721
   pragma Import (C, vsnprintf_P, "vsnprintf_P");

   function fprintf (uu_stream : access FILE; uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:726
   pragma Import (C, fprintf, "fprintf");

   function fprintf_P (uu_stream : access FILE; uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:732
   pragma Import (C, fprintf_P, "fprintf_P");

   function fputs (uu_str : Interfaces.C.Strings.chars_ptr; uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:739
   pragma Import (C, fputs, "fputs");

   function fputs_P (uu_str : Interfaces.C.Strings.chars_ptr; uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:744
   pragma Import (C, fputs_P, "fputs_P");

   function puts (uu_str : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:750
   pragma Import (C, puts, "puts");

   function puts_P (uu_str : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:755
   pragma Import (C, puts_P, "puts_P");

   function fwrite
     (uu_ptr : System.Address;
      uu_size : stddef_h.size_t;
      uu_nmemb : stddef_h.size_t;
      uu_stream : access FILE) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:764
   pragma Import (C, fwrite, "fwrite");

   function fgetc (uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:773
   pragma Import (C, fgetc, "fgetc");

   function getc (uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:778
   pragma Import (C, getc, "getc");

   function getchar return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:781
   pragma Import (C, getchar, "getchar");

   function ungetc (uu_c : int; uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:812
   pragma Import (C, ungetc, "ungetc");

   function fgets
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_size : int;
      uu_stream : access FILE) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:824
   pragma Import (C, fgets, "fgets");

   function gets (uu_str : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:831
   pragma Import (C, gets, "gets");

   function fread
     (uu_ptr : System.Address;
      uu_size : stddef_h.size_t;
      uu_nmemb : stddef_h.size_t;
      uu_stream : access FILE) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:842
   pragma Import (C, fread, "fread");

   procedure clearerr (uu_stream : access FILE);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:848
   pragma Import (C, clearerr, "clearerr");

   function feof (uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:859
   pragma Import (C, feof, "feof");

   function ferror (uu_stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:870
   pragma Import (C, ferror, "ferror");

   function vfscanf
     (uu_stream : access FILE;
      uu_fmt : Interfaces.C.Strings.chars_ptr;
      uu_ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:877
   pragma Import (C, vfscanf, "vfscanf");

   function vfscanf_P
     (uu_stream : access FILE;
      uu_fmt : Interfaces.C.Strings.chars_ptr;
      uu_ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:882
   pragma Import (C, vfscanf_P, "vfscanf_P");

   function fscanf (uu_stream : access FILE; uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:890
   pragma Import (C, fscanf, "fscanf");

   function fscanf_P (uu_stream : access FILE; uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:895
   pragma Import (C, fscanf_P, "fscanf_P");

   function scanf (uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:902
   pragma Import (C, scanf, "scanf");

   function scanf_P (uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:907
   pragma Import (C, scanf_P, "scanf_P");

   function vscanf (uu_fmt : Interfaces.C.Strings.chars_ptr; uu_ap : stdarg_h.va_list) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:915
   pragma Import (C, vscanf, "vscanf");

   function sscanf (uu_buf : Interfaces.C.Strings.chars_ptr; uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:923
   pragma Import (C, sscanf, "sscanf");

   function sscanf_P (uu_buf : Interfaces.C.Strings.chars_ptr; uu_fmt : Interfaces.C.Strings.chars_ptr  -- , ...
      ) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:928
   pragma Import (C, sscanf_P, "sscanf_P");

   function fflush (stream : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:940
   pragma Import (C, fflush, "fflush");

   subtype fpos_t is Long_Long_Integer;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:950

   function fgetpos (stream : access FILE; pos : access fpos_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:951
   pragma Import (C, fgetpos, "fgetpos");

   function fopen (path : Interfaces.C.Strings.chars_ptr; mode : Interfaces.C.Strings.chars_ptr) return access FILE;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:952
   pragma Import (C, fopen, "fopen");

   function freopen
     (path : Interfaces.C.Strings.chars_ptr;
      mode : Interfaces.C.Strings.chars_ptr;
      stream : access FILE) return access FILE;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:953
   pragma Import (C, freopen, "freopen");

   function fdopen (arg1 : int; arg2 : Interfaces.C.Strings.chars_ptr) return access FILE;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:954
   pragma Import (C, fdopen, "fdopen");

   function fseek
     (stream : access FILE;
      offset : long;
      whence : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:955
   pragma Import (C, fseek, "fseek");

   function fsetpos (stream : access FILE; pos : access fpos_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:956
   pragma Import (C, fsetpos, "fsetpos");

   function ftell (stream : access FILE) return long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:957
   pragma Import (C, ftell, "ftell");

   function fileno (arg1 : access FILE) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:958
   pragma Import (C, fileno, "fileno");

   procedure perror (s : Interfaces.C.Strings.chars_ptr);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:959
   pragma Import (C, perror, "perror");

   function remove (pathname : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:960
   pragma Import (C, remove, "remove");

   function rename (oldpath : Interfaces.C.Strings.chars_ptr; newpath : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:961
   pragma Import (C, rename, "rename");

   procedure rewind (stream : access FILE);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:962
   pragma Import (C, rewind, "rewind");

   procedure setbuf (stream : access FILE; buf : Interfaces.C.Strings.chars_ptr);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:963
   pragma Import (C, setbuf, "setbuf");

   function setvbuf
     (stream : access FILE;
      buf : Interfaces.C.Strings.chars_ptr;
      mode : int;
      size : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:964
   pragma Import (C, setvbuf, "setvbuf");

   function tmpfile return access FILE;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:965
   pragma Import (C, tmpfile, "tmpfile");

   function tmpnam (s : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdio.h:966
   pragma Import (C, tmpnam, "tmpnam");

end stdio_h;
