----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces;

with Buzzer_Demo.Driver;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.Melodies;

package body Buzzer_Demo.Main_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   --  Next activation time

   Next_Time : RT.Time;
   Period : constant RT.Time_Span := RT.Seconds (10);

   --------------------
   -- Main_Task_Init --
   --------------------

   procedure Main_Task_Init is
   begin
      DIO.Put_Line ("Buzzer_Demo.Main_Task initialization");

      Next_Time := RT.Clock;
   end Main_Task_Init;

   --------------------
   -- Main_Task_Body --
   --------------------

   type Counter is mod 9;
   Loop_Counter : Counter := Counter'First;

   procedure Main_Task_Body is
   begin
      DIO.Put (" Buzzer_Demo: ");
      Buzzer_Demo.Driver.Silent;
      loop
         case Loop_Counter is
            when 0 =>
               DIO.Put_Line ("Tone 1s");

               Buzzer_Demo.Driver.Tone
                 (Frequency_Hz     => Arduino.Melodies.Beep_Frecquency_Hz,
                  Duration_Ms      => 1000,
                  Repeat_Period_Ms => 0);

            when 1 =>
               DIO.Put_Line ("Tone 300ms/700ms repeat");

               Buzzer_Demo.Driver.Tone
                 (Frequency_Hz     => Arduino.Melodies.Beep_Frecquency_Hz,
                  Duration_Ms      => 300,
                  Repeat_Period_Ms => 1_000);

            when 2 =>
               DIO.Put_Line ("scale Adagio");

               Buzzer_Demo.Driver.Play_Melody
                 (Melody   => Arduino.Melodies.Scale'Access,
                  Tempo_Ms => Arduino.Melodies.Adagio,
                  Repeat   => True);

            when 3 =>
               DIO.Put_Line ("scale Andante");

               Buzzer_Demo.Driver.Play_Melody
                 (Melody   => Arduino.Melodies.Scale'Access,
                  Tempo_Ms => Arduino.Melodies.Andante,
                  Repeat   => True);

            when 4 =>
               DIO.Put_Line ("Melody Andante");

               Buzzer_Demo.Driver.Play_Melody
                 (Melody   => Arduino.Melodies.Melody1'Access,
                  Tempo_Ms => Arduino.Melodies.Andante,
                  Repeat   => True);

            when 5 =>
               DIO.Put_Line ("Melody Moderato");

               Buzzer_Demo.Driver.Play_Melody
                 (Melody   => Arduino.Melodies.Melody1'Access,
                  Tempo_Ms => Arduino.Melodies.Moderato,
                  Repeat   => True);

            when 6 =>
               DIO.Put_Line ("Twinkle Twinkle Little Star Andante");

               Buzzer_Demo.Driver.Play_Melody
                 (Melody   => Arduino.Melodies.Twinkle_Twinkle_Little_Star'Access,
                  Tempo_Ms => Arduino.Melodies.Andante,
                  Repeat   => True);

            when 7 =>
               DIO.Put_Line ("Twinkle Twinkle Little Star Moderato");

               Buzzer_Demo.Driver.Play_Melody
                 (Melody   => Arduino.Melodies.Twinkle_Twinkle_Little_Star'Access,
                  Tempo_Ms => Arduino.Melodies.Moderato,
                  Repeat   => True);

            when 8 =>
               DIO.Put_Line ("Happy Birthday");
               Buzzer_Demo.Driver.Play_Melody
                 (Melody   => Arduino.Melodies.Happy_Birthday'Access,
                  Tempo_Ms => Arduino.Melodies.Andante,
                  Repeat   => True);
         end case;

         Loop_Counter := Loop_Counter + 1;

         Next_Time := Next_Time + Period;
         delay until Next_Time;
      end loop;
   end Main_Task_Body;

   -----------------
   -- Buzzer_Task --
   -----------------

   Main_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Main_Task_Init'Unrestricted_Access,
      Body_Ac  => Main_Task_Body'Unrestricted_Access,
      Priority => Buzzer_Demo.Main_Task_Prio);

end Buzzer_Demo.Main_Task;
