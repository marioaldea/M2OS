----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Interfaces;
with System;
--  with Ada.Unchecked_Conversion;

with M2.Kernel.Scheduler;
with M2.HAL;
with M2.Direct_IO;
with M2.Debug;

package body M2.Kernel.Initialization is

   --  Symbols defined in the linker script (Not in Arduino yet)

--     Heap_Start : Integer_Address;
--     pragma Import (C, Heap_Start, "_heap_start");
--     Heap_End : Integer_Address;
--     pragma Import (C, Heap_End, "_heap_end");

--     Data_End : Integer;
--     pragma Import (C, Data_End, "__heap_start");
--     EEPROM_End : Integer;
--     pragma Import (C, EEPROM_End, "__eeprom_end");

   --  Main_Priority. Defined in System.Tasking

--     Main_Priority : System.Priority;
--     pragma Import (C, Main_Priority, "__gl_main_priority");
   --  Priority associated with the environment task. By default, its value is
   --  undefined, and can be set by using pragma Priority in the main program.


   --------------------
   -- Initialization --
   --------------------

   procedure Initialization is
      use type HAL.Stack_Size;
   begin
      Direct_IO.Initialization;

      Direct_IO.Put ("-M2OS-");
      Direct_IO.New_Line;
      pragma Debug (Direct_IO.Put ("Stack size:"));
      pragma Debug (Direct_IO.Put (Integer (HAL.Get_Stack_Max_Size_In_Bytes)));
      pragma Debug (Direct_IO.Put ("-"));
      HAL.Mark_Stack_Area;
      pragma Debug (Direct_IO.Put (Integer (HAL.Get_Max_Stack_Usage_In_Bytes)));
      pragma Debug (Direct_IO.New_Line);
      pragma Assert (HAL.Get_Current_Stack_Size_In_Bytes <=
                       HAL.Get_Stack_Max_Size_In_Bytes);
      pragma Assert (HAL.Get_Stack_Max_Size_In_Bytes <=
                       HAL.Get_Current_Stack_Size_In_Bytes +
                         HAL.Get_Current_Stack_Margin_In_Bytes);


      HAL.Initialization (Scheduler.Tick_Handler'Access);

      pragma Debug (M2.Debug.Assert (not HAL.Are_Interrupts_Enabled));

      HAL.Enable_Interrupts;
   end Initialization;

begin
   Initialization;
end M2.Kernel.Initialization;
