----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Real_Time;

with M2.Direct_IO;

package body Two_Tasks_Entry_2 is
   package DIO renames M2.Direct_IO;
   use type Ada.Real_Time.Time;

   --------
   -- PO --
   --------

   protected PO is
      entry Wait;

      procedure Increment;

   private
      Value : Natural := 0;
      Open : Boolean := False;
   end PO;

   protected body PO is

      entry Wait when Open is
      begin
         DIO.Put ("-Wait-");
         Value := 0;
         Open := False;
      end Wait;

      procedure Increment is
      begin
         DIO.Put ("-Incr-");
         Value := Value + 1;
         Open := True;
      end Increment;
   end PO;

   ---------------
   -- Main_Task --
   ---------------

   task Main_Task with Priority => 10;

   task body Main_Task is

      Next_Time : Ada.Real_Time.Time;
      Period : constant Ada.Real_Time.Time_Span := Ada.Real_Time.Seconds (2);

   begin
      Next_Time := Ada.Real_Time.Clock;
      loop
         DIO.Put ("-Main-");
         if Wait_In_Main then
            Two_Tasks_Entry_2.PO.Wait;

         else
            Two_Tasks_Entry_2.PO.Increment;
         end if;
         DIO.Put ("-MainDelay-");
         Next_Time := Next_Time + Period;
         delay until Next_Time;
      end loop;
   end Main_Task;


   ----------------
   -- Other_Task --
   ----------------

   task Other_Task with Priority => 9;

   task body Other_Task is

      Next_Time : Ada.Real_Time.Time;

      Period : constant Ada.Real_Time.Time_Span := Ada.Real_Time.Seconds (3);
   begin
      Next_Time := Ada.Real_Time.Clock;

      loop
         DIO.Put ("-Other-");

         if Wait_In_Main then
            PO.Increment;

         else
            PO.Wait;
         end if;

         DIO.Put ("-OtherDelay-");
         Next_Time := Next_Time + Period;
         delay until Next_Time;
      end loop;
   end Other_Task;

end Two_Tasks_Entry_2;
