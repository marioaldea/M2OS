#!/bin/sh


echo "\n == Test summary =="
total_tests=0
passed_tests=0
known_bugs=0

for f in *.out ; do
  total_tests=$(expr $total_tests + 1)
  echo -n "Test $f:"

  if grep -q "Known Bug" "$f" ; then
     known_bugs=$(expr $known_bugs + 1)
     echo -n " (Known Bug) "
  fi

  if grep -q "Test OK" "$f" ; then
     if grep -q "rwmem.cpp" "$f" ; then
        echo "   *** Invalid memory access ***"
     else
        passed_tests=$(expr $passed_tests + 1)
        echo "   PASSED"
     fi
  else
     echo "   *** FAILED ***"
  fi
done

echo "\nTotal tests: $total_tests"
echo "     Passed: $passed_tests"
echo "     Failed: $(expr $total_tests - $passed_tests) ($known_bugs Known bugs)"
echo "----------------------------------------"

exit 0
