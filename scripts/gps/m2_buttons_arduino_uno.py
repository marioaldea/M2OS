# Arduino Uno support for M2OS.
# Buttons for debugging and running on simulavr and for running on board.
# Uses de build actions defined in the m2_build_actions_arduino_uno.py script.

import GPS
from gps_utils.console_process import Console_Process
import os
import os.path
# import time
import sys


def on_gps_started(hook_name):
    if GPS.Project.get_attribute_as_string(GPS.Project.root(),
                                           "target") != "avr":
        return  # Only for avr target

    build_targets = GPS.Project.get_attribute_as_list(GPS.Project.root(),
                                                      "main")

    # Debug on simulavr:
    # Define a group button and an action for each main program
    i = 1
    for target in build_targets:
        action = "Debug " + target + " on simulavr"
        action_xml = (
            "<action name='" + action +
            "' category='Build' output='Messages' show-command='false'>" +
            "<shell lang='python'>m2_buttons_arduino_uno.debug_on_simulavr('" +
            str(i) + "')</shell>  </action>")
        # print "action:", action, "action_xml:", action_xml
        # define the action
        GPS.parse_xml(action_xml)
        # create the button associated with the action
        GPS.Action(action).button(label=action, group='Debug on simulavr',
                                  icon='gps-emulatorloading-debug')
        i = i + 1

    # Run on simulavr:
    # Define a group button and an action for each main program
    i = 1
    for target in build_targets:
        action = "Run " + target + " on simulavr"
        action_xml = (
            "<action name='" + action +
            "' category='Build' output='Messages' show-command='false'>" +
            "<shell lang='python'>m2_buttons_arduino_uno.run_on_simulavr('" +
            str(i) + "')</shell>  </action>")
        # print "action:", action, "action_xml:", action_xml
        # define the action
        GPS.parse_xml(action_xml)
        # create the button associated with the action
        GPS.Action(action).button(label=action, group='Run on simulavr',
                                  icon='gps-emulatorloading-run')
        i = i + 1

    # Run on board: Define a group button and an action for each main program
    i = 1
    for target in build_targets:
        action = "Run " + target + " on board"
        action_xml = (
            "<action name='" + action +
            "' category='Build' output='Messages' show-command='false'>" +
            "<shell lang='python'>m2_buttons_arduino_uno.run_on_board('" +
            str(i) + "')</shell>  </action>")
        # print "action:", action, "action_xml:", action_xml
        # define the action
        GPS.parse_xml(action_xml)
        # create the button associated with the action
        GPS.Action(action).button(label=action, group='Run on board',
                                  icon='gps-boardloading-debug')
        i = i + 1


GPS.Hook("gps_started").add(on_gps_started)


def debugger_state_changed(name, debugger, new_state):
    """ End simulavr when debugger is terminated """
    # print "new_state:", new_state
    if new_state == "none":
        # print "Finishing simulavr process:", new_state
        GPS.Hook("debugger_state_changed").remove(debugger_state_changed)
        simulavr_proc.kill()


def exec_command(command, console_name):
    print "Console_Process_Dont_Close on_exit"
    f = os.popen(command)
    console = GPS.Console(console_name)
    for l in f.readlines():
        console.write(l)


class Console_Process_Dont_Close(Console_Process):
    """ Same as Console_Process but do not close when process finishes """
    def on_exit(self, status, unmatched_output):
        print "Console_Process_Dont_Close on_exit"


def get_target(target_num):
    """target_num: position of the target in the main attribute of the
                   proyect (first target is in position 1)"""
    return GPS.Project.get_attribute_as_list(GPS.Project.root(),
                                             "main")[int(target_num) - 1]


def build_main(target_num):
    """Build the main program
       target_num: position of the target in the main attribute of the
                   proyect (first target is in position 1)
       return: exec_file name (without .adb)
       throws AssertionError if build fails"""
    target = get_target(target_num)

    exec_file = os.path.splitext(target)[0]  # without .adb

    os.system("rm -f " + exec_file)

    # Build the target
    GPS.execute_action("Build Main Number " + target_num)

    if not os.path.exists(exec_file):
        raise AssertionError("Building failed")

    return exec_file


def debug_on_simulavr(target_num):
    """Debug on simulavr Arduino simulator
       target_num: position of the target in the main attribute of the
                   proyect (first target is in position 1)"""
    exec_file = build_main(target_num)

    # Execute simulavr
    global simulavr_proc
    simulavr_proc = Console_Process_Dont_Close(
        "simulavr -d atmega328 -W 0xc6,- " +
        " -f " + exec_file + " -g")

    # Execute the debugger
    print "Executing debugger for", exec_file
    GPS.Hook("debugger_state_changed").add(debugger_state_changed)
    debugger = GPS.Debugger.spawn(GPS.File(exec_file),
                                  remote_target='localhost:1212',
                                  remote_protocol='remote')
    debugger.send("del")
    debugger.send("br _ada_" + exec_file)
    debugger.send("source " +
                  GPS.Project("global_switches").
                  get_attribute_as_string("Project_Dir") + "gdb_commands.txt")


def run_on_simulavr(target_num):
    """Run on simulavr Arduino simulator
       target_num: position of the target in the main attribute of the
                   proyect (first target is in position 1)"""
    exec_file = build_main(target_num)

    # Execute simulavr
    print "Runnin program:", exec_file
    simulavr_proc = Console_Process_Dont_Close(
        "simulavr -d atmega328 -W 0xc6,- " +
        " -T m2__end_execution__end_execution -f " +
        exec_file)
    # exec_command(
    #    "simulavr -d atmega328 -W 0xc6,- " +
    #    " -T m2__end_execution__end_execution -f " +
    #    exec_file,
    #    "simulavr " + exec_file)


def run_on_board(target_num):
    """Run on Arduino board
       target_num: position of the target in the main attribute of the
                   proyect (first target is in position 1)"""
    exec_file = build_main(target_num)

    # Generate hex file
    generate_hex_proc = GPS.Process(
        "avr-objcopy -O ihex -R .eeprom " +
        exec_file + " " + exec_file + ".hex")
    generate_hex_proc.get_result()  # Wait for process to finish

    # Load to Arduino
    print "Runnin program:", exec_file, "on board"
    # avrdude_proccess = Console_Process_Dont_Close(
    #    "avrdude -F -V -c arduino -p ATMEGA328P -P /dev/ttyACM0 " +
    #    "-b 115200 -U flash:w:" + exec_file + ".hex")

    existsACM0 = os.path.exists('/dev/ttyACM0')
    existsUSB0 = os.path.exists('/dev/ttyUSB0')
    if existsACM0:
        ttyfile = '/dev/ttyACM0'
    elif existsUSB0:
        ttyfile = '/dev/ttyUSB0'
    else:
        raise AssertionError(
            "Error: neither /dev/ttyACM0 nor /dev/ttyUSB0 exist")
    avrdude_process = Console_Process(
    # avrdude_process = GPS.Process(
    # avrdude_proccess = Console_Process_Dont_Close(
        "avrdude -F -V -c arduino -p ATMEGA328P -P " + ttyfile +
        " -b 115200 -U flash:w:" + exec_file + ".hex")
    # time.sleep(6)
    avrdude_process.get_result()  # Wait for process to finish

    # Open console
    stty_process = GPS.Process(
        "stty -F " + ttyfile + " cs8 19200 ignbrk -brkint -icrnl " +
        "-imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe " +
        "-echok -echoctl -echoke noflsh -ixon -crtscts")
    stty_process.get_result()  # Wait for process to finish

    cat_proc = Console_Process_Dont_Close("cat " + ttyfile)
    #Console_Process("cat /dev/ttyACM0")
