----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Demo program for the "Elechouse Voice Recognition V3"
--  https://www.elechouse.com/elechouse/index.php?main_page=product_info&cPath&products_id=2254
--  https://github.com/kksjunior/Voice-Recognition-with-Elechouse-V3/blob/master
--
--     ------------------------                 -----------------------
--     |                      |                 |                     |
--     |                   GND|-----------------|GND                  |
--     |                   VCC|-----------------|5V                   |
--     |                   RXD|-----------------|D3                   |
--     |                   TXD|-----------------|D2                   |
--     |      Elechouse       |                 |         Arduino Uno |
--     | Voice Recognition V3 |                 |                     |
--     |                      |                 |                     |
--     ------------------------                 -----------------------

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino.Voice_Recognition;

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure Voice_Recognition_Demo is
   package DIO renames M2.Direct_IO;

   use type Arduino.Digital_Pin_Out_Value, Interfaces.C.int;
   
   package VR is new Arduino.Voice_Recognition.VR_Module (Receive_Pin  => 2,
                                                          Transmit_Pin => 3);
   
   subtype Records_To_Load is 
     Arduino.Voice_Recognition.VR_Record range 0 .. 4;
   
   Ret : Interfaces.C.int;
   Record_Buf : aliased Arduino.Voice_Recognition.VR_Buffer;
   
   procedure Print_Buffer (Buf : Arduino.Voice_Recognition.VR_Buffer) is
   begin
      DIO.Put ("Record_Num:");
      DIO.Put (Integer (Buf.Record_Num));
      for I in 1 .. Buf.Signature_Length loop
         DIO.PutChar (Character (Buf.Signature (Interfaces.C.Size_T (I))));
      end loop;
      DIO.New_Line;
   end Print_Buffer;       

begin
   DIO.Put_Line ("VR demo");
   
   -- Initialize VR module
   
   VR.Begin_VR (9600);
   Ret := VR.Clear;
   if Ret /= 0 then
      DIO.Put_Line ("Error clearing VR");
   end if;
   
   -- Load records to be used (words to be recognized)
   
   for Rec in Records_To_Load loop      
      Ret := VR.Load (Rec);
      if Ret < 0 then
         DIO.Put ("Error loading rec:");
         DIO.Put (Integer (Rec));
         DIO.New_Line;
      end if;
   end loop;
   
   loop
      DIO.Put_Line ("Listening...");
      Ret := VR.Recognize (Record_Buf'Access);
      
      if Ret > 0 then
         Print_Buffer (Record_Buf);
      else
         DIO.Put_Line ("  Not detected.");
      end if;
           
      Arduino.C_Delay (500);
   end loop;
end Voice_Recognition_Demo;
