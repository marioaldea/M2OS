----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with System;
with Interfaces;
with Ada.Real_Time;
with Ada.Real_Time.Timing_Events;
with Arduino.Servo;
with M2.Direct_IO;

package body Arduino.Coordinated_Servos is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;
   package TE renames Ada.Real_Time.Timing_Events;

   use type Interfaces.Unsigned_8, RT.Time;

   Servos : array (Servo_Id) of aliased Arduino.Servo.Servo :=
     (others => Arduino.Servo.New_Servo);

   Event_Is_Active : Boolean := False;
   Event : aliased TE.Timing_Event;
   Event_Activation_Time : RT.Time;
   Event_Period : RT.Time_Span;

   Current_Trayectory : access constant Trayectory;
   Current_Position : Interfaces.Unsigned_8;
   Repeat_Trayectory : Boolean;

   --------
   -- PO --
   --------

   protected PO with Priority => System.Interrupt_Priority'Last is
      procedure Event_Handler (Event : in out TE.Timing_Event);
   end PO;

   protected body PO is
      procedure Event_Handler (Event : in out TE.Timing_Event) is
      begin
         DIO.Put_Line ("-Event_Handler-");
         if not Event_Is_Active then
            return;
         end if;

         Move_To (Current_Trayectory (Current_Position));

         if Current_Position = Current_Trayectory'Last and then
           not Repeat_Trayectory then
            Event_Is_Active := False;

         else
            Current_Position :=
              (if Current_Position = Current_Trayectory'Last
               then Current_Trayectory'First
               else Current_Position + 1);


            Event_Activation_Time := Event_Activation_Time + Event_Period;
            TE.Set_Handler (Event,
                            Event_Activation_Time,
                            Event_Handler'Unrestricted_Access);
         end if;
      end Event_Handler;
   end PO;

   ------------
   -- Attach --
   ------------

   procedure Attach (Pins : Servo_Pins) is
      Ret : Arduino.Servo.Attach_Ret;
   begin
      for Id in Servo_Id'Range loop
         Ret := Arduino.Servo.Attach (Servos (Id)'Access, Pins (Id));
      end loop;
   end Attach;

   -----------------------
   -- Follow_Trayectory --
   -----------------------

   procedure Follow_Trayectory (Tray_Ac : access Trayectory;
                                Cycle_Time : Ada.Real_Time.Time_Span;
                                Repeat : Boolean) is
   begin
      Current_Trayectory := Tray_Ac;
      Current_Position := Tray_Ac'First;
      Event_Is_Active := True;
      Event_Period := Cycle_Time;
      Repeat_Trayectory := Repeat;

      TE.Set_Handler (Event,
                      RT.Clock,
                      PO.Event_Handler'Access);
   end Follow_Trayectory;


   -------------
   -- Move_To --
   -------------

   procedure Move_To (Pos : Position) is
   begin
      --DIO.Put_Line ("-Move_To-" & Pos (1)'img);
      for Id in Servo_Id'Range loop
         DIO.Put_Line ("-Move_To-" & Pos (Id)'Img);
         Arduino.Servo.Write (Servos (Id)'Access, Pos (Id));
      end loop;
   end Move_To;

   ----------
   -- Stop --
   ----------

   procedure Stop is
   begin
      Event_Is_Active := False;
   end Stop;

--     procedure End_Control is
--     begin
--        Detach
--     end End_Control;

end Arduino.Coordinated_Servos;
