----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--  Test for periodic tasks created using the generic package
--  AdaX_Dispatching_Stack_Sharing.Periodic_Task.
--  Also tests the use of 'Img and 'Image attributes.
with Ada.Real_Time;

package Periodic_Tasks is

   procedure Periodic_Task_1_Init;
   procedure Periodic_Task_1_Body;
   Periodic_Task_1_Period : Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (25);
   Periodic_Task_1_Job_Counter : Integer := 0;
   Periodic_Task_1_Priority : constant := 5;

   procedure Periodic_Task_2_Init;
   procedure Periodic_Task_2_Body;
   Periodic_Task_2_Period : Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);
   Periodic_Task_2_Job_Counter : Integer := 0;
   Periodic_Task_2_Priority : constant := 5;

   procedure Periodic_Task_3_Init;
   procedure Periodic_Task_3_Body;
   Periodic_Task_3_Period : Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (75);
   Periodic_Task_3_Job_Counter : Integer := 0;
   Periodic_Task_3_Priority : constant := 5;

end Periodic_Tasks;
