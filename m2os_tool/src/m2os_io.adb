with Ada.Strings.Fixed; use Ada.Strings; -- Trim utility
with Output;                             -- Write utilities

package body M2OS_IO is

   -----------------
   -- Create_File --
   -----------------

   function Create_File (Filename : String) return File_Type is
      The_File : OS_Lib.File_Descriptor;
   begin
      -- Create file in text mode
      The_File := OS_Lib.Create_File (Name  => Filename,
                                      Fmode => OS_Lib.Text);

      Output.Set_Output (The_File);
      return File_Type (The_File);
   end Create_File;

   ----------------
   -- Close_File --
   ----------------

   procedure Close_File (File : File_Type) is
   begin
      OS_Lib.Close (FD => OS_Lib.File_Descriptor (File));
   end Close_File;

   -----------------
   -- Backup_File --
   -----------------

   function Backup_File (Filename : String) return Boolean is
      Is_Done : Boolean := False;
   begin
      OS_Lib.Rename_File (Old_Name => Filename,
                          New_Name => Filename & ".orig",
                          Success  => Is_Done);
      return Is_Done;
   end Backup_File;

   ----------------
   -- Set_Output --
   ----------------

   procedure Set_Output (File : File_Type) is
   begin
      Output.Set_Output (FD => OS_Lib.File_Descriptor (File));
   end Set_Output;

   -----------------------
   -- Write_Indentation --
   -----------------------

   procedure Write_Indentation is
   begin
      Output.Indent;
   end Write_Indentation;

   ------------------------
   -- Remove_Indentation --
   ------------------------

   procedure Remove_Indentation is
   begin
      Output.Outdent;
   end Remove_Indentation;

   ----------------
   -- Write_Line --
   ----------------

   procedure Write_Line (Sentence : String) is
   begin
      Output.Write_Line (Sentence);
   end Write_Line;

   --------------
   -- New_Line --
   --------------

   procedure New_Line is
   begin
      Output.Write_Line ("");
   end New_Line;

   ----------
   -- Trim --
   ----------

   function Trim (Sentence: String) return String is
   begin
      return Ada.Strings.Fixed.Trim (Source => Sentence,
                                     Side   => Both);
   end Trim;

end M2OS_IO;
