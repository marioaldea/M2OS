----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Real_Time;

with M2.Direct_IO;

with Tests_Reports;

package body Suspension_Object_Closed_2 is
   package DIO renames M2.Direct_IO;
   package STC renames Ada.Synchronous_Task_Control;

   use type Ada.Real_Time.Time;

   ----------------
   -- Other_Task --
   ----------------
   task Other_Task;

   task body Other_Task is
      Loop_Counter : Integer := 0;

      Next_Time : Ada.Real_Time.Time;
      Period : constant Ada.Real_Time.Time_Span :=
        Ada.Real_Time.Milliseconds (50);
   begin
      Next_Time := Ada.Real_Time.Clock + Period;
      delay until Next_Time;

      loop
         DIO.Put ("-Other-");

         Tests_Reports.Assert (Step_Counter = Loop_Counter * 2 + 1);
         Step_Counter := Step_Counter + 1;

         Loop_Counter := Loop_Counter + 1;
         Tests_Reports.Assert (Loop_Counter <= Num_Of_Loops);

         STC.Set_True (Suspension_Object);

         Next_Time := Next_Time + Period;
         delay until Next_Time;
      end loop;
   end Other_Task;

end Suspension_Object_Closed_2;
