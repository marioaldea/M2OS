with M2.Direct_IO;

package body Protected_Interrupt_Handler.PO is
   package DIO renames M2.Direct_IO;

   --------
   -- PO --
   --------

   protected body PO is

      entry Wait when Is_Open is
      begin
         DIO.Put ("-PO.Wait-");
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         DIO.Put ("-PO.Open-");
         Is_Open := True;
      end Open;
   end PO;

end Protected_Interrupt_Handler.PO;
