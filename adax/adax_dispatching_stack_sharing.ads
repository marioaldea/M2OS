----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with System;
with Ada.Unchecked_Conversion;

with M2.HAL;

package AdaX_Dispatching_Stack_Sharing is
   
   type Initialization_Ac is access procedure;
   type Job_Body_Ac is access procedure;
   function To_Code_Address is
     new Ada.Unchecked_Conversion(Job_Body_Ac, M2.HAL.Code_Address);

   -----------------------------
   -- Task type One_Shot_Task --
   -----------------------------
   
   task type One_Shot_Task (Init_Ac  : Initialization_Ac;
                            Body_Ac  : Job_Body_Ac;
                            Priority : System.Priority) with
     Priority => Priority,
       Storage_Size => 0, --  There is no individual stack for each task
       Secondary_Stack_Size => 0;

end AdaX_Dispatching_Stack_Sharing;
