----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Direct_IO;
with Tests_Reports;

package body Periodic_Tasks is
   package DIO renames M2.Direct_IO;

   --------------------------
   -- Periodic_Task_1_Init --
   --------------------------

   procedure Periodic_Task_1_Init is
   begin
      DIO.Put_Line ("Task_1_Init");

      Periodic_Task_1_Job_Counter := 0;
   end Periodic_Task_1_Init;

   --------------------------
   -- Periodic_Task_1_Body --
   --------------------------

   procedure Periodic_Task_1_Body is
   begin
      Periodic_Task_1_Job_Counter := Periodic_Task_1_Job_Counter + 1;

      DIO.Put_Line ("Task_1_Body" & Natural'Image (Periodic_Task_1_Job_Counter));

      Tests_Reports.Assert_Equal (Val1   => Periodic_Task_3_Job_Counter,
                                  Val2   => Periodic_Task_1_Job_Counter / 3,
                                  Margin => 1);
      Tests_Reports.Assert_Equal (Val1   => Periodic_Task_2_Job_Counter,
                                  Val2   => Periodic_Task_1_Job_Counter / 2,
                                  Margin => 1);
      if Periodic_Task_1_Job_Counter = 20 then
         Tests_Reports.Test_OK;
      end if;
   end Periodic_Task_1_Body;

   --------------------------
   -- Periodic_Task_2_Init --
   --------------------------

   procedure Periodic_Task_2_Init is
   begin
      DIO.Put_Line ("Task_2_Init");

      Periodic_Task_2_Job_Counter := 0;
   end Periodic_Task_2_Init;

   --------------------------
   -- Periodic_Task_2_Body --
   --------------------------

   procedure Periodic_Task_2_Body is
   begin
      Periodic_Task_2_Job_Counter := Periodic_Task_2_Job_Counter + 1;

      DIO.Put_Line ("Task_2_Body" & Periodic_Task_2_Job_Counter'Img);
   end Periodic_Task_2_Body;

   --------------------------
   -- Periodic_Task_3_Init --
   --------------------------

   procedure Periodic_Task_3_Init is
   begin
      DIO.Put_Line ("Task_3_Init");

      Periodic_Task_3_Job_Counter := 0;
   end Periodic_Task_3_Init;

   --------------------------
   -- Periodic_Task_3_Body --
   --------------------------

   procedure Periodic_Task_3_Body is
   begin
      Periodic_Task_3_Job_Counter := Periodic_Task_3_Job_Counter + 1;

      DIO.Put_Line ("Task_3_Body" & Periodic_Task_3_Job_Counter'Img);
   end Periodic_Task_3_Body;

end Periodic_Tasks;
