----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
limited with M2.Kernel.TCBs;
with M2.HAL;

package M2.Kernel.CPU_Time is

   CPU_Time_Last : constant := 
     (if M2.Account_CPU_Time'First then HAL.HWTime'Last else 0);
   CPU_Time_Size : constant := 
     (if M2.Account_CPU_Time'First then HAL.HWTime'Size else 0);
   type CPU_Time is new HAL.HWTime range 0 .. CPU_Time_Last;
   for CPU_Time'Size use CPU_Time_Size;
   
   procedure Update_CPU_Time_Of_Running_Thread;
   --  Updates the accounted CPU time for the running thread adding the
   --  time elapsed since the last update operation.
   
   procedure Update_Idle_CPU_Time;
   --  Updates the accounted CPU time for the running thread adding the
   --  time elapsed since the last update operation.
   
   function Thread_CPU_Time (Th_Ac : TCBs.TCB_Ac) return CPU_Time;
   --  Returns the CPU time of the thread.
     
   function Idle_CPU_Time return CPU_Time;
   --  Returns the time the system has remained idle (without any thread to
   --  execute.

end M2.Kernel.CPU_Time;
