pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package ctype_h is

   function isalnum (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:74
   pragma Import (C, isalnum, "isalnum");

   function isalpha (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:81
   pragma Import (C, isalpha, "isalpha");

   function isascii (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:88
   pragma Import (C, isascii, "isascii");

   function isblank (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:94
   pragma Import (C, isblank, "isblank");

   function iscntrl (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:100
   pragma Import (C, iscntrl, "iscntrl");

   function isdigit (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:106
   pragma Import (C, isdigit, "isdigit");

   function isgraph (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:112
   pragma Import (C, isgraph, "isgraph");

   function islower (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:118
   pragma Import (C, islower, "islower");

   function isprint (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:124
   pragma Import (C, isprint, "isprint");

   function ispunct (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:131
   pragma Import (C, ispunct, "ispunct");

   function isspace (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:139
   pragma Import (C, isspace, "isspace");

   function isupper (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:145
   pragma Import (C, isupper, "isupper");

   function isxdigit (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:152
   pragma Import (C, isxdigit, "isxdigit");

   function toascii (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:173
   pragma Import (C, toascii, "toascii");

   function tolower (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:179
   pragma Import (C, tolower, "tolower");

   function toupper (uu_c : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/ctype.h:185
   pragma Import (C, toupper, "toupper");

end ctype_h;
