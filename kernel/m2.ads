----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

pragma Restrictions (No_Elaboration_Code);

pragma Discard_Names;

package M2 is
   pragma Pure;
   --  pragma No_Elaboration_Code_All;

   Priority_Max : constant := 31;

   type Trace_Levels is (Warning, Trace);

   Trace_Enabled : constant array (Trace_Levels) of Boolean :=
     (Warning => True,
      Trace   => False);
   --  Requires M2OS to be compiled with "-gnata" flag

   subtype Use_Timing_Events is Boolean range True .. True;
   subtype Allow_Timing_Events_Cancellation is Boolean range True .. True;

   subtype Account_CPU_Time is Boolean range True .. True;
   --  Affects the value of Thread_T_Size_In_Bytes in system.ads

end M2;
