----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with M2.Direct_IO;

with Round_Robin_2;

with Tests_Reports;

package body Round_Robin_3 is
   package DIO renames M2.Direct_IO;
   package STC renames Ada.Synchronous_Task_Control;

   ------------------
   -- Other_Task_3 --
   ------------------

   task Other_Task_3;

   task body Other_Task_3 is
      Loop_Counter : Integer := 0;
   begin
      DIO.Put ("-Other_3-");
      STC.Suspend_Until_True (Suspension_Object_3);
      loop
         DIO.Put ("-Other_3-");
         DIO.Put (Integer (Round_Robin_2.Step_Counter));
         DIO.Put (Loop_Counter);

         if Loop_Counter mod 2 = 0 then
            Tests_Reports.Assert (Round_Robin_2.Step_Counter =
                                    Loop_Counter * 3 + 1);
         else
            Tests_Reports.Assert (Round_Robin_2.Step_Counter =
                                    Loop_Counter * 3 + 2);
         end if;


         Round_Robin_2.Step_Counter := Round_Robin_2.Step_Counter + 1;

         Loop_Counter := Loop_Counter + 1;
         Tests_Reports.Assert (Loop_Counter <= Round_Robin_2.Num_Of_Loops);

         STC.Suspend_Until_True (Suspension_Object_3);
      end loop;
   end Other_Task_3;

end Round_Robin_3;
