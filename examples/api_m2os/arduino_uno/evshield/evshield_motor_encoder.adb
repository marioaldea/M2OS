
----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on motor_encoder.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- setup for this example:
-- attach external power to EVShield.
-- attach 4 motors to motor ports
----------------------------------------------------------------------------

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Arduino.EVShield.Motors;
with SHDefines_h; use SHDefines_h;
with Interfaces.C.Extensions;
with stdint_h;

procedure evshield_motor_encoder is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, Interfaces.C.Extensions.bool;

   -- Program variables
   time : Arduino.Time_MS;
   aux : Interfaces.C.Extensions.bool;
   encoder1: stdint_h.int32_t;
   encoder2: stdint_h.int32_t;

   -- Bank A Motor 1
   package BAM1 is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM1);
--     -- Bank A Motor 2
   package BAM2 is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM2);

begin

   DIO.Put_Line ("Turn the motor shaft to see changing encoder values.");
   DIO.Put_Line ("Press Left Arrow button to reset motor encoder.");
   DIO.Put_Line ("---------------------------");
   DIO.Put_Line ("Press GO button to continue");
   DIO.Put_Line ("---------------------------");

   -- Wait until user presses GO button to continue the program
   while not Arduino.EVShield.Get_Button_State(Arduino.EVShield.BTN_GO) loop
      time := Arduino.Millis;
      if time mod 1000 < 3 then
         DIO.Put_Line ("Press GO button to continue");
      end if;
   end loop;

   -- reset motors.
   aux := BAM1.Motor_Reset;
--     aux := BAM2.Motor_Reset;

   loop
      -- get motor positions
      encoder1 := BAM1.Motor_Get_Encoder_Position;
      encoder2 := BAM2.Motor_Get_Encoder_Position;

      -- Print results
      DIO.Put("Encoder 1: "); DIO.Put(Integer(encoder1));
      DIO.Put(" Encoder 2: "); DIO.Put(Integer(encoder2)); DIO.New_Line;

      -- wait for some time
      Arduino.C_Delay (1000);

   end loop;
end evshield_motor_encoder;
