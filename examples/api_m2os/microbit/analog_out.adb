with MicroBit.IOs;
with MicroBit.Time;
with Ada.Text_IO;

with AdaX_Dispatching_Stack_Sharing;

procedure Analog_Out is
begin
   Ada.Text_IO.Put_Line ("Analog Out working in M2OS!");
   --  Loop forever
   loop

      --  Loop for value between 0 and 128
      for Value in MicroBit.IOs.Analog_Value range 0 .. 128 loop

         --  Write the analog value of pin 0
         MicroBit.IOs.Write (8, Value);

         --  Wait 20 milliseconds
         MicroBit.Time.Delay_Ms (20);
      end loop;
   end loop;
end Analog_Out;
