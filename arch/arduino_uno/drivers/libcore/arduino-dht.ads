----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of the DTH library DTH.h (Grove Temperature And Humidity Sensor)
--  https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor
--
----------------------------------------------------------------------------
with Interfaces.C;

with Arduino;

package Arduino.DHT is

   type DHT_Type is (DHT10, DHT11, DHT21, DHT22);
   for DHT_Type use (DHT10 => 10, DHT11 => 11, DHT21 => 21, DHT22 => 22);
   for DHT_Type'Size use Interfaces.Unsigned_8'Size;

   type Measurement is record
      Humidity : Float;
      Temperature : Float;
   end record;
   for Measurement'Size use Float'Size * 2;

   Sizeof_DHT : Integer with
     Import, Convention => C,
     External_Name => "sizeof_DHT";
   --  Defined in 'sizes_arduino_types.cpp' (created by Makefile)
   --  Takes the value of -1 if the library is not included.

   DHT_Size_In_Bytes : constant := 14;

   type DHT_Filling is
     array(1 .. DHT_Size_In_Bytes) of Interfaces.Unsigned_8;

   type DHT is limited record
      Filling: DHT_Filling;
   end record;
   pragma Import (CPP, DHT);
   for DHT'Size use DHT_Size_In_Bytes * 8;
   for DHT'Alignment use Standard'Maximum_Alignment;

   Count_AVR_16MHz : constant := 6; -- Value for 16 MHz(ish) AVR (see DHT.cpp)

   ------------------------
   -- Imported functions --
   ------------------------

   function New_DHT return DHT;
   pragma CPP_Constructor (New_DHT, "_ZN3DHTC1Ev");

   function New_DHT (Pin_Number : Arduino.Digital_Pin;
                     Sensor_Type : DHT_Type;
                     Count : Interfaces.Unsigned_8 := Count_AVR_16MHz)
                     return DHT;
   pragma CPP_Constructor (New_DHT, "_ZN3DHTC1Ehhh");

   procedure Begin_DHT (This : access DHT);
   pragma Import (CPP, Begin_DHT, "_ZN3DHT5beginEv");

   function Read (This : access DHT;
                  TH : access Measurement)
                  return Integer;
   pragma Import (CPP, Read, "_ZN3DHT19readTempAndHumidityEPf");

end Arduino.DHT;
