--------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding for the "Grove - Ultrasonic Ranger" library:
--    https://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/
--
----------------------------------------------------------------------------
with Interfaces.C;

package Arduino.Ultrasonic_Ranger_Grove is

   generic
      Pin : Arduino.Digital_Pin;
   package Ranger is

      function Measure_In_Centimeters return Interfaces.C.Long
        with Inline;

      function Measure_In_Millimeters return Interfaces.C.Long
        with Inline;

      function Measure_In_Inches return Interfaces.C.Long
        with Inline;

   end Ranger;

   -----------------
   -- Type's size --
   -----------------

   Sizeof_Ultrasonic : Integer with
     Import, Convention => C,
     External_Name => "sizeof_Ultrasonic";
   --  Defined in 'sizes_arduino_types.cpp' (created by Makefile)
   --  Takes the value of -1 if the library is not included.

   Ultrasonic_Size_In_Bytes : constant := 109;

end Arduino.Ultrasonic_Ranger_Grove;
