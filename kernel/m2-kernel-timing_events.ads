----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with M2.HAL;

package M2.Kernel.Timing_Events is
#if not M2_TARGET = "rp2040" then
   pragma Preelaborate;
#end if;

   subtype Time is HAL.HWTime;

   type Timing_Event is limited private;

   type Timing_Event_Ac is access all Timing_Event;

   type Timing_Event_Handler is
     access protected procedure (Event : in out Timing_Event);

   procedure Set_Handler (Event_Ac : Timing_Event_Ac;
                          At_Time : Time;
                          Handler : Timing_Event_Handler)
     with Export => True, Convention => C,
     External_Name => "m2__kernel__timing_events__set_handler";

   --  Not allowed in Ravenscar (pragma Restrictions No_Relative_Delay)
--     procedure Set_Handler_Relative (Event_Ac : Timing_Event_Ac;
--                                     In_Time : Time;
--                                     Handler : Timing_Event_Handler)
--       with Export => True, Convention => C,
--       External_Name => "m2__kernel__timing_events__set_handler_relative";

   function Current_Handler (Event : Timing_Event)
                             return Timing_Event_Handler
     with Export => True, Convention => C,
     External_Name => "m2__kernel__timing_events__current_handler";

   type Boolean_C is new Boolean;
   for Boolean_C'Size use 8;

   procedure Cancel_Handler (Event_Ac  : Timing_Event_Ac;
                             Cancelled : out Boolean_C)
     with Export => True, Convention => C,
     External_Name => "m2__kernel__timing_events__cancel_handler";

   function Time_Of_Event (Event : Timing_Event) return Time
     with Export => True, Convention => C,
     External_Name => "m2__kernel__timing_events__time_of_event";

   procedure Clear_Event (Event_Ac : Timing_Event_Ac);

   function Queue_Head return Timing_Event_Ac;

   procedure Queue_Dequeue_Head;

private

   type Timing_Event is record
      Next : Timing_Event_Ac := null;
      Expiration_Time : HAL.HWTime := HAL.HWTime'First;
      Handler : Timing_Event_Handler := null;
   end record with Alignment => Standard'Maximum_Alignment;

end M2.Kernel.Timing_Events;
