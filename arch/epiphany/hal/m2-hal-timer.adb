----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: David Garci�a Villaescusa (garciavd@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces;
with Interfaces.C;
use Interfaces;

package body M2.HAL.Timer is
   package C renames Interfaces.C;

   function Interrupts_Get_Timer_Value return Interfaces.Unsigned_32
     with
       Import => True,
       convention => C;
   -- Imports function 'interrupts_get_timer_value' from C.
   -- Can be called from Ada.

   function Interrupts_Get_Initial_Timer_Value return Interfaces.Unsigned_32
     with
       Import => True,
       convention => C;
   -- Imports function 'interrupts_get_initial_timer_value' from C.
   -- Can be called from Ada.

   -----------------
   -- Get_Counter --
   -----------------

   function Get_Counter return Timer_Ticks is
   begin
      return Timer_Ticks (Interrupts_Get_Initial_Timer_Value - Interrupts_Get_Timer_Value);
   end Get_Counter;

end M2.HAL.Timer;
