----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with M2.Debug;
with M2.Direct_IO;

package body M2.Kernel.Ready_Queue is

   --  pragma No_Elaboration_Code_All;

   use type HAL.HWTime, TCBs.TCB_Ac, Interfaces.Unsigned_8;

   ---------------------------------------
   -- Ready and Suspended Threads Queue --
   ---------------------------------------

   --  All threads are in this queue.
   --  They are queued in priority order by Enqueue_Thread.
   --  After initialization, threads do not change its position any more:
   --   - Ready threads have an activation time in the past.
   --   - Suspended threads have an activation time in the future.
   --   - Blocked threads (p.e. in a Susp. Obj.) have an activation time equal
   --     to HWTime'Last
   --  The most prioritary thread is the one with higher priority and lower
   --  activation time in the past.

   RQ : TCBs.TCB_Ac;

   ------------------------
   -- Enqueue_New_Thread --
   ------------------------

   procedure Enqueue_New_Thread (Th_Ac : TCBs.TCB_Ac) is
   begin
      pragma Assert (Th_Ac.Activation_Time = 0);

      --  Look for position according to priority

      if RQ = Null or else Th_Ac.Priority >= RQ.Priority then
         Th_Ac.Next := RQ;
         RQ := Th_Ac;

      else
         declare
            Aux : TCBs.TCB_Ac := RQ;
         begin
            loop
               if Aux.Next = Null or else Th_Ac.Priority >= Aux.Next.Priority then
                  Th_Ac.Next := Aux.Next;
                  Aux.Next := Th_Ac;
                  exit;
               end if;
               Aux := Aux.Next;
            end loop;
         end;
      end if;
   end Enqueue_New_Thread;

   -------------------------
   -- Head_Of_Ready_Queue --
   -------------------------

   --  Returns the thread with higher priority and activation time in the past.
   --  If there are more than one active thread at the highest priority, the
   --  one with shorter activation time is returned.

   function Head_Of_Ready_Queue return TCBs.TCB_Ac is
      Aux : TCBs.TCB_Ac := RQ;
      Earlier_Time : HAL.HWTime := HAL.Get_HWTime;
      RT_Ac : TCBs.TCB_Ac := null;
   begin
      pragma Assert (RQ /= null);

      loop
         if Aux.Activation_Time < Earlier_Time or else
           (Aux.Activation_Time = Earlier_Time and then
              (RT_Ac = null or else
               Aux.Order_Counter < RT_Ac.Order_Counter))
         then
            RT_Ac := Aux;
            Earlier_Time := Aux.Activation_Time;
         end if;
         Aux := Aux.Next;
         exit when Aux = null or else
           (RT_Ac /= null and then Aux.Priority < RT_Ac.Priority);
      end loop;

      return RT_Ac;
   end Head_Of_Ready_Queue;

   ----------
   -- Show --
   ----------

   procedure Show (DBG_Level : M2.Trace_Levels) is
      Th_Ac : TCBs.TCB_Ac;
   begin
      if not M2.Trace_Enabled (DBG_Level) then
         return;
      end if;

      Direct_IO.Put ("RQ:");
      Th_Ac := RQ;
      while Th_Ac /= null loop
         TCBs.Show (Th_Ac.all);
         Th_Ac := Th_Ac.Next;
      end loop;
      Direct_IO.Put (":");
   end Show;

end M2.Kernel.Ready_Queue;
