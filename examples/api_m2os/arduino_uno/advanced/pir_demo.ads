----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Uses a Passive Infrared (PIR) Motion Sensor (HC-SR501).
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |            VCC|-----------------|5V                   |
--     | HC-SR501   OUT|-----------------|D5       Arduino Uno |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--

with System;
with Ada.Real_Time;

with Arduino;

package PIR_Demo is
   PIR_Pin : Arduino.Digital_Pin := 5;
   PIR_Task_Prio : System.Priority := 11;
   PIR_Task_Polling_Period_Ms : constant := 100;

   Main_Task_Period_Ms : constant := 1_000;
   Main_Task_Prio : System.Priority := 10;
end PIR_Demo;
