--  Based on: http://playground.arduino.cc/Main/I2cScanner

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure I2C_Scanner is
   package DIO renames M2.Direct_IO;

   Error : Arduino.Wire.Transmission_Result;

   use type Arduino.Wire.Transmission_Result;

begin
   DIO.Put_Line ("I2C_Scanner");

   Arduino.Wire.Begin_Two_Wire (Arduino.Wire.Wire'Access);

   loop
      DIO.Put_Line ("Scanning...");

      for Address in Arduino.Wire.Dev_Address'Range loop

         Arduino.Wire.Begin_Transmission(Arduino.Wire.Wire'Access, Address);
         Error := Arduino.Wire.End_Transmission(Arduino.Wire.Wire'Access);

         if Error = Arduino.Wire.OK then
            DIO.Put (" I2C device found at address:0x");
            DIO.Put (Integer (Address), 16);
            DIO.New_Line;

         elsif Error = Arduino.Wire.Other_Error then
            DIO.Put (" Unknown error at address:0x");
            DIO.Put (Integer (Address), 16);
            DIO.New_Line;
         end if;
      end loop;

      Arduino.C_Delay (2_000);
   end loop;
end I2C_Scanner;
