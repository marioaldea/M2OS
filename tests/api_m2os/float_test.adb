----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--  Test float operations (+, * y /) and 'Img and 'Image

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
with Tests_Reports;

procedure Float_Test  is
   package DIO renames M2.Direct_IO;

   --  Geometric progression (a=1, r=2/3)
   --    1 + 2/3 + 4/9 + 8/27 + ... = a/(1-r) = 1/(1/3) = 3
   Ratio : constant Float := 2.0 / 3.0;
   Number_Of_Terms : constant := 25;
   Limit : constant Float := 3.0;
   Margin : constant := 0.001;

   Sum : Float := 0.0;
   Term : Float;

begin
--  #if M2_TARGET = "arduino_uno" then
--     Tests_Reports.Known_Bug;
--  #end if;
   Term := 1.0;

   for I in 1 .. Number_Of_Terms loop
      Sum := Sum + Term;
      DIO.Put ("Term" & I'Img & " = " & Term'Img);
      DIO.Put_Line ("  Sum =" & Float'Image (Sum));

      Term := Term * Ratio;
   end loop;

   DIO.Put_Line ("  Limit =" & Limit'Img);
   DIO.Put_Line ("  Integer (Sum) =" & Integer'Image (Integer (Sum)));

   Tests_Reports.Assert (Limit - Sum < Margin);

   Tests_Reports.Test_OK;
end Float_Test;
