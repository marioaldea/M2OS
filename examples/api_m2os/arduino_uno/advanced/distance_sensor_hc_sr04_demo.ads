----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of using Ultrasonic Distance Sensor HC-SR04
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |            GND|-----------------|GND                  |
--     |           Echo|-----------------|D2                   |
--     | HC-SR04   Trig|-----------------|D7       Arduino Uno |
--     |            VCC|-----------------|VCC                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--
---------------------------------------------------------------------------
with Arduino;

package Distance_Sensor_HC_SR04_Demo is

   Trigger_Pin : constant Arduino.Digital_Pin := 7;
   Echo_Pin : constant Arduino.Interrupt_Digital_Pin := 2;

end Distance_Sensor_HC_SR04_Demo;
