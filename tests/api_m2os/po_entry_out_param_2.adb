----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Direct_IO;

with AdaX_Dispatching_Stack_Sharing;

with Tests_Reports;

package body PO_Entry_Out_Param_2 is
   package DIO renames M2.Direct_IO;

   --------
   -- PO --
   --------

   protected body PO is

      entry Wait (Data_In : Integer; Data_Out : out Integer) when Is_Open is
      begin
         DIO.Put ("-PO.Wait-");
         DIO.Put (Integer (Step_Counter));
         DIO.Put (Data_In);
         Tests_Reports.Assert (Step_Counter = Data_In);
         Data_Out := Step_Counter + PO.Data;
         DIO.Put (Data_Out);
         Is_Open := False;
      end Wait;

      procedure Open (Data : Integer) is
      begin
         DIO.Put ("-PO.Open-");
         PO.Data := Data;
         Is_Open := True;
      end Open;
   end PO;

   ---------------------
   -- Other_Task_Init --
   ---------------------

   procedure Other_Task_Init is
   begin
      DIO.Put ("-Other Init-");
   end Other_Task_Init;

   Loop_Counter : Integer := 0;
   Data_Out : Integer := 1 + PO_Data;

   ---------------------
   -- Other_Task_Body --
   ---------------------

   procedure Other_Task_Body is
   begin
      loop
         DIO.Put ("-Other-");
         DIO.Put (Integer (Step_Counter));
         DIO.Put (Loop_Counter);
         DIO.Put (Data_Out);

         Tests_Reports.Assert (Data_Out = Step_Counter + PO_Data);

         case Loop_Counter is
         when 0 =>
            Tests_Reports.Assert (Step_Counter = 1);

         when 1 =>
            Tests_Reports.Assert (Step_Counter = 2);
            Tests_Reports.Assert (Data_Out = 1);

         when 2 =>
            Tests_Reports.Assert (Step_Counter = 4);

         when 3 =>
            Tests_Reports.Assert (Step_Counter = 6);

         when 4 =>
            Tests_Reports.Assert (Step_Counter = 8);

         when others =>
            Tests_Reports.Assert (False);
         end case;

         Step_Counter := Step_Counter + 1;
         Loop_Counter := Loop_Counter + 1;

         PO.Wait (Step_Counter, Data_Out);
      end loop;
   end Other_Task_Body;

   Other_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Other_Task_Init'Access,
      Body_Ac  => Other_Task_Body'Access,
      Priority => 4);

end PO_Entry_Out_Param_2;
