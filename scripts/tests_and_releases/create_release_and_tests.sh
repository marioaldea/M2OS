#!/bin/bash
# Create release from the GitLab repository of M2OS
# Cloned repositories are expected in $M2OS_HOME/M2OS_MASTER/ and
# $M2OS_HOME/M2OS_WIKI/
#  $ git clone git@gitlab.com:marioaldea/M2OS.git M2OS_MASTER
#  $ git clone git@gitlab.com:marioaldea/M2OS.wiki M2OS_WIKI
#set -o xtrace

: ${1?"Usage: $0 M2OS_HOME (e.g. $0 /home/mtests/m2os)"}

readonly M2OS_MASTER="M2OS_MASTER"
readonly M2OS_WIKI="M2OS_WIKI"


# Check environment
M2OS_HOME=$1
if [ ! -d ${M2OS_HOME} ]; then
    echo "ERROR: ${M2OS_HOME} does not exists"
    exit
fi
cd ${M2OS_HOME}
readonly M2OS_HOME=$(pwd)
if [ ! -d ${M2OS_MASTER} ]; then
    echo "ERROR: $M2OS_HOME/$M2OS_MASTER does not exists"
    exit
fi
if [ ! -d ${M2OS_MASTER}/.git/ ]; then
    echo "ERROR: $M2OS_HOME/$M2OS_MASTER is not a GIT repository"
    exit
fi
if [ ! -d ${M2OS_WIKI} ]; then
    echo "ERROR: $M2OS_HOME/$M2OS_WIKI does not exists"
    exit
fi
if [ ! -d ${M2OS_WIKI}/.git/ ]; then
    echo "ERROR: $M2OS_HOME/$M2OS_WIKI is not a GIT repository"
    exit
fi

# Clean and update M2OS repository
echo "Cleaning and updating M2OS repository..."
cd ${M2OS_MASTER}
readonly OUT_MASTER=$(git pull origin master | tail -1)
git clean -f -d    # clean untracked files and directories

# Clean and update wiki repository
echo  "Cleaning and updating M2OS WIKI repository..."
cd ../${M2OS_WIKI}
readonly OUT_WIKI=$(git pull origin master | tail -1)
git clean -f -d    # clean untracked files and directories

# Check if repositories have changed
if [ "$OUT_MASTER" == "Already up to date." ] && [ "$OUT_WIKI" == "Already up to date." ]; then
    echo "M2OS repositories up to date. Leaving..."
    exit
fi

# Create release
cd $M2OS_HOME
readonly VERSION_NAME="M2OS_"$(date +%y-%m-%d)
${M2OS_MASTER}/scripts/tests_and_releases/create_release.sh ${M2OS_HOME} $VERSION_NAME | tee create_release.out

# Install and tests release
cd $M2OS_HOME
${M2OS_MASTER}/scripts/tests_and_releases/install_and_test.sh $VERSION_NAME  | tee install_and_test.out
