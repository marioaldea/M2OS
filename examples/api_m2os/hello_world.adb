----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
pragma Profile (Ravenscar);
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
#if (not M2_TARGET="arduino_uno") and (not M2_TARGET="epiphany") then
with Ada.Text_IO;
with GNAT.IO;
#end if;

with AdaX_Dispatching_Stack_Sharing;
--with M2.Kernel.Initialization;
--pragma Elaborate_All (M2.Kernel.Initialization);

procedure Hello_World is
begin
   M2.Direct_IO.Put_Line ("M2.Direct_IO: Hello world");
#if (not M2_TARGET="arduino_uno") and (not M2_TARGET="epiphany") then
   Ada.Text_IO.Put_Line ("Text_IO: Hello world");
   GNAT.IO.Put_Line ("GNAT.IO: Hello world");
#end if;
   loop
      null;
   end loop;
end Hello_World;
