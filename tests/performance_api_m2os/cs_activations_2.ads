pragma Warnings (Off);
with M2.Kernel.Scheduler;
pragma Elaborate_All (M2.Kernel.Scheduler);
pragma Warnings (On);

with Ada.Synchronous_Task_Control;

package CS_Activations_2 is

   Num_Of_Loops : constant := 20;

   Task_2_Prio : constant := 5;

   type Suspension_Primitives is (Delay_Until_Past_Time,
                                  --Delay_Until,
                                  Suspension_Object,
                                  Yield,
                                  PO_Entry,
                                  PO_Entry_Param);
   Suspension_Primitive : Suspension_Primitives :=
     Suspension_Primitives'First;

   Suspension_Primitives_Names : constant array (Suspension_Primitives)
     of String (1 .. 21) := ("Delay_Until_Past_Time",
                             --"Delay_Until",
                             "Suspension_Object    ",
                             "Yield                ",
                             "PO_Entry             ",
                             "PO_Entry_Param       ");

   Loop_Counter : Integer := 0;

   SO : Ada.Synchronous_Task_Control.Suspension_Object;

   protected PO is
      entry Wait;
      procedure Open;
   private
      Is_Open : Boolean := False;
   end PO;

   protected PO_Param is
      entry Wait (Data_Out : access Integer);
      procedure Open;
   private
      Is_Open : Boolean := False;
      Counter : Integer := 0;
   end PO_Param;

end CS_Activations_2;
