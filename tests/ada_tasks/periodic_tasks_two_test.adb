----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Two periodic taks (Main and Other).
--  Period of the "Other" task is double than period of "Main".
--  The test checks the number of activations of "Other" is half the number
--  of activations of "Main".

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;
with Ada.Synchronous_Task_Control;

with M2.Direct_IO;
with Periodic_Tasks_Two_2;

with Tests_Reports;
with M2.HAL;

procedure Periodic_Tasks_Two_Test  is
   package DIO renames M2.Direct_IO;
   package P2 renames Periodic_Tasks_Two_2;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   Loop_Counter : Integer := 0;

   Next_Time : RT.Time;
   Period : constant RT.Time_Span := RT.Milliseconds (P2.Period_In_Ms);

begin
   DIO.Put ("-Periodic_Tasks_Two_Test-");
   DIO.New_Line;
   DIO.Put ("-MainInit-");
   Next_Time := Ada.Real_Time.Clock + Period;
   delay until Next_Time;

   loop
      M2.HAL.Enable_Interrupts;
      DIO.Put ("-Main-");

      if Loop_Counter /= 0 then
         DIO.Put (" Main=");
         DIO.Put (Loop_Counter);
         DIO.Put (" Other=");
         DIO.Put (P2.Loop_Counter);
         Tests_Reports.Assert_Equal (Val1   => P2.Loop_Counter * 2,
                                     Val2   => Loop_Counter,
                                     Margin => 1);
      end if;

      if Loop_Counter = P2.Num_Of_Loops then

         Tests_Reports.Test_OK;
      end if;

      Loop_Counter := Loop_Counter + 1;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end loop;

end Periodic_Tasks_Two_Test;
