----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on ev3_color_ambient.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
--  setup for this example:
--  attach NXT Color Sensor to Port BAS1
--  Open the Serial terminal to view.
----------------------------------------------------------------------------

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Interfaces.C.Extensions;
with Arduino.EVShield.EV3Color;
with stdint_h;

procedure EVShield_EV3color_Ambient is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, Interfaces.C.Extensions.bool;

   -- Program variables
   time : Arduino.Time_MS;
   aux : stdint_h.uint8_t;
   light : Float;

   package Color_sensor is new Arduino.EVShield.EV3Color.Color (Arduino.EVShield.SH_BAS1);

begin

   DIO.Put_Line ("--------------------------------------");
   DIO.Put_Line ("Starting EV3 Color Sensor Ambient Light Test program");
   DIO.Put_Line ("--------------------------------------");

   aux := Color_sensor.Set_Mode(Color_sensor.MODE_Color_AmbientLight);

   DIO.Put_Line ("setup done");
   DIO.Put_Line ("shine or block light going into color sensor to see change in value");

   -- Wait until user presses GO button to continue the program
   while not Arduino.EVShield.Get_Button_State(Arduino.EVShield.BTN_GO) loop
      time := Arduino.Millis;
      if time mod 1000 < 3 then
         DIO.Put_Line (" Press GO button to continue");
      end if;
   end loop;

   loop

      light := Color_sensor.Get_Val;
      DIO.Put ("Ambient Light: "); DIO.Put (light); DIO.New_Line;

      -- wait for some time
      Arduino.C_Delay (1000);
   end loop;
end EVShield_EV3color_Ambient;
