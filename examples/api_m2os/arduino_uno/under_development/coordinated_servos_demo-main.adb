----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Demo program that moves two coordinated servos following a trayectory.
--  See coordinated_servos.ads for details.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;
with Arduino.Coordinated_Servos;

with M2.Direct_IO;
with Coordinated_Servos_Demo.Servos;
with Coordinated_Servos_Demo.Trayectory_Data;

procedure Coordinated_Servos_Demo.Main is
   package DIO renames M2.Direct_IO;
begin
   DIO.Put_Line ("Main: Initialization");

   Servos.Attach (Trayectory_Data.Servo_Pins);

   Servos.Follow_Trayectory (Trayectory_Data.Trayectory'Access,
                             Trayectory_Data.Trayectory_Cicle_Time,
                             Repeat => True);
   DIO.Put_Line ("Main: end Initialization");
end Coordinated_Servos_Demo.Main;
