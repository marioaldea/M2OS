//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Simple example of use of the M2OS POSIX-like layer.
// Read clock using clock_gettime() from a periodic thread.
#include <pthread.h>
#include <stdio.h>
#include <time.h>

THREAD_POOL(1);

void putts(struct timespec ts) {
  putint(TS_SEC(ts)); puts("s "); putint(TS_NSEC(ts)); puts("ns");
}

//****************//
//  thread_body1  //
//****************//
struct timespec next_activation_time1;
const struct timespec period1 = TS(0, 600000000);

void * thread_body1(void *arg) {
  puts("Thread 1\n");
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  putts(ts); puts("\n");
    
  TS_INC(next_activation_time1, period1);
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time1, NULL);
  
  return NULL;
}    
    
//**********//
//   main   //
//**********//
int main (int argc, char **argv) {
  m2osinit();
  puts("Main\n");
  check_posix_api();
    
  pthread_t th1;
  pthread_attr_t attr;
  struct sched_param sch_param = {.sched_priority = 2};
  
  clock_gettime(CLOCK_MONOTONIC, &next_activation_time1);
  
  pthread_attr_init(&attr);
  pthread_attr_setschedparam(&attr, &sch_param);
    
  pthread_create(&th1, &attr, thread_body1, NULL);
  
  puts("Main ends\n");

  return 0;
}
