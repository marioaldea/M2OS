----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
package body M2.Direct_IO is

   ---------
   -- Put --
   ---------

   procedure Put (Num : Integer; Base : Interfaces.Unsigned_8 := 10) is
      pragma Unreferenced (Base);
   begin
      GNAT.IO.Put (Num);
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (Num : Interfaces.Integer_32;
                  Base : Interfaces.Unsigned_8 := 10) is
      pragma Unreferenced (Base);
   begin
      GNAT.IO.Put (Integer (Num));
   end Put;

   -------------------------
   -- Put_Null_Terminated --
   -------------------------

   procedure Put_Null_Terminated (Str_Ad : System.Address) is      
      type Buffer is array (Natural) of Character;
      Str : Buffer with Address => Str_Ad;
      Index : Natural := 0;
   begin
      loop
         exit when Str (Index) = Character'Val(0);
         GNAT.IO.Put (Str (Index));
         Index := Index + 1;
      end loop;
   end Put_Null_Terminated;

   ---------
   -- Put --
   ---------

   procedure Put (F : Float) is
   begin
      Put (Interfaces.Integer_32 (Float'Floor (F)));
      PutChar ('.');
      Put (Interfaces.Integer_32 ((F - Float'Floor (F)) * 10.0));
   end Put;


   --------------
   -- New_Line --
   --------------

   procedure New_Line is
   begin
      GNAT.IO.New_Line (1);
   end New_Line;
   
   --------------------
   -- Initialization --
   --------------------

   procedure Initialization is
   begin
      null;
   end Initialization;

end M2.Direct_IO;
