----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Autopilot version for the lego robot, this program has 2 task, one that
-- controls the movement and one that gets the sensor outputs.
----------------------------------------------------------------------------
-- Setup:
-- Attach the lego robot with the ultrasonic sensors.
----------------------------------------------------------------------------

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
with EVShield_Distance_Control_Task;
with EVShield_Distance_Measure_Task;

procedure EVShield_Periodic_Demo_Autopilot  is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put_line ("-Main-");
end EVShield_Periodic_Demo_Autopilot;
