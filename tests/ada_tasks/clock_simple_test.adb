----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Simple test for Ada.Real_Time.Clock.
--  Test the time measured with Ada.Real_Time.Clock is double for a double
--  length loop.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;
with M2.Direct_IO;
with M2.HAL;

with Tests_Reports;

procedure Clock_Simple_Test  is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   Eat_Loops : constant := 150 * M2.HAL.HWTime_Hz / 1_000;
   Foo : Integer;
   pragma Volatile (Foo);

   Margin_In_Percentage : constant := 10;
   TS_Margin : RT.Time_Span;

   use type RT.Time, RT.Time_Span;

   Start_Time : Ada.Real_Time.Time;
   Elapsed_Time_1 : RT.Time_Span;
   Elapsed_Time_2 : RT.Time_Span;

begin
   DIO.Put ("-Clock_Simple_Test-");

   loop
      DIO.Put ("-Main-");

      --  First measure

      Start_Time := Ada.Real_Time.Clock;

      for I in 1 .. Eat_Loops loop
         for J in 1 .. Eat_Loops loop
            Foo := J - I;
            Tests_Reports.Assert (Foo = J - I);
         end loop;
      end loop;

      Elapsed_Time_1 := Ada.Real_Time.Clock - Start_Time;

      DIO.Put (" Elapsed_Time_1 (ms)=");
      DIO.Put (Integer (RT.To_Duration (Elapsed_Time_1) * 1_000));

      --  assert the measure is long enough

      Tests_Reports.Assert (Elapsed_Time_1 > RT.Milliseconds (10));

      --  Second measure: double time

      Start_Time := Ada.Real_Time.Clock;

      for I in 1 .. Eat_Loops * 2 loop
         for J in 1 .. Eat_Loops loop
            Foo := J - I;
            Tests_Reports.Assert (Foo = J - I);
         end loop;
      end loop;

      Elapsed_Time_2 := Ada.Real_Time.Clock - Start_Time;

      DIO.Put (" Elapsed_Time_2 (ms)=");
      DIO.Put (Integer (RT.To_Duration (Elapsed_Time_2) * 1_000));

      --  Check Elapsed_Time_2 ~= 2*Elapsed_Time_1

      Tests_Reports.Assert (Elapsed_Time_2 > Elapsed_Time_1);
      TS_Margin := Elapsed_Time_2 * Margin_In_Percentage / 100;
      DIO.Put (" Margin (ms)=");
      DIO.Put (Integer (RT.To_Duration (TS_Margin) * 1_000));
      Tests_Reports.Assert (Elapsed_Time_2 - 2*Elapsed_Time_1 <= TS_Margin);

      Tests_Reports.Test_OK;

      delay until RT.Time_First;
   end loop;

end Clock_Simple_Test;
