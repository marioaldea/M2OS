--  Arduino Uno version of this package

--  pragma Restrictions (No_Elaboration_Code);

with System;
with Interfaces;
with Ada.Unchecked_Conversion;

package M2.HAL is
   --  pragma Pure;
   pragma Preelaborate;

   type Procedure_Ac is access procedure;
--   pragma Convention (C, Procedure_Ac);

   subtype Thread_Context is Procedure_Ac;
   --  PC

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch (New_Context : Thread_Context)
     with Inline_Always;

--     ------------------
--     -- Save_Context --
--     ------------------
--
--     procedure Save_Context (Context : access Thread_Context;
--                             Job_Body_Ac : Procedure_Ac);
   --  pragma Inline_Always (Save_Context);

   subtype Address_Int is Interfaces.Unsigned_16;

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

--     procedure Set_Global_Stack_Limit;
--   --  pragma Inline_Always (Set_Global_Stack_Limit);
--  Not requied now, since the stack is always started at the lower
--  position.

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Address_Int;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Address_Int;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Address_Int;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Address_Int;

   -------------------------------
   -- Enable/disable interrupts --
   -------------------------------

   procedure Enable_Interrupts;
   pragma Inline_Always (Enable_Interrupts);
   pragma Import (Intrinsic, Enable_Interrupts, "__builtin_avr_sei");

   procedure Disable_Interrupts;
   pragma Inline_Always (Disable_Interrupts);
   pragma Import (Intrinsic, Disable_Interrupts, "__builtin_avr_cli");

   function Are_Interrupts_Enabled return Boolean;

   ------------
   -- HWTime --
   ------------

   HWTime_Hz : constant := 1_000;
   --  Should have the same value than OSI.Ticks_Per_Second

--     pragma Compile_Time_Warning (Duration'Small /= 1.0/HWTime_Hz,
--                                  "Duration'Small /= 1/HWTime_Hz");

   type HWTime is new Interfaces.Unsigned_32;
--   for HWTime'Size use Interfaces.Unsigned_64'Size;
--   type HWTime is new Interfaces.Unsigned_32;
--   for HWTime'Size use Duration'Size;

--   ------------------------
--   -- HWTime_To_Duration --
--   ------------------------
--
--     function HWTime_To_Duration is
--        new Ada.Unchecked_Conversion (HWTime, Duration);
--
--     ------------------------
--     -- Duration_To_HWTime --
--     ------------------------
--
--     function Duration_To_HWTime is
--        new Ada.Unchecked_Conversion (Duration, HWTime);

   ----------------
   -- Get_HWTime --
   ----------------

   function Get_HWTime return HWTime;
   --  with Inline_Always;
   --  NO Inline_Always to avoid an elaboration or linking problem in simple
   --  programs with no tasking (i.e. dht11_demo-main.adb)

   ------------------------
   -- OS_Tick_Handler_Ac --
   ------------------------

   type OS_Tick_Handler_Ac is access procedure;

   --------------------
   -- Initialization --
   --------------------

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac);

end M2.HAL;
