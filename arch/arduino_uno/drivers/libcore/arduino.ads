----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of the Arduino library
--  Header file:
--    - hardware/arduino/avr/cores/arduino/Arduino.h
--  C and Cpp files:
--    - hardware/arduino/avr/cores/arduino/wiring_analog.c
--
with Interfaces.C;
with M2.Kernel.Initialization;
pragma Elaborate_All (M2.Kernel.Initialization);

package Arduino is

   --  unsupported macro: HIGH 0x1
   --  unsupported macro: LOW 0x0
   --  unsupported macro: INPUT 0x0
   --  unsupported macro: OUTPUT 0x1
   --  unsupported macro: INPUT_PULLUP 0x2
   --  unsupported macro: PI 3.1415926535897932384626433832795
   --  unsupported macro: HALF_PI 1.5707963267948966192313216916398
   --  unsupported macro: TWO_PI 6.283185307179586476925286766559
   --  unsupported macro: DEG_TO_RAD 0.017453292519943295769236907684886
   --  unsupported macro: RAD_TO_DEG 57.295779513082320876798154814105
   --  unsupported macro: EULER 2.718281828459045235360287471352
   --  unsupported macro: SERIAL 0x0
   --  unsupported macro: DISPLAY 0x1
   --  unsupported macro: LSBFIRST 0
   --  unsupported macro: MSBFIRST 1
   --  unsupported macro: CHANGE 1
   --  unsupported macro: FALLING 2
   --  unsupported macro: RISING 3
   --  unsupported macro: INTERNAL 3
   --  unsupported macro: DEFAULT 1
   --  unsupported macro: EXTERNAL 0
   --  arg-macro: function min (a, b)
   --    return (a)<(b)?(a):(b);
   --  arg-macro: function max (a, b)
   --    return (a)>(b)?(a):(b);
   --  arg-macro: function abs (x)
   --    return (x)>0?(x):-(x);
   --  arg-macro: function constrain (amt, low, high)((amt)<(low)?(low):((amt)>(high)?(high):(amt))
   --    return (amt)<(low)?(low):((amt)>(high)?(high):(amt));
   --  arg-macro: function round (x)
   --    return (x)>=0?(long)((x)+0.5):(long)((x)-0.5);
   --  arg-macro: function radians (deg)
   --    return (deg)*DEG_TO_RAD;
   --  arg-macro: function degrees (rad)
   --    return (rad)*RAD_TO_DEG;
   --  arg-macro: function sq (x)
   --    return (x)*(x);
   --  arg-macro: procedure interrupts ()
   --    sei()
   --  arg-macro: procedure noInterrupts ()
   --    cli()
   --  arg-macro: function clockCyclesPerMicrosecond ()
   --    return  F_CPU / 1000000L ;
   --  arg-macro: function clockCyclesToMicroseconds (a)
   --    return  (a) / clockCyclesPerMicrosecond() ;
   --  arg-macro: function microsecondsToClockCycles (a)
   --    return  (a) * clockCyclesPerMicrosecond() ;
   --  arg-macro: function lowByte (w)
   --    return (uint8_t) ((w) and 0xff);
   --  arg-macro: function highByte (w)
   --    return (uint8_t) ((w) >> 8);
   --  arg-macro: function bitRead (value, bit)
   --    return ((value) >> (bit)) and 0x01;
   --  arg-macro: function bitSet (value, bit)
   --    return (value) |= (1UL << (bit));
   --  arg-macro: function bitClear (value, bit)
   --    return (value) &= ~(1UL << (bit));
   --  arg-macro: function bitWrite (value, bit, bit(bitvalue ? bitSet(value, bit) : bitClear(value, bit)
   --    return bitvalue ? bitSet(value, bit) : bitClear(value, bit);
   --  arg-macro: function bit (b)
   --    return 1UL << (b);
   --  arg-macro: function digitalPinToPort (P)
   --    return  pgm_read_byte( digital_pin_to_port_PGM + (P) ) ;
   --  arg-macro: function digitalPinToBitMask (P)
   --    return  pgm_read_byte( digital_pin_to_bit_mask_PGM + (P) ) ;
   --  arg-macro: function digitalPinToTimer (P)
   --    return  pgm_read_byte( digital_pin_to_timer_PGM + (P) ) ;
   --  arg-macro: function analogInPinToBit (P)
   --    return P;
   --  arg-macro: function portOutputRegister (P)
   --    return  (volatile uint8_t *)( pgm_read_word( port_to_output_PGM + (P))) ;
   --  arg-macro: function portInputRegister (P)
   --    return  (volatile uint8_t *)( pgm_read_word( port_to_input_PGM + (P))) ;
   --  arg-macro: function portModeRegister (P)
   --    return  (volatile uint8_t *)( pgm_read_word( port_to_mode_PGM + (P))) ;
   --  unsupported macro: NOT_A_PIN 0
   --  unsupported macro: NOT_A_PORT 0
   --  unsupported macro: NOT_AN_INTERRUPT -1
   --  unsupported macro: NOT_ON_TIMER 0
   --  unsupported macro: TIMER0A 1
   --  unsupported macro: TIMER0B 2
   --  unsupported macro: TIMER1A 3
   --  unsupported macro: TIMER1B 4
   --  unsupported macro: TIMER1C 5
   --  unsupported macro: TIMER2 6
   --  unsupported macro: TIMER2A 7
   --  unsupported macro: TIMER2B 8
   --  unsupported macro: TIMER3A 9
   --  unsupported macro: TIMER3B 10
   --  unsupported macro: TIMER3C 11
   --  unsupported macro: TIMER4A 12
   --  unsupported macro: TIMER4B 13
   --  unsupported macro: TIMER4C 14
   --  unsupported macro: TIMER4D 15
   --  unsupported macro: TIMER5A 16
   --  unsupported macro: TIMER5B 17
   --  unsupported macro: TIMER5C 18
   --  unsupported macro: word(...) makeWord(__VA_ARGS__)

--     procedure yield;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:38
--     pragma Import (C, yield, "yield");
--
--     subtype word is unsigned;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:121
--
--     subtype boolean is Extensions.bool;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:125
--
--     subtype byte is stdint_h.uint8_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:126

   use type Interfaces.C.int;

--     procedure initVariant;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:129
--     pragma Import (C, initVariant, "initVariant");
--
--     function atexit (func : access procedure) return int;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:131
--     pragma Import (C, atexit, "atexit");

   type Boolean8 is new Boolean;
   for Boolean8'Size use Interfaces.C.unsigned_char'Size;

   ----------------
   -- IO Digital --
   ----------------

   type Digital_Pin is new Interfaces.Unsigned_8 range 0 .. 13;
   for Digital_Pin'Size use Interfaces.Unsigned_8'Size;

   type Digital_Pin_Mode is (Input, Output);
   for Digital_Pin_Mode use (Input => 0,
                             Output => 1);
   for Digital_Pin_Mode'Size use 8;

   type Digital_Pin_Out_Value is (Out_Low, Out_High);
   for Digital_Pin_Out_Value use (Out_Low => 0,
                                  Out_High => 1);
   for Digital_Pin_Out_Value'Size use 8;

   type Digital_Pin_In_Value is (In_Low, In_High);
   for Digital_Pin_In_Value use (In_Low => 0,
                                 In_High => 1);
   for Digital_Pin_In_Value'Size use Interfaces.C.int'Size;

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:133
   procedure Pin_Mode (Pin : Digital_Pin; Mode : Digital_Pin_Mode);
   --  pragma Import (C, Pin_Mode, "pinMode");

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:134
   procedure Digital_Write (Pin : Digital_Pin; Val : Digital_Pin_Out_Value);
   pragma Import (C, Digital_Write, "digitalWrite");

  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:135
   function Digital_Read (Pin : Digital_Pin) return Digital_Pin_In_Value;
   pragma Import (C, Digital_Read, "digitalRead");

   ---------
   -- ADC --
   ---------

   type ADC_Chanel is new Interfaces.Unsigned_8 range 0 .. 5;
   for ADC_Chanel'Size use Interfaces.Unsigned_8'Size;

   type ADC_Value is new Interfaces.C.int;

   type ADC_Reference is (ADC_Ref_External,
                          ADC_Ref_Default,
                          ADC_Ref_Internal);
   for ADC_Reference use (ADC_Ref_External => 0,
                          ADC_Ref_Default => 1,
                          ADC_Ref_Internal => 3);
   for ADC_Reference'Size use 8;

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:136
   function Analog_Read (Chanel : ADC_Chanel) return ADC_Value;
   pragma Import (C, Analog_Read, "analogRead");

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:137
   procedure Analog_Reference (Mode : ADC_Reference);
   pragma Import (C, Analog_Reference, "analogReference");

   type PWM_Digital_Pin is (PWM_D3, PWM_D5, PWM_D6,
                            PWM_D9, PWM_D10, PWM_D11);
   for PWM_Digital_Pin use (PWM_D3 => 3, PWM_D5 => 5,
                            PWM_D6 => 6, PWM_D9 => 9,
                            PWM_D10 => 10, PWM_D11 => 11);
   for PWM_Digital_Pin'Size use Interfaces.Unsigned_8'Size;

   procedure Pin_Mode (Pin : PWM_Digital_Pin; Mode : Digital_Pin_Mode);
   pragma Import (C, Pin_Mode, "pinMode");

   type PWM_Value_Increment is new Interfaces.C.int range -255 .. 255;
   for PWM_Value_Increment'Size use Interfaces.C.int'Size;

   subtype PWM_Value is PWM_Value_Increment range 0 .. 255;
   --  for PWM_Value'Size use PWM_Value_Increment'Size;

  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:138
   procedure Analog_Write (Pin : PWM_Digital_Pin; Value : PWM_Value);
   pragma Import (C, Analog_Write, "analogWrite");

   ----------
   -- Time --
   ----------

   type Time_MS is new Interfaces.C.unsigned_long;
   type Time_MS_16 is new Interfaces.C.unsigned;
   type Time_US is new Interfaces.C.unsigned_long;
   type Time_US_16 is new Interfaces.C.unsigned;

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:140
   function Millis return Time_MS;
   pragma Export (C, Millis, "millis");

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:141
   function Micros return Time_US;
   pragma Export (C, Micros, "micros");

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:142
   procedure C_Delay (MS : Time_MS);
   pragma Export (C, C_Delay, "delay");

  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:143
   procedure Delay_Microseconds (US : Time_US_16);
   pragma Export (C, Delay_Microseconds, "delayMicroseconds");
   pragma Inline (Delay_Microseconds);

--     procedure shiftOut
--       (dataPin : stdint_h.uint8_t;
--        clockPin : stdint_h.uint8_t;
--        bitOrder : stdint_h.uint8_t;
--        val : stdint_h.uint8_t);  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:147
--     pragma Import (C, shiftOut, "shiftOut");
--
--     function shiftIn
--       (dataPin : stdint_h.uint8_t;
--        clockPin : stdint_h.uint8_t;
--        bitOrder : stdint_h.uint8_t) return stdint_h.uint8_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:148
--     pragma Import (C, shiftIn, "shiftIn");

   subtype Interrupt_Digital_Pin is Digital_Pin range 2 .. 3;

   type Interrupt_Mode is
     (Interrupt_When_Low, -- trigger the interrupt whenever the pin is low
      Interrupt_When_Change, -- trigger interrupt whenever the pin changes value
      Interrupt_When_Falling, -- trigger when the pin goes from high to low.
      Interrupt_When_Rising); -- trigger when the pin goes from low to high
   for Interrupt_Mode use
     (Interrupt_When_Low => 0,
      Interrupt_When_Change => 1,
      Interrupt_When_Falling => 2,
      Interrupt_When_Rising => 3);
   for Interrupt_Mode'Size use Interfaces.C.Int'Size;

   type ISR_Ac is access procedure;
   pragma Convention(Convention => C,
                     Entity => ISR_Ac);


   ---------------------------
   -- DigitalPinToInterrupt --
   ---------------------------

   function DigitalPinToInterrupt (Pin  : Interrupt_Digital_Pin)
                                   return Interfaces.C.Int is
     (Interfaces.C.Int (Interrupt_Digital_Pin'Pos (Pin) mod 2));
   --  Arduino Uno pin 2 produces interrupt 0 and pin 3 produces interrupt 1

   ----------------------
   -- Attach_Interrupt --
   ----------------------

   --  Use example:
   --    Attach_Interrupt (Int_Num => DigitalPinToInterrupt (2),
   --                      ISR => interrupt_handler'access,
   --                      Mode => Interrupt_When_Rising);

   procedure Attach_Interrupt
     (Int_Num : Interfaces.C.Int;
      ISR  : ISR_Ac;
      Mode : Interrupt_Mode);
   pragma Import (C, Attach_Interrupt, "attachInterrupt");

   ----------------------
   -- Detach_Interrupt --
   ----------------------

   --  Use example:
   --    Detach_Interrupt (DigitalPinToInterrupt (2));

   procedure Detach_Interrupt (Int_Num : Interfaces.C.Int);
   pragma Import (C, Detach_Interrupt, "detachInterrupt");
--
--     procedure setup;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:153
--     pragma Import (C, setup, "setup");
--
--     procedure c_loop;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:154
--     pragma Import (C, c_loop, "loop");
--
--     port_to_mode_PGM : aliased array (size_t) of aliased stdint_h.uint16_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:163
--     pragma Import (C, port_to_mode_PGM, "port_to_mode_PGM");
--
--     port_to_input_PGM : aliased array (size_t) of aliased stdint_h.uint16_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:164
--     pragma Import (C, port_to_input_PGM, "port_to_input_PGM");
--
--     port_to_output_PGM : aliased array (size_t) of aliased stdint_h.uint16_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:165
--     pragma Import (C, port_to_output_PGM, "port_to_output_PGM");
--
--     digital_pin_to_port_PGM : aliased array (size_t) of aliased stdint_h.uint8_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:167
--     pragma Import (C, digital_pin_to_port_PGM, "digital_pin_to_port_PGM");
--
--     digital_pin_to_bit_mask_PGM : aliased array (size_t) of aliased stdint_h.uint8_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:169
--     pragma Import (C, digital_pin_to_bit_mask_PGM, "digital_pin_to_bit_mask_PGM");
--
--     digital_pin_to_timer_PGM : aliased array (size_t) of aliased stdint_h.uint8_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:170
--     pragma Import (C, digital_pin_to_timer_PGM, "digital_pin_to_timer_PGM");
--
--     function makeWord (w : stdint_h.uint16_t) return stdint_h.uint16_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:238
--     pragma Import (CPP, makeWord, "_Z8makeWordj");
--
--     function makeWord (h : byte; l : byte) return stdint_h.uint16_t;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:239
--     pragma Import (CPP, makeWord, "_Z8makeWordhh");
--
--     function pulseIn
--       (pin : stdint_h.uint8_t;
--        state : stdint_h.uint8_t;
--        timeout : unsigned_long) return unsigned_long;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:243
--     pragma Import (C, pulseIn, "pulseIn");
--
--     function pulseInLong
--       (pin : stdint_h.uint8_t;
--        state : stdint_h.uint8_t;
--        timeout : unsigned_long) return unsigned_long;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:244
--     pragma Import (C, pulseInLong, "pulseInLong");
--

   type Frequency_In_Hz is new Interfaces.C.unsigned;

   -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:246
   procedure Tone
     (Pin       : PWM_Digital_Pin;
      Frequency : Frequency_In_Hz; -- Interfaces.C.unsigned
      Duration  : Time_Ms);-- Interfaces.C.unsigned_long);
   pragma Import (CPP, Tone, "_Z4tonehjm");

   procedure No_Tone (Pin : PWM_Digital_Pin);  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:247
   pragma Import (CPP, No_Tone, "_Z6noToneh");
--
--     function random (arg1 : long) return long;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:250
--     pragma Import (CPP, random, "_Z6randoml");
--
--     function random (arg1 : long; arg2 : long) return long;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:251
--     pragma Import (CPP, random, "_Z6randomll");
--
--     procedure randomSeed (arg1 : unsigned_long);  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:252
--     pragma Import (CPP, randomSeed, "_Z10randomSeedm");
--
   function Map
     (arg1 : Interfaces.C.long;
      arg2 : Interfaces.C.long;
      arg3 : Interfaces.C.long;
      arg4 : Interfaces.C.long;
      arg5 : Interfaces.C.long) return Interfaces.C.long;  -- /opt/arduino-1.8.2/hardware/arduino/avr/cores/arduino/Arduino.h:253
   pragma Import (CPP, map, "_Z3maplllll");

end Arduino;
