----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Synchronous_Task_Control;
with Interfaces;

with AdaX_Dispatching_Stack_Sharing;

with M2.Debug;

with Microbit.Time;
with Microbit.Music;

package body Microbit.Buzzer is
   package RT renames Ada.Real_Time;
   package STC renames Ada.Synchronous_Task_Control;

   use type Ada.Real_Time.Time;
   use type Ada.Real_Time.Time_Span;
   use type Interfaces.Unsigned_8;

   Suspension_Object : STC.Suspension_Object;

   type Buzzer_State is (Off, Playing_Melody);

   Next_State : Buzzer_State := Off;
   pragma Volatile (Next_State);
   Current_State : Buzzer_State := Off;

   One_Note_Melody : aliased Melodies.Melody_Notes := (0 => (0, 0));
   --  To simplify the task implementation, a simple tone wihtout repetition
   --  is a one note melody (Used by the Tone() procedure).

   Two_Note_Melody : aliased Melodies.Melody_Notes := ((0, 0), (0, 0));
   --  To simplify the task implementation, a simple tone wihtout repetition
   --  is a two note melody (one tone and one rest).
   --  (Used by the Tone() procedure).

   --  Current melody parameters

   Current_Melody : access constant Melodies.Melody_Notes;
   Current_Note : Interfaces.Unsigned_8;

   Repeat_Melody : Boolean;

   Current_Note_End : RT.Time;

   Tempo_Ms : Microbit.Time.Time_Ms;

   --  Next activation time

   Next_Time : RT.Time;
   Period : constant RT.Time_Span := RT.Milliseconds (20);

   -------------------
   -- Note_Duration --
   -------------------

   function Current_Note_Duration_Ms return Microbit.Time.Time_Ms;

   function Current_Note_Duration_Ms return Microbit.Time.Time_Ms is
   begin
      return Microbit.Buzzer.Tempo_Ms /
        Time_MS (Current_Melody (Current_Note).Rel_Duration);
   end Current_Note_Duration_Ms;

   ----------
   -- Tone --
   ----------

   procedure Tone (Frequency_Hz     : Microbit.Music.Pitch;
                   Duration_Ms      : Microbit.Time.Time_Ms;
                   Repeat_Period_Ms : Microbit.Time.Time_Ms := 0) is
   begin
      pragma Assert (Repeat_Period_Ms = 0 or Duration_Ms <= Repeat_Period_Ms);
      pragma Assert (Frequency_Hz >= 1);

      if Repeat_Period_Ms = 0 then
         One_Note_Melody (0) := (Frequency_Hz => Frequency_Hz,
                                 Rel_Duration => 1);

         Play_Melody (One_Note_Melody'Access,
                      Tempo_Ms => Duration_Ms,
                      Repeat => False);

      else
         Two_Note_Melody :=
           ((Frequency_Hz => Frequency_Hz,
             Rel_Duration =>
               Interfaces.Unsigned_8 (Repeat_Period_Ms * 10 / Duration_Ms)),

            (Frequency_Hz => Arduino.Melodies.NOTE_REST,
             Rel_Duration => Interfaces.Unsigned_8
               (Repeat_Period_Ms * 10 / (Repeat_Period_Ms - Duration_Ms))));

         Play_Melody (Two_Note_Melody'Access,
                      Tempo_Ms => Repeat_Period_Ms * 10,
                      Repeat => True);
      end if;

   end Tone;

   -----------------
   -- Play_Melody --
   -----------------

   procedure Play_Melody (Melody   : access constant Melodies.Melody_Notes;
                          Tempo_Ms : Microbit.Time.Time_Ms;
                          Repeat   : Boolean := True) is
   begin
      Current_Melody := Melody;
      Current_Note := Current_Melody'First;

      Microbit.Buzzer.Tempo_Ms := Time_MS (Tempo_Ms);

      Repeat_Melody := Repeat;

      Next_Time := Ada.Real_Time.Clock;

      Microbit.Music.Tone
        (Pin       => Pin_Buzzer,
         Frequency => Current_Melody (Current_Note).Frequency_Hz,
         Duration  => Current_Note_Duration_Ms);

      Current_Note_End := Next_Time +
        Ada.Real_Time.Milliseconds (Integer (Current_Note_Duration_Ms));

      Next_State := Playing_Melody;

      STC.Set_True (Suspension_Object);
   end Play_Melody;

   ------------
   -- Silent --
   ------------

   procedure Silent is
   begin
      Next_State := Off;

      STC.Set_False (Suspension_Object);
   end Silent;

   ----------------------
   -- Buzzer_Task_Init --
   ----------------------

   procedure Buzzer_Task_Init is
   begin
      Arduino.Pin_Mode (Pin  => Pin_Buzzer,
                        Mode => Arduino.Output);

      STC.Suspend_Until_True (Suspension_Object);
   end Buzzer_Task_Init;

   ----------------------
   -- Buzzer_Task_Body --
   ----------------------

   procedure Buzzer_Task_Body is
   begin
      Current_State := Next_State;

      case (Current_State) is

         when Off =>
            --  Wait until next call to 'Tone'

            STC.Suspend_Until_True (Suspension_Object);

         when Playing_Melody =>

            if Next_Time >= Current_Note_End then

               if Current_Note = Current_Melody'Last
                 and then not Repeat_Melody
               then
                  --  One shot melody: end of sound
                  Arduino.No_Tone (Pin_Buzzer);

                  Next_State := Off;

               else
                  Current_Note :=
                    (if Current_Note = Current_Melody'Last
                     then Current_Melody'First
                     else Current_Note + 1);

                  Arduino.Tone
                    (Pin       => Pin_Buzzer,
                     Frequency => Current_Melody (Current_Note).Frequency_Hz,
                     Duration  => Current_Note_Duration_Ms);

                  Current_Note_End := Current_Note_End +
                    RT.Milliseconds (Integer (Current_Note_Duration_Ms));
               end if;
            end if;
      end case;

      --  Wait for next polling time

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Buzzer_Task_Body;

   -----------------
   -- Buzzer_Task --
   -----------------

   Buzzer_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac => Buzzer_Task_Init'Unrestricted_Access,
      Body_Ac => Buzzer_Task_Body'Unrestricted_Access,
      Priority  => Task_Prio);

end Microbit.Buzzer;
