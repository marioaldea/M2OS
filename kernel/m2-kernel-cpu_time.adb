----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Unchecked_Conversion;
with M2.Kernel.TCBs;
with M2.Kernel.Ready_Queue;
--  with M2.Direct_IO;

package body M2.Kernel.CPU_Time is
   package RQ renames M2.Kernel.Ready_Queue;
--     package DIO renames M2.Direct_IO;
   
   use type HAL.HWTime, TCBs.TCB_Ac;

   Last_Dispatch_Time : HAL.HWTime := 0;
   Idle_Time : CPU_Time := 0;
   
   function To_CPUT is new Ada.Unchecked_Conversion (HAL.HWTime, CPU_Time);
   --  Warning when CPU time is not used, but in that case this conversion is
   --  not going to be used anyway.
 
   ---------------------------------------
   -- Update_CPU_Time_Of_Running_Thread --
   ---------------------------------------

   procedure Update_CPU_Time_Of_Running_Thread is
      Now : constant HAL.HWTime := HAL.Get_HWTime;
   begin
      pragma Assert (RQ.Running_Ac /= null);
      pragma Assert (Now >= Last_Dispatch_Time);

      RQ.Running_Ac.CPU_Time := RQ.Running_Ac.CPU_Time +
        To_CPUT (Now - Last_Dispatch_Time);
      Last_Dispatch_Time := Now;
   end Update_CPU_Time_Of_Running_Thread; 
   
   --------------------------
   -- Update_Idle_CPU_Time --
   --------------------------

   procedure Update_Idle_CPU_Time is
      Now : constant HAL.HWTime := HAL.Get_HWTime;
   begin
      pragma Assert (Now >= Last_Dispatch_Time);
      
      Idle_Time := Idle_Time + To_CPUT (Now - Last_Dispatch_Time);
      Last_Dispatch_Time := Now;
   end Update_Idle_CPU_Time;
   
   ---------------------
   -- Thread_CPU_Time --
   ---------------------

   function Thread_CPU_Time (Th_Ac : TCBs.TCB_Ac) return CPU_Time is
   begin
      if not M2.Account_CPU_Time'First then
         return 0;
      end if;
      pragma Assert (HAL.Get_HWTime >= Last_Dispatch_Time);
      
      if Th_Ac = RQ.Running_Ac then
         return Th_Ac.CPU_Time + To_CPUT (HAL.Get_HWTime - Last_Dispatch_Time);
      else
         return Th_Ac.CPU_Time;
      end if;      
   end Thread_CPU_Time; 
   
   -------------------
   -- Idle_CPU_Time --
   -------------------

   function Idle_CPU_Time return CPU_Time is
      (if M2.Account_CPU_Time'First then Idle_Time else 0);

end m2.kernel.CPU_Time;
