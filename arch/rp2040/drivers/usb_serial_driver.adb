----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with RP.Device;
with RP.Clock;
with Pico;

with USB.HAL.Device;
with USB.Device.Serial;
with USB.Device;
with USB;
with HAL; use HAL;

package body USB_Serial_Driver is
   
   Max_Packet_Size   : constant := 64;

   USB_Stack         : USB.Device.USB_Device_Stack (Max_Classes => 1);
   USB_Serial        : aliased USB.Device.Serial.Default_Serial_Class
      (TX_Buffer_Size => Max_Packet_Size,
       RX_Buffer_Size => Max_Packet_Size);

   use type USB.Device.Init_Result;

   ----------
   -- Open --
   ----------

   procedure Open (Mode : M2.Drivers.File_Access_Mode) is
      Success : Boolean;
      Status  : USB.Device.Init_Result;
   begin
      RP.Clock.Initialize (Pico.XOSC_Frequency);
      
      Success := USB_Stack.Register_Class (USB_Serial'Unchecked_Access);
      pragma Assert (Success);

      Status := USB_Stack.Initialize
        (Controller      => RP.Device.UDC'Access,
         Manufacturer    => USB.To_USB_String ("Raspberry Pi"),
         Product         => USB.To_USB_String ("M2OS"),
         Serial_Number   => USB.To_USB_String ("42"),
         Max_Packet_Size => Max_Packet_Size);

      pragma Assert (Status /= USB.Device.Ok);

      USB_Stack.Start;
   end Open;

   -----------
   -- Write --
   -----------

   function Write (Buffer_Ptr : System.Address;
                   Bytes      : size_t)
                   return int is
      Str : String (1 .. Natural (Bytes))
        with Address => Buffer_Ptr;
      Length     : HAL.UInt32;
   begin
      --  TODO while
      USB_Stack.Poll;
      
      USB_Serial.Write (RP.Device.UDC, Str, Length);

      return int (Bytes);
   end Write;
   

end USB_Serial_Driver;
