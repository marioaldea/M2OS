----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Manages a piezoelectric buzzer conected in the 'Pin_Buzzer' PWM pin.
--
--     ------------                 -----------------------
--     |          |                 |                     |
--     |         +|-----------------|Pin_Buzzer           |
--     | Buzzer   |                 |         Arduino Uno |
--     |         -|-----------------|GND                  |
--     |          |                 |                     |
--     ------------                 -----------------------
--
--  Uses a internal task which allows to play peridodic tones and melodies.
--
with System;
with Ada.Real_Time;

with Arduino;
with Arduino.Melodies;

generic
   Pin_Buzzer : Arduino.PWM_Digital_Pin;
   Task_Prio : System.Priority;
package Arduino.Buzzer is

   procedure Silent;

   procedure Tone (Frequency_Hz     : Frequency_In_Hz;
                   Duration_Ms      : Time_Ms_16;
                   Repeat_Period_Ms : Time_MS_16 := 0);

   procedure Play_Melody (Melody   : access constant Melodies.Melody_Notes;
                          Tempo_Ms : Time_Ms_16;
                          Repeat   : Boolean := True);

end Arduino.Buzzer;
