----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of Arduino standard library Servo.h
--  #include <Servo.h> must appear in lib_core_generator.ino
--
with Interfaces.C;

package Arduino.Servo is

   subtype PWM_Digital_Pin is Arduino.PWM_Digital_Pin;

   type Signed_Angle_Deg is new Interfaces.C.Int range -360 .. 360;
   for Signed_Angle_Deg'Size use Interfaces.C.Int'Size;

   subtype Angle_Deg is Signed_Angle_Deg range 0 .. 180;

   type Attach_Ret is new Interfaces.Unsigned_8;
   Attach_Ret_Error : constant := 0;

   type Servo is limited record
      ServoIndex : aliased Interfaces.Unsigned_8;
      Min : aliased Interfaces.Unsigned_8;
      Max : aliased Interfaces.Unsigned_8;
   end record;
   pragma Import (CPP, Servo);

   Sizeof_Servo : Integer with
     Import, Convention => C,
     External_Name => "sizeof_Servo";
   --  Defined in 'arch/arduino_uno/drivers/libcore/sizes_arduino_types.cpp'
   --  Take the value of -1 if the library is not included.

   ---------------
   -- New_Servo --
   ---------------

   function New_Servo return Servo;
   pragma CPP_Constructor (New_Servo, "_ZN5ServoC1Ev");

   ------------
   -- Attach --
   ------------

   function Attach (This : access Servo; Pin : PWM_Digital_Pin)
                    return Attach_Ret;
   pragma Import (CPP, Attach, "_ZN5Servo6attachEi");

   -----------
   -- write --
   -----------

   procedure Write (This : access Servo; Angle : Angle_Deg);
   pragma Import (CPP, Write, "_ZN5Servo5writeEi");


   --  Other functions to include in the binding:

--     function Attach
--       (This : access Servo;
--        Pin : Int;
--        Min : Int;
--        Max : Int) return Stdint_H.Uint8_T;  -- /opt/arduino-1.8.2/libraries/Servo/src/Servo.h:105
--     pragma Import (CPP, Attach, "_ZN5Servo6attachEiii");
--
--     procedure Detach (This : access Servo);  -- /opt/arduino-1.8.2/libraries/Servo/src/Servo.h:106
--     pragma Import (CPP, Detach, "_ZN5Servo6detachEv");
--
--     procedure WriteMicroseconds (This : access Servo; Value : Int);  -- /opt/arduino-1.8.2/libraries/Servo/src/Servo.h:108
--     pragma Import (CPP, WriteMicroseconds, "_ZN5Servo17writeMicrosecondsEi");
--
--     function Read (This : access Servo) return Int;  -- /opt/arduino-1.8.2/libraries/Servo/src/Servo.h:109
--     pragma Import (CPP, Read, "_ZN5Servo4readEv");
--
--     function ReadMicroseconds (This : access Servo) return Int;  -- /opt/arduino-1.8.2/libraries/Servo/src/Servo.h:110
--     pragma Import (CPP, ReadMicroseconds, "_ZN5Servo16readMicrosecondsEv");
--
--     function Attached (This : access Servo) return Extensions.Bool;  -- /opt/arduino-1.8.2/libraries/Servo/src/Servo.h:111
--     pragma Import (CPP, Attached, "_ZN5Servo8attachedEv");

end Arduino.Servo;
