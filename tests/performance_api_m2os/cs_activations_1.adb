pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Interfaces;
with Ada.Real_Time;
with Ada.Synchronous_Task_Control;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;

with CS_Activations_2;

with Measurements_HiRes;

package body CS_Activations_1 is
   package DIO renames M2.Direct_IO;
   package P2 renames CS_Activations_2;
   package RT renames Ada.Real_Time;
   package Measurements renames Measurements_HiRes;

   use type RT.Time_Span, P2.Suspension_Primitives;

   Loop_Counter : Integer := 0;

   Next_Time, Start_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span := RT.Milliseconds (20);
   
   I : aliased Integer := 11;
   
   ------------
   -- Task_1 --
   ------------
   
   procedure Task_1_Init is
   begin
      Start_Time := Ada.Real_Time.Clock;
      
      Ada.Synchronous_Task_Control.Set_True (P2.SO);
      --  To ensure this task starts its body before Task_2
   end Task_1_Init;
      
   
   procedure Task_1_Body  is
   begin
      if Loop_Counter /= 0 then
         Measurements.End_Measurement;
      end if;
      
      --  DIO.Put ("-Task_1-");

      if Loop_Counter = P2.Num_Of_Loops then
--           DIO.Put (P2.Suspension_Primitives'Pos (P2.Suspension_Primitive));
         DIO.Put (" ");
         DIO.Put (P2.Suspension_Primitives_Names (P2.Suspension_Primitive));
--           DIO.Put (" L1=");
--           DIO.Put (Loop_Counter);
--           DIO.Put (" L2=");
--           DIO.Put (P2.Loop_Counter);
         Measurements.Assert_Equal (P2.Num_Of_Loops, P2.Loop_Counter, 1);

         DIO.Put (" Measurement CSTime=");
         DIO.Put (Interfaces.Integer_32 (Measurements.To_Time_Units (
                  Measurements.Time_Long (Measurements.Measurement_Avg))));
         DIO.PutChar (Measurements.Time_Unit_Symbol); DIO.PutChar ('s');
         DIO.New_Line;

         if P2.Suspension_Primitive /= P2.Suspension_Primitives'Last then
            P2.Suspension_Primitive :=
              P2.Suspension_Primitives'Succ (P2.Suspension_Primitive);
            Loop_Counter := 0;
            P2.Loop_Counter := 0;
            Measurements.Reset;
         else
            Measurements.Finish;
         end if;
      end if;

      Loop_Counter := Loop_Counter + 1;

      Next_Time := Start_Time + Period;
      
      --  DIO.Put (P2.Suspension_Primitives'Pos (P2.Suspension_Primitive));

      case P2.Suspension_Primitive is
         when P2.Delay_Until_Past_Time =>
            delay until Ada.Real_Time.Time_First;
            --when P2.Delay_Until =>
            --   delay until Ada.Real_Time.Time_First; --  Next_Time;
         when P2.Suspension_Object =>
            Ada.Synchronous_Task_Control.Suspend_Until_True (P2.SO);
         when P2.Yield =>
            System.OS_Interface.Running_Thread_Ends_Job_Without_Blocking;
         when P2.PO_Entry =>
            P2.PO.Wait;
         when P2.PO_Entry_Param =>
            P2.PO_Param.Wait (I'Access);
         when others =>
            delay until Ada.Real_Time.Time_First;
      end case;
   end Task_1_Body;
   
   ------------
   -- Task_1 --
   ------------

   Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_1_Init'Access,
      Body_Ac  => Task_1_Body'Access,
      Priority => P2.Task_2_Prio);

end CS_Activations_1;
