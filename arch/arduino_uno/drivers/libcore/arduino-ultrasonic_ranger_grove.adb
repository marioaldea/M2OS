--------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding for the "Grove - Ultrasonic Ranger" library:
--    https://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/
--
----------------------------------------------------------------------------
with Interfaces.C;

package body Arduino.Ultrasonic_Ranger_Grove is

   ----------------
   -- Ultrasonic --
   ----------------

   type Ultrasonic_Filling is
     array(1 .. Ultrasonic_Size_In_Bytes) of Interfaces.Unsigned_8;

   type Ultrasonic is limited record
      Filling: Ultrasonic_Filling;
   end record;
   pragma Import (CPP, Ultrasonic);
   for Ultrasonic'Size use Ultrasonic_Size_In_Bytes * 8;
   for Ultrasonic'Alignment use Standard'Maximum_Alignment;

   ------------------------
   -- Imported functions --
   ------------------------

   function New_Ultrasonic return Ultrasonic;
   pragma CPP_Constructor (New_Ultrasonic, "_ZN10UltrasonicC1Ei");
   --  Do not use, just to avoid GNAT warning

   function New_Ultrasonic (Pin : Integer) return Ultrasonic;
   pragma CPP_Constructor (New_Ultrasonic, "_ZN10UltrasonicC1Ei");

   function Measure_In_Centimeters_Cpp (This : access Ultrasonic)
                                        return Interfaces.C.Long
     with Import, Convention => CPP,
     External_Name => "_ZN10Ultrasonic20MeasureInCentimetersEv";

   function Measure_In_Millimeters_Cpp (This : access Ultrasonic)
                                        return Interfaces.C.Long
     with Import, Convention => CPP,
     External_Name => "_ZN10Ultrasonic20MeasureInMillimetersEv";

   function Measure_In_Inches_Cpp (This : access Ultrasonic)
                                   return Interfaces.C.Long
     with Import, Convention => CPP,
     External_Name => "_ZN10Ultrasonic15MeasureInInchesEv";

   ------------
   -- Ranger --
   ------------

   package body Ranger is

      Sensor : aliased Ultrasonic := New_Ultrasonic (Integer (Pin));

      function Measure_In_Centimeters return Interfaces.C.Long is
        (Measure_In_Centimeters_Cpp (Sensor'Access));

      function Measure_In_Millimeters return Interfaces.C.Long is
        (Measure_In_Millimeters_Cpp (Sensor'Access));

      function Measure_In_Inches return Interfaces.C.Long is
        (Measure_In_Inches_Cpp (Sensor'Access));

   end Ranger;

end Arduino.Ultrasonic_Ranger_Grove;
