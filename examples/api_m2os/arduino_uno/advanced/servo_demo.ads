----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of using Arduino.Servo package.
--  Arduino.Servo is a binding to the Servo.h standar Arduino library.
--  This example uses two servos simustaneously.
--  For the test you can connect both or only one.
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |          Pulse|-----------------|D3                   |
--     | Servo      VDD|-----------------|VDD                  |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 |         Arduino Uno |
--     -----------------                 |                     |
--     |               |                 |                     |
--     |          Pulse|-----------------|D5                   |
--     | Servo      VDD|-----------------|VDD                  |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--
--  --------------------------------------------------------------------------
with System;
with Ada.Real_Time;

with Arduino;

package Servo_Demo is
   Pin_Servo_0 : constant Arduino.PWM_Digital_Pin := Arduino.PWM_D3;
   Pin_Servo_1 : constant Arduino.PWM_Digital_Pin := Arduino.PWM_D5;
   Main_Task_Prio : constant System.Priority := 10;
   Main_Task_Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (400);
end Servo_Demo;
