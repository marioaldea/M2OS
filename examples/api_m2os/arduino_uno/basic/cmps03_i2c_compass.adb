----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Reads the bearing provided by the CMPS03 Magnetic Compass using the I2C bus.
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |           Pin1|-----------------|VDD                  |
--     |           Pin2|-----------------|A5                   |
--     |           Pin3|-----------------|A4                   |
--     | CMPS03    Pin4|                 |         Arduino Uno |
--     |           Pin5|                 |                     |
--     |           Pin6|                 |                     |
--     |           Pin7|                 |                     |
--     |           Pin8|                 |                     |
--     |           Pin9|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--
--  Based on: https://www.robot-electronics.co.uk/htm/arduino_examples.htm#CMPS03%20Magnetic%20Compass

with Interfaces.C;

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure CMPS03_I2C_Compass is
   package DIO renames M2.Direct_IO;

   Error : Arduino.Wire.Transmission_Result;

   CMPS03_Address : constant := 16#60#;
   CMPS03_Data_Reg : constant := 2;

   Num_Bytes : Interfaces.C.Size_T;
   High_Byte, Low_Byte : Interfaces.Unsigned_16;

   use type Arduino.Wire.Transmission_Result, Interfaces.C.Int,
       Interfaces.Unsigned_16;

begin
   Num_Bytes := Interfaces.C.Size_T'Size;
   DIO.Put_Line ("CMPS03_I2C_Compass" & Num_Bytes'Img);

   Arduino.Wire.Begin_Two_Wire (Arduino.Wire.Wire'Access);

   loop
      DIO.Put_Line ("Begin transmission...");

      Arduino.Wire.Begin_Transmission(Arduino.Wire.Wire'Access,
                                      CMPS03_Address);
      Num_Bytes := Arduino.Wire.Write (Arduino.Wire.Wire'Access,
                                       CMPS03_Data_Reg);
      Error := Arduino.Wire.End_Transmission(Arduino.Wire.Wire'Access);

      if Error = Arduino.Wire.OK then
         DIO.Put_Line (" Reading data...");
         Num_Bytes := Arduino.Wire.Request_From (Arduino.Wire.Wire'Access,
                                                 CMPS03_Address,
                                                 Quantity => 2);
         loop
            exit when Arduino.Wire.Available (Arduino.Wire.Wire'Access) >= 2;
         end loop;

         High_Byte := Interfaces.Unsigned_16
           (Arduino.Wire.Read (Arduino.Wire.Wire'Access));
         Low_Byte := Interfaces.Unsigned_16
           (Arduino.Wire.Read (Arduino.Wire.Wire'Access));

         DIO.Put (Integer (High_Byte), 16);
         DIO.New_Line;
         DIO.Put (Integer (Low_Byte), 16);
         DIO.New_Line;

         High_Byte := Interfaces.Shift_Left (High_Byte, 8);
         High_Byte := (High_Byte + Low_Byte) / 10;

         DIO.Put ("Bearing: " & High_Byte'Img);
         DIO.New_Line;

      else
         DIO.Put (" I2C error");
         DIO.New_Line;
      end if;

      Arduino.C_Delay (1_000);
   end loop;
end CMPS03_I2C_Compass;
