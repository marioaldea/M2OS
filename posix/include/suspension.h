//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// M2OS POSIX-like layer.
// Ada "Suspension Objects".
// "Suspension Objects" are a very simple synchronization primitive that allows
// a single thread to suspend in a call to 'suspension_suspend_until_true()'
// until another thread activates it using 'suspension_set_true()'.
// The M2OS POSIX-like layer is in a preliminary stage.
#ifndef	_M2_SUSPENSION_H_
#define _M2_SUSPENSION_H_
#include <m2_api.h>
#include <stdbool.h>

#define SUSPENSION_INITIALIZER {.filling = {0}};

static inline int suspension_set_true (suspension_t *so) {
  ada__synchronous_task_control__set_true (so);
  return 0;
}

static inline int suspension_set_false (suspension_t *so) {
  ada__synchronous_task_control__set_false (so);
  return 0;
}

static inline bool suspension_current_state (suspension_t *so) {
  return ada__synchronous_task_control__current_state (so);
}

static inline int suspension_suspend_until_true (suspension_t *so) {
  suspend_until_true (so);
  return 0;
}

#endif // _M2_SUSPENSION_H_
