----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with M2.HAL;
with System;  --  M2OS to solve a GNAT internal error
with M2.Kernel.CPU_Time;
with Interfaces.C;
--  with M2.Kernel.Timing_Events;

package M2.Kernel.TCBs is
   --  pragma No_Elaboration_Code_All;
   --  pragma Preelaborate;
   use type Interfaces.C.Unsigned_Char;

   type TCB; -- is limited private;
   --  Thread Control Block

   type TCB_Ac is access all TCB;

   Null_TCB_Ac : constant TCB_Ac;

   Blocked_Activation_Time : constant := HAL.HWTime'Last;
   Initial_Activation_Time : constant := 0;
   Order_Counter_Last : constant := Interfaces.Unsigned_8'Last;

   procedure Initialize_TCB
     (Th_Ac : TCB_Ac;
      Id : Kernel.Thread_Id;
      Priority : M2.Kernel.Priority;
      Body_Ac : HAL.Code_Address;
      Args : System.Address);

   procedure Set_Job_Body (Th : in out TCB;
                           Job_Body_Ac : HAL.Code_Address)
     with Inline_Always;

   procedure Show  (Th : in TCB; Level : M2.Trace_Levels := M2.Trace);

   type TCB is record
      Next : TCB_Ac;

      Ret_Addr : HAL.Code_Address;
      --  Memory address to execute when the thread is dispatched (returns to
      --  the processor).
      --  It can take three values:
      --  - During the the first activation of an Ada task: points to
      --    Scheduler.Thread_Body_Wrapper.
      --  - Most of the time: points to the starting address of the body of
      --    the thread (the same value that the field Body_Ac).
      --  - When the thread calls to "yield_to_higher": points to the return
      --    lavel defined in Dispatcher.Dispatch_After_Yield_To_Higher.

      Priority : Kernel.Priority;

      Id : Kernel.Thread_Id;

      Order_Counter : Interfaces.Unsigned_8;
      -- To keep FIFO order when threads have equal Activation_Time

      Body_Ac : HAL.Code_Address;
      --  - During the the first activation of an Ada task: body of the Ada task
      --  type. It calls API.Set_Job_Body_Of_Running_Thread to set the
      --  definitive body of the task.
      --  - In subsecuent activations used to store the body of the thread.
      --  - In POSIX threads this field points to the body of the thread from
      --  its creation with pthread_create().

      Aux_Args : System.Address;
      --  - In the first activation used to pass the task discriminants.
      --  - In subsecuent activations used to store the previous value of
      --  Stack_Base in a thread that has called Yield_To_Higher.

      Stack_Base : HAL.Stack_Address;
      --  Stack base of the current thread activation.

      Activation_Time : HAL.HWTime;
      --  Time the task will be/was activated.
      --  Equal to HWTime'Last if the task is blocked.

      CPU_Time : M2.Kernel.CPU_Time.CPU_Time;
      --  CPU time consumed by the task
   end record;
   pragma Suppress_Initialization (TCB);
   --  Changes in TCB must be synchronized with System.Thread_T_Size_In_Bytes

   TCB_Size_In_Bytes : constant Interfaces.C.Unsigned_Char :=
     Interfaces.C.Unsigned_Char (TCB'Size / 8);

private
   Null_TCB_Ac : constant TCB_Ac := null;

end M2.Kernel.TCBs;
