//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Juan Rom�n Pe�a
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Simple example of use of the M2OS POSIX-like layer.
// Three periodic threads to test scheduling is performed well in different
// situations.
//
//   th1 ##......_##.....__##....##......              ^ = thread activation
//       ^       ^       ^       ^       ^             # = thread execution
//   th2 __###....._###......###.......###             _ = thread active
//       ^         ^         ^         ^               . = thread suspended
//   th3 _____####...__####......__####.
//       ^           ^           ^
//       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6

#include "../reports/tests_reports.h"
#include <m2_api.h>
#include <pthread.h>
#include <stdio.h>
#include <time.h>

THREAD_POOL (3);

#define TIME_UNIT_NS 50000000 // -> 0.05 seg
#define NUM_OF_LOOPS_TH2 4

const struct timespec period1 = TS (0,  8 * TIME_UNIT_NS); // -> 0.4 seg
const struct timespec period2 = TS (0, 10 * TIME_UNIT_NS); // -> 0.5 seg
const struct timespec period3 = TS (0, 12 * TIME_UNIT_NS); // -> 0.6 seg

const struct timespec eat1 = TS (0, 2 * TIME_UNIT_NS); // -> 0.1 seg
const struct timespec eat2 = TS (0, 3 * TIME_UNIT_NS); // -> 0.15 seg
const struct timespec eat3 = TS (0, 4 * TIME_UNIT_NS); // -> 0.2 seg

int counter1 = 0;
int counter2 = 0;
int counter3 = 0;
int global_counter = 0;

void
print_counters ()
{
  puts (" Counter1: ");
  putint (counter1);
  puts (" ");
  puts ("Counter2: ");
  putint (counter2);
  puts (" ");
  puts ("Counter3: ");
  putint (counter3);
  puts (" ");
  puts ("Global: ");
  putint (global_counter);
  puts ("\n");
}


void
putts (struct timespec ts)
{
  putint (TS_SEC (ts));
  puts ("s ");
  putint (TS_NSEC (ts));
  puts ("ns");
}

//****************//
//  thread_body1  //
//****************//
struct timespec next_activation_time1;

void *
thread_body1 (void *arg)
{
  puts ("Thread 1\n");
  switch (counter1)
    {
    case 0:
      print_counters ();
      tests_reports__assert (counter2 == 0);
      tests_reports__assert (counter3 == 0);
      tests_reports__assert (global_counter == 0);
      break;
    case 1:
      print_counters ();
      tests_reports__assert (counter2 == 1);
      tests_reports__assert (counter3 == 1);
      tests_reports__assert (global_counter == 3);
      break;
    case 2:
      print_counters ();
      tests_reports__assert (counter2 == 2);
      tests_reports__assert (counter3 == 2);
      tests_reports__assert (global_counter == 6);
      break;
    case 3:
      print_counters ();
      tests_reports__assert (counter2 == 3);
      tests_reports__assert (counter3 == 2);
      tests_reports__assert (global_counter == 8);
      break;
    default: // unexpected value
      print_counters ();
      tests_reports__assert (0);
    }

  counter1++;
  global_counter++;

  // EAT TIME
  tests_reports__eat(eat1);

  TS_INC (next_activation_time1, period1);
  clock_nanosleep (CLOCK_MONOTONIC, TIMER_ABSTIME, &next_activation_time1,
                   NULL);

  return NULL;
}

//****************//
//  thread_body2  //
//****************//
struct timespec next_activation_time2 = TS (0, 0);

void *
thread_body2 (void *arg)
{
  puts ("Thread 2 \n");
  switch (counter2)
    {
    case 0:
      print_counters ();
      tests_reports__assert (counter1 == 1);
      tests_reports__assert (counter3 == 0);
      tests_reports__assert (global_counter == 1);
      break;
    case 1:
      print_counters ();
      tests_reports__assert (counter1 == 2);
      tests_reports__assert (counter3 == 1);
      tests_reports__assert (global_counter == 4);
      break;
    case 2:
      print_counters ();
      tests_reports__assert (counter1 == 3);
      tests_reports__assert (counter3 == 2);
      tests_reports__assert (global_counter == 7);
      break;
    case 3:
      print_counters ();
      tests_reports__assert (counter1 == 4);
      tests_reports__assert (counter3 == 3);
      tests_reports__assert (global_counter == 10);
      break;
    default: // unexpected value
      print_counters ();
      tests_reports__assert (0);
    }

  counter2++;
  global_counter++;

  // EAT TIME
  tests_reports__eat(eat2);

  if (counter2 == NUM_OF_LOOPS_TH2) {
    tests_reports__test_ok ();
  }

  TS_INC (next_activation_time2, period2);
  clock_nanosleep (CLOCK_MONOTONIC, TIMER_ABSTIME, &next_activation_time2,
                   NULL);

  return NULL;
}

//****************//
//  thread_body3  //
//****************//
struct timespec next_activation_time3 = TS (0, 0);

void *
thread_body3 (void *arg)
{
  puts ("Thread 3 \n");

  switch (counter3)
    {
    case 0:
      print_counters ();
      tests_reports__assert (counter1 == 1);
      tests_reports__assert (counter2 == 1);
      tests_reports__assert (global_counter == 2);
      break;
    case 1:
      print_counters ();
      tests_reports__assert (counter1 == 2);
      tests_reports__assert (counter2 == 2);
      tests_reports__assert (global_counter == 5);
      break;
    case 2:
      print_counters ();
      tests_reports__assert (counter1 == 4);
      tests_reports__assert (counter2 == 3);
      tests_reports__assert (global_counter == 9);
      break;
    default: // unexpected value
      print_counters ();
      tests_reports__assert (0);
    }

  // UPDATE COUNTERS AND ACTIVATION TIME
  counter3++;
  global_counter++;

  // EAT TIME
  tests_reports__eat(eat3);

  TS_INC (next_activation_time3, period3);
  clock_nanosleep (CLOCK_MONOTONIC, TIMER_ABSTIME, &next_activation_time3,
                   NULL);

  return NULL;
}

//**********//
//   main   //
//**********//
int
main (int argc, char **argv)
{
  m2osinit ();
  puts ("Main\n");
  check_posix_api ();

  pthread_t th1;
  pthread_t th2;
  pthread_t th3;
  pthread_attr_t attr1;
  pthread_attr_t attr2;
  pthread_attr_t attr3;
  struct sched_param sch_param1 = { .sched_priority = 3 };
  struct sched_param sch_param2 = { .sched_priority = 2 };
  struct sched_param sch_param3 = { .sched_priority = 1 };

  clock_gettime (CLOCK_MONOTONIC, &next_activation_time1);
  next_activation_time2 = next_activation_time1;
  next_activation_time3 = next_activation_time1;

  pthread_attr_init (&attr1);
  pthread_attr_init (&attr2);
  pthread_attr_init (&attr3);
  pthread_attr_setschedparam (&attr1, &sch_param1);
  pthread_attr_setschedparam (&attr2, &sch_param2);
  pthread_attr_setschedparam (&attr3, &sch_param3);

  pthread_create (&th1, &attr1, thread_body1, NULL);
  pthread_create (&th2, &attr2, thread_body2, NULL);
  pthread_create (&th3, &attr3, thread_body3, NULL);

  puts ("Main ends\n");

  return 0;
}
