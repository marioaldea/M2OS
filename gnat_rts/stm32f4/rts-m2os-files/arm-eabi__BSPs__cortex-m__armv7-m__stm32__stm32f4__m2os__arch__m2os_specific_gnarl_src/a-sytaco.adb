--  This is the M2OS version of this package

package body Ada.Synchronous_Task_Control is

   use type OSI.Thread_Ac;

   -------------------
   -- Current_State --
   -------------------

   function Current_State (S : Suspension_Object) return Boolean is
   begin
      return S.State;
   end Current_State;

   ---------------
   -- Set_False --
   ---------------

   procedure Set_False (S : in out Suspension_Object) is
   begin
      S.State := False;
   end Set_False;

   --------------
   -- Set_True --
   --------------

   procedure Set_True (S : in out Suspension_Object) is
   begin
      --  The procedure Suspend_Until_True blocks the calling task until the
      --  state of the object S is true; at that point the task becomes ready
      --  and the state of the object becomes false (ARM D.10 par. 9)

      if S.Suspended_Thread /= OSI.Null_Thread_Ac then

         S.State := False;

         OSI.Activate_Blocked_Thread (S.Suspended_Thread);
         S.Suspended_Thread := OSI.Null_Thread_Ac;

      else
         S.State := True;
      end if;
   end Set_True;

   ------------------------
   -- Suspend_Until_True --
   ------------------------

   procedure Suspend_Until_True (S : in out Suspension_Object) is
   begin
      if S.Suspended_Thread /= OSI.Null_Thread_Ac then
         --  Program_Error is raised if another task is already waiting on that
         --  suspension object (ARM D.10 par. 10)

         raise Program_Error;
      end if;

      if S.State then
         S.State := False;

         OSI.Running_Thread_Ends_Job_Without_Blocking;

      else
         --  State = False => Suspend the task

         S.Suspended_Thread := OSI.Self;
         OSI.Block_Running_Thread;
      end if;
   end Suspend_Until_True;

end Ada.Synchronous_Task_Control;
