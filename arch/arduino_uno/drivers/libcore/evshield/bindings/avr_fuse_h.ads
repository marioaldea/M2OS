pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package avr_fuse_h is

   --  unsupported macro: FUSEMEM __attribute__((__used__, __section__ (".fuse")))
   --  unsupported macro: FUSES __fuse_t __fuse FUSEMEM
   type uu_fuse_t is record
      low : aliased unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/fuse.h:241
      high : aliased unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/fuse.h:242
      extended : aliased unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/fuse.h:243
   end record;
   pragma Convention (C_Pass_By_Copy, uu_fuse_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/fuse.h:244

   --  skipped anonymous struct anon_9

end avr_fuse_h;
