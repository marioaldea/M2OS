----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Demo program that uses two servos managed by two tasks.
--  See servos_two_tasks.ads for details.

with Arduino.Servo;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

package body Servos_Two_Tasks.Tasks is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;
   use type Arduino.Servo.Angle_Deg, Arduino.Servo.Signed_Angle_Deg;

   Max_Angle : constant := 170;
   Min_Angle : constant := 0;

   ------------------
   -- Servo_Task_0 --
   ------------------

   Servo_0 : aliased Arduino.Servo.Servo;
   Next_Time_0 : RT.Time;
   Angle_0 : Arduino.Servo.Angle_Deg := Min_Angle;
   Angle_Increment_0 : Arduino.Servo.Signed_Angle_Deg := -6;

   procedure Servo_Task_0_Init is
      Ret : Arduino.Servo.Attach_Ret;
      use type Arduino.Servo.Attach_Ret;
   begin
      DIO.Put_Line ("Servo_Task_0_Init");
      Ret := Arduino.Servo.Attach (Servo_0'Access, Pin => Pin_Servo_0);

      Next_Time_0 := RT.Clock;
   end Servo_Task_0_Init;

   procedure  Servo_Task_0_Body is
   begin
      if Angle_0 >= Max_Angle or Angle_0 <= Min_Angle then
         Angle_Increment_0 := -Angle_Increment_0;
         DIO.Put (" A0:");
         DIO.Put (Integer (Angle_0));
      end if;

      Angle_0 := Angle_0 + Angle_Increment_0;

      Arduino.Servo.Write (Servo_0'Access, Angle => Angle_0);

      Next_Time_0 := Next_Time_0 + Servos_Two_Tasks.Period_Task_Servo_0;
      delay until Next_Time_0;
   end Servo_Task_0_Body;

   Servo_Task_0 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Servo_Task_0_Init'Unrestricted_Access,
      Body_Ac  => Servo_Task_0_Body'Unrestricted_Access,
      Priority => Servos_Two_Tasks.Prio_Task_Servo_0);

   ------------------
   -- Servo_Task_1 --
   ------------------

   Servo_1 : aliased Arduino.Servo.Servo;
   Next_Time_1 : RT.Time;
   Angle_1 : Arduino.Servo.Angle_Deg := Min_Angle;
   Angle_Increment_1 : Arduino.Servo.Signed_Angle_Deg := -6;

   procedure Servo_Task_1_Init is
      Ret : Arduino.Servo.Attach_Ret;
   begin
      DIO.Put_Line ("Servo_Task_1_Init");
      Ret := Arduino.Servo.Attach (Servo_1'Access, Pin => Pin_Servo_1);

      Next_Time_1 := RT.Clock;
   end Servo_Task_1_Init;

   procedure  Servo_Task_1_Body is
   begin
      if Angle_1 >= Max_Angle or Angle_1 <= Min_Angle then
         Angle_Increment_1 := -Angle_Increment_1;
         DIO.Put (" A1:");
         DIO.Put (Integer (Angle_1));
      end if;

      Angle_1 := Angle_1 + Angle_Increment_1;

      Arduino.Servo.Write (Servo_1'Access, Angle => Angle_1);

      Next_Time_1 := Next_Time_1 + Servos_Two_Tasks.Period_Task_Servo_1;
      delay until Next_Time_1;
   end Servo_Task_1_Body;

   Servo_Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Servo_Task_1_Init'Unrestricted_Access,
      Body_Ac  => Servo_Task_1_Body'Unrestricted_Access,
      Priority => Servos_Two_Tasks.Prio_Task_Servo_1);

end Servos_Two_Tasks.Tasks;
