pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package USBAPI_h is

   USB_EP_SIZE : constant := 64;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/USBAPI.h:38

   subtype u8 is unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/USBAPI.h:29

   subtype u16 is unsigned_short;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/USBAPI.h:30

   subtype u32 is unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/USBAPI.h:31

end USBAPI_h;
