----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of evs_nxtcolor.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with SHDefines_h;
with stdint_h;
with EVs_NXTColor_h;
with Arduino.EVShield;


package body Arduino.EVShield.NXTColor is
   package body Color is
      -------------------------
      -- EVs_NXTColor object --
      -------------------------
      color : aliased EVs_NXTColor_h.Class_EVs_NXTColor.EVs_NXTColor := EVs_NXTColor_h.Class_EVs_NXTColor.New_EVs_NXTColor;

      aux : Extensions.bool:= EVs_NXTColor_h.Class_EVs_NXTColor.init(color'Access,Arduino.EVShield.EVShield_obj'Access, bp);

      function Set_Color ( SH_TYPE : stdint_h.uint8_t ) return Extensions.bool is
      begin
         return EVs_NXTColor_h.Class_EVs_NXTColor.setType(color'Access, SH_TYPE);
      end Set_Color;

      function Read_Value return stdint_h.uint8_t is
      begin
         return EVs_NXTColor_h.Class_EVs_NXTColor.readValue(color'Access);
      end Read_Value;

      function Read_Color return stdint_h.uint8_t is
      begin
       return EVs_NXTColor_h.Class_EVs_NXTColor.readColor(color'Access);
      end Read_Color;

   end Color;

end Arduino.EVShield.NXTColor;
