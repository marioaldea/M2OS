pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;

package stdint_h is

   INT8_MAX : constant := 16#7f#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:307
   --  unsupported macro: INT8_MIN (-INT8_MAX - 1)
   --  unsupported macro: UINT8_MAX (INT8_MAX * 2 + 1)

   INT16_MAX : constant := 16#7fff#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:336
   --  unsupported macro: INT16_MIN (-INT16_MAX - 1)
   --  unsupported macro: UINT16_MAX (__CONCAT(INT16_MAX, U) * 2U + 1U)

   INT32_MAX : constant := 16#7fffffff#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:351
   --  unsupported macro: INT32_MIN (-INT32_MAX - 1L)
   --  unsupported macro: UINT32_MAX (__CONCAT(INT32_MAX, U) * 2UL + 1UL)

   INT64_MAX : constant := 16#7fffffffffffffff#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:368
   --  unsupported macro: INT64_MIN (-INT64_MAX - 1LL)
   --  unsupported macro: UINT64_MAX (__CONCAT(INT64_MAX, U) * 2ULL + 1ULL)
   --  unsupported macro: INT_LEAST8_MAX INT8_MAX
   --  unsupported macro: INT_LEAST8_MIN INT8_MIN
   --  unsupported macro: UINT_LEAST8_MAX UINT8_MAX
   --  unsupported macro: INT_LEAST16_MAX INT16_MAX
   --  unsupported macro: INT_LEAST16_MIN INT16_MIN
   --  unsupported macro: UINT_LEAST16_MAX UINT16_MAX
   --  unsupported macro: INT_LEAST32_MAX INT32_MAX
   --  unsupported macro: INT_LEAST32_MIN INT32_MIN
   --  unsupported macro: UINT_LEAST32_MAX UINT32_MAX
   --  unsupported macro: INT_LEAST64_MAX INT64_MAX
   --  unsupported macro: INT_LEAST64_MIN INT64_MIN
   --  unsupported macro: UINT_LEAST64_MAX UINT64_MAX
   --  unsupported macro: INT_FAST8_MAX INT8_MAX
   --  unsupported macro: INT_FAST8_MIN INT8_MIN
   --  unsupported macro: UINT_FAST8_MAX UINT8_MAX
   --  unsupported macro: INT_FAST16_MAX INT16_MAX
   --  unsupported macro: INT_FAST16_MIN INT16_MIN
   --  unsupported macro: UINT_FAST16_MAX UINT16_MAX
   --  unsupported macro: INT_FAST32_MAX INT32_MAX
   --  unsupported macro: INT_FAST32_MIN INT32_MIN
   --  unsupported macro: UINT_FAST32_MAX UINT32_MAX
   --  unsupported macro: INT_FAST64_MAX INT64_MAX
   --  unsupported macro: INT_FAST64_MIN INT64_MIN
   --  unsupported macro: UINT_FAST64_MAX UINT64_MAX
   --  unsupported macro: INTPTR_MAX INT16_MAX
   --  unsupported macro: INTPTR_MIN INT16_MIN
   --  unsupported macro: UINTPTR_MAX UINT16_MAX
   --  unsupported macro: INTMAX_MAX INT64_MAX
   --  unsupported macro: INTMAX_MIN INT64_MIN
   --  unsupported macro: UINTMAX_MAX UINT64_MAX
   --  unsupported macro: PTRDIFF_MAX INT16_MAX
   --  unsupported macro: PTRDIFF_MIN INT16_MIN
   --  unsupported macro: SIG_ATOMIC_MAX INT8_MAX
   --  unsupported macro: SIG_ATOMIC_MIN INT8_MIN
   --  unsupported macro: SIZE_MAX UINT16_MAX
   --  unsupported macro: WCHAR_MAX __WCHAR_MAX__
   --  unsupported macro: WCHAR_MIN __WCHAR_MIN__
   --  unsupported macro: WINT_MAX __WINT_MAX__
   --  unsupported macro: WINT_MIN __WINT_MIN__
   --  arg-macro: procedure INT8_C (c)
   --    __INT8_C(c)
   --  arg-macro: procedure INT16_C (c)
   --    __INT16_C(c)
   --  arg-macro: procedure INT32_C (c)
   --    __INT32_C(c)
   --  arg-macro: procedure INT64_C (c)
   --    __INT64_C(c)
   --  arg-macro: procedure UINT8_C (c)
   --    __UINT8_C(c)
   --  arg-macro: procedure UINT16_C (c)
   --    __UINT16_C(c)
   --  arg-macro: procedure UINT32_C (c)
   --    __UINT32_C(c)
   --  arg-macro: procedure UINT64_C (c)
   --    __UINT64_C(c)
   --  arg-macro: procedure INTMAX_C (c)
   --    __INTMAX_C(c)
   --  arg-macro: procedure UINTMAX_C (c)
   --    __UINTMAX_C(c)

   subtype int8_t is signed_char;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:125

   subtype uint8_t is unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:126

   subtype int16_t is int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:127

   subtype uint16_t is unsigned;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:128

   subtype int32_t is long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:129

   subtype uint32_t is unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:130

   subtype int64_t is Long_Long_Integer;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:132

   subtype uint64_t is Extensions.unsigned_long_long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:133

   subtype intptr_t is int16_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:146

   subtype uintptr_t is uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:151

   subtype int_least8_t is int8_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:163

   subtype uint_least8_t is uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:168

   subtype int_least16_t is int16_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:173

   subtype uint_least16_t is uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:178

   subtype int_least32_t is int32_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:183

   subtype uint_least32_t is uint32_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:188

   subtype int_least64_t is int64_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:196

   subtype uint_least64_t is uint64_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:203

   subtype int_fast8_t is int8_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:217

   subtype uint_fast8_t is uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:222

   subtype int_fast16_t is int16_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:227

   subtype uint_fast16_t is uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:232

   subtype int_fast32_t is int32_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:237

   subtype uint_fast32_t is uint32_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:242

   subtype int_fast64_t is int64_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:250

   subtype uint_fast64_t is uint64_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:257

   subtype intmax_t is int64_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:277

   subtype uintmax_t is uint64_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdint.h:282

end stdint_h;
