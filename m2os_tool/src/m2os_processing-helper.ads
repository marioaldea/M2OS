
package M2os_Processing.Helper is

   -- Package with procedures to write procedure and package headers in the
   -- modified files

   function Get_Node_As_String (N : Ada_Node'Class) return String;

   -- Procedure to write variables
   -- procedure Write_Variable (Variable : Ada_Node'Class);

   -- Procedure to write task's local variables as global variables
   procedure Write_Global_Variable (Variable  : Ada_Node'Class;
                                    Task_Name : String);

   -- Procedure to rename a variable inside a task
   procedure Write_Rename_Variable (The_Name : String; The_Type : String; Task_Name : String);

   -- Write package spec header
   procedure Write_Package_Spec_Header (Name : String);

   -- Write package body header
   procedure Write_Package_Body_Header (Name : String);

   -- Write end package header
   procedure Write_End_Package_Header (Name : String);

   -- Write procedure header
   procedure Write_Procedure_Header (Name : String);

   -- Write procedure body header for a task
   procedure Write_Procedure_Body_Header (Name : String);

   -- Write end procedure header for a task
   procedure Write_End_Procedure_Body (Name : String);

end M2os_Processing.Helper;
