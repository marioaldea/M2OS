pragma Warnings (Off);
with M2.Kernel.Scheduler;
pragma Elaborate_All (M2.Kernel.Scheduler);
pragma Warnings (On);

with Ada.Synchronous_Task_Control;

package CS_Suspension_Object_2 is
   pragma Elaborate_Body;

   Short_Time : constant Boolean := False;
   Num_Of_Loops : constant := (if Short_Time then 8 else 300);


   Loop_Counter : Integer := 0;

   Suspension_Object_1 : Ada.Synchronous_Task_Control.Suspension_Object;
   Suspension_Object_2 : Ada.Synchronous_Task_Control.Suspension_Object;

   task Other_Task;

end CS_Suspension_Object_2;
