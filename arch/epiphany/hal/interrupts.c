/****************************************************************************
*                                  M2OS
*
*                           Copyright (C) 2022
*                    Universidad de Cantabria, SPAIN
*
*  Authors: David Garcia Villaescusa (garciavd@unican.es)
*           Mario Aldea Rivas (aldeam@unican.es)
*
*  This file is part of M2OS.
*
*  M2OS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  M2OS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/

#include "e_lib.h"
#include "synchronization.c"
#define SET_START_TIME 108
#define WAIT 1000
#define MS_VALUE 600000

void timer_isr();
extern unsigned now_timer;

static volatile int interrupted = 0;

void interrupts_set_timer_counter(){
  volatile int *barrier = (int*)0x7550;
  e_coreid_t coreid;
  unsigned my_row, my_col;
  
  *barrier = 0;
  
  coreid = e_get_coreid ();
  e_coords_from_coreid (coreid, &my_row, &my_col);

  if (my_row == 0 && my_col == 0) {
    // If 0x0 core
    // INIT THE TIMERS
  
    *barrier = 600000;
    sync_timers (*barrier);
   
  } else {
    // If other core
    // WAIT FOR IT
    while (*barrier == 0) {
      NULL;
    }
    
  }
  e_irq_attach(E_TIMER0_INT, timer_isr);
  e_ctimer_set(E_CTIMER_0, *barrier);
  e_ctimer_start(E_CTIMER_0, E_CTIMER_CLK);
}

void interrupts_enable_timer0() {
  e_irq_mask(E_TIMER0_INT, E_FALSE);
}

void interrupts_disable_timer0() {
  e_irq_mask(E_TIMER0_INT, E_TRUE);
}

void interrupts_enable_global() {
  e_irq_global_mask(E_FALSE);
}

void interrupts_disable_global() {
  e_irq_global_mask(E_TRUE);
}

/**
 * Return 2 if the interrupts are disabled, 0 if they are enabled.
 */
unsigned interrupts_disabled() {
  unsigned irqState = e_reg_read(E_REG_STATUS);
  if (interrupted) {
    return 2;
  }

  irqState = irqState & 0x2;
  return irqState;
}

unsigned interrupts_get_timer_value() {
  return e_ctimer_get(E_CTIMER_0);
}

unsigned interrupts_get_initial_timer_value() {
  return MS_VALUE;
}

extern unsigned char m2__hal__events_order_counter;

void (*interrupts__os_tick_handler)(unsigned now);

void __attribute__((interrupt)) timer_isr()
{
  interrupted = 1;
  // This ISR is called when the respective event occured (IOW, when
  // the event it was attached to by e_irq_attach() is raised).
  now_timer=now_timer+1;
  m2__hal__events_order_counter = 0;

  e_ctimer_set(E_CTIMER_0, MS_VALUE);
  e_ctimer_start(E_CTIMER_0, E_CTIMER_CLK);

  interrupts__os_tick_handler(now_timer);

  interrupted = 0;

  return;
}
