----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of EVSHield.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with EVShieldI2C_h;
with stdint_h;
with Interfaces.C.Extensions;
with SHDefines_h;
with Interfaces.C.Strings;
with Arduino;
with EVShield_h;

package body Arduino.EVShield is

   procedure Init is
   begin
       EVShield_h.Class_EVShield.init(EVShield_obj'Access, SHDefines_h.SH_HardwareI2C);
   end init;

   function Get_Button_State ( Btn : stdint_h.uint8_t ) return Extensions.bool is
   begin
       return EVShield_h.Class_EVShield.getButtonState(EVShield_obj'Access, btn);
   end Get_Button_State;

   package body Bank_A is
      function Led_Set_RGB
        (R : stdint_h.uint8_t;
         G : stdint_h.uint8_t;
         B : stdint_h.uint8_t) return Extensions.bool is
      begin
         return EVShield_h.Class_EVShieldBank.ledSetRGB(EVShield_obj.bank_a'Access,R,G,B);
      end Led_Set_RGB;
   end Bank_A;

   package body Bank_B is
      function Led_Set_RGB
        (R : stdint_h.uint8_t;
         G : stdint_h.uint8_t;
         B : stdint_h.uint8_t) return Extensions.bool is
      begin
         return EVShield_h.Class_EVShieldBank.ledSetRGB(EVShield_obj.bank_b.parent'Access,R,G,B);
      end Led_Set_RGB;
   end Bank_B;
begin
      EVShield_h.Class_EVShield.Init(EVShield_obj'Access,SHDefines_h.SH_HardwareI2C);

end Arduino.EVShield;
