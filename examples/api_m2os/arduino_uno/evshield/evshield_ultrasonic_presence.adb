----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on ev3_ultrasonic_presence.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
--  setup for this example:
--  attach EV3 Ultrasonic Sensor to Port BAS1
----------------------------------------------------------------------------

with Interfaces.C;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Interfaces.C.Extensions;
with stdint_h;
with Arduino.EVShield;
with Arduino.EVShield.EV3Ultrasonic;

procedure evShield_ultrasonic_presence is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, Interfaces.C.Extensions.bool;

   -- Program variables
   time : Arduino.Time_MS;
   value : stdint_h.uint8_t;
   aux : stdint_h.uint8_t;

   package Ultrasonic_sensor is new Arduino.EVShield.EV3Ultrasonic.Ultrasonic (Arduino.EVShield.SH_BAS1);

begin

   DIO.Put_Line ("--------------------------------------");
   DIO.Put_Line ("Starting EV3 Ultrasonic Sensor Presence Test program");
   DIO.Put_Line ("--------------------------------------");

   -- Set ultrasonic sensor mode.
   aux := Ultrasonic_sensor.Set_Mode(Ultrasonic_sensor.MODE_Sonar_Presence);

   DIO.Put_Line ("setup done");
   DIO.Put_Line ("turn another ultrasonic device on and off");

   -- Wait until user presses GO button to continue the program
   while not Arduino.EVShield.Get_Button_State(Arduino.EVShield.BTN_GO) loop
      time := Arduino.Millis;
      if time mod 1000 < 3 then
         DIO.Put_Line (" Press GO button to continue");
      end if;
   end loop;

   loop
      -- get the reading(s) form sensor
      value := Ultrasonic_sensor.Detect;

      -- print the sensor value(s)
      DIO.Put ("Device found? "); DIO.Put_Line (value'Img);

      -- wait for some time
      Arduino.C_Delay (1000);
   end loop;
end evShield_ultrasonic_presence;
