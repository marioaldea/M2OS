----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Task that control the robot based on the color measures.
----------------------------------------------------------------------------

pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Interfaces.C;

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Arduino.EVShield.EV3Touch;
with Arduino.EVShield.Motors;
with SHDefines_H; use SHDefines_H;
with Interfaces.C.Extensions;
with Stdint_H;
with EVShield_Color_Measure_Task; use EVShield_Color_Measure_Task;
with Ada.Real_Time;


package body EVShield_Lines_Control_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   ----------------
   -- Other_Task --
   ----------------

   Period : constant RT.Time_Span := RT.Milliseconds (200);

   Next_Time : RT.Time;
   Loop_Count : Natural := 0;
   -- Program variables
   --     Aux1 : Stdint_H.Uint8_T;
   --     Aux2 : Interfaces.C.Extensions.Bool;
   Count : Interfaces.C.Int;
   Aux2 : Interfaces.C.Extensions.Bool;

   --     Color_Left : Float;
   --     Color_Right : Stdint_H.Uint8_T;

   package Touch_Sensor is new Arduino.EVShield.EV3Touch.Touch (SHDefines_H.SH_BAS1);
   -- Bank A Motor 1
   package Motor_Left is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM1);
   --     -- Bank A Motor 2
   package Motor_Right is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM2);

   use type Interfaces.C.Int;
   use type Interfaces.C.Unsigned_Char;

   procedure Other_Task_Init is
   begin
      Next_Time := Ada.Real_Time.Clock;

      DIO.Put_Line ("-----------------------------");
      DIO.Put_Line ("Starting EVShield light demo.");
      DIO.Put_Line ("-----------------------------");
   end Other_Task_Init;


   procedure Other_Task_Body is
   begin
      Count := Touch_Sensor.Get_Bump_Count;

      if Count mod 2 = 1 then

         -- Sensir derecha (nxt) Negro 4
         --
         -- Sensor izq (Ev3) negro 3.0
         if EVShield_Color_Measure_Task.Color_Right = 4 then
            DIO.Put_Line ("Limite de linea detectado por la derecha --> Girar izquierda.");
            DIO.Put ("Color en el sensor derecho: "); DIO.Put_Line (Color_Right'Img);

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,0);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,40);

         elsif EVShield_Color_Measure_Task.Color_Left = 3.0 then
            DIO.Put_Line ("Limite de linea detectado por la izquierda --> Girar Light_Leftderecha.");
            DIO.Put ("Color en el sensor izquierda: "); DIO.Put (Color_Left); DIO.New_Line;

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,40);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,0);

         else
            DIO.Put_Line ("Siguiendo correcto la linea --> Sigue recto.");

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            10);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           10);

         end if;
         -- wait for some time
         Arduino.C_Delay (200);

      else
         DIO.Put_Line ("Demo is STOP. Press touch sensor to start.");
         Aux2 := Motor_Right.Motor_Stop(Motor_Right.SH_Next_Action_Brake);
         Aux2 := Motor_Left.Motor_Stop(Motor_Left.SH_Next_Action_Brake);
         -- wait for some time
         Arduino.C_Delay (1000);
      end if;


      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Other_Task_Body;

   Other_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Other_Task_Init'Access,
      Body_Ac  => Other_Task_Body'Access,
      Priority => 4);

end EVShield_Lines_Control_Task;

