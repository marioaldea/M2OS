pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with stdint_h;

package inttypes_h is

   PRId8 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:96

   PRIdLEAST8 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:99

   PRIdFAST8 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:102

   PRIi8 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:106

   PRIiLEAST8 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:109

   PRIiFAST8 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:112

   PRId16 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:117

   PRIdLEAST16 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:120

   PRIdFAST16 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:123

   PRIi16 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:127

   PRIiLEAST16 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:130

   PRIiFAST16 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:133

   PRId32 : aliased constant String := "ld" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:138

   PRIdLEAST32 : aliased constant String := "ld" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:141

   PRIdFAST32 : aliased constant String := "ld" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:144

   PRIi32 : aliased constant String := "li" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:148

   PRIiLEAST32 : aliased constant String := "li" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:151

   PRIiFAST32 : aliased constant String := "li" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:154
   --  unsupported macro: PRIdPTR PRId16
   --  unsupported macro: PRIiPTR PRIi16

   PRIo8 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:182

   PRIoLEAST8 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:185

   PRIoFAST8 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:188

   PRIu8 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:192

   PRIuLEAST8 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:195

   PRIuFAST8 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:198

   PRIx8 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:202

   PRIxLEAST8 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:205

   PRIxFAST8 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:208

   PRIX8 : aliased constant String := "X" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:212

   PRIXLEAST8 : aliased constant String := "X" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:215

   PRIXFAST8 : aliased constant String := "X" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:218

   PRIo16 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:223

   PRIoLEAST16 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:226

   PRIoFAST16 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:229

   PRIu16 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:233

   PRIuLEAST16 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:236

   PRIuFAST16 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:239

   PRIx16 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:243

   PRIxLEAST16 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:246

   PRIxFAST16 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:249

   PRIX16 : aliased constant String := "X" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:253

   PRIXLEAST16 : aliased constant String := "X" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:256

   PRIXFAST16 : aliased constant String := "X" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:259

   PRIo32 : aliased constant String := "lo" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:264

   PRIoLEAST32 : aliased constant String := "lo" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:267

   PRIoFAST32 : aliased constant String := "lo" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:270

   PRIu32 : aliased constant String := "lu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:274

   PRIuLEAST32 : aliased constant String := "lu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:277

   PRIuFAST32 : aliased constant String := "lu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:280

   PRIx32 : aliased constant String := "lx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:284

   PRIxLEAST32 : aliased constant String := "lx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:287

   PRIxFAST32 : aliased constant String := "lx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:290

   PRIX32 : aliased constant String := "lX" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:294

   PRIXLEAST32 : aliased constant String := "lX" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:297

   PRIXFAST32 : aliased constant String := "lX" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:300
   --  unsupported macro: PRIoPTR PRIo16
   --  unsupported macro: PRIuPTR PRIu16
   --  unsupported macro: PRIxPTR PRIx16
   --  unsupported macro: PRIXPTR PRIX16

   SCNd8 : aliased constant String := "hhd" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:344

   SCNdLEAST8 : aliased constant String := "hhd" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:347

   SCNdFAST8 : aliased constant String := "hhd" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:350

   SCNi8 : aliased constant String := "hhi" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:354

   SCNiLEAST8 : aliased constant String := "hhi" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:357

   SCNiFAST8 : aliased constant String := "hhi" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:360

   SCNd16 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:365

   SCNdLEAST16 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:368

   SCNdFAST16 : aliased constant String := "d" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:371

   SCNi16 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:375

   SCNiLEAST16 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:378

   SCNiFAST16 : aliased constant String := "i" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:381

   SCNd32 : aliased constant String := "ld" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:386

   SCNdLEAST32 : aliased constant String := "ld" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:389

   SCNdFAST32 : aliased constant String := "ld" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:392

   SCNi32 : aliased constant String := "li" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:396

   SCNiLEAST32 : aliased constant String := "li" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:399

   SCNiFAST32 : aliased constant String := "li" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:402
   --  unsupported macro: SCNdPTR SCNd16
   --  unsupported macro: SCNiPTR SCNi16

   SCNo8 : aliased constant String := "hho" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:429

   SCNoLEAST8 : aliased constant String := "hho" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:432

   SCNoFAST8 : aliased constant String := "hho" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:435

   SCNu8 : aliased constant String := "hhu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:439

   SCNuLEAST8 : aliased constant String := "hhu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:442

   SCNuFAST8 : aliased constant String := "hhu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:445

   SCNx8 : aliased constant String := "hhx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:449

   SCNxLEAST8 : aliased constant String := "hhx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:452

   SCNxFAST8 : aliased constant String := "hhx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:455

   SCNo16 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:459

   SCNoLEAST16 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:462

   SCNoFAST16 : aliased constant String := "o" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:465

   SCNu16 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:469

   SCNuLEAST16 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:472

   SCNuFAST16 : aliased constant String := "u" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:475

   SCNx16 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:479

   SCNxLEAST16 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:482

   SCNxFAST16 : aliased constant String := "x" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:485

   SCNo32 : aliased constant String := "lo" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:490

   SCNoLEAST32 : aliased constant String := "lo" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:493

   SCNoFAST32 : aliased constant String := "lo" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:496

   SCNu32 : aliased constant String := "lu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:500

   SCNuLEAST32 : aliased constant String := "lu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:503

   SCNuFAST32 : aliased constant String := "lu" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:506

   SCNx32 : aliased constant String := "lx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:510

   SCNxLEAST32 : aliased constant String := "lx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:513

   SCNxFAST32 : aliased constant String := "lx" & ASCII.NUL;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:516
   --  unsupported macro: SCNoPTR SCNo16
   --  unsupported macro: SCNuPTR SCNu16
   --  unsupported macro: SCNxPTR SCNx16

   subtype int_farptr_t is stdint_h.int32_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:77

   subtype uint_farptr_t is stdint_h.uint32_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/inttypes.h:81

end inttypes_h;
