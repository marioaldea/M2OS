----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding to Elechouse Voice Recognition V3
--  https://www.elechouse.com/elechouse/index.php?main_page=product_info&cPath&products_id=2254
--  https://github.com/kksjunior/Voice-Recognition-with-Elechouse-V3/blob/master

with Interfaces.C.Strings;

package Arduino.Voice_Recognition is
   
   type VR_Record is new Interfaces.Unsigned_8 range 0 .. 255;
   for VR_Record'Size use Interfaces.Unsigned_8'Size;
   
   Max_Signature_Length : constant := 10;
   type VR_Buffer is record
      Group_Mode : Interfaces.Unsigned_8;
      Record_Num : VR_Record;
      Recognizer_Index : Interfaces.Unsigned_8;
      Signature_Length : Interfaces.Unsigned_8;
      Signature : Interfaces.C.Char_Array (1 .. Max_Signature_Length);
   end record;
   for VR_Buffer'Size use (4 + Max_Signature_Length) * 8;
      
   
   VR_Default_Timeout : constant := 1_000;
  
   generic
      Receive_Pin : Arduino.Digital_Pin;
      Transmit_Pin : Arduino.Digital_Pin;
   package VR_Module is   
      procedure Begin_VR (Speed : Interfaces.C.Long)
        with Inline_Always;
   
      function Clear return Interfaces.C.Int
        with Inline_Always;
   
      function Load (Record_Num : VR_Record;
                     Buf : access VR_Buffer := null)
                  return Interfaces.C.Int
        with Inline_Always;               
     
      function Recognize (Buf : access VR_Buffer;
                          Timeout : Interfaces.C.Int := VR_Default_Timeout)
                          return Interfaces.C.Int
        with Inline_Always;
   end VR_Module;
   
   VR_Size_In_Bytes : constant := 31;
   
   Sizeof_VR : Integer with
     Import, Convention => C,
     External_Name => "sizeof_VR";
   --  Defined in 'sizes_arduino_types.cpp' (created by Makefile)
   --  Takes the value of -1 if the library is not included.

end Arduino.Voice_Recognition;
