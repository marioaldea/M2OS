----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on blinkers.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- setup for this example:
-- None, only uses buttons and LEDs built in to the EVShield.
----------------------------------------------------------------------------

with Interfaces.C;

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with SHDefines_h; use SHDefines_h;
with Interfaces.C.Extensions;
with Interfaces.C.Strings;
with EVShieldI2C_h; use EVShieldI2C_h;

procedure evShield_blinkers is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, SHDefines_h.SH_Protocols, Interfaces.C.Extensions.bool;

   -- Program variables
   time : Arduino.Time_MS;
   aux : Interfaces.C.Extensions.bool;

   use type Interfaces.C.Strings.chars_ptr;

begin

   DIO.Put_Line ("Press Shield buttons to see results");

   -- Wait until user presses GO button to continue the program
   while not Arduino.EVshield.Get_Button_State(SHDefines_h.BTN_GO) loop
      time := Arduino.Millis;
      if time mod 1000 < 3 then
         DIO.Put_Line (" Press GO button to continue");
      end if;
   end loop;

   loop
       -- see if right button is pressed
       if Arduino.EVshield.Get_Button_State(SHDefines_h.BTN_RIGHT) then
         DIO.Put_Line (" RIGHT button pressed");
         aux := Arduino.EVshield.Bank_B.Led_Set_RGB(100,0,0);
         Arduino.C_Delay (500);
         aux := Arduino.EVshield.Bank_B.Led_Set_RGB(0,0,0);
         Arduino.C_Delay (500);
       end if;

      if Arduino.EVshield.Get_Button_State(SHDefines_h.BTN_LEFT) then
         DIO.Put_Line (" LEFT button pressed");
         aux := Arduino.EVshield.Bank_a.Led_Set_RGB(100,0,0);
         Arduino.C_Delay (500);
         aux := Arduino.EVshield.Bank_A.Led_Set_RGB(0,0,0);
         Arduino.C_Delay (500);
       end if;


      -- wait for some time
      Arduino.C_Delay (1000);

   end loop;
end evShield_blinkers;
