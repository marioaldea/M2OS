--  Sending data to Maqueen example

with Maqueen;
with Microbit.Time;


with AdaX_Dispatching_Stack_Sharing;

procedure Maqueen_library_example_1 is
   LuzIzquierda : Maqueen.Light := Maqueen.Left_Light;
   LuzDerecha   : Maqueen.Light := Maqueen.Right_Light;
begin

   Maqueen.Initialize;
   Maqueen.SetSpeed(200);

   loop

      if(Maqueen.Read_Patrol_Left = False and Maqueen.Read_Patrol_Right = False) then
         Maqueen.Forward;
      end if;

      if(Maqueen.Read_Patrol_Left = True and Maqueen.Read_Patrol_Right = False) then
         Maqueen.Spin_Left;
      end if;

      if(Maqueen.Read_Patrol_Right = True and Maqueen.Read_Patrol_Left = False ) then
         Maqueen.Spin_Right;
      end if;

      if(Maqueen.Read_Patrol_Left = True and Maqueen.Read_Patrol_Right = True) then
         Maqueen.Spin_Left;
      end if;

      MicroBit.Time.Delay_Ms (50);
   end loop;

end Maqueen_library_example_1;
