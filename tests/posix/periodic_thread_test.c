//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2023
//                    Universidad de Cantabria, SPAIN
//
//  Author: Juan Rom�n Pe�a, Mario Aldea Rivas
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Test that absolute clock_nanosleep() activates a thread at the correct time
// without drift relative to the monotonic clock.
#include "../reports/tests_reports.h"
#include <pthread.h>
#include <stdio.h>
#include <time.h>

THREAD_POOL (1);

#define NUM_OF_LOOPS 50
#define PERIOD_NS 20000000 // -> 0.02seg

const struct timespec period = TS (0, PERIOD_NS);
int counter = 0;

//****************//
//  putts         //
//****************//
void
putts (struct timespec ts)
{
  putint (TS_SEC (ts));
  puts ("s ");
  putint (TS_NSEC (ts));
  puts ("ns");
}

//****************//
//  thread_body   //
//****************//
struct timespec next_activation_time;
struct timespec actual_wakeup;
struct timespec margin = TS(0, NS_IN_S/HWTIME_HZ); // one clock tick
void *
thread_body (void *arg)
{
  clock_gettime (CLOCK_MONOTONIC, &actual_wakeup);

  if (counter > 0)
    {
      hwtime_t hwt_dif = TS2HWT (actual_wakeup) - TS2HWT (next_activation_time);
      struct timespec ts_diff;
      HWT2TS (ts_diff, hwt_dif);

      puts ("Dif (actual - next) = ");
      putts (ts_diff);
      puts ("("); putts (actual_wakeup);
      puts (" - "); putts (next_activation_time);
      puts (")\n");

      if (hwt_dif < 0 || hwt_dif > TS2HWT (margin))
	{
	  puts ("Dif > Margin (");
	  putts (ts_diff);
	  puts (" > ");
	  putts (margin);
	  puts (")\n");
	  m2__end_execution__end_execution (-1);
	}
    }

  if (counter == NUM_OF_LOOPS)
    {
      tests_reports__test_ok ();
    }
  counter++;

  TS_INC (next_activation_time, period);
  clock_nanosleep (CLOCK_MONOTONIC, TIMER_ABSTIME, &next_activation_time, NULL);
  return NULL;
}

//**********//
//   main   //
//**********//
int
main (int arc, char **argv)
{
  m2osinit ();

  puts ("Main\n");
  check_posix_api ();
  puts ("Period -> ");
  putts (period);
  puts (" Margin -> ");
  putts (margin);
  puts (" (");
  putint (TS2HWT (margin));
  puts (" ticks)\n");
  pthread_t th;
  pthread_attr_t attr;
  struct sched_param sch_param
    = { .sched_priority = 1 }; // only one thread so it doesn't matter

  clock_gettime (CLOCK_MONOTONIC, &next_activation_time);
  actual_wakeup = next_activation_time;
  pthread_attr_init (&attr);
  pthread_attr_setschedparam (&attr, &sch_param);

  pthread_create (&th, &attr, thread_body, NULL);

  puts ("Main Ends\n");

  return 0;
}
