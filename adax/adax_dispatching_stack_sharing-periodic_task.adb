----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Kernel.API;

--------------------------------------------------
-- AdaX_Dispatching_Stack_Sharing.Periodic_Task --
--------------------------------------------------

package body AdaX_Dispatching_Stack_Sharing.Periodic_Task is
   package RT renames Ada.Real_Time;

   use type RT.Time;

   --  Next activation time

   Next_Time : RT.Time;

   ------------------------
   -- Periodic_Task_Body --
   ------------------------

   procedure Periodic_Task_Body is
   begin
      Next_Time := Next_Time + Period;

      Body_Ac.all;

      delay until Next_Time;
   end Periodic_Task_Body;

   ----------------------------
   -- Periodic_One_Shot_Task --
   ----------------------------

   task Periodic_One_Shot_Task with
     Priority => Priority,
     Storage_Size => 0, --  There is no individual stack for each task
     Secondary_Stack_Size => 0;



   task body Periodic_One_Shot_Task is
   begin
      M2.Kernel.API.Set_Job_Body_Of_Running_Thread
        (Body_Ac => To_Code_Address (Periodic_Task_Body'Unrestricted_Access));

      Next_Time := RT.Clock;

      if Init_Ac /= null then
         Init_Ac.all;
      end if;

      Periodic_Task_Body;
   end Periodic_One_Shot_Task;

end AdaX_Dispatching_Stack_Sharing.Periodic_Task;
