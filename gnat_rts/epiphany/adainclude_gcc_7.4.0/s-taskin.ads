------------------------------------------------------------------------------
--                                                                          --
--                 GNAT RUN-TIME LIBRARY (GNARL) COMPONENTS                 --
--                                                                          --
--                        S Y S T E M . T A S K I N G                       --
--                                                                          --
--                                  S p e c                                 --
--                                                                          --
--          Copyright (C) 1992-2018, Free Software Foundation, Inc.         --
--                                                                          --
-- GNARL is free software; you can  redistribute it  and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 3,  or (at your option) any later ver- --
-- sion. GNARL is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.                                     --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
-- GNARL was developed by the GNARL team at Florida State University.       --
-- Extensive contributions were provided by Ada Core Technologies, Inc.     --
--                                                                          --
------------------------------------------------------------------------------

--  M2OS: stack sharing version of this package.

--  Simplified version of the GNAT System.Tasking package.

--  with Ada.Unchecked_Conversion;
with System.OS_Interface;

package System.Tasking is
   pragma Preelaborate;

   pragma Discard_Names;  --  M2OS

   ---------------------------------
   -- Task_Id related definitions --
   ---------------------------------

   type Ada_Task_Control_Block;

   type Task_Id is access all Ada_Task_Control_Block;
   pragma No_Strict_Aliasing (Task_Id);
   --  M2OS: required to avoid error in s-taprop.adb

   Null_Task : constant Task_Id := null;

   type Task_List is array (Positive range <>) of Task_Id;
   pragma Suppress_Initialization (Task_List);

   function Self return Task_Id;
   --  This is the compiler interface version of this function. Do not call
   --  from the run-time system.

   -----------------------
   -- Enumeration types --
   -----------------------

   type Task_States is
     (Unactivated,
      --  Task was created but has not been activated. It cannot be executing

      --  For all states from here down, the task has been activated. In
      --  addition, for all states from here down, except for Terminated,
      --  the task may be executing.

      Runnable,
      --  Task is not blocked for any reason known to Ada. (It may be waiting
      --  for a mutex, though.) It is conceptually "executing" in normal mode.

      Terminated,
      --  The task is terminated, in the sense of ARM 9.3 (5)

      Activator_Sleep,
      --  Task is waiting for created tasks to complete activation

      Acceptor_Sleep,
      --  Task is waiting on an accept or selective wait statement

      Entry_Caller_Sleep,
      --  Task is waiting on an entry call

      Async_Select_Sleep,
      --  Task is waiting to start the abortable part of an asynchronous select
      --  statement.

      Delay_Sleep,
      --  Task is waiting on a delay statement

      Master_Completion_Sleep,
      --  Master completion has two phases. In Phase 1 the task is sleeping
      --  in Complete_Master having completed a master within itself, and is
      --  waiting for the tasks dependent on that master to become terminated
      --  or waiting on a terminate Phase.

      Master_Phase_2_Sleep,
      --  In Phase 2 the task is sleeping in Complete_Master waiting for tasks
      --  on terminate alternatives to finish terminating.

      Interrupt_Server_Idle_Sleep,
      Interrupt_Server_Blocked_Interrupt_Sleep,
      Timer_Server_Sleep,
      AST_Server_Sleep,
      --  Special uses of sleep, for server tasks within the run-time system

      Asynchronous_Hold,
      --  The task has been held by Asynchronous_Task_Control.Hold_Task

      Interrupt_Server_Blocked_On_Event_Flag
      --  The task has been blocked on a system call waiting for the
      --  completion event.
     );

   --  The following status indicators are never used in a Ravenscar run time.
   --  They Are defined for debugging purposes: The same code in GDB to get
   --  the Current status of a task in a full run-time environment and in a
   --  Ravenscar environment.

   pragma Unreferenced (Activator_Sleep);
   pragma Unreferenced (Acceptor_Sleep);
   pragma Unreferenced (Async_Select_Sleep);
   pragma Unreferenced (Master_Completion_Sleep);
   pragma Unreferenced (Master_Phase_2_Sleep);
   pragma Unreferenced (Interrupt_Server_Idle_Sleep);
   pragma Unreferenced (Interrupt_Server_Blocked_Interrupt_Sleep);
   pragma Unreferenced (Timer_Server_Sleep);
   pragma Unreferenced (AST_Server_Sleep);
   pragma Unreferenced (Asynchronous_Hold);
   pragma Unreferenced (Interrupt_Server_Blocked_On_Event_Flag);

   -------------------------------
   -- Entry related definitions --
   -------------------------------

   --  These need comments ???

   Null_Entry : constant := 0;

   Max_Entry : constant := Integer'Last;

   Interrupt_Entry : constant := -2;

   Cancelled_Entry : constant := -1;

   type Entry_Index is range Interrupt_Entry .. Max_Entry;

   Null_Task_Entry : constant := Null_Entry;

   Max_Task_Entry : constant := Max_Entry;

   type Task_Entry_Index is new Entry_Index
     range Null_Task_Entry .. Max_Task_Entry;

   type Entry_Call_Record;

   type Entry_Call_Link is access all Entry_Call_Record;

   ----------------------------------
   -- Entry_Call_Record definition --
   ----------------------------------

   type Entry_Call_Record is record
      Self : Task_Id;
      --  ID of the caller

      Uninterpreted_Data : System.Address;
      --  Data passed by the compiler

      Next : Entry_Call_Link;
      --  Entry_Call List
   end record;
   pragma Suppress_Initialization (Entry_Call_Record);

   -------------------------------------------
   -- Task termination procedure definition --
   -------------------------------------------

   --  We need to redefine this type (already defined in Ada.Task_Termination)
   --  here to avoid circular dependencies.

   type Termination_Handler is access protected procedure (T : Task_Id);
   --  Represent a protected procedure to be executed when a task terminates

   Fall_Back_Handler : Termination_Handler;
   --  This is the fall-back handler that applies to all the tasks in the
   --  partition (this is only for Ravenscar-compliant systems).

   ------------------------------------
   -- Other Task-Related Definitions --
   ------------------------------------

   Idle_Priority : constant Integer := Any_Priority'First - 1;
   --  A priority lower than any user priority. Used by the idle task

   subtype Extended_Priority is
     Integer range Idle_Priority .. Any_Priority'Last;
   --  Priority range that also includes the idle priority

   type Activation_Chain is limited private;

   type Activation_Chain_Access is access all Activation_Chain;

   type Task_Procedure_Access is access procedure (Arg : System.Address);

   type Access_Boolean is access all Boolean;

   ----------------------------------------------
   -- Ada_Task_Control_Block (ATCB) definition --
   ----------------------------------------------

--     type Common_ATCB is record
--
--        Base_Priority : Priority;
--        --  Base priority
--
--        Task_Arg : System.Address;
      --  The argument to task procedure. Currently unused, this will provide
      --  a handle for discriminant information.
--     end record;
--     pragma Suppress_Initialization (Common_ATCB);

   type Ada_Task_Control_Block (Entry_Num : Task_Entry_Index) is record

      Thread : aliased OS_Interface.Thread_T;

--        Common : Common_ATCB;

   end record;

   --  Get the record in the expected order
   Word : constant := 40;
   for Ada_Task_Control_Block use
       record
           Thread at 0 * Word range 0  .. 319;  -- 255
           Entry_Num at 1 * Word range 0  .. 31;
       end record;
   pragma Suppress_Initialization (Ada_Task_Control_Block);

--     type Ada_Task_Control_Block (Entry_Num : Task_Entry_Index) is record
--     --  The discriminant Entry_Num is not needed, but we keep it here for
--     --  compatibility reasons with the rest of the run times, so that the
--     --  expander does not need to know which run time is being used.
--
--        Common : Common_ATCB;
--
--        Entry_Call : aliased Entry_Call_Record;
--      --  Protection: This field is used on entry call queues associated with
--        --  protected objects, and is protected by the protected object lock.
--     end record;
--     pragma Suppress_Initialization (Ada_Task_Control_Block);

   --------------------------------
   -- Master Related Definitions --
   --------------------------------

   subtype Master_Level is Integer;
   subtype Master_ID is Master_Level;

   Library_Task_Level : constant Master_Level := 3;

   ----------------------------------------
   -- Task size, priority, affinity info --
   ----------------------------------------

   Unspecified_Priority : constant := -1;
   --  No specified priority. This value is also hard-coded in gnatbind.

   Unspecified_CPU : constant := -1;
   --  No affinity specified

   --------------------
   -- Initialization --
   --------------------

   procedure Initialize;
   --  This procedure constitutes the first part of the initialization of the
   --  GNARL. This includes creating data structures to make the initial thread
   --  into the environment task. The last part of the initialization is done
   --  in System.Tasking.Initialization or System.Tasking.Restricted.Stages.
   --  All the initializations used to be in Tasking.Initialization, but this
   --  is no longer possible with the run time simplification (including
   --  optimized PO and the restricted run time) since one cannot rely on
   --  System.Tasking.Initialization being present, as was done before.

--     procedure Initialize_ATCB (Thread_Ac : OS_Interface.Thread_Ac;
--                                T         : Task_Id);

private

   type Activation_Chain is limited record
      T_ID : Task_Id;
   end record;

end System.Tasking;
