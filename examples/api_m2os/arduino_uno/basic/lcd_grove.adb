----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Simple use example of Grove-16x2 LCD
--  https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/

with Arduino.LCD_I2C;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure LCD_Grove is
   package DIO renames M2.Direct_IO;
   package LCD renames Arduino.LCD_I2C;

begin
   DIO.Put_Line ("Simple use example of Grove-16x2 LCD");

   LCD.Begin_LCD (16, 2);

   loop
       LCD.Set_Cursor (0, 0);
      for I in 0 .. 12 loop
         Arduino.C_Delay (500);
         LCD.Print_Integer (I);
      end loop;

       LCD.Set_Cursor (0, 1);
      for C in Character range 'A' .. 'M' loop
         Arduino.C_Delay (500);
         LCD.Print_Char (C);
      end loop;

      Arduino.C_Delay (1_000);
      LCD.Clear;
   end loop;
end LCD_Grove;
