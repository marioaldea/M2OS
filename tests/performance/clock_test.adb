pragma Profile (Ravenscar);
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;

with M2.Direct_IO;

with Measurements;

procedure Clock_Test is
   package DIO renames M2.Direct_IO;

   Num_Of_Loops : constant := 10_000;
   Loop_Counter : Natural := 0;
   Time : Ada.Real_Time.Time;

begin
   DIO.Put ("Clock test: ");
   Measurements.Init (Measurements.Long);

   loop
      Measurements.Start_Measurement;
      for I in 1 .. Num_Of_Loops loop
         Time := Ada.Real_Time.Clock;
      end loop;
      Measurements.End_Measurement;

      Loop_Counter := Loop_Counter + 1;

      Measurements.Finish;

   end loop;
end Clock_Test;
