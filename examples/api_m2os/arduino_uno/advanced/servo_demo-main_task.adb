----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Demo program that uses two servos.
--  See servo_demo.ads for details.

with Arduino.Servo;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

package body Servo_Demo.Main_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   --  servos

   Servo_0 : aliased Arduino.Servo.Servo;
   Servo_1 : aliased Arduino.Servo.Servo;

   --  Next activation time

   Next_Time : RT.Time;

   --------------------
   -- Main_Task_Init --
   --------------------

   procedure Main_Task_Init is
      Ret : Arduino.Servo.Attach_Ret;
      use type Arduino.Servo.Attach_Ret;
   begin
      DIO.Put_Line ("Servo_Demo.Main_Task initialization");
      Ret := Arduino.Servo.Attach (Servo_0'Access, Pin => Pin_Servo_0);
      Ret := Arduino.Servo.Attach (Servo_1'Access, Pin => Pin_Servo_1);

      Next_Time := RT.Clock;
   end Main_Task_Init;

   --------------------
   -- Main_Task_Body --
   --------------------

   use type Arduino.Servo.Angle_Deg, Arduino.Servo.Signed_Angle_Deg;

   Angle_0 : Arduino.Servo.Angle_Deg := 0;
   Angle_Increment_0 : Arduino.Servo.Signed_Angle_Deg := -6;
   Angle_1 : Arduino.Servo.Angle_Deg := 0;
   Angle_Increment_1 : Arduino.Servo.Signed_Angle_Deg := -12;

   procedure  Main_Task_Body is
   begin
      if Angle_0 = 180 or Angle_0 = 0 then
         Angle_Increment_0 := -Angle_Increment_0;
      end if;
      if Angle_1 = 180 or Angle_1 = 0 then
         Angle_Increment_1 := -Angle_Increment_1;
      end if;

      Angle_0 := Angle_0 + Angle_Increment_0;
      Angle_1 := Angle_1 + Angle_Increment_1;

      DIO.Put (" Angle_0:");
      DIO.Put (Integer (Angle_0));

      DIO.Put (" Angle_1:");
      DIO.Put (Integer (Angle_1));

      Arduino.Servo.Write (Servo_0'Access, Angle => Angle_0);

      Arduino.Servo.Write (Servo_1'Access, Angle => Angle_1);

      Next_Time := Next_Time + Servo_Demo.Main_Task_Period;
      delay until Next_Time;
   end Main_Task_Body;

   ---------------------
   -- Servo Demo Task --
   ---------------------

   Main_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Main_Task_Init'Unrestricted_Access,
      Body_Ac  => Main_Task_Body'Unrestricted_Access,
      Priority => Servo_Demo.Main_Task_Prio);

end Servo_Demo.Main_Task;
