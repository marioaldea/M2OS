----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Task to read de CCS811 Sensor.

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing.Periodic_Task;
with Ada.Real_Time;

with Arduino.CCS811;

with CO2_Sensor.LCD_Tasks;

package body CO2_Sensor.CCS811_Task is
   package DIO renames M2.Direct_IO;
   package CCS811 renames Arduino.CCS811; 
   
   use type CCS811.CO2_PPM_T, CCS811.TVOC_PPB_T;
   
   Ret : Integer := 0;
   
   CO2_PPM : CCS811.CO2_PPM_T;
   TVOC_PPB : CCS811.TVOC_PPB_T;
   
   ----------------------
   -- Set_Env_Temp_Hum --
   ----------------------

   procedure Set_Env_Temp_Hum (Temp : Float; Hum : Float) is
   begin
      CCS811.Set_In_Temp_Hum (Temp, Hum);
   end Set_Env_Temp_Hum;

   ----------------------
   -- CCS811_Task_Init --
   ----------------------

   procedure CCS811_Task_Init is
   begin
      -- DIO.Put_Line ("CCS811_Task_Init");        
   
      loop
         Ret := CCS811.Begin_CCS811;     
         exit when Ret = 0;
      
         Arduino.C_Delay (1_000);
      end loop;
   
      CCS811.Set_Meas_Cycle (CCS811.ECycle_250ms);
   end CCS811_Task_Init;
   
   ----------------------
   -- CCS811_Task_Body --
   ----------------------

   procedure CCS811_Task_Body is
   begin 
      -- DIO.Put_Line ("CCS811_Task");
        
      if CCS811.Check_Data_Ready then
         CO2_PPM := CCS811.Get_CO2_PPM;
         TVOC_PPB := CCS811.Get_TVOC_PPB;
         -- DIO.Put ("CO2 (ppm):" & Integer (CO2_PPM)'Img);
         -- DIO.Put (" TVOC (ppb):" & Integer (TVOC_PPB)'Img);
         -- DIO.New_Line;
      else
         CO2_PPM := 0;
         TVOC_PPB := 0;
         DIO.Put_Line ("Data not ready");
      end if;
      
      --  Write on LCD
      
      LCD_Tasks.Set_CO2_PPM (CO2_PPM);
      if CO2_PPM < CCS811.CO2_PPM_IDA1_Limit then
         LCD_Tasks.Set_IDA (IDA1);
      elsif CO2_PPM < CCS811.CO2_PPM_IDA2_Limit then
         LCD_Tasks.Set_IDA (IDA2);
      elsif CO2_PPM < CCS811.CO2_PPM_IDA3_Limit then
         LCD_Tasks.Set_IDA (IDA3);
      else
         LCD_Tasks.Set_IDA (IDA4);
      end if;      
      LCD_Tasks.Set_TVOC_PPB (TVOC_PPB);        
      
      CCS811.Write_Base_Line (CCS811.Base_Line_Value);
   end CCS811_Task_Body;
    
   --------------
   -- CCS811_Task --
   --------------

   package CCS811_Task is new AdaX_Dispatching_Stack_Sharing.Periodic_Task
     (Init_Ac  => CCS811_Task_Init'Access,
      Body_Ac  => CCS811_Task_Body'Access,
      Priority => CO2_Sensor.CCS811_Task_Prio,
      Period   => Ada.Real_Time.Milliseconds (CO2_Sensor.CCS811_Task_T_In_Ms));
  
end CO2_Sensor.CCS811_Task;
