with MicroBit.Display;
with MicroBit.Buttons; use MicroBit.Buttons;
with MicroBit.Time;
with Ada.Text_IO;
with MicroBit.IOs;

with AdaX_Dispatching_Stack_Sharing;

procedure Buttons is
begin
   Ada.Text_IO.Put_Line ("Buttons working in M2OS!");
   loop
      MicroBit.Display.Clear;

      if MicroBit.Buttons.State (Button_A) = Pressed then
         --  If button A is pressed

         --  Display the letter A
         MicroBit.Display.Display ('<');
         Ada.Text_IO.Put_Line ("Se pulsa el boton de la izquierda!");
         MicroBit.IOs.Set (12, True);
      elsif MicroBit.Buttons.State (Button_B) = Pressed then
         --  If button B is pressed

         --  Display the letter B
         MicroBit.Display.Display ('B');
         Ada.Text_IO.Put_Line ("Se pulsa el boton de la derecha!");
      else
         MicroBit.IOs.Set (8, False);
         MicroBit.IOs.Set (12, False);
      end if;

      MicroBit.Time.Delay_Ms (200);
   end loop;
end Buttons;
