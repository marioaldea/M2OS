----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Four task with periods with relation 1:2:3:4.
--  Test checks the number of activations of each task is in accordance with
--  its period.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;
with Ada.Synchronous_Task_Control;

with M2.Direct_IO;
with Periodic_Tasks_Four_2;
with Periodic_Tasks_Four_3;
with Periodic_Tasks_Four_4;

with Tests_Reports;
with M2.HAL;

procedure Periodic_Tasks_Four_Test  is
   package DIO renames M2.Direct_IO;
   package P2 renames Periodic_Tasks_Four_2;
   package P3 renames Periodic_Tasks_Four_3;
   package P4 renames Periodic_Tasks_Four_4;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   Loop_Counter : Integer := 0;

   Next_Time : RT.Time;
   Period : constant RT.Time_Span := RT.Milliseconds (P2.Period_In_Ms);

begin
   DIO.Put ("-Periodic_Tasks_Four_Test-");
   DIO.New_Line;

   Next_Time := Ada.Real_Time.Clock + Period;
   delay until Next_Time;

   loop
      M2.HAL.Enable_Interrupts;
      DIO.Put ("-Main-");

      if Loop_Counter /= 0 then
         DIO.Put (" L1=");
         DIO.Put (Loop_Counter);
         DIO.Put (" L2=");
         DIO.Put (P2.Loop_Counter);
         Tests_Reports.Assert_Equal (P2.Loop_Counter * 2,
                                    Loop_Counter,
                                    1);
         DIO.Put (" L3=");
         DIO.Put (P3.Loop_Counter);
         Tests_Reports.Assert_Equal (P3.Loop_Counter * 3,
                                    Loop_Counter,
                                    2);
         DIO.Put (" L4=");
         DIO.Put (P4.Loop_Counter);
         Tests_Reports.Assert_Equal (P4.Loop_Counter * 4,
                                    Loop_Counter,
                                    3);
      end if;

      if Loop_Counter = P2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      Loop_Counter := Loop_Counter + 1;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end loop;

end Periodic_Tasks_Four_Test;
