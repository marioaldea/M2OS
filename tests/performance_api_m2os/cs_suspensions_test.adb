--  Measure the context switch when using different suspension primitives:
--    - Delay_Until with a time in the past
--    - Suspension_Object
--    - Yield
--    - PO_Entry,
--    - PO_Entry with Parameter
--  The time is measured since one task (Task_1) calls a closed suspension
--  primitive and suspendsuntil other task (Task 1) executes.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
with CS_Suspensions_1;
with CS_Suspensions_2;

with Measurements_HiRes;

procedure CS_Suspensions_Test  is
   package DIO renames M2.Direct_IO;
   package Measurements renames Measurements_HiRes;

begin
   DIO.Put ("-CS_Suspensions_Test-");
   DIO.New_Line;

   Measurements.Init;

end CS_Suspensions_Test;
