pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package stddef_h is

   --  unsupported macro: NULL __null
   --  arg-macro: procedure offsetof (TYPE, MEMBER)
   --    __builtin_offsetof (TYPE, MEMBER)
   subtype ptrdiff_t is int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/lib/gcc/avr/7.3.0/include/stddef.h:149

   subtype size_t is unsigned;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/lib/gcc/avr/7.3.0/include/stddef.h:216

   type max_align_t is record
      uu_max_align_ll : aliased Long_Long_Integer;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/lib/gcc/avr/7.3.0/include/stddef.h:427
      uu_max_align_ld : aliased long_double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/lib/gcc/avr/7.3.0/include/stddef.h:428
   end record;
   pragma Convention (C_Pass_By_Copy, max_align_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/lib/gcc/avr/7.3.0/include/stddef.h:437

   --  skipped anonymous struct anon_10

   subtype nullptr_t is ;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/lib/gcc/avr/7.3.0/include/stddef.h:444

end stddef_h;
