with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

with Protected_Interrupt_Handler.PO;

package body Protected_Interrupt_Handler.Server_Task is
   package DIO renames M2.Direct_IO;
   package PO renames Protected_Interrupt_Handler.PO;

   ----------------------
   -- Server_Task_Init --
   ----------------------

   procedure Server_Task_Init is
   begin
      DIO.Put ("-Server_Task_Init-");
   end Server_Task_Init;

   ----------------------
   -- Server_Task_Body --
   ----------------------

   procedure Server_Task_Body is
   begin
      DIO.Put ("-Server_Task-");

      PO.PO.Wait;
   end Server_Task_Body;

   Server_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Server_Task_Init'Access,
      Body_Ac  => Server_Task_Body'Access,
      Priority => 5);

end Protected_Interrupt_Handler.Server_Task;
