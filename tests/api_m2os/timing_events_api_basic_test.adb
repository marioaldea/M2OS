----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with System;

with M2.Kernel.API;

with M2.Direct_IO;
with Tests_Reports;

with Timing_Events_API_Basic_PO;

procedure Timing_Events_API_Basic_Test  is
   package DIO renames M2.Direct_IO;
   package API renames M2.Kernel.API;

   use type API.Time, API.Timing_Event_Handler;

   Event : aliased API.Timing_Event;
   T : API.Time;
   Handler : API.Timing_Event_Handler;
   Cancelled : API.Boolean_C;
   use type API.Boolean_C;

begin
   DIO.Put_Line ("-Timing_Events_Basic_Test-");

   DIO.Put_Line ("Tests initial state of Event");

   T := API.Time_Of_Event (Event);
   Tests_Reports.Assert (T = API.Time'First);

   Handler := API.Current_Handler (Event);
   Tests_Reports.Assert (Handler = null);

   API.Cancel_Handler (Event'Unrestricted_Access, Cancelled);
   Tests_Reports.Assert (not Boolean (Cancelled));

   DIO.Put_Line ("Tests state of a 'set' Event");

   API.Set_Handler (Event'Unrestricted_Access,
                    API.Time'Last,
                    Timing_Events_API_Basic_PO.PO.PO_Handler'Access);

   T := API.Time_Of_Event (Event);
   Tests_Reports.Assert (T = API.Time'Last);

   Handler := API.Current_Handler (Event);
   Tests_Reports.Assert
     (Handler = Timing_Events_API_Basic_PO.PO.PO_Handler'Access);

   API.Cancel_Handler (Event'Unrestricted_Access, Cancelled);
   Tests_Reports.Assert (Boolean (Cancelled));

   DIO.Put_Line ("Tests state of a 'cleared' Event");

   T := API.Time_Of_Event (Event);
   Tests_Reports.Assert (T = API.Time'First);

   Handler := API.Current_Handler (Event);
   Tests_Reports.Assert (Handler = null);

   API.Cancel_Handler (Event'Unrestricted_Access, Cancelled);
   Tests_Reports.Assert (not Boolean (Cancelled));

   Tests_Reports.Test_OK;

end Timing_Events_API_Basic_Test;
