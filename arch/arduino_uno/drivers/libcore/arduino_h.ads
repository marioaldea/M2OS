pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with stdint_h;

package Arduino_h is

   HIGH : constant := 16#1#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:40
   LOW : constant := 16#0#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:41

   INPUT : constant := 16#0#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:43
   OUTPUT : constant := 16#1#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:44
   INPUT_PULLUP : constant := 16#2#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:45

   PI : constant := 3.1415926535897932384626433832795;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:47
   HALF_PI : constant := 1.5707963267948966192313216916398;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:48
   TWO_PI : constant := 6.283185307179586476925286766559;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:49
   DEG_TO_RAD : constant := 0.017453292519943295769236907684886;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:50
   RAD_TO_DEG : constant := 57.295779513082320876798154814105;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:51
   EULER : constant := 2.718281828459045235360287471352;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:52

   SERIAL : constant := 16#0#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:54
   DISPLAY : constant := 16#1#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:55

   LSBFIRST : constant := 0;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:57
   MSBFIRST : constant := 1;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:58

   CHANGE : constant := 1;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:60
   FALLING : constant := 2;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:61
   RISING : constant := 3;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:62

   INTERNAL : constant := 3;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:81

   DEFAULT : constant := 1;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:83
   EXTERNAL : constant := 0;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:84
   --  arg-macro: function max (a, b)
   --    return (a)>(b)?(a):(b);
   --  arg-macro: function abs (x)
   --    return (x)>0?(x):-(x);
   --  arg-macro: function constrain (amt, low, high)
   --    return (amt)<(low)?(low):((amt)>(high)?(high):(amt));
   --  arg-macro: function round (x)
   --    return (x)>=0?(long)((x)+0.5):(long)((x)-0.5);
   --  arg-macro: function radians (deg)
   --    return (deg)*DEG_TO_RAD;
   --  arg-macro: function degrees (rad)
   --    return (rad)*RAD_TO_DEG;
   --  arg-macro: function sq (x)
   --    return (x)*(x);
   --  arg-macro: procedure interrupts ()
   --    sei()
   --  arg-macro: procedure noInterrupts ()
   --    cli()
   --  arg-macro: function clockCyclesPerMicrosecond ()
   --    return  F_CPU / 1000000 ;
   --  arg-macro: function clockCyclesToMicroseconds (a)
   --    return  (a) / clockCyclesPerMicrosecond() ;
   --  arg-macro: function microsecondsToClockCycles (a)
   --    return  (a) * clockCyclesPerMicrosecond() ;
   --  arg-macro: function lowByte (w)
   --    return (uint8_t) ((w) and 16#ff#);
   --  arg-macro: function highByte (w)
   --    return (uint8_t) ((w) >> 8);
   --  arg-macro: function bitRead (value, bit)
   --    return ((value) >> (bit)) and 16#01#;
   --  arg-macro: function bitSet (value, bit)
   --    return (value) |= (2 ** (bit));
   --  arg-macro: function bitClear (value, bit)
   --    return (value) &= ~(2 ** (bit));
   --  arg-macro: function bitToggle (value, bit)
   --    return (value) ^= (2 ** (bit));
   --  arg-macro: function bitWrite (value, bit, bitvalue)
   --    return (bitvalue) ? bitSet(value, bit) : bitClear(value, bit);
   --  arg-macro: function bit (b)
   --    return 2 ** (b);
   --  arg-macro: function digitalPinToPort (P)
   --    return  pgm_read_byte( digital_pin_to_port_PGM + (P) ) ;
   --  arg-macro: function digitalPinToBitMask (P)
   --    return  pgm_read_byte( digital_pin_to_bit_mask_PGM + (P) ) ;
   --  arg-macro: function digitalPinToTimer (P)
   --    return  pgm_read_byte( digital_pin_to_timer_PGM + (P) ) ;
   --  arg-macro: function analogInPinToBit (P)
   --    return P;
   --  arg-macro: function portOutputRegister (P)
   --    return  (volatile uint8_t *)( pgm_read_word( port_to_output_PGM + (P))) ;
   --  arg-macro: function portInputRegister (P)
   --    return  (volatile uint8_t *)( pgm_read_word( port_to_input_PGM + (P))) ;
   --  arg-macro: function portModeRegister (P)
   --    return  (volatile uint8_t *)( pgm_read_word( port_to_mode_PGM + (P))) ;

   NOT_A_PIN : constant := 0;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:186
   NOT_A_PORT : constant := 0;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:187

   NOT_AN_INTERRUPT : constant := -1;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:189

   NOT_ON_TIMER : constant := 0;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:205
   TIMER0A : constant := 1;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:206
   TIMER0B : constant := 2;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:207
   TIMER1A : constant := 3;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:208
   TIMER1B : constant := 4;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:209
   TIMER1C : constant := 5;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:210
   TIMER2 : constant := 6;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:211
   TIMER2A : constant := 7;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:212
   TIMER2B : constant := 8;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:213

   TIMER3A : constant := 9;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:215
   TIMER3B : constant := 10;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:216
   TIMER3C : constant := 11;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:217
   TIMER4A : constant := 12;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:218
   TIMER4B : constant := 13;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:219
   TIMER4C : constant := 14;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:220
   TIMER4D : constant := 15;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:221
   TIMER5A : constant := 16;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:222
   TIMER5B : constant := 17;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:223
   TIMER5C : constant := 18;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:224
   --  unsupported macro: word(...) makeWord(__VA_ARGS__)

   procedure yield;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:38
   pragma Import (C, yield, "yield");

   subtype word is unsigned;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:122

   subtype boolean is Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:126

   subtype byte is stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:127

   procedure init;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:129
   pragma Import (C, init, "init");

   procedure initVariant;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:130
   pragma Import (C, initVariant, "initVariant");

   function atexit (arg1 : access procedure) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:132
   pragma Import (C, atexit, "atexit");

   procedure pinMode (pin : stdint_h.uint8_t; mode : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:134
   pragma Import (C, pinMode, "pinMode");

   procedure digitalWrite (pin : stdint_h.uint8_t; val : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:135
   pragma Import (C, digitalWrite, "digitalWrite");

   function digitalRead (pin : stdint_h.uint8_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:136
   pragma Import (C, digitalRead, "digitalRead");

   function analogRead (pin : stdint_h.uint8_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:137
   pragma Import (C, analogRead, "analogRead");

   procedure analogReference (mode : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:138
   pragma Import (C, analogReference, "analogReference");

   procedure analogWrite (pin : stdint_h.uint8_t; val : int);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:139
   pragma Import (C, analogWrite, "analogWrite");

   function millis return unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:141
   pragma Import (C, millis, "millis");

   function micros return unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:142
   pragma Import (C, micros, "micros");

   procedure c_delay (ms : unsigned_long);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:143
   pragma Import (C, c_delay, "delay");

   procedure delayMicroseconds (us : unsigned);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:144
   pragma Import (C, delayMicroseconds, "delayMicroseconds");

   procedure shiftOut
     (dataPin : stdint_h.uint8_t;
      clockPin : stdint_h.uint8_t;
      bitOrder : stdint_h.uint8_t;
      val : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:148
   pragma Import (C, shiftOut, "shiftOut");

   function shiftIn
     (dataPin : stdint_h.uint8_t;
      clockPin : stdint_h.uint8_t;
      bitOrder : stdint_h.uint8_t) return stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:149
   pragma Import (C, shiftIn, "shiftIn");

   procedure attachInterrupt
     (interruptNum : stdint_h.uint8_t;
      userFunc : access procedure;
      mode : int);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:151
   pragma Import (C, attachInterrupt, "attachInterrupt");

   procedure detachInterrupt (interruptNum : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:152
   pragma Import (C, detachInterrupt, "detachInterrupt");

   procedure setup;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:154
   pragma Import (C, setup, "setup");

   procedure c_loop;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:155
   pragma Import (C, c_loop, "loop");

   port_to_mode_PGM : aliased array (size_t) of aliased stdint_h.uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:164
   pragma Import (C, port_to_mode_PGM, "port_to_mode_PGM");

   port_to_input_PGM : aliased array (size_t) of aliased stdint_h.uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:165
   pragma Import (C, port_to_input_PGM, "port_to_input_PGM");

   port_to_output_PGM : aliased array (size_t) of aliased stdint_h.uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:166
   pragma Import (C, port_to_output_PGM, "port_to_output_PGM");

   digital_pin_to_port_PGM : aliased array (size_t) of aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:168
   pragma Import (C, digital_pin_to_port_PGM, "digital_pin_to_port_PGM");

   digital_pin_to_bit_mask_PGM : aliased array (size_t) of aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:170
   pragma Import (C, digital_pin_to_bit_mask_PGM, "digital_pin_to_bit_mask_PGM");

   digital_pin_to_timer_PGM : aliased array (size_t) of aliased stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:171
   pragma Import (C, digital_pin_to_timer_PGM, "digital_pin_to_timer_PGM");

   function makeWord (w : stdint_h.uint16_t) return stdint_h.uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:239
   pragma Import (CPP, makeWord, "_Z8makeWordj");

   function makeWord (h : byte; l : byte) return stdint_h.uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:240
   pragma Import (CPP, makeWord, "_Z8makeWordhh");

   function pulseIn
     (pin : stdint_h.uint8_t;
      state : stdint_h.uint8_t;
      timeout : unsigned_long) return unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:244
   pragma Import (C, pulseIn, "pulseIn");

   function pulseInLong
     (pin : stdint_h.uint8_t;
      state : stdint_h.uint8_t;
      timeout : unsigned_long) return unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:245
   pragma Import (C, pulseInLong, "pulseInLong");

   procedure tone
     (u_pin : stdint_h.uint8_t;
      frequency : unsigned;
      duration : unsigned_long);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:247
   pragma Import (CPP, tone, "_Z4tonehjm");

   procedure noTone (u_pin : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:248
   pragma Import (CPP, noTone, "_Z6noToneh");

   function random (arg1 : long) return long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:251
   pragma Import (CPP, random, "_Z6randoml");

   function random (arg1 : long; arg2 : long) return long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:252
   pragma Import (CPP, random, "_Z6randomll");

   procedure randomSeed (arg1 : unsigned_long);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:253
   pragma Import (CPP, randomSeed, "_Z10randomSeedm");

   function map
     (arg1 : long;
      arg2 : long;
      arg3 : long;
      arg4 : long;
      arg5 : long) return long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Arduino.h:254
   pragma Import (CPP, map, "_Z3maplllll");

end Arduino_h;
