----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Real_Time;
with Ada.Synchronous_Task_Control;
with Ada.Dispatching.Non_Preemptive;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;

with M2.Kernel.API;
with M2.HAL;

with Measurements_HiRes;

package body Yield_To_Higher_Tasks is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;
   package API renames M2.Kernel.API;
   package STC renames Ada.Synchronous_Task_Control;
   package Measurements renames Measurements_HiRes;
   
   Suspension_Object : STC.Suspension_Object;
   
   Num_Loops  : constant := 200;
   Loop_Counter : Natural := 1 with Volatile;
   Step_Counter : Natural := 0 with Volatile;

   -----------------
   -- Task_H_Init --
   -----------------

   procedure Task_H_Init is
   begin
      --  DIO.Put ("-TH Init-");
      --  DIO.New_Line;
      
      Measurements.Init;
           
      STC.Suspend_Until_True (Suspension_Object);
   end Task_H_Init;

   ---------------
   -- Task_H_Body --
   ---------------

   procedure Task_H_Body is
   begin
      --  Executes when the low prio tasks opens the suspension object.
      
      --  DIO.Put ("-TH:");
      --  DIO.Put (Integer (Step_Counter));
      --  DIO.Put (" SP:");
      --  DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      --  DIO.New_Line;
      
      if Loop_Counter <= Num_Loops / 2 then
         Measurements.End_Measurement;
      end if;
      
      Measurements.Assert (Step_Counter = 2);
      Step_Counter := Step_Counter + 1; 
      
      if Loop_Counter > Num_Loops / 2 then
         Measurements.Start_Measurement;
      end if;
      
      STC.Suspend_Until_True (Suspension_Object);
   end Task_H_Body;

   ------------
   -- Task_H --
   ------------

   Task_H : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_H_Init'Access,
      Body_Ac  => Task_H_Body'Access,
      Priority => 5);

   -----------------
   -- Task_L_Init --
   -----------------

   procedure Task_L_Init is
   begin
      --  DIO.Put ("-TL Init-");
      --  DIO.New_Line;
      
      null;
   end Task_L_Init;

   ---------------
   -- Task_L_Body --
   ---------------

   procedure Task_L_Body is
   begin
      --  DIO.Put ("-TL1:");
      --  DIO.Put (Integer (Step_Counter));
      --  DIO.Put (" SP:");
      --  DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      --  DIO.New_Line;     
      Measurements.Assert (Step_Counter = 0);
      Step_Counter := Step_Counter + 1;
      
      STC.Set_True (Suspension_Object); -- activate high prio task
      
      M2.Kernel.API.Eat (0.01);
      
      --  DIO.Put ("-TL2:");
      --  DIO.Put (Integer (Step_Counter));
      --  DIO.Put (" SP:");
      --  DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      --  DIO.New_Line;     
      Measurements.Assert (Step_Counter = 1);
      Step_Counter := Step_Counter + 1;
            
      if Loop_Counter <= Num_Loops / 2 then
         Measurements.Start_Measurement;
      end if;
      
      Ada.Dispatching.Non_Preemptive.Yield_To_Higher;
      
      if Loop_Counter > Num_Loops / 2 then
         Measurements.End_Measurement;
      end if;
      
      --  DIO.Put ("-TL3:");
      --  DIO.Put (Integer (Step_Counter));
      --  DIO.Put (" SP:");
      --  DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      --  DIO.New_Line;     
      Measurements.Assert (Step_Counter = 3);
      Step_Counter := Step_Counter + 1;
      
      M2.Kernel.API.Eat (0.01);
      
      if Loop_Counter = Num_Loops / 2 then
         DIO.Put (" Measurement Preempt(us): ");
         Measurements.Print_Measures_Data;
         Measurements.Reset;
      elsif Loop_Counter = Num_Loops then
         DIO.Put (" Measurement EndPreempt(us): ");
         Measurements.Finish;
      end if;      
      Loop_Counter := Loop_Counter + 1;
      Step_Counter := 0;
      
      delay until Ada.Real_Time.Time_First;
   end Task_L_Body;

   ------------
   -- Task_L --
   ------------

   Task_L : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_L_Init'Access,
      Body_Ac  => Task_L_Body'Access,
      Priority => 4);

end Yield_To_Higher_Tasks;
