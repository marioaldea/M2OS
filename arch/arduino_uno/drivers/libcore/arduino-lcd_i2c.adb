----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of Grove-16x2 LCD library
--  https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/
--
--  IMPORTANT: change line of 74 of rgb_lcd.cpp replacing:
--      delayMicroseconds(50000);
--  by:
--      delay(50);
--  Arduino reference page for delayMicroseconds() says: "Currently, the largest
--  value that will produce an accurate delay is 16383". In fact, the use of
--  delayMicroseconds() with a long time causes a short delay that, some times,
--  avoids the proper initialization of the LCD.
----------------------------------------------------------------------------
with Interfaces.C.Strings;

package body Arduino.LCD_I2C is

   LCD_5x8DOTS : constant := 0;

   -----------------------------
   --  C++ imported functions --
   -----------------------------

   procedure Begin_LCD (This : access LCD;
                        Cols : Interfaces.Unsigned_8;
                        Rows : Interfaces.Unsigned_8;
                        CharSize : Interfaces.Unsigned_8);
   pragma Import (CPP, Begin_LCD, "_ZN7rgb_lcd5beginEhhh");

   procedure Clear (This : access LCD);
   pragma Import (CPP, Clear, "_ZN7rgb_lcd5clearEv");

   procedure Set_Cursor (This : access LCD;
                         Cols : Cols_T;
                         Rows : Rows_T);
   pragma Import (CPP, Set_Cursor, "_ZN7rgb_lcd9setCursorEhh");

   procedure Print_Integer (This : access LCD;
                            I : Integer;
                            BASE : Integer := 10);
   pragma Import (CPP, Print_Integer, "_ZN5Print5printEii");

   procedure Print_Null_Terminated_Char_Array
     (This : access LCD;
      Str : Interfaces.C.Strings.char_array_access);
   pragma Import (CPP, Print_Null_Terminated_Char_Array, "_ZN5Print5printEPKc");

   procedure Print_Char
     (This : access LCD;
      Char : Character);
   pragma Import (CPP, Print_Char, "_ZN5Print5printEc");

   ---------------
   -- Begin_LCD --
   ---------------

   procedure Begin_LCD (Cols : Interfaces.Unsigned_8;
                        Rows : Interfaces.Unsigned_8) is
   begin
      Begin_LCD (Screen'Access, Cols, Rows, LCD_5x8DOTS);
   end Begin_LCD;

   -----------
   -- Clear --
   -----------

   procedure Clear is
   begin
      Clear (Screen'Access);
   end Clear;

   ----------------
   -- Set_Cursor --
   ----------------

   procedure Set_Cursor (Cols : Cols_T;
                         Rows : Rows_T) is
   begin
      Set_Cursor (Screen'Access, Cols, Rows);
   end Set_Cursor;

   -------------------
   -- Print_Integer --
   -------------------

   procedure Print_Integer (I : Integer;
                            Base : Integer := 10) is
   begin
      Print_Integer (Screen'Access, I, Base);
   end Print_Integer;

   --------------------------------------
   -- Print_Null_Terminated_Char_Array --
   --------------------------------------

   procedure Print_Null_Terminated_Char_Array
     (Str : Interfaces.C.Strings.char_array_access) is
   begin
      Print_Null_Terminated_Char_Array (Screen'Access, Str);
   end Print_Null_Terminated_Char_Array;

   ----------------
   -- Print_Char --
   ----------------

   procedure Print_Char (Char : Character) is
   begin
      Print_Char (Screen'Access, Char);
   end Print_Char;

end Arduino.LCD_I2C;
