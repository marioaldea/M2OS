#ifndef _M2_STDDEF_H
#define _M2_STDDEF_H

#undef NULL
#if defined(__cplusplus)
#  define NULL 0
#else
#  define NULL ((void *)0)
#endif

#endif
