----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Authors: David Garcia Villaescusa (garcia@unican.es)
--           Mario Aldea Rivas (aldeam@unican.es))
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--  Epiphany version of this package

with M2.Debug;
with M2.Direct_IO;
with System;
with System.Storage_Elements;
use System.Storage_Elements;

package body M2.HAL is
   use type System.Address;

   Stack_Not_Used_Magic : constant Interfaces.Unsigned_8 := 16#AB#;
   Stack_Start : constant Integer_Address := 16#7FF0#;
   Console_Start : constant Integer_Address := 16#7500#;
   Console_Size : constant Integer_Address := 16#100#;
   Console_End : constant Integer_Address := 16#7600#;

   procedure Interrupts_Set_Timer_Counter
     with
       Import => True,
       convention => C;
   -- Imports function 'interrupts_set_timer_counter' from C.
   -- Can be called from Ada.

   procedure Interrupts_Enable_Timer0
     with
       Import => True,
       convention => C;
   -- Imports function 'interrupts_enable_timer0' from C.
   -- Can be called from Ada.

   procedure Interrupts_Disable_Timer0
     with
       Import => True,
       convention => C;
   -- Imports function 'interrupts_disable_timer0' from C.
   -- Can be called from Ada.

   procedure Interrupts_Enable_Global
     with
       Import => True,
       convention => C;
   -- Imports function 'interrupts_enable_global' from C.
   -- Can be called from Ada.

   procedure Interrupts_Disable_Global
     with
       Import => True,
       convention => C;
   -- Imports function 'interrupts_disable_global' from C.
   -- Can be called from Ada.

   function Interrupts_Disabled return Integer
     with
       Import => True,
       convention => C;
   -- Imports function 'interrupts_disabled' from C.
   -- Can be called from Ada.

   procedure Jump_To_Context (Funct : Thread_Context;
                              Stack_Top : System.Address);
   pragma Import (Asm, Jump_To_Context, "m2_hal_regs_jump_to_context");
   --  From m2_hal_regs.S

   Stack_Base_Linker_Script : Interfaces.Unsigned_32;
   pragma Import (C, Stack_Base_Linker_Script, "__stack_start_");

   Initialized : Boolean := False;
   Stack_Base : Integer_Address;

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch (New_Context : Thread_Context;
                             New_Stack : System.Address) is
   begin
      pragma Debug (Debug.Assert (New_Context /= null));
      --  pragma Debug (Debug.Assert (Stack_Base /= 0));

      --  Jump to the task body address (stored in New_Context)

      Jump_To_Context (New_Context, New_Stack);

      --  Never returns

      pragma Debug (Debug.Assert (False));
   end Context_Switch;

   -----------------------
   -- Global_Stack_Base --
   -----------------------

   function Global_Stack_Base return System.Address is
      (To_Address (Stack_Base));

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

   procedure Set_Global_Stack_Limit is
   begin
      pragma Compile_Time_Warning (True, "Set_Global_Stack_Limit");

      --  Stack_Base := Get_SP_Reg;
   end Set_Global_Stack_Limit;

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Natural is
   begin
      return Natural(System.Storage_Elements."-"(Stack_Start, Console_End));
   end Get_Stack_Max_Size_In_Bytes;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Natural is
   begin
      return Natural(Stack_Start) - Natural(Current_Stack_Top);
   end Get_Current_Stack_Size_In_Bytes;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Natural is
   begin
      return Natural(Current_Stack_Top) - Natural(Console_End);
   end Get_Current_Stack_Margin_In_Bytes;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area is
      Stack : array (1 .. Get_Stack_Max_Size_In_Bytes)
        of Interfaces.Unsigned_8;
      for Stack'Address use To_Address(Console_End);
   begin
      for I in Natural range Stack'First ..
        Stack'Last - Get_Current_Stack_Size_In_Bytes loop
         Stack (I) := Stack_Not_Used_Magic;
      end loop;
   end Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Natural is
      Stack : array (1 .. Get_Stack_Max_Size_In_Bytes)
        of Interfaces.Unsigned_8;
      for Stack'Address use To_Address(Console_End);
      I : Natural := 1;
      use type Interfaces.Unsigned_8;
   begin
      loop
         exit when Stack (I) /= Stack_Not_Used_Magic;
         I := I + 1;
      end loop;
      return Stack'Last - I + 1;
   end Get_Max_Stack_Usage_In_Bytes;

   -----------------------
   -- Enable_Interrupts --
   -----------------------

   procedure Enable_Interrupts is
   begin
      Interrupts_Enable_Global;
      Interrupts_Enable_Timer0;
   end Enable_Interrupts;

   ------------------------
   -- Disable_Interrupts --
   ------------------------

   procedure Disable_Interrupts is
   begin
      Interrupts_Disable_Global;
      Interrupts_Disable_Timer0;
   end Disable_Interrupts;

   ----------------------------
   -- Are_Interrupts_Enabled --
   ----------------------------

   function Are_Interrupts_Enabled return Boolean is
      Result : Integer;
   begin
      Result := Interrupts_Disabled;
      if Result = 0 then
         return True;
      else
         return False;
      end if;
   end Are_Interrupts_Enabled;

   ----------
   -- Time --
   ----------

   Now_Timer : HWTime := 0
     with
       Export        => True,
       Convention    => C;

   ----------------
   -- Get_HWTime --
   ----------------

   function Get_HWTime return HWTime is
   begin
      pragma Assert (Initialized);

      return Now_Timer;
   end Get_HWTime;

   -----------------------
   --  OS_Tick_Handler  --
   -----------------------

   OS_Tick_Handler : OS_Tick_Handler_Ac
     with
       Import,
       Convention => C,
       External_Name => "interrupts__os_tick_handler";
   --  Defined in interrupts.c

   --------------------
   -- Initialization --
   --------------------

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac) is
      function To_Unsigned_32 is
        new Ada.Unchecked_Conversion (System.Address, Interfaces.Unsigned_32);
   begin
      pragma Assert (not Initialized);

      -- Timer synchronization

      HAL.OS_Tick_Handler := OS_Tick_Handler;

      Interrupts_Set_Timer_Counter;
      Interrupts_Disable_Timer0;
      Interrupts_Disable_Global;

      Stack_Base := To_Integer (Stack_Base_Linker_Script'Address);

      Initialized := True;
   end Initialization;

end M2.HAL;
