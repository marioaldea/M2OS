----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Task that gets the color measures.
----------------------------------------------------------------------------

pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

with M2.Direct_IO;
with M2.HAL;
with M2.Kernel.API;
with Arduino.EVShield.EV3Ultrasonic;
with Stdint_H;
with Interfaces.C.Extensions;
with Arduino.EVShield.EV3Color;
with Arduino.EVShield.NXTColor;

package body EVShield_Color_Measure_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;
   ----------------
   -- Other_Task --
   ----------------

   Period : constant RT.Time_Span := RT.Milliseconds (200);
   Next_Time : RT.Time;

   Aux1 : Stdint_H.Uint8_T;
   Aux2 : Interfaces.C.Extensions.Bool;

   -- Ultrasonic sensors
   package Light_Right is new Arduino.EVShield.NXTColor.Color (Arduino.EVShield.SH_BBS2);
   package Light_Left is new Arduino.EVShield.EV3Color.Color (Arduino.EVShield.SH_BAS2);

   procedure Other_Task_Init is
   begin
      Next_Time := Ada.Real_Time.Clock;

      -- initialize the sensor mode.
      Aux1 := Light_Left.Set_Mode(Light_Left.MODE_Color_MeasureColor);
      Aux2 := Light_Right.Set_Color(Light_Right.SH_Type_COLORFULL);
   end Other_Task_Init;


   procedure Other_Task_Body is
   begin
      -- get the reading(s) from sensor
      Color_Right := Light_Right.Read_Color;
      Color_Left := Light_Left.Get_Val;

      -- print the sensor value(s)
--        DIO.Put ("Light right: "); DIO.Put_Line (Color_Right'Img);
--        DIO.Put ("Distance left: "); DIO.Put (Color_Left); DIO.New_Line;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Other_Task_Body;

   Other_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Other_Task_Init'Access,
      Body_Ac  => Other_Task_Body'Access,
      Priority => 4);

end EVShield_Color_Measure_Task;
