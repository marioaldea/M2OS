----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with GNAT.Source_Info;

package Tests_Reports is
   
   procedure Assert
     (Condition : Boolean;
      Msg       : String := GNAT.Source_Info.Source_Location);
   
   procedure Assert_Equal
     (Val1 : Integer; Val2 : Integer; Margin : Integer;
      Msg       : String := GNAT.Source_Info.Source_Location);

   procedure Test_OK;

   procedure Known_Bug;
   --  To inform it is a known bug so this circunstance can be displayed
   --  allong with the "FAILED" message.

end Tests_Reports;
