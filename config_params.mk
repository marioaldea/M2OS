#
# Target (uncomment just one line)
#
CONFIG_BOARD_ARDUINO=y
#CONFIG_BOARD_STM32F4=y
#CONFIG_BOARD_RP2040=y # (under development)
#CONFIG_BOARD_MICROBIT=y # (some bugs)
#CONFIG_BOARD_EPIPHANY=y
#CONFIG_BOARD_RISCV=y # (under development)

#
# Compilation flags for RTS and M2OS lib
#
#CONFIG_DEBUG_RTS=y              # -g -O0 flags
#CONFIG_ASSERTS_RTS=y            # -gnata -gnato flags
CONFIG_SIZEOPTIMIZATION_RTS=y   # -Os -gnatp

#
# Compilation flags for M2OS
#
#CONFIG_DEBUG_M2=y              # -g -O0 flags
#CONFIG_ASSERTS_M2=y            # -gnata -gnato flags
CONFIG_SIZEOPTIMIZATION_M2=y   # -Os -gnatp

#
# Compilation flags for applications
#
#CONFIG_DEBUG_APP=y              # -g -O0 flags
#CONFIG_ASSERTS_APP=y            # -gnata -gnato flags
CONFIG_SIZEOPTIMIZATION_APP=y   # -Os -gnatp

#
# Board specific options
#
CONFIG_GNATAVR_COMPILER="/opt/avr-ada/avr-ada-122"

#
# M2OS_Tool specific options
#
CONFIG_HOSTGNATFORTOOL="${HOME}/opt/GNAT/2019"
