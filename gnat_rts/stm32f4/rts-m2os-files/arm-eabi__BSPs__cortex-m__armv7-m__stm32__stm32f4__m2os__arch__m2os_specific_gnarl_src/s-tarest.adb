------------------------------------------------------------------------------
--                                                                          --
--                 GNAT RUN-TIME LIBRARY (GNARL) COMPONENTS                 --
--                                                                          --
--     S Y S T E M . T A S K I N G . R E S T R I C T E D . S T A G E S      --
--                                                                          --
--                                  B o d y                                 --
--                                                                          --
--                     Copyright (C) 1999-2018, AdaCore                     --
--                                                                          --
-- GNARL is free software; you can  redistribute it  and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 3,  or (at your option) any later ver- --
-- sion.  GNAT is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.                                     --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
-- GNARL was developed by the GNARL team at Florida State University.       --
-- Extensive contributions were provided by Ada Core Technologies, Inc.     --
--                                                                          --
------------------------------------------------------------------------------

--  M2OS  version of this package

--  This is a simplified version of the System.Tasking.Stages package, for use
--  with the ravenscar/HI-E profile.

--  This package represents the high level tasking interface used by the
--  compiler to expand Ada 95 tasking constructs into simpler run time calls.

with System.OS_Interface;
with Ada.Unchecked_Conversion;

package body System.Tasking.Restricted.Stages is

   -----------------------
   -- Local Subprograms --
   -----------------------

   procedure Create_Restricted_Task
     (Priority          : Integer;
      Stack_Address     : System.Address;
      Size              : System.Parameters.Size_Type;
      Sec_Stack_Address : System.Secondary_Stack.SS_Stack_Ptr;
      Sec_Stack_Size    : System.Parameters.Size_Type;
      Task_Info         : System.Task_Info.Task_Info_Type;
      CPU               : Integer;
      State             : Task_Procedure_Access;
      Discriminants     : System.Address;
      Created_Task      : Task_Id);
   --  Code shared between Create_Restricted_Task (the concurrent version) and
   --  Create_Restricted_Task_Sequential. See comment of the former in the
   --  specification of this package.

   -----------------------
   -- Restricted GNARLI --
   -----------------------

   -----------------------------------
   -- Activate_All_Tasks_Sequential --
   -----------------------------------

   procedure Activate_All_Tasks_Sequential is
   begin
      raise Program_Error;
   end Activate_All_Tasks_Sequential;

   -------------------------------
   -- Activate_Restricted_Tasks --
   -------------------------------

   procedure Activate_Restricted_Tasks
     (Chain_Access : Activation_Chain_Access) is
      pragma Unreferenced (Chain_Access);
   begin
      null;
      --  GNAT.IO.Put_Line (" Activate_Restricted_Tasks ");
   end Activate_Restricted_Tasks;

   ------------------------------------
   -- Complete_Restricted_Activation --
   ------------------------------------

   procedure Complete_Restricted_Activation is
   begin
      --  Nothing to be done

      null;
   end Complete_Restricted_Activation;

   ------------------------------
   -- Complete_Restricted_Task --
   ------------------------------

   procedure Complete_Restricted_Task is
   begin
      raise Program_Error;
   end Complete_Restricted_Task;

   ----------------------------
   -- Create_Restricted_Task --
   ----------------------------

   procedure Create_Restricted_Task
     (Priority          : Integer;
      Stack_Address     : System.Address;
      Size              : System.Parameters.Size_Type;
      Sec_Stack_Address : System.Secondary_Stack.SS_Stack_Ptr;
      Sec_Stack_Size    : System.Parameters.Size_Type;
      Task_Info         : System.Task_Info.Task_Info_Type;
      CPU               : Integer;
      State             : Task_Procedure_Access;
      Discriminants     : System.Address;
      Created_Task      : Task_Id)
   is
      pragma Unreferenced (Stack_Address, Size, Sec_Stack_Address,
                           Sec_Stack_Size, Task_Info, CPU);
      function To_Thread_Body_Ac is
        new Ada.Unchecked_Conversion (Task_Procedure_Access,
                                      OS_Interface.Thread_Body_Ac);

      Base_Priority        : System.Any_Priority;
   begin
      Base_Priority :=
        (if Priority = Unspecified_Priority
         then System.Default_Priority
         else System.Any_Priority (Priority));

      OS_Interface.Create_Thread (Th      => Created_Task.Thread'Access,
                                  Th_Body => To_Thread_Body_Ac (State),
                                  Prio    => Base_Priority,
                                  Args    => Discriminants);

      --  Initialize_ATCB (Th_Ac, Created_Task);

   end Create_Restricted_Task;

   procedure Create_Restricted_Task
     (Priority          : Integer;
      Stack_Address     : System.Address;
      Stack_Size        : System.Parameters.Size_Type;
      Sec_Stack_Address : System.Secondary_Stack.SS_Stack_Ptr;
      Sec_Stack_Size    : System.Parameters.Size_Type;
      Task_Info         : System.Task_Info.Task_Info_Type;
      CPU               : Integer;
      State             : Task_Procedure_Access;
      Discriminants     : System.Address;
      Elaborated        : Access_Boolean;
      Chain             : in out Activation_Chain;
      Task_Image        : String;
      Created_Task      : Task_Id)
   is
      pragma Unreferenced (Chain);
   begin
      if Partition_Elaboration_Policy = 'S' then

         --  A unit may have been compiled without partition elaboration
         --  policy, and in this case the compiler will emit calls for the
         --  default policy (concurrent). But if the partition policy is
         --  sequential, activation must be deferred.

         Create_Restricted_Task_Sequential
           (Priority, Stack_Address, Stack_Size, Sec_Stack_Address,
            Sec_Stack_Size, Task_Info, CPU, State, Discriminants, Elaborated,
            Task_Image, Created_Task);

      else
         Create_Restricted_Task
           (Priority, Stack_Address, Stack_Size, Sec_Stack_Address,
             Sec_Stack_Size, Task_Info, CPU, State, Discriminants,
             Created_Task);
      end if;
   end Create_Restricted_Task;

   ---------------------------------------
   -- Create_Restricted_Task_Sequential --
   ---------------------------------------

   procedure Create_Restricted_Task_Sequential
     (Priority          : Integer;
      Stack_Address     : System.Address;
      Stack_Size        : System.Parameters.Size_Type;
      Sec_Stack_Address : System.Secondary_Stack.SS_Stack_Ptr;
      Sec_Stack_Size    : System.Parameters.Size_Type;
      Task_Info         : System.Task_Info.Task_Info_Type;
      CPU               : Integer;
      State             : Task_Procedure_Access;
      Discriminants     : System.Address;
      Elaborated        : Access_Boolean;
      Task_Image        : String;
      Created_Task      : Task_Id)
   is
      pragma Unreferenced (Task_Image, Elaborated);

   begin
      Create_Restricted_Task
        (Priority, Stack_Address, Stack_Size, Sec_Stack_Address,
         Sec_Stack_Size, Task_Info, CPU, State, Discriminants, Created_Task);
   end Create_Restricted_Task_Sequential;

   ---------------------------
   -- Finalize_Global_Tasks --
   ---------------------------

   --  Dummy version since this procedure is not used in true ravenscar mode

   procedure Finalize_Global_Tasks is
   begin
      raise Program_Error;
   end Finalize_Global_Tasks;

   ---------------------------
   -- Restricted_Terminated --
   ---------------------------

   function Restricted_Terminated (T : Task_Id) return Boolean is
   begin
      raise Program_Error;
      return False;
   end Restricted_Terminated;

begin
   Tasking.Initialize;
end System.Tasking.Restricted.Stages;
