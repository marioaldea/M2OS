----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Authors: David Garcia Villaescusa (garcia@unican.es)
--           Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with E_Lib;
use E_Lib;
with Ada.Unchecked_Conversion;
with Interfaces;
use Interfaces;
with M2.Direct_IO;

package body Epiphany_Config is

   ----------------------
   -- Get_Core_Address --
   ----------------------

   function Get_Core_Address (C : in Core) return System.Address is
      Core_Address : System.Address;
      Core_Id : Interfaces.Unsigned_32;
      My_Core : Core;
   begin

      case C.Row is
         when 0 => Core_Address := System'To_Address(16#80000000#);
         when 1 => Core_Address := System'To_Address(16#84000000#);
         when 2 => Core_Address := System'To_Address(16#88000000#);
         when 3 => Core_Address := System'To_Address(16#8C000000#);
         when others => Core_Address := System'To_Address(16#0#);
      end case;

      -- The addition is version-dependent
      -- With the clause use type System.Address of M2OS should work
      case C.Col is
         when 0 => Core_Address := Add_Addresses (Core_Address, System'To_Address(16#800000#));
         when 1 => Core_Address := Add_Addresses (Core_Address, System'To_Address(16#900000#));
         when 2 => Core_Address := Add_Addresses (Core_Address, System'To_Address(16#A00000#));
         when 3 => Core_Address := Add_Addresses (Core_Address, System'To_Address(16#B00000#));
         when others => Core_Address := System'To_Address(16#0#);
      end case;

      return Core_Address;

   end Get_Core_Address;

   -------------------
   -- Add_Addresses --
   -------------------

   function Add_Addresses (A : System.Address; B : System.Address)return System.Address is
      function To_Unsigned_32 is new Ada.Unchecked_Conversion (System.Address, Unsigned_32);
   begin
      return System'To_Address(To_Unsigned_32 (A) + To_Unsigned_32 (B));
   end Add_Addresses;

end Epiphany_Config;
