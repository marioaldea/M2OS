//
//  Wrapper to the Zowi robot
//       (https://www.bq.com/es/zowi and https://www.bq.com/en/zowi)
//  Library downloaded from: https://github.com/bq/zowiLibs
//
#include <Servo.h>
#include <Oscillator.h>
#include <EEPROM.h>
#include <BatReader.h>
#include <US.h>
#include <LedMatrix.h>
#include <EnableInterrupt.h>
//#include <ZowiSerialCommand.h>
//ZowiSerialCommand SCmd;  //The SerialCommand object
#include <Zowi.h>

//-- Configuration of pins where the servos are attached
/*
         ---------------
        |               |
        |     O   O     |
        |               |
 YR ==> |               | <== YL
         ---------------
            ||     ||
            ||     ||
            ||     ||
 RR ==>   -----   ------  <== RL
          -----   ------
*/

#define PIN_YL 2 //servo[0]
#define PIN_YR 3 //servo[1]
#define PIN_RL 4 //servo[2]
#define PIN_RR 5 //servo[3]

#define TRIM_RR 0
#define TRIM_RL 0
#define TRIM_YR 0
#define TRIM_YL 0

//---Zowi Buttons
#define PIN_SecondButton 6
#define PIN_ThirdButton 7

Zowi zowi;

extern "C" int zowi_init() {
  //pinMode(PIN_SecondButton,INPUT);
  //pinMode(PIN_ThirdButton,INPUT);

  //Set the servo pins
  zowi.init(PIN_YL, PIN_YR, PIN_RL, PIN_RR, true, PIN_NoiseSensor, PIN_Buzzer, PIN_Trigger, PIN_Echo);
  //zowi.setTrims(TRIM_YL, TRIM_YR, TRIM_RL, TRIM_RR);

  //Interrumptions
  //enableInterrupt(PIN_SecondButton, secondButtonPushed, RISING);
  //enableInterrupt(PIN_ThirdButton, thirdButtonPushed, RISING);
  return sizeof(zowi);
}

extern "C" int zowi_home() {
  zowi.home();
}

extern "C" void zowi_walk(float steps, int T, int dir) {
  zowi.walk(steps, T, dir);
}

extern "C" void zowi_turn(float steps, int T, int dir) {
  zowi.turn(steps, T, dir);
}

extern "C" void zowi_putMouth(unsigned long int mouth) {
  zowi.putMouth(mouth);
}
extern "C" void zowi_putAnimationMouth(unsigned long int anim, int index) {
  zowi.putAnimationMouth(anim, index);
}

extern "C" void zowi_playGesture(int gesture) {
  zowi.playGesture(gesture);
}

extern "C" float zowi_getDistance() {
  return zowi.getDistance();
}
