----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Authors: Mario Aldea Rivas (aldeam@unican.es)
--           David Garcia Villaescusa (garcia@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Direct_IO;

package body Epiphany_Messages is
   SP_Size : constant Unsigned_32 := Sampling_Port'Size; -- Structure Size
                                                                    
   SPs : array (SP_Index) of aliased Sampling_Port;
   for SPs'Address use System'To_Address (16#7400#);
   
   function Addr_To_Int is new Ada.Unchecked_Conversion (System.Address, Integer);
   function Addr_To_SP is new Ada.Unchecked_Conversion (System.Address, SP_Id);
   function SP_To_Addr is new Ada.Unchecked_Conversion (SP_Id, System.Address);
   
   -------------------
   -- Get_SP_Offset --
   -------------------
   
   function Get_SP_Offset (Id : in SP_Index) return System.Address is
   begin
      return SPs(SP_Index)'Address;
   end Get_SP_Offset;
   
   --------------------------
   -- Start_Sampling_Ports --
   --------------------------
   
   procedure Start_Sampling_Ports is
      Core_Id : Unsigned_32;
      Core : E_Lib.Core;
      Index : SP_Index := 0;
   begin
      --  Get the current core
      Core_Id := E_Lib.E_Get_Coreid;
      E_Lib.E_Coords_From_Coreid (Core_Id, Core.Row, Core.Col);
      
      for Index in SP_Index'Range loop         
         SPs(Index).Initialized := False;
         SPs(Index).Is_New := False;
         SPs(Index).Mutex := 0;
         E_Lib.E_Mutex_Init (Core.Row, Core.Col, SPs(Index).Mutex, 0);
         SPs(Index).Core := Core;
      end loop;
   end Start_Sampling_Ports;
   
   -------------------------
   -- Init_Sampling_Ports --
   -------------------------

   function Init_Sampling_Port (C : in E_Lib.Core; 
                                Id : in SP_Index;
                                Addr : in System.Address;
                                Size : in Unsigned_32)
                                return SP_Id is
      Ext_Addr : System.Address;
      SP_Acc : SP_Id;
   begin
      -- Get the shared object address
      Ext_Addr := Epiphany_Config.Add_Addresses (Get_Core_Address (C), Addr);
      
      -- Obtain the Sampling Port
      SP_Acc := Get_Sampling_Port (C, Id);

      -- Take the mutex
      E_Lib.E_Mutex_Lock (SP_Acc.all.Core.Row, SP_Acc.all.Core.Col, SP_Acc.all.Mutex);
      
      SP_Acc.all.Size := Size;
      SP_Acc.all.Content_Address := Ext_Addr;
      SP_Acc.all.Initialized := True;
      SP_Acc.all.Is_New := False;
      
      -- Release the mutex
      E_Lib.E_Mutex_Unlock (SP_Acc.all.Core.Row, SP_Acc.all.Core.Col, SP_Acc.all.Mutex);
      
      return SP_Acc;
   end Init_Sampling_Port;
   
   ------------------------
   -- Get_Sampling_Ports --
   ------------------------

   function Get_Sampling_Port (C : in E_Lib.Core;
                               Id : in SP_Index)
                               return SP_Id is
   begin
      return Addr_To_SP(Epiphany_Config.Add_Addresses (Get_Core_Address (C), SPs(Id)'Address));
   end Get_Sampling_Port;
   
   --------------------------
   -- Write_Sampling_Ports --
   --------------------------

   function Write_Sampling_Port (SP : in SP_Id;
                                 Orig : in System.Address;
                                 Orig_Size : in Unsigned_32)
                                 return Boolean is
      -- Source of the content to be written
      Orig_Array : array (1 .. Orig_Size/8) of Unsigned_8;
      for Orig_Array'Address use Orig;
   begin
      -- Take the mutex
      E_Lib.E_Mutex_Lock (SP.all.Core.Row, SP.all.Core.Col, SP.all.Mutex);
      -- Check if there is enough space
      if Orig_Size > SP.all.Size then
         return False;
      end if;
      declare
         -- Taget where the content must be written
         Dest_Array : array (1 .. Orig_Size/8) of Unsigned_8;
         for Dest_Array'Address use SP.all.Content_Address;
      begin
         -- Copying the content
         for I in 1 .. Orig_Size / 8 loop
            Dest_Array (I) := Orig_Array (I);
         end loop;
      end;
      SP.Is_New := True;
      
      -- Release the mutex
      E_Lib.E_Mutex_Unlock (SP.all.Core.Row, SP.all.Core.Col, SP.all.Mutex);
      return True;
   end Write_Sampling_Port;
   
   -------------------------
   -- Read_Sampling_Ports --
   -------------------------

   procedure Read_Sampling_Port (SP : in SP_Id;
                                 Dest : in System.Address;
                                 Dest_Size : in Unsigned_32;
                                 Successful : out Boolean;
                                 Is_New : out Boolean) is
      SP_Content : array (1 .. SP.all.Size/8) of Unsigned_8;
      for SP_Content'Address use SP.all.Content_Address;
      Dest_Array : array (1 .. SP.all.Size/8) of Unsigned_8;
      for Dest_Array'Address use Dest;
   begin
      -- Take the mutex
      E_Lib.E_Mutex_Lock (SP.all.Core.Row, SP.all.Core.Col, SP.all.Mutex);
      
      -- Check if there is enough space
      if Dest_Size < SP.all.Size then
         Successful := False;
      else
         -- Copying the content
         for I in 1 .. SP.all.Size / 8 loop
            Dest_Array (I) := SP_Content (I);
         end loop;
         Is_New := SP.all.Is_New;
         -- This content is no longer new
         SP.all.Is_New := False;
         Successful := True;
      end if;
      
      -- Release the mutex
      E_Lib.E_Mutex_Unlock (SP.all.Core.Row, SP.all.Core.Col, SP.all.Mutex);
   end Read_Sampling_Port;
   
   -------------------
   -- Take_SP_Mutex --
   -------------------
   
   procedure Take_SP_Mutex (SP_Acc : in SP_Id) is
   begin
      -- Take the mutex
      E_Lib.E_Mutex_Lock (SP_Acc.all.Core.Row, SP_Acc.all.Core.Col, SP_Acc.all.Mutex);
   end Take_SP_Mutex;
   
   ----------------------
   -- Release_SP_Mutex --
   ----------------------
   
   procedure Release_SP_Mutex (SP_Acc : in SP_Id) is
   begin
      -- Release the mutex
      E_Lib.E_Mutex_Unlock (SP_Acc.all.Core.Row, SP_Acc.all.Core.Col, SP_Acc.all.Mutex);
   end Release_SP_Mutex;
   
begin
   Epiphany_Messages.Start_Sampling_Ports;
end Epiphany_Messages;
