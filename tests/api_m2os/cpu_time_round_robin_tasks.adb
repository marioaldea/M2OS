----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Task_Identification;
pragma Warnings (Off);
with System.Tasking;
pragma Warnings (On);
with Ada.Unchecked_Conversion;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;
with M2.Kernel.API;

with Eat_CPU_Time;

with Tests_Reports;

package body CPU_Time_Round_Robin_Tasks is
   package DIO renames M2.Direct_IO;
   package API renames M2.Kernel.API;
   
   Prio : constant := 3;
   Eat_Us : constant := 10_000;
      
   --------------------------
   -- Task_Id_To_Thread_Ac --
   --------------------------

   function Task_Id_To_Thread_Ac (T_Id : Ada.Task_Identification.Task_Id)
                                  return API.Thread_Ac Is
      function To_STask_Id is
        new Ada.Unchecked_Conversion (Ada.Task_Identification.Task_Id,
                                      System.Tasking.Task_Id);
      pragma Warnings (Off);
      function To_Th_Ac is
        new Ada.Unchecked_Conversion (System.Address,
                                      API.Thread_Ac);
      pragma Warnings (On);
   begin
      return To_Th_Ac (To_STask_Id (T_Id).Thread'Address);
   end Task_Id_To_Thread_Ac;
         
   -----------
   -- TaskA --
   -----------

   procedure TaskA_Init is
   begin
      DIO.Put ("-TA Init-");
   end TaskA_Init;

   procedure TaskA_Body is
   begin
      Eat_CPU_Time.Eat_Microseconds (Eat_Us / 4);

      API.Running_Thread_Ends_Job_Without_Blocking;
   end TaskA_Body;

   Task_A : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => TaskA_Init'Access,
      Body_Ac  => TaskA_Body'Access,
      Priority => Prio);

   -----------
   -- TaskB --
   -----------

   procedure TaskB_Init is
   begin
      DIO.Put ("-TB Init-");
   end TaskB_Init;

   procedure TaskB_Body is
   begin
      Eat_CPU_Time.Eat_Microseconds (Eat_Us / 2);
      
      API.Running_Thread_Ends_Job_Without_Blocking;
   end TaskB_Body;

   Task_B : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => TaskB_Init'Access,
      Body_Ac  => TaskB_Body'Access,
      Priority => Prio);

   -----------
   -- TaskC --
   -----------
   C_Counter : Natural := 0;
   Counter_Limit : constant := 10;

   procedure TaskC_Init is
   begin
      DIO.Put ("-TC Init-"); 
      DIO.New_Line;
   end TaskC_Init;

   procedure TaskC_Body is
      CPU_Time_A : Integer;
      CPU_Time_B : Integer;
      CPU_Time_C : Integer;
      CPU_Time_Total : Integer;  --  CPU_Time_A + CPU_Time_B + CPU_Time_C
      Now : API.Time;
   begin
      Eat_CPU_Time.Eat_Microseconds (Eat_Us);
      
      CPU_Time_A :=
        Integer (API.Thread_CPU_Time (Task_Id_To_Thread_Ac (Task_A'Identity)));       
      CPU_Time_B :=
        Integer (API.Thread_CPU_Time (Task_Id_To_Thread_Ac (Task_B'Identity)));     
      CPU_Time_C :=Integer (API.Thread_CPU_Time (API.Self));
      CPU_Time_Total := CPU_Time_A + CPU_Time_B + CPU_Time_C;
      Now := API.Get_Time;
      
      DIO.Put ("CPU time A:");
      DIO.Put (CPU_Time_A);  
      DIO.Put ("  CPU time B:");
      DIO.Put (CPU_Time_B);  
      DIO.Put ("  CPU time C:");
      DIO.Put (CPU_Time_C);  
      DIO.New_Line;
      DIO.Put ("Now:");
      DIO.Put (Integer (Now));  
      DIO.Put ("  CPU Total:");
      DIO.Put (CPU_Time_Total);  
      DIO.New_Line;
      
      --  The total CPU time must be equal than the current time and the
      --  idle time must be 0.
      
      Tests_Reports.Assert_Equal (Integer (Now), CPU_Time_Total, 1);
      Tests_Reports.Assert_Equal (0, Integer (API.Idle_CPU_Time), 0);
      
      --  Check all tasks have executed and its CPU times are larger than 0
      
      Tests_Reports.Assert (C_Counter = 0 or else
                              (CPU_Time_A > 0 and CPU_Time_B > 0));
      
      C_Counter := C_Counter + 1;
      if C_Counter = Counter_Limit then
         Tests_Reports.Test_OK;
      end if;
      
      API.Running_Thread_Ends_Job_Without_Blocking;
   end TaskC_Body;

   Task_C : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => TaskC_Init'Access,
      Body_Ac  => TaskC_Body'Access,
      Priority => Prio);
   
end CPU_Time_Round_Robin_Tasks;
