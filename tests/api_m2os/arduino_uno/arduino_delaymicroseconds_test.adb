----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

pragma Profile (Ravenscar);
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);
with M2.Direct_IO;

with System;
with Ada.Real_Time;

with Tests_Reports;
with Arduino;
with M2.HAL.Timer;

procedure Arduino_DelayMicroseconds_Test is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;
   package HT renames M2.HAL.Timer;
   package HAL renames M2.HAL;

   use type RT.Time, RT.Time_Span, HT.Timer_Ticks, HAL.HWTime;

   T_Ini, T_Fin : RT.Time;
   HWT_Ini, HWT_Fin : HAL.HWTime;
   Ticks_Ini, Ticks_Fin : HT.Timer_Ticks;

   Delay_US_Limit_1 : constant := 1_000 - HT.Microseconds_Per_Timer_Count * 2;
   Delay_US_Limit_1_Loops : constant := 100;
   Delay_US_Limit_2 : constant := Delay_US_Limit_1 - 1;
   Delay_US_Limit_2_Loops : constant := 100;
   Delay_US_Very_Short : constant := 1;
   Delay_US_Very_Short_4 : constant := 4;
   Delay_US_Very_Short_4_Loops : constant := 60; --  Multiple of 20
   Delay_US_Short : constant := 20;
   Delay_US_Short_Loops : constant := 1_000;
   Delay_US_Medium : constant := 480;
   Delay_US_Long : constant := 16_000;

   function To_Millis (D : Duration) return Integer is
   begin
      return Integer (D * 1_000);
   end To_Millis;

   procedure Put_Duration (D : Duration) is
      S : Integer := Integer (D);
   begin
      DIO.Put (S);
      DIO.Put ("s");
      DIO.Put (To_Millis (D) - S * 1_000);
      DIO.Put ("ms");
   end Put_Duration;

begin
   DIO.Put_Line ("Arduino_Delay_Microseconds_Test");

   --  Test Delay_Microseconds (very-short)

   Ticks_Ini := HT.Get_Counter;
   Arduino.Delay_Microseconds (Delay_US_Very_Short);
   Ticks_Fin := HT.Get_Counter;

   DIO.Put ("Delay_Microseconds very short (");
   DIO.Put (Integer (Delay_US_Very_Short));
   DIO.Put ("us)  Measured:");
   DIO.Put (Integer (Ticks_Fin - Ticks_Ini));
   DIO.Put ("ticks (");
   DIO.Put (Integer (Ticks_Fin - Ticks_Ini) *
              HT.Microseconds_Per_Timer_Count);
   DIO.Put ("us)");
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_US_Very_Short,
      Val2   => Integer (Ticks_Fin - Ticks_Ini) *
          HT.Microseconds_Per_Timer_Count,
      Margin => HT.Microseconds_Per_Timer_Count - 1);

   --  Test Delay_Microseconds (very-short several times)

   Ticks_Ini := HT.Get_Counter;
   for I in 1 .. Delay_US_Very_Short_4_Loops loop
      Arduino.Delay_Microseconds (Delay_US_Very_Short_4);
   end loop;
   Ticks_Fin := HT.Get_Counter;

   DIO.Put ("Delay_Microseconds very short several times (");
   DIO.Put (Integer (Delay_US_Very_Short_4));
   DIO.Put ("us)*");
   DIO.Put (Integer (Delay_US_Very_Short_4_Loops));
   DIO.Put ("  Measured:");
   DIO.Put (Integer (Ticks_Fin - Ticks_Ini));
   DIO.Put ("ticks (");
   DIO.Put (Integer (Ticks_Fin - Ticks_Ini) *
              HT.Microseconds_Per_Timer_Count);
   DIO.Put ("us)");
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_US_Very_Short_4 * Delay_US_Very_Short_4_Loops,
      Val2   => Integer (Ticks_Fin - Ticks_Ini) *
          HT.Microseconds_Per_Timer_Count,
      Margin => Delay_US_Very_Short_4_Loops / 3);

   --  Test Delay_Microseconds (short several times)

   T_Ini := RT.Clock;
   for I in 1 .. Delay_US_Short_Loops loop
      Arduino.Delay_Microseconds (Delay_US_Short);
   end loop;
   T_Fin := RT.Clock;

   DIO.Put ("Delay_Microseconds short (");
   DIO.Put (Integer (Delay_US_Short));
   DIO.Put ("us)*");
   DIO.Put (Integer (Delay_US_Short_Loops));
   DIO.Put ("   Measured:");
   Put_Duration (RT.To_Duration (T_Fin - T_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => (Delay_US_Short * Delay_US_Short_Loops) / 1_000,
      Val2   => To_Millis (RT.To_Duration (T_Fin - T_Ini)),
      Margin => 1);

   --  Test Delay_Microseconds (Delay_US_Limit_1)

   T_Ini := RT.Clock;
   for I in 1 .. Delay_US_Limit_1_Loops loop
      Arduino.Delay_Microseconds (Delay_US_Limit_1);
   end loop;
   T_Fin := RT.Clock;

   DIO.Put ("Delay_Microseconds Limit_1 (");
   DIO.Put (Integer (Delay_US_Limit_1));
   DIO.Put ("us)*");
   DIO.Put (Integer (Delay_US_Limit_1_Loops));
   DIO.Put ("   Measured:");
   Put_Duration (RT.To_Duration (T_Fin - T_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => (Delay_US_Limit_1 * Delay_US_Limit_1_Loops) / 1_000,
      Val2   => To_Millis (RT.To_Duration (T_Fin - T_Ini)),
      Margin => 1);

   --  Test Delay_Microseconds (Delay_US_Limit_2)

   Ticks_Ini := HT.Get_Counter;
   for I in 1 .. Delay_US_Limit_2_Loops loop
      Arduino.Delay_Microseconds (Delay_US_Limit_2);
   end loop;
   Ticks_Fin := HT.Get_Counter;

   DIO.Put ("Delay_Microseconds Limit_2 (");
   DIO.Put (Integer (Delay_US_Limit_2));
   DIO.Put ("us)*");
   DIO.Put (Integer (Delay_US_Limit_2_Loops));
   DIO.Put ("   Measured:");
   Put_Duration (RT.To_Duration (T_Fin - T_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => (Delay_US_Limit_2 * Delay_US_Limit_2_Loops) / 1_000,
      Val2   => To_Millis (RT.To_Duration (T_Fin - T_Ini)),
      Margin => 1);

   --  Test Delay_Microseconds (short-once)

   Ticks_Ini := HT.Get_Counter;
   Arduino.Delay_Microseconds (Delay_US_Short);
   Ticks_Fin := HT.Get_Counter;

   DIO.Put ("Delay_Microseconds Short (");
   DIO.Put (Integer (Delay_US_Short));
   DIO.Put ("us)  Measured:");
   DIO.Put (Integer (Ticks_Fin - Ticks_Ini));
   DIO.Put ("ticks (");
   DIO.Put (Integer (Ticks_Fin - Ticks_Ini) *
              HT.Microseconds_Per_Timer_Count);
   DIO.Put ("us)");
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_US_Short,
      Val2   => Integer (Ticks_Fin - Ticks_Ini) *
          HT.Microseconds_Per_Timer_Count,
      Margin => HT.Microseconds_Per_Timer_Count);

   --  Test Delay_Microseconds (medium-once)

   Ticks_Ini := HT.Get_Counter;
   Arduino.Delay_Microseconds (Delay_US_Medium);
   Ticks_Fin := HT.Get_Counter;

   DIO.Put ("Delay_Microseconds Medium (");
   DIO.Put (Integer (Delay_US_Medium));
   DIO.Put ("us)  Measured:");
   DIO.Put (Integer (Ticks_Fin - Ticks_Ini));
   DIO.Put ("ticks (");
   DIO.Put (Integer (Ticks_Fin - Ticks_Ini) *
              HT.Microseconds_Per_Timer_Count);
   DIO.Put ("us)");
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_US_Medium,
      Val2   => Integer (Ticks_Fin - Ticks_Ini) *
          HT.Microseconds_Per_Timer_Count,
      Margin => HT.Microseconds_Per_Timer_Count * 3);

   --  Test Delay_Microseconds (long)

   HWT_Ini := HAL.Get_HWTime;
   Arduino.Delay_Microseconds (Delay_US_Long);
   HWT_Fin := HAL.Get_HWTime;

   DIO.Put ("Delay_Microseconds Long (");
   DIO.Put (Integer (Delay_US_Long / 1_000));
   DIO.Put ("ms)  Measured:");
   DIO.Put (Integer (HWT_Fin - HWT_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Integer (Delay_US_Long / 1_000),
      Val2   => Integer (HWT_Fin - HWT_Ini),
      Margin => 1);

   Tests_Reports.Test_OK;
end Arduino_DelayMicroseconds_Test;
