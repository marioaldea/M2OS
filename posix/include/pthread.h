//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// M2OS POSIX-like layer.
// The M2OS POSIX-like layer is in a preliminary stage.
#ifndef	_M2_PTHREAD_H_
#define _M2_PTHREAD_H_
#include <m2_api.h>

struct sched_param {
  int sched_priority;
};

typedef struct {
  struct sched_param param;
} pthread_attr_t;

/*
 * pthread_create
 */
int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
		   void *(*start_routine) (void *), void *arg);
// Implemented in m2_pthread.h

/*
 * pthread_attr_init
 */
static inline int pthread_attr_init (pthread_attr_t *attr) {
  attr->param.sched_priority = 0;
  return 0;
}

/*
 * pthread_attr_setschedparam
 */
static inline int pthread_attr_setschedparam (pthread_attr_t *attr,
					      const struct sched_param *param) {
  attr->param = *param;
  return 0;
}

/*
 * yield_to_higher (M2OS specific)
 */
static inline int yield_to_higher() {
  m2_dispatch_after_yield_to_higher();
  return 0;
}


#endif // _M2_PTHREAD_H_
