//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// M2OS POSIX-like layer.
// The M2OS POSIX-like layer is in a preliminary stage.
#ifndef	_M2_TIME_H_
#define _M2_TIME_H_
#include <stdint.h>
#include <m2_api.h>

typedef int32_t time_t;
typedef int clockid_t;

#define NS_IN_S 1000000000
#ifndef HWTIME_64
# define NS_PER_TICK (NS_IN_S / HWTIME_HZ)
#endif

/*
 * struct timespec
 */

// By default (when M2_USE_POSIX_TIMESPEC is NOT defined) timespec is a count of
// clock ticks (type hwtime_t). This implementation is more efficient in terms
// of footprint and computation time.
// In order to use legacy POSIX code, macro M2_USE_POSIX_TIMESPEC should be
// defined in your program.
// M2 programs that want to be compatible with both implementations of timespec
// should not use the timespec fields directly. Instead, they should use
// timespec objects through the macros defined below (TS, TS_SEC, TS_INC, ...).

#ifndef M2_USE_POSIX_TIMESPEC
// timespec based on hardware time (more efficient)
struct timespec {
  hwtime_t hwtime;
};
# ifdef HWTIME_64
#  define TS(s, ns) {.hwtime = ((hwtime_t) (s)) * HWTIME_HZ + \
                                (hwtime_t) (ns) * HWTIME_HZ / NS_IN_S}
# else
#  define TS(s, ns) {.hwtime = ((hwtime_t) (s)) * HWTIME_HZ + (ns) / NS_PER_TICK}
# endif
# define TS_ADD(a, b, c) (a).hwtime = (b).hwtime + (c).hwtime
# define TS_SUB(a, b, c) (a).hwtime = (b).hwtime - (c).hwtime
# define TS_INC(a, b) (a).hwtime += (b).hwtime
# define TS_SEC(ts) ((ts).hwtime / HWTIME_HZ)
# ifdef HWTIME_64
#  define TS_NSEC(ts) (((ts).hwtime % HWTIME_HZ) * NS_IN_S / HWTIME_HZ)
# else
#  define TS_NSEC(ts) (((ts).hwtime % HWTIME_HZ) * NS_PER_TICK)
# endif
# define TS2HWT(ts) ((ts).hwtime)
# define HWT2TS(ts, hwt) (ts).hwtime = hwt
# define TS2NS(ts) ((int64_t) (ts).hwtime * NS_IN_S / HWTIME_HZ)

#else
// Standard POSIX timespec (more inefficient time conversions)
struct timespec {
  time_t tv_sec;
  int32_t tv_nsec;
};
# define TS(s, ns) {s, ns}
# define TS_ADD(a, b, c) \
  (a).tv_nsec = (b).tv_nsec + (c).tv_nsec; \
  (a).tv_sec = (b).tv_sec + (c).tv_sec; \
  if ((a).tv_nsec >= NS_IN_S) { \
   (a).tv_nsec = (a).tv_nsec - NS_IN_S; \
   (a).tv_sec = (a).tv_sec + 1; \
  }
# define TS_INC(a, b) \
  (a).tv_nsec += (b).tv_nsec; \
  (a).tv_sec += (b).tv_sec; \
  if ((a).tv_nsec >= NS_IN_S) { \
   (a).tv_nsec = (a).tv_nsec - NS_IN_S; \
   (a).tv_sec = (a).tv_sec + 1; \
  }
# define TS_SEC(ts) ((ts).tv_sec)
# define TS_NSEC(ts) ((ts).tv_nsec)
# ifdef HWTIME_64
# define TS2HWT(ts) ((hwtime_t) (ts).tv_sec * HWTIME_HZ + \
                     (hwtime_t) (ts).tv_nsec * HWTIME_HZ / NS_IN_S)
# define HWT2TS(ts, hwt) \
  (ts).tv_sec = (hwt) / HWTIME_HZ; \
  (ts).tv_nsec = (hwtime_t) ((hwt) % HWTIME_HZ) * NS_IN_S / HWTIME_HZ
# else
#  define TS2HWT(ts) ((ts).tv_sec * HWTIME_HZ + (ts).tv_nsec / NS_PER_TICK)
#  define HWT2TS(ts, hwt) \
  (ts).tv_sec = (hwt) / HWTIME_HZ; \
  (ts).tv_nsec = ((hwt) % HWTIME_HZ) * NS_PER_TICK
# endif
# define TS2NS(ts) ((int64_t) (ts).tv_sec * NS_IN_S + (ts).tv_nsec)
#endif  // ifndef M2_USE_POSIX_TIMESPEC

#define CLOCK_MONOTONIC  0
#define TIMER_ABSTIME 1

/*
 * clock_gettime
 */
static inline int clock_gettime(clockid_t clock_id, struct timespec *tp) {
  HWT2TS(*tp, m2__kernel__api__get_time());
  return 0;
}

/*
 * clock_nanosleep
 */
static inline int clock_nanosleep(clockid_t clock_id, int flags,
				  const struct timespec *rqtp,
				  struct timespec *rmtp) {
  if (flags == TIMER_ABSTIME) {
    m2__kernel__scheduler__suspend_running_thread(TS2HWT(*rqtp));
  } else {
    m2__kernel__scheduler__suspend_running_thread(TS2HWT(*rqtp) +
						  m2__kernel__api__get_time());
  }
  return 0;
}

#endif // _M2_TIME_H_
