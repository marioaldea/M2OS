----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Test the time for the delay until and for Ada.Real_Time.Clock are
--  equivalent.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;
with M2.Direct_IO;

with Measurements;
with Tests_Reports;

procedure Delay_And_Clock_Test  is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   Loop_Counter : Integer := 0;

   Delay_In_Milliseconds : constant := 200;
   Margin_In_Milliseconds : constant := 20;
   TS_Margin : constant RT.Time_Span :=
     RT.Milliseconds (Margin_In_Milliseconds);

   use type Ada.Real_Time.Time, RT.Time_Span, Measurements.Time;

   Next_Time, Start_Time : Ada.Real_Time.Time;
   Elapsed_Time : Ada.Real_Time.Time_Span;
   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (Delay_In_Milliseconds);

   Ticks_Per_Ms : constant Integer :=
     1_000 / Integer (Measurements.Time_Unit_In_Us);

begin
   DIO.Put ("-Delay_And_Clock_Test-");

   Measurements.Init (Measurements.Long);
   Measurements.Start_Measurement;

   Start_Time := Ada.Real_Time.Clock;
   loop
      DIO.Put ("-Main-");

      if Loop_Counter = 1 then
         --  Check RT.Clock is OK
         Elapsed_Time := Ada.Real_Time.Clock - Start_Time;
         Tests_Reports.Assert (abs (Elapsed_Time - Period) <= TS_Margin);

         --  Check conversion to Duration is OK
         DIO.Put (Integer (RT.To_Duration (Elapsed_Time) * 1_000));
         Tests_Reports.Assert_Equal
           (Integer (RT.To_Duration (Elapsed_Time) * 1_000),
            Delay_In_Milliseconds, Margin_In_Milliseconds);

         --  Check Measurements package works OK
         Measurements.End_Measurement;
         DIO.Put ("Measurement_Time (ms):");
         DIO.Put (Integer (Measurements.Measurement_Time) / Ticks_Per_Ms);
         Tests_Reports.Assert_Equal
           (Integer (Measurements.Measurement_Time) / Ticks_Per_Ms,
            Delay_In_Milliseconds, Margin_In_Milliseconds);

         Measurements.Finish;
      end if;

      Loop_Counter := Loop_Counter + 1;

      Next_Time := Start_Time + Period;
      delay until Next_Time;
   end loop;

end Delay_And_Clock_Test;
