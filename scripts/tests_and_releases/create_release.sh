#!/bin/sh
# Create release from the GitLab repository of M2OS
# A cloned repository is expected in $M2OS_HOME/M2OS_MASTER/ and
# $M2OS_HOME/M2OS_WIKI/
#  $ git clone git@gitlab.com:marioaldea/M2OS.git M2OS_MASTER
#  $ git clone git@gitlab.com:marioaldea/M2OS.wiki M2OS_WIKI
#set -o xtrace

: ${2?"Usage: $0 M2OS_HOME VERSION_NAME (e.g. create_relase.sh /home/mtests/m2os M2OS_19-10-05)"}

readonly M2OS_HOME=$1
readonly DISTRIB=$2

readonly M2OS_MASTER="M2OS_MASTER"
readonly M2OS_WIKI="M2OS_WIKI"
readonly DISTRIB_WIKI=${DISTRIB}"/wiki"

# Copy to the distrib directory
echo "Creating the distribution directory..."
cd ${M2OS_HOME}
rm -rf ${DISTRIB}
cp -r $M2OS_MASTER ${DISTRIB}
mkdir ${DISTRIB_WIKI}
cp -r $M2OS_WIKI/* ${DISTRIB_WIKI}

# convert wiki files (in markdown format) to PDF
echo "Converting wiki files (in markdown format) to PDF..."
cd $M2OS_HOME/$DISTRIB_WIKI
for f in *.md; do
    fname=$(basename $f .md)
    # packages required: sudo apt install texlive pandoc
    pandoc $f -s -V geometry:margin=1cm -o ${fname}.pdf
done

# convert other markdown files
echo "Converting other markdown files..."
markdown_files="INSTALL.md README.md"
cd $M2OS_HOME/$DISTRIB
for f in ${markdown_files}; do
    fname=$(basename $f .md)
    pandoc $f -s -V geometry:margin=1cm -o ${fname}.pdf
done

# remove not required files and directories
echo "Remove not required files and directories"
not_required=".git/ .gitignore 
             arch/arduino_uno/drivers/lcd_1602a_driver.ad?
             arch/arduino_uno/drivers/Makefile
             arch/arduino_uno/drivers/lcd.ad?
             arch/arduino_uno/drivers/lcd-wiring.ads
             arch/arduino_uno/drivers/libcore/arduino-coordinated_servos.ad?
             arch/arduino_uno/hal/marte-hal-wrapper_main.ad?
             arch/avr_iot/
             arch/rp2040/
             examples/api_m2os/arduino_uno/under_development/
             gnat_rts/avr_iot/
             gnat_rts/rp2040/
             scripts/lxdialog/
             scripts/kconfig/
             scripts/tests_and_releases/create_release.sh
             scripts/tests_and_releases/create_release_and_tests.sh
             tests/performance
             tests_*.log"
cd $M2OS_HOME/$DISTRIB
for f in ${not_required}; do
    rm -rf ../$DISTRIB/$f
done

# compress release
echo "Compressing release..." 
cd $M2OS_HOME
rm -f ${DISTRIB}.zip
zip -q -r ${DISTRIB}.zip $DISTRIB
echo " Release ${DISTRIB} done." 



