----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of the "Seeed_Alphanumeric_Display_HT16K33".
--  https://www.seeedstudio.com/Grove-0-54-Red-Quad-Alphanumeric-Display-p-4032.html
--  https://github.com/Seeed-Studio/Seeed_Alphanumeric_Display_HT16K33/blob/master/grove_alphanumeric_display.h
----------------------------------------------------------------------------
with Arduino.Wire;

package body Arduino.Alphanumeric_Display is

   TYPE_4_DEFAULT_I2C_ADDR : constant := 16#71#;

   type Tube_Type_T is (TYPE_2,
                        TYPE_4);
   for Tube_Type_T'Size use Interfaces.C.Int'Size;

   ------------------------
   -- Seeed_Digital_Tube --
   ------------------------

   type Seeed_Digital_Tube_Filling is
     array(1 .. Seeed_Digital_Tube_Size_In_Bytes) of Interfaces.Unsigned_8;

   type Seeed_Digital_Tube is limited record
      Filling: Seeed_Digital_Tube_Filling;
   end record;
   pragma Import (CPP, Seeed_Digital_Tube);
   for Seeed_Digital_Tube'Size use Seeed_Digital_Tube_Size_In_Bytes * 8;
   for Seeed_Digital_Tube'Alignment use Standard'Maximum_Alignment;

   ------------------------
   -- Imported functions --
   ------------------------

   function New_Digital_Tube return Seeed_Digital_Tube;
   pragma CPP_Constructor (New_Digital_Tube, "_ZN18Seeed_Digital_TubeC1Ev");

   procedure Set_Tube_Type_CPP (This : access Seeed_Digital_Tube;
                                Tube_Type : Tube_Type_T;
                                Addr : Arduino.Wire.Dev_Address)
     with Import, Convention =>  CPP,
     External_Name => "_ZN18Seeed_Digital_Tube11setTubeTypeE10TubeType_th";

   procedure Clear_CPP (This : access Seeed_Digital_Tube)
     with Import, Convention =>  CPP,
     External_Name => "_ZN18Seeed_Digital_Tube5clearEv";

   procedure Display_Num_CPP (This : access Seeed_Digital_Tube;
                              Num : Interfaces.Unsigned_32;
                              Interval : Time_MS)
     with Import, Convention =>  CPP,
     External_Name => "_ZN18Seeed_Digital_Tube10displayNumEmm";

   procedure Display_String_CPP (This : access Seeed_Digital_Tube;
                                 Str : Interfaces.C.Strings.Char_Array_Access;
                                 Interval : Time_MS)
     with Import, Convention =>  CPP,
     External_Name => "_ZN18Seeed_Digital_Tube13displayStringEPcm";

   procedure Set_Point_CPP (This : access Seeed_Digital_Tube;
                            First_Dot : Boolean8;
                            Second_Dot : Boolean8)
     with Import, Convention =>  CPP,
     External_Name => "_ZN18Seeed_Digital_Tube8setPointEbb";

   procedure Set_Blink_Rate_CPP (This : access Seeed_Digital_Tube;
                                 Blink_Type : Blink_Type_T)
     with Import, Convention =>  CPP,
     External_Name => "_ZN7HT16K3312setBlinkRateE12blink_type_t";

   procedure Set_Brightness_CPP (This : access Seeed_Digital_Tube;
                                 Brightness : Interfaces.Unsigned_8)
     with Import, Convention =>  CPP,
     External_Name => "_ZN7HT16K3313setBrightnessEh";

   -------------
   -- Display --
   -------------

   Display : aliased Seeed_Digital_Tube;

   --------------------------------
   -- Begin_Alphanumeric_Display --
   --------------------------------

   procedure Begin_Alphanumeric_Display is
   begin
      Set_Tube_Type_CPP (Display'Access, TYPE_4, TYPE_4_DEFAULT_I2C_ADDR);
      Wire.Begin_Two_Wire (Wire.Wire'Access);
   end Begin_Alphanumeric_Display;

   -----------
   -- Clear --
   -----------

   procedure Clear is
   begin
      Clear_CPP (Display'Access);
   end Clear;

   -----------------
   -- Display_Num --
   -----------------

   procedure Display_Num (Num : Interfaces.Unsigned_32;
                          Interval : Time_MS := 500) is
   begin
      Display_Num_CPP (Display'Access, Num, Interval);
   end Display_Num;

   ----------------------------------------
   -- Display_Null_Terminated_Char_Array --
   ----------------------------------------

   procedure Display_Null_Terminated_Char_Array
     (Str : Interfaces.C.Strings.char_array_access;
      Interval : Time_MS := 500) is
   begin
      Display_String_CPP (Display'Access, Str, Interval);
   end Display_Null_Terminated_Char_Array;

   ---------------
   -- Set_Point --
   ---------------

   procedure Set_Point (First_Dot : Boolean8;
                        Second_Dot : Boolean8) is
   begin
      Set_Point_CPP (Display'Access, First_Dot, Second_Dot);
   end Set_Point;

   --------------------
   -- Set_Blink_Rate --
   --------------------

   procedure Set_Blink_Rate (Blink_Type : Blink_Type_T) is
   begin
      Set_Blink_Rate_CPP (Display'Access, Blink_Type);
   end Set_Blink_Rate;

   --------------------
   -- Set_Brightness --
   --------------------

   procedure Set_Brightness (Brightness : Interfaces.Unsigned_8) is
   begin
      Set_Brightness_CPP (Display'Access, Brightness);
   end Set_Brightness;

end Arduino.Alphanumeric_Display;
