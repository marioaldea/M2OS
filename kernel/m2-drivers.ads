----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with System;
with Interfaces.C;
use Interfaces.C;

package M2.Drivers is
   pragma Pure;

   type File_Access_Mode is new int;

   type Open_Function_Ac is access procedure
     (Mode : File_Access_Mode);

   type Read_Function_Ac is access function
     (Buffer_Ptr : System.Address;
      Bytes      : size_t)
     return int;

   type Write_Function_Ac is access function
     (Buffer_Ptr : System.Address;
      Bytes      : size_t)
     return int;

   type Driver_Operations is record
      Open_Ac : Open_Function_Ac;
      Read_Ac : Read_Function_Ac;
      Write_Ac : Write_Function_Ac;
   end record;

end M2.Drivers;
