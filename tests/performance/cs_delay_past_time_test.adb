--  Measure the context switch when using a delay until with a
--  time in the past.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

pragma Warnings (Off);
with System.OS_Interface;
pragma Warnings (On);

with Ada.Real_Time;
with M2.Direct_IO;

with CS_Delay_Past_Time_2;

with Measurements;
with M2.HAL;

procedure CS_Delay_Past_Time_Test  is
   package DIO renames M2.Direct_IO;
   package S2 renames CS_Delay_Past_Time_2;

   Loop_Counter : Integer := 0;

begin
   DIO.Put ("-CS_Delay_Past_Time_Test-");

   Measurements.Init (Measurements.Long);
   Measurements.Start_Measurement;
   loop
      M2.HAL.Enable_Interrupts;
      --DIO.Put ("-Main-");

      if Loop_Counter = S2.Num_Of_Loops then
         Measurements.End_Measurement;

         DIO.Put ("L1=");
         DIO.Put (Loop_Counter);
         DIO.Put (" L2=");
         DIO.Put (S2.Loop_Counter);
         Measurements.Assert_Equal (S2.Num_Of_Loops, S2.Loop_Counter, 1);

         DIO.Put (" CSTime(us)=");
         DIO.Put (Integer (Measurements.Measurement_Time) * 100 /
                    (Loop_Counter + S2.Loop_Counter) * 10);
         Measurements.Finish;
      end if;

      Loop_Counter := Loop_Counter + 1;

      delay until Ada.Real_Time.Time_First;
   end loop;

end CS_Delay_Past_Time_Test;
