----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of using Ultrasonic Distance Sensor HC-SR04
--  See distance_sensor_hc_sr04_demo.ads for connection details.

package body Distance_Sensor_HC_SR04_Demo.ISR is

   ---------------------
   -- ISR_Digital_Pin --
   ---------------------

   procedure ISR_Digital_Pin is
   begin
      Falling_Edge_Echo_Time_Us := Arduino.Micros;
      ISR_Executed := True;
   end ISR_Digital_Pin;

end Distance_Sensor_HC_SR04_Demo.ISR;
