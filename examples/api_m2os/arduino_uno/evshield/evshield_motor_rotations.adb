----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on motor_rotations.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- setup for this example:
-- attach external power to EVShield.
-- attach 4 motors to motor ports
----------------------------------------------------------------------------

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Arduino.EVShield.Motors; use Arduino.EVShield.Motors;
with SHDefines_h; use SHDefines_h;
with Interfaces.C.Extensions;
with stdint_h;

procedure evshield_motor_rotations is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, SHDefines_h.SH_Protocols, Interfaces.C.Extensions.bool;

   -- Program variables
   time : Arduino.Time_MS;
   aux : Interfaces.C.Extensions.bool;
   output : stdint_h.uint8_t;
   rotations : Interfaces.C.long := 1;

   -- Bank A Motor 1
   package BAM1 is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM1);
   -- Bank A Motor 2
   package BAM2 is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM2);
   -- Bank A Motor 2
   package BBM1 is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BBM1);
   -- Bank A Motor 2
   package BBM2 is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BBM2);

begin
   -- initialize the shield i2c interface.
   Arduino.EVShield.init;

   DIO.Put_Line ("-----------------------------------");
   DIO.Put_Line ("Starting EVShield motor rotations program.");
   DIO.Put_Line ("-----------------------------------");

   -- Wait until user presses GO button to continue the program
   while not Arduino.EVShield.Get_Button_State(SHDefines_h.BTN_GO) loop
      time := Arduino.Millis;
      if time mod 1000 < 3 then
         DIO.Put_Line ("Press GO button to continue");
      end if;
   end loop;

   --
   -- reset motors.
   --
   aux := BAM1.Motor_Reset;
   aux := BBM1.Motor_Reset;

   -- Drive motor 1 forward and backward for a specific number of rotations
   Arduino.C_Delay (1000);
   DIO.Put_Line ("Bank A motors>>");
   DIO.Put("Motor 1 Forward "); DIO.Put(Integer(rotations)); DIO.Put(" Rotations"); DIO.New_Line;
   output := BAM1.Motor_Run_Rotations(BAM1.SH_Direction_Forward,
                                    BAM1.SH_Speed_Medium,
                                    rotations,
                                    BAM1.SH_Completion_Wait_For,
                                    BAM1.SH_Next_Action_BrakeHold);
   Arduino.C_Delay (1000);
   DIO.Put("Motor 1 Reverse "); DIO.Put(Integer(rotations)); DIO.Put(" Rotations"); DIO.New_Line;
   output := BAM1.Motor_Run_Rotations(BAM1.SH_Direction_Reverse,
                                    BAM1.SH_Speed_Medium,
                                    rotations,
                                    BAM1.SH_Completion_Wait_For,
                                    BAM1.SH_Next_Action_BrakeHold);

   -- Drive motor 2 forward and backward for a specific number of rotations
   Arduino.C_Delay (1000);
   DIO.Put("Motor 2 Forward "); DIO.Put(Integer(rotations)); DIO.Put(" Rotations"); DIO.New_Line;
   output := BAM2.Motor_Run_Rotations(BAM2.SH_Direction_Forward,
                                    BAM2.SH_Speed_Medium,
                                    rotations,
                                    BAM2.SH_Completion_Wait_For,
                                    BAM2.SH_Next_Action_BrakeHold);

   Arduino.C_Delay (1000);
   DIO.Put("Motor 2 Reverse "); DIO.Put(Integer(rotations)); DIO.Put(" Rotations"); DIO.New_Line;
   output := BAM2.Motor_Run_Rotations(BAM2.SH_Direction_Reverse,
                                    BAM2.SH_Speed_Medium,
                                    rotations,
                                    BAM2.SH_Completion_Wait_For,
                                    BAM2.SH_Next_Action_BrakeHold);

   DIO.Put_Line ("Bank B motors>>");
   DIO.Put("Motor 1 Forward "); DIO.Put(Integer(rotations)); DIO.Put(" Rotations"); DIO.New_Line;
   output := BBM1.Motor_Run_Rotations(BBM1.SH_Direction_Forward,
                                    BBM1.SH_Speed_Medium,
                                    rotations,
                                    BBM1.SH_Completion_Wait_For,
                                    BBM1.SH_Next_Action_BrakeHold);
   Arduino.C_Delay (1000);
   DIO.Put("Motor 1 Reverse "); DIO.Put(Integer(rotations)); DIO.Put(" Rotations"); DIO.New_Line;
   output := BBM1.Motor_Run_Rotations(BBM1.SH_Direction_Reverse,
                                    BBM1.SH_Speed_Medium,
                                    rotations,
                                    BBM1.SH_Completion_Wait_For,
                                    BBM1.SH_Next_Action_BrakeHold);

   DIO.Put("Motor 2 Forward "); DIO.Put(Integer(rotations)); DIO.Put(" Rotations"); DIO.New_Line;
   output := BBM2.Motor_Run_Rotations(BBM2.SH_Direction_Forward,
                                    BBM2.SH_Speed_Medium,
                                    rotations,
                                    BBM2.SH_Completion_Wait_For,
                                    BBM2.SH_Next_Action_BrakeHold);
   Arduino.C_Delay (1000);
   DIO.Put("Motor 2 Reverse "); DIO.Put(Integer(rotations)); DIO.Put(" Rotations"); DIO.New_Line;
   output := BBM2.Motor_Run_Rotations(BBM2.SH_Direction_Reverse,
                                    BBM2.SH_Speed_Medium,
                                    rotations,
                                    BBM2.SH_Completion_Wait_For,
                                    BBM2.SH_Next_Action_BrakeHold);

   -- wait for some time
   Arduino.C_Delay (1000);

end evshield_motor_rotations;
