pragma Warnings (Off);
with M2.Kernel.Scheduler;
pragma Elaborate_All (M2.Kernel.Scheduler);
pragma Warnings (On);

package CS_Delay_Past_Time_2 is
   pragma Elaborate_Body;

   Task_2_Prio : constant := 5;

   Num_Of_Loops : constant := 300;

   Loop_Counter : Integer := 0;

end CS_Delay_Past_Time_2;
