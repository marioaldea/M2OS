----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces;

with M2.Kernel.Scheduler;
with M2.HAL;
with M2.Kernel.Ready_Queue;

with M2.Direct_IO;

package body M2.Kernel.API is

   use type HAL.HWTime;

   ------------------------------------
   -- Set_Job_Body_Of_Running_Thread --
   ------------------------------------

   procedure Set_Job_Body_Of_Running_Thread (Body_Ac : HAL.Code_Address) is
   begin
      TCBs.Set_Job_Body (Ready_Queue.Running_Thread.all, Body_Ac);
   end Set_Job_Body_Of_Running_Thread;

   --------------
   -- Get_Time --
   --------------

   function Get_Time return Time is
   begin
      return HAL.Get_HWTime;
   end Get_Time;

   ---------
   -- Eat --
   ---------

   procedure Eat (Eat_Time : Duration) is
      Time_End : constant HAL.HWTime :=
        HAL.HWTime (Eat_Time * HAL.HWTime_Hz) + HAL.Get_HWTime;
   begin
      while Time_End > HAL.Get_HWTime loop
         null;
      end loop;
   end Eat;

   -----------------------
   -- Global_Stack_Base --
   -----------------------

   function Global_Stack_Base return Stack_Address is
   begin
      return HAL.Global_Stack_Base;
   end Global_Stack_Base;

   -----------------------
   -- Current_Stack_Top --
   -----------------------

   function Current_Stack_Top return Stack_Address is
   begin
      return HAL.Current_Stack_Top;
   end Current_Stack_Top;

end M2.Kernel.API;
