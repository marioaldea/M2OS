----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Moves a group of coordinated servos following a trayectory.
--  Uses a Timing_Event.

with Interfaces;
with Ada.Real_Time;

with Arduino.Servo;

generic
   Num_Of_Servos : Interfaces.Unsigned_8;
package Arduino.Coordinated_Servos is

   type Servo_Id is new Interfaces.Unsigned_8 range 1 .. Num_Of_Servos;

   type Servo_Pins is array (Servo_Id) of Arduino.PWM_Digital_Pin;

   type Position is array (Servo_Id) of Arduino.Servo.Angle_Deg;

   type Trayectory is array (Interfaces.Unsigned_8 range <>) of Position;

   procedure Attach (Pins : Servo_Pins);

   procedure Follow_Trayectory (Tray_Ac : access Trayectory;
                                Cycle_Time : Ada.Real_Time.Time_Span;
                                Repeat : Boolean);

   procedure Move_To (Pos : Position);

   procedure Stop;

--     procedure End_Control;

end Arduino.Coordinated_Servos;
