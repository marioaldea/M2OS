----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of motors functions of EVSHield.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with EVShieldI2C_h;
with stdint_h;
with Interfaces.C.Extensions;
with SHDefines_h;
with Interfaces.C.Strings;
with Arduino;
with Arduino.EVShield;
with EVShield_h;

package Arduino.EVShield.Motors is
   -- Bank A motors
   SH_BAM1 : constant SH_BankPort := 5;
   SH_BAM2 : constant SH_BankPort := 6;
   -- Bank B motors
   SH_BBM1 : constant SH_BankPort := 7;
   SH_BBM2 : constant SH_BankPort := 8;

   generic
      Bp : Arduino.EVShield.SH_BankPort;
      --        bank : EVShield_h.Class_EVShieldBank.EVShieldBank;
      --        which_motor : SH_Motor;
   package Motor is
      -- constants to be used by user programs
      -- Next action related constants
      type SH_Next_Action is new EVShield_h.SH_Next_Action;
      --  (SH_Next_Action_Float, -- stop and let the motor coast.
      --   SH_Next_Action_Brake, -- apply brakes, and resist change to tachometer, but if tach position is forcibly changed, do not restore position
      --   SH_Next_Action_BrakeHold); -- apply brakes, and restore externally forced change to tachometer

      -- SH_Direction Motor direction related constants.
      type SH_Direction is new EVShield_h.SH_Direction;
      -- (SH_Direction_Reverse, -- Run motor in reverse direction
      -- SH_Direction_Forward); -- Run motor in forward direction

      -- Tachometer movement related constants
      type SH_Move is new EVShield_h.SH_Move;
      --  (SH_Move_Absolute, -- Move the tach to absolute value provided
      --   SH_Move_Relative); -- Move the tach relative to previous position

      -- Whether to wait for motor to finish it's current task or not
      type SH_Completion_Wait is new EVShield_h.SH_Completion_Wait;
      --  (SH_Completion_Dont_Wait, -- Don't wait for motor to finish, program will continue with next function
      --   SH_Completion_Wait_For); -- Wait for motor to finish, program will wait until current function finishes it's operation

      -- Speed constants, these are just convenience constants,
      -- You can use any value between 0 and 100.
      SH_Speed_Full : constant := 90;
      SH_Speed_Medium : constant := 60;
      SH_Speed_Slow : constant := 25;

      --  Reset all the set values for the motors
      --  Applies to all motors on this bank.
      function Motor_Reset return Extensions.bool;

      --  Get the current encoder position of the motor in degrees
      --    @param which_motor    Provide which motor to operate on
      --    @return              current encoder value
      function Motor_Get_Encoder_Position return stdint_h.int32_t;

      --  Run the motor for a set number of complete rotations and proceed to the next action
      --   @param which_motors     specifiy the motor(s) to operate on
      --   @param direction        specifiy the direction to run the motor
      --   @param speed            the speed value (between 0 and 100)
      --   @param rotations        The rotations the motor should rotate through
      --   @param wait_for_completion    whether this API should wait for completion or not
      --   @param next_action      for these motor being operated on
      --   @return        0 if the operation was finished satisfactorily,
      --            in case return value is non-zero you should check for the bits for error conditions.
      function Motor_Run_Rotations
        (Direction : SH_Direction;
         Speed : int;
         Rotations : long;
         Wait_For_Completion : SH_Completion_Wait;
         next_action : SH_Next_Action) return stdint_h.uint8_t;

      -- Run the motor endlessly at the desired speed in the desired direction
      -- @param which_motors     specifiy the motor(s) to operate on
      -- @param direction        specifiy the direction to run the motor
      -- @param speed            the speed value (between 0 and 100)
      -- @return  Starts the motors and function returns immediately
      procedure Motor_Run_Unlimited
        (Direction : SH_Direction;
         Speed : int);

      -- stop the motor and do the next action
      -- @param which_motors     specifiy the motor(s) to operate on
      -- @param next_action      for these motor being operated on
      function Motor_Stop
        (Next_Action : SH_Next_Action) return Extensions.bool;

      --     -- Methods for EVShield Bank A
      --     package Bank_A is
      --
      --        --  Reset all the set values for the motors
      --        --  Applies to all motors on this bank.
      --        function motorReset return Extensions.bool;
      --
      --        --  Get the current encoder position of the motor in degrees
      --        --    @param which_motor    Provide which motor to operate on
      --        --    @return              current encoder value
      --        function motorGetEncoderPosition
      --          (which_motor : SH_Motor) return stdint_h.int32_t;
      --
      --        --  Run the motor for a set number of complete rotations and proceed to the next action
      --        --   @param which_motors     specifiy the motor(s) to operate on
      --        --   @param direction        specifiy the direction to run the motor
      --        --   @param speed            the speed value (between 0 and 100)
      --        --   @param rotations        The rotations the motor should rotate through
      --        --   @param wait_for_completion    whether this API should wait for completion or not
      --        --   @param next_action      for these motor being operated on
      --        --   @return        0 if the operation was finished satisfactorily,
      --        --            in case return value is non-zero you should check for the bits for error conditions.
      --        function motorRunRotations
      --          (which_motors : SH_Motor;
      --           direction : SH_Direction;
      --           speed : int;
      --           rotations : long;
      --           wait_for_completion : SH_Completion_Wait;
      --           next_action : SH_Next_Action) return stdint_h.uint8_t;
      --
      --        -- Run the motor endlessly at the desired speed in the desired direction
      --        -- @param which_motors     specifiy the motor(s) to operate on
      --        -- @param direction        specifiy the direction to run the motor
      --        -- @param speed            the speed value (between 0 and 100)
      --        -- @return  Starts the motors and function returns immediately
      --        procedure motorRunUnlimited
      --          (which_motors : SH_Motor;
      --           direction : SH_Direction;
      --           speed : int);
      --
      --        -- stop the motor and do the next action
      --        -- @param which_motors     specifiy the motor(s) to operate on
      --        -- @param next_action      for these motor being operated on
      --        function motorStop
      --          (which_motors : SH_Motor;
      --           next_action : SH_Next_Action) return Extensions.bool;
      --     end Bank_A;
      --
      --     -- Methods for EVShield Bank b
      --     package Bank_B is
      --
      --        --  Reset all the set values for the motors
      --        --  Applies to all motors on this bank.
      --        function motorReset return Extensions.bool;
      --
      --        --  Get the current encoder position of the motor in degrees
      --        --    @param which_motor    Provide which motor to operate on
      --        --    @return              current encoder value
      --        function motorGetEncoderPosition
      --          (which_motor : SH_Motor) return stdint_h.int32_t;
      --
      --        --  Run the motor for a set number of complete rotations and proceed to the next action
      --        --   @param which_motors     specifiy the motor(s) to operate on
      --        --   @param direction        specifiy the direction to run the motor
      --        --   @param speed            the speed value (between 0 and 100)
      --        --   @param rotations        The rotations the motor should rotate through
      --        --   @param wait_for_completion    whether this API should wait for completion or not
      --        --   @param next_action      for these motor being operated on
      --        --   @return        0 if the operation was finished satisfactorily,
      --        --            in case return value is non-zero you should check for the bits for error conditions.
      --        function motorRunRotations
      --          (which_motors : SH_Motor;
      --           direction : SH_Direction;
      --           speed : int;
      --           rotations : long;
      --           wait_for_completion : SH_Completion_Wait;
      --           next_action : SH_Next_Action) return stdint_h.uint8_t;
      --
      --        -- Run the motor endlessly at the desired speed in the desired direction
      --        -- @param which_motors     specifiy the motor(s) to operate on
      --        -- @param direction        specifiy the direction to run the motor
      --        -- @param speed            the speed value (between 0 and 100)
      --        -- @return  Starts the motors and function returns immediately
      --        procedure motorRunUnlimited
      --          (which_motors : SH_Motor;
      --           direction : SH_Direction;
      --           speed : int);
      --
      --        -- stop the motor and do the next action
      --        -- @param which_motors     specifiy the motor(s) to operate on
      --        -- @param next_action      for these motor being operated on
      --        function motorStop
      --          (which_motors : SH_Motor;
      --           next_action : SH_Next_Action) return Extensions.bool;
      --
      --        end Bank_B;


   end Motor;

end Arduino.EVShield.Motors;
