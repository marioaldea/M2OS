//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Simple example of use of the M2OS POSIX-like layer.
// Two periodic threads.
// TODO: implement argument passing to avoid having to duplicate threads body.

//#define M2_USE_POSIX_TIMESPEC // uncoment to use POSIX timespec (less efficient)
#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include "../reports/tests_reports.h"

THREAD_POOL(2);

#define NUM_OF_LOOPS 12
#define PERIOD1_NS 200000000
#define PERIOD2_NS 600000000
const struct timespec period1 = TS(0, PERIOD1_NS);
const struct timespec period2 = TS(0, PERIOD2_NS);
#define PERIOD1 200
#define PERIOD2 600
int counter1 = 0;
int counter2 = 0;

//****************//
//  thread_body1  //
//****************//
struct timespec next_activation_time1;

void * thread_body1(void *arg) {
  puts("Thread 1\n");
  counter1++;
  
  if (counter1 == NUM_OF_LOOPS) {
    tests_reports__assert_equal_c(counter2 * (PERIOD2_NS / PERIOD1_NS),
				  counter1, 1);
    tests_reports__test_ok();
  }
    
  TS_INC(next_activation_time1, period1);
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time1, NULL);
  
  return NULL;
}

//****************//
//  thread_body2  //
//****************//
struct timespec next_activation_time2 = TS(0, 0);

void * thread_body2(void *arg) {
  puts("Thread 2\n");
  counter2++;

  TS_INC(next_activation_time2, period2);
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time2, NULL);
  
  return NULL;
}  
    
//**********//
//   main   //
//**********//
int main (int argc, char **argv) {
  m2osinit();
  puts("Main\n");
  check_posix_api();
    
  pthread_t th1;
  pthread_t th2;
  pthread_attr_t attr;
  struct sched_param sch_param = {.sched_priority = 2};
  
  clock_gettime(CLOCK_MONOTONIC, &next_activation_time1);
  next_activation_time2 = next_activation_time1;

  pthread_attr_init(&attr);
  pthread_attr_setschedparam(&attr, &sch_param);
    
  pthread_create(&th1, &attr, thread_body1, NULL);    
  pthread_create(&th2, &attr, thread_body2, NULL);

  puts("Main ends\n");

  return 0;
}
