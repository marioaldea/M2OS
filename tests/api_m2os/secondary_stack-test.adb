----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--  Test secondary stack and tasks

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy (Ceiling_Locking);
pragma Queuing_Policy (Priority_Queuing);

with M2.Direct_IO;
with Tests_Reports;

with Secondary_Stack.Task_1;
with Secondary_Stack.Task_2;

procedure Secondary_Stack.Test is
   --pragma Secondary_Stack_Size (256);

   package DIO renames M2.Direct_IO;

   function Ret_String (Length : Positive) return String is
      Str : String (1 .. Length) := (others => 'a');
   begin
      return Str;
   end Ret_String;

begin
   DIO.Put_Line ("Secondary_Stack_Test");

   DIO.Put_Line ("String: " & Ret_String (8));
end Secondary_Stack.Test;
