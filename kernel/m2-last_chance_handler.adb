----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces.C;

with M2.Direct_IO;
with M2.End_Execution;

package body M2.Last_Chance_Handler is

   use type Interfaces.C.Int;

   -------------------------
   -- Last_Chance_Handler --
   -------------------------

   procedure Last_Chance_Handler
     (Source_Location : System.Address;
      Line : Integer) is
   begin
      Direct_IO.Put ("Except. at ");
      Direct_IO.Put_Null_Terminated (Source_Location);
      Direct_IO.Put (":");
      Direct_IO.Put (Line);
      Direct_IO.New_Line;

      End_Execution.End_Execution (-1);
   end Last_Chance_Handler;

end M2.Last_Chance_Handler;
