----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of the CCS811 library for the
--  "KS0457 keyestudio CCS811 Carbon Dioxide Air Quality Sensor".
--  https://wiki.keyestudio.com/KS0457_keyestudio_CCS811_Carbon_Dioxide_Air_Quality_Sensor
----------------------------------------------------------------------------
package body Arduino.CCS811 is

   type CCS811_Filling is
     array(1 .. 2) of Interfaces.Unsigned_8;

   ------------
   -- CCS811 --
   ------------

   type CCS811 is limited record
      Device_Addr : Wire.Dev_Address;
      Filling: CCS811_Filling;
      Wire_Ac : access Arduino.Wire.Two_Wire;
      ECO2 : Interfaces.Unsigned_16;
      ETVOC : Interfaces.Unsigned_16;
   end record;
--     pragma Import (CPP, CCS811);
   for CCS811'Size use CCS811_Size_In_Bytes * 8;
   for CCS811'Alignment use Standard'Maximum_Alignment;

   Sensor : aliased CCS811;
   pragma Import (CPP, Sensor, "CCS811_wrapper__sensor");
   --  Defined in 'CCS811_wrapper.cpp'

   ------------------------
   -- Imported functions --
   ------------------------

   function Begin_CCS811_Cpp (This : access CCS811) return Integer;
   pragma Import (CPP, Begin_CCS811_Cpp, "_ZN6CCS8115beginEv");
   -- Return 0 if initialization succeeds, otherwise return non-zero.

   procedure Set_Meas_Cycle_Cpp (This : access CCS811;
                                 Cycle : ECycle_T);
   pragma Import (CPP, Set_Meas_Cycle_Cpp,
                  "_ZN6CCS81112setMeasCycleENS_8eCycle_tE");


   function Get_CO2_PPM_Cpp (This : access CCS811) return CO2_PPM_T;
   pragma Import (CPP, Get_CO2_PPM_Cpp, "_ZN6CCS8119getCO2PPMEv");
   -- return current carbon dioxide concentration, unit:ppm

   function Get_TVOC_PPB_Cpp (This : access CCS811) return TVOC_PPB_T;
   pragma Import (CPP, Get_TVOC_PPB_Cpp, "_ZN6CCS81110getTVOCPPBEv");
   --  @return Return current TVOC concentration, unit: ppb

   procedure Write_Base_Line_Cpp (This : access CCS811;
                                  Base_Line : Base_Line_T);
   pragma Import (CPP, Write_Base_Line_Cpp, "_ZN6CCS81113writeBaseLineEj");


   function Check_Data_Ready_Cpp (This : access CCS811) return Data_Ready_T;
   pragma Import (CPP, Check_Data_Ready_Cpp, "_ZN6CCS81114checkDataReadyEv");

   procedure Set_In_Temp_Hum_Cpp (This : access CCS811;
                                  Temp : Float; Hum : Float);
   pragma Import (CPP, Set_In_Temp_Hum_Cpp, "_ZN6CCS81112setInTempHumEff");
   --  Set environment parameter
   --    Temp Set temperature value, unit: centigrade, range (-40~85)
   --    Hum  Set humidity value, unit: RH, range (0~100)

   ------------------
   -- Begin_CCS811 --
   ------------------

   function Begin_CCS811 return Integer is
      (Begin_CCS811_Cpp (Sensor'Access));

   --------------------
   -- Set_Meas_Cycle --
   --------------------

   procedure Set_Meas_Cycle (Cycle : ECycle_T) is
   begin
      Set_Meas_Cycle_Cpp (Sensor'Access, Cycle);
   end Set_Meas_Cycle;

   -----------------
   -- Get_CO2_PPM --
   -----------------

   function Get_CO2_PPM return CO2_PPM_T is
      (Get_CO2_PPM_Cpp (Sensor'Access));

   ------------------
   -- Get_TVOC_PPB --
   ------------------

   function Get_TVOC_PPB return TVOC_PPB_T is
      (Get_TVOC_PPB_Cpp (Sensor'Access));

   ---------------------
   -- Write_Base_Line --
   ---------------------

   procedure Write_Base_Line (Base_Line : Base_Line_T) is
   begin
      Write_Base_Line_Cpp (Sensor'Access, Base_Line);
   end Write_Base_Line;

   ----------------------
   -- Check_Data_Ready --
   ----------------------

   function Check_Data_Ready return Data_Ready_T is
     (Check_Data_Ready_Cpp (Sensor'Access));

   ---------------------
   -- Set_In_Temp_Hum --
   ---------------------

   procedure Set_In_Temp_Hum (Temp : Float; Hum : Float) is
   begin
      Set_In_Temp_Hum_Cpp (Sensor'Access, Temp, Hum);
   end Set_In_Temp_Hum;

end Arduino.CCS811;
