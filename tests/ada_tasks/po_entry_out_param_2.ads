----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
package PO_Entry_Out_Param_2 is

   Step_Counter : Natural := 0;
   Num_Of_Loops : constant := 3;
   PO_Data : constant := 2;

   protected PO is
      entry Wait (Data_In : Integer; Data_Out : out Integer);

      procedure Open (Data : Integer);

   private
      Is_Open : Boolean := False;
      Data : Integer := PO_Data;
   end PO;

   task Other_Task;

end PO_Entry_Out_Param_2;
