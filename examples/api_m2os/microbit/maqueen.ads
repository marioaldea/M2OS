with MicroBit.Music; use Microbit.Music;
with HAL;

package Maqueen is
   
   type Light is (Left_Light, Right_Light);
   
   type Motor is (Left_Motor, Right_Motor);
   
   type Direction is (Forwards, Backwards);
   
   -- HAL.UInt8
   
   type Speed is range 0 .. 250;
   
   procedure Turn_On_Light (FrontLight : Light);
   --  Turn on Light
   
   procedure Turn_Off_Light (FrontLight : Light);
   --  Turn off one Light

   procedure Play_Sound (Snd : Melody);
   
   procedure Initialize;
   
   procedure Forward;
   
   procedure Backward;
   
   procedure SetSpeed(Spd : Speed);
   
   procedure Spin_Left;
   
   procedure Spin_Right;
   
   procedure Turn_Left;
   
   procedure Turn_Right;
   
   procedure Stop;
   
   procedure Motor_Run(MotorToMove : Motor; WhichDirection : Direction; Spd : Speed);
   
   function Is_Running return Boolean;
   
   function Read_Patrol_Right return Boolean;
   
   function Read_Patrol_Left return Boolean;
   
   function Left_Light_Status return Boolean;
   
   function Right_Light_Status return Boolean;
end Maqueen;
