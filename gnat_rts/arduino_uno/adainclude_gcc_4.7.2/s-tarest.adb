--  M2OS version of this package (for GCC 4.7.2, GNAT GPL 2016/GCC 4.9.4
--  and GCC 5.2.0)

with GNAT.IO;
with System.OS_Interface;
with Ada.Unchecked_Conversion;

package body System.Tasking.Restricted.Stages is

   -----------------------
   -- Restricted GNARLI --
   -----------------------

   -----------------------------------
   -- Activate_All_Tasks_Sequential --
   -----------------------------------

   procedure Activate_All_Tasks_Sequential is
   begin
      raise Program_Error;
   end Activate_All_Tasks_Sequential;

   -------------------------------
   -- Activate_Restricted_Tasks --
   -------------------------------

   procedure Activate_Restricted_Tasks
     (Chain_Access : Activation_Chain_Access) is
      pragma Unreferenced (Chain_Access);
   begin
      null;
      --  GNAT.IO.Put_Line (" Activate_Restricted_Tasks ");
   end Activate_Restricted_Tasks;

   ------------------------------------
   -- Complete_Restricted_Activation --
   ------------------------------------

   procedure Complete_Restricted_Activation is
   begin
      --  Nothing to be done

      null;
   end Complete_Restricted_Activation;

   ------------------------------
   -- Complete_Restricted_Task --
   ------------------------------

   procedure Complete_Restricted_Task is
   begin
      raise Program_Error;
   end Complete_Restricted_Task;

   ----------------------------
   -- Create_Restricted_Task --
   ----------------------------

   procedure Create_Restricted_Task
     (Priority      : Integer;
      Stack_Address : System.Address;
      Size          : System.Parameters.Size_Type;
      Task_Info     : System.Task_Info.Task_Info_Type;
      CPU           : Integer;
      State         : Task_Procedure_Access;
      Discriminants : System.Address;
      Elaborated    : Access_Boolean;
      Chain         : in out Activation_Chain;
      Task_Image    : String;
      Created_Task  : Task_Id)
   is
      pragma Unreferenced (Size, Task_Info, CPU,
                           Elaborated, Chain, Task_Image, Stack_Address);
      function To_Thread_Body_Ac is
        new Ada.Unchecked_Conversion (Task_Procedure_Access,
                                      OS_Interface.Thread_Body_Ac);

      Base_Priority        : System.Any_Priority;
      Th_Ac : OS_Interface.Thread_Ac;
   begin
      Base_Priority :=
        (if Priority = Unspecified_Priority
         then System.Default_Priority
         else System.Any_Priority (Priority));

      OS_Interface.Create_Thread (Th_Ac   => Th_Ac,
                                  Th_Body => To_Thread_Body_Ac (State),
                                  Prio    => Base_Priority,
                                  Args    => Discriminants);

      Initialize_ATCB (Th_Ac, Created_Task);

   end Create_Restricted_Task;

   ---------------------------------------
   -- Create_Restricted_Task_Sequential --
   ---------------------------------------

   procedure Create_Restricted_Task_Sequential
     (Priority      : Integer;
      Stack_Address : System.Address;
      Size          : System.Parameters.Size_Type;
      Task_Info     : System.Task_Info.Task_Info_Type;
      CPU           : Integer;
      State         : Task_Procedure_Access;
      Discriminants : System.Address;
      Elaborated    : Access_Boolean;
      Task_Image    : String;
      Created_Task  : Task_Id)
   is
      pragma Unreferenced (Priority, Stack_Address, Size, Task_Info,
                           CPU, State, Discriminants, Elaborated, Task_Image,
                           Created_Task);

   begin
      --  GNAT.IO.Put_Line (" Create_Restricted_Task_Sequential ");
      raise Program_Error;
   end Create_Restricted_Task_Sequential;

   ---------------------------
   -- Finalize_Global_Tasks --
   ---------------------------

   --  Dummy version since this procedure is not used in true ravenscar mode

   procedure Finalize_Global_Tasks is
   begin
      raise Program_Error;
   end Finalize_Global_Tasks;

   ---------------------------
   -- Restricted_Terminated --
   ---------------------------

   function Restricted_Terminated (T : Task_Id) return Boolean is
   begin
      raise Program_Error;
      return False;
   end Restricted_Terminated;

begin
   Tasking.Initialize;
end System.Tasking.Restricted.Stages;
