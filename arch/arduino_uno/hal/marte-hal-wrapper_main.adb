with Interfaces.C;
use Interfaces.C;
with System;

with M2.Kernel.Initialization;
with M2.Kernel.Ready_Queue;
with M2.Debug;

package body M2.HAL.Wrapper_Main is

   --  Main user's function

   function Main (Argc : int;
                  Argv : System.Address;
                  Envp : System.Address)
                  return int;
   pragma Import (C, Main, "main");

   ------------------
   -- Wrapper_Main --
   ------------------

   procedure Wrapper_Main is
      Ret : int;
   begin
      pragma Compile_Time_Warning (True, "Wrapper_Main not used");
      Kernel.Initialization.Initialization;

      Ret := Main (0, System.Null_Address, System.Null_Address);
   end Wrapper_Main;

end M2.HAL.Wrapper_Main;
