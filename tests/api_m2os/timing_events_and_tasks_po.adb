----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with M2.Direct_IO;
with M2.HAL;
with Tests_Reports;

package body Timing_Events_And_Tasks_PO is
   package DIO renames M2.Direct_IO;

   use type Ada.Real_Time.Time;

   protected body PO is

      ------------
      -- Start1 --
      ------------

      procedure Start1 (First_Activation : RT.Time;
                        Period : RT.Time_Span) is
      begin
         Activation_Time_1 := First_Activation;
         Period_1 := Period;

         TE.Set_Handler (Event   => Event1,
                         At_Time => First_Activation,
                         Handler => PO_Handler1'Access);
      end Start1;

      -----------------
      -- PO_Handler1 --
      -----------------

      procedure PO_Handler1 (Event : in out TE.Timing_Event) is
      begin
         DIO.Put_Line ("PO_Handler1" & Step'Img);
         Tests_Reports.Assert (not M2.HAL.Are_Interrupts_Enabled);
         Tests_Reports.Assert (Step = 5 or Step = 9);
         Step := Step + 1;

         Ada.Synchronous_Task_Control.Set_True (Susp_Object);

         Activation_Time_1 := Activation_Time_1 + Period_1;
         TE.Set_Handler (Event,
                         Activation_Time_1,
                         PO_Handler1'Access);
      end PO_Handler1;

      ------------
      -- Start1 --
      ------------

      procedure Start2 (First_Activation : RT.Time;
                        Period : RT.Time_Span) is
      begin
         Activation_Time_2 := First_Activation;
         Period_2 := Period;

         TE.Set_Handler (Event   => Event2,
                         At_Time => First_Activation,
                         Handler => PO_Handler2'Access);
      end Start2;

      -----------------
      -- PO_Handler2 --
      -----------------

      procedure PO_Handler2 (Event : in out TE.Timing_Event) is
      begin
         DIO.Put_Line ("PO_Handler2" & Step'Img);
         Tests_Reports.Assert (not M2.HAL.Are_Interrupts_Enabled);
         Tests_Reports.Assert (Step = 7 or Step = 10);
         Step := Step + 1;

         Open := True;

         Ada.Synchronous_Task_Control.Set_True (Susp_Object2);

         Activation_Time_2 := Activation_Time_2 + Period_2;
         TE.Set_Handler (Event,
                         Activation_Time_2,
                         PO_Handler2'Access);
      end PO_Handler2;

      -----------
      -- Wait2 --
      -----------

      entry Wait2 when Open is
      begin
         DIO.Put_Line ("Wait2" & Step'Img);
         Tests_Reports.Assert (not M2.HAL.Are_Interrupts_Enabled);
         Tests_Reports.Assert (Step = 3 or Step = 6 or Step = 7 or
                                 Step = 9 or Step = 11);
         Step := Step + 1;

         Open := False;
      end Wait2;

   end PO;

end Timing_Events_And_Tasks_PO;
