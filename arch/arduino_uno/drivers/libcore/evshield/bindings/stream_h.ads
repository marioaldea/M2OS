pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with stddef_h;
with Print_h;
with Interfaces.C.Extensions;
with stdint_h;
with WString_h;

package Stream_h is

   type LookaheadMode is 
     (SKIP_ALL,
      SKIP_NONE,
      SKIP_WHITESPACE);
   pragma Convention (C, LookaheadMode);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:41

   type Stream;
   type MultiTarget is record
      str : Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:118
      len : aliased stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:119
      index : aliased stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:120
   end record;
   pragma Convention (C_Pass_By_Copy, MultiTarget);
   package Class_Stream is
      type Stream is abstract limited new Print_h.Class_Print.Print with record
         u_timeout : aliased unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:52
         u_startMillis : aliased unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:53
      end record;
      pragma Import (CPP, Stream);

      function timedRead (this : access Stream'Class) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:54
      pragma Import (CPP, timedRead, "_ZN6Stream9timedReadEv");

      function timedPeek (this : access Stream'Class) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:55
      pragma Import (CPP, timedPeek, "_ZN6Stream9timedPeekEv");

      function peekNextDigit
        (this : access Stream'Class;
         lookahead : LookaheadMode;
         detectDecimal : Extensions.bool) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:56
      pragma Import (CPP, peekNextDigit, "_ZN6Stream13peekNextDigitE13LookaheadModeb");

      function available (this : access Stream) return int is abstract;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:59

      function read (this : access Stream) return int is abstract;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:60

      function peek (this : access Stream) return int is abstract;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:61

      function New_Stream return Stream is abstract;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:63
      pragma CPP_Constructor (New_Stream, "_ZN6StreamC1Ev");

      procedure setTimeout (this : access Stream'Class; timeout : unsigned_long);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:67
      pragma Import (CPP, setTimeout, "_ZN6Stream10setTimeoutEm");

      function getTimeout (this : access Stream'Class) return unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:68
      pragma Import (CPP, getTimeout, "_ZN6Stream10getTimeoutEv");

      function find (this : access Stream'Class; target : Interfaces.C.Strings.chars_ptr) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:70
      pragma Import (CPP, find, "_ZN6Stream4findEPc");

      function find (this : access Stream'Class; target : access stdint_h.uint8_t) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:71
      pragma Import (CPP, find, "_ZN6Stream4findEPh");

      function find
        (this : access Stream'Class;
         target : Interfaces.C.Strings.chars_ptr;
         length : stddef_h.size_t) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:74
      pragma Import (CPP, find, "_ZN6Stream4findEPcj");

      function find
        (this : access Stream'Class;
         target : access stdint_h.uint8_t;
         length : stddef_h.size_t) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:75
      pragma Import (CPP, find, "_ZN6Stream4findEPhj");

      function find (this : access Stream'Class; target : char) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:78
      pragma Import (CPP, find, "_ZN6Stream4findEc");

      function findUntil
        (this : access Stream'Class;
         target : Interfaces.C.Strings.chars_ptr;
         terminator : Interfaces.C.Strings.chars_ptr) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:80
      pragma Import (CPP, findUntil, "_ZN6Stream9findUntilEPcS0_");

      function findUntil
        (this : access Stream'Class;
         target : access stdint_h.uint8_t;
         terminator : Interfaces.C.Strings.chars_ptr) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:81
      pragma Import (CPP, findUntil, "_ZN6Stream9findUntilEPhPc");

      function findUntil
        (this : access Stream'Class;
         target : Interfaces.C.Strings.chars_ptr;
         targetLen : stddef_h.size_t;
         c_terminate : Interfaces.C.Strings.chars_ptr;
         termLen : stddef_h.size_t) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:83
      pragma Import (CPP, findUntil, "_ZN6Stream9findUntilEPcjS0_j");

      function findUntil
        (this : access Stream'Class;
         target : access stdint_h.uint8_t;
         targetLen : stddef_h.size_t;
         c_terminate : Interfaces.C.Strings.chars_ptr;
         termLen : stddef_h.size_t) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:84
      pragma Import (CPP, findUntil, "_ZN6Stream9findUntilEPhjPcj");

      function parseInt
        (this : access Stream'Class;
         lookahead : LookaheadMode;
         ignore : char) return long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:86
      pragma Import (CPP, parseInt, "_ZN6Stream8parseIntE13LookaheadModec");

      function parseFloat
        (this : access Stream'Class;
         lookahead : LookaheadMode;
         ignore : char) return float;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:93
      pragma Import (CPP, parseFloat, "_ZN6Stream10parseFloatE13LookaheadModec");

      function readBytes
        (this : access Stream'Class;
         buffer : Interfaces.C.Strings.chars_ptr;
         length : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:96
      pragma Import (CPP, readBytes, "_ZN6Stream9readBytesEPcj");

      function readBytes
        (this : access Stream'Class;
         buffer : access stdint_h.uint8_t;
         length : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:97
      pragma Import (CPP, readBytes, "_ZN6Stream9readBytesEPhj");

      function readBytesUntil
        (this : access Stream'Class;
         terminator : char;
         buffer : Interfaces.C.Strings.chars_ptr;
         length : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:101
      pragma Import (CPP, readBytesUntil, "_ZN6Stream14readBytesUntilEcPcj");

      function readBytesUntil
        (this : access Stream'Class;
         terminator : char;
         buffer : access stdint_h.uint8_t;
         length : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:102
      pragma Import (CPP, readBytesUntil, "_ZN6Stream14readBytesUntilEcPhj");

      function readString (this : access Stream'Class) return WString_h.Class_String.String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:107
      pragma Import (CPP, readString, "_ZN6Stream10readStringEv");

      function readStringUntil (this : access Stream'Class; terminator : char) return WString_h.Class_String.String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:108
      pragma Import (CPP, readStringUntil, "_ZN6Stream15readStringUntilEc");

      function parseInt (this : access Stream'Class; ignore : char) return long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:111
      pragma Import (CPP, parseInt, "_ZN6Stream8parseIntEc");

      function parseFloat (this : access Stream'Class; ignore : char) return float;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:112
      pragma Import (CPP, parseFloat, "_ZN6Stream10parseFloatEc");

      function findMulti
        (this : access Stream'Class;
         targets : access MultiTarget;
         tCount : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Stream.h:125
      pragma Import (CPP, findMulti, "_ZN6Stream9findMultiEPNS_11MultiTargetEi");
   end;
   use Class_Stream;
end Stream_h;
