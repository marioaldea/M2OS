----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------


with AdaX_Dispatching_Stack_Sharing;

with M2.Direct_IO;

with Tests_Reports;

package body PO_Entry_Closed_Access_Param_2 is
   package DIO renames M2.Direct_IO;

   --------
   -- PO --
   --------

   protected body PO is

      entry Wait (Data_Out : access Integer) when Is_Open is
      begin
         DIO.Put ("-PO.Wait-");
         DIO.Put (Counter);

         Data_Out.all := Counter;
         DIO.Put (Data_Out.all);
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         DIO.Put ("-PO.Open-");
         DIO.Put (Counter);

         Counter := Counter + 1;
         Is_Open := True;
      end Open;
   end PO;

   ---------------------
   -- Other_Task_Init --
   ---------------------

   procedure Other_Task_Init is
   begin
      DIO.Put ("-Other_Task_Init-");
   end Other_Task_Init;

   Loop_Counter : Integer := 0;
   Data_Out : aliased Integer := 9;

   ---------------------
   -- Other_Task_Body --
   ---------------------

   procedure Other_Task_Body is
   begin
      loop
         DIO.Put ("-Other-");
         DIO.Put (Integer (Step_Counter));
         DIO.Put (Loop_Counter);
         DIO.Put (Data_Out);

         if Loop_Counter /= 0 then
            Tests_Reports.Assert (Data_Out = Loop_Counter);
         end if;

         Tests_Reports.Assert (Step_Counter = Loop_Counter * 2 + 1);

         Step_Counter := Step_Counter + 1;
         Loop_Counter := Loop_Counter + 1;

         PO.Wait (Data_Out'Access);
      end loop;
   end Other_Task_Body;

   Main_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Other_Task_Init'Access,
      Body_Ac  => Other_Task_Body'Access,
      Priority => 4);

end PO_Entry_Closed_Access_Param_2;
