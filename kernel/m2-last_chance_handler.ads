----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with System;

--  Restriction No_Exception_Handlers ensures at compile time that there are no
--  explicit exception handlers. It also indicates that no exception propagation
--  will be provided. In this mode, exceptions may be raised but will result in
--  an immediate call to the last chance handler, a routine that the user must
--  define with the following profile:

package M2.Last_Chance_Handler is

   --  The parameter is a C null-terminated string representing a message to be
   --  associated with the exception (typically the source location of the raise
   --  statement generated by the compiler). The Line parameter when nonzero
   --  represents the line number in the source program where the raise occurs.

    procedure Last_Chance_Handler
     (Source_Location : System.Address;
      Line : Integer);
    pragma Export (C, Last_Chance_Handler,
                   "__gnat_last_chance_handler");

end M2.Last_Chance_Handler;
