pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with EVShieldUART_h;
limited with EVShield_h;
with SHDefines_h;
with Interfaces.C.Extensions;

package EVs_EV3Touch_h is

   package Class_EVs_EV3Touch is
      type EVs_EV3Touch is limited record
         parent : aliased EVShieldUART_h.Class_EVShieldUART.EVShieldUART;
      end record;
      pragma Import (CPP, EVs_EV3Touch);
           
      function New_EV3Touch return EVs_EV3Touch;  -- EVShieldUART.h:46
      pragma CPP_Constructor (New_EV3Touch, "_ZN12EVShieldUARTC1Ev");

      function init
        (this : access EVs_EV3Touch;
         shield : access EVShield_h.Class_EVShield.EVShield;
         bp : SHDefines_h.SH_BankPort) return Extensions.bool;  -- EVs_EV3Touch.h:34
      pragma Import (CPP, init, "_ZN12EVs_EV3Touch4initEP8EVShield11SH_BankPort");

      function isPressed (this : access EVs_EV3Touch) return Extensions.bool;  -- EVs_EV3Touch.h:37
      pragma Import (CPP, isPressed, "_ZN12EVs_EV3Touch9isPressedEv");

      function getBumpCount (this : access EVs_EV3Touch) return int;  -- EVs_EV3Touch.h:49
      pragma Import (CPP, getBumpCount, "_ZN12EVs_EV3Touch12getBumpCountEv");

      function resetBumpCount (this : access EVs_EV3Touch) return Extensions.bool;  -- EVs_EV3Touch.h:52
      pragma Import (CPP, resetBumpCount, "_ZN12EVs_EV3Touch14resetBumpCountEv");
   end;
   use Class_EVs_EV3Touch;
end EVs_EV3Touch_h;
