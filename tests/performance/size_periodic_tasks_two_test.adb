--  Measure the context switch when using a delay until with a
--  time in the past.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

pragma Warnings (Off);
with System.OS_Interface;
pragma Warnings (On);

with Ada.Real_Time;
with Ada.Synchronous_Task_Control;

--with M2.Direct_IO;
with Size_Periodic_Tasks_Two_2;

with Tests_Reports;
with M2.HAL;

procedure Size_Periodic_Tasks_Two_Test  is
--   package DIO renames M2.Direct_IO;
   package P2 renames Size_Periodic_Tasks_Two_2;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   Loop_Counter : Integer := 0;

   Next_Time : RT.Time;
   Period : constant RT.Time_Span := RT.Milliseconds (P2.Period_In_Ms);

begin
--     DIO.Put ("-Size_Periodic_Tasks_Two_2-");
--     DIO.New_Line;

   Next_Time := Ada.Real_Time.Clock;

   loop
      M2.HAL.Enable_Interrupts;
--        DIO.Put ("-Main-");

--        if Loop_Counter /= 0 then
--           DIO.Put (" L1=");
--           DIO.Put (Loop_Counter);
--           DIO.Put (" L2=");
--           DIO.Put (P2.Loop_Counter);
--           Tests_Reports.Assert_Equal (P2.Loop_Counter * 2,
--                                      Loop_Counter,
--                                      1);
--        end if;

      if Loop_Counter = P2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      Loop_Counter := Loop_Counter + 1;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end loop;

end Size_Periodic_Tasks_Two_Test;
