----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of the "Seeed_Alphanumeric_Display_HT16K33".
--  https://www.seeedstudio.com/Grove-0-54-Red-Quad-Alphanumeric-Display-p-4032.html
--  hhttps://github.com/Seeed-Studio/Seeed_Alphanumeric_Display_HT16K33/blob/master/grove_alphanumeric_display.h
----------------------------------------------------------------------------
with Interfaces.C;
with Interfaces.C.Strings;
with Arduino.Wire;

package Arduino.Alphanumeric_Display is
     
   type Blink_Type_T is (BLINK_OFF,
                         BLINK_2HZ,
                         BLINK_1HZ);
   for Blink_Type_T'Size use Interfaces.C.Int'Size;
   
   procedure Begin_Alphanumeric_Display
     with Inline_Always;
   
   procedure Clear
     with Inline_Always; 
   
   procedure Display_Num (Num : Interfaces.Unsigned_32;
                          Interval : Time_MS := 500)
     with Inline_Always;
   --  @brief Display number,If the param-num's len less than count of
   --  tubes(2 or 4),The tubes display static number,otherwise,it displays
   --  scroll number.
   --  When it displays scroll number,the param interval is scrolling interval(ms) ..
   --  @param num the number to display.
   --  @param interval :the interval of scroll number.
   
   procedure Display_Null_Terminated_Char_Array
     (Str : Interfaces.C.Strings.char_array_access;
      Interval : Time_MS := 500)
     with Inline_Always;
   
   procedure Set_Point (First_Dot : Boolean8;
                        Second_Dot : Boolean8)
     with Inline_Always;
   --  @brief Set two points status.
   --  @prarm upper_on if true,the first point light on ,otherwise turn off;
   --  @prarm lower_on if true,the second point light on ,otherwise turn off;
   
   procedure Set_Blink_Rate (Blink_Type : Blink_Type_T)
     with Inline_Always;
   
   procedure Set_Brightness (Brightness : Interfaces.Unsigned_8)
     with Inline_Always;
  
   -----------------
   -- Type's size --
   -----------------

   Sizeof_Seeed_Digital_Tube : Integer with
     Import, Convention => C,
     External_Name => "sizeof_Seeed_Digital_Tube";
   --  Defined in 'sizes_arduino_types.cpp' (created by Makefile)
   --  Takes the value of -1 if the library is not included.

   Seeed_Digital_Tube_Size_In_Bytes : constant := 27;   
end Arduino.Alphanumeric_Display;
