
package body System.Interrupts is
   package OSI renames System.OS_Interface;

   ---------------------------------
   -- Install_Restricted_Handlers --
   ---------------------------------

   procedure Install_Restricted_Handlers
     (Prio     : Any_Priority;
      Handlers : Handler_Array) is
      pragma Unreferenced (Prio);
   begin
      OSI.Disable_Interrupts;
      for Handler of Handlers loop
         OSI.Attach_Interrupt
           (Int_Num => OSI.Interrupt_Range (Handler.Interrupt),
            Handler => Handler.Handler,
            Mode    => OSI.Interrupt_When_Rising);
      end loop;
   end Install_Restricted_Handlers;

   --------------------------------------------
   -- Install_Restricted_Handlers_Sequential --
   --------------------------------------------

   procedure Install_Restricted_Handlers_Sequential is
   begin
      pragma Assert (False);
      null;
   end Install_Restricted_Handlers_Sequential;

end System.Interrupts;
