pragma Warnings (Off);
with M2.Kernel.Scheduler;
pragma Elaborate_All (M2.Kernel.Scheduler);
pragma Warnings (On);

package Size_Periodic_Tasks_Six_4 is
   pragma Elaborate_Body;

   Num_Of_Loops : constant := 20;

   Loop_Counter : Integer := 0;

   Period_In_Ms : constant := 20;

   task Other_Task;

end Size_Periodic_Tasks_Six_4;
