----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Real_Time;
with AdaX_Dispatching_Stack_Sharing;

with Tests_Reports;
with M2.Direct_IO;

with PO_Entry_Closed_Access_Param_2;

package body PO_Entry_Closed_Access_Param_1 is
   package DIO renames M2.Direct_IO;
   package P2 renames PO_Entry_Closed_Access_Param_2;

   use type Ada.Real_Time.Time;
   Next_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);

   Loop_Counter : Integer := 0;

   --------------------
   -- Main_Task_Init --
   --------------------

   procedure Main_Task_Init is
   begin
      DIO.Put ("-PO_Entry_Closed_Param_Test-");

      Next_Time := Ada.Real_Time.Clock;
   end Main_Task_Init;

   --------------------
   -- Main_Task_Body --
   --------------------

   procedure Main_Task_Body is
   begin
      DIO.Put ("-Main-");
      DIO.Put (Integer (P2.Step_Counter));
      DIO.Put (Loop_Counter);

      Tests_Reports.Assert (P2.Step_Counter = Loop_Counter * 2);

      P2.Step_Counter := P2.Step_Counter + 1;
      Loop_Counter := Loop_Counter + 1;

      if Loop_Counter /= 1 then
         P2.PO.Open;
      end if;

      if Loop_Counter = P2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Main_Task_Body;

   Main_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Main_Task_Init'Access,
      Body_Ac  => Main_Task_Body'Access,
      Priority => 5);

end PO_Entry_Closed_Access_Param_1;
