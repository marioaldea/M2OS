----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Known_Bug:
--    Test Duration'small is 1/M2.HAL.HWTime_Hz
--    That is NOT true for Arduino RTS.
----------------------------------------------------------------------------
pragma Profile (Ravenscar);
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
with M2.HAL;
pragma Elaborate_All (M2);
with Arduino;
with Ada.Real_Time;

with Tests_Reports;

procedure Arduino_Duration_Small_Test is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put_Line ("Arduino_Duration_Small_Test");

   Tests_Reports.Known_Bug;

   DIO.Put ("Duration'Small:");
   DIO.Put (Integer (Duration'Small * 1_000));
   DIO.Put_Line ("ms");
   Tests_Reports.Assert (Duration'Small = 1.0 / M2.HAL.HWTime_Hz,
                        "Duration'Small /= 1/HWTime_Hz");

   DIO.Put_Line ("Main ends");
   Tests_Reports.Test_OK;
end Arduino_Duration_Small_Test;
