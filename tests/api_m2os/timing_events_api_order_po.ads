----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with System;

with M2.Kernel.API;

package Timing_Events_API_Order_PO is
   package API renames M2.Kernel.API;

   Step : Natural := 0 with Volatile;

   protected PO with Priority => System.Interrupt_Priority'Last is

      procedure PO_Handler1 (Event : in out API.Timing_Event);

      procedure PO_Handler2 (Event : in out API.Timing_Event);

      procedure PO_Handler3 (Event : in out API.Timing_Event);
   end PO;

end Timing_Events_API_Order_PO;
