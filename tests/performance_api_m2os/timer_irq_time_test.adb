pragma Profile (Ravenscar);
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Interfaces;
with Ada.Real_Time;

with M2.Direct_IO;

with Measurements_HiRes;

procedure Timer_IRQ_Time_Test is
   package DIO renames M2.Direct_IO;
   package Measurements renames Measurements_HiRes;

   Num_Of_Loops : constant := 20_000;
   Avg_Time_Us : Measurements.Time_Units;
   Avg_Time : Float;

   Num_Of_Longer_Times : Measurements.Time_Long := 0;
   Sum_Of_Longer_Times : Measurements.Time_Long := 0;


   use type Measurements.Time, Measurements.Time_Long, Interfaces.Integer_32;

begin
   DIO.Put (" Timer_IRQ_Test ");
   DIO.Put (Integer (Num_Of_Loops));
   DIO.New_Line;
   Measurements.Init;

   loop
      -- Measure time to take measurements

      for I in 1 .. Num_Of_Loops loop
         Measurements.Start_Measurement;
         Measurements.End_Measurement;
      end loop;
      Avg_Time_Us := Measurements.Measurement_Avg_In_Time_Units;
      Avg_Time := Float (Measurements.Measurement_Time) /
        Float (Num_Of_Loops);
      DIO.Put (" Avg_Time:");
      DIO.Put (Avg_Time);
      DIO.Put ("Time Units  ");
      Measurements.Print_Measures_Data;

      -- Detect times longer tham expected

      Num_Of_Longer_Times := 0;
      Sum_Of_Longer_Times := 0;
      for I in 1 .. Num_Of_Loops / 30 loop
         Measurements.Reset;
         Measurements.Start_Measurement;
         Measurements.End_Measurement;
         if Measurements.Measurement_Time >
           Measurements.Time_Long (Avg_Time * 1.5)
         then
            DIO.Put (" T:");
            DIO.Put (Integer (Measurements.Measurement_Time));
            Num_Of_Longer_Times := Num_Of_Longer_Times + 1;
            Sum_Of_Longer_Times := Sum_Of_Longer_Times +
              Measurements.Measurement_Time;
         end if;
      end loop;

      DIO.New_Line;
      DIO.Put (" Measurement Timer handler time:");
      DIO.Put (Interfaces.Integer_32 (Measurements.To_Time_Units (
               (Sum_Of_Longer_Times / Num_Of_Longer_Times))) -
                 Interfaces.Integer_32 (Avg_Time_Us));
      DIO.PutChar (Measurements.Time_Unit_Symbol); DIO.PutChar('s');
      DIO.Put (" (Num_Of_Longer_Times:");
      DIO.Put (Integer (Num_Of_Longer_Times));
      DIO.Put (" Sum_Of_Longer_Times:");
      DIO.Put (Interfaces.Integer_32 (Sum_Of_Longer_Times));
      DIO.Put (") ");

      Measurements.Finish;

   end loop;
end Timer_IRQ_Time_Test;
