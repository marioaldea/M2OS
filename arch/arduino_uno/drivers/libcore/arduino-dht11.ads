----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of the DTH11 library DTH11.h
--  DTH11.h can be downloaded from:
--           https://www.prometec.net/wp-content/uploads/2014/10/DHT11.zip
--  To use arduino-dht11.ads:
--      - include DHT11.h in lib_core_generator.ino
--      - add libraries/DHT11/DHT11.cpp to EXTRA_LIBS in Makefile
with Interfaces.C;

with Arduino;

package Arduino.DHT11 is

   DHT11_Size_In_Bytes : constant := 6;

   type DHT11 is limited record
      Pin : Interfaces.C.Int;
      Last_Read_Time : aliased Interfaces.C.Unsigned_Long;
   end record;
   pragma Import (CPP, DHT11);
   for DHT11'Size use DHT11_Size_In_Bytes * 8;

   Sizeof_DHT11 : Integer with
     Import, Convention => C,
     External_Name => "sizeof_DHT11";
   --  Defined in 'arch/arduino_uno/drivers/libcore/sizes_arduino_types.cpp'
   --  Take the value of -1 if the library is not included.

   ---------------
   -- New_DHT11 --
   ---------------

   function New_DHT11 return DHT11;
   pragma CPP_Constructor (New_DHT11, "_ZN5DHT11C1E");
   --  Do not use, only to avoid warning

   function New_DHT11 (Pin_Number : Arduino.Digital_Pin)
                       return DHT11;
   pragma CPP_Constructor (New_DHT11, "_ZN5DHT11C1Ei");

   ----------
   -- Read --
   ----------

   function Read (This : access DHT11;
                  Humidity : access Float;
                  Temperature : access Float)
                  return Natural;
   pragma Import (CPP, Read, "_ZN5DHT114readERfS0_");

end Arduino.DHT11;
