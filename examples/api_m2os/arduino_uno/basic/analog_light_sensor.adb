----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of use of the analog "Grove-Light Sensor" connected on A0.
--      https://wiki.seeedstudio.com/Grove-Light_Sensor/
--
----------------------------------------------------------------------------

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino;
with Arduino.LCD_I2C;

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure Analog_Light_Sensor is
   package DIO renames M2.Direct_IO;
   package LCD renames Arduino.LCD_I2C;
   
   Light_Pin : constant := 0;
   
   Light_Intensity : Arduino.ADC_Value;
   
begin 
   LCD.Begin_LCD (16, 2); 
   loop    
      Light_Intensity := Arduino.Analog_Read (Light_Pin);
      
      DIO.Put ("Light Intensity:");
      DIO.Put (Integer (Light_Intensity));
      DIO.New_Line;
      
      LCD.Clear;
      LCD.Set_Cursor (4, 1);
      LCD.Print_Integer (Integer (Light_Intensity));
      
      Arduino.C_Delay (1_500);
   end loop;
end Analog_Light_Sensor;
