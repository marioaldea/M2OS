----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Uses a Passive Infrared (PIR) Motion Sensor (HC-SR501).
--
--  See pir_demo.ads for details.

with PIR_Demo.PIR;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

package body PIR_Demo.Main_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   --  Next activation time

   Next_Time : RT.Time;

   --------------------
   -- Main_Task_Init --
   --------------------

   procedure Main_Task_Init is
   begin
      DIO.Put_Line ("PIR_Demo.Main_Task initialization");

      Next_Time := RT.Clock +
        Ada.Real_Time.Milliseconds (PIR_Demo.Main_Task_Period_Ms);
      delay until Next_Time;
   end Main_Task_Init;

   --------------------
   -- Main_Task_Body --
   --------------------

   procedure  Main_Task_Body is
   begin
      DIO.Put ("PIR detections:");
      DIO.Put (PIR.Get_Num_Of_Detections);
      PIR.Reset;
      DIO.New_Line;

      Next_Time := Next_Time +
        Ada.Real_Time.Milliseconds (PIR_Demo.Main_Task_Period_Ms);
      delay until Next_Time;
   end Main_Task_Body;

   -----------------
   -- Buzzer_Task --
   -----------------

   Main_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Main_Task_Init'Unrestricted_Access,
      Body_Ac  => Main_Task_Body'Unrestricted_Access,
      Priority => PIR_Demo.Main_Task_Prio);
end PIR_Demo.Main_Task;
