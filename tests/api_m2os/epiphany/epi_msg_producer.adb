----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Authors: David Garc�a Villaescusa (garciavd@unican.es)
--           Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Interfaces;
with E_Lib;
with M2.Direct_IO;
with Tests_Reports;
with Epiphany_Messages;
use Epiphany_Messages;
with Ada.Unchecked_Conversion;
with Ada.Real_Time;
use Ada.Real_Time;

package body Epi_Msg_Producer is
   package DIO renames M2.Direct_IO;
   
   Loop_Counter : Integer;
   
   Next_Time : Ada.Real_Time.Time;

   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);
   
   
   SP_Acc : Epiphany_Messages.SP_Id;
   SP_Num : Epiphany_Messages.SP_Index := 0;
   
   --------------------
   -- Main_Task_Init --
   --------------------
      
   procedure Main_Task_Init is
      Target_Core : E_Lib.Core;
   begin
      DIO.Put_Line ("-Epi_Msg_Producer-Init");
   
      Target_Core.Row := 3;
      Target_Core.Col := 2;
      
      Loop_Counter := 0;
   
      while Epiphany_Messages.Get_Sampling_Port (Target_Core, SP_Num, SP_Acc) = False loop
         null;
      end loop;
   
      Next_Time := Ada.Real_Time.Clock;
   end Main_Task_Init;
   
   --------------------
   -- Main_Task_Body --
   -------------------
   
   procedure Main_Task_Body is
      Success : Boolean;
   begin      
      Loop_Counter := Loop_Counter + 1;
      Success := Epiphany_Messages.Write_Sampling_Port (SP_Acc, Loop_Counter'Address, Loop_Counter'Size);
      
      if Loop_Counter = 5 then
         DIO.Put_Line ("-Epi_Msg_Producer-Body 5 loops done");
         Tests_Reports.Test_OK;
      end if;
      
      Next_Time := Next_Time + Period;
      delay until Next_Time; 
   end Main_Task_Body;

end Epi_Msg_Producer;
