----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Task to read de DHT11 Sensor.

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing.Periodic_Task;
with Ada.Real_Time;

with Arduino.DHT.DHT11_Sensor;

with CO2_Sensor.LCD_Tasks;
with CO2_Sensor.CCS811_Task;

package body CO2_Sensor.DHT11_Tasks is
   package DIO renames M2.Direct_IO;
   
   package DHT11 is new Arduino.DHT.DHT11_Sensor (Pin => CO2_Sensor.DHT11_Pin);

   Measurement : aliased Arduino.DHT.Measurement := (20.0, 50.0);
   Ret : Integer := 0;
   
   ---------------------
   -- DHT11_Task_Body --
   ---------------------

   procedure DHT11_Task_Body is
   begin 
      -- DIO.Put_Line ("DHT11_Task");
      
      Ret := DHT11.Read (Measurement'Access);
      if Ret = 0 then
         -- DIO.Put ("Humidity:" & Integer (Measurement.Humidity)'Img);
         -- DIO.Put (" Temperature:" & Integer (Measurement.Temperature)'Img);
         DIO.New_Line;
      else
         DIO.Put ("DHT11 error:");
         DIO.Put (Ret);
         Measurement.Temperature := 0.0;
         Measurement.Humidity := 0.0;
      end if;
      
      --  Write on LCD
      
      LCD_Tasks.Set_Temperature (Measurement.Temperature);
      LCD_Tasks.Set_Humidity (Measurement.Humidity);
      if Measurement.Humidity < 30.0 then
         LCD_Tasks.Set_Humidity_Level (Hum_Very_Dry);
      elsif Measurement.Humidity < 40.0 then
         LCD_Tasks.Set_Humidity_Level (Hum_Dry);
      elsif Measurement.Humidity < 60.0 then
         LCD_Tasks.Set_Humidity_Level (Hum_Normal);
      elsif Measurement.Humidity < 70.0 then    
         LCD_Tasks.Set_Humidity_Level (Hum_Wet);
      else
         LCD_Tasks.Set_Humidity_Level (Hum_Very_Wet);
      end if;
   end DHT11_Task_Body;
    
   ----------------
   -- DHT11_Task --
   ----------------

   package DHT11_Task is new AdaX_Dispatching_Stack_Sharing.Periodic_Task
     (Init_Ac  => null,
      Body_Ac  => DHT11_Task_Body'Access,
      Priority => CO2_Sensor.DHT11_Task_Prio,
      Period   => Ada.Real_Time.Milliseconds (CO2_Sensor.DHT11_Task_T_In_Ms));
     
   ---------------------------------
   -- Update_CCS811_Env_Task_Body --
   ---------------------------------

   procedure Update_CCS811_Env_Task_Body is
   begin 
      -- DIO.Put_Line ("Update_CCS811_Env_Task");
      
      CCS811_Task.Set_Env_Temp_Hum (Measurement.Temperature,
                                    Measurement.Humidity);
   end Update_CCS811_Env_Task_Body;
    
   ----------------------------
   -- Update_CCS811_Env_Task --
   ----------------------------

   package Update_CCS811_Env_Task is
     new AdaX_Dispatching_Stack_Sharing.Periodic_Task
       (Init_Ac  => null,
        Body_Ac  => Update_CCS811_Env_Task_Body'Access,
        Priority => CO2_Sensor.Update_CCS811_Env_Task_Prio,
        Period   => Ada.Real_Time.Milliseconds
          (CO2_Sensor.Update_CCS811_Env_Task_T_In_Ms));

   
end CO2_Sensor.DHT11_Tasks;
