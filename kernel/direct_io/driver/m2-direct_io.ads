----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
pragma Restrictions (No_Elaboration_Code);

with System;
with Interfaces;
with M2.Drivers;

package M2.Direct_IO is

#if not M2_TARGET = "rp2040" then
   --  pragma Pure;
   pragma Preelaborate;
#end if;

   procedure Put (Str : String);

   procedure Put_Line (Str : String);

   procedure Put (Num : Integer; Base : Interfaces.Unsigned_8 := 10)
     with
       Export        => True,
       Convention    => C,
       External_Name => "m2__direct_io__putint";

   procedure Put (Num : Interfaces.Integer_32;
                  Base : Interfaces.Unsigned_8 := 10)
     with
       Export        => True,
       Convention    => C,
       External_Name => "m2__direct_io__putint32";

   procedure Put_Null_Terminated (Str_Ad : System.Address);

   procedure Put (D : Duration);

   procedure Put (F : Float);

   procedure PutChar (C : Character);

   procedure New_Line;

--     procedure Change_Device (Write_Function : Drivers.Write_Function_Ac);

   procedure Initialization;

end M2.Direct_IO;
