----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding to Elechouse Voice Recognition V3
--  https://www.elechouse.com/elechouse/index.php?main_page=product_info&cPath&products_id=2254
--  https://github.com/kksjunior/Voice-Recognition-with-Elechouse-V3/blob/master

package body Arduino.Voice_Recognition is

   -------------
   -- VR type --
   -------------

   type VR_Filling is
     array(1 .. VR_Size_In_Bytes) of Interfaces.Unsigned_8;

   type VR is limited record
      Filling: VR_Filling;
   end record;
   pragma Import (CPP, VR);
   for VR'Size use VR_Size_In_Bytes * 8;
   for VR'Alignment use Standard'Maximum_Alignment;

   ------------------------
   -- Imported functions --
   ------------------------

   function New_VR return VR;
   pragma CPP_Constructor (New_VR, "_ZN2VRC1Ev");
   --  Don't use it, just to avoid warning

   function New_VR (Receive_Pin : Arduino.Digital_Pin;
                    Transmit_Pin : Arduino.Digital_Pin)
                    return VR;
   pragma CPP_Constructor (New_VR, "_ZN2VRC1Ehh");

   procedure Begin_VR (This : access VR;
                       Speed : Interfaces.C.Long);
   pragma Import (CPP, Begin_VR, "_ZN14SoftwareSerial5beginEl");

   function Clear (This : access VR)
                  return Interfaces.C.Int;
   pragma Import (CPP, Clear, "_ZN2VR5clearEv");

   function Load (This : access VR;
                  Record_Num : VR_Record;
                  Buf : access VR_Buffer)
                  return Interfaces.C.Int;
   pragma Import (CPP, Load, "_ZN2VR4loadEhPh");

   function Recognize (This : access VR;
                       Buf : access VR_Buffer;
                       Timeout : Interfaces.C.Int)
                       return Interfaces.C.Int;
   pragma Import (CPP, Recognize, "_ZN2VR9recognizeEPhi");

   ------------------------------
   -- Voice Recognition Module --
   ------------------------------

   package body VR_Module is

      ---------------
      -- VR_Module --
      ---------------

      VR_Module : aliased VR := New_VR (Receive_Pin, Transmit_Pin);

      --------------
      -- Begin_VR --
      --------------

      procedure Begin_VR (Speed : Interfaces.C.Long) is
      begin
         Begin_VR (VR_Module'Access, Speed);
      end Begin_VR;

      -----------
      -- Clear --
      -----------

      function Clear return Interfaces.C.Int is
        (Clear (VR_Module'Access));


      function Load (Record_Num : VR_Record;
                     Buf : access VR_Buffer := null)
                     return Interfaces.C.Int is
         (Load (VR_Module'Access, Record_Num, Buf));


      function Recognize (Buf : access VR_Buffer;
                          Timeout : Interfaces.C.Int := VR_Default_Timeout)
          return Interfaces.C.Int is
         (Recognize (VR_Module'Access, Buf, Timeout));
   end VR_Module;

end Arduino.Voice_Recognition;
