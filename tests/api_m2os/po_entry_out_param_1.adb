----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Real_Time;

with AdaX_Dispatching_Stack_Sharing;

with Tests_Reports;
with M2.Direct_IO;

with PO_Entry_Out_Param_2;

package body PO_Entry_Out_Param_1 is
   package DIO renames M2.Direct_IO;
   package P2 renames PO_Entry_Out_Param_2;

   use type Ada.Real_Time.Time;

   Next_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);

   Loop_Counter : Integer := 0;

   -----------------
   -- Task_1_Init --
   -----------------

   procedure Task_1_Init is
   begin
      DIO.Put ("-Task_1_Init-");

      Next_Time := Ada.Real_Time.Clock;
   end Task_1_Init;

   -----------------
   -- Task_1_Body --
   -----------------

   procedure Task_1_Body is
   begin
      DIO.Put ("-Task_1_Body-");
      DIO.Put (Integer (P2.Step_Counter));
      DIO.Put (Loop_Counter);

      case Loop_Counter is
         when 0 =>
            Tests_Reports.Assert (P2.Step_Counter = 0);

         when 1 =>
            Tests_Reports.Assert (P2.Step_Counter = 3);

         when 2 =>
            Tests_Reports.Assert (P2.Step_Counter = 5);

         when 3 =>
            Tests_Reports.Assert (P2.Step_Counter = 7);

         when others =>
            Tests_Reports.Assert (False);
      end case;

      P2.Step_Counter := P2.Step_Counter + 1;
      Loop_Counter := Loop_Counter + 1;

      P2.PO.Open (P2.PO_Data);

      if Loop_Counter = P2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

--      System.OS_Interface.Running_Thread_Ends_Job_Without_Blocking;
      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Task_1_Body;

   Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_1_Init'Access,
      Body_Ac  => Task_1_Body'Access,
      Priority => 5);

end PO_Entry_Out_Param_1;
