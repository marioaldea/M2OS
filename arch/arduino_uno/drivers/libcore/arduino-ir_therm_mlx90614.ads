--------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  MLX90614 Single and Dual Zone Infra Red Thermometer.
--  Binding for the "SparkFun_MLX90614_Arduino_Library" library:
--    https://github.com/sparkfun/SparkFun_MLX90614_Arduino_Library
--
--  Tested with the "Grove - Single-Point Infrared Thermometer - MLX90614 DCC
--  with 35� FOV"
--    https://www.seeedstudio.com/Grove-Thermal-Imaging-Camera-MLX90614-DCC-IR-Array-with-35-FOV-p-4657.html
--
--  Only one MLX90614 is supported in the current version. It is possible
--  to extend this package to allow more than one sensors (see SparkFun
--  Library for details).
----------------------------------------------------------------------------
with Interfaces.C;
with Arduino.Wire;

package Arduino.IR_Therm_MLX90614 is

   type Temperature_Units is (TEMP_RAW,
                              TEMP_K,
                              TEMP_C,
                              TEMP_F);
   for Temperature_Units'Size use 16;

   MLX90614_DEFAULT_ADDRESS : constant Wire.Dev_Address := 16#5A#;

   function Initialize
     (I2C_Address : Wire.Dev_Address := MLX90614_DEFAULT_ADDRESS)
      return Boolean;

   --  Configures the units returned by the Temp_Ambient() and
   --  Temp_Object().
   --  <unit> can be either:
   --    - TEMP_RAW: No conversion, just the raw 12-bit ADC reading
   --    - TEMP_K: Kelvin
   --    - TEMP_C: Celsius
   --    - TEMP_F: Farenheit
   procedure Set_Unit (Unit : Temperature_Units)
     with Inline;

   --  Returns the MLX90614's most recently read object temperature
   --  after the read() function has returned successfully. The float value
   --  returned will be in the units specified by Set_Unit().
   function Temp_Object return Float
     with Inline;

   --  Returns the MLX90614's most recently read ambient temperature
   --  after the read() function has returned successfully. The float value
   --  returned will be in the units specified by Set_Unit().
   function Temp_Ambient return Float
     with Inline;

   --  Pulls the latest ambient and object temperatures from the
   --  MLX90614. It will return true on success. (Failure
   --  can result from either a timed out I2C transmission, or an incorrect
   --  checksum value)
   function Read_Temps return Boolean8
     with Inline;

   subtype Emissivity is Float range 0.1 .. 1.0;

   -- Reads the MLX90614's emissivity setting. It will
   -- return a value between 0.1 and 1.0.
   function Read_Emissivity return Emissivity
     with Inline;

   --  Set the MLX90614's configured emissivity EEPROM value.
   --  The <emis> parameter should be a value between 0.1 and 1.0.
   --  The function will return true on success.
   function Set_Emissivity (Emis : Float) return Boolean8
     with Inline;

   -----------------
   -- Type's size --
   -----------------

   Sizeof_IRTherm : Integer with
     Import, Convention => C,
     External_Name => "sizeof_IRTherm";
   --  Defined in 'sizes_arduino_types.cpp' (created by Makefile)
   --  Takes the value of -1 if the library is not included.

   IRTherm_Size_In_Bytes : constant := 23;

end Arduino.IR_Therm_MLX90614;
