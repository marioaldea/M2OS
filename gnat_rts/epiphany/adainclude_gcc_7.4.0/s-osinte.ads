--  M2OS Arduino Uno version of this package.

--  Imports from M2.Kernel.API

with Interfaces;
with Interfaces.C;
with Ada.Unchecked_Conversion;

package System.OS_Interface is
   pragma Preelaborate;

   ----------------
   -- Interrupts --
   ----------------

   procedure Enable_Interrupts;
   pragma Import (C, Enable_Interrupts, "m2__hal__enable_interrupts");

   procedure Disable_Interrupts;
   pragma Import (C, Disable_Interrupts, "m2__hal__disable_interrupts");

--   subtype Interrupt_Range is ;
   --  Range of interrupts identifiers, for s-inter

--   subtype Interrupt_ID is ;
   --  Interrupt identifiers

--   subtype Any_Interrupt_ID is Integer
--     range Interrupt_ID'First .. Interrupt_ID'Last + 1;
   --  Interrupt identifiers plus No_Interrupt

--  No_Interrupt : constant Any_Interrupt_ID := Any_Interrupt_ID'Last;
   --  Special value indicating no interrupt

--   type Interrupt_Handler is access procedure (Id : Interrupt_ID);
   --  Interrupt handlers

   --------------------------
   -- Interrupt processing --
   --------------------------

--     function Current_Interrupt return Any_Interrupt_ID
--       renames System.BB.Interrupts.Current_Interrupt;
--     --  Function that returns the hardware interrupt currently being
--     --  handled (if any). In case no hardware interrupt is being handled
--     --  the returned value is No_Interrupt.
--
--   procedure Attach_Handler
--     (Handler : Interrupt_Handler;
--      Id      : Interrupt_ID;
--      PO_Prio : Interrupt_Priority) is null;
--     --  Attach a handler to a hardware interrupt
--
--     procedure Power_Down;
--     --  Put current CPU in power-down mode

   ----------
   -- Time --
   ----------

   type Time is new Interfaces.Unsigned_64;
   --  Representation of the time in the underlying tasking system

   type Time_Span is new Interfaces.Integer_64;
   --  Represents the length of time intervals in the underlying tasking
   --  system.

   Ticks_Per_Second : constant := 1_000;
   --  Number of clock ticks per second
   --  M2OS: Should have the same value than M2.HAL.HWT_HZ

   function Clock return Time;
   pragma Import (C, Clock, "m2__hal__get_hwtime");
   --  Get the number of ticks elapsed since startup

   procedure Delay_Until (T : Time);
   pragma Import (C, Delay_Until,
                  "m2__kernel__scheduler__suspend_running_thread");
   --  Suspend the calling task until the absolute time specified by T

   -------------
   -- Threads --
   -------------

   type Thread_Ac is new System.Address;

   Null_Thread_Ac : constant Thread_Ac := Thread_Ac (System.Null_Address);

   subtype Thread_Id is Thread_Ac;

   type Thread_T is array (1 .. System.Thread_T_Size_In_Bytes) of
     Interfaces.Unsigned_8;
   for Thread_T'Size use System.Thread_T_Size_In_Bytes * 8;

   subtype Thread_Body_Ac is System.Address;
--     pragma Convention (C, Thread_Body_Ac);

--     type Procedure_Ac is access procedure;
--     function To_Thread_Body_Ac is
--       new Ada.Unchecked_Conversion (Procedure_Ac, Thread_Body_Ac);
--     function To_Thread_Body_Ac is
--       new Ada.Unchecked_Conversion (System.Address, Thread_Body_Ac);

   ----------
   -- Self --
   ----------

   function Self return Thread_Ac;
   pragma Import (C, Self, "m2_self");

   ------------------
   -- Set_Job_Body --
   ------------------

   procedure Set_Job_Body (Body_Ac : Thread_Body_Ac);
   pragma Import (C, Set_Job_Body,
                  "m2__kernel__api__set_job_body_of_running_thread");
   pragma Import_Procedure (Set_Job_Body,
                            "m2__kernel__api__set_job_body_of_running_thread",
                            (Thread_Body_Ac),
                            Mechanism => Value);

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

--     procedure Set_Global_Stack_Limit;
--     pragma Import (C, Set_Global_Stack_Limit,
--                    "m2__kernel__api__set_global_stack_limit");
--  Not requied now, since the stack is always started at the lower
--  position.

   ----------
   -- ATCB --
   ----------

   function Get_ATCB return System.Address;
   pragma Import (C, Get_ATCB, "m2_self");
   --  Get the ATCB associated to the currently running thread

   -------------------
   -- Create_Thread --
   -------------------

   procedure Create_Thread (Th  : access Thread_T;
                            Th_Body : Thread_Body_Ac;
                            Prio : Priority;
                            Args : System.Address);
   pragma Import (C, Create_Thread, "m2__kernel__api__create_thread");
--     pragma Import_Procedure (
--       (Internal        => Create_Thread,
--        External        => "marte__kernel__api__create_thread",
--        Parameter_Types => (Thread_Ac,
--                            Thread_Body_Ac,
--                            Priority),
--        Mechanism       => (Th_Ac   => Reference,
--                            Th_Body => Value,
--                            Prio    => Value)));

   ----------------------------------------------
   -- Running_Thread_Ends_Job_Without_Blocking --
   ----------------------------------------------

   procedure Running_Thread_Ends_Job_Without_Blocking;
   pragma Import (C, Running_Thread_Ends_Job_Without_Blocking,
                  "m2_running_thread_ends_job_without_blocking");

   --------------------------
   -- Block_Running_Thread --
   --------------------------

   procedure Block_Running_Thread;
   pragma Import (C, Block_Running_Thread, "m2_block_running_thread");

   -----------------------------
   -- Activate_Blocked_Thread --
   -----------------------------

   procedure Activate_Blocked_Thread (Th_Ac : Thread_Ac);
   pragma Import (C, Activate_Blocked_Thread,
                  "m2_activate_blocked_thread");

   ---------------------
   -- Multiprocessors --
   ---------------------

--     function Get_Affinity (Id : Thread_Id) return Multiprocessors.CPU_Range
--       renames System.BB.Threads.Get_Affinity;
   --  Return CPU affinity of the given thread (maybe Not_A_Specific_CPU)

--     function Get_CPU  (Id : Thread_Id) return Multiprocessors.CPU is
--       (Multiprocessors.CPU'First);
--       renames System.BB.Threads.Get_CPU;
   --  Return the CPU in charge of the given thread (always a valid CPU)

--     function Current_Priority
--       (CPU_Id : Multiprocessors.CPU) return System.Any_Priority
--       renames System.BB.Threads.Queues.Current_Priority;
   --  Return the active priority of the current thread or
   --  System.Any_Priority'First if no threads are running.

   --  function Current_CPU return Multiprocessors.CPU is
   --   (Multiprocessors.CPU'First);
--       renames System.BB.Board_Support.Multiprocessors.Current_CPU;

   procedure Initialize;
   --  To be called from System.Tasking.Initialize
   pragma Import (C, Initialize, "m2osinit");
   --  M2OS library (libm2os.a) initializacion
end System.OS_Interface;
