--  Measure the context switch when using a delay until with a
--  time in the past.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

pragma Warnings (Off);
with System.OS_Interface;
pragma Warnings (On);

with Ada.Real_Time;
with Ada.Synchronous_Task_Control;

with M2.Direct_IO;
with CS_Activations_2;

with Measurements;
with M2.HAL;

procedure CS_Activations_Test  is
   package DIO renames M2.Direct_IO;
   package P2 renames CS_Activations_2;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span, P2.Suspension_Primitives;

   Loop_Counter : Integer := 0;

   Next_Time, Start_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span := RT.Milliseconds (20);

   I : aliased Integer;

begin
   DIO.Put ("-CS_Delay_Simple_Test-");
   DIO.New_Line;

   Measurements.Init (Measurements.Short);
   Start_Time := Ada.Real_Time.Clock;

   loop
      M2.HAL.Enable_Interrupts;
      --DIO.Put ("-Main-");

      if Loop_Counter /= 0 then
         Measurements.End_Measurement;
      end if;

      if Loop_Counter = P2.Num_Of_Loops then
         DIO.Put (P2.Suspension_Primitives'Pos (P2.Suspension_Primitive));
         DIO.Put (" L1=");
         DIO.Put (Loop_Counter);
         DIO.Put (" L2=");
         DIO.Put (P2.Loop_Counter);
         Measurements.Assert_Equal (P2.Num_Of_Loops, P2.Loop_Counter, 1);

         DIO.Put (" CSTime(us)=");
         DIO.Put (Integer (Measurements.Measurement_Avg_Time_Us));
         DIO.New_Line;

         if P2.Suspension_Primitive /= P2.Suspension_Primitives'Last then
            P2.Suspension_Primitive :=
              P2.Suspension_Primitives'Succ (P2.Suspension_Primitive);
            Loop_Counter := 0;
            P2.Loop_Counter := 0;
            Measurements.Reset;
         else
            Measurements.Finish;
         end if;
      end if;

      Loop_Counter := Loop_Counter + 1;

      Next_Time := Start_Time + Period;

      case P2.Suspension_Primitive is
         when P2.Delay_Until_Past_Time =>
            delay until Ada.Real_Time.Time_First;
         --when P2.Delay_Until =>
         --   delay until Ada.Real_Time.Time_First; --  Next_Time;
         when P2.Suspension_Object =>
            Ada.Synchronous_Task_Control.Suspend_Until_True (P2.SO);
         when P2.Yield =>
            System.OS_Interface.Running_Thread_Ends_Job_Without_Blocking;
         when P2.PO_Entry =>
            P2.PO.Wait;
         when P2.PO_Entry_Param =>
            P2.PO_Param.Wait (I'Access);
         when others =>
            delay until Ada.Real_Time.Time_First;
      end case;
   end loop;

end CS_Activations_Test;
