----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--  Test the time for the delay until and for Ada.Real_Time.Clock are
--  equivalent.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with System;
with M2.Kernel.TCBs;

with M2.Direct_IO;
with Tests_Reports;

with Delay_And_Clock_1;

procedure Delay_And_Clock_Test  is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put_Line ("-Delay_And_Clock_Test-");

   DIO.Put_Line ("TCB'Size" & Integer'Image (M2.Kernel.TCBs.TCB'Size));
   DIO.Put_Line ("System.Thread_T_Size_In_Bytes" &
                   Integer'Image (System.Thread_T_Size_In_Bytes));
   Tests_Reports.Assert
     (M2.Kernel.TCBs.TCB'Size = System.Thread_T_Size_In_Bytes * 8);
end Delay_And_Clock_Test;
