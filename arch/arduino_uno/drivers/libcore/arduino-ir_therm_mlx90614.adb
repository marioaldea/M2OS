--------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding for the "SparkFun_MLX90614_Arduino_Library" library:
--    https://github.com/sparkfun/SparkFun_MLX90614_Arduino_Library
--
----------------------------------------------------------------------------
with System;
with Interfaces.C;

package body Arduino.IR_Therm_MLX90614 is

   -------------
   -- IRTherm --
   -------------

   type IRTherm_Filling is
     array(1 .. IRTherm_Size_In_Bytes) of Interfaces.Unsigned_8;

   type IRTherm is limited record
      Filling : IRTherm_Filling;
   end record;
   pragma Import (CPP, IRTherm);
   for IRTherm'Size use IRTherm_Size_In_Bytes * 8;
   for IRTherm'Alignment use Standard'Maximum_Alignment;

   ------------------------
   -- Imported functions --
   ------------------------

   function New_IRTherm return IRTherm;
   pragma CPP_Constructor (New_IRTherm, "_ZN7IRThermC1Ev");

   function Begin_Cpp (This : access IRTherm;
                       Address : Wire.Dev_Address;
                       Wire_Ac : access Wire.Two_Wire)
                       return Boolean8
     with Import, Convention => CPP,
     External_Name => "_ZN7IRTherm5beginEhR7TwoWire";

   procedure Set_Unit_Cpp (This : access IRTherm;
                           Unit : Temperature_Units)
     with Import, Convention => CPP,
     External_Name => "_ZN7IRTherm7setUnitE17temperature_units";

   function Object_Cpp (This : access IRTherm)
                        return Float
     with Import, Convention => CPP,
     External_Name => "_ZN7IRTherm6objectEv";

   function Ambient_Cpp (This : access IRTherm)
                         return Float
     with Import, Convention => CPP,
     External_Name => "_ZN7IRTherm7ambientEv";

   function Read_Cpp (This : access IRTherm)
                      return Boolean8
     with Import, Convention => CPP,
     External_Name => "_ZN7IRTherm4readEv";

   function Read_Emissivity_Cpp (This : access IRTherm)
                                 return Emissivity
     with Import, Convention => CPP,
     External_Name => "_ZN7IRTherm14readEmissivityEv";

   function Set_Emissivity_Cpp (This : access IRTherm;
                                Emis : Float)
                                return Boolean8;
   pragma Import (CPP, Set_Emissivity_Cpp, "_ZN7IRTherm13setEmissivityEf");
   pragma Import_Function
     (Internal => Set_Emissivity_Cpp,
      External => "_ZN7IRTherm13setEmissivityEf",
      --Parameters_Types => (access IRTherm, Emissivity),
      Mechanism => (This => Value, Emis => Value));
--       with Import, Convention => CPP,
--       External_Name => "_ZN7IRTherm13setEmissivityEf";

   ------------
   -- Sensor --
   ------------

   Sensor : aliased IRTherm;

   ----------------
   -- Initialize --
   ----------------

   function Initialize
     (I2C_Address : Wire.Dev_Address := MLX90614_DEFAULT_ADDRESS)
      return Boolean is
   begin
      Wire.Begin_Two_Wire (Wire.Wire'Access);
      return Boolean (Begin_Cpp (Sensor'Access,
                      Address => I2C_Address,
                      Wire_Ac => Arduino.Wire.Wire'Access));
   end Initialize;

   --------------
   -- Set_Unit --
   --------------

   procedure Set_Unit (Unit : Temperature_Units) is
   begin
      Set_Unit_Cpp (Sensor'Access, Unit);
   end Set_Unit;

   -----------------
   -- Temp_Object --
   -----------------

   function Temp_Object return Float is
     (Object_Cpp (Sensor'Access));

   ------------------
   -- Temp_Ambient --
   ------------------

   function Temp_Ambient return Float is
     (Ambient_Cpp (Sensor'Access));

   ----------------
   -- Read_Temps --
   ----------------

   function Read_Temps return Boolean8 is
     (Read_Cpp (Sensor'Access));

   ---------------------
   -- Read_Emissivity --
   ---------------------

   function Read_Emissivity return Emissivity is
     (Read_Emissivity_Cpp (Sensor'Access));

   --------------------
   -- Set_Emissivity --
   --------------------

   function Set_Emissivity (Emis : Float) return Boolean8 is
     (Set_Emissivity_Cpp (Sensor'Access, Emis));

end Arduino.IR_Therm_MLX90614;
