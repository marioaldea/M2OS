----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--  Test FIFO ordering in ready queue by simulating a Round-Robin scheduling.
--  Main task uses using Running_Thread_Ends_Activation_Without_Blocking
--  and activates the secoundary tasks using suspension objects.
--  Activation order of secoundary tasks is changed to be sure FIFO ordering
--  is working properly.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);
--  pragma Restrictions (No_Secondary_Stack);

with M2.Direct_IO;

with Round_Robin_1;
with Round_Robin_2;
with Round_Robin_3;

procedure Round_Robin_Test  is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put ("-Round_Robin_Test-");
end Round_Robin_Test;
