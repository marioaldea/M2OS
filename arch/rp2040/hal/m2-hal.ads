----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  RP2040 version of this package

pragma Restrictions (No_Elaboration_Code);

with System;
with Interfaces;
with Ada.Unchecked_Conversion;

package M2.HAL is
#if not M2_TARGET = "rp2040" then
   --  pragma Pure;
   pragma Preelaborate;
#end if;

   type Procedure_Ac is access procedure;

   subtype Thread_Context is Procedure_Ac;

   Events_Order_Counter : Interfaces.Unsigned_8 := 0;
   --  To keep FIFO order when threads have equal Activation_Time.
   --  Set to 0 in the timer handler.

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch (New_Context : Thread_Context);

--     ------------------
--     -- Save_Context --
--     ------------------
--
--     procedure Save_Context (Context : access Thread_Context;
--                             Job_Body_Ac : Procedure_Ac);
   --  pragma Inline_Always (Save_Context);

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

   procedure Set_Global_Stack_Limit;
   --  pragma Inline_Always (Set_Global_Stack_Limit);

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Natural;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Natural;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Natural;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Natural;

   -------------------------------
   -- Enable/disable interrupts --
   -------------------------------

   procedure Enable_Interrupts;
   --  pragma Inline_Always (Enable_Interrupts);

   procedure Disable_Interrupts;
   --  pragma Inline_Always (Disable_Interrupts);

   function Are_Interrupts_Enabled return Boolean;

   ------------
   -- HWTime --
   ------------

   HWTime_Hz : constant := 1000;
   --  Should have the same value than OSI.Ticks_Per_Second

   type HWTime is new Interfaces.Unsigned_64;
--   for HWTime'Size use Interfaces.Unsigned_64'Size;
--   type HWTime is new Interfaces.Unsigned_32;
--   for HWTime'Size use Duration'Size;

--   ------------------------
--   -- HWTime_To_Duration --
--   ------------------------
--
--     function HWTime_To_Duration is
--        new Ada.Unchecked_Conversion (HWTime, Duration);
--
--     ------------------------
--     -- Duration_To_HWTime --
--     ------------------------
--
--     function Duration_To_HWTime is
--        new Ada.Unchecked_Conversion (Duration, HWTime);

   ----------------
   -- Get_HWTime --
   ----------------

   function Get_HWTime return HWTime;

   ------------------------
   -- OS_Tick_Handler_Ac --
   ------------------------

   type OS_Tick_Handler_Ac is access procedure (Now : HWTime);

   --------------------
   -- Initialization --
   --------------------

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac);

end M2.HAL;
