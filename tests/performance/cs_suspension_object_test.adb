--  Measure the context switch when using a suspension object.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Synchronous_Task_Control;

with M2.Direct_IO;

with CS_Suspension_Object_2;

with Measurements;
with M2.HAL;

procedure CS_Suspension_Object_Test  is
   package DIO renames M2.Direct_IO;
   package S2 renames CS_Suspension_Object_2;
   package STC renames Ada.Synchronous_Task_Control;

   Loop_Counter : Integer := 0;

begin
   DIO.Put ("-CS_Suspension_Object_Test-");

   if S2.Short_Time then
      Measurements.Init (Measurements.Short);
   else
      Measurements.Init (Measurements.Long);
   end if;
   Measurements.Start_Measurement;
   loop
      M2.HAL.Enable_Interrupts;
      --DIO.Put ("-Main-");

      if Loop_Counter = S2.Num_Of_Loops then
         Measurements.End_Measurement;

         DIO.Put ("L1=");
         DIO.Put (Loop_Counter);
         DIO.Put (" L2=");
         DIO.Put (S2.Loop_Counter);
         Measurements.Assert_Equal (S2.Num_Of_Loops, S2.Loop_Counter, 0);

         DIO.Put (" CSTime(us)=");
         DIO.Put (Integer (Measurements.Measurement_Avg_Time_Us) /
                    (Loop_Counter + S2.Loop_Counter));
         Measurements.Finish;
      end if;

      Loop_Counter := Loop_Counter + 1;

      STC.Set_True (S2.Suspension_Object_2);
      STC.Suspend_Until_True (S2.Suspension_Object_1);
   end loop;

end CS_Suspension_Object_Test;
