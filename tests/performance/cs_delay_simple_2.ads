pragma Warnings (Off);
with M2.Kernel.Scheduler;
pragma Elaborate_All (M2.Kernel.Scheduler);
pragma Warnings (On);

package CS_Delay_Simple_2 is
   pragma Elaborate_Body;

   Num_Of_Loops : constant := 20;

   Loop_Counter : Integer := 0;
   Other_Task_Executed : Boolean := False;

   task Other_Task;

end CS_Delay_Simple_2;
