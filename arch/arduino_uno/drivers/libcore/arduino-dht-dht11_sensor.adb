package body Arduino.DHT.DHT11_Sensor is


      ------------
      -- Sensor --
      ------------

      Sensor : aliased DHT := New_DHT (Pin, Arduino.DHT.DHT11);

      ---------------
      -- Begin_DHT --
      ---------------

      procedure Begin_DHT is
      begin
         Begin_DHT (Sensor'Access);
      end Begin_DHT;

      ----------
      -- Read --
      ----------

      function Read (TH : access Measurement) return Integer is
      begin
         return Read (Sensor'Access, TH);
      end Read;
   

end Arduino.DHT.DHT11_Sensor;
