----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

pragma Profile (Ravenscar);
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;

with Arduino.Wire;
with Arduino.Servo;
with Arduino.Software_Serial;
with Arduino.LCD_I2C;
with Arduino.DHT11;
with Arduino.DHT;
with Arduino.Voice_Recognition;
with Arduino.CCS811;
with Arduino.IR_Therm_MLX90614;

with Tests_Reports;

procedure Arduino_Types_Size_Test is
   package DIO renames M2.Direct_IO;

   type Arduino_CPP_Types is (Servo, Two_Wire, SoftwareSerial,
                              RGB_LCD, DHT, DHT11, VR, CCS811, IRTherm);

   type Type_Data is record
      Name : String (1 .. 14);
      CPP_Sizeof : Integer;
      Ada_Size_In_Bytes : Positive;
   end record;

   Types_Data : array (Arduino_CPP_Types) of Type_Data :=
     (Servo =>
        (Name              => "Servo         ",
         CPP_Sizeof        => Arduino.Servo.Sizeof_Servo,
         Ada_Size_In_Bytes => Arduino.Servo.Servo'Size / 8),
      Two_Wire =>
        (Name              => "Two_Wire      ",
         CPP_Sizeof        => Arduino.Wire.Sizeof_Two_Wire,
         Ada_Size_In_Bytes => Arduino.Wire.Two_Wire'Size / 8),
      SoftwareSerial =>
        (Name              => "SoftwareSerial",
         CPP_Sizeof        => Arduino.Software_Serial.Sizeof_Software_Serial,
         Ada_Size_In_Bytes => Arduino.Software_Serial.Software_Serial'Size / 8),
      RGB_LCD =>
        (Name              => "RGB_LCD       ",
         CPP_Sizeof        => Arduino.LCD_I2C.Sizeof_LCD,
         Ada_Size_In_Bytes => Arduino.LCD_I2C.LCD_Size_In_Bytes),
      DHT =>
        (Name              => "DHT           ",
         CPP_Sizeof        => Arduino.DHT.Sizeof_DHT,
         Ada_Size_In_Bytes => Arduino.DHT.DHT_Size_In_Bytes),
      DHT11 =>
        (Name              => "DHT11         ",
         CPP_Sizeof        => Arduino.DHT11.Sizeof_DHT11,
         Ada_Size_In_Bytes => Arduino.DHT11.DHT11_Size_In_Bytes),
      VR =>
        (Name              => "VR            ",
         CPP_Sizeof        => Arduino.Voice_Recognition.Sizeof_VR,
         Ada_Size_In_Bytes => Arduino.Voice_Recognition.VR_Size_In_Bytes),
      CCS811 =>
        (Name              => "CCS811        ",
         CPP_Sizeof        => Arduino.CCS811.Sizeof_CCS811,
         Ada_Size_In_Bytes => Arduino.CCS811.CCS811_Size_In_Bytes),
      IRTherm =>
        (Name              => "IRTherm       ",
         CPP_Sizeof        => Arduino.IR_Therm_MLX90614.Sizeof_IRTherm,
         Ada_Size_In_Bytes => Arduino.IR_Therm_MLX90614.IRTherm_Size_In_Bytes));

begin
   DIO.Put_Line ("Types_Size_Test");

   for T_Data of Types_Data loop
      DIO.Put (T_Data.Name);
      DIO.Put ("-> ");
      if T_Data.CPP_Sizeof /= -1 then
         DIO.Put ("CPP:");
         DIO.Put (T_Data.CPP_Sizeof);
         DIO.Put (" Ada:");
         DIO.Put (T_Data.Ada_Size_In_Bytes);
         Tests_Reports.Assert (T_Data.CPP_Sizeof = T_Data.Ada_Size_In_Bytes);
      else
         DIO.Put ("Not included");
      end if;
      DIO.New_Line;
   end loop;

   Tests_Reports.Test_OK;
end Arduino_Types_Size_Test;
