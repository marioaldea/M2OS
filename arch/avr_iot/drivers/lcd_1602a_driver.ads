with Interfaces.C; use Interfaces.C;
with System;
with M2.Drivers;

package LCD_1602A_Driver is
   --  pragma Pure;
   pragma Preelaborate;

   procedure Open (Mode : M2.Drivers.File_Access_Mode);

   function Write (Buffer_Ptr : System.Address;
                   Bytes      : size_t)
                   return int;

end LCD_1602A_Driver;
