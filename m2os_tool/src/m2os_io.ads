--  This package contains all the routines for handling input and
--  output operations.

with GNAT.OS_Lib; use GNAT;  -- File operation utilities

package M2OS_IO is

   type File_Type is private;

   ------------------------------
   -- File handling operations --
   ------------------------------

   function  Create_File (Filename : String) return File_Type;
   procedure Close_File  (File : File_Type);
   function  Backup_File (Filename : String) return Boolean;

   ------------------------------
   -- File's output operations --
   ------------------------------

   procedure Set_Output (File : File_Type);
   procedure Write_Indentation;
   procedure Remove_Indentation;
   procedure Write_Line (Sentence : String);
   procedure New_Line;


   ----------
   -- Trim --
   ----------

   function Trim (Sentence: String) return String;
   --  Trim leading and trailing spaces in the input String

private
   type File_Type is new OS_Lib.File_Descriptor;


end M2OS_IO;
