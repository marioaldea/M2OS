----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on ev3_touch.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- setup for this example:
-- attach EV3 Touch Sensor to Port BAS1
----------------------------------------------------------------------------

with Interfaces.C;

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield.EV3Touch;
with Arduino.EVShield;
with Interfaces.C.Extensions;


procedure EvShield_Touch_Sensor is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, Interfaces.C.Extensions.Bool;

   -- Program variables
   Time : Arduino.Time_MS;
   Touch : Interfaces.C.Extensions.Bool;
   Count : Interfaces.C.Int;

   package Touch_Sensor is new Arduino.EVShield.EV3Touch.Touch (Arduino.EVShield.SH_BAS1);

begin
   DIO.Put_Line ("--------------------------------------");
   DIO.Put_Line ("Starting EV3 Touch Sensor Test program");
   DIO.Put_Line ("--------------------------------------");

   -- initialize the sensor, and tell where it is connected.
   DIO.Put_Line ("setup done");
   DIO.Put_Line ("press the touch sensor to see changes in the values");

   -- Wait until user presses GO button to continue the program
   while not Arduino.EVShield.Get_Button_State(Arduino.EVShield.BTN_GO) loop
      Time := Arduino.Millis;
      if Time mod 1000 < 3 then
         DIO.Put_Line (" Press GO button to continue");
      end if;
   end loop;

   loop
      -- get the reading(s) from sensor
      Touch := Touch_Sensor.Is_Pressed;
      Count := Touch_Sensor.Get_Bump_Count;

      -- print the sensor value(s)
      if Touch then
         DIO.Put_Line ("Button is pressed!");
      end if;
      DIO.Put ("Count: "); DIO.Put_Line (Count'Img);

      -- wait for some time
      Arduino.C_Delay (1000);

   end loop;
end EvShield_Touch_Sensor;
