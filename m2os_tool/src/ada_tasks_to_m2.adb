with Ada.Text_IO;                use Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Directories;
with Ada.Exceptions;

with Libadalang.Analysis;  use Libadalang.Analysis;
with Libadalang.Common;    use Libadalang.Common;

with GNATCOLL.Utils;
with GNATCOLL.Projects;

with M2OS_Utils; use M2OS_Utils;
with M2OS_Processing;

with LAL_Extensions;

procedure Ada_Tasks_To_M2 is
   Ctx         : Analysis_Context;
   Unit        : Analysis_Unit;
   Provider    : Unit_Provider_Reference;
   Src_Files   : M2OS_Utils.String_Vectors.Vector;
   Project     : Unbounded_String;
   The_Project : GNATCOLL.Projects.Project_Type;

   Feature_Not_Implemented : exception;

   -----------------------------
   -- Parse_Console_Arguments --
   -----------------------------

   function Parse_Console_Arguments return Unbounded_String is
      Project_File  : Unbounded_String := Null_Unbounded_String;
   begin
      --  Decode all command-line arguments, but ignore them unless refer to a gpr file
      for I in 1 .. Ada.Command_Line.Argument_Count loop
         if GNATCOLL.Utils.Ends_With (Ada.Command_Line.Argument (I), ".gpr") then
            Project_File := +Ada.Command_Line.Argument (I);
         end if;
      end loop;

      return Project_File;

   end Parse_Console_Arguments;

   ------------------------------------
   -- Check_Features_Not_Implemented --
   ------------------------------------

   procedure Check_Features_Not_Implemented (Root_Node : Ada_Node'Class) is
   begin
      if LAL_Extensions.Contains_Kind (Root_Node, Ada_Task_Type_Decl) then
         raise Feature_Not_Implemented with "Task types not supported in this tool version";
      end if;
   end Check_Features_Not_Implemented;

begin
   -- Parse arguments
   Project := Parse_Console_Arguments;
   if Project = Null_Unbounded_String then
      Load_Default_Project (Provider => Provider, Files => Src_Files);
   else
      Load_Project (Name => +Project, Provider => Provider, Files => Src_Files, The_Project => The_Project);
   end if;

   -- Create context for analysis
   Ctx := Create_Context (Unit_Provider => Provider);

   -- Loop through all the units
   for F of Src_Files loop
      -- packages starting with 'adax' should not be processed
      if not GNATCOLL.Utils.Starts_With (Ada.Directories.Simple_Name (+F), "adax_dispatching_stack_sharing") then
         --if (Ada.Directories.Simple_Name (+F) (1 .. 4) /= "adax") then
         Put_Line ("Processing file: " & Ada.Directories.Simple_Name (+F));
         Unit := Get_From_File (Context  => Ctx, Filename => +F);
         M2OS_Utils.Show_Diagnostics (Unit);

         Check_Features_Not_Implemented (Unit.Root);

         -- Ada main packages has a different processing
         if Is_Ada_Main (+F, The_Project) then
            M2os_Processing.Process_Main_Procedure (Root_Node => Unit.Root,
                                                    Trace     => False);
            -- Regular Ada packages
         else
            if LAL_Extensions.Contains_Kind (Unit.Root, Ada_Task_Body) then
               M2os_Processing.Process_Package_Body (Root_Node => Unit.Root,
                                                     Trace     => False);
               -- If Task_Decl is in adb is handled in Process_Package_Body, now handle it for ads files
            elsif LAL_Extensions.Contains_Kind (Unit.Root, Ada_Single_Task_Decl) then -- or LAL_Extensions.Contains_Kind (Unit.Root, Ada_Task_Type_Decl) ) then
               M2os_Processing.Process_Package_Spec (Root_Node => Unit.Root,
                                                     Trace     => False);
            end if;
         end if;
      end if;
   end loop;

   New_Line;
   Put_Line ("Done.");

exception
   when E : others =>
      Put_Line (Ada.Exceptions.Exception_Information (E));
end Ada_Tasks_To_M2;
