----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of using Arduino.DHT, Arduino.CCS811 and Arduino.LCD_I2C packages.
--
--  Reads temperature and humidity from a DHT11 sensor, CO2 concentration (ppm)
--  and Total Volatile Organic Compounds (ppb) using the "KS0457 keyestudio
--  CCS811 Carbon Dioxide Air Quality Sensor" and displays them on an LCD.
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |           DATA|-----------------|D4                   |
--     | DHT11      VDD|-----------------|VDD      Arduino Uno |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 |                     |
--                                       |                     |
--     -----------------                 |                     |
--     |               |                 |                     |
--     |            SCL|-----------------|A0                   |
--     |            SDA|-----------------|A1                   |
--     | LCD        VDD|-----------------|VDD                  |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 |                     |
--                                       |                     |
--     -----------------                 |                     |
--     |               |                 |                     |
--     |           Wake|-----------------|GND                  |
--     | KS0457     SCL|-----------------|A5                   |
--     | (CCS811)   SDA|-----------------|A4                   |
--     |            VCC|-----------------|5V                   |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--
--  Application is made of 5 tasks:
--   - LCD_Blink_Task         (T:    600ms, Prio: 6)
--   - LCD_Task               (T:  4_000ms, Prio: 5)
--   - CCS811_Task            (T:  5_500ms, Prio: 3)
--   - DHT11_Task             (T:  5_500ms, Prio: 3)
--   - Update_CCS811_Env_Task (T: 20_000ms, Prio: 1)
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

with CO2_Sensor.LCD_Tasks;
with CO2_Sensor.DHT11_Tasks;
with CO2_Sensor.CCS811_Task;

procedure CO2_Sensor.Main is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put_Line ("CO2 sensor and LCD");   
end CO2_Sensor.Main;
