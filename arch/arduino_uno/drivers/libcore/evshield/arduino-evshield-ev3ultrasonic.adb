----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of evs_ev3ultrasoonic.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with EVShieldUART_h;
with SHDefines_h;
with Interfaces.C.Extensions;
with stdint_h;
with EVs_EV3Ultrasonic_h;
with EVShieldUART_h;

package body Arduino.EVShield.EV3Ultrasonic is

   package body Ultrasonic is
      Ultrasonic_Sensor : aliased EVs_EV3Ultrasonic_h.Class_EVs_EV3Ultrasonic.EVs_EV3Ultrasonic := EVs_EV3Ultrasonic_h.Class_EVs_EV3Ultrasonic.New_EV3Ultrasonic;

      Aux : Interfaces.C.Extensions.bool:= EVs_EV3Ultrasonic_h.Class_EVs_EV3Ultrasonic.init(Ultrasonic_Sensor'Access, Arduino.EVShield.EVShield_Obj'Access, Bp);

      function Get_Dist return float is
      begin
         return EVs_EV3Ultrasonic_h.Class_EVs_EV3Ultrasonic.getDist(Ultrasonic_Sensor'Access);
      end Get_Dist;

      function Detect return stdint_h.uint8_t is
      begin
         return EVs_EV3Ultrasonic_h.Class_EVs_EV3Ultrasonic.detect(Ultrasonic_Sensor'Access);
      end Detect;

      function Set_Mode ( New_Mode : char ) return stdint_h.uint8_t is
      begin
         return EVShieldUART_h.Class_EVShieldUART.setMode(Ultrasonic_Sensor.parent'Access, New_Mode);
      end Set_Mode;

   end Ultrasonic;

end Arduino.EVShield.EV3Ultrasonic;
