------------------------------------------------------------------------------
--                                                                          --
--                         GNAT RUN-TIME COMPONENTS                         --
--                                                                          --
--          A D A . R E A L _ T I M E . T I M I N G _ E V E N T S           --
--                                                                          --
--                                   Body                                   --
--                                                                          --
--  See Ada Reference Manual D.15 Timing Events                             --
------------------------------------------------------------------------------
--  M2OS version of this package.

package body Ada.Real_Time.Timing_Events is
   pragma Compile_Time_Error
     (Timing_Event'Size /= System.Timing_Event_Size_In_Bytes * 8,
      "Unexpected size of Ada.Real_Time.Timing_Events.Timing_Event");

   -----------------
   -- Set_Handler --
   -----------------

   --  Not allowed in Ravenscar (pragma Restrictions No_Relative_Delay)

   procedure Set_Handler (Event   : in out Timing_Event;
                          In_Time : Time_Span;
                          Handler : Timing_Event_Handler) is
      pragma Unreferenced (Event, In_Time, Handler);
   begin
      raise Program_Error;
   end Set_Handler;

end Ada.Real_Time.Timing_Events;
