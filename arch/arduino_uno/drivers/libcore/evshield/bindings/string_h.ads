pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with System;
with stddef_h;
with Interfaces.C.Strings;

package string_h is

   function ffs (uu_val : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:125
   pragma Import (C, ffs, "ffs");

   function ffsl (uu_val : long) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:131
   pragma Import (C, ffsl, "ffsl");

   function ffsll (uu_val : Long_Long_Integer) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:137
   pragma Import (C, ffsll, "ffsll");

   function memccpy
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : int;
      arg4 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:150
   pragma Import (C, memccpy, "memccpy");

   function memchr
     (arg1 : System.Address;
      arg2 : int;
      arg3 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:162
   pragma Import (C, memchr, "memchr");

   function memcmp
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:180
   pragma Import (C, memcmp, "memcmp");

   function memcpy
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:191
   pragma Import (C, memcpy, "memcpy");

   function memmem
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : System.Address;
      arg4 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:203
   pragma Import (C, memmem, "memmem");

   function memmove
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:213
   pragma Import (C, memmove, "memmove");

   function memrchr
     (arg1 : System.Address;
      arg2 : int;
      arg3 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:225
   pragma Import (C, memrchr, "memrchr");

   function memset
     (arg1 : System.Address;
      arg2 : int;
      arg3 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:235
   pragma Import (C, memset, "memset");

   function strcat (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:248
   pragma Import (C, strcat, "strcat");

   function strchr (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:262
   pragma Import (C, strchr, "strchr");

   function strchrnul (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:274
   pragma Import (C, strchrnul, "strchrnul");

   function strcmp (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:287
   pragma Import (C, strcmp, "strcmp");

   function strcpy (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:305
   pragma Import (C, strcpy, "strcpy");

   function strcasecmp (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:320
   pragma Import (C, strcasecmp, "strcasecmp");

   function strcasestr (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:333
   pragma Import (C, strcasestr, "strcasestr");

   function strcspn (uu_s : Interfaces.C.Strings.chars_ptr; uu_reject : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:344
   pragma Import (C, strcspn, "strcspn");

   function strdup (s1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:364
   pragma Import (C, strdup, "strdup");

   function strlcat
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:377
   pragma Import (C, strlcat, "strlcat");

   function strlcpy
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:388
   pragma Import (C, strlcpy, "strlcpy");

   function strlen (arg1 : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:399
   pragma Import (C, strlen, "strlen");

   function strlwr (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:411
   pragma Import (C, strlwr, "strlwr");

   function strncat
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:422
   pragma Import (C, strncat, "strncat");

   function strncmp
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:434
   pragma Import (C, strncmp, "strncmp");

   function strncpy
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:449
   pragma Import (C, strncpy, "strncpy");

   function strncasecmp
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:464
   pragma Import (C, strncasecmp, "strncasecmp");

   function strnlen (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:478
   pragma Import (C, strnlen, "strnlen");

   function strpbrk (uu_s : Interfaces.C.Strings.chars_ptr; uu_accept : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:491
   pragma Import (C, strpbrk, "strpbrk");

   function strrchr (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:505
   pragma Import (C, strrchr, "strrchr");

   function strrev (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:515
   pragma Import (C, strrev, "strrev");

   function strsep (arg1 : System.Address; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:533
   pragma Import (C, strsep, "strsep");

   function strspn (uu_s : Interfaces.C.Strings.chars_ptr; uu_accept : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:544
   pragma Import (C, strspn, "strspn");

   function strstr (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:557
   pragma Import (C, strstr, "strstr");

   function strtok (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:576
   pragma Import (C, strtok, "strtok");

   function strtok_r
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : System.Address) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:593
   pragma Import (C, strtok_r, "strtok_r");

   function strupr (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:606
   pragma Import (C, strupr, "strupr");

   function strcoll (s1 : Interfaces.C.Strings.chars_ptr; s2 : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:610
   pragma Import (C, strcoll, "strcoll");

   function strerror (errnum : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:611
   pragma Import (C, strerror, "strerror");

   function strxfrm
     (dest : Interfaces.C.Strings.chars_ptr;
      src : Interfaces.C.Strings.chars_ptr;
      n : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/string.h:612
   pragma Import (C, strxfrm, "strxfrm");

end string_h;
