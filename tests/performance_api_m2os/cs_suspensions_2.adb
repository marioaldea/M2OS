pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;
with AdaX_Dispatching_Stack_Sharing;

with M2.Direct_IO;
with M2.HAL;

with Measurements_HiRes;

package body CS_Suspensions_2 is
   package DIO renames M2.Direct_IO;
   package Measurements renames Measurements_HiRes;

   --------
   -- PO --
   --------

   protected body PO is

      entry Wait when Is_Open is
      begin
         --DIO.Put ("-PO.Wait-");
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         --DIO.Put ("-PO.Open-");
         Is_Open := True;
      end Open;
   end PO;

   --------------
   -- PO_Param --
   --------------

   protected body PO_Param is

      entry Wait (Data_Out : access Integer) when Is_Open is
      begin
         --DIO.Put ("-PO.Wait-");

         Data_Out.all := Counter;
         --DIO.Put (Data_Out.All);
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         --DIO.Put ("-PO.Open-");
         --DIO.Put (Counter);

         Counter := Counter + 1;
         Is_Open := True;
      end Open;
   end PO_Param;

   ------------
   -- Task_2 --
   ------------

   procedure Task_2_Body is
   begin

      Measurements.End_Measurement;
--        DIO.Put ("-Task_2-");

      Loop_Counter := Loop_Counter + 1;

      case Suspension_Primitive is
         when Suspension_Object =>
            Ada.Synchronous_Task_Control.Set_True (SO);
         when PO_Entry =>
            PO.Open;
         when PO_Entry_Param =>
            PO_Param.Open;
      end case;

      System.OS_Interface.Running_Thread_Ends_Job_Without_Blocking;
   end Task_2_Body;

   ------------
   -- Task_2 --
   ------------

   Task_2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => null,
      Body_Ac  => Task_2_Body'Access,
      Priority => Task_2_Prio);

end CS_Suspensions_2;
