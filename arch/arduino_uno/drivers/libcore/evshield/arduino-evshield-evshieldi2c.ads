----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of EVSHieldI2C.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with EVShieldI2C_h;
with stdint_h;
with Interfaces.C.Extensions;
with SHDefines_h;
with Interfaces.C.Strings;
with Arduino;
with Arduino.EVShield;
with EVShieldI2C_h;

package Arduino.EVShield.EVShieldI2C is
   -- Methods for EVShield Bank A
   package Bank_A is
      -- get the firmware version of the device
      function Get_Firmware_Version return Interfaces.C.Strings.chars_ptr;

      -- get the name of the vendor of the device
      function Get_Vendor_ID return Interfaces.C.Strings.chars_ptr;

      -- get the name of the device
      function Get_Device_ID return Interfaces.C.Strings.chars_ptr;

      -- get the features the device is capable of; only supported by some devices
      function Get_Feature_Set return Interfaces.C.Strings.chars_ptr;

      -- get the error code of last i2c operation
      function Get_Error_Code return stdint_h.uint8_t;

      -- check the i2c address for this device
      function Check_Address return Extensions.bool;

      -- set the i2c address for this device
      -- @param address new device address.
      function Set_Address (Address : stdint_h.uint8_t) return Extensions.bool;

   end Bank_A;

   -- Methods for EVShield Bank B
   package Bank_B is
      -- get the firmware version of the device
      function Get_Firmware_Version return Interfaces.C.Strings.chars_ptr;

      -- get the name of the vendor of the device
      function Get_Vendor_ID return Interfaces.C.Strings.chars_ptr;

      -- get the name of the device
      function Get_Device_ID return Interfaces.C.Strings.chars_ptr;

      -- get the features the device is capable of; only supported by some devices
      function Get_Feature_Set return Interfaces.C.Strings.chars_ptr;

      -- get the error code of last i2c operation
      function Get_Error_Code return stdint_h.uint8_t;

      -- check the i2c address for this device
      function Check_Address return Extensions.bool;

      -- set the i2c address for this device
      -- @param address new device address.
      function Set_Address (address : stdint_h.uint8_t) return Extensions.bool;

   end Bank_B;

end Arduino.EVShield.EVShieldI2C;
