----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

with M2.Direct_IO;
with M2.HAL;
with M2.Kernel.API;

with Maqueen;

package body Maqueen_Task_Lights is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   ----------------
   -- Other_Task --
   ----------------

   Period : constant RT.Time_Span := RT.Milliseconds (5_000);

   Next_Time : RT.Time;
   Loop_Count : Natural := 0;

   procedure Other_Task_Init is
      LED_Izquierdo : Maqueen.Light := Maqueen.Left_Light;
      LED_Derecho : Maqueen.Light := Maqueen.Right_Light;
   begin

      --Turn On Lights
      Maqueen.Turn_On_Light(LED_Izquierdo);
      Maqueen.Turn_On_Light(LED_Derecho);

      Next_Time := Ada.Real_Time.Clock;
   end Other_Task_Init;


   procedure Other_Task_Body is
      LED_Izquierdo : Maqueen.Light := Maqueen.Left_Light;
      LED_Derecho : Maqueen.Light := Maqueen.Right_Light;
   begin
      --  Check if pin 1 is high
      if Maqueen.Right_Light_Status and Maqueen.Left_Light_Status then
         --  Turn OFF lights
         Maqueen.Turn_Off_Light(LED_Izquierdo);
         Maqueen.Turn_Off_Light(LED_Derecho);
      else
         --  Turn ON lights
         Maqueen.Turn_On_Light(LED_Izquierdo);
         Maqueen.Turn_On_Light(LED_Derecho);
      end if;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Other_Task_Body;

   Other_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Other_Task_Init'Access,
      Body_Ac  => Other_Task_Body'Access,
      Priority => 4);

end Maqueen_Task_Lights;
