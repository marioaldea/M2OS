//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Test suspension objects.
#include <pthread.h>
#include <stdio.h>
#include <suspension.h>
#include "../reports/tests_reports.h"

THREAD_POOL(2);

suspension_t so1 = SUSPENSION_INITIALIZER;
suspension_t so2 = SUSPENSION_INITIALIZER;

int counter = 0;
int counter1 = 0;
int counter2 = 0;

//****************//
//  thread_body1  //
//****************//
struct timespec next_activation_time1;

void * thread_body1(void *arg) {
  puts("Thread 1\n");
  counter1++;
  
  switch (counter1) {
    case 1:      
      // first activation. so1 is closed  
      tests_reports__assert(counter == 0); 
      counter++;
  
      puts(" Th1 gets suspended in so1\n");
      suspension_suspend_until_true(&so1);
      break;
    case 2:  
      // second activation. Ready after suspension in so1
      tests_reports__assert(counter == 3);  
      counter++;
  
      puts(" Th1 ready after suspended in so1\n");
      tests_reports__test_ok();
      break;
    default:
      // should never be reached
      tests_reports__assert(false);
  }
  
      return NULL;
  }

  //****************//
  //  thread_body2  //
  //****************//
  void * thread_body2(void *arg) {
    puts("Thread 2\n");
    counter2++;
  
    if (counter2 == 1) {
      // first activation. so2 is open
  
      tests_reports__assert(counter == 1); 
      counter++;
  
      puts(" Th2 does not suspend in so2\n");
      suspension_suspend_until_true(&so2); 
    }
  
    // second activation. so2 is closed
    tests_reports__assert(counter2 == 2);
    tests_reports__assert(counter == 2);  
    counter++;
     
    puts(" Th2 opens so1\n");
    suspension_set_true(&so1);
  
    puts(" Th2 gets suspended in so2\n");
    suspension_suspend_until_true(&so2);
  
    // should never be reached
    tests_reports__assert(false);
  
    return NULL;
  }  
    
  //**********//
  //   main   //
  //**********//
  int main (int argc, char **argv) {
    m2osinit();
    puts("Main\n");
  
    puts("SUSP_OBJ_SIZE="); putint(SUSP_OBJ_SIZE);
    puts("  m2__kernel__api__so_size_in_bytes="); putint(m2__kernel__api__so_size_in_bytes);
    puts("\n");
  
    check_posix_api();
    
    pthread_t th1;
    pthread_t th2;
    pthread_attr_t attr;
    struct sched_param sch_param;

    pthread_attr_init(&attr);
  
    sch_param.sched_priority = 4;
    pthread_attr_setschedparam(&attr, &sch_param);    
    pthread_create(&th1, &attr, thread_body1, NULL);   
  
    sch_param.sched_priority = 3;
    pthread_attr_setschedparam(&attr, &sch_param);     
    pthread_create(&th2, &attr, thread_body2, NULL);
  
    // open so2
    suspension_set_true(&so2);
  
    // check so1 is closed and so2 open
    tests_reports__assert(!suspension_current_state(&so1));
    tests_reports__assert(suspension_current_state(&so2));

    puts("Main ends\n");

    return 0;
  }
