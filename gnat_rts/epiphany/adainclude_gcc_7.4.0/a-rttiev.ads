------------------------------------------------------------------------------
--                                                                          --
--                         GNAT RUN-TIME COMPONENTS                         --
--                                                                          --
--          A D A . R E A L _ T I M E . T I M I N G _ E V E N T S           --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
--  See Ada Reference Manual D.15 Timing Events                             --
------------------------------------------------------------------------------
--  M2OS version of this package.

with System;
with Interfaces.C;

package Ada.Real_Time.Timing_Events is

   type Timing_Event is limited private; --  NO tagged support in M2 just now
   --  type Timing_Event is tagged limited private;

   type Timing_Event_Handler is
     access protected procedure (Event : in out Timing_Event);

   procedure Set_Handler (Event   : in out Timing_Event;
                          At_Time : Time;
                          Handler : Timing_Event_Handler)
     with Import => True, Convention => C;
   pragma Import_Procedure
     (Set_Handler, "m2__kernel__timing_events__set_handler",
      Parameter_Types => (Timing_Event, Time, Timing_Event_Handler),
      Mechanism => (Reference, Value, Value));

   procedure Set_Handler (Event   : in out Timing_Event;
                          In_Time : Time_Span;
                          Handler : Timing_Event_Handler);
   --  Not allowed in Ravenscar (pragma Restrictions No_Relative_Delay)
--       with Import => True, Convention => C;
--     pragma Import_Procedure
--       (Set_Handler, "m2__kernel__timing_events__set_handler_relative",
--        Parameter_Types => (Timing_Event, Time_Span, Timing_Event_Handler),
--        Mechanism => (Reference, Value, Value));

   function Current_Handler (Event : Timing_Event)
                             return Timing_Event_Handler
     with Import => True, Convention => C,
     External_Name => "m2__kernel__timing_events__current_handler";

   pragma Warnings (Off);
   procedure Cancel_Handler (Event     : in out Timing_Event;
                             Cancelled : out Boolean)
     with Import => True, Convention => C;
   pragma Warnings (On);
   pragma Import_Procedure
     (Cancel_Handler, "m2__kernel__timing_events__cancel_handler",
      Parameter_Types => (Timing_Event, Boolean),
      Mechanism => (Reference));

   function Time_Of_Event (Event : Timing_Event) return Time
     with Import => True, Convention => C,
     External_Name => "m2__kernel__timing_events__time_of_event";

private
   type Timing_Event_Stuffing is array (1 .. System.Timing_Event_Size_In_Bytes)
     of Interfaces.Unsigned_8;
   for Timing_Event_Stuffing'Size use System.Timing_Event_Size_In_Bytes * 8;

   type Timing_Event is limited record
      Stuffing : Timing_Event_Stuffing := (others => 0);
   end record;

end Ada.Real_Time.Timing_Events;
