/****************************************************************************
*                                  M2OS
*
*                           Copyright (C) 2019
*                    Universidad de Cantabria, SPAIN
*
*  Authors: David Garcia Villaescusa (garciavd@unican.es)
*           Mario Aldea Rivas (aldeam@unican.es)
*
*  This file is part of M2OS.
*
*  M2OS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  M2OS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/


.section .text

# void m2_hal_regs_jump_to_context(uint32 *new_task_body_address,
#                                  uint32 *stack_top)
#     r0=new_task_body_address      r1=stack_top

.globl m2_hal_regs_jump_to_context

m2_hal_regs_jump_to_context:
    mov sp, r1
    jr r0

.globl m2_hal_regs_get_sp_reg

m2_hal_regs_get_sp_reg:
    mov r0, sp
    jr lr

# void m2_hal_regs_push_status()
# XXX not implemented
.globl m2_hal_regs_push_status
m2_hal_regs_push_status:
    #push {r4-r7}
    jr lr

# void m2_hal_regs_pop_status()
# XXX not implemented
.globl m2_hal_regs_pop_status
m2_hal_regs_pop_status:
    #pop {r4-r7}
    jr lr
