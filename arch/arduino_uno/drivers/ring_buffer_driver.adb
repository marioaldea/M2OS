----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Unchecked_Conversion;

package body Ring_Buffer_Driver is

   use Interfaces;
   use Interfaces.C;

   -- Buffer type

   type Buffer is array (size_t) of Character;
   type Buffer_Ac is access Buffer;
   function To_Buffer_Ac is
     new Ada.Unchecked_Conversion (System.Address, Buffer_Ac);

   -----------------
   -- Ring Buffer --
   -----------------

   type Buf_Index is mod 2**7;
   Ring_Buffer : array (Buf_Index) of Character;
   Ring_Buffer_Cursor : Buf_Index;

   ----------
   -- Open --
   ----------

   procedure Open (Mode : M2.Drivers.File_Access_Mode) is
   begin
      pragma Compile_Time_Error (Character'Size /= unsigned_8'Size,
                                   "Character'size /= 8");
      pragma Compile_Time_Warning (True, "Using Ring Buffer Driver");

      for I in Buf_Index'Range loop
         Ring_Buffer (I) := ' ';
      end loop;
      Ring_Buffer_Cursor := Buf_Index'First;
      Ring_Buffer (Buf_Index'First) := '#';
   end Open;

   -----------
   -- Write --
   -----------

   function Write (Buffer_Ptr : System.Address;
                   Bytes      : size_t)
                   return int is
      Buff_Ac : Buffer_Ac := To_Buffer_Ac (Buffer_Ptr);
   begin
      for I in 0 .. Bytes-1 loop
         Ring_Buffer (Ring_Buffer_Cursor) := Buff_Ac (I);
         Ring_Buffer_Cursor := Ring_Buffer_Cursor + 1;
      end loop;
      Ring_Buffer (Ring_Buffer_Cursor) := '#';

      return int (Bytes);
   end Write;

end Ring_Buffer_Driver;
