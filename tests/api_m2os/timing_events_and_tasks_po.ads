----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with System;

with Ada.Real_Time.Timing_Events;
with Ada.Synchronous_Task_Control;

package Timing_Events_And_Tasks_PO is
   package RT renames Ada.Real_Time;
   package TE renames Ada.Real_Time.Timing_Events;

   Step : Natural := 0 with Volatile;

   Susp_Object : Ada.Synchronous_Task_Control.Suspension_Object;
   Susp_Object2 : Ada.Synchronous_Task_Control.Suspension_Object;

   protected PO with Priority => System.Interrupt_Priority'Last is

      procedure Start1 (First_Activation : RT.Time;
                        Period : RT.Time_Span);

      procedure PO_Handler1 (Event : in out TE.Timing_Event);

      procedure Start2 (First_Activation : RT.Time;
                        Period : RT.Time_Span);

      procedure PO_Handler2 (Event : in out TE.Timing_Event);

      entry Wait2;

   private
      Activation_Time_1 : RT.Time;
      Period_1 : RT.Time_Span;
      Event1 : TE.Timing_Event;

      Activation_Time_2 : RT.Time;
      Period_2 : RT.Time_Span;
      Event2 : TE.Timing_Event;

      Open : Boolean := False;
   end PO;

end Timing_Events_And_Tasks_PO;
