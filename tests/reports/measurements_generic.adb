with M2.Direct_IO;
with Interfaces;
use Interfaces;

package body Measurements_Generic is
 
   package DIO renames M2.Direct_IO;

   ------------------
   -- Measurements --
   ------------------

   Num_Of_Measurements : Time_Long := 0;

   Total_Measurements_Time : Time_Long := 0;

   Measurement_Start_Time : Time := 0;
   Measurement_End_Time : Time := 0;

   Measure_Time_Offset : Time_Long := 0;

   Initialized : Boolean := False;

   ----------
   -- Init --
   ----------

   procedure Init is
      Num_Of_Measures : constant := 1_000;
   begin
      pragma Assert (not Initialized);

      Initialized := True;

      --  Get Measure_Time_Offset
      
      Num_Of_Measurements := 0;
      Total_Measurements_Time := 0;
      for I in 1 .. Num_Of_Measures loop
         Start_Measurement;
         End_Measurement;
      end loop;

      Measure_Time_Offset := Total_Measurements_Time / Num_Of_Measures;

      --  Reset values to be ready for the "real" measurements

      Num_Of_Measurements := 0;
      Total_Measurements_Time := 0;
   end Init;

   -----------
   -- Reset --
   -----------

   procedure Reset is
   begin
      pragma Assert (Initialized);

      Num_Of_Measurements := 0;
      Total_Measurements_Time := 0;
   end Reset;


   -----------------------
   -- Start_Measurement --
   -----------------------

   procedure Start_Measurement is
   begin
      pragma Assert (Initialized);

      Measurement_Start_Time := Get_Time;
   end Start_Measurement;

   ---------------------
   -- End_Measurement --
   ---------------------

   procedure End_Measurement is
      T : Time_Long;
   begin
      pragma Assert (Initialized);
      
      if Upward then
         T := Time_Long (Get_Time - Measurement_Start_Time);
      else
         T := Time_Long (Measurement_Start_Time - Get_Time);
      end if;   

      Total_Measurements_Time := Total_Measurements_Time + T;

      Num_Of_Measurements := Num_Of_Measurements + 1;
   end End_Measurement;

   ----------------------
   -- Measurement_Time --
   ----------------------

   function Measurement_Time return Time_Long is
   begin
      return Total_Measurements_Time;
   end Measurement_Time;

   ---------------------
   -- Measurement_Avg --
   ---------------------

   function Measurement_Avg return Time is
   begin
      return Time
        ((Total_Measurements_Time -
             Measure_Time_Offset * Num_Of_Measurements)
         / Num_Of_Measurements);
   end Measurement_Avg;
   
   -----------------------------------
   -- Measurement_Avg_In_Time_Units --
   -----------------------------------
   
   function Measurement_Avg_In_Time_Units return Time_Units is
   begin
      return To_Time_Units (Time_Long (Measurement_Avg));
   end Measurement_Avg_In_Time_Units;

   -------------------
   -- To_Time_Units --
   -------------------

   function To_Time_Units (T : Time_Long) return Time_Units is
   begin
      -- ticks * xs/s / (tick/s) = ticks * xs/s * s/tick = xs
      return Time_Units (Unsigned_64 (T) * Unsigned_64 (Time_Unit_Hz) /
                           Unsigned_64 (Time_Hz));
   end To_Time_Units;

   --------------------------
   --  Print_Measures_Data --
   --------------------------

   procedure Print_Measures_Data is
   begin
      pragma Assert (Initialized);

      DIO.Put ("Total:");
      DIO.Put (Interfaces.Integer_32 (Total_Measurements_Time));
      DIO.Put (" N:");
      DIO.Put (Integer (Num_Of_Measurements));
      DIO.Put (" Off:");
      DIO.Put (Integer (Measure_Time_Offset));
      DIO.Put (" Avg:");
      DIO.Put (Integer (Measurement_Avg));
      DIO.Put (" = ");
      DIO.Put (Integer (To_Time_Units (Time_long (Measurement_Avg))));
      DIO.PutChar (Time_Unit_Char);
      DIO.PutChar ('s');
      DIO.New_Line;
   end Print_Measures_Data;

   ------------
   -- Finish --
   ------------

   procedure Finish is
      pragma Suppress (Overflow_Check);
   begin
      pragma Assert (Initialized);

      Print_Measures_Data;

      Tests_Reports.Test_OK;
   end Finish;
   

end Measurements_Generic;
