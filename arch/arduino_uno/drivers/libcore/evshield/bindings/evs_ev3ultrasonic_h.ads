pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with EVShieldUART_h;
limited with EVShield_h;
with SHDefines_h;
with Interfaces.C.Extensions;
with stdint_h;

package EVs_EV3Ultrasonic_h is

   type MODE_Sonar is 
     (MODE_Sonar_CM,
      MODE_Sonar_Inches,
      MODE_Sonar_Presence);
   pragma Convention (C, MODE_Sonar);  -- EVs_EV3Ultrasonic.h:33

   package Class_EVs_EV3Ultrasonic is
      type EVs_EV3Ultrasonic is limited record
         parent : aliased EVShieldUART_h.Class_EVShieldUART.EVShieldUART;
      end record;
      pragma Import (CPP, EVs_EV3Ultrasonic);

      function New_EV3Ultrasonic return EVs_EV3Ultrasonic;  -- EVShieldUART.h:46
      pragma CPP_Constructor (New_EV3Ultrasonic, "_ZN12EVShieldUARTC1Ev");

      function init
        (this : access EVs_EV3Ultrasonic;
         shield : access EVShield_h.Class_EVShield.EVShield;
         bp : SHDefines_h.SH_BankPort) return Extensions.bool;  -- EVs_EV3Ultrasonic.h:42
      pragma Import (CPP, init, "_ZN17EVs_EV3Ultrasonic4initEP8EVShield11SH_BankPort");

      function getDist (this : access EVs_EV3Ultrasonic) return float;  -- EVs_EV3Ultrasonic.h:47
      pragma Import (CPP, getDist, "_ZN17EVs_EV3Ultrasonic7getDistEv");

      function detect (this : access EVs_EV3Ultrasonic) return stdint_h.uint8_t;  -- EVs_EV3Ultrasonic.h:50
      pragma Import (CPP, detect, "_ZN17EVs_EV3Ultrasonic6detectEv");
   end;
   use Class_EVs_EV3Ultrasonic;
end EVs_EV3Ultrasonic_h;
