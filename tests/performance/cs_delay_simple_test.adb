--  Measure the context switch when using a delay until with a
--  time in the past.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

pragma Warnings (Off);
with System.OS_Interface;
pragma Warnings (On);

with Ada.Real_Time;
with Ada.Synchronous_Task_Control;

with M2.Direct_IO;
with CS_Delay_Simple_2;

with Measurements;
with M2.HAL;

procedure CS_Delay_Simple_Test  is
   package DIO renames M2.Direct_IO;
   package P2 renames CS_Delay_Simple_2;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   Loop_Counter : Integer := 0;

   Next_Time, Start_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span := RT.Milliseconds (20);

begin
   DIO.Put ("-CS_Delay_Simple_Test-");
   DIO.New_Line;

   Measurements.Init (Measurements.Short);
   Start_Time := Ada.Real_Time.Clock;

   loop
      M2.HAL.Enable_Interrupts;
      --DIO.Put ("-Main-");

      if Loop_Counter /= 0 then
         Measurements.Assert (P2.Other_Task_Executed);
      end if;

      if Loop_Counter = P2.Num_Of_Loops then
         DIO.Put (" L1=");
         DIO.Put (Loop_Counter);

         DIO.Put (" CSTime(us)=");
         DIO.Put (Integer (Measurements.Measurement_Avg_Time_Us));
         DIO.New_Line;

         Measurements.Finish;
      end if;

      Loop_Counter := Loop_Counter + 1;
      P2.Other_Task_Executed := False;

      Next_Time := Start_Time + Period;

      Measurements.Start_Measurement;
      delay until Next_Time;
   end loop;

end CS_Delay_Simple_Test;
