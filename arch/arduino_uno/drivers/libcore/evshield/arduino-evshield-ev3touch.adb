----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of evs_ev3touch.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with EVShieldUART_h;
with SHDefines_h;
with Interfaces.C.Extensions;
with EVs_EV3Touch_h;

package body Arduino.EVShield.EV3Touch is

   package body Touch is
      use type EVs_EV3Touch_h.Class_EVs_EV3Touch.EVs_EV3Touch;
      Touch : aliased EVs_EV3Touch_h.Class_EVs_EV3Touch.EVs_EV3Touch := EVs_EV3Touch_h.Class_EVs_EV3Touch.New_EV3Touch;

      Aux : Extensions.bool:= EVs_EV3Touch_h.Class_EVs_EV3Touch.init(Touch'Access,Arduino.EVShield.EVShield_Obj'Access, Bp);

      function Is_Pressed return Extensions.bool is
      begin
         return EVs_EV3Touch_h.Class_EVs_EV3Touch.isPressed(Touch'Access);
      end Is_Pressed;

      function Get_Bump_Count return int is
      begin
         return EVs_EV3Touch_h.Class_EVs_EV3Touch.getBumpCount(Touch'Access);
      end Get_Bump_Count;

      function Reset_Bump_Count return Extensions.bool is
      begin
         return EVs_EV3Touch_h.Class_EVs_EV3Touch.resetBumpCount(Touch'Access);
      end Reset_Bump_Count;

   end Touch;

end Arduino.EVShield.EV3Touch;
