----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on print_bak_info.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- setup for this example:
-- None, only uses buttons and LEDs built in to the EVShield.
----------------------------------------------------------------------------

with Interfaces.C;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with SHDefines_h; use SHDefines_h;
with Interfaces.C.Extensions;
with Interfaces.C.Strings;
with Ada.Unchecked_Conversion;
with stdint_h;
with System;
with Arduino.EVShield.EVShieldI2C;

procedure evShield_print_info is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, Arduino.Boolean8,Interfaces.C.Extensions.bool;
   use type Interfaces.C.Strings.chars_ptr;

   -- Program variables
   time : Arduino.Time_MS;
   aux : Interfaces.C.Strings.chars_ptr;

   function Char_Ptr_To_Address is new Ada.Unchecked_Conversion (Interfaces.C.Strings.chars_ptr,  System.Address);

begin

   DIO.Put_Line ("--------------------------------------");
   DIO.Put_Line ("Starting EVShield info Test program");
   DIO.Put_Line ("--------------------------------------");

   DIO.Put_Line ("Press Shield buttons to see results");

   -- Wait until user presses GO button to continue the program
   while not Arduino.EVShield.Get_Button_State(SHDefines_h.BTN_GO ) loop
      time := Arduino.Millis;
      if time mod 1000 < 3 then
         DIO.Put_Line (" Press GO button to continue");
      end if;
   end loop;

   -- Bank A info
   DIO.Put_Line (" BANK A");
   aux := Arduino.EVShield.EVShieldI2C.Bank_A.Get_Firmware_Version;
   DIO.Put ("Firmware Version: "); DIO. Put_Null_Terminated( Char_Ptr_To_Address (aux)); DIO.New_Line;
   aux := Arduino.EVShield.EVShieldI2C.Bank_A.Get_Vendor_ID;
   DIO.Put ("Vendor ID: "); DIO.Put_Null_Terminated (Char_Ptr_To_Address (aux)); DIO.New_Line;
   aux := Arduino.EVShield.EVShieldI2C.Bank_A.Get_Device_ID;
   DIO.Put ("Device ID: "); DIO.Put_Null_Terminated (Char_Ptr_To_Address (aux)); DIO.New_Line;
   aux := Arduino.EVShield.EVShieldI2C.Bank_A.Get_Feature_Set;
   DIO.Put ("Feature Set: " ); DIO.Put_Null_Terminated (Char_Ptr_To_Address (aux)); DIO.New_Line;

   -- wait for some time
   Arduino.C_Delay (500);

   -- Bank B info
   DIO.Put_Line ("BANK B");
   aux := Arduino.EVShield.EVShieldI2C.Bank_B.Get_Firmware_Version;
   DIO.Put ("Firmware Version: "); DIO. Put_Null_Terminated( Char_Ptr_To_Address (aux)); DIO.New_Line;
   aux := Arduino.EVShield.EVShieldI2C.Bank_B.Get_Vendor_ID;
   DIO.Put ("Vendor ID: "); DIO.Put_Null_Terminated (Char_Ptr_To_Address (aux)); DIO.New_Line;
   aux := Arduino.EVShield.EVShieldI2C.Bank_B.Get_Device_ID;
   DIO.Put ("Device ID: "); DIO.Put_Null_Terminated (Char_Ptr_To_Address (aux)); DIO.New_Line;
   aux := Arduino.EVShield.EVShieldI2C.Bank_B.Get_Feature_Set;
   DIO.Put ("Feature Set: " ); DIO.Put_Null_Terminated (Char_Ptr_To_Address (aux)); DIO.New_Line;

   -- wait for some time
   Arduino.C_Delay (500);

end evShield_print_info;
