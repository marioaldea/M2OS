----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of using Ultrasonic Distance Sensor HC-SR04
--  See distance_sensor_hc_sr04_demo.ads for connection details.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;
with Interfaces;

with Arduino;
with M2.Direct_IO;
with M2.Kernel.API;

with Distance_Sensor_HC_SR04_Demo.ISR;

procedure Distance_Sensor_HC_SR04_Demo.Main is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type Arduino.Digital_Pin_In_Value, Arduino.Time_US, RT.Time;

   Nanoseconds_Per_Mm : constant := 2_914;
   --  1_000_000_000 us/s * 0.001 mm/m / 343,2 m/s

   Echo_Delay_Us : Arduino.Time_US;

   Falling_Edge_Time_Us : Arduino.Time_US;
   Time_Out_End : RT.Time;
   Time_Out_Ms : constant RT.Time_Span := RT.Milliseconds (10);
   Error : Boolean;

begin
   DIO.Put_Line ("Distance_Sensor_HC_SR04_Demo");

   Arduino.Pin_Mode (Pin  => Distance_Sensor_HC_SR04_Demo.Trigger_Pin,
                     Mode => Arduino.Output);
   Arduino.Pin_Mode (Pin  => Distance_Sensor_HC_SR04_Demo.Echo_Pin,
                     Mode => Arduino.Input);

   Arduino.Attach_Interrupt
     (Int_Num => Arduino.DigitalPinToInterrupt (Echo_Pin),
      ISR     => ISR.ISR_Digital_Pin'Access,
      Mode    => Arduino.Interrupt_When_Falling);

   loop
      --  Fire pulse

      DIO.Put ("Fire pulse ");
      Arduino.Digital_Write (Trigger_Pin, Arduino.Out_Low); -- start low
      Arduino.Delay_Microseconds (2);
      Arduino.Digital_Write (Trigger_Pin, Arduino.Out_High);  -- Pulse start
      Arduino.Delay_Microseconds (10);
      Arduino.Digital_Write (Trigger_Pin, Arduino.Out_Low);  -- Pulse end
      Falling_Edge_Time_Us := Arduino.Micros;

      Time_Out_End := RT.Clock + Time_Out_Ms;
      Error := False;

      --  Wait for pin to go high

      loop
         --DIO.Put (".");
         if RT.Clock > Time_Out_End then
            Error := True;
            DIO.Put_Line ("ERROR1");
         end if;
         exit when Error or
           Arduino.Digital_Read (Echo_Pin) = Arduino.In_High;
      end loop;

      --  Wait for ISR interrupt

      ISR.ISR_Executed := False;
      loop
         --DIO.Put ("-");
         if RT.Clock > Time_Out_End then
            Error := True;
            DIO.Put_Line ("ERROR2");
         end if;

         exit when Error or ISR.ISR_Executed;
      end loop;

      if not Error then
         DIO.Put ("t0:" & Falling_Edge_Time_Us'Img);
         DIO.Put ("  t1:" & ISR.Falling_Edge_Echo_Time_Us'Img);
         Echo_Delay_Us := ISR.Falling_Edge_Echo_Time_Us - Falling_Edge_Time_Us;
         DIO.Put ("  dif:" &  Echo_Delay_Us'Img);
         DIO.Put (" us   dist:");
         DIO.Put (Integer (Echo_Delay_Us * 1_000 / Nanoseconds_Per_Mm / 2));
         DIO.Put_Line (" mm");
      end if;

      Arduino.C_Delay (MS => 2_000);

   end loop;

end Distance_Sensor_HC_SR04_Demo.Main;
