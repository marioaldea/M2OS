----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Grove - Infrared Temperature Sensor:
--  Measure the Surrounding temperature around the sensor
--  and the temperature of the target which is in front of the sensor.
--   https://wiki.seeedstudio.com/Grove-Infrared_Temperature_Sensor/
--
----------------------------------------------------------------------------
package Arduino.IR_Temp_Sensor is

   procedure Initialize
     with Import, Convention => C,
     External_Name => "ir_temp__initialize";
   
   function Surronding_Temperature return Float
     with Import, Convention => C,
     External_Name => "ir_temp__measureSurTemp";
   
   function Object_Temperature return Float
     with Import, Convention => C,
     External_Name => "ir_temp__measureObjectTemp";
   
   function Get_Last_Offset return Float
     with Import, Convention => C,
     External_Name => "ir_temp__getLastOffset";
   
   procedure Adjust_Offset_Correction
     with Import, Convention => C,
     External_Name => "ir_temp__adjustOffsetCorrection";
   --  To be used when no object is in front of the sensor.
   
   procedure Set_Offset_Correction (Offset : Float)
     with Import, Convention => C,
     External_Name => "ir_temp__setOffsetCorrection";
   
   function Get_Offset_Correction return Float
     with Import, Convention => C,
     External_Name => "ir_temp__getOffsetCorrection";

end Arduino.IR_Temp_Sensor;
