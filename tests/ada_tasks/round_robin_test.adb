----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Test FIFO ordering in ready queue by simulating a Round-Robin scheduling.
--  Main task uses Running_Thread_Ends_Job_Without_Blocking
--  and activates the secoundary tasks using suspension objects.
--  Activation order of secoundary tasks is changed to be sure FIFO ordering
--  is working properly.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

--pragma Warnings (Off);
--with System.OS_Interface;
--pragma Warnings (On);

with Ada.Synchronous_Task_Control;

with M2.Direct_IO;
with M2.Kernel.API;

with Round_Robin_2;
with Round_Robin_3;

with Tests_Reports;

procedure Round_Robin_Test  is
   package DIO renames M2.Direct_IO;
   package R2 renames Round_Robin_2;
   package R3 renames Round_Robin_3;
   package STC renames Ada.Synchronous_Task_Control;

   Loop_Counter : Integer := 0;

begin
   DIO.Put ("-Round_Robin_Test-");

   M2.Kernel.API.Running_Thread_Ends_Job_Without_Blocking;

   loop
      DIO.Put ("-Main-");
      DIO.Put (Integer (R2.Step_Counter));
      DIO.Put (Loop_Counter);

      Tests_Reports.Assert (R2.Step_Counter = Loop_Counter * 3);
      R2.Step_Counter := R2.Step_Counter + 1;

      Loop_Counter := Loop_Counter + 1;

      if Loop_Counter = R2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      --  wake up the other tasks

      if Loop_Counter mod 2 = 0 then
         STC.Set_True (R2.Suspension_Object_2);
         STC.Set_True (R3.Suspension_Object_3);
      else
         STC.Set_True (R3.Suspension_Object_3);
         STC.Set_True (R2.Suspension_Object_2);
      end if;

      --  End current activation

      M2.Kernel.API.Running_Thread_Ends_Job_Without_Blocking;
   end loop;

end Round_Robin_Test;
