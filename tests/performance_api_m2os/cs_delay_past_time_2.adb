with Ada.Real_Time;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;

package body CS_Delay_Past_Time_2 is
   package DIO renames M2.Direct_IO;

   -----------------
   -- Task_2_Body --
   -----------------

   procedure Task_2_Body is
   begin
      --  DIO.Put ("-Task2-");

      Loop_Counter := Loop_Counter + 1;

      delay until Ada.Real_Time.Time_First;
   end Task_2_Body;

   ------------
   -- Task_2 --
   ------------

   Task_2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => null,
      Body_Ac  => Task_2_Body'Access,
      Priority => Task_2_Prio);

end CS_Delay_Past_Time_2;
