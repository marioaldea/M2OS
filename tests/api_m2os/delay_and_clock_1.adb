----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Interfaces;
with Ada.Real_Time;
with M2.Direct_IO;

with Measurements_LoRes;
with Tests_Reports;

package body Delay_And_Clock_1 is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   Loop_Counter : Integer := 0;

   Delay_In_Milliseconds : constant := 500;
   Margin_In_Milliseconds : constant := 1;
   TS_Margin : constant RT.Time_Span :=
     RT.Milliseconds (Margin_In_Milliseconds);

   use type Ada.Real_Time.Time, RT.Time_Span;

   Next_Time, Start_Time : Ada.Real_Time.Time;
   Elapsed_Time : Ada.Real_Time.Time_Span;
   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (Delay_In_Milliseconds);

   --------------------
   -- Main_Task_Init --
   --------------------

   procedure Main_Task_Init is
   begin
      DIO.Put_Line ("-Main_Task_Init-");

      Measurements_LoRes.Init;
      Measurements_LoRes.Start_Measurement;

      Start_Time := Ada.Real_Time.Clock;
   end Main_Task_Init;

   --------------------
   -- Main_Task_Body --
   --------------------

   procedure Main_Task_Body is
      use type Measurements_LoRes.Time_Units;
      T_Ms : Interfaces.Integer_32;
   begin
      DIO.Put_Line ("-Main-");

      if Loop_Counter = 1 then
         Measurements_LoRes.End_Measurement;

         --  Check RT.Clock is OK
         Elapsed_Time := Ada.Real_Time.Clock - Start_Time;
         Tests_Reports.Assert (abs (Elapsed_Time - Period) <= TS_Margin);

         --  Check conversion to Duration is OK
         DIO.Put ("To_Duration in ms:");
         DIO.Put (Integer (RT.To_Duration (Elapsed_Time) * 1_000));
         DIO.New_Line;

         Tests_Reports.Assert_Equal
           (Integer (RT.To_Duration (Elapsed_Time) * 1_000),
            Delay_In_Milliseconds, Margin_In_Milliseconds);

         --  Check Measurements package works OK
         DIO.Put ("Measurement_Time (in ms):");
         --  ticks / [ticks/s] * [ms/s] = ms
         T_Ms := Interfaces.Integer_32
           (Measurements_LoRes.To_Time_Units(Measurements_LoRes.Measurement_Time)
            * 1_000 /  Measurements_LoRes.Time_Unit_Hz);
         DIO.Put (Interfaces.Integer_32 (T_Ms));
         DIO.New_Line;

         Tests_Reports.Assert_Equal
           (Integer (T_Ms),
            Delay_In_Milliseconds,
            Margin_In_Milliseconds);

         Measurements_LoRes.Finish;
      end if;

      Loop_Counter := Loop_Counter + 1;

      Next_Time := Start_Time + Period;
      delay until Next_Time;
   end Main_Task_Body;

end Delay_And_Clock_1;
