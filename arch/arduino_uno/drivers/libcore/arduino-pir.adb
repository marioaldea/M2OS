----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with AdaX_Dispatching_Stack_Sharing;

--with M2.Direct_IO;
with Arduino;

package body Arduino.PIR is
--   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type Ada.Real_Time.Time;
   use type Ada.Real_Time.Time_Span;

   --  Number of detections

   Num_Of_Detections : Natural := 0 with Volatile;

   ---------------------------
   -- Get_Num_Of_Detections --
   ---------------------------

   function Get_Num_Of_Detections return Natural is
   begin
      return Num_Of_Detections;
   end Get_Num_Of_Detections;

   -------------------------------------
   -- Get_Num_Of_Detections_And_Reset --
   -------------------------------------

   function Get_Num_Of_Detections_And_Reset return Natural is
      Tmp : Natural := Num_Of_Detections;
   begin
      Num_Of_Detections := 0;
      return Tmp;
   end Get_Num_Of_Detections_And_Reset;

   -----------
   -- Reset --
   -----------

   procedure Reset is
   begin
      Num_Of_Detections := 0;
   end Reset;

   --  Next activation time

   Next_Time : RT.Time;

   -------------------
   -- PIR_Task_Init --
   -------------------

   procedure PIR_Task_Init is
   begin
      --DIO.Put (" PIRInit ");
      Arduino.Pin_Mode (Pin  => Pin_PIR,
                        Mode => Arduino.Input);

      Next_Time := Ada.Real_Time.Clock;
   end PIR_Task_Init;

   -------------------
   -- PIR_Task_Body --
   -------------------

   procedure PIR_Task_Body is
   begin
      if Arduino.Digital_Read (Pin_PIR) = Arduino.In_High then
         --DIO.Put_Line (" PIR:detect ");
         Num_Of_Detections := Num_Of_Detections + 1;
      end if;

      --  Wait for next polling time

      Next_Time := Next_Time + Polling_Period;
      delay until Next_Time;
   end PIR_Task_Body;

   --------------
   -- PIR_Task --
   --------------

   PIR_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac => PIR_Task_Init'Unrestricted_Access,
      Body_Ac => PIR_Task_Body'Unrestricted_Access,
      Priority  => Task_Prio);

end Arduino.PIR;
