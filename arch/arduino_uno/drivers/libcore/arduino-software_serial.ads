----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of Arduino standard library SoftwareSerial.h
--  #include <SoftwareSerial.h> must appear in lib_core_generator.ino
--  See https://www.arduino.cc/en/Reference/SoftwareSerial for function
--  documentation.
--
with System;
with Interfaces.C;

package Arduino.Software_Serial is

   Software_Serial_Size_In_Bytes : constant := 31;

   Sizeof_Software_Serial : Integer with
     Import, Convention => C,
     External_Name => "sizeof_SoftwareSerial";
   --  Defined in 'arch/arduino_uno/drivers/libcore/sizes_arduino_types.cpp'
   --  Take the value of -1 if the library is not included.

   type Software_Serial_Filling is
     array(1 .. Software_Serial_Size_In_Bytes) of Interfaces.Unsigned_8;

   ---------------------
   -- Software_Serial --
   ---------------------

   type Software_Serial is limited record
      Filling: Software_Serial_Filling;
   end record;
   pragma Import (CPP, Software_Serial);
   for Software_Serial'Size use Software_Serial_Size_In_Bytes * 8;

   -------------------------
   -- New_Software_Serial --
   -------------------------

   function New_Software_Serial return Software_Serial;
   pragma CPP_Constructor (New_Software_Serial, "_ZN14SoftwareSerialC1Ehhb");

   ---------------------------
   -- Begin_Software_Serial --
   ---------------------------

   procedure Begin_Software_Serial (This : access Software_Serial;
                                    Speed : Interfaces.C.Long);
   pragma Import (CPP, Begin_Software_Serial, "_ZN14SoftwareSerial5beginEl");

   ----------
   -- Read --
   ----------

   function Read (This : access Software_Serial)
                  return Interfaces.C.Int;
   pragma Import (CPP, Read, "_ZN14SoftwareSerial4readEv");

   -----------
   -- Write --
   -----------

   function Write (This : access Software_Serial;
                   B : Interfaces.C.Unsigned_Char)
                   return Interfaces.C.Size_T;
   pragma Import (CPP, Write, "_ZN14SoftwareSerial5writeEh");

   ---------------
   -- Available --
   ---------------

   function Available (This : access Software_Serial)
                       return Interfaces.C.Int;
   pragma Import (CPP, Available, "_ZN14SoftwareSerial9availableEv");

end Arduino.Software_Serial;
