//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2023
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Simple example of use of the M2OS POSIX-like layer and the Arduino API.
// Uses a periodic thread to blink the builtin led.
#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include <Arduino.h>

THREAD_POOL(1);

/*
 *  thread_body
 */
struct timespec next_activation_time;
const struct timespec period = TS(1, 0);
uint8_t val = HIGH;

void * thread_body(void *arg) {
  puts("Thread blink\n");
  digitalWrite(LED_BUILTIN, val);
  val = (val == HIGH? LOW : HIGH);
    
  TS_INC(next_activation_time, period);
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME,
		  &next_activation_time, NULL);
  
  return NULL;
}
    
/*
 *   main
 */
int main (int argc, char **argv) {
  m2osinit();
  check_posix_api();
  
  puts("Main: configure LED_BUILTIN\n");
  pinMode(LED_BUILTIN, OUTPUT);
      
  puts("Main: create thread\n");
  pthread_t th1;
  pthread_attr_t attr;
  struct sched_param sch_param = {.sched_priority = 2};
  
  clock_gettime(CLOCK_MONOTONIC, &next_activation_time);
  
  pthread_attr_init(&attr);
  pthread_attr_setschedparam(&attr, &sch_param);
    
  pthread_create(&th1, &attr, thread_body, NULL);
    
  puts("Main ends\n");

  return 0;
}
