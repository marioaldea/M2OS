----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of evs_ev3touch.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with EVShieldUART_h;
with SHDefines_h;
with Interfaces.C.Extensions;
with Arduino.EVShield;


package Arduino.EVShield.EV3Touch is

   generic
      Bp : SHDefines_h.SH_BankPort;
   package Touch is

      -- initialize the interface and tell the shield where the sensor is connected
      -- function init ( bp : SHDefines_h.SH_BankPort ) return Extensions.bool;
      -- check if the touch sensor is pressed (or bumped)
      function Is_Pressed return Extensions.bool;

      -- You can get bump count for EV3Touch Sensor (an incremental
      -- pressed value) this function will return the bump count since last reset.
      -- (The max value of bumpCount is 254, after that it will not increment).
      -- Programming Tip:
      -- If you don't want to wait to see if button is pressed,
      -- use this bump count,
      -- store the old bumpCount in a variable and see if the new
      -- bumpCount is larger than the old value.
      function Get_Bump_Count return Interfaces.C.int;

      -- reset the bump count and start the incremental bumps from zero
      function Reset_Bump_Count return Extensions.bool;
   end Touch;

end Arduino.EVShield.EV3Touch;
