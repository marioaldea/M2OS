# M2 main Makefile

VERSION=0
SUBVERSION=1
KERNELRELEASE=$(VERSION).$(SUBVERSION)

DEFAULT: help

install: copy_gps_scripts generate_global_gpr rts kernel m2os_tool epilogue
install_no_tool: copy_gps_scripts generate_global_gpr rts kernel epilogue

help:
	@echo "\nUsage: make [help|install|menuconfig|rts|kernel|m2os_tool|clean|distclean]\n"
	@echo "  help:       display this help and exit."
	@echo "  install:    builds and installs the RTS, M2OS and the code transformation tool."
	@echo "  install_no_tool:    builds and installs the RTS and M2OS."
	@echo "  menuconfig: runs the configuration tool (not working yet)."
	@echo "  rts:        builds the GNAT Run-time System for M2OS."
	@echo "  kernel:     builds the M2OS kernel."
	@echo "  m2os_tool:  builds the M2OS instrumentation tool."
	@echo "  clean:      removes all files generated during the building."
	@echo "  distclean:  removes any configuration and generated file from the source tree.\n"
	@echo "The standard user will probably want to use 'make install'.\n"


include config.mk

$(info Building M2OS for the $(M2_TARGET) target ...)

GNAT_RTS_PATH = $(M2OS)gnat_rts/$(M2_TARGET)

.PHONY: copy_gps_scripts kernel m2os_tool rts libm2os examples fresh_build help generate_global_gpr lib_arduino_core target_clean libAtmelStart

export M2OS KERNELRELEASE HOSTGNAT_PATH HOSTCC HOSTCFLAGS HOSTCFLAGS HOSTCXXFLAGS

# look for the arduino IDE installation path
#ifeq ($(M2_TARGET),arduino_uno)
#  ARDUINO_PATH = $(dir $(shell which arduino))

#  ifeq ($(ARDUINO_PATH),)
#	#ADA_LINKER = "avr-gcc"
#        $(info -------------------------------)
#        $(info INFO: cannot find 'arduino' command (Arduino IDE))
#        $(info Install official Arduino IDE from https://www.arduino.cc/en/Main/Software)
#        $(info -------------------------------)

#  else
#    ifeq ($(ARDUINO_PATH),/usr/bin/)
#	#ADA_LINKER = /usr/share/arduino/hardware/tools/avr/bin/avr-gcc
#        $(info CLI not supported in Arduino standard package.)
#        $(info Install official Arduino IDE from https://www.arduino.cc/en/Main/Software)
#        ADA_LINKER = "avr-gcc"
#    else
#	ADA_LINKER := $(ARDUINO_PATH)hardware/tools/avr/bin/avr-gcc
#        $(info Using linker from Arduino IDE at $(ARDUINO_PATH))
#    endif
#  endif # ARDUINO_PATH = ""
#endif # target arduino_uno

# menuconfig
menuconfig: rm_config
	@exec echo -n -e "\b \n>> Building the configuration utility:\n";
	@$(MAKE) --no-print-directory -C scripts all 2>/dev/null
	@scripts/kconfig/mconf Kconfig

distclean: rm_config

# in case the .config has been updated clean up .config dependent parts
rm_config: #.config
	@find -iname ".config*" -delete
	@find -iname "autoconf.h" -delete

# m2os_tool
m2os_tool:
	@exec echo -n -e "\b \n>> Building the automatic conversion tool using libadalang from $(HOSTGNAT_PATH):\n";
	PATH=$(HOSTGNAT_PATH)/bin:$PATH GPR_PROJECT_PATH=$(HOSTGNAT_PATH) gprbuild -j0 m2os_tool/src/tool.gpr
	@mv -f m2os_tool/src/ada_tasks_to_m2 m2os_tool/bin
	@echo DONE

# Copy script to automatize Arduino development in GPS
copy_gps_scripts:
ifeq ($(M2_TARGET), arduino_uno)
	@echo "-- Copying GPS scripts to .gps/plug-ins ..."
	mkdir -p ~/.gps/plug-ins/
	ln -s -f $(M2OS)/scripts/gps/m2_buttons_arduino_uno.py ~/.gps/plug-ins/
	ln -s -f $(M2OS)/scripts/gps/m2_build_actions_arduino_uno.py ~/.gps/plug-ins/
else
	@echo "-- Copying GPS script to .gnatstudio/plug-ins ..."
	mkdir -p ~/.gnatstudio/plug-ins/
	ln -s -f $(M2OS)/scripts/gps/examples_microbit.xml ~/.gnatstudio/plug-ins/
endif

# generate global_switches.gpr
sep:=\",\"-
App_Ada_Flags := $(shell echo $(DBG_ADA_APP) $(OPT_SIZE_FLAGS_APP) $(SUPPRESS_ALL_CHECKS_APP) | sed 's/ //g')
App_C_Flags := $(shell echo $(DBG_C_APP) $(OPT_SIZE_FLAGS_APP) | sed 's/ //g')
RTS_Ada_Flags := $(shell echo $(DBG_ADA_RTS) $(OPT_SIZE_FLAGS_RTS) $(SUPPRESS_ALL_CHECKS_RTS) | sed 's/ //g')
RTS_C_Flags  := $(shell echo $(DBG_C_RTS) $(OPT_SIZE_FLAGS_RTS) | sed 's/ //g')
M2_Ada_Flags := $(DBG_ADA_M2) $(OPT_SIZE_FLAGS_M2) $(SUPPRESS_ALL_CHECKS_M2)
M2_Ada_Flags_No_Spcs := $(shell echo $(M2_Ada_Flags) | sed 's/ //g')
M2_C_Flags  := $(shell echo $(DBG_C_M2) $(OPT_SIZE_FLAGS_M2) | sed 's/ //g')
generate_global_gpr:
	@echo "-- Generating global_switches.gpr..."
	@echo "--  Authomatically generated from Makefile" > global_switches.gpr
	@echo "abstract project Global_Switches is" >> global_switches.gpr
	@echo "   for Source_Files use ();   --  no sources" >> global_switches.gpr
	@echo "   App_Ada_Flags := ("\"$(subst -,$(sep),$(App_Ada_Flags))\"");" >> global_switches.gpr
	@echo "   App_C_Flags  := ("\"$(subst -,$(sep),$(App_C_Flags))\"");" >> global_switches.gpr
	@echo "   RTS_Ada_Flags := ("\"$(subst -,$(sep),$(RTS_Ada_Flags))\"");" >> global_switches.gpr
	@echo "   RTS_C_Flags  := ("\"$(subst -,$(sep),$(RTS_C_Flags))\"");" >> global_switches.gpr
	@echo "   M2_Ada_Flags := ("\"$(subst  -,$(sep),$(M2_Ada_Flags_No_Spcs))\"");" >> global_switches.gpr
	@echo "   M2_C_Flags  := ("\"$(subst -,$(sep),$(M2_C_Flags))\"");" >> global_switches.gpr
#ifeq ($(M2_TARGET),arduino_uno)
#	@echo "   Ada_Linker_Driver := \"$(ADA_LINKER)\";" >> global_switches.gpr
#endif
	@echo "end Global_Switches;" >> global_switches.gpr
	@echo "  Done."

# M2_TARGET = gnat_bb
ifeq ($(M2_TARGET),gnat_bb)
$(error Solve names collision with architecture stm32f4. Aborted)

# M2_TARGET = rp2040 or riscv
else ifeq ($(M2_TARGET),$(filter $(M2_TARGET),rp2040 riscv))

rts:
	@echo "-- Generating RTS..."
	cd gnat_rts/$(M2_TARGET)/ && gprbuild -P runtime_build.gpr
	
kernel:
	@echo "-- Building M2OS (flags: $(M2_Ada_Flags))..."
	gprbuild -v -P $(ARCH_PATH)/m2os_$(M2_TARGET).gpr
	@echo DONE

target_clean:
	@echo ">> Cleaning M2OS..."
	-gprclean $(M2_TARGET)_m2os.gpr
	@echo ">> End cleaning M2OS"
# End M2_TARGET = rp2040 or riscv

# M2_TARGET = stm32f4 or microbit
else ifeq ($(M2_TARGET),$(filter $(M2_TARGET),stm32f4 microbit))

rts:
	@echo "-- Setting files for M2OS RTSs in $(GNAT_PATH)..."
	cd gnat_rts/$(M2_TARGET)/ && ./create_m2os_rts_in_gnat.sh

ifneq ("$(wildcard $(GNAT_PATH)/arm-eabi/lib/gnat/m2os-$(M2_TARGET))","")
	@echo "-- Cleaning m2os_$(M2_TARGET) RTS..."
	gprclean -P $(GNAT_PATH)/arm-eabi/BSPs/m2os_$(M2_TARGET).gpr
endif

	@echo "-- Building m2os_$(M2_TARGET) RTS (flags $(DBG_ADA_RTS))..."
	gprbuild -j0 $(DBG_ADA_RTS) -P $(GNAT_PATH)/arm-eabi/BSPs/m2os_$(M2_TARGET).gpr

#ifneq ("$(wildcard $(GNAT_PATH)/arm-eabi/lib/gnat/m2os-stm32f4)","")
#	@echo Uninstalling m2os_stm32f4 RTS...
#	gprinstall --uninstall -P $(GNAT_PATH)/arm-eabi/BSPs/m2os_stm32f4.gpr
#endif

	@echo "-- Installing m2os_$(M2_TARGET) RTS..."
	gprinstall -p -f -P $(GNAT_PATH)/arm-eabi/BSPs/m2os_$(M2_TARGET).gpr

kernel:
	@echo "-- Building M2OS ..."
	@echo "(flags: $(M2_Ada_Flags))"
	gprbuild -P $(ARCH_PATH)/m2os_$(M2_TARGET).gpr
	@echo DONE

target_clean:
	@echo "-- Clearing M2OS..."
	gprclean $(ARCH_PATH)/m2os_$(M2_TARGET).gpr

	@echo "-- Clearing m2os_$(M2_TARGET) RTS..."
	gprclean -P $(GNAT_PATH)/arm-eabi/BSPs/m2os_$(M2_TARGET).gpr

#ifneq ("$(wildcard $(GNAT_PATH)/arm-eabi/lib/gnat/m2os-stm32f4)","")
#	@echo Uninstalling m2os_stm32f4 RTS...
#	gprinstall --uninstall -P $(GNAT_PATH)/arm-eabi/BSPs/m2os_stm32f4.gpr
#endif

# End M2_TARGET = stm32f4 or microbit

# M2_TARGET = Arduino or AVR-IoT
else ifeq ($(M2_TARGET),$(filter $(M2_TARGET),arduino_uno avr_iot))

rts $(GNAT_RTS_PATH)/adalib/libgnat.a:
	@echo "-- Generating RTS..."
	rm -f $(GNAT_RTS_PATH)/adainclude
	ln -sf $(GNAT_RTS_PATH)/adainclude_gcc_$(GCC_VER) $(GNAT_RTS_PATH)/adainclude
	mkdir -p $(GNAT_RTS_PATH)/adalib
	make -C $(GNAT_RTS_PATH)/adainclude -f Makefile.adalib

ifeq ($(M2_TARGET),arduino_uno)
lib_arduino_core:
	@echo "-- Building Arduino library..."
	cd $(ARCH_PATH)/drivers/libcore && make copy_library

kernel: lib_arduino_core
else ifeq ($(M2_TARGET),avr_iot)
libAtmelStart:
	cp $(ARCH_PATH)/drivers/libAtmelStart.a.orig $(ARCH_PATH)/drivers/libAtmelStart.a

kernel: libAtmelStart
else
$(error Unexpected architecture $(M2_TARGET))
endif
	@echo "-- Building M2OS (flags: $(M2_Ada_Flags))..."
	gprbuild -P $(ARCH_PATH)/m2os_$(M2_TARGET).gpr
	@echo DONE

target_clean:
	@echo ">> Cleaning M2OS..."
	-gprclean $(M2_TARGET)_m2os.gpr
	@echo ">> End cleaning M2OS"

#else
## RPi
#kernel: rts-m2os/adalib/libgnat.a
#	make -C libm2os
#	@echo DONE
#endif

# End M2_TARGET = Arduino or AVR-IoT

# M2_TARGET = epiphany
else ifeq ($(M2_TARGET),epiphany)

rts $(GNAT_RTS_PATH)/adalib/libgnat.a:
	@echo "-- Generating RTS..."
	rm -f $(GNAT_RTS_PATH)/adainclude
	ln -sf $(GNAT_RTS_PATH)/adainclude_gcc_$(GCC_VER) $(GNAT_RTS_PATH)/adainclude
	mkdir -p $(GNAT_RTS_PATH)/adalib
	make -C $(GNAT_RTS_PATH)/adainclude -f Makefile.adalib

kernel: rts
	@echo "-- Building M2OS..."
	@echo "(flags: $(M2_Ada_Flags))"
	gprbuild -P $(ARCH_PATH)/m2os_$(M2_TARGET).gpr
	@echo "Building libm2os.a by hand since libraries are not supported..."
	rm -f $(ARCH_PATH)/libm2os/libm2os.a
	make -C $(ARCH_PATH)/libm2os libm2os_$(M2_TARGET)
	$(AR) r $(ARCH_PATH)/libm2os/libm2os.a $(ARCH_PATH)/obj/*.o $(ARCH_PATH)/libm2os/*.o
	@echo "DONE"

target_clean:
	@echo ">> Cleaning M2OS..."
	-gprclean $(M2_TARGET)_m2os.gpr
	@echo ">> End cleaning M2OS"
# End M2_TARGET = epiphany


# fresh_build
fresh_build: clean kernel m2os_tool

else
$(ERROR: unexpected target value:$(M2_TARGET):)
endif

epilogue:
	@echo "M2OS is set to use the $(M2_TARGET) architecture"


include rules.mk
