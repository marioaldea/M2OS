----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Synchronous_Task_Control;
with Ada.Real_Time;

with M2.Direct_IO;
with Tests_Reports;

with Suspension_Object_Open_2;

procedure Suspension_Object_Open_Test is
   package DIO renames M2.Direct_IO;
   package S2 renames Suspension_Object_Open_2;
   package STC renames Ada.Synchronous_Task_Control;

   use type Ada.Real_Time.Time;

   Loop_Counter : Integer := 0;

   Next_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);

begin
   DIO.Put ("-Suspension_Object_Open_Test-");

   Next_Time := Ada.Real_Time.Clock;

   loop
      DIO.Put ("-Main-");
      DIO.Put (Integer (S2.Step_Counter));
      DIO.Put (Loop_Counter);

      if S2.Step_Counter mod 2 = 0 then
         Tests_Reports.Assert (S2.Step_Counter = Loop_Counter * 2);
      else
         Tests_Reports.Assert (S2.Step_Counter = Loop_Counter * 2 + 1);
      end if;
      S2.Step_Counter := S2.Step_Counter + 1;

      Loop_Counter := Loop_Counter + 1;

      if Loop_Counter = S2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      STC.Set_True (S2.Suspension_Object);

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end loop;

end Suspension_Object_Open_Test;
