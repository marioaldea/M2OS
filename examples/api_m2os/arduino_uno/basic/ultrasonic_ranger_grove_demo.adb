----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Grove - Ultrasonic Ranger:
--    https://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/
--
--     ---------------------                 ----------------------
--     |                   |                 |                    |
--     | Grove          GND|-----------------|GND     Arduino Uno |
--     | Ultrasonic     VCC|-----------------|5V                  |
--     | Ranger          NC|                 |                    |
--     |                SIG|-----------------|D7                  |
--     |                   |                 |                    |
--     ---------------------                 ----------------------
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino.Ultrasonic_Ranger_Grove;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Interfaces.C;

procedure Ultrasonic_Ranger_Grove_Demo is
   package DIO renames M2.Direct_IO;
   
   package Ranger is new Arduino.Ultrasonic_Ranger_Grove.Ranger (Pin => 7);
   
   Dist : Interfaces.C.Long;
begin
   DIO.Put_Line ("Ultrasonic Ranger Grove Demo");
 
   loop
      Dist := Ranger.Measure_In_Centimeters;       
      DIO.Put ("Dist:");
      DIO.Put (Integer (Dist));       
      DIO.Put_Line (" cm");
      
      Dist := Ranger.Measure_In_Millimeters;       
      DIO.Put ("Dist:");
      DIO.Put (Integer (Dist));       
      DIO.Put_Line (" mm");
      
      Dist := Ranger.Measure_In_Inches;       
      DIO.Put ("Dist:");
      DIO.Put (Integer (Dist));       
      DIO.Put_Line (" in");
      
      DIO.New_Line;

      Arduino.C_Delay (2_000);
   end loop;
   
end Ultrasonic_Ranger_Grove_Demo;
