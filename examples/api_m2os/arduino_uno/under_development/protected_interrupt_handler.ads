--  Task activated by an entry opened from a protected interrupt handler in
--  a Protected Object.
--  The Pin 5 must be wired to pin 2 to produce the interrupt
with Ada.Interrupts;

package Protected_Interrupt_Handler is
   Interrupt_Num : constant Ada.Interrupts.Interrupt_Id := 0;
end Protected_Interrupt_Handler;
