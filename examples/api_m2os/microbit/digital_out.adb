
-- Led in pin 0

with MicroBit.IOs;
with MicroBit.Time;
with Ada.Text_IO;

with AdaX_Dispatching_Stack_Sharing;

procedure Digital_Out is
begin
   Ada.Text_IO.Put_Line ("Digital Out working in M2OS!");
   --  Loop forever
   loop
      --  Turn on the LED connected to pin 0
      MicroBit.IOs.Set (8, True);
      MicroBit.IOs.Set (12, True);
      Ada.Text_IO.Put_Line ("LED Turned On");
      --  Wait 500 milliseconds
      MicroBit.Time.Delay_Ms (1000);

      --  Turn off the LED connected to pin 0
      MicroBit.IOs.Set (8, False);
      MicroBit.IOs.Set (12, False);
      Ada.Text_IO.Put_Line ("LED Turned Off");
      --  Wait 500 milliseconds
      MicroBit.Time.Delay_Ms (1000);
   end loop;
end Digital_Out;
