----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--  Test the activation order of three Timing Events.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with System;

with M2.Kernel.API;

with M2.Direct_IO;
with M2.HAL;
with Tests_Reports;

with Timing_Events_API_Order_PO;

procedure Timing_Events_API_Order_Test  is
   package DIO renames M2.Direct_IO;
   package API renames M2.Kernel.API;

   use type API.Time;

   Event1 : aliased API.Timing_Event;
   Event2 : aliased API.Timing_Event;
   Event3 : aliased API.Timing_Event;
   T : API.Time := API.Get_Time;

begin
   DIO.Put_Line ("-Timing_Events_Order_Test-");
   DIO.Put_Line ("Event1'Size" & Integer'Image (Event1'Size));
   DIO.Put_Line ("Timing_Event'Size" & Integer'Image (API.Timing_Event'Size));
   DIO.Put_Line ("System.Timing_Event_Size_In_Bytes" &
                   Integer'Image (System.Timing_Event_Size_In_Bytes));
   Tests_Reports.Assert
     (API.Timing_Event'Size = System.Timing_Event_Size_In_Bytes * 8);

   DIO.Put_Line ("Program three Timing Events");

   API.Set_Handler (Event1'Unrestricted_Access,
                    T + 150,
                    Timing_Events_API_Order_PO.PO.PO_Handler1'Access);

   API.Set_Handler (Event2'Unrestricted_Access,
                    T + 100,
                    Timing_Events_API_Order_PO.PO.PO_Handler2'Access);

   API.Set_Handler (Event3'Unrestricted_Access,
                    T + 50,
                    Timing_Events_API_Order_PO.PO.PO_Handler3'Access);

   Tests_Reports.Assert (Timing_Events_API_Order_PO.Step = 0);
   Timing_Events_API_Order_PO.Step := Timing_Events_API_Order_PO.Step + 1;

   while Timing_Events_API_Order_PO.Step < 12 loop
      Tests_Reports.Assert (M2.HAL.Are_Interrupts_Enabled);
   end loop;

   Tests_Reports.Test_OK;

end Timing_Events_API_Order_Test;
