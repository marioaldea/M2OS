with Interfaces;

package body M2.HAL.Timer is

   use type Interfaces.Unsigned_8;

   ----------------------------------
   -- ATmega328p Timer 0 registers --
   ----------------------------------

   -- Timer/Counter Control Register A
   TCCR0A : Interfaces.Unsigned_8;
   for TCCR0A'Address use 16#44#;
   pragma Volatile (TCCR0A);

   -- Timer/Counter Control Register B
   TCCR0B : Interfaces.Unsigned_8;
   for TCCR0B'Address use 16#45#;
   pragma Volatile (TCCR0B);

   -- Timer/Counter Register
   TCNT0 : Timer_Ticks;
   for TCNT0'Address use 16#46#;
   pragma Volatile (TCNT0);

   --  Output Compare Register A
   OCR0A : Interfaces.Unsigned_8;
   for OCR0A'Address use 16#47#;
   pragma Volatile (OCR0A);

   --  Timer/Counter Interrupt Mask Register
   TIMSK0 : Interfaces.Unsigned_8;
   for TIMSK0'Address use 16#6E#;
   pragma Volatile (TIMSK0);

   -------------------
   -- Timer_Handler --
   -------------------

   OS_Tick_Handler : access procedure;

   procedure Tick;
   pragma Machine_Attribute (Entity         => Tick,
                             Attribute_Name => "signal");
   pragma Export (C, Tick, "__vector_14");

   procedure Tick is
   begin
      Now := Now + 1;

      if M2.Use_Timing_Events'First then
         OS_Tick_Handler.all;
      end if;
   end Tick;

   --------------
   -- Get_Time --
   --------------

   function Get_Time return HWTime is
   begin
      return Now;
   end Get_Time;

   -----------------
   -- Get_Counter --
   -----------------

   function Get_Counter return Timer_Ticks is
   begin
      return TCNT0;
   end Get_Counter;

   ---------------
   -- Initilize --
   ---------------

   --  Timer 0 configuration for 1ms periodic interrupt:
   --    Core freq 16 MHz => Period = 0.000_000_062_5 s
   --    Prescaler = 64
   --    End of count (TOP): Prescaler * Period * 250 = 0.001 s = 1 ms

   procedure Initialize (OS_Tick_Handler : HAL.OS_Tick_Handler_Ac) is
   begin
      Timer.OS_Tick_Handler := OS_Tick_Handler;

      --  Set timer mode to  "Clear Timer on Compare" (CTC)
      --  Use OCR0A as "TOP" counter value

      TCCR0A := 2#0000_0010#;

      -- Set end of count (TOP) value

      OCR0A := 249;

      --  Enable Output Compare Match A Interrupt

      TIMSK0 := TIMSK0 or 2#0000_0010#;

      --  Set prescaler to 64

      TCCR0B := TCCR0B or 2#0000_0011#;

      --  Reset counter

      TCNT0 := 0;
   end Initialize;

end M2.HAL.Timer;
