with LCD;

with Ada.Unchecked_Conversion;

package body LCD_1602A_Driver is

   -- Buffer type

   type Buffer is array (size_t) of Character;
   type Buffer_Ac is access Buffer;
   function To_Buffer_Ac is
     new Ada.Unchecked_Conversion (System.Address, Buffer_Ac);

   ----------
   -- Open --
   ----------

   procedure Open (Mode : M2.Drivers.File_Access_Mode) is
   begin
      LCD.Init;
   end Open;

   -----------
   -- Write --
   -----------

   function Write (Buffer_Ptr : System.Address;
                   Bytes      : size_t)
                   return int is
      Buff_Ac : Buffer_Ac := To_Buffer_Ac (Buffer_Ptr);
   begin
      for I in size_t range 0 .. Bytes - 1 loop
         LCD.Put (Buff_Ac (I));
      end loop;

      return int (Bytes);
   end Write;

end LCD_1602A_Driver;
