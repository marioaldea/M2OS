----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of using Arduino.Servo package.
--  Arduino.Servo is a binding to the Servo.h standar Arduino library.
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |          Pulse|-----------------|D5                   |
--     | Servo      VDD|-----------------|VDD      Arduino Uno |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------
--
--  --------------------------------------------------------------------------
with Arduino.Servo;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with M2.Kernel.API;

procedure Servo_Simple_Demo is
   package DIO renames M2.Direct_IO;

   Pin_Servo : constant Arduino.PWM_Digital_Pin := Arduino.PWM_D5;

   use type Arduino.Servo.Angle_Deg, Arduino.Servo.Signed_Angle_Deg;

   Angle : Arduino.Servo.Angle_Deg := 0;
   Angle_Increment : Arduino.Servo.Signed_Angle_Deg := -6;

   Servo : aliased Arduino.Servo.Servo;

   Ret : Arduino.Servo.Attach_Ret;
   use type Arduino.Servo.Attach_Ret;

begin
   DIO.Put_Line ("Servo_Simple_Demo.Attach");
   Ret := Arduino.Servo.Attach (Servo'Access, Pin_Servo);

   DIO.Put_Line ("Servo_Simple_Demo.Write");
   DIO.Put (" Angle:");
   loop
      if Angle = 180 or Angle = 0 then
         Angle_Increment := -Angle_Increment;
      end if;

      Angle := Angle + Angle_Increment;

      DIO.Put (" ");
      DIO.Put (Integer (Angle));

      Arduino.Servo.Write (Servo'Access, Angle => Angle);

      Arduino.C_Delay (MS => 400);
   end loop;
end Servo_Simple_Demo;
