# Install M2OS

Installation tested in `Ubuntu 20.04 (64 bits)`.

## 0. Common Prerequisites

This prerequisites must be fulfilled for any target you plan to use M2OS.

- **Native GNAT compiler**: gnat-community-2019-20190517-x86_64-linux-bin (https://www.adacore.com/download) (For Arduino Uno and Epiphany targets more modern versions of GNAT can be used)

```sh
   $ chmod u+x gnat-community-2019-20190517-x86_64-linux-bin
   $ ./gnat-community-2019-20190517-x86_64-linux-bin
```

   During the GNAT installation choose all the default settings. GNAT will be installed in ~/opt/GNAT/2019/

## 1.a Install M2OS for Arduino Uno target

**AVR-gcc compiler** with support for the Ada language
 
Download [avr-gcc-gnat-7.3.0.zip](https://m2os.unican.es/wp-content/uploads/avr-gcc-gnat-7.3.0.zip) and unzip it (for example in ~/opt/avr-gcc-gnat-7.3.0).

**simulavr** (Only if you want to use the Arduino emulator)

Two options:
- Install from the official repository (`sudo apt install simulavr`).
- Download `libsim_1.1.0_amd64.deb` and `simulavr_1.1.0_amd64.deb` from the
[simulavr site](http://download.savannah.nongnu.org/releases/simulavr/). Install both
packages (in Ubuntu you can install them with "Software Install" or with "GDebi Package Installer").

Add everything to the **PATH environment variable**:

```sh
   $ export PATH=~/opt/avr-gcc-gnat-7.3.0/bin:~/opt/GNAT/2019/bin:$PATH
```

## 1.b Install M2OS for STM32F target

**ARM GNAT compiler 2018**: gnat-community-2018-20180524-arm-elf-linux64-bin (https://www.adacore.com/download "More packages, platforms, versions and sources")

```sh
   $ chmod u+x gnat-community-2018-20180524-arm-elf-linux64-bin
   $ ./gnat-community-2018-20180524-arm-elf-linux64-bin
```

   During the GNAT installation choose all the default settings. GNAT will be installed in ~/opt/GNAT/2018-arm-elf/

Add everything to the **PATH environment variable**:

```sh
   $ export PATH=~/opt/GNAT/2018-arm-elf/bin:~/opt/GNAT/2019/bin:$PATH
```

## 1.c Install M2OS for Epiphany target

Install **epiphany-gcc compiler** with support for the Ada language:
 
   Download [epiphany-gcc-7.4.0.zip](https://m2os.unican.es/wp-content/uploads/epiphany-gcc-7.4.0.zip) and unzip it (for example in ~/opt/epiphany-gcc-7.4.0).

Add everything to the **PATH environment variable**:

```sh
   $ export PATH=~/opt/epiphany-gcc-7.4.0/bin:~/opt/GNAT/2019/bin:$PATH
```

Install the **Epiphany specific libraries**:

   Download [e-lib.zip](https://m2os.unican.es/wp-content/uploads/e-lib.zip) and unzip it in the $(M2OS)/arch/epiphany/ folder.
   
## 2. Build M2OS libraries and RTS

Edit `config_params.mk` and comment out all the `CONFIG_BOARD_*` constants except the one corresponding to the target you want to build M2OS for.

Build the M2OS libraries, the GNAT RTS and the code transformation tool:

```sh
$ make install
```

## 3. Build and run an application

For instructions about how build and run an application visit the appropriate User's Guide:

- [M2OS in Arduino Uno User's Guide](https://m2os.unican.es/wp-content/uploads/M2OS-in-Arduino-Uno-Users-Guide.pdf)
- [M2OS in STM32 User's Guide](https://m2os.unican.es/wp-content/uploads/m2os-in-stm32f4-users-guide.pdf)
- [M2OS in Epiphany User's Guide](https://m2os.unican.es/wp-content/uploads/m2os-in-epiphany-users-guide.pdf)
