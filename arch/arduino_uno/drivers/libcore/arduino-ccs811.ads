----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of the CCS811 library for the
--  "KS0457 keyestudio CCS811 Carbon Dioxide Air Quality Sensor".
--  https://wiki.keyestudio.com/KS0457_keyestudio_CCS811_Carbon_Dioxide_Air_Quality_Sensor
----------------------------------------------------------------------------
with Interfaces.C;
with Arduino.Wire;

package Arduino.CCS811 is
     
   type ECycle_T is (EClosed, -- Idle (Measurements are disabled in this mode)
                     ECycle_1s, -- Constant power mode, IAQ measurement every second
                     ECycle_10s, -- Pulse Heating Mode IAQ Measurement Every 10 Seconds
                     ECycle_60s, -- Low Power Pulse Heating Mode IAQ Measurement Every 60 Seconds
                     ECycle_250ms -- constant Power Mode, Sensor Measurement Every 250ms 1xx: Reserved Modes (for Future use)
                    );
   for ECycle_T'Size use Interfaces.C.Int'Size;
   
   type Base_Line_T is (Base_Line_Value);
   for Base_Line_T use (Base_Line_Value => 16#847B#);
   for Base_Line_T'Size use Interfaces.Unsigned_16'Size;
   
   type Data_Ready_T is new Boolean;
   for Data_Ready_T'Size use Interfaces.Unsigned_8'Size;
      
   type CO2_PPM_T is new Interfaces.Unsigned_16;
   --  CO2 concentration (ppm)
   
   --  Indoor air classification (IDA):
   --  IDA1: High Quality (<= 400 ppm) (Hospitals, ...) 
   --  IDA2: Medium Quality (400 ppm, 600 ppm] (Home, office, musseum, ...)
   --  IDA3: Moderate Quality (600 ppm, 1_000 ppm] (Restaurants, ...)  
   --  IDA4: Low Quality (> 1_000 ppm]
   CO2_PPM_IDA1_Limit : constant CO2_PPM_T :=   400; 
   CO2_PPM_IDA2_Limit : constant CO2_PPM_T :=   600; 
   CO2_PPM_IDA3_Limit : constant CO2_PPM_T := 1_000;
   
   type TVOC_PPB_T is new Interfaces.Unsigned_16;
   --  Total Volatile Organic Compounds
   
   TVOC_PPB_Level_Limit_Very_Good : constant TVOC_PPB_T :=   200;   
   TVOC_PPB_Level_Limit_Good      : constant TVOC_PPB_T :=   600;   
   TVOC_PPB_Level_Limit_Medium    : constant TVOC_PPB_T := 1_000;
   TVOC_PPB_Level_Limit_Bad       : constant TVOC_PPB_T := 2_000;
   --  Above 2000 ppb is "unacceptable", run away
   
   function Begin_CCS811 return Integer
     with Inline_Always;
   -- Return 0 if initialization succeeds, otherwise return non-zero.
   
   procedure Set_Meas_Cycle (Cycle : ECycle_T)
     with Inline_Always; 
   
   function Get_CO2_PPM return CO2_PPM_T
     with Inline_Always;
   -- return current carbon dioxide concentration, unit:ppm
   
   function Get_TVOC_PPB return TVOC_PPB_T
     with Inline_Always;
   --  @return Return current TVOC concentration, unit: ppb
   
   procedure Write_Base_Line (Base_Line : Base_Line_T)
     with Inline_Always;
        
   function Check_Data_Ready return Data_Ready_T
     with Inline_Always;
   
   procedure Set_In_Temp_Hum (Temp : Float; Hum : Float);
   --  Set environment parameter
   --    Temp Set temperature value, unit: centigrade, range (-40~85)
   --    Hum  Set humidity value, unit: RH, range (0~100)
  
   -----------------
   -- Type's size --
   -----------------

   Sizeof_CCS811 : Integer with
     Import, Convention => C,
     External_Name => "sizeof_CCS811";
   --  Defined in 'sizes_arduino_types.cpp' (created by Makefile)
   --  Takes the value of -1 if the library is not included.

   CCS811_Size_In_Bytes : constant := 9;   
end Arduino.CCS811;
