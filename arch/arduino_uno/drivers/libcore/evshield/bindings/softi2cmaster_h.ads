pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with stdint_h;
with Interfaces.C.Strings;
with Arduino;

package SoftI2cMaster_h is

   I2C_DELAY_USEC : constant := 30;  --  SoftI2cMaster.h:33

   I2C_READ : constant := 1;  --  SoftI2cMaster.h:36
   I2C_WRITE : constant := 0;  --  SoftI2cMaster.h:37

   function min (a : double; b : double) return double;  -- SoftI2cMaster.h:31
   pragma Import (CPP, min, "_Z3mindd");

   package Class_SoftI2cMaster is
      type SoftI2cMaster is limited record
         initialized : aliased Arduino.Boolean8;  -- SoftI2cMaster.h:44
         sclPin_u : aliased stdint_h.uint8_t;  -- SoftI2cMaster.h:46
         sdaPin_u : aliased stdint_h.uint8_t;  -- SoftI2cMaster.h:47
         deviceAddr : aliased stdint_h.uint8_t;  -- SoftI2cMaster.h:48
         u_error_code : aliased stdint_h.uint8_t;  -- SoftI2cMaster.h:49
         u_so_buffer : access stdint_h.uint8_t;  -- SoftI2cMaster.h:53
      end record;
      pragma Import (CPP, SoftI2cMaster);

      function start (this : access SoftI2cMaster; addressRW : stdint_h.uint8_t) return stdint_h.uint8_t;  -- SoftI2cMaster.h:56
      pragma Import (CPP, start, "_ZN13SoftI2cMaster5startEh");

      procedure stop (this : access SoftI2cMaster);  -- SoftI2cMaster.h:59
      pragma Import (CPP, stop, "_ZN13SoftI2cMaster4stopEv");

      function restart (this : access SoftI2cMaster; addressRW : stdint_h.uint8_t) return stdint_h.uint8_t;  -- SoftI2cMaster.h:62
      pragma Import (CPP, restart, "_ZN13SoftI2cMaster7restartEh");

      function write (this : access SoftI2cMaster; b : stdint_h.uint8_t) return stdint_h.uint8_t;  -- SoftI2cMaster.h:65
      pragma Import (CPP, write, "_ZN13SoftI2cMaster5writeEh");

      function read (this : access SoftI2cMaster; last : stdint_h.uint8_t) return stdint_h.uint8_t;  -- SoftI2cMaster.h:68
      pragma Import (CPP, read, "_ZN13SoftI2cMaster4readEh");

      function New_SoftI2cMaster (devAddr : stdint_h.uint8_t) return SoftI2cMaster;  -- SoftI2cMaster.h:71
      pragma CPP_Constructor (New_SoftI2cMaster, "_ZN13SoftI2cMasterC1Eh");

      procedure initProtocol
        (this : access SoftI2cMaster;
         sclPin : stdint_h.uint8_t;
         sdaPin : stdint_h.uint8_t);  -- SoftI2cMaster.h:74
      pragma Import (CPP, initProtocol, "_ZN13SoftI2cMaster12initProtocolEhh");

      function readRegisters
        (this : access SoftI2cMaster;
         startRegister : stdint_h.uint8_t;
         bytes : stdint_h.uint8_t;
         buf : access stdint_h.uint8_t) return access stdint_h.uint8_t;  -- SoftI2cMaster.h:77
      pragma Import (CPP, readRegisters, "_ZN13SoftI2cMaster13readRegistersEhhPh");

      function writeRegistersWithLocation
        (this : access SoftI2cMaster;
         bytes : int;
         buf : access stdint_h.uint8_t) return Extensions.bool;  -- SoftI2cMaster.h:80
      pragma Import (CPP, writeRegistersWithLocation, "_ZN13SoftI2cMaster26writeRegistersWithLocationEiPh");

      function writeRegisters
        (this : access SoftI2cMaster;
         location : stdint_h.uint8_t;
         bytes_to_write : stdint_h.uint8_t;
         buffer : access stdint_h.uint8_t) return Extensions.bool;  -- SoftI2cMaster.h:83
      pragma Import (CPP, writeRegisters, "_ZN13SoftI2cMaster14writeRegistersEhhPh");

      function writeByte
        (this : access SoftI2cMaster;
         location : stdint_h.uint8_t;
         data : stdint_h.uint8_t) return Extensions.bool;  -- SoftI2cMaster.h:87
      pragma Import (CPP, writeByte, "_ZN13SoftI2cMaster9writeByteEhh");

      function writeInteger
        (this : access SoftI2cMaster;
         location : stdint_h.uint8_t;
         data : stdint_h.uint16_t) return Extensions.bool;  -- SoftI2cMaster.h:90
      pragma Import (CPP, writeInteger, "_ZN13SoftI2cMaster12writeIntegerEhj");

      function writeLong
        (this : access SoftI2cMaster;
         location : stdint_h.uint8_t;
         data : stdint_h.uint32_t) return Extensions.bool;  -- SoftI2cMaster.h:93
      pragma Import (CPP, writeLong, "_ZN13SoftI2cMaster9writeLongEhm");

      function readString
        (this : access SoftI2cMaster;
         startRegister : stdint_h.uint8_t;
         bytes : stdint_h.uint8_t;
         buf : access stdint_h.uint8_t;
         len : stdint_h.uint8_t) return Interfaces.C.Strings.chars_ptr;  -- SoftI2cMaster.h:96
      pragma Import (CPP, readString, "_ZN13SoftI2cMaster10readStringEhhPhh");

      function readByte (this : access SoftI2cMaster; location : stdint_h.uint8_t) return stdint_h.uint8_t;  -- SoftI2cMaster.h:99
      pragma Import (CPP, readByte, "_ZN13SoftI2cMaster8readByteEh");

      function readInteger (this : access SoftI2cMaster; location : stdint_h.uint8_t) return stdint_h.int16_t;  -- SoftI2cMaster.h:102
      pragma Import (CPP, readInteger, "_ZN13SoftI2cMaster11readIntegerEh");

      function readLong (this : access SoftI2cMaster; location : stdint_h.uint8_t) return stdint_h.uint32_t;  -- SoftI2cMaster.h:105
      pragma Import (CPP, readLong, "_ZN13SoftI2cMaster8readLongEh");

      function getFirmwareVersion (this : access SoftI2cMaster) return Interfaces.C.Strings.chars_ptr;  -- SoftI2cMaster.h:108
      pragma Import (CPP, getFirmwareVersion, "_ZN13SoftI2cMaster18getFirmwareVersionEv");

      function getVendorID (this : access SoftI2cMaster) return Interfaces.C.Strings.chars_ptr;  -- SoftI2cMaster.h:111
      pragma Import (CPP, getVendorID, "_ZN13SoftI2cMaster11getVendorIDEv");

      function getDeviceID (this : access SoftI2cMaster) return Interfaces.C.Strings.chars_ptr;  -- SoftI2cMaster.h:114
      pragma Import (CPP, getDeviceID, "_ZN13SoftI2cMaster11getDeviceIDEv");

      function getWriteErrorCode (this : access SoftI2cMaster) return stdint_h.uint8_t;  -- SoftI2cMaster.h:117
      pragma Import (CPP, getWriteErrorCode, "_ZN13SoftI2cMaster17getWriteErrorCodeEv");

      function checkAddress (this : access SoftI2cMaster) return Extensions.bool;  -- SoftI2cMaster.h:119
      pragma Import (CPP, checkAddress, "_ZN13SoftI2cMaster12checkAddressEv");

      function setAddress (this : access SoftI2cMaster; address : stdint_h.uint8_t) return Extensions.bool;  -- SoftI2cMaster.h:124
      pragma Import (CPP, setAddress, "_ZN13SoftI2cMaster10setAddressEh");
   end;
   use Class_SoftI2cMaster;
end SoftI2cMaster_h;
