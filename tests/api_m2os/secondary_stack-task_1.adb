----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with AdaX_Dispatching_Stack_Sharing;

with Ada.Real_Time;
with M2.Direct_IO;

with Tests_Reports;

package body Secondary_Stack.Task_1 is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type Ada.Real_Time.Time;

   Num_Of_Loops : constant := 10;
   Loop_Counter : Integer := 0;

   Next_Time : Ada.Real_Time.Time;

   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);

   function Ret_String (Length : Positive) return String is
      Str : String (1 .. Length) := (others => 'A');
   begin
      return Str;
   end Ret_String;

   -----------------
   -- Task_1_Init --
   -----------------

   procedure Task_1_Init is
      Str : String (1 .. 5);
   begin
      DIO.Put ("-Task_1_Init-");

      Str := Ret_String (5);

      DIO.Put_Line (Str);

      Next_Time := Ada.Real_Time.Clock;
   end Task_1_Init;

   -----------------
   -- Task_1_Body --
   -----------------

   procedure Task_1_Body is
   begin
      DIO.Put ("-Main 1-");

      declare
         Str : String (1 .. 8);
      begin
         Str := Ret_String (8);

         DIO.Put_Line (Str);
      end;

      Loop_Counter := Loop_Counter + 1;

      if Loop_Counter = Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Task_1_Body;

   Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_1_Init'Access,
      Body_Ac  => Task_1_Body'Access,
      Priority => 5);

end Secondary_Stack.Task_1;
