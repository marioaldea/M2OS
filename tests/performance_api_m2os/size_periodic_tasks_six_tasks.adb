   
with Ada.Real_Time;

#if not MEASURE_SIZE then
with M2.Direct_IO;
with Tests_Reports;
#end if;

with AdaX_Dispatching_Stack_Sharing.Periodic_Task;

package body Size_Periodic_Tasks_Six_Tasks is
#if not MEASURE_SIZE then
   package DIO renames M2.Direct_IO;
#end if;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;
   
   Priority : constant := 3;
   Period_In_Ms : constant := 100;

   ------------
   -- Task_1 --
   ------------
 
#if not MEASURE_SIZE then  
   Task_1_Executed : Boolean := False;
#end if;
   Next_Time_1 : RT.Time;
   Period_1 : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms);
   
   procedure Task_1_Init is
   begin
      Next_Time_1 := RT.Clock;
   end Task_1_Init;

   procedure Task_1_Body is
   begin
#if not MEASURE_SIZE then
      DIO.Put ("Task1");
      Task_1_Executed := True;
#end if;
      
      Next_Time_1 := Next_Time_1 + Period_1;
      delay until Next_Time_1;
   end Task_1_Body;
     
   Task_1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_1_Init'Access,
      Body_Ac  => Task_1_Body'Access,
      Priority => Priority);

   ------------
   -- Task_2 --
   ------------
 
#if not MEASURE_SIZE then  
   Task_2_Executed : Boolean := False;
#end if;
   Next_Time_2 : RT.Time;
   Period_2 : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms);
   
   procedure Task_2_Init is
   begin
      Next_Time_2 := RT.Clock;
   end Task_2_Init;

   procedure Task_2_Body is
   begin
#if not MEASURE_SIZE then
      DIO.Put ("Task2");
      Task_2_Executed := True;
#end if;
      
      Next_Time_2 := Next_Time_2 + Period_2;
      delay until Next_Time_2;
   end Task_2_Body;
     
   Task_2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_2_Init'Access,
      Body_Ac  => Task_2_Body'Access,
      Priority => Priority);

   ------------
   -- Task_3 --
   ------------
 
#if not MEASURE_SIZE then  
   Task_3_Executed : Boolean := False;
#end if;
   Next_Time_3 : RT.Time;
   Period_3 : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms);
   
   procedure Task_3_Init is
   begin
      Next_Time_3 := RT.Clock;
   end Task_3_Init;

   procedure Task_3_Body is
   begin
#if not MEASURE_SIZE then
      DIO.Put ("Task3");
      Task_3_Executed := True;
#end if;
      
      Next_Time_3 := Next_Time_3 + Period_3;
      delay until Next_Time_3;
   end Task_3_Body;
     
   Task_3 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_3_Init'Access,
      Body_Ac  => Task_3_Body'Access,
      Priority => Priority);

   ------------
   -- Task_4 --
   ------------
 
#if not MEASURE_SIZE then  
   Task_4_Executed : Boolean := False;
#end if;
   Next_Time_4 : RT.Time;
   Period_4 : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms);
   
   procedure Task_4_Init is
   begin
      Next_Time_4 := RT.Clock;
   end Task_4_Init;

   procedure Task_4_Body is
   begin
#if not MEASURE_SIZE then
      DIO.Put ("Task4");
      Task_4_Executed := True;
#end if;
      
      Next_Time_4 := Next_Time_4 + Period_4;
      delay until Next_Time_4;
   end Task_4_Body;
     
   Task_4 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_4_Init'Access,
      Body_Ac  => Task_4_Body'Access,
      Priority => Priority);

   ------------
   -- Task_5 --
   ------------
 
#if not MEASURE_SIZE then  
   Task_5_Executed : Boolean := False;
#end if;
   Next_Time_5 : RT.Time;
   Period_5 : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms);
   
   procedure Task_5_Init is
   begin
      Next_Time_5 := RT.Clock;
   end Task_5_Init;

   procedure Task_5_Body is
   begin
#if not MEASURE_SIZE then
      DIO.Put ("Task5");
      Task_5_Executed := True;
#end if;
      
      Next_Time_5 := Next_Time_5 + Period_5;
      delay until Next_Time_5;
   end Task_5_Body;
     
   Task_5 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_5_Init'Access,
      Body_Ac  => Task_5_Body'Access,
      Priority => Priority);

   ------------
   -- Task_6 --
   ------------
 
#if not MEASURE_SIZE then  
   Task_6_Executed : Boolean := False;
#end if;
   Next_Time_6 : RT.Time;
   Period_6 : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms);
   
   procedure Task_6_Init is
   begin
      Next_Time_6 := RT.Clock;
   end Task_6_Init;

   procedure Task_6_Body is
   begin
#if not MEASURE_SIZE then
      DIO.Put ("Task6");
      if Task_6_Executed then
         -- Second activation
             
         Tests_Reports.Assert (Task_1_Executed and Task_2_Executed and
                                 Task_3_Executed and Task_4_Executed and
                                   Task_5_Executed);
         Tests_Reports.Test_OK; 
 
      else
         --  First activation
       
         Tests_Reports.Assert (not Task_1_Executed and not Task_2_Executed and
                                 not Task_3_Executed and not Task_4_Executed and
                                   not Task_5_Executed);

      end if;
      
      Task_6_Executed := True;
#end if;
      
      Next_Time_6 := Next_Time_6 + Period_6;
      delay until Next_Time_6;
   end Task_6_Body;
     
   Task_6 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_6_Init'Access,
      Body_Ac  => Task_6_Body'Access,
      Priority => Priority + 1);

end Size_Periodic_Tasks_Six_Tasks;
