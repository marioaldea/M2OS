----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Secondary task activated by a simple entry.
--  The secondary task executes two activations in a row,
--  since the entry is opened and the main task is sleeping.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;

with Tests_Reports;
with M2.Direct_IO;

with PO_Entry_2;

procedure PO_Entry_Test is
   package DIO renames M2.Direct_IO;

   use type Ada.Real_Time.Time;
   Next_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (50);

   Loop_Counter : Integer := 0;

begin
   DIO.Put ("-PO_Entry_Test-");

   Next_Time := Ada.Real_Time.Clock;
   loop
      DIO.Put ("-Main-");
      DIO.Put (Integer (PO_Entry_2.Step_Counter));
      DIO.Put (Loop_Counter);

      case Loop_Counter is
         when 0 =>
            Tests_Reports.Assert (PO_Entry_2.Step_Counter = 0);

         when 1 =>
            Tests_Reports.Assert (PO_Entry_2.Step_Counter = 3);

         when 2 =>
            Tests_Reports.Assert (PO_Entry_2.Step_Counter = 5);

         when 3 =>
            Tests_Reports.Assert (PO_Entry_2.Step_Counter = 7);

         when others =>
            Tests_Reports.Assert (False);
      end case;

      PO_Entry_2.Step_Counter := PO_Entry_2.Step_Counter + 1;
      Loop_Counter := Loop_Counter + 1;

      PO_Entry_2.PO.Open;

      if Loop_Counter = PO_Entry_2.Num_Of_Loops then
         Tests_Reports.Test_OK;
      end if;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end loop;

end PO_Entry_Test;
