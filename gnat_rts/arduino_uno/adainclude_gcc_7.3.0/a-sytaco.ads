--  This is the M2OS version of this package

with System.OS_Interface;

package Ada.Synchronous_Task_Control is
   pragma Preelaborate;
   --  In accordance with Ada 2005 AI-362

   type Suspension_Object is limited private;

   procedure Set_True (S : in out Suspension_Object);

   procedure Set_False (S : in out Suspension_Object);

   function Current_State (S : Suspension_Object) return Boolean;

   procedure Suspend_Until_True (S : in out Suspension_Object);
   pragma Export_Procedure (Suspend_Until_True,
                            Mechanism => (S => Reference));

private

   package OSI renames System.OS_Interface;

   type Suspension_Object is record
      State : Boolean := False;
      pragma Atomic (State);

      Suspended_Thread : OSI.Thread_Ac := OSI.Null_Thread_Ac;
   end record;

end Ada.Synchronous_Task_Control;
