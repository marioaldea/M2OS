----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Read temperature with and IR thermometer and displays it in
--  a 4 digit alphanumeric display.
--
--  Uses:
--   - "Grove - Single-Point Infrared Thermometer - MLX90614 DCC with 35� FOV"
--     https://www.seeedstudio.com/Grove-Thermal-Imaging-Camera-MLX90614-DCC-IR-Array-with-35-FOV-p-4657.html
--   - "Seeed Digital Tube HT16K33".
--     https://github.com/Seeed-Studio/Seeed_Alphanumeric_Display_HT16K33/blob/master/grove_alphanumeric_display.h    
--
--     --------------------                 ----------------------
--     |                  |                 |                    |
--     | Seeed         SCL|-----------------|A5      Arduino Uno |
--     | Digital Tube  SDA|-----------------|A4                  |
--     | HT16K33       VCC|-----------------|5V                  |
--     |               GND|-----------------|GND                 |
--     |                  |                 |                    |
--     --------------------                 |                    |
--                                          |                    |
--     -----------------                    |                    |
--     |               |                    |                    |
--     | MLX90614   SCL|--------------------|A5                  |
--     |            SDA|--------------------|A4                  |
--     |            VCC|--------------------|5V                  |
--     |            GND|--------------------|GND                 |
--     |               |                    |                    |
--     -----------------                    ----------------------
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with AdaX_Dispatching_Stack_Sharing;
with Interfaces.C;

with Arduino.IR_Therm_MLX90614;
with Arduino.Alphanumeric_Display;
with M2.Direct_IO;

procedure IR_Thermometer_MLX90614_Digital_Display is
   package DIO renames M2.Direct_IO;
   package IR_Therm renames Arduino.IR_Therm_MLX90614;
   package Display renames Arduino.Alphanumeric_Display;
   
   use type Arduino.Boolean8;
   
   Temp : Float;
begin
   DIO.Put_Line ("IR Thermometer MLX90614 with display");
   
   if not IR_Therm.Initialize then
      raise Program_Error;
   end if;
   
   Display.Begin_Alphanumeric_Display;
   Display.Set_Blink_Rate (Display.BLINK_OFF);
   
   loop      
      if IR_Therm.Read_Temps then
         Temp := IR_Therm.Temp_Ambient;       
         DIO.Put (" Temp Ambient/Object: ");
         DIO.Put (Temp);
         
      
         Temp := IR_Therm.Temp_Object;       
         DIO.Put ("  ");
         DIO.Put (Temp);       
         DIO.Put ("  ");
         
         DIO.New_Line;
         
         Display.Display_Num (Interfaces.Unsigned_32 (Temp * 100.0));
         
      else
         DIO.Put_Line ("Not available");
      end if;

      Arduino.C_Delay (2_000);
   end loop;
   
end IR_Thermometer_MLX90614_Digital_Display;
