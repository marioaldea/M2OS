pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;
with Ada.Synchronous_Task_Control;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;

with Measurements_HiRes;

package body CS_Activations_2 is
   package DIO renames M2.Direct_IO;
   package Measurements renames Measurements_HiRes;

   --------
   -- PO --
   --------

   protected body PO is

      entry Wait when Is_Open is
      begin
         --DIO.Put ("-PO.Wait-");
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         --DIO.Put ("-PO.Open-");
         Is_Open := True;
      end Open;
   end PO;

   --------------
   -- PO_Param --
   --------------

   protected body PO_Param is

      entry Wait (Data_Out : Access Integer) when Is_Open is
      begin
         --DIO.Put ("-PO.Wait-");

         Data_Out.All := Counter;
         --DIO.Put (Data_Out.All);
         Is_Open := False;
      end Wait;

      procedure Open is
      begin
         --DIO.Put ("-PO.Open-");
         --DIO.Put (Counter);

         Counter := Counter + 1;
         Is_Open := True;
      end Open;
   end PO_Param;

   ------------
   -- Task_2 --
   ------------

   procedure Task_2_Init is
   begin
      Ada.Synchronous_Task_Control.Set_True (SO);

      Ada.Synchronous_Task_Control.Suspend_Until_True (SO);
      --  To ensure this task starts its body after Task_1
   end Task_2_Init;

   procedure Task_2_Body is
   begin
      --  DIO.Put ("-Task_2-");

      Loop_Counter := Loop_Counter + 1;

      Measurements.Start_Measurement;

      case Suspension_Primitive is
         when Suspension_Object =>
            Ada.Synchronous_Task_Control.Set_True (SO);
         when PO_Entry =>
            PO.Open;
         when PO_Entry_Param =>
            PO_Param.Open;
         when others =>
            null;
      end case;

      System.OS_Interface.Running_Thread_Ends_Job_Without_Blocking;
   end Task_2_Body;

   ------------
   -- Task_2 --
   ------------

   Task_2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_2_Init'Access,
      Body_Ac  => Task_2_Body'Access,
      Priority => Task_2_Prio);

end CS_Activations_2;
