--  Partial implementation for M2OS and Arduino Uno

with System;
with System.Interrupts;
--  with System.Multiprocessors;

package Ada.Interrupts is
   type Interrupt_Id is new System.Interrupts.Interrupt_ID;

   type Parameterless_Handler is
     access protected procedure;

   function Is_Reserved (Interrupt : Interrupt_Id)
                         return Boolean is (False);

--     function Is_Attached (Interrupt : Interrupt_Id)
--                           return Boolean;

--     function Current_Handler (Interrupt : Interrupt_Id)
--                               return Parameterless_Handler;

--     procedure Attach_Handler
--        (New_Handler : in Parameterless_Handler;
--         Interrupt   : in Interrupt_Id);
--
--     procedure Exchange_Handler
--        (Old_Handler : out Parameterless_Handler;
--         New_Handler : in Parameterless_Handler;
--         Interrupt   : in Interrupt_Id);
--
--     procedure Detach_Handler
--       (Interrupt : in Interrupt_Id);
--
--     function Reference (Interrupt : Interrupt_Id)
--                         return System.Address;

--     function Get_CPU (Interrupt : Interrupt_Id)
--                       return System.Multiprocessors.CPU_Range;

end Ada.Interrupts;
