----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  MLX90614 Single and Dual Zone Infra Red Thermometer.
--  Read temperature varying the emissivity.
--  The program serves as demo and also can be used to know the emissivity
--  of a surface whose temperature is known.
--
--  Tested with the "Grove - Single-Point Infrared Thermometer - MLX90614 DCC
--  with 35� FOV"
--    https://www.seeedstudio.com/Grove-Thermal-Imaging-Camera-MLX90614-DCC-IR-Array-with-35-FOV-p-4657.html
--
--     -----------------                 ----------------------
--     |               |                 |                    |
--     | MLX90614   SCL|-----------------|A5      Arduino Uno |
--     |            SDA|-----------------|A4                  |
--     |            VCC|-----------------|5V                  |
--     |            GND|-----------------|GND                 |
--     |               |                 |                    |
--     -----------------                 ----------------------
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino.IR_Therm_MLX90614;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure IR_Thermometer_MLX90614_Demo is
   package DIO renames M2.Direct_IO;
   package IR_Therm renames Arduino.IR_Therm_MLX90614;
   
   use type Arduino.Boolean8;
   
   Temp : Float;
--     Emissivity : IR_Therm.Emissivity := 0.1;
--     Emissivity_Increment : constant Float := 0.1;
--     
--     type Counter_Range is mod 5;
--     Counter : Counter_Range := 0;
begin
   DIO.Put_Line ("IR Thermometer MLX90614 Demo");
   
   if not IR_Therm.Initialize then
      raise Program_Error;
   end if;
   
   loop      
      if IR_Therm.Read_Temps then
         Temp := IR_Therm.Temp_Ambient;       
         DIO.Put (" Temp Ambient/Object: ");
         DIO.Put (Temp);
      
         Temp := IR_Therm.Temp_Object;       
         DIO.Put ("  ");
         DIO.Put (Temp);       
         DIO.Put ("  ");
         
         DIO.New_Line;
         
      else
         DIO.Put_Line ("Not available");
      end if;

      Arduino.C_Delay (2_000);
   end loop;
   
end IR_Thermometer_MLX90614_Demo;
