----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: David Garci�a Villaescusa (garciavd@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

package M2.HAL.Timer is
   --  pragma Pure;
   pragma Preelaborate;

   Now : HWTime := 0; --  Current time

   HWT_HZ : constant := 1_000;
   --  'HWT_HZ' is the number of 'HWTime' units per second.

   type Timer_Ticks is mod 600000;

   function Get_Counter return Timer_Ticks;
   pragma Inline_Always (Get_Counter);

end M2.HAL.Timer;
