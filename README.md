# M2OS

The main objective of this project is to allow programming small microcontrollers
using concurrent Ada applications. In particular the project is focused on the
Arduino Uno platform, although there are some other targets supported (namely STM32F4 board and Epiphany manycore).

The possibility of writing concurrent applications (using Ada tasks) and the rest
of advanced features of the Ada language (i.e. programing by contract,
strong typing, representation clauses, static compiler checks, among others) are
the benefits offered by our approach in contrast to the traditional way of
programming small microcontrollers (typically based on sequential C/C++
applications). 

M2OS is a small RTOS (Real-Time Operating System) written in Ada that implements
a stack sharing, non-preemptive scheduling algorithm.

The M2OS kernel is the base of a RTS (Run Time System) for the GNAT Ada compiler
which supports stack sharing, non-preemptive Ada Tasks with the same restrictions
that the Ravenscar profile.

M2OS allows to execute concurrent Ada applications on small microcontrollers
with very tight memory constraints. For example the footprint of M2OS+RTS in an
Arduino Uno board for an application with two tasks and a suspension object
is below 4KB/500B (FLASH/RAM) and the maximum stack usage remains under 100B. 

A LibAdaLang based tool is provided to transform standard Ada code that uses tasks
and protected objects into a code that performs the calls required for the
M2OS kernel thread model.

Further information can be found in the [INSTALL](INSTALL.md) file and in the
[Wiki](https://gitlab.com/marioaldea/M2OS/wikis/M2OS-Wiki).

### License

M2OS is licensed under the terms of the GNU General Public License version 3 (GPLv3).
See the LICENSE file for more information.