
with Libadalang.Analysis;  use Libadalang.Analysis;
with Libadalang.Iterators; use Libadalang.Iterators;

with GNATCOLL.Projects; use GNATCOLL.Projects;

with Ada.Containers.Vectors;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package M2OS_Utils is

   package String_Vectors is new Ada.Containers.Vectors
     (Positive, Unbounded_String);

   function "+" (S : String) return Unbounded_String
                 renames To_Unbounded_String;
   function "+" (S : Unbounded_String) return String renames To_String;

   procedure Show_Diagnostics (Unit: Analysis_Unit);

   function Corresponding_Spec (Filename : String) return String;

   procedure Put_Node (N : Ada_Node);
   --  Put the image of N on the standard output

   procedure Put_Node_Text (N : Ada_Node);
   --  Put the text of N on the standard output

   procedure Run_Find
     (Ctx  : Analysis_Context;
      Filename, Label : String;
      Predicate       : Ada_Node_Predicate);
   --  Load the Filename unit and then show all nodes that Predicate matches in
   --  this unit.

   procedure Load_Project (Name    : String;
                           Provider: out Unit_Provider_Reference;
                           The_Project : out Project_Type;
                           Files   : out String_Vectors.Vector);

   procedure Load_Default_Project (Provider: out Unit_Provider_Reference;
                                   Files   : out String_Vectors.Vector);

   function Is_Ada_Main (Filename     : String;
                         The_Project  : Project_Type) return Boolean;

end M2OS_Utils;
