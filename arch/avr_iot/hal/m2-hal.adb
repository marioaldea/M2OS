--  AVRIoT WG board (ATmega4808) version of this package

with System;
with System.Machine_Code; use System.Machine_Code;
with Interfaces;

with M2.HAL.Timer;

with M2.Debug;

package body M2.HAL is

   package DBG renames M2.Debug;
   use type Interfaces.Unsigned_16;
   use type System.Address;

   Initialized : Boolean := False;

   Stack_Base : Address_Int := 0;
   pragma Volatile (Stack_Base);

   ----------------------------------
   --  ATmega4808 SRAM memory map  --
   ----------------------------------

   Internal_SRAM_Start_Addr    : constant := 16#2800#;
   Internal_SRAM_Size_In_Bytes : constant := 16#1800#;

   ----------------------
   --  Stack register  --
   ----------------------
   --  SPL_Addr                 : constant Address    := 16#0d#;
   SP : Address_Int;
   for SP'Size use 16;
   for SP'Address use 16#3d#;
   pragma Volatile (SP);

   SPL_Addr                 : constant System.Address    := 16#3d#;
   SPL : Interfaces.Unsigned_8;
   for SPL'Address use SPL_Addr;
   pragma Volatile (SPL);

--     -----------------
--     -- R30 and R31 --
--     -----------------
--     --  Z registers used for the indirect call (icall) assembler instruction
--     R30 : Procedure_Ac;
--  --   for R30'Size use Procedure_Ac'Size;
--     for R30'Address use 16#1e#;
--     pragma Import (C, R30);
--     pragma Volatile (R30);
--
--     R31 : Procedure_Ac;
--  --   for R31'Size use Procedure_Ac'Size;
--     for R31'Address use 16#1f#;
--     pragma Import (C, R31);
--     pragma Volatile (R31);

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch
     (New_Context : Thread_Context)
   is
--      function UC is new Ada.Unchecked_Conversion (Procedure_Ac,
--                                                   Integer_Address);
--      Dummy : Interfaces.Unsigned_8;
      procedure Jump_To_Context (Funct : Thread_Context;
                                 Stack_Top : Address_Int);
      pragma Import (C, Jump_To_Context, "marte_hal_jump_to_context");

   begin
      pragma Debug (Debug.Assert (New_Context /= null));
      pragma Debug (Debug.Assert (Stack_Base /= 0));

      --  Jump to the task body address (stored in New_Context)

      Jump_To_Context (New_Context, Stack_Base);

      --  Never returns

      pragma Debug (Debug.Assert (False));
   end Context_Switch;

--     ------------------
--     -- Save_Context --
--     ------------------
--
--     procedure Save_Context (Context : access Thread_Context;
--                             Job_Body_Ac : Procedure_Ac) is
--     begin
--        Context.all := Job_Body_Ac;
--     end Save_Context;

   --------------------------
   -- Stack related values --
   --------------------------

   function To_Int is new Ada.Unchecked_Conversion (System.Address,
                                                    Address_Int);

   --  Boundaries of the stack defined by the linker script file.
   --  The usable stack area is:
   --     SRAM_End_Addr down to Data_End'Address

   SRAM_End_Addr : constant := Internal_SRAM_Start_Addr +
     Internal_SRAM_Size_In_Bytes - 1;
   --  In Arduino Uno:
   --     SRAM_End_Addr = 0x100 + 0x800 - 1 =
   --                     0x8FF = 2303

   Data_End : Integer;
   pragma Import (C, Data_End, "__bss_end");
   --  end of .data section (defined in linker script).

   Stack_Not_Used_Magic : constant := 16#AB#;

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

--     procedure Set_Global_Stack_Limit is
--        SPL2 : Interfaces.Unsigned_8;
--     begin
--  --      pragma Debug (Debug.Assert (Stack_Base = 0, ""));
--
--        SPL2 := SPL;
--        --  M2OS: sin esta linea, la siguiente no funcionaba bien (!!)
--
--        Stack_Base := SP;
--     end Set_Global_Stack_Limit;

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Address_Int is
   begin
      return SRAM_End_Addr - To_Int (Data_End'Address) + 1;
   end Get_Stack_Max_Size_In_Bytes;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Address_Int is
      SPL2 : Interfaces.Unsigned_8;
   begin
      SPL2 := SPL;
      --  M2OS: sin esta linea, la siguiente no funcionaba bien (!!)

      return SRAM_End_Addr - Address_Int (SP) + 1;
   end Get_Current_Stack_Size_In_Bytes;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Address_Int is
   begin
      return Address_Int (SP) - To_Int (Data_End'Address);
   end Get_Current_Stack_Margin_In_Bytes;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area is
--        Stack : array (1 .. Get_Stack_Max_Size_In_Bytes)
--          of Interfaces.Unsigned_8;
--        for Stack'Address use Data_End'Address;
   begin
--        for I in Address_Int range Stack'First ..
--          Stack'Last - Get_Current_Stack_Size_In_Bytes loop
--           Stack (I) := Stack_Not_Used_Magic;
--        end loop;
      null;
   end Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Address_Int is
      Stack : array (1 .. Get_Stack_Max_Size_In_Bytes)
        of Interfaces.Unsigned_8;
      for Stack'Address use Data_End'Address;
      I : Address_Int := 1;
      use type Interfaces.Unsigned_8;
   begin
      loop
         exit when Stack (I) /= Stack_Not_Used_Magic;
         I := I + 1;
      end loop;
      return Stack'Last - I + 1;
   end Get_Max_Stack_Usage_In_Bytes;

   ----------------------------
   -- Are_Interrupts_Enabled --
   ----------------------------

   function Are_Interrupts_Enabled return Boolean is

      use type Interfaces.Unsigned_8;

      --  Status register

      SREG : Interfaces.Unsigned_8;
      for SREG'Address use 16#3f#;
      pragma Volatile (SREG);

      --  Interrupt Enable bit

      Interrupt_Flag : constant := 16#80#;

   begin
      return (SREG and Interrupt_Flag) = Interrupt_Flag;
   end Are_Interrupts_Enabled;

   ----------------
   -- Get_HWTime --
   ----------------

   --  function Get_HWTime return HWTime renames Timer.Get_Time;
   --  To avoid error:
   --  "sorry, unimplemented: nested function trampolines not supported on
   --  This target"

   function Get_HWTime return HWTime is
   begin
      pragma Assert (Initialized);

      return Timer.Now;
   end Get_HWTime;

   --------------------
   -- Initialization --
   --------------------

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac) is
   begin
      pragma Assert (not Initialized);
      pragma Assert (To_Int (Data_End'Address) /= 0);

      HAL.Timer.Initialize (OS_Tick_Handler);

      Stack_Base := SRAM_End_Addr;

      Initialized := True;
   end Initialization;

end M2.HAL;
