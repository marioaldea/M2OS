----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

pragma Profile (Ravenscar);
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);
with M2.Direct_IO;

with System;
with Ada.Real_Time;

with Tests_Reports;
with Arduino;
with M2.HAL.Timer;

procedure Arduino_Delay_Test is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;
   package HT renames M2.HAL.Timer;
   package HAL renames M2.HAL;

   use type RT.Time, RT.Time_Span, HT.Timer_Ticks, HAL.HWTime;

   T_Ini, T_Fin : RT.Time;
   HWT_Ini, HWT_Fin : HAL.HWTime;

   Delay_MS_Short : constant := 1;
   Delay_MS_Medium : constant := 123;
   Delay_MS_Long : constant := 1_000;

   function To_Millis (D : Duration) return Integer is
   begin
      return Integer (D * 1_000);
   end To_Millis;

   procedure Put_Duration (D : Duration) is
      S : Integer := Integer (D);
   begin
      DIO.Put (S);
      DIO.Put ("s");
      DIO.Put (To_Millis (D) - S * 1_000);
      DIO.Put ("ms");
   end Put_Duration;

begin
   DIO.Put_Line ("Arduino_Delay_Test");

   --  Delay_MS_Short

   HWT_Ini := HAL.Get_HWTime;
   Arduino.C_Delay (Delay_MS_Short);
   HWT_Fin := HAL.Get_HWTime;

   DIO.Put ("Delay_MS_Short (");
   DIO.Put (Integer (Delay_MS_Short));
   DIO.Put ("ms)  Measured:");
   DIO.Put (Integer (HWT_Fin - HWT_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_MS_Short,
      Val2   => Integer (HWT_Fin - HWT_Ini),
      Margin => 1);

   --  Delay_MS_Medium (1)

   T_Ini := RT.Clock;
   Arduino.C_Delay (Delay_MS_Medium);
   T_Fin := RT.Clock;

   DIO.Put ("Delay_MS_Medium (");
   DIO.Put (Integer (Delay_MS_Medium));
   DIO.Put ("ms)  Measured:");
   Put_Duration (RT.To_Duration (T_Fin - T_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_MS_Medium,
      Val2   => To_Millis (RT.To_Duration (T_Fin - T_Ini)),
      Margin => 10);

   --  Delay_MS_Medium (2)

   HWT_Ini := HAL.Get_HWTime;
   Arduino.C_Delay (Delay_MS_Medium);
   HWT_Fin := HAL.Get_HWTime;

   DIO.Put ("Delay_MS_Medium (");
   DIO.Put (Integer (Delay_MS_Medium));
   DIO.Put ("ms)  Measured:");
   DIO.Put (Integer (HWT_Fin - HWT_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_MS_Medium,
      Val2   => Integer (HWT_Fin - HWT_Ini),
      Margin => 1);

   --  Delay_MS_Long (1)

   T_Ini := RT.Clock;
   Arduino.C_Delay (Delay_MS_Long);
   T_Fin := RT.Clock;

   DIO.Put ("Delay_MS_Long (");
   DIO.Put (Integer (Delay_MS_Long));
   DIO.Put ("ms)  Measured:");
   Put_Duration (RT.To_Duration (T_Fin - T_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_MS_Long,
      Val2   => To_Millis (RT.To_Duration (T_Fin - T_Ini)),
      Margin => 10);

   --  Delay_MS_Long (2)

   HWT_Ini := HAL.Get_HWTime;
   Arduino.C_Delay (Delay_MS_Long);
   HWT_Fin := HAL.Get_HWTime;

   DIO.Put ("Delay_MS_Long (");
   DIO.Put (Integer (Delay_MS_Long));
   DIO.Put ("ms)  Measured:");
   DIO.Put (Integer (HWT_Fin - HWT_Ini));
   DIO.New_Line;
   Tests_Reports.Assert_Equal
     (Val1   => Delay_MS_Long,
      Val2   => Integer (HWT_Fin - HWT_Ini),
      Margin => 1);

   Tests_Reports.Test_OK;
end Arduino_Delay_Test;
