----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with System;
with M2.Kernel.TCBs;
with M2.Kernel.Dispatcher;
with M2.HAL;

package M2.Kernel.Scheduler is

   --  pragma Preelaborate;

   procedure Thread_Body_Wrapper;
   --  Executed in the first activation of any task different from the
   --  enviroment task. Calls to the thread init body with the appropiate
   --  arguments.

   procedure Tick_Handler (Now : HAL.HWTime);

   procedure New_Thread (Th_Ac : TCBs.TCB_Ac;
                         Th_Body_Ac : HAL.Code_Address;
                         Priority : M2.Kernel.Priority;
                         Args : System.Address)
     with Export, Convention => C,
       External_Name => "m2__kernel__api__create_thread";

   procedure Activate_Blocked_Thread (Th_Ac : M2.Kernel.TCBs.TCB_Ac);
   pragma Export (C, Activate_Blocked_Thread, "m2_activate_blocked_thread");

   procedure Block_Running_Thread;
   pragma Export (C, Block_Running_Thread, "m2_block_running_thread");

   procedure Running_Thread_Ends_Job_Without_Blocking;
   pragma Export (C, Running_Thread_Ends_Job_Without_Blocking,
                  "m2_running_thread_ends_job_without_blocking");

   procedure Suspend_Running_Thread (To_Abs_Time : HAL.HWTime);
   pragma Export (C, Suspend_Running_Thread,
                  "m2__kernel__scheduler__suspend_running_thread");

   procedure Yield_To_Higher
     renames M2.Kernel.Dispatcher.Dispatch_After_Yield_To_Higher;


end M2.Kernel.Scheduler;
