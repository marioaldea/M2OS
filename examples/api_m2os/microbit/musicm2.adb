with MicroBit.Music; use MicroBit.Music;
with Ada.Text_IO;

with AdaX_Dispatching_Stack_Sharing;

procedure MusicM2 is

   Pirates_of_the_Caribbean : constant MicroBit.Music.Melody :=
     ((E4,   125), (G4, 125), (A4,   250), (A4, 125), (Rest, 125),
      (A4,   125), (B4,   125), (C5,   250), (C5,   125), (Rest, 125),
      (C5,   125), (D5,   125), (B4,   250), (B4,   125), (Rest, 125),
      (A4,   125), (G4,   125), (A4,   375), (Rest, 125),

      (E4,   125), (G4, 125), (A4,   250), (A4, 125), (Rest, 125),
      (A4,   125), (B4,   125), (C5,   250), (C5,   125), (Rest, 125),
      (C5,   125), (D5,   125), (B4,   250), (B4,   125), (Rest, 125),
      (A4,   125), (G4,   125), (A4,   375), (Rest, 125),

      (E4,   125), (G4, 125), (A4,   250), (A4, 125), (Rest, 125),
      (A4,   125), (C5,   125), (D5,   250), (D5,   125), (Rest, 125),
      (D5,   125), (E5,   125), (F5,   250), (F5,   125), (Rest, 125),
      (E5,   125), (D5,   125), (E5,   125), (A4,   250), (Rest, 125),

      (A4,   125), (B4, 125), (C5,   250), (C5, 125), (Rest, 125),
      (D5,   250), (E5,   125), (A4,   250), (Rest, 125),
      (A4,   125), (C5,   125), (B4,   250), (B4,   125), (Rest, 125),
      (C5,   125), (A4,   125), (B4,   375), (Rest, 375)
     );
begin
   Ada.Text_IO.Put_Line ("Music working in M2OS!");
   --  Loop forever
   loop
      --  Play Pirates of the Caribbean on pin 0
      MicroBit.Music.Play (0, Pirates_of_the_Caribbean);
   end loop;
end MusicM2;
