pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with stdint_h;
with System;
with stddef_h;

package avr_eeprom_h is

   --  unsupported macro: EEMEM __attribute__((section(".eeprom")))
   --  arg-macro: procedure eeprom_is_ready ()
   --    bit_is_clear (EECR, EEPE)
   --  arg-macro: procedure eeprom_busy_wait ()
   --    do {} while (noteeprom_is_ready())
   function eeprom_read_byte (uu_p : access stdint_h.uint8_t) return stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:139
   pragma Import (C, eeprom_read_byte, "eeprom_read_byte");

   function eeprom_read_word (uu_p : access stdint_h.uint16_t) return stdint_h.uint16_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:144
   pragma Import (C, eeprom_read_word, "eeprom_read_word");

   function eeprom_read_dword (uu_p : access stdint_h.uint32_t) return stdint_h.uint32_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:149
   pragma Import (C, eeprom_read_dword, "eeprom_read_dword");

   function eeprom_read_float (uu_p : access float) return float;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:154
   pragma Import (C, eeprom_read_float, "eeprom_read_float");

   procedure eeprom_read_block
     (uu_dst : System.Address;
      uu_src : System.Address;
      uu_n : stddef_h.size_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:160
   pragma Import (C, eeprom_read_block, "eeprom_read_block");

   procedure eeprom_write_byte (uu_p : access stdint_h.uint8_t; uu_value : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:166
   pragma Import (C, eeprom_write_byte, "eeprom_write_byte");

   procedure eeprom_write_word (uu_p : access stdint_h.uint16_t; uu_value : stdint_h.uint16_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:171
   pragma Import (C, eeprom_write_word, "eeprom_write_word");

   procedure eeprom_write_dword (uu_p : access stdint_h.uint32_t; uu_value : stdint_h.uint32_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:176
   pragma Import (C, eeprom_write_dword, "eeprom_write_dword");

   procedure eeprom_write_float (uu_p : access float; uu_value : float);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:181
   pragma Import (C, eeprom_write_float, "eeprom_write_float");

   procedure eeprom_write_block
     (uu_src : System.Address;
      uu_dst : System.Address;
      uu_n : stddef_h.size_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:187
   pragma Import (C, eeprom_write_block, "eeprom_write_block");

   procedure eeprom_update_byte (uu_p : access stdint_h.uint8_t; uu_value : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:193
   pragma Import (C, eeprom_update_byte, "eeprom_update_byte");

   procedure eeprom_update_word (uu_p : access stdint_h.uint16_t; uu_value : stdint_h.uint16_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:198
   pragma Import (C, eeprom_update_word, "eeprom_update_word");

   procedure eeprom_update_dword (uu_p : access stdint_h.uint32_t; uu_value : stdint_h.uint32_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:203
   pragma Import (C, eeprom_update_dword, "eeprom_update_dword");

   procedure eeprom_update_float (uu_p : access float; uu_value : float);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:208
   pragma Import (C, eeprom_update_float, "eeprom_update_float");

   procedure eeprom_update_block
     (uu_src : System.Address;
      uu_dst : System.Address;
      uu_n : stddef_h.size_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/eeprom.h:214
   pragma Import (C, eeprom_update_block, "eeprom_update_block");

end avr_eeprom_h;
