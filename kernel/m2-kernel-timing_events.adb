----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Unchecked_Conversion;

package body M2.Kernel.Timing_Events is
   use type HAL.HWTime;

   pragma Compile_Time_Error
     (Timing_Event'Size /= System.Timing_Event_Size_In_Bytes * 8,
      "Unexpected size of Timing_Event");

   -----------------------
   -- Local subprograms --
   -----------------------

   procedure Cancel_Handler (Event_Ac : Timing_Event_Ac);

   -----------
   -- Queue --
   -----------

   --  Timing events ordered by Expiration_Time.

   Q : Timing_Event_Ac := null;

   -----------------
   -- Set_Handler --
   -----------------

   procedure Set_Handler (Event_Ac : Timing_Event_Ac;
                          At_Time : Time;
                          Handler : Timing_Event_Handler) is
   begin
      Cancel_Handler (Event_Ac);

      if Handler = null then
         return;
      end if;

      Event_Ac.Expiration_Time := At_Time;
      Event_Ac.Handler := Handler;

      --  Enqueue event

      if Q = Null or else Q.Expiration_Time > Event_Ac.Expiration_Time then
         Event_Ac.Next := Q;
         Q := Event_Ac;

      else
         declare
            Aux : Timing_Event_Ac := Q;
         begin
            loop
               if Aux.Next = Null or else
                 Aux.Next.Expiration_Time > Event_Ac.Expiration_Time
               then
                  Event_Ac.Next := Aux.Next;
                  Aux.Next := Event_Ac;
                  exit;
               end if;
               Aux := Aux.Next;
            end loop;
         end;
      end if;
   end Set_Handler;

   --------------------------
   -- Set_Handler_Relative --
   --------------------------

   --  Not allowed in Ravenscar (pragma Restrictions No_Relative_Delay)

--     procedure Set_Handler_Relative (Event_Ac : Timing_Event_Ac;
--                                     In_Time : Time;
--                                     Handler : Timing_Event_Handler) is
--     begin
--        Set_Handler (Event_Ac, In_Time + HAL.Get_HWTime, Handler);
--     end Set_Handler_Relative;

   ---------------------
   -- Current_Handler --
   ---------------------

   function Current_Handler (Event : Timing_Event)
                             return Timing_Event_Handler is
   begin
      return Event.Handler;
   end Current_Handler;

   -----------------
   -- Clear_Event --
   -----------------

   procedure Clear_Event (Event_Ac : Timing_Event_Ac) is
   begin
      Event_Ac.Expiration_Time := HAL.HWTime'First;
      Event_Ac.Handler := null;
   end Clear_Event;

   --------------------
   -- Cancel_Handler --
   --------------------

   procedure Cancel_Handler (Event_Ac : Timing_Event_Ac) is
   begin
      if Event_Ac.Handler /= null then
         if M2.Allow_Timing_Events_Cancellation'First then
            --  Dequeue event

            if Event_Ac = Q then
               Q := Q.Next;
            else
               declare
                  Aux : Timing_Event_Ac := Q;
               begin
                  loop
                     pragma Assert (Aux /= null);

                     if Aux.Next = Event_Ac then
                        Aux.Next := Event_Ac.Next;
                        exit;
                     end if;
                     Aux := Aux.Next;
                  end loop;
               end;
            end if;
         else
            raise Program_Error;
         end if;
      end if;

      Clear_Event (Event_Ac);
   end Cancel_Handler;

   --------------------
   -- Cancel_Handler --
   --------------------

   procedure Cancel_Handler (Event_Ac : Timing_Event_Ac;
                             Cancelled : out Boolean_C) is
   begin
      Cancelled := Boolean_C (Event_Ac.Handler /= null);
      Cancel_Handler (Event_Ac);
   end Cancel_Handler;

   -------------------
   -- Time_Of_Event --
   -------------------

   function Time_Of_Event (Event : Timing_Event) return Time is
     (Event.Expiration_Time);

   ----------------
   -- Queue_Head --
   ----------------

   function Queue_Head return Timing_Event_Ac is
   begin
      return Q;
   end Queue_Head;

   ------------------------
   -- Queue_Dequeue_Head --
   ------------------------

   procedure Queue_Dequeue_Head is
   begin
      pragma Assert (Q /= null);

      Q := Q.Next;
   end Queue_Dequeue_Head;

end M2.Kernel.Timing_Events;
