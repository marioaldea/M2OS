#include "../reports/tests_reports.h"
#include <stdint.h>
#include <pthread.h>
#include <stdio.h>
#include <time.h>

#define NUM_OF_MEAS 10000

int main(int argc, char **argv) {
  m2osinit();
  puts("\nclock_gettime_test\n");
  struct timespec ts0, ts1, ts_dif;

  measurements_hires__init();

  clock_gettime(CLOCK_MONOTONIC, &ts0);
  for (int i = 0; i < NUM_OF_MEAS; i++) {
    measurements_hires__start_measurement();
    clock_gettime(CLOCK_MONOTONIC, &ts1);
    measurements_hires__end_measurement();
  }
  measurements_hires__print_measures_data();
  hires_time_long_t hrt_total = measurements_hires__measurement_time();
  hires_time_units_t hrt_total_units = measurements_hires__to_time_units(hrt_total);
  uint32_t hrt_avg = hrt_total_units / NUM_OF_MEAS * NS_PER_HIRES_TIME_UNIT;
  puts("hires total:"); putint32(hrt_total); puts(" hires ticks");
  puts(" ("); putint32(hrt_total_units); puts(" time units)\n");

  TS_SUB(ts_dif, ts1, ts0);
  puts("clock_gettime total:"); putts(ts_dif); puts("\n");

  clock_gettime(CLOCK_MONOTONIC, &ts0);
  for (int i = 0; i < NUM_OF_MEAS; i++) {
    clock_gettime(CLOCK_MONOTONIC, &ts1);
  }

  TS_SUB(ts_dif, ts1, ts0);

  puts("clock_gettime total:"); putts(ts_dif);
  puts(" ("); putint32(NUM_OF_MEAS); puts("num of measusements)\n");

  tests_reports__assert (TS_SEC(ts_dif) == 0);
  hwtime_t avg = TS_NSEC(ts_dif) / NUM_OF_MEAS;

  puts("Time of clock_gettime():"); putint(hrt_avg); puts("ns (with hires)\n");
  puts("TIME OF clock_gettime():"); putint(avg); puts("ns\n");

  tests_reports__assert_equal_c(hrt_avg, avg, avg/12); // 8.3% error

  tests_reports__test_ok();
  return 0;
}
