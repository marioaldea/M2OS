
--  M2OS: stack sharing version of this package.

--  Simplified version of the GNAT System.Tasking package.

package body System.Tasking is

   ------------------------
   -- Local Declarations --
   ------------------------

   Main_Priority : Integer := Unspecified_Priority;
   pragma Export (C, Main_Priority, "__gl_main_priority");
   --  Priority associated with the environment task. By default, its value is
   --  undefined, and can be set by using pragma Priority in the main program.

   Main_CPU : Integer := Unspecified_CPU;
   pragma Export (C, Main_CPU, "__gl_main_cpu");
   --  Affinity associated with the environment task. By default, its value is
   --  undefined, and can be set by using pragma CPU in the main program.
   --  Switching the environment task to the right CPU is left to the user.

   ---------------------
   -- Initialize_ATCB --
   ---------------------

   procedure Initialize_ATCB (Thread_Ac : OS_Interface.Thread_Ac;
                              T         : Task_Id) is
   begin
      T.Thread_Ac := Thread_Ac;
   end Initialize_ATCB;

   ----------------
   -- Initialize --
   ----------------

   Initialized : Boolean := False;
   --  Used to prevent multiple calls to Initialize

   procedure Initialize is
--      Base_Priority : Any_Priority;

--      Success : Boolean;
--      pragma Warnings (Off, Success);

--        procedure M2OSInit;
--        pragma Import (C, M2OSInit, "m2osinit");
      --  libm2os elaboration code.

   begin
      if Initialized then
         return;
      end if;

      Initialized := True;

      --  Compute priority

      if Main_Priority = Unspecified_Priority then
         Main_Priority := Default_Priority;  -- M2OS
      end if;

      --  Task_Primitives.Operations.Initialize;
      System.OS_Interface.Initialize;  -- M2OS
--        M2OSInit;
   end Initialize;

end System.Tasking;
