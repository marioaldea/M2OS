//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// M2OS POSIX-like layer.
// The M2OS POSIX-like layer is in a preliminary stage.
#ifndef	_M2_STDIO_H_
#define _M2_STDIO_H_
#include <stdint.h>

extern void m2__direct_io__put_null_terminated(const char *str);
extern void m2__direct_io__putchar(int c);
extern void m2__direct_io__putint(int num, char base);
extern void m2__direct_io__putint32(int32_t num, char base);

static inline int putchar(int c) {
  m2__direct_io__putchar(c);
  return c;
}

static inline int puts(const char *str) {
  m2__direct_io__put_null_terminated(str);
  return 1;
}

static inline void putint(int num) {
  m2__direct_io__putint(num, 10);
}

static inline void putint32(int32_t num) {
  m2__direct_io__putint32(num, 10);
}

static inline void putintbase(int num, int base) {
  m2__direct_io__putint(num, base);
}

#endif // _M2_STDIO_H_
