----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

pragma Warnings (Off);
with System.OS_Interface; -- for System.OS_Interface.Set_Global_Stack_Limit;
with Ada.Real_Time; -- for Ada.Real_Time.Time_Last
pragma Warnings (On);

with M2.Direct_IO;

with LEDs;
with Task_Orange_LED;
with Task_Red_LED;
with Task_Blue_LED;
with Task_Green_LED;

procedure Blink_LEDs is
   package DIO renames M2.Direct_IO;

begin
   DIO.Put ("-Blink Leds-");
   DIO.New_Line;
end Blink_LEDs;
