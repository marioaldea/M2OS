----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Defines two tasks:
--   - LCD_Task: to refresh the LCD measures and change among the two
--               views: Temp/Hum and CO2.
--   - LCD_Blink_Task: shorter period task to blink the IDA and humidity levels
--                     when there are out of the desired ranges.
----------------------------------------------------------------------------

with Interfaces.C;

with AdaX_Dispatching_Stack_Sharing.Periodic_Task;
with Ada.Real_Time;

with Arduino.LCD_I2C;
with Arduino.LCD_I2C.Advanced;

with M2.Direct_IO;

package body CO2_Sensor.LCD_Tasks is
   package DIO renames M2.Direct_IO;
   package LCD renames Arduino.LCD_I2C;
   
   use type Ada.Real_Time.Time_Span;
   
   --  Strings definition
   
   NUL : constant Interfaces.C.Char := Interfaces.C.Char'Val(0);
   T_Str : aliased Interfaces.C.Char_Array := ('T', NUL);
   Cel_Str : aliased Interfaces.C.Char_Array := ('C', NUL);
   H_Str : aliased Interfaces.C.Char_Array := ('H', NUL); 
   Percent_Str : aliased Interfaces.C.Char_Array := ('%', NUL); 
   M2OS_Str : aliased Interfaces.C.Char_Array := ('M', '2', 'O', 'S', NUL); 
   CO2_Str : aliased Interfaces.C.Char_Array := ('C', 'O', '2', NUL);
   PPM_Str : aliased Interfaces.C.Char_Array := ('p', 'p', 'm', NUL); 
   TVOC_Str : aliased Interfaces.C.Char_Array := ('T', 'V', 'O', 'C', NUL);
   PPB_Str : aliased Interfaces.C.Char_Array := ('p', 'p', 'b', NUL);
   IDA_Str : aliased Interfaces.C.Char_Array := ('I', 'D', 'A', NUL);
   Dry_Str : aliased Interfaces.C.Char_Array := ('D', 'r', 'y', NUL);
   Wet_Str : aliased Interfaces.C.Char_Array := ('W', 'e', 't', NUL);
   Spc5_Str : aliased Interfaces.C.Char_Array := (' ', ' ', ' ', ' ', ' ', NUL);
   Spc12_Str : aliased Interfaces.C.Char_Array :=
     (' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', NUL);

   --  Global data
   
   Temperature : Float := 20.0;
   Humidity : Float := 50.0;
   Hum_Level : Humidity_Level := Hum_Normal;
   CO2_PPM : Arduino.CCS811.CO2_PPM_T := 0;
   TVOC_PPB: Arduino.CCS811.TVOC_PPB_T := 0;
   IDA : IDA_Level := IDA1;
   
   ---------------------
   -- Set_Temperature --
   ---------------------

   procedure Set_Temperature (Temp : Float) is
   begin
      LCD_Tasks.Temperature := Temp;
   end Set_Temperature;
   
   ------------------
   -- Set_Humidity --
   ------------------

   procedure Set_Humidity (Hum : Float) is
   begin
      LCD_Tasks.Humidity := Hum;
   end Set_Humidity;
   
   ------------------------
   -- Set_Humidity_Level --
   ------------------------

   procedure Set_Humidity_Level (Hum : Humidity_Level) is
   begin
      LCD_Tasks.Hum_Level := Hum;
   end Set_Humidity_Level;     
   
   -----------------
   -- Set_CO2_PPM --
   -----------------

   procedure Set_CO2_PPM (CO2_PPM : Arduino.CCS811.CO2_PPM_T) is
   begin
      LCD_Tasks.CO2_PPM := CO2_PPM;
   end Set_CO2_PPM;
   
   ------------------
   -- Set_TVOC_PPB --
   ------------------

   procedure Set_TVOC_PPB (TVOC_PPB: Arduino.CCS811.TVOC_PPB_T) is
   begin
      LCD_Tasks.TVOC_PPB := TVOC_PPB;
   end Set_TVOC_PPB;    
   
   -------------
   -- Set_IDA --
   -------------

   procedure Set_IDA (IDA : IDA_Level) is
   begin
      LCD_Tasks.IDA := IDA;
   end Set_IDA;
   
   -------------------
   -- LCD_Task_Init --
   -------------------

   procedure LCD_Task_Init is
   begin
      -- DIO.Put_Line ("LCD Task Init");
      
      --  Initial delay to wait for sensor tasks to read values
      
      delay until Ada.Real_Time.Clock + Ada.Real_Time.Milliseconds (500);
   end LCD_Task_Init;
   
   -------------------
   -- LCD_Task_Body --
   -------------------
   
   type Screen_Views is mod 2;
   Screen_View : Screen_Views := 0;
   --  0 : temperature and Humidity
   --  1 : CO2 ppm

   procedure LCD_Task_Body is
      
      procedure Temp_Hum_View is
      begin
         LCD.Set_Cursor (0, 0);
         LCD.Print_Null_Terminated_Char_Array (Spc12_Str'Unrestricted_Access);
         LCD.Set_Cursor (0, 0);
         LCD.Print_Null_Terminated_Char_Array (T_Str'Unrestricted_Access);
         LCD.Print_Char (':');
         LCD.Print_Integer (Integer (Temperature));      
         LCD.Print_Null_Terminated_Char_Array (Cel_Str'Unrestricted_Access);
         
         LCD.Set_Cursor (0, 1);
         LCD.Print_Null_Terminated_Char_Array (Spc12_Str'Unrestricted_Access);
         LCD.Set_Cursor (0, 1);
         LCD.Print_Null_Terminated_Char_Array (H_Str'Unrestricted_Access);
         LCD.Print_Char (':');
         LCD.Print_Integer (Integer (Humidity));
         LCD.Print_Null_Terminated_Char_Array (Percent_Str'Unrestricted_Access);
      end Temp_Hum_View;        
           
      procedure CO2_PPM_View is
      begin
         LCD.Set_Cursor (0, 0);
         LCD.Print_Null_Terminated_Char_Array (Spc12_Str'Unrestricted_Access);
         LCD.Set_Cursor (0, 0);
         LCD.Print_Null_Terminated_Char_Array (CO2_Str'Unrestricted_Access);
         LCD.Print_Char (':');
         LCD.Print_Integer (Integer (CO2_PPM));
         LCD.Print_Null_Terminated_Char_Array (PPM_Str'Unrestricted_Access);
         
         LCD.Set_Cursor (0, 1);
         LCD.Print_Null_Terminated_Char_Array (Spc12_Str'Unrestricted_Access);
         LCD.Set_Cursor (0, 1);
         LCD.Print_Null_Terminated_Char_Array (TVOC_Str'Unrestricted_Access);
         LCD.Print_Char (':');
         LCD.Print_Integer (Integer (TVOC_PPB));
         LCD.Print_Null_Terminated_Char_Array (PPB_Str'Unrestricted_Access);
      end CO2_PPM_View;
      
   begin
      -- DIO.Put_Line ("LCD_Task");
      
      if Screen_View = 0 then
         Temp_Hum_View;
      else
         CO2_PPM_View;
      end if;
      
      Screen_View := Screen_View + 1;
   end LCD_Task_Body;
   
   --------------
   -- LCD_Task --
   --------------

   package LCD_Task is new AdaX_Dispatching_Stack_Sharing.Periodic_Task
     (Init_Ac  => LCD_Task_Init'Access,
      Body_Ac  => LCD_Task_Body'Access,
      Priority => CO2_Sensor.LCD_Task_Prio,
      Period   => Ada.Real_Time.Milliseconds (CO2_Sensor.LCD_Task_T_In_Ms));
 
   -------------------------
   -- LCD_Blink_Task_Init --
   -------------------------

   procedure LCD_Blink_Task_Init is
   begin
      -- DIO.Put_Line ("LCD Blink Init");
      
      LCD.Begin_LCD (16, 2);
      
      LCD.Print_Null_Terminated_Char_Array (M2OS_Str'Unrestricted_Access);
      
      delay until Ada.Real_Time.Clock + Ada.Real_Time.Milliseconds (1_000);
   end LCD_Blink_Task_Init;
   
   -------------------------
   -- LCD_Blink_Task_Body --
   -------------------------
   
   type Blink_Mode is mod 2;
   Blink_Counter : Blink_Mode := 0;
   --  0 : do not display.     1 : display

   procedure LCD_Blink_Task_Body is
   begin
      -- DIO.Put_Line ("LCD_Blink_Task");
      
      if Blink_Counter = 0 then
         --  Blink
         
         LCD.Set_Cursor (12, 0);
         if IDA >= IDA3 then
            LCD.Print_Null_Terminated_Char_Array
              (Spc5_Str'Unrestricted_Access);
         end if;
         
         LCD.Set_Cursor (12, 1);
         case Hum_Level is
            when Hum_Very_Dry | Hum_Very_Wet =>
               LCD.Print_Null_Terminated_Char_Array
                 (Spc5_Str'Unrestricted_Access);
            when others =>
               null;
         end case;
      else
         --  Display data
         
         if IDA > IDA1 then
            LCD.Set_Cursor (12, 0);
            LCD.Print_Null_Terminated_Char_Array (IDA_Str'Unrestricted_Access);
            LCD.Print_Integer (IDA_Level'Pos (IDA));
         end if;
         
         LCD.Set_Cursor (12, 1);
         case Hum_Level is
            when Hum_Very_Dry | Hum_Dry =>
               LCD.Print_Null_Terminated_Char_Array
                 (Dry_Str'Unrestricted_Access);
            when Hum_Normal =>
               LCD.Print_Null_Terminated_Char_Array
                 (Spc5_Str'Unrestricted_Access);
            when Hum_Wet | Hum_Very_Wet =>
               LCD.Print_Null_Terminated_Char_Array
                 (Wet_Str'Unrestricted_Access);
         end case;
      end if;
         
      Blink_Counter := Blink_Counter + 1; 
   end LCD_Blink_Task_Body;
               
   --------------------
   -- LCD_Blink_Task --
   --------------------

   package LCD_Blink_Task is new AdaX_Dispatching_Stack_Sharing.Periodic_Task
     (Init_Ac  => LCD_Blink_Task_Init'Access,
      Body_Ac  => LCD_Blink_Task_Body'Access,
      Priority => CO2_Sensor.LCD_Blink_Task_Prio,
      Period   => Ada.Real_Time.Milliseconds (CO2_Sensor.LCD_Blink_Task_T_In_Ms));
 
end CO2_Sensor.LCD_Tasks;
