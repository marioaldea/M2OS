
--  M2OS: stack sharing version of this package.

--  Simplified version of the GNAT System.Tasking package.

with Ada.Unchecked_Conversion;
with System.OS_Interface;

package System.Tasking is
   pragma Preelaborate;

   ---------------------------------
   -- Task_Id related definitions --
   ---------------------------------

   type Ada_Task_Control_Block;

   type Task_Id is access all Ada_Task_Control_Block;

   Null_Task : constant Task_Id := null;

   -------------------------------
   -- Entry related definitions --
   -------------------------------

   Null_Entry : constant := 0;

   Max_Entry : constant := Integer'Last;

   Interrupt_Entry : constant := -2;

   Cancelled_Entry : constant := -1;

   type Entry_Index is range Interrupt_Entry .. Max_Entry;

   Null_Task_Entry : constant := Null_Entry;

   Max_Task_Entry : constant := Max_Entry;

   type Task_Entry_Index is new Entry_Index
     range Null_Task_Entry .. Max_Task_Entry;

--     type Entry_Call_Record;
--
--     type Entry_Call_Link is access all Entry_Call_Record;
--
--     ----------------------------------
--     -- Entry_Call_Record definition --
--     ----------------------------------
--
--     type Entry_Call_Record is record
--        Self : Task_Id;
--        --  ID of the caller
--
--        Uninterpreted_Data : System.Address;
--        --  Data passed by the compiler
--     end record;
--     pragma Suppress_Initialization (Entry_Call_Record);

   ------------------------------------
   -- Other Task-Related Definitions --
   ------------------------------------

   type Activation_Chain is limited private;

   type Activation_Chain_Access is access all Activation_Chain;

   type Task_Procedure_Access is access procedure (Arg : System.Address);

   type Access_Boolean is access all Boolean;

   ----------------------------------------------
   -- Ada_Task_Control_Block (ATCB) definition --
   ----------------------------------------------

--     type Common_ATCB is record
--
--        Base_Priority : Priority;
--        --  Base priority
--
--        Task_Arg : System.Address;
--      --  The argument to task procedure. Currently unused, this will provide
--        --  a handle for discriminant information.
--     end record;
--     pragma Suppress_Initialization (Common_ATCB);

   type Ada_Task_Control_Block (Entry_Num : Task_Entry_Index) is record

      Thread_Ac : OS_Interface.Thread_Ac;

   end record;
   pragma Suppress_Initialization (Ada_Task_Control_Block);

--     type Ada_Task_Control_Block (Entry_Num : Task_Entry_Index) is record
--     --  The discriminant Entry_Num is not needed, but we keep it here for
--     --  compatibility reasons with the rest of the run times, so that the
--     --  expander does not need to know which run time is being used.
--
--        Common : Common_ATCB;
--
--        Entry_Call : aliased Entry_Call_Record;
--      --  Protection: This field is used on entry call queues associated with
--        --  protected objects, and is protected by the protected object lock.
--     end record;
--     pragma Suppress_Initialization (Ada_Task_Control_Block);

   --------------------------------
   -- Master Related Definitions --
   --------------------------------

   subtype Master_Level is Integer;
   subtype Master_ID is Master_Level;

   Library_Task_Level : constant Master_Level := 3;

   ----------------------------------------
   -- Task size, priority, affinity info --
   ----------------------------------------

   Unspecified_Priority : constant := -1;
   --  No specified priority. This value is also hard-coded in gnatbind.

   Unspecified_CPU : constant := -1;
   --  No affinity specified

   --------------------
   -- Initialization --
   --------------------

   procedure Initialize;
   --  This procedure constitutes the first part of the initialization of the
   --  GNARL. This includes creating data structures to make the initial thread
   --  into the environment task. The last part of the initialization is done
   --  in System.Tasking.Initialization or System.Tasking.Restricted.Stages.
   --  All the initializations used to be in Tasking.Initialization, but this
   --  is no longer possible with the run time simplification (including
   --  optimized PO and the restricted run time) since one cannot rely on
   --  System.Tasking.Initialization being present, as was done before.

   procedure Initialize_ATCB (Thread_Ac : OS_Interface.Thread_Ac;
                              T         : Task_Id);

private

   type Activation_Chain is limited record
      T_ID : Task_Id;
      --  Null terminated simply linked list of tasks
   end record;
end System.Tasking;
