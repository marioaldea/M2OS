with Ada.Text_IO;                use Ada.Text_IO;

with GNATCOLL.VFS;      use GNATCOLL.VFS;
with GNAT.Regpat;

with Libadalang.Common;           use Libadalang.Common;
with Libadalang.Auto_Provider;    use Libadalang.Auto_Provider;
with Libadalang.Project_Provider; use Libadalang.Project_Provider;

with Langkit_Support.Text;
with Langkit_Support.Diagnostics; use Langkit_Support.Diagnostics;

package body M2OS_Utils is

   Source_Files_Pattern : constant GNAT.Regpat.Pattern_Matcher :=
                            GNAT.Regpat.Compile (".*\.(ads|adb)$");

   ----------------------
   -- Show_Diagnostics --
   ----------------------

   procedure Show_Diagnostics (Unit: Analysis_Unit) is
   begin
      if Has_Diagnostics (Unit) then
         Put_Line ("   ...   but we got diagnostics:");
         for D of Diagnostics (Unit) loop
            Put_Line ("   " & To_Pretty_String (D));
         end loop;
      end if;
   end Show_Diagnostics;

   --------------
   -- Basename --
   --------------

   function Corresponding_Spec (Filename : String) return String is
      Base : constant String := (Filename (Filename'First .. Filename'Last - 4));
   begin
      return (Base & ".ads");
   end Corresponding_Spec;

   --------------
   -- Put_Node --
   --------------

   procedure Put_Node (N : Ada_Node) is
   begin
      Put_Line ("  " & Short_Image (N));
   end Put_Node;

   procedure Put_Node_Text (N : Ada_Node) is
      Content_Bytes : constant String :=
                        Langkit_Support.Text.Encode (Text(Node => N), Default_Charset);
   begin
      --Put_Line (Image (Text_Image (N)));
      --Print (Node       => N,
      --       Show_Slocs => False);
      --Put_Line (Debug_Text(Node => N));
      Put_Line (Content_Bytes);

   end Put_Node_Text;

   --------------
   -- Run_Find --
   --------------

   procedure Run_Find
     (Ctx             : Analysis_Context;
      Filename, Label : String;
      Predicate       : Ada_Node_Predicate) is
   begin
      Put_Line ("[" & Filename & "] " & Label & ":");
      for Node of Find (Ctx.Get_From_File (Filename).Root, Predicate).Consume
      loop
         Put_Node_Text (Node);
      end loop;
      New_Line;
   end Run_Find;


   ----------------------
   -- To_String_Vector --
   ----------------------

   function To_String_Vector (List : File_Array_Access) return String_Vectors.Vector is
      Files : String_Vectors.Vector;
   begin
      --  Sort list of files according to the filename. This is important 
      --  to get repeatable results
      Sort (List.all); 

      for F of List.all loop
         declare
            Full_Name : Filesystem_String renames F.Full_Name.all;
            Name      : constant String := +Full_Name;
         begin
            if GNAT.Regpat.Match (Source_Files_Pattern, +F.Base_Name) then
               Files.Append (+Name);
            end if;
         end;
         
      end loop;
      return Files;
   end To_String_Vector;
    
   ----------------------------
   -- Add_Files_From_Project --
   ----------------------------

   procedure Add_Files_From_Project (P : Project_Type; Files : in out String_Vectors.Vector) is
      List : File_Array_Access := P.Source_Files;
   begin
      Files := To_String_Vector (List);
      Unchecked_Free (List);
   end Add_Files_From_Project;

   ------------------
   -- Load_Project --
   ------------------

   procedure Load_Project (Name        : String;
                           Provider    : out Unit_Provider_Reference;
                           The_Project : out Project_Type;
                           Files       : out String_Vectors.Vector) is
      Env     : Project_Environment_Access;
      Project : constant Project_Tree_Access := new Project_Tree;

   begin
      Initialize (Env);
      Load (Project.all, Create (+Name), Env);
      The_Project := Project.Root_Project;
      Add_Files_From_Project (The_Project, Files);
      Provider := Create_Project_Unit_Provider_Reference (Project, Env, True);     
   end Load_Project;

   -------------------
   --  Is_Ada_Main  --
   -------------------

   function Is_Ada_Main (Filename     : String;
                         The_Project  : Project_Type) return Boolean is
   begin
      return Is_Main_File (Project        => The_Project,
                           File           => GNATCOLL.VFS."+"(Filename),
                           Case_Sensitive => True);
   end Is_Ada_Main;

   --------------------------
   -- Load_Default_Project --
   --------------------------

   procedure Load_Default_Project (Provider: out Unit_Provider_Reference;
                                   Files   : out String_Vectors.Vector) is
      Ada_Files : GNATCOLL.VFS.File_Array_Access;
      Dirs      : GNATCOLL.VFS.File_Array (1..1);
      
   begin
      Dirs(1) := GNATCOLL.VFS.Get_Current_Dir;  --  Search only in current dir
      Ada_Files := Find_Files (Directories => Dirs, Name_Pattern => Source_Files_Pattern);
      Files := To_String_Vector (Ada_Files);
      Provider := Create_Auto_Provider_Reference (Ada_Files.all);
      GNATCOLL.VFS.Unchecked_Free (Ada_Files);
   end Load_Default_Project;

end M2OS_Utils;
