----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Authors: David Garcia Villaescusa (garcia@unican.es)
--           Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Interfaces;
with Interfaces.C;

package E_Lib is

   package C renames Interfaces.C;

   type Epi_Rows is range 0 .. 3;
   for Epi_Rows'Size use Interfaces.Unsigned_32'Size;
   type Epi_Cols is range 0 .. 3;
   for Epi_Cols'Size use Interfaces.Unsigned_32'Size;

   type Core is record
      Row : aliased Epi_Rows;
      Col : aliased Epi_Cols;
   end record;

   function E_Get_Coreid return Interfaces.Unsigned_32
     with
       Import => True,
       convention => C;
   -- Imports function 'e_get_coreid' from C.
   -- Can be called from Ada.

   --  An Ada access T parameter, or an Ada out or in out parameter of an
   --  elementary type T, is passed as a t* argument to a C function, where t is
   --  the C type corresponding to the Ada type T. In the case of an elementary
   --  out or in out parameter, a pointer to a temporary copy is used to
   --  preserve by-copy semantics.

   procedure E_Coords_From_Coreid (Core_Id : in Interfaces.Unsigned_32;
                                   Rows : out Epi_Rows;
                                   Cols : out Epi_Cols)
     with
       Import => True,
       convention => C;
   -- Imports function 'e_coords_from_coreid' from C.
   -- Can be called from Ada.

   procedure E_Mutex_Init (Rows : in Epi_Rows;
                           Cols : in Epi_Cols;
                           Mutex : in out Integer;
                           Attr : in Integer)
     with
       Import => True,
       convention => C;
   -- Imports function 'e_mutex_init' from C.
   -- Can be called from Ada.

   procedure E_Mutex_Lock (Rows : in Epi_Rows;
                           Cols : in Epi_Cols;
                           Mutex : in out Integer)
     with
       Import => True,
       convention => C;
   -- Imports function 'e_mutex_lock' from C.
   -- Can be called from Ada.

   procedure E_Mutex_Unlock (Rows : in Epi_Rows;
                             Cols : in Epi_Cols;
                             Mutex : in out Integer)
     with
       Import => True,
       convention => C;
   -- Imports function 'e_mutex_unlock' from C.
   -- Can be called from Ada.

end E_Lib;
