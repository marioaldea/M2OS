pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with stdint_h;
with Stream_h;
with Interfaces.C.Extensions;
with Print_h;
with stddef_h;

package HardwareSerial_h is

   SERIAL_TX_BUFFER_SIZE : constant := 64;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:46

   SERIAL_RX_BUFFER_SIZE : constant := 64;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:53

   SERIAL_5N1 : constant := 16#00#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:68
   SERIAL_6N1 : constant := 16#02#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:69
   SERIAL_7N1 : constant := 16#04#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:70
   SERIAL_8N1 : constant := 16#06#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:71
   SERIAL_5N2 : constant := 16#08#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:72
   SERIAL_6N2 : constant := 16#0A#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:73
   SERIAL_7N2 : constant := 16#0C#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:74
   SERIAL_8N2 : constant := 16#0E#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:75
   SERIAL_5E1 : constant := 16#20#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:76
   SERIAL_6E1 : constant := 16#22#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:77
   SERIAL_7E1 : constant := 16#24#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:78
   SERIAL_8E1 : constant := 16#26#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:79
   SERIAL_5E2 : constant := 16#28#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:80
   SERIAL_6E2 : constant := 16#2A#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:81
   SERIAL_7E2 : constant := 16#2C#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:82
   SERIAL_8E2 : constant := 16#2E#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:83
   SERIAL_5O1 : constant := 16#30#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:84
   SERIAL_6O1 : constant := 16#32#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:85
   SERIAL_7O1 : constant := 16#34#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:86
   SERIAL_8O1 : constant := 16#36#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:87
   SERIAL_5O2 : constant := 16#38#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:88
   SERIAL_6O2 : constant := 16#3A#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:89
   SERIAL_7O2 : constant := 16#3C#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:90
   SERIAL_8O2 : constant := 16#3E#;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:91

   subtype tx_buffer_index_t is stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:59

   subtype rx_buffer_index_t is stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:64

   type HardwareSerial_u_rx_buffer_array is array (0 .. 63) of aliased unsigned_char;
   type HardwareSerial_u_tx_buffer_array is array (0 .. 63) of aliased unsigned_char;
   package Class_HardwareSerial is
      type HardwareSerial is limited new Stream_h.Class_Stream.Stream with record
         u_ubrrh : access stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:96
         u_ubrrl : access stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:97
         u_ucsra : access stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:98
         u_ucsrb : access stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:99
         u_ucsrc : access stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:100
         u_udr : access stdint_h.uint8_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:101
         u_written : aliased Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:103
         u_rx_buffer_head : aliased rx_buffer_index_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:105
         u_rx_buffer_tail : aliased rx_buffer_index_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:106
         u_tx_buffer_head : aliased tx_buffer_index_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:107
         u_tx_buffer_tail : aliased tx_buffer_index_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:108
         u_rx_buffer : aliased HardwareSerial_u_rx_buffer_array;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:113
         u_tx_buffer : aliased HardwareSerial_u_tx_buffer_array;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:114
          : aliased Print_h.Class_Print.Print;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:134
      end record;
      pragma Import (CPP, HardwareSerial);

      function New_HardwareSerial
        (ubrrh : access stdint_h.uint8_t;
         ubrrl : access stdint_h.uint8_t;
         ucsra : access stdint_h.uint8_t;
         ucsrb : access stdint_h.uint8_t;
         ucsrc : access stdint_h.uint8_t;
         udr : access stdint_h.uint8_t) return HardwareSerial;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:117
      pragma CPP_Constructor (New_HardwareSerial, "_ZN14HardwareSerialC1EPVhS1_S1_S1_S1_S1_");

      procedure c_begin (this : access HardwareSerial'Class; baud : unsigned_long);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:121
      pragma Import (CPP, c_begin, "_ZN14HardwareSerial5beginEm");

      procedure c_begin
        (this : access HardwareSerial'Class;
         arg2 : unsigned_long;
         arg3 : stdint_h.uint8_t);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:122
      pragma Import (CPP, c_begin, "_ZN14HardwareSerial5beginEmh");

      procedure c_end (this : access HardwareSerial'Class);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:123
      pragma Import (CPP, c_end, "_ZN14HardwareSerial3endEv");

      function available (this : access HardwareSerial) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:124
      pragma Import (CPP, available, "_ZN14HardwareSerial9availableEv");

      function peek (this : access HardwareSerial) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:125
      pragma Import (CPP, peek, "_ZN14HardwareSerial4peekEv");

      function read (this : access HardwareSerial) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:126
      pragma Import (CPP, read, "_ZN14HardwareSerial4readEv");

      function availableForWrite (this : access HardwareSerial) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:127
      pragma Import (CPP, availableForWrite, "_ZN14HardwareSerial17availableForWriteEv");

      procedure flush (this : access HardwareSerial);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:128
      pragma Import (CPP, flush, "_ZN14HardwareSerial5flushEv");

      function write (this : access HardwareSerial; arg2 : stdint_h.uint8_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:129
      pragma Import (CPP, write, "_ZN14HardwareSerial5writeEh");

      function write (this : access HardwareSerial'Class; n : unsigned_long) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:130
      pragma Import (CPP, write, "_ZN14HardwareSerial5writeEm");

      function write (this : access HardwareSerial'Class; n : long) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:131
      pragma Import (CPP, write, "_ZN14HardwareSerial5writeEl");

      function write (this : access HardwareSerial'Class; n : unsigned) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:132
      pragma Import (CPP, write, "_ZN14HardwareSerial5writeEj");

      function write (this : access HardwareSerial'Class; n : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:133
      pragma Import (CPP, write, "_ZN14HardwareSerial5writeEi");

      function operator_2 (this : access HardwareSerial'Class) return Extensions.bool;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:135
      pragma Import (CPP, operator_2, "_ZN14HardwareSerialcvbEv");

      --  skipped func _rx_complete_irq

      --  skipped func _tx_udr_empty_irq
   end;
   use Class_HardwareSerial;
   Serial : aliased HardwareSerial;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:143
   pragma Import (C, Serial, "Serial");

   procedure serialEventRun;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/HardwareSerial.h:159
   pragma Import (CPP, serialEventRun, "_Z14serialEventRunv");

end HardwareSerial_h;
