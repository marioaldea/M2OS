----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Example of IR thermometer with a temperature sensor to detect when the
--  object target is at the appropriate distance and an lCD to display data.
--
--     ---------------------             ----------------------
--     |                   |             |                    |
--     | Grove     SUR_TEMP|-------------|A0      Arduino Uno |
--     | IR Temp   OBJ_TEMP|-------------|A1                  |
--     | Sensor         VCC|-------------|5V                  |
--     |                GND|-------------|GND                 |
--     |                   |             |                    |
--     ---------------------             |                    |
--     -----------------                 |                    |
--     |               |                 |                    |
--     |            SCL|-----------------|A0                  |
--     |            SDA|-----------------|A1                  |
--     | LCD        VDD|-----------------|5V                  |
--     |            GND|-----------------|GND                 |
--     |               |                 |                    |
--     -----------------                 |                    |
--     ---------------------             |                    |
--     |                   |             |                    |
--     | Grove          GND|-------------|GND                 |
--     | Ultrasonic     VCC|-------------|5V                  |
--     | Ranger          NC|             |                    |
--     |                SIG|-------------|D7                  |
--     |                   |             |                    |
--     ---------------------             ----------------------
--
------------------------------------------------------------------------
pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Arduino;
with Arduino.Ultrasonic_Ranger_Grove;
with Arduino.IR_Temp_Sensor;
with Arduino.LCD_I2C;
with Arduino.LCD_I2C.Advanced;

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;

procedure IR_Thermometer is
   package DIO renames M2.Direct_IO;
   package LCD renames Arduino.LCD_I2C;
   package IR_Temp renames Arduino.IR_Temp_Sensor;
   
   use type Interfaces.C.Long, Interfaces.C.Double;
   
   Nominal_Distance_Mx : constant := 55; -- in mm   
   Nominal_Distance_Mn : constant := 45; -- in mm  
   Large_Distance_Mn : constant := 2000; -- in mm

   package Ranger is new Arduino.Ultrasonic_Ranger_Grove.Ranger (Pin => 7);
   
   Temp_Air : Float;   
   Temp_Obj : Float;    
   Temp_Obj_Sum : Float := 0.0;
   Temp_Obj_Count : Natural := 0;
   Dist : Interfaces.C.Long;
   Long_Dist_Loops : Natural := 0;
   Long_Dist_Loops_Reset : constant := 5;
   
   --  Strings definition
   
   NUL : constant Interfaces.C.Char := Interfaces.C.Char'Val(0);
   T_Str : aliased Interfaces.C.Char_Array := ('T', ':', NUL);
   Cel_Str : aliased Interfaces.C.Char_Array := ('C', NUL);
   Adjusting_Str : aliased Interfaces.C.Char_Array :=
     ('A', 'd', 'j', 'u', 's', 't', 'i', 'n', 'g', '.', '.', '.', NUL);
   Dist_Str : aliased Interfaces.C.Char_Array :=
     ('D', 'i', 's', 't', NUL);
   
   --------------
   -- Put_Dist --
   --------------

   procedure Put_Dist (Dist : Interfaces.C.Long) is
   begin
      LCD.Set_Cursor (0, 0);
      LCD.Print_Null_Terminated_Char_Array (Dist_Str'Unrestricted_Access);
      LCD.Print_Char (':');
      for I in 1 .. 6 loop
         LCD.Print_Char (' ');
      end loop;
      LCD.Set_Cursor (5, 0);
      LCD.Print_Integer (Integer (Dist));
      LCD.Print_Char ('m');
      LCD.Print_Char ('m');
   end Put_Dist; 
   
   --------------
   -- Put_Temp --
   --------------

   procedure Put_Temp (Temp_Obj : Float;
                       Temp_Obj_Count : Natural) is
   begin
      LCD.Set_Cursor (0, 1);
      LCD.Print_Null_Terminated_Char_Array (T_Str'Unrestricted_Access);
      LCD.Print_Char (':'); 
      for I in 1 .. 14 loop
         LCD.Print_Char (' ');
      end loop;
      LCD.Set_Cursor (2, 1);
      LCD.Advanced.Print_Double (Interfaces.C.Double (Temp_Obj));
      LCD.Print_Null_Terminated_Char_Array (Cel_Str'Unrestricted_Access);
      
      LCD.Set_Cursor (11, 1);
      LCD.Print_Integer (Temp_Obj_Count);
   end Put_Temp;      
   
begin
   DIO.Put_Line ("IR Thermometer Demo");
   
   IR_Temp.Initialize;
   LCD.Begin_LCD (16, 2);
    
   DIO.Put_Line ("Adjusting...");
   LCD.Set_Cursor (0, 0);
   LCD.Print_Null_Terminated_Char_Array (Adjusting_Str'Unrestricted_Access);
   loop       
      Temp_Air := IR_Temp.Surronding_Temperature;
      Temp_Obj := IR_Temp.Object_Temperature;
      
      exit when abs (Temp_Air - Temp_Obj) < 0.1; 
      IR_Temp.Adjust_Offset_Correction; 
      Arduino.C_Delay (500);          
   end loop;
   LCD.Clear;
   
   loop       
      Temp_Obj := IR_Temp.Object_Temperature;
      
      Dist := Ranger.Measure_In_Millimeters;
      
      Put_Dist (Dist);
      
      DIO.Put ("Dist:" & Integer (Dist)'Img & "mm");
      DIO.Put (" Temp_Obj:");
      DIO.Put (Temp_Obj);
      
      if Dist in Nominal_Distance_Mn .. Nominal_Distance_Mx then
         DIO.Put (" In range"); 
         Temp_Obj_Count := Temp_Obj_Count + 1;
         Temp_Obj_Sum := Temp_Obj_Sum + Temp_Obj;
         Put_Temp (Temp_Obj_Sum / Float (Temp_Obj_Count),
                   Temp_Obj_Count);
         Long_Dist_Loops := 0;
         
      elsif Dist > Large_Distance_Mn then
         DIO.Put (" Adjust");      
         Temp_Obj := IR_Temp.Object_Temperature; 
         DIO.Put (" Temp_Air:");
         DIO.Put (Temp_Air);
         
         IR_Temp.Adjust_Offset_Correction;
         
         Long_Dist_Loops := Long_Dist_Loops + 1;
         if Long_Dist_Loops > Long_Dist_Loops_Reset then
            Long_Dist_Loops := 0;
            Temp_Obj_Count := 0;
            Temp_Obj_Sum := 0.0;
            Put_Temp (0.0, 0);
         end if;
      end if;
      
      DIO.New_Line;
      
      Arduino.C_Delay (500);
   end loop;
end IR_Thermometer;
