----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of Grove-16x2 LCD library
--  https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/
----------------------------------------------------------------------------

package Arduino.LCD_I2C.Advanced is

   procedure Auto_Scroll
     with Inline_Always;

   procedure No_Auto_Scroll
     with Inline_Always; 

   procedure Print_Double (Num : Interfaces.C.double;
                           Digital_Places: Integer := 2)
     with Inline_Always;

end Arduino.Lcd_I2c.Advanced;
