----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Task that gets the light measures.
----------------------------------------------------------------------------

pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

with M2.Direct_IO;
with M2.HAL;
with M2.Kernel.API;
with Interfaces.C.Extensions;
with Arduino.EVShield.EV3Color;
with Arduino.EVShield.NXTColor;


package body EVShield_Light_Measure_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;
   ----------------
   -- Other_Task --
   ----------------

   Next_Time : RT.Time;
   Period : constant RT.Time_Span := RT.Milliseconds (250);

   Aux1 : Stdint_H.Uint8_T;
   Aux2 : Float;
   Aux3 : Interfaces.C.Extensions.Bool;

   -- Light sensors
   package Light_Right is new Arduino.EVShield.NXTColor.Color (Arduino.EVShield.SH_BBS2);
   package Light_Left is new Arduino.EVShield.EV3Color.Color (Arduino.EVShield.SH_BAS2);
   use type Interfaces.C.Unsigned_Char;

   procedure Other_Task_Init is
   begin
      Next_Time := Ada.Real_Time.Clock;

      -- initialize the sensor mode.
      Aux1 := Light_Left.Set_Mode(Light_Left.MODE_Color_AmbientLight);
      Aux3 := Light_Right.Set_Color(Light_Right.SH_Type_COLORNONE);

   end Other_Task_Init;


   procedure Other_Task_Body is
   begin
      Aux1 := 0;
      Aux2 := 0.0;

      -- read the value from color sensors
      Value_Right := Light_Right.Read_Value;
      Value_Left := Light_Left.Get_Val;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Other_Task_Body;

   Other_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Other_Task_Init'Access,
      Body_Ac  => Other_Task_Body'Access,
      Priority => 4);

end EVShield_Light_Measure_Task;
