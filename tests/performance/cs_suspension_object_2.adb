pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Synchronous_Task_Control;

with M2.Direct_IO;
with M2.HAL;

package body CS_Suspension_Object_2 is
   package DIO renames M2.Direct_IO;
   package STC renames Ada.Synchronous_Task_Control;

   ----------------
   -- Other_Task --
   ----------------

   task body Other_Task is
   begin
      loop
         M2.HAL.Enable_Interrupts;
         --DIO.Put ("-Other-");
--           DIO.Put (Integer
--                    (Ada.Real_Time.To_Duration (Ada.Real_Time.Clock)));

         Loop_Counter := Loop_Counter + 1;

         STC.Set_True (Suspension_Object_1);
         STC.Suspend_Until_True (Suspension_Object_2);
      end loop;
   end Other_Task;

end CS_Suspension_Object_2;
