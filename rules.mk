.PHONY: clean

%.o: %.c
	$(CC) -o../libm2os/$@ -c $(CFLAGS) $<

%.o: %.S
	$(AS) -o../libm2os/$@ $(ASFLAGS) $<

%.o: %.adb
	$(GNATMAKE) -c $(GNATFLAGS) $<


clean: clean_gprs clean_dirs clean_global clean_scripts

GPRS = $(shell find -name '*$(M2_TARGET).gpr' ! -name "*__*")
clean_gprs:
	@exec echo -e "\n>> Cleaning GPRs...";
	-@for gpr in $(GPRS); do \
	echo "  Cleaning $$gpr ..."; \
	gprclean -q $$gpr; \
	done
	@exec echo ">> End Cleaning GPRs"


DIRS_TO_CLEAN = tests/ada_tasks examples/ada_tasks
clean_dirs:
	@exec echo -e "\n>> Cleaning dirs... ";
	-@for dir in $(DIRS_TO_CLEAN); do \
	echo "  Cleaning $$dir ..."; \
	make -C $$dir clean; \
	done
	@exec echo ">> End Cleaning dirs"

clean_global:
	@exec echo -e "\n>> General cleaning (*.o, *.ali, links, ...)... ";
	find -type l -print -delete
	-find \( \( -name '*.[oa]' ! -name 'libgcc.a' ! -name 'libe-lib.a' \) \
                -or -name '*~*' -or -name '*.ali'  \
		-or -name '*.exe' -or -name 'mprogram' -or -name 'a.out'  \
		-or -name '*.out' -or -name '*.su'  \
                -or -name '*.stderr' -or -name '*.stdout'  \
                -or -name '*.bexch' -or -name '*.ci'  -or -name '*.bin' \
                -or -name '*loc.xml' \
                -or -name '*.hex' \
                -or -name '*.deps' \
                -or -name '*.lexch' \
                -or -name '*.d' \
                -or -name 'm2osmain.ad?' \
                -or -name 'b__*.ad?' -or -name 'map.txt' \) \
                -delete 2>/dev/null
	-rm -rf obj_m2os_arduino/ obj_m2os_epiphany/ obj_m2os_stm32f4/
	rm -f m2os_tool/bin/ada_tasks_to_m2
	@exec echo ">> End General Cleaning"
	
clean_scripts:
	@exec echo -e "\n>> Scripts cleaning (Arduino_uno, Microbit,...)... ";
	rm -f ~/.gps/plug-ins/m2_buttons_arduino_uno.py
	rm -f ~/.gps/plug-ins/m2_build_actions_arduino_uno.py
	rm -f ~/.gnatstudio/plug-ins/serial_console_button.xml
	@exec echo ">> End Cleaning M2OS Scripts"
