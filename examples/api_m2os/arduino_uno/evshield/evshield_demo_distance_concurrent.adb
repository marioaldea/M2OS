----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Concurrent version for the Autopilot demo.
----------------------------------------------------------------------------
-- Setup:
-- Attach the lego robot with the ultrasonic sensors.
----------------------------------------------------------------------------

with Interfaces.C;

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Arduino.EVShield.EV3Touch;
with Arduino.EVShield.Motors; use Arduino.EVShield.Motors;
with Arduino.EVShield.EV3Ultrasonic;
with SHDefines_H; use SHDefines_H;
with Interfaces.C.Extensions;
with Stdint_H;

procedure EvShield_Demo_Distance_Concurrent is
   package DIO renames M2.Direct_IO;

   -- Program variables
   Count : Interfaces.C.Int;
   Aux : Stdint_H.Uint8_T;
   Aux2 : Interfaces.C.Extensions.Bool;
   Distance_Right : Float;
   Distance_Left : Float;

   package Touch_Sensor is new Arduino.EVShield.EV3Touch.Touch (SHDefines_H.SH_BBS1);
   -- Bank A Motor 1
   package Motor_Right is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM1);
   -- Bank A Motor 2
   package Motor_Left is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM2);
   -- Ultrasonic sensors
   package Ultrasonic_Right is new Arduino.EVShield.EV3Ultrasonic.Ultrasonic (Arduino.EVShield.SH_BAS2);
   package Ultrasonic_Left is new Arduino.EVShield.EV3Ultrasonic.Ultrasonic (Arduino.EVShield.SH_BBS2);
   use type Interfaces.C.Int;
begin
   DIO.Put_Line ("--------------------------------");
   DIO.Put_Line ("Starting EVShield distance demo.");
   DIO.Put_Line ("--------------------------------");

   -- initialize the sensor mode.
   Aux := Ultrasonic_Right.Set_Mode(Ultrasonic_Right.MODE_Sonar_CM);
   Aux := Ultrasonic_Left.Set_Mode(Ultrasonic_Left.MODE_Sonar_CM);

   -- reset motors.
   Aux2 := Motor_Right.Motor_Reset;
   Aux2 := Motor_Left.Motor_Reset;

   loop
      Count := Touch_Sensor.Get_Bump_Count;

      if Count mod 2 = 1 then
         -- get the reading(s) from sensor
         Distance_Right := Ultrasonic_Right.Get_Dist;
         Distance_Left := Ultrasonic_Left.Get_Dist;

         -- Obstacle in right side
         if Distance_Right <= 40.0 then
            -- Obstacle also in left side (backwards)
            if Distance_Left <= 40.0 then
               DIO.Put_Line ("Obtaculos en ambas direcciones --> Dar vuelta.");
               DIO.Put ("Distacia a la derecha: "); DIO.Put (Distance_Right); DIO.New_Line;
               DIO.Put ("Distacia a la izquierda: "); DIO.Put (Distance_Left); DIO.New_Line;

               Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Forward,
                                               15);
               Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Forward,
                                              50);
            else
               -- Turn left
               DIO.Put_Line ("Obtaculo por la derecha --> Girar a la izqierda.");
               DIO.Put ("Distacia a la derecha: "); DIO.Put (Distance_Right); DIO.New_Line;

               Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                               50);
               Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                              15);
            end if;

            -- Obstacle in left side
         elsif Distance_Left <= 40.0 then
            -- Turn left
            DIO.Put_Line ("Obtaculo por la izquierda --> Girar a la derecha.");
            DIO.Put ("Distacia a la izquierda: "); DIO.Put (Distance_Left); DIO.New_Line;

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            15);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           50);
         else
            -- No obstacles. Continue
            DIO.Put_Line ("Sin obstaculos.");

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            30);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           30);
         end if;
      else
         Aux2 := Motor_Right.Motor_Stop(Motor_Right.SH_Next_Action_Brake);
         Aux2 := Motor_Left.Motor_Stop(Motor_Left.SH_Next_Action_Brake);
         DIO.Put_Line ("Demo is STOP. Press touch sensor to start.");
      end if;
      -- wait for some time
      Arduino.C_Delay (1000);
   end loop;

end EvShield_Demo_Distance_Concurrent;
