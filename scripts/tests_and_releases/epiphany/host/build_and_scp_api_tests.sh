#!/bin/bash
M2OS_DIR=$(HOME)/M2OS/
API_TEST_DIR=tests/api_m2os

PARALLELLA_DIR=/home/parallella/Debug
PARALLELLA_USER=parallella
PARALLELLA_PASS="parallella"
PARALLELLA_IP=$(ENV_PARALLELLA_IP)

cd $M2OS_DIR
#Compile M2OS
#make clean
#make install

cd $API_TEST_DIR

gprbuild -v -P tests_api_m2os_epiphany.gpr

#gprbuild -v -P tests_basic_epiphany.gpr delay_and_clock_test.adb
sshpass -p $PARALLELLA_PASS scp delay_and_clock_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_0x0.elf

#gprbuild -P tests_basic_epiphany.gpr float_test.adb
sshpass -p $PARALLELLA_PASS scp float_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_0x1.elf

#gprbuild -P tests_api_m2os_epiphany.gpr periodic_tasks-test.adb
sshpass -p $PARALLELLA_PASS scp periodic_tasks-test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_0x2.elf

#gprbuild -P tests_basic_epiphany.gpr po_entry_closed_access_param_test.adb
sshpass -p $PARALLELLA_PASS scp po_entry_closed_access_param_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_0x3.elf

#gprbuild -P tests_basic_epiphany.gpr po_entry_out_param_test.adb
sshpass -p $PARALLELLA_PASS scp po_entry_out_param_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_1x0.elf

#gprbuild -P tests_basic_epiphany.gpr po_entry_simple_test.adb
sshpass -p $PARALLELLA_PASS scp po_entry_simple_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_1x1.elf

#gprbuild -P tests_basic_epiphany.gpr round_robin_test.adb
sshpass -p $PARALLELLA_PASS scp round_robin_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_1x2.elf

#gprbuild -P tests_basic_epiphany.gpr secondary_stack-test.adb
sshpass -p $PARALLELLA_PASS scp secondary_stack-test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_1x3.elf

#gprbuild -P tests_basic_epiphany.gpr simple_tasks_delay_until_test.adb
sshpass -p $PARALLELLA_PASS scp simple_tasks_delay_until_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_2x0.elf

#gprbuild -P tests_api_m2os_epiphany.gpr suspension_object_open_test.adb
sshpass -p $PARALLELLA_PASS scp suspension_object_open_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_2x1.elf

#gprbuild -P tests_api_m2os_epiphany.gpr timing_events_ada_basic_test.adb
sshpass -p $PARALLELLA_PASS scp timing_events_ada_basic_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_2x2.elf

#gprbuild -P tests_api_m2os_epiphany.gpr timing_events_and_tasks_test.adb
sshpass -p $PARALLELLA_PASS scp timing_events_and_tasks_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_2x3.elf

#gprbuild -P tests_api_m2os_epiphany.gpr timing_events_api_basic_test.adb
sshpass -p $PARALLELLA_PASS scp timing_events_api_basic_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_3x0.elf

#gprbuild -P tests_api_m2os_epiphany.gpr timing_events_api_order_test.adb
sshpass -p $PARALLELLA_PASS scp timing_events_api_order_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_3x1.elf

#gprbuild -v -P tests_api_m2os_epiphany.gpr epi_msg_consumer_test.adb
sshpass -p $PARALLELLA_PASS scp epi_msg_consumer_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_3x2.elf

#gprbuild -v -P tests_api_m2os_epiphany.gpr epi_msg_producer_test.adb
sshpass -p $PARALLELLA_PASS scp epi_msg_producer_test $PARALLELLA_USER@$PARALLELLA_IP:$PARALLELLA_DIR/e_core_3x3.elf

#Copy sources for Debugging
#cd $M2OS_DIR
#sshpass -p "parallella" scp -r * parallella@193.144.198.97:/home/parallella/propio/status_monitor/debug_src
