# usage: include config.mk, from Makefiles
# this file contains all the .config configuration related things
# next variable should be set before including this file.
# In any case, if the makefile is located within the m2os tree
# then we try to set it automatically.

ifndef M2OS
M2OS=$(shell echo `pwd`/ | sed -n \
       's|\(\(M2OS[^/.]*/\)\+\).*|\1|p;' )
endif

$(if $(M2OS),, \
	$(warning "The configuration variable M2OS is not set") \
	$(error "edit this file to set the path to M2OS directory."))

include $(M2OS)/config_params.mk
# defines: CONFIG_BOARD_*,
#          CONFIG_DEBUG, CONFIG_SIZEOPTIMIZATION
#          CONFIG_GNATAVR_COMPILER
#          CONFIG_HOSTGNATFORTOOL

# set M2_TARGET (Board)
M2_TARGET 		= Invalid_target
ifdef CONFIG_BOARD_RPI
M2_TARGET 		= rpi
endif

ifdef CONFIG_BOARD_ARDUINO
M2_TARGET 		= arduino_uno
endif

ifdef CONFIG_BOARD_AVR_IOT
M2_TARGET 		= avr_iot
endif

ifdef CONFIG_BOARD_EPIPHANY
M2_TARGET 		= epiphany
endif

ifdef CONFIG_BOARD_STM32F4
M2_TARGET 		= stm32f4
endif

ifdef CONFIG_BOARD_MICROBIT
M2_TARGET 		= microbit
endif

ifdef CONFIG_BOARD_RP2040
M2_TARGET 		= rp2040
endif

ifdef CONFIG_BOARD_RISCV
M2_TARGET 		= riscv
endif

ifdef CONFIG_BOARD_GNATBB
M2_TARGET 		= gnat_bb
#  Only used when M2_TARGET is gnat_bb
#  Possible values: stm32f4
M2_GNAT_BOARD   = stm32f4
endif

# native gnatmake for building the m2os intrumentation tool
ifdef CONFIG_HOSTGNATFORTOOL
HOSTGNAT_PATH = $(patsubst "%",%,$(CONFIG_HOSTGNATFORTOOL))
endif

# gcc for building menuconfig
HOSTCC          = gcc -m32
HOSTCXX         = g++
HOSTCFLAGS      = -Wall -Wstrict-prototypes -O2 -fomit-frame-pointer
HOSTCXXFLAGS    = -O2

check_gcc = $(shell \
	if $(CC) $(1) -S -o /dev/null -xc /dev/null > /dev/null 2>&1 ;\
	then \
		echo "$(1)";\
	else \
		echo "$(2)";\
	fi)


M2_PATH = $(M2OS)
ARCH_PATH = $(M2OS)arch/$(M2_TARGET)


ifeq ($(M2_TARGET),$(filter $(M2_TARGET),arduino_uno avr_iot))
   GNAT_PATH = $(subst /bin/avr-gnatmake,,$(shell echo `which avr-gnatmake`))
   $(if $(GNAT_PATH),, $(error Not found avr-gnatmake))
   GNAT_AVR_LIB_PATH = $(GNAT_PATH)/avr/lib/gnat/avr_lib

   GCC_PREFIX = avr

   DEFINE_FLAGS = -DF_CPU=16000000UL

ifeq ($(M2_TARGET),arduino_uno)
   ARCH_FLAGS =  -mmcu=atmega328p
else ifeq ($(M2_TARGET),avr_iot)
   ARCH_FLAGS =  -mmcu=atmega4808 -B "$(M2OS)arch/arduino_uno/atmega4808" -l:libatmega4808.a
else
$(error Unexpected target $(M2_TARGET))
endif
   ARCH_CFLAGS =  -ffunction-sections -fdata-sections

   GNAT_TARGET_UTIL_LIB_INC= -aI$(M2_PATH)/lib_arduino_core -aI$(GNAT_AVR_LIB_PATH)/board-arduino -aI$(GNAT_AVR_LIB_PATH)/atmega328p -aI$(GNAT_AVR_LIB_PATH)

   GNAT_TARGET_FLAGS= -gnateDUART=usart0 -gnateDMCU=atmega328p -gnateDM2_TARGET=arduino_uno
   LD_TARGET_LIBS_PATH= -L$(M2_PATH)/lib_arduino_core
   LD_TARGET_LIBS= #-larduinocore

   GNAT_RTS = --RTS=$(M2_PATH)/gnat_rts/arduino_uno  #--RTS=$(M2_PATH)/rts-m2os-arduino

   LD_OPTS_LTO = --LINK=/usr/bin/avr-gcc -flto -fuse-linker-plugin
   LD_OPTS = -Wl,--gc-sections $(LD_OPTS_LTO) -Wl,-Map,mprogram.map,-v # ,-T,../hal/avr5_m2.x
   START_OBJS =

else ifeq ($(M2_TARGET),rpi)
   GNAT_PATH = GNAT_PATH_not_used_with_target_RPi
   GNAT_AVR_LIB_PATH = GNAT_AVR_LIB_PATH_not_used_with_target_RPi

   GCC_PREFIX = arm-eabi

   DEFINE_FLAGS =

   # RPi 1
   ARCH_FLAGS = -mcpu=arm1176jzf-s
   # RPi 2
   #ARCH_FLAGS = -mcpu=cortex-a7 -marm
   # marm must be used to use UART driver

   ASFLAGS += #-marm

   GNAT_TARGET_UTIL_LIB_INC=

   GNAT_TARGET_FLAGS=
   LD_TARGET_LIBS_PATH= -L$(M2_PATH)/libgcc_rpi
   LD_TARGET_LIBS=

   GNAT_RTS = --RTS=$(M2_PATH)/rts-m2os

   LD_OPTS = -v -nodefaultlibs -nostdlib -nostartfiles -static -Wl,-v,--no-wchar-size-warning,--no-undefined,-T,../hal_$(M2_TARGET)/m2_rpi.ld,-Map,mprogram.map
   START_OBJS = $(M2_PATH)/hal_rpi/m2_hal_start.o

else ifeq ($(M2_TARGET),gnat_bb)
   GNAT_PATH = $(subst /bin/arm-eabi-gnat,,$(shell echo `which arm-eabi-gnat`))
   $(if $(GNAT_PATH),, $(error Not found arm-eabi-gnat))

   GCC_PREFIX = arm-eabi

   DEFINE_FLAGS =

   GNAT_RTS = --RTS=$(GNAT_PATH)/arm-eabi/lib/gnat/m2os-$(M2_GNAT_BOARD)-bb

else ifeq ($(M2_TARGET),$(filter $(M2_TARGET),stm32f4 microbit))
   GNAT_PATH = $(subst /bin/arm-eabi-gnat,,$(shell echo `which arm-eabi-gnat`))
   $(if $(GNAT_PATH),, $(error Not found arm-eabi-gnat))

   GCC_PREFIX = arm-eabi

   DEFINE_FLAGS =

   GNAT_RTS = --RTS=$(GNAT_PATH)/arm-eabi/lib/gnat/m2os-$(M2_TARGET)

else ifeq ($(M2_TARGET),rp2040)
   GNAT_PATH = $(subst /bin/arm-eabi-gnat,,$(shell echo `which arm-eabi-gnat`))
   $(if $(GNAT_PATH),, $(error Not found arm-eabi-gnat))

   GCC_PREFIX = arm-eabi

   DEFINE_FLAGS =

   GNAT_RTS = GNAT_RTS_not_used_in_$(M2_TARGET)
   
else ifeq ($(M2_TARGET),riscv)
   GNAT_PATH = $(subst /bin/riscv64-elf-gnat,,$(shell echo `which riscv64-elf-gnat`))
   $(if $(GNAT_PATH),, $(error Not found riscv64-elf-gnat))

   GCC_PREFIX = arm-eabi

   DEFINE_FLAGS =

   GNAT_RTS = GNAT_RTS_not_used_in_$(M2_TARGET)

else ifeq ($(M2_TARGET),epiphany)
   GCC_PREFIX = epiphany-elf
   ARCH_FLAGS =             # used in Makefile.adalib

   GNAT_RTS = --RTS=$(M2OS)/gnat_rts/$(M2_TARGET) # usado en libm2os/Makefile

else

   $(error Unexpected value of M2_TARGET variable: $(M2_TARGET))
endif


CC = $(GCC_PREFIX)-gcc
AS = $(GCC_PREFIX)-as
GNATMAKE = $(GCC_PREFIX)-gnatmake
GNATBIND = $(GCC_PREFIX)-gnatbind
AR = $(GCC_PREFIX)-ar

GNAT_TARGET_FLAGS += -gnateDM2_TARGET=$(M2_TARGET)

GCC_VER = $(shell $(CC) -dumpversion)

# used for RTS
ifdef CONFIG_DEBUG_RTS
DBG_C_RTS=-g -O0
DBG_ADA_RTS=$(DBG_C_RTS)
endif

# used for M2 kernel
ifdef CONFIG_DEBUG_M2
DBG_C_M2=-g -O0
DBG_ADA_M2=$(DBG_C_M2)
endif

# used for applications
ifdef CONFIG_DEBUG_APP
DBG_C_APP=-g -O0
DBG_ADA_APP=$(DBG_C_APP)
endif

# used for RTS
ifdef CONFIG_ASSERTS_RTS
DBG_ADA_RTS+=-gnata -gnato
endif

# used for M2 kernel
ifdef CONFIG_ASSERTS_M2
DBG_ADA_M2+=-gnata -gnato
endif

# used for applications
ifdef CONFIG_ASSERTS_APP
DBG_ADA_APP+=-gnata -gnato
endif

# used for RTS
ifdef CONFIG_SIZEOPTIMIZATION_RTS
OPT_SIZE_FLAGS_RTS=-Os
SUPPRESS_ALL_CHECKS_RTS=-gnatp
endif

# used for M2 kernel
ifdef CONFIG_SIZEOPTIMIZATION_M2
OPT_SIZE_FLAGS_M2=-Os
SUPPRESS_ALL_CHECKS_M2=-gnatp
endif

# used for applications
ifdef CONFIG_SIZEOPTIMIZATION_APP
OPT_SIZE_FLAGS_APP=-Os
SUPPRESS_ALL_CHECKS_APP=-gnatp
endif

CFLAGS +=-Wall $(DBG_C) -Werror $(DEFINE_FLAGS) $(ARCH_FLAGS) $(ARCH_CFLAGS) $(OPT_SIZE_FLAGS)
ASFLAGS += $(ARCH_FLAGS)

GNAT_INC = $(EXTRA_GNAT_INC) -aI$(ARCH_PATH)/drivers -aI$(ARCH_PATH)/hal -aI$(M2_PATH)/kernel -aI$(M2_PATH)/adax $(GNAT_TARGET_UTIL_LIB_INC)
GNATFLAGS += $(ARCH_CFLAGS) -gnat2012 $(GNAT_TARGET_FLAGS) -aL$(ARCH_PATH)/libm2os/ $(GNAT_RTS) $(DEFINE_FLAGS) $(ARCH_FLAGS) $(GNAT_INC)
# -D ../libm2os/  Specify dir as the object directory
# -gnateG muestra los ficheros .prep en libm2os/

GNATBIND_FLAGS +=  #-v -F
LD_LIBS_PATH = -L$(ARCH_PATH)/libm2os $(LD_TARGET_LIBS_PATH)
# -L$(GNAT_PATH)/lib/gcc/avr/4.7.2/avr5 #-L$(GNAT_PATH)/avr/lib/avr5/

LD_LIBS = -Wl,--start-group $(M2_PATH)/rts-m2os-arduino/adalib/libgnarl.a $(M2_PATH)/rts-m2os-arduino/adalib/libgnat.a -lm2os -lgcc -lm2os $(LD_TARGET_LIBS) -Wl,--end-group

