----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  with GNAT.Source_Info;

package M2.Debug is
#if not M2_TARGET = "rp2040" then
   --  pragma Pure;
   --  pragma No_Elaboration_Code_All;
   pragma Preelaborate;
#end if;

   procedure Assert (Assertion : Boolean);
   --                Msg       : String := GNAT.Source_Info.Source_Location);

   procedure Put (Msg : String; Task_Id : Integer;
                  Level : M2.Trace_Levels := M2.Trace);

   procedure Put (Msg_1 : String; Task_Id_1 : Integer;
                  Msg_2 : String; Task_Id_2 : Integer;
                  Level : M2.Trace_Levels := M2.Trace);

   procedure Put (Msg : String;
                  Level : M2.Trace_Levels := M2.Trace);

end M2.Debug;
