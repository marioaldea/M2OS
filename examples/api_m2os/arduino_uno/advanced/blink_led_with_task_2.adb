----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Uses a task to blink the led in pin 13.

with AdaX_Dispatching_Stack_Sharing;

with Arduino;
with Ada.Real_Time;
with M2.Direct_IO;

package body Blink_Led_With_Task_2 is
   package DIO renames M2.Direct_IO;

   use type Arduino.Digital_Pin_Out_Value;

   Led_Pin : constant Arduino.Digital_Pin := 13;

   Led_Status : Arduino.Digital_Pin_Out_Value := Arduino.Out_High;

   use type Ada.Real_Time.Time;
   Next_Time : Ada.Real_Time.Time := Ada.Real_Time.Clock;
   Period : constant Ada.Real_Time.Time_Span := Ada.Real_Time.Seconds (1);

   -------------------
   -- Led_Task_Init --
   -------------------

   procedure Led_Task_Init is
   begin
      Arduino.Pin_Mode (Led_Pin, Arduino.Output);
      DIO.Put_Line ("Led Task init");
   end Led_Task_Init;

   -------------------
   -- Led_Task_Body --
   -------------------

   procedure Led_Task_Body is
   begin
      Arduino.Digital_Write (Led_Pin, Led_Status);

      if Led_Status = Arduino.Out_High then
         Led_Status := Arduino.Out_Low;
      else
         Led_Status := Arduino.Out_High;
      end if;

      DIO.Put ("-LedTask-");
      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Led_Task_Body;

   Led_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Led_Task_Init'Access,
      Body_Ac  => Led_Task_Body'Access,
      Priority => 10);

end Blink_Led_With_Task_2;
