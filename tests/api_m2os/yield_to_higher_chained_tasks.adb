----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Real_Time;
with Ada.Synchronous_Task_Control;
with Ada.Dispatching.Non_Preemptive;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;

with M2.Kernel.API;
with M2.HAL;

with Tests_Reports;

package body Yield_To_Higher_Chained_Tasks is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;
   package API renames M2.Kernel.API;
   package STC renames Ada.Synchronous_Task_Control;
   
   SO5 : STC.Suspension_Object;   
   SO4 : STC.Suspension_Object;   
   SO3 : STC.Suspension_Object;   
   SO2 : STC.Suspension_Object;
   
   Num_Loops  : constant := 2;
   Loop_Counter : Natural := 1 with Volatile;
   Step_Counter : Natural := 0 with Volatile;

   ------------------
   -- Task_P5_Init --
   ------------------

   procedure Task_P5_Init is
   begin
      DIO.Put ("-T5 Init-");
      DIO.New_Line;
      
      STC.Suspend_Until_True (SO5);
   end Task_P5_Init;

   ------------------
   -- Task_P5_Body --
   ------------------

   procedure Task_P5_Body is
   begin
      --  Executes when the low prio tasks opens the suspension object SO5.
      
      DIO.Put ("-T5:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;      
      Tests_Reports.Assert (Step_Counter = 4);
      Step_Counter := Step_Counter + 1;
      
      STC.Suspend_Until_True (SO5);
   end Task_P5_Body;

   -------------
   -- Task_P5 --
   -------------

   Task_P5 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_P5_Init'Access,
      Body_Ac  => Task_P5_Body'Access,
      Priority => 5);

   ------------------
   -- Task_P4_Init --
   ------------------

   procedure Task_P4_Init is
   begin
      DIO.Put ("-T4 Init-");
      DIO.New_Line;
      
      STC.Suspend_Until_True (SO4);
   end Task_P4_Init;

   ------------------
   -- Task_P4_Body --
   ------------------

   procedure Task_P4_Body is
   begin
      --  Executes when the low prio tasks opens the suspension object SO5.
      
      DIO.Put ("-T4:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;     
      Tests_Reports.Assert (Step_Counter = 5);
      Step_Counter := Step_Counter + 1;
      
      STC.Suspend_Until_True (SO4);
   end Task_P4_Body;

   -------------
   -- Task_P4 --
   -------------

   Task_P4 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_P4_Init'Access,
      Body_Ac  => Task_P4_Body'Access,
      Priority => 4);

   ------------------
   -- Task_P3_Init --
   ------------------

   procedure Task_P3_Init is
   begin
      DIO.Put ("-T3 Init-");
      DIO.New_Line;
      
      STC.Suspend_Until_True (SO3);
   end Task_P3_Init;

   ------------------
   -- Task_P3_Body --
   ------------------

   procedure Task_P3_Body is
   begin
      DIO.Put ("-T3a:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;     
      Tests_Reports.Assert (Step_Counter = 2);
      Step_Counter := Step_Counter + 1;
      
      --  Activate low prio non-yield task
      STC.Set_True (SO2);
      if Loop_Counter /= 1 then
         --  Activate other higher priority non-yield tasks
         STC.Set_True (SO5);
         STC.Set_True (SO4);
      end if;
      
      M2.Kernel.API.Eat (0.05);
      
      DIO.Put ("-T3b:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;    
      Tests_Reports.Assert (Step_Counter = 3);
      Step_Counter := Step_Counter + 1;
      
      --  Yields CPU
      Ada.Dispatching.Non_Preemptive.Yield_To_Higher;
      
      DIO.Put ("-T3c:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;            
      if Loop_Counter = 1 then 
         Tests_Reports.Assert (Step_Counter = 4);
      else 
         Tests_Reports.Assert (Step_Counter = 6);
      end if;
      Step_Counter := Step_Counter + 1;
      
      M2.Kernel.API.Eat (0.05);
            
      STC.Suspend_Until_True (SO3);
   end Task_P3_Body;

   -------------
   -- Task_P3 --
   -------------

   Task_P3 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_P3_Init'Access,
      Body_Ac  => Task_P3_Body'Access,
      Priority => 3);
   
   ------------------
   -- Task_P2_Init --
   ------------------

   procedure Task_P2_Init is
   begin
      DIO.Put ("-T2 Init-");
      DIO.New_Line;
      
      STC.Suspend_Until_True (SO2);
   end Task_P2_Init;

   ------------------
   -- Task_P2_Body --
   ------------------

   procedure Task_P2_Body is
   begin
      --  Executes when the low prio tasks opens the suspension object SO5.
      
      DIO.Put ("-T2:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;            
      if Loop_Counter = 1 then       
         Tests_Reports.Assert (Step_Counter = 5);
      else       
         Tests_Reports.Assert (Step_Counter = 7);
      end if;
      Step_Counter := Step_Counter + 1;
      
      STC.Suspend_Until_True (SO2);
   end Task_P2_Body;

   -------------
   -- Task_P2 --
   -------------

   Task_P2 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_P2_Init'Access,
      Body_Ac  => Task_P2_Body'Access,
      Priority => 2);

   ------------------
   -- Task_P1_Init --
   ------------------

   procedure Task_P1_Init is
   begin
      DIO.Put ("-T1 Init-");
      DIO.New_Line;
   end Task_P1_Init;

   ------------------
   -- Task_P1_Body --
   ------------------

   procedure Task_P1_Body is
   begin
      DIO.Put ("-T1a:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;     
      Tests_Reports.Assert (Step_Counter = 0);
      Step_Counter := Step_Counter + 1;
      
      --  Activate the higher priority yield task
      STC.Set_True (SO3);
      
      M2.Kernel.API.Eat (0.05);
      
      DIO.Put ("-T1b:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;     
      Tests_Reports.Assert (Step_Counter = 1);
      Step_Counter := Step_Counter + 1;
      
      --  Yields CPU
      Ada.Dispatching.Non_Preemptive.Yield_To_Higher;
      
      DIO.Put ("-T1c:");
      DIO.Put (Integer (Step_Counter));
      DIO.Put (" SP:");
      DIO.Put (Integer (M2.HAL.Current_Stack_Top), 16);
      DIO.New_Line;  
      if Loop_Counter = 1 then        
         Tests_Reports.Assert (Step_Counter = 6);
      else        
         Tests_Reports.Assert (Step_Counter = 8);
      end if;
      Step_Counter := Step_Counter + 1;
      
      M2.Kernel.API.Eat (0.05);
      
      if Loop_Counter = Num_Loops then
         Tests_Reports.Test_OK;
      end if;      
      Loop_Counter := Loop_Counter + 1;
      Step_Counter := 0;
      
      delay until Ada.Real_Time.Time_First;
   end Task_P1_Body;

   -------------
   -- Task_P1 --
   -------------

   Task_P1 : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Task_P1_Init'Access,
      Body_Ac  => Task_P1_Body'Access,
      Priority => 1);

end Yield_To_Higher_Chained_Tasks;
