
with Interfaces;
with M2.HAL;

with Measurements_Generic;

package Measurements_LoRes is new
   Measurements_Generic (Time_T_Hz   => M2.HAL.HWTime_Hz,
                         Time_T      => M2.HAL.HWTime,
                         Time_Long_T => Interfaces.Unsigned_32,
                         Get_Time    => M2.HAL.Get_HWTime,
#if M2_TARGET = "arduino_uno" then
                         Time_Units_T    => Interfaces.Unsigned_32,
                         Time_Unit_T_Hz  => 1_000,
                         Time_Unit_Char  => 'm');
   
#elsif M2_TARGET = "stm32f4" or M2_TARGET = "microbit" or M2_TARGET = "epiphany" or M2_TARGET = "avr_iot" then 
                         Time_Units_T   => Interfaces.Unsigned_32,
                         Time_Unit_T_Hz => 1_000_000,
                         Time_Unit_Char => 'u'); 
   
#else
   pragma Compile_Time_Error (True, "Unexpected architecture");
#end if;
