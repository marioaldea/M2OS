----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with System;

with Ada.Real_Time.Timing_Events;

with M2.Direct_IO;
with Tests_Reports;

with Timing_Events_Ada_Basic_PO;

procedure Timing_Events_Ada_Basic_Test  is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;
   package TE renames Ada.Real_Time.Timing_Events;

   use type Ada.Real_Time.Time, TE.Timing_Event_Handler;

   T : Ada.Real_Time.Time;
   Handler : TE.Timing_Event_Handler;
   Cancelled : Boolean;

begin
   DIO.Put_Line ("-Timing_Events_Basic_Test-");

   DIO.Put_Line ("Tests initial state of Event");

   T := TE.Time_Of_Event (Timing_Events_Ada_Basic_PO.Event);
   Tests_Reports.Assert (T = RT.Time_First);

   Handler := TE.Current_Handler (Timing_Events_Ada_Basic_PO.Event);
   Tests_Reports.Assert (Handler = null);

   TE.Cancel_Handler (Timing_Events_Ada_Basic_PO.Event, Cancelled);
   Tests_Reports.Assert (not Cancelled);

   DIO.Put_Line ("Tests state of a 'set' Event");

   TE.Set_Handler (Timing_Events_Ada_Basic_PO.Event,
                   RT.Time_Last,
                   Timing_Events_Ada_Basic_PO.PO.PO_Handler'Access);

   T := TE.Time_Of_Event (Timing_Events_Ada_Basic_PO.Event);
   Tests_Reports.Assert (T = RT.Time_Last);

   Handler := TE.Current_Handler (Timing_Events_Ada_Basic_PO.Event);
   Tests_Reports.Assert
     (Handler = Timing_Events_Ada_Basic_PO.PO.PO_Handler'Access);

   TE.Cancel_Handler (Timing_Events_Ada_Basic_PO.Event, Cancelled);
   Tests_Reports.Assert (Cancelled);

   DIO.Put_Line ("Tests state of a 'cleared' Event");

   T := TE.Time_Of_Event (Timing_Events_Ada_Basic_PO.Event);
   Tests_Reports.Assert (T = RT.Time_First);

   Handler := TE.Current_Handler (Timing_Events_Ada_Basic_PO.Event);
   Tests_Reports.Assert (Handler = null);

   TE.Cancel_Handler (Timing_Events_Ada_Basic_PO.Event, Cancelled);
   Tests_Reports.Assert (not Cancelled);

   Tests_Reports.Test_OK;

end Timing_Events_Ada_Basic_Test;
