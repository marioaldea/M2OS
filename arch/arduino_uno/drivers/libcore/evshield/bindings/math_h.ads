pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package math_h is

   M_E : constant := 2.7182818284590452354;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:71

   M_LOG2E : constant := 1.4426950408889634074;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:74

   M_LOG10E : constant := 0.43429448190325182765;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:77

   M_LN2 : constant := 0.69314718055994530942;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:80

   M_LN10 : constant := 2.30258509299404568402;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:83

   M_PI : constant := 3.14159265358979323846;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:86

   M_PI_2 : constant := 1.57079632679489661923;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:89

   M_PI_4 : constant := 0.78539816339744830962;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:92

   M_1_PI : constant := 0.31830988618379067154;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:95

   M_2_PI : constant := 0.63661977236758134308;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:98

   M_2_SQRTPI : constant := 1.12837916709551257390;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:101

   M_SQRT2 : constant := 1.41421356237309504880;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:104

   M_SQRT1_2 : constant := 0.70710678118654752440;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:107
   --  unsupported macro: NAN __builtin_nan("")
   --  unsupported macro: INFINITY __builtin_inf()
   --  unsupported macro: cosf cos
   --  unsupported macro: sinf sin
   --  unsupported macro: tanf tan
   --  unsupported macro: fabsf fabs
   --  unsupported macro: fmodf fmod
   --  unsupported macro: cbrtf cbrt
   --  unsupported macro: hypotf hypot
   --  unsupported macro: squaref square
   --  unsupported macro: floorf floor
   --  unsupported macro: ceilf ceil
   --  unsupported macro: frexpf frexp
   --  unsupported macro: ldexpf ldexp
   --  unsupported macro: expf exp
   --  unsupported macro: coshf cosh
   --  unsupported macro: sinhf sinh
   --  unsupported macro: tanhf tanh
   --  unsupported macro: acosf acos
   --  unsupported macro: asinf asin
   --  unsupported macro: atanf atan
   --  unsupported macro: atan2f atan2
   --  unsupported macro: logf log
   --  unsupported macro: log10f log10
   --  unsupported macro: powf pow
   --  unsupported macro: isnanf isnan
   --  unsupported macro: isinff isinf
   --  unsupported macro: isfinitef isfinite
   --  unsupported macro: copysignf copysign
   --  unsupported macro: signbitf signbit
   --  unsupported macro: fdimf fdim
   --  unsupported macro: fmaf fma
   --  unsupported macro: fmaxf fmax
   --  unsupported macro: fminf fmin
   --  unsupported macro: truncf trunc
   --  unsupported macro: roundf round
   --  unsupported macro: lroundf lround
   --  unsupported macro: lrintf lrint

   function cos (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:127
   pragma Import (C, cos, "cos");

   function sin (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:133
   pragma Import (C, sin, "sin");

   function tan (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:139
   pragma Import (C, tan, "tan");

   function fabs (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:146
   pragma Import (C, fabs, "fabs");

   function fmod (uu_x : double; uu_y : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:153
   pragma Import (C, fmod, "fmod");

   function modf (uu_x : double; uu_iptr : access double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:168
   pragma Import (C, modf, "modf");

   function modff (uu_x : float; uu_iptr : access float) return float;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:171
   pragma Import (C, modff, "modff");

   function sqrt (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:176
   pragma Import (C, sqrt, "sqrt");

   function sqrtf (arg1 : float) return float;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:179
   pragma Import (C, sqrtf, "sqrtf");

   function cbrt (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:184
   pragma Import (C, cbrt, "cbrt");

   function hypot (uu_x : double; uu_y : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:195
   pragma Import (C, hypot, "hypot");

   function square (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:203
   pragma Import (C, square, "square");

   function floor (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:210
   pragma Import (C, floor, "floor");

   function ceil (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:217
   pragma Import (C, ceil, "ceil");

   function frexp (uu_x : double; uu_pexp : access int) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:235
   pragma Import (C, frexp, "frexp");

   function ldexp (uu_x : double; uu_exp : int) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:243
   pragma Import (C, ldexp, "ldexp");

   function exp (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:249
   pragma Import (C, exp, "exp");

   function cosh (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:255
   pragma Import (C, cosh, "cosh");

   function sinh (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:261
   pragma Import (C, sinh, "sinh");

   function tanh (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:267
   pragma Import (C, tanh, "tanh");

   function acos (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:275
   pragma Import (C, acos, "acos");

   function asin (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:283
   pragma Import (C, asin, "asin");

   function atan (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:290
   pragma Import (C, atan, "atan");

   function atan2 (uu_y : double; uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:299
   pragma Import (C, atan2, "atan2");

   function log (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:305
   pragma Import (C, log, "log");

   function log10 (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:311
   pragma Import (C, log10, "log10");

   function pow (uu_x : double; uu_y : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:317
   pragma Import (C, pow, "pow");

   function isnan (uu_x : double) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:324
   pragma Import (C, isnan, "isnan");

   function isinf (uu_x : double) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:334
   pragma Import (C, isinf, "isinf");

   function isfinite (uu_x : double) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:341
   pragma Import (C, isfinite, "isfinite");

   function copysign (uu_x : double; uu_y : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:359
   pragma Import (C, copysign, "copysign");

   function signbit (uu_x : double) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:377
   pragma Import (C, signbit, "signbit");

   function fdim (uu_x : double; uu_y : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:384
   pragma Import (C, fdim, "fdim");

   function fma
     (uu_x : double;
      uu_y : double;
      uu_z : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:393
   pragma Import (C, fma, "fma");

   function fmax (uu_x : double; uu_y : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:401
   pragma Import (C, fmax, "fmax");

   function fmin (uu_x : double; uu_y : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:409
   pragma Import (C, fmin, "fmin");

   function trunc (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:416
   pragma Import (C, trunc, "trunc");

   function round (uu_x : double) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:427
   pragma Import (C, round, "round");

   function lround (uu_x : double) return long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:440
   pragma Import (C, lround, "lround");

   function lrint (uu_x : double) return long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/math.h:454
   pragma Import (C, lrint, "lrint");

end math_h;
