----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
------------------------------------------------------------------------
--
--  "Freeze!" children game.
--  Freeze while the buzzer is silent or the alarm will sound!!
--
--  Requires:
--   - Passive Infrared Sensor (PIR)
--   - Buzzer (or Piezo Speaker)
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |            VCC|-----------------|5V                   |
--     | PIR     SIGNAL|-----------------|D5       Arduino Uno |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 |                     |
--                                       |                     |
--     -----------------                 |                     |
--     |               |                 |                     |
--     | Buzzer       +|-----------------|~D6                  |
--     |              -|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 |                     |
--                                       -----------------------
--
--  Application is made of 3 tasks:
--   - Game_Task: game dynamics.
--   - Buzzer: plays melodies and sounds.
--   - PIR: account PIR detecctions.
--
------------------------------------------------------------------------
with System;
with Ada.Real_Time;

with Arduino;

package Freeze_Game is
   Pin_PIR : constant Arduino.Digital_Pin := 5;
   Pin_Buzzer : constant Arduino.PWM_Digital_Pin := Arduino.PWM_D6;

   Buzzer_Task_Prio : constant System.Priority := 9;
   Game_Task_Prio : constant System.Priority := 10;
   PIR_Task_Prio : constant System.Priority := 11;

   PIR_Task_Polling_Period_Ms : constant := 100;

   Freeze_Phase_Period_Ms : constant := 500;
   Freeze_Phase_Periods : constant := 20;

   Positive_Detections_In_Polling_Period : constant :=
     Freeze_Phase_Period_Ms / PIR_Task_Polling_Period_Ms;

   PIR_Task_Polling_Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (PIR_Task_Polling_Period_Ms);

   type Game_Phase is (Move, Pre_Freeze, Freeze, Warning, Detected);

   Waring_Phase_Period_Ms : constant := 3_000;
   Detect_Phase_Period_Ms : constant := 4_000;

   Phase_Period : constant array (Game_Phase) of Ada.Real_Time.Time_Span :=
     (Move => Ada.Real_Time.Seconds (6),
      Pre_Freeze => Ada.Real_Time.Seconds (2),
      Freeze => Ada.Real_Time.Milliseconds (Freeze_Phase_Period_Ms),
      Warning =>  Ada.Real_Time.Milliseconds (Waring_Phase_Period_Ms),
      Detected => Ada.Real_Time.Milliseconds (Detect_Phase_Period_Ms));
end Freeze_Game;
