----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces;
with System;

with M2.HAL;

package M2.Kernel is
#if not M2_TARGET = "rp2040" then
   pragma Preelaborate;
   --  pragma No_Elaboration_Code_All;
#end if;

   use type Interfaces.Unsigned_16;

   --------------
   -- Priority --
   --------------

   subtype Priority is System.Priority;

   ----------------
   -- Thread Ids --
   ----------------

   Invalid_Thread_Id : constant := 0;

   type Thread_Id_Full is new Interfaces.Unsigned_8 range
     0 .. Interfaces.Unsigned_8'Last;

   subtype Thread_Id is Thread_Id_Full range
     1 .. Thread_Id_Full'Last;

   -----------------
   -- Thread body --
   -----------------

--   subtype Thread_Job_Body_Ac is M2.HAL.Procedure_Ac;
   --  "job body" of the task. Stored in context by
   --  API.Set_Job_Body_Of_Running_Thread. Executed in every thread activation
   --  but in the first one.


   type Thread_Init_Body_Ac is access procedure (Args : System.Address);
   --  Initial body of the thread: it is the main body of the Ada task that
   --  receives an Address parameter with the task type discriminants (in
   --  the case the task has been created from a task type with
   --  discriminants).
   --  Only executed in the first activation when called from
   --  Scheduler.Thread_Body_Wrapper, subsecuent activations execute the
   --  "job body" stored in context by API.Set_Job_Body_Of_Running_Thread.
   --  pragma Convention (C, Thread_Init_Body_Ac);


--     procedure Enter_Kernel;
--
--     procedure Leave_Kernel;

end M2.Kernel;
