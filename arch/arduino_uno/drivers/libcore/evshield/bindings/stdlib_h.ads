pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with System;
with stddef_h;
with Interfaces.C.Strings;

package stdlib_h is

   --  arg-macro: procedure labs (__i)
   --    __builtin_labs(__i)
   RAND_MAX : constant := 16#7FFF#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:364

   RANDOM_MAX : constant := 16#7FFFFFFF#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:576

   DTOSTR_ALWAYS_SIGN : constant := 16#01#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:617

   DTOSTR_PLUS_SIGN : constant := 16#02#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:620

   DTOSTR_UPPERCASE : constant := 16#04#;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:623

   EXIT_SUCCESS : constant := 0;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:673

   EXIT_FAILURE : constant := 1;  --  /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:679

   type div_t is record
      quot : aliased int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:71
      c_rem : aliased int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:72
   end record;
   pragma Convention (C_Pass_By_Copy, div_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:73

   --  skipped anonymous struct anon_7

   type ldiv_t is record
      quot : aliased long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:77
      c_rem : aliased long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:78
   end record;
   pragma Convention (C_Pass_By_Copy, ldiv_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:79

   --  skipped anonymous struct anon_8

   type uu_compar_fn_t is access function (arg1 : System.Address; arg2 : System.Address) return int;
   pragma Convention (C, uu_compar_fn_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:82

   procedure c_abort;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:116
   pragma Import (C, c_abort, "abort");

   function c_abs (uu_i : int) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:121
   pragma Import (C, c_abs, "abs");

   function labs (uu_i : long) return long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:130
   pragma Import (C, labs, "labs");

   function bsearch
     (uu_key : System.Address;
      uu_base : System.Address;
      uu_nmemb : stddef_h.size_t;
      uu_size : stddef_h.size_t;
      uu_compar : access function (arg1 : System.Address; arg2 : System.Address) return int) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:153
   pragma Import (C, bsearch, "bsearch");

   function div (uu_num : int; uu_denom : int) return div_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:162
   pragma Import (C, div, "__divmodhi4");

   function ldiv (uu_num : long; uu_denom : long) return ldiv_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:168
   pragma Import (C, ldiv, "__divmodsi4");

   procedure qsort
     (uu_base : System.Address;
      uu_nmemb : stddef_h.size_t;
      uu_size : stddef_h.size_t;
      uu_compar : uu_compar_fn_t);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:185
   pragma Import (C, qsort, "qsort");

   function strtol
     (uu_nptr : Interfaces.C.Strings.chars_ptr;
      uu_endptr : System.Address;
      uu_base : int) return long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:218
   pragma Import (C, strtol, "strtol");

   function strtoul
     (uu_nptr : Interfaces.C.Strings.chars_ptr;
      uu_endptr : System.Address;
      uu_base : int) return unsigned_long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:252
   pragma Import (C, strtoul, "strtoul");

   function atol (uu_s : Interfaces.C.Strings.chars_ptr) return long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:264
   pragma Import (C, atol, "atol");

   function atoi (uu_s : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:276
   pragma Import (C, atoi, "atoi");

   procedure c_exit (uu_status : int);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:288
   pragma Import (C, c_exit, "exit");

   function malloc (uu_size : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:300
   pragma Import (C, malloc, "malloc");

   procedure free (uu_ptr : System.Address);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:307
   pragma Import (C, free, "free");

   function calloc (uu_nele : stddef_h.size_t; uu_size : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:329
   pragma Import (C, calloc, "calloc");

   function realloc (uu_ptr : System.Address; uu_size : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:348
   pragma Import (C, realloc, "realloc");

   function strtod (uu_nptr : Interfaces.C.Strings.chars_ptr; uu_endptr : System.Address) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:350
   pragma Import (C, strtod, "strtod");

   function atof (uu_nptr : Interfaces.C.Strings.chars_ptr) return double;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:361
   pragma Import (C, atof, "atof");

   function rand return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:383
   pragma Import (C, rand, "rand");

   procedure srand (uu_seed : unsigned);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:387
   pragma Import (C, srand, "srand");

   function rand_r (uu_ctx : access unsigned_long) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:394
   pragma Import (C, rand_r, "rand_r");

   function itoa
     (uu_val : int;
      uu_s : Interfaces.C.Strings.chars_ptr;
      uu_radix : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:429
   pragma Import (C, itoa, "itoa");

   function ltoa
     (uu_val : long;
      uu_s : Interfaces.C.Strings.chars_ptr;
      uu_radix : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:474
   pragma Import (C, ltoa, "ltoa");

   function utoa
     (uu_val : unsigned;
      uu_s : Interfaces.C.Strings.chars_ptr;
      uu_radix : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:517
   pragma Import (C, utoa, "utoa");

   function ultoa
     (uu_val : unsigned_long;
      uu_s : Interfaces.C.Strings.chars_ptr;
      uu_radix : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:559
   pragma Import (C, ultoa, "ultoa");

   function random return long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:590
   pragma Import (C, random, "random");

   procedure srandom (uu_seed : unsigned_long);  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:595
   pragma Import (C, srandom, "srandom");

   function random_r (uu_ctx : access unsigned_long) return long;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:603
   pragma Import (C, random_r, "random_r");

   function dtostre
     (uu_val : double;
      uu_s : Interfaces.C.Strings.chars_ptr;
      uu_prec : unsigned_char;
      uu_flags : unsigned_char) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:649
   pragma Import (C, dtostre, "dtostre");

   function dtostrf
     (uu_val : double;
      uu_width : signed_char;
      uu_prec : unsigned_char;
      uu_s : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:666
   pragma Import (C, dtostrf, "dtostrf");

   function c_system (arg1 : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:686
   pragma Import (C, c_system, "system");

   function getenv (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/stdlib.h:687
   pragma Import (C, getenv, "getenv");

end stdlib_h;
