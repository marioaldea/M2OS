pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with System;
with stddef_h;
with Interfaces.C.Strings;
with inttypes_h;

package avr_pgmspace_h is

   --  unsupported macro: PROGMEM __ATTR_PROGMEM__
   --  unsupported macro: PGM_P const char *
   --  unsupported macro: PGM_VOID_P const void *
   --  arg-macro: function PSTR (s)
   --    return __extension__({static const char __c() PROGMEM := (s); and__c(0);});
   --  arg-macro: procedure pgm_read_byte_near (address_short)
   --    __LPM((uint16_t)(address_short))
   --  arg-macro: procedure pgm_read_word_near (address_short)
   --    __LPM_word((uint16_t)(address_short))
   --  arg-macro: procedure pgm_read_dword_near (address_short)
   --    __LPM_dword((uint16_t)(address_short))
   --  arg-macro: procedure pgm_read_float_near (address_short)
   --    __LPM_float((uint16_t)(address_short))
   --  arg-macro: function pgm_read_ptr_near (address_short)
   --    return void*)__LPM_word((uint16_t)(address_short);
   --  arg-macro: procedure pgm_read_byte (address_short)
   --    pgm_read_byte_near(address_short)
   --  arg-macro: procedure pgm_read_word (address_short)
   --    pgm_read_word_near(address_short)
   --  arg-macro: procedure pgm_read_dword (address_short)
   --    pgm_read_dword_near(address_short)
   --  arg-macro: procedure pgm_read_float (address_short)
   --    pgm_read_float_near(address_short)
   --  arg-macro: procedure pgm_read_ptr (address_short)
   --    pgm_read_ptr_near(address_short)
   --  arg-macro: function pgm_get_far_address (var)
   --    return { uint_farptr_t tmp; __asm__ __volatile__( "ldi	%A0, lo8(%1)" "" & ASCII.LF & "" & ASCII.HT & "" "ldi	%B0, hi8(%1)" "" & ASCII.LF & "" & ASCII.HT & "" "ldi	%C0, hh8(%1)" "" & ASCII.LF & "" & ASCII.HT & "" "clr	%D0" "" & ASCII.LF & "" & ASCII.HT & "" : "=d" (tmp) : "p" (and(var)) ); tmp; };
   function memchr_P
     (arg1 : System.Address;
      uu_val : int;
      uu_len : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1158
   pragma Import (C, memchr_P, "memchr_P");

   function memcmp_P
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1172
   pragma Import (C, memcmp_P, "memcmp_P");

   function memccpy_P
     (arg1 : System.Address;
      arg2 : System.Address;
      uu_val : int;
      arg4 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1179
   pragma Import (C, memccpy_P, "memccpy_P");

   function memcpy_P
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1188
   pragma Import (C, memcpy_P, "memcpy_P");

   function memmem_P
     (arg1 : System.Address;
      arg2 : stddef_h.size_t;
      arg3 : System.Address;
      arg4 : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1195
   pragma Import (C, memmem_P, "memmem_P");

   function memrchr_P
     (arg1 : System.Address;
      uu_val : int;
      uu_len : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1207
   pragma Import (C, memrchr_P, "memrchr_P");

   function strcat_P (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1217
   pragma Import (C, strcat_P, "strcat_P");

   function strchr_P (arg1 : Interfaces.C.Strings.chars_ptr; uu_val : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1233
   pragma Import (C, strchr_P, "strchr_P");

   function strchrnul_P (arg1 : Interfaces.C.Strings.chars_ptr; uu_val : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1245
   pragma Import (C, strchrnul_P, "strchrnul_P");

   function strcmp_P (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1258
   pragma Import (C, strcmp_P, "strcmp_P");

   function strcpy_P (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1268
   pragma Import (C, strcpy_P, "strcpy_P");

   function strcasecmp_P (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1285
   pragma Import (C, strcasecmp_P, "strcasecmp_P");

   function strcasestr_P (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1292
   pragma Import (C, strcasestr_P, "strcasestr_P");

   function strcspn_P (uu_s : Interfaces.C.Strings.chars_ptr; uu_reject : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1305
   pragma Import (C, strcspn_P, "strcspn_P");

   function strlcat_P
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1321
   pragma Import (C, strlcat_P, "strlcat_P");

   function strlcpy_P
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1334
   pragma Import (C, strlcpy_P, "strlcpy_P");

   function strnlen_P (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1346
   pragma Import (C, strnlen_P, "strnlen_P");

   function strncmp_P
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1357
   pragma Import (C, strncmp_P, "strncmp_P");

   function strncasecmp_P
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1376
   pragma Import (C, strncasecmp_P, "strncasecmp_P");

   function strncat_P
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1387
   pragma Import (C, strncat_P, "strncat_P");

   function strncpy_P
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1401
   pragma Import (C, strncpy_P, "strncpy_P");

   function strpbrk_P (uu_s : Interfaces.C.Strings.chars_ptr; uu_accept : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1416
   pragma Import (C, strpbrk_P, "strpbrk_P");

   function strrchr_P (arg1 : Interfaces.C.Strings.chars_ptr; uu_val : int) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1427
   pragma Import (C, strrchr_P, "strrchr_P");

   function strsep_P (uu_sp : System.Address; uu_delim : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1447
   pragma Import (C, strsep_P, "strsep_P");

   function strspn_P (uu_s : Interfaces.C.Strings.chars_ptr; uu_accept : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1460
   pragma Import (C, strspn_P, "strspn_P");

   function strstr_P (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1474
   pragma Import (C, strstr_P, "strstr_P");

   function strtok_P (uu_s : Interfaces.C.Strings.chars_ptr; uu_delim : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1496
   pragma Import (C, strtok_P, "strtok_P");

   function strtok_rP
     (uu_s : Interfaces.C.Strings.chars_ptr;
      uu_delim : Interfaces.C.Strings.chars_ptr;
      uu_last : System.Address) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1516
   pragma Import (C, strtok_rP, "strtok_rP");

   function strlen_PF (src : inttypes_h.uint_farptr_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1529
   pragma Import (C, strlen_PF, "strlen_PF");

   function strnlen_PF (src : inttypes_h.uint_farptr_t; len : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1545
   pragma Import (C, strnlen_PF, "strnlen_PF");

   function memcpy_PF
     (dest : System.Address;
      src : inttypes_h.uint_farptr_t;
      len : stddef_h.size_t) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1560
   pragma Import (C, memcpy_PF, "memcpy_PF");

   function strcpy_PF (dest : Interfaces.C.Strings.chars_ptr; src : inttypes_h.uint_farptr_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1575
   pragma Import (C, strcpy_PF, "strcpy_PF");

   function strncpy_PF
     (dest : Interfaces.C.Strings.chars_ptr;
      src : inttypes_h.uint_farptr_t;
      len : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1595
   pragma Import (C, strncpy_PF, "strncpy_PF");

   function strcat_PF (dest : Interfaces.C.Strings.chars_ptr; src : inttypes_h.uint_farptr_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1611
   pragma Import (C, strcat_PF, "strcat_PF");

   function strlcat_PF
     (dst : Interfaces.C.Strings.chars_ptr;
      src : inttypes_h.uint_farptr_t;
      siz : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1632
   pragma Import (C, strlcat_PF, "strlcat_PF");

   function strncat_PF
     (dest : Interfaces.C.Strings.chars_ptr;
      src : inttypes_h.uint_farptr_t;
      len : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1649
   pragma Import (C, strncat_PF, "strncat_PF");

   function strcmp_PF (s1 : Interfaces.C.Strings.chars_ptr; s2 : inttypes_h.uint_farptr_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1665
   pragma Import (C, strcmp_PF, "strcmp_PF");

   function strncmp_PF
     (s1 : Interfaces.C.Strings.chars_ptr;
      s2 : inttypes_h.uint_farptr_t;
      n : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1682
   pragma Import (C, strncmp_PF, "strncmp_PF");

   function strcasecmp_PF (s1 : Interfaces.C.Strings.chars_ptr; s2 : inttypes_h.uint_farptr_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1698
   pragma Import (C, strcasecmp_PF, "strcasecmp_PF");

   function strncasecmp_PF
     (s1 : Interfaces.C.Strings.chars_ptr;
      s2 : inttypes_h.uint_farptr_t;
      n : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1716
   pragma Import (C, strncasecmp_PF, "strncasecmp_PF");

   function strstr_PF (s1 : Interfaces.C.Strings.chars_ptr; s2 : inttypes_h.uint_farptr_t) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1732
   pragma Import (C, strstr_PF, "strstr_PF");

   function strlcpy_PF
     (dst : Interfaces.C.Strings.chars_ptr;
      src : inttypes_h.uint_farptr_t;
      siz : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1744
   pragma Import (C, strlcpy_PF, "strlcpy_PF");

   function memcmp_PF
     (arg1 : System.Address;
      arg2 : inttypes_h.uint_farptr_t;
      arg3 : stddef_h.size_t) return int;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1760
   pragma Import (C, memcmp_PF, "memcmp_PF");

   --  skipped func __strlen_P

   function strlen_P (s : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h:1781
   pragma Import (C, strlen_P, "strlen_P");

end avr_pgmspace_h;
