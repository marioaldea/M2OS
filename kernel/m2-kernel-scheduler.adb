----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Unchecked_Conversion;

with M2.Kernel.Ready_Queue;
with M2.Kernel.Dispatcher;
with M2.Kernel.Timing_Events;

with M2.Direct_IO;
with M2.Debug;

package body M2.Kernel.Scheduler is
   package TE renames M2.Kernel.Timing_Events;
   package RQ renames M2.Kernel.Ready_Queue;

   use type TCBs.TCB_Ac, HAL.HWTime, Interfaces.Unsigned_8;

   --------------
   -- End_Main --
   --------------

   -- Called at the end of the main procedure

   procedure End_Main;
#if M2_TARGET = "arduino_uno" or M2_TARGET = "avr_iot" then
   pragma Export (C, End_Main, "exit");
#elsif M2_TARGET = "stm32f4" or M2_TARGET = "microbit" or M2_TARGET = "epiphany"
   pragma Export (C, End_Main, "_exit");
#end if;

   procedure End_Main is
   begin
      Dispatcher.Dispatch;
      --  XXX call End_Execution all but the first time ??
   end End_Main;

   ------------------
   -- Tick_Handler --
   ------------------

   procedure Tick_Handler (Now : HAL.HWTime) is
      use type TE.Timing_Event_Ac, HAL.HWTime, TE.Timing_Event_Handler;
      Event : TE.Timing_Event_Ac;
      Handler : TE.Timing_Event_Handler;
   begin
      if not M2.Use_Timing_Events'First then
         return;
      end if;

      loop
         Event := TE.Queue_Head;

         exit when Event = null
           or else TE.Time_Of_Event (Event.all) > Now;

         Handler := TE.Current_Handler (Event.all);
         pragma Assert (Handler /= null);

         TE.Queue_Dequeue_Head;
         TE.Clear_Event (Event);

         Handler.all (Event.all);
      end loop;
   end Tick_Handler;

   -------------------------
   -- Thread_Body_Wrapper --
   -------------------------

   procedure Thread_Body_Wrapper is
      type Init_Ada_Task_Body_Ac is access procedure (Args : System.Address);
      function To_Init_Ada_Task_Body_Ac is new
        Ada.Unchecked_Conversion(HAL.Code_Address, Init_Ada_Task_Body_Ac);
   begin
      To_Init_Ada_Task_Body_Ac (Ready_Queue.Running_Thread.Body_Ac).all
        (Ready_Queue.Running_Thread.Aux_Args);

#if M2_TARGET = "stm32f4" then
      pragma Debug (Debug.Assert (False));
#else
      raise Program_Error;  --  No task termination
#end if;
   end Thread_Body_Wrapper;

   ----------------
   -- New_Thread --
   ----------------

   Id_Counter : M2.Kernel.Thread_Id := 1;

   procedure New_Thread (Th_Ac : TCBs.TCB_Ac;
                         Th_Body_Ac : HAL.Code_Address;
                         Priority : M2.Kernel.Priority;
                         Args : System.Address) is
   begin
      pragma Debug (Debug.Put ("|RQ.NT", Integer (Id_Counter)));

      TCBs.Initialize_TCB (Th_Ac        => Th_Ac,
                           Id           => Id_Counter,
                           Priority     => Priority,
                           Body_Ac      => Th_Body_Ac,
                           Args         => Args);

      Id_Counter := Id_Counter + 1;

      if RQ.Running_Ac = null then
         --  The first created thread executes the body of the main procedure
         --  regardless its priority.

         RQ.Running_Ac := Th_Ac;
      end if;

      Ready_Queue.Enqueue_New_Thread (Th_Ac);
   end New_Thread;

   -----------------------------
   -- Activate_Blocked_Thread --
   -----------------------------

   procedure Activate_Blocked_Thread (Th_Ac : TCBs.TCB_Ac) is
   begin
      pragma Debug (Debug.Put ("|SCH.ABT", Integer (Th_Ac.Id)));
      pragma Assert (Th_Ac.Activation_Time = TCBs.Blocked_Activation_Time);

      Th_Ac.Activation_Time := HAL.Get_HWTime;
      HAL.Events_Order_Counter := HAL.Events_Order_Counter + 1;
      Th_Ac.Order_Counter := HAL.Events_Order_Counter;
   end Activate_Blocked_Thread;

   --------------------------
   -- Block_Running_Thread --
   --------------------------

   procedure Block_Running_Thread is
   begin
      pragma Debug (Debug.Put ("|SCH.BRT", Integer (RQ.Running_Ac.Id)));
      pragma Assert (RQ.Running_Ac.Activation_Time <= HAL.Get_HWTime);

      RQ.Running_Ac.Activation_Time := TCBs.Blocked_Activation_Time;

      Dispatcher.Dispatch;
   end Block_Running_Thread;

   ----------------------------------------------
   -- Running_Thread_Ends_Job_Without_Blocking --
   ----------------------------------------------

   procedure Running_Thread_Ends_Job_Without_Blocking is
   begin
      pragma Debug (Debug.Put ("|SCH.RTEJNB", Integer (RQ.Running_Ac.Id)));
      pragma Assert (RQ.Running_Ac.Activation_Time <= HAL.Get_HWTime);

      RQ.Running_Ac.Activation_Time := HAL.Get_HWTime;
      HAL.Events_Order_Counter := HAL.Events_Order_Counter + 1;
      RQ.Running_Ac.Order_Counter := HAL.Events_Order_Counter;

      Dispatcher.Dispatch;
   end Running_Thread_Ends_Job_Without_Blocking;

   ----------------------------
   -- Suspend_Running_Thread --
   ----------------------------

   procedure Suspend_Running_Thread (To_Abs_Time : HAL.HWTime) is
   begin
      pragma Debug (Debug.Put ("|SCH.RTSusp", Integer (RQ.Running_Ac.Id)));
      pragma Assert (RQ.Running_Ac.Activation_Time <= HAL.Get_HWTime);

      if To_Abs_Time < HAL.Get_HWTime then
         RQ.Running_Ac.Activation_Time := HAL.Get_HWTime;
      else
         RQ.Running_Ac.Activation_Time := To_Abs_Time;
      end if;
      HAL.Events_Order_Counter := HAL.Events_Order_Counter + 1;
      RQ.Running_Ac.Order_Counter := HAL.Events_Order_Counter;

      Dispatcher.Dispatch;
   end Suspend_Running_Thread;

end M2.Kernel.Scheduler;
