----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Uses a Passive Infrared (PIR) Motion Sensor (HC-SR501) connected to the
--  'Pin_PIR' digital pin.
--
--  An internal task is used to count the number of motion detections (the
--  output of the PIR sensor goes high when a movement is detected)
--
--     -----------------                 -----------------------
--     |               |                 |                     |
--     |            VCC|-----------------|5V                   |
--     | HC-SR501   OUT|-----------------|Pin_PIR  Arduino Uno |
--     |            GND|-----------------|GND                  |
--     |               |                 |                     |
--     -----------------                 -----------------------

with System;
with Ada.Real_Time;

with Arduino;

generic
   Pin_PIR : Arduino.Digital_Pin;
   Task_Prio : System.Priority;
   Polling_Period : Ada.Real_Time.Time_Span;
package Arduino.PIR is

   function Get_Num_Of_Detections return Natural;

   function Get_Num_Of_Detections_And_Reset return Natural;

   procedure Reset;

end Arduino.PIR;
