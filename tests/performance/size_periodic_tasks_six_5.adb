pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

--with M2.Direct_IO;
with M2.HAL;

package body Size_Periodic_Tasks_Six_5 is
   --package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   ----------------
   -- Other_Task --
   ----------------

   task body Other_Task is
      Next_Time : RT.Time;
      Period : constant RT.Time_Span := RT.Milliseconds (Period_In_Ms*5);
   begin
      Next_Time := Ada.Real_Time.Clock;

      loop
         M2.HAL.Enable_Interrupts;

         --DIO.Put ("-Other4-");
         Loop_Counter := Loop_Counter + 1;

         Next_Time := Next_Time + Period;
         delay until Next_Time;
      end loop;
   end Other_Task;

end Size_Periodic_Tasks_Six_5;
