/****************************************************************************
*                                  M2OS
*
*                           Copyright (C) 2019
*                    Universidad de Cantabria, SPAIN
*
*  Author: David Garcia Villaescusa (garciavd@unican.es)
*
*  This file is part of M2OS.
*
*  M2OS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  M2OS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/

#include "e_lib.h"

#define TARGET_ADDR 0x7550

void sync_timers (int timer_c) {
  int* sync_targets[15];
  
  // SET ADDRESS
  sync_targets[0] = (int*)0x80907550;
  sync_targets[1] = (int*)0x80A07550;
  sync_targets[2] = (int*)0x80B07550;
  sync_targets[3] = (int*)0x84807550;
  sync_targets[4] = (int*)0x84907550;
  sync_targets[5] = (int*)0x84A07550;
  sync_targets[6] = (int*)0x84B07550;
  sync_targets[7] = (int*)0x88807550;
  sync_targets[8] = (int*)0x88907550;
  sync_targets[9] = (int*)0x88A07550;
  sync_targets[10] = (int*)0x88B07550;
  sync_targets[11] = (int*)0x8C807550;
  sync_targets[12] = (int*)0x8C907550;
  sync_targets[13] = (int*)0x8CA07550;
  sync_targets[14] = (int*)0x8CB07550;

  // SEND MESSAGE
  *sync_targets[0] = timer_c-1-3;
  *sync_targets[1] = timer_c-2-5;
  *sync_targets[2] = timer_c-3-6;
  *sync_targets[3] = timer_c-4-3;
  *sync_targets[4] = timer_c-5-5;
  *sync_targets[5] = timer_c-6-6;
  *sync_targets[6] = timer_c-7-8;
  *sync_targets[7] = timer_c-8-5;
  *sync_targets[8] = timer_c-9-6;
  *sync_targets[9] = timer_c-10-8;
  *sync_targets[10] = timer_c-11-9;
  *sync_targets[11] = timer_c-12-6;
  *sync_targets[12] = timer_c-13-8;
  *sync_targets[13] = timer_c-14-9;
   *sync_targets[14] = timer_c-15-11;
}
