pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with stdint_h;
with stddef_h;
with Interfaces.C.Strings;
with System;
limited with WString_h;
limited with Printable_h;

package Print_h is

   DEC : constant := 10;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:29
   HEX : constant := 16;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:30
   OCT : constant := 8;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:31

   BIN : constant := 2;  --  /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:35

   package Class_Print is
      type Print is abstract tagged limited record
         write_error : aliased int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:40
      end record;
      pragma Import (CPP, Print);

      function printNumber
        (this : access Print'Class;
         arg2 : unsigned_long;
         arg3 : stdint_h.uint8_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:41
      pragma Import (CPP, printNumber, "_ZN5Print11printNumberEmh");

      function printFloat
        (this : access Print'Class;
         arg2 : double;
         arg3 : stdint_h.uint8_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:42
      pragma Import (CPP, printFloat, "_ZN5Print10printFloatEdh");

      procedure setWriteError (this : access Print'Class; err : int);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:44
      pragma Import (CPP, setWriteError, "_ZN5Print13setWriteErrorEi");

      function New_Print return Print is abstract;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:46
      pragma CPP_Constructor (New_Print, "_ZN5PrintC1Ev");

      function getWriteError (this : access Print'Class) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:48
      pragma Import (CPP, getWriteError, "_ZN5Print13getWriteErrorEv");

      procedure clearWriteError (this : access Print'Class);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:49
      pragma Import (CPP, clearWriteError, "_ZN5Print15clearWriteErrorEv");

      function write (this : access Print; arg2 : stdint_h.uint8_t) return stddef_h.size_t is abstract;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:51

      function write (this : access Print'Class; str : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:52
      pragma Import (CPP, write, "_ZN5Print5writeEPKc");

      function write
        (this : access Print;
         buffer : access stdint_h.uint8_t;
         size : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:56
      pragma Import (CPP, write, "_ZN5Print5writeEPKhj");

      function write
        (this : access Print'Class;
         buffer : Interfaces.C.Strings.chars_ptr;
         size : stddef_h.size_t) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:57
      pragma Import (CPP, write, "_ZN5Print5writeEPKcj");

      function availableForWrite (this : access Print) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:63
      pragma Import (CPP, availableForWrite, "_ZN5Print17availableForWriteEv");

      function print (this : access Print'Class; arg2 : System.Address) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:65
      pragma Import (CPP, print, "_ZN5Print5printEPK19__FlashStringHelper");

      function print (this : access Print'Class; arg2 : access constant WString_h.Class_String.String) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:66
      pragma Import (CPP, print, "_ZN5Print5printERK6String");

      function print (this : access Print'Class; arg2 : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:67
      pragma Import (CPP, print, "_ZN5Print5printEPKc");

      function print (this : access Print'Class; arg2 : char) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:68
      pragma Import (CPP, print, "_ZN5Print5printEc");

      function print
        (this : access Print'Class;
         arg2 : unsigned_char;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:69
      pragma Import (CPP, print, "_ZN5Print5printEhi");

      function print
        (this : access Print'Class;
         arg2 : int;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:70
      pragma Import (CPP, print, "_ZN5Print5printEii");

      function print
        (this : access Print'Class;
         arg2 : unsigned;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:71
      pragma Import (CPP, print, "_ZN5Print5printEji");

      function print
        (this : access Print'Class;
         arg2 : long;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:72
      pragma Import (CPP, print, "_ZN5Print5printEli");

      function print
        (this : access Print'Class;
         arg2 : unsigned_long;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:73
      pragma Import (CPP, print, "_ZN5Print5printEmi");

      function print
        (this : access Print'Class;
         arg2 : double;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:74
      pragma Import (CPP, print, "_ZN5Print5printEdi");

      function print (this : access Print'Class; arg2 : access constant Printable_h.Class_Printable.Printable'Class) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:75
      pragma Import (CPP, print, "_ZN5Print5printERK9Printable");

      function println (this : access Print'Class; arg2 : System.Address) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:77
      pragma Import (CPP, println, "_ZN5Print7printlnEPK19__FlashStringHelper");

      function println (this : access Print'Class; s : access constant WString_h.Class_String.String) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:78
      pragma Import (CPP, println, "_ZN5Print7printlnERK6String");

      function println (this : access Print'Class; arg2 : Interfaces.C.Strings.chars_ptr) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:79
      pragma Import (CPP, println, "_ZN5Print7printlnEPKc");

      function println (this : access Print'Class; arg2 : char) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:80
      pragma Import (CPP, println, "_ZN5Print7printlnEc");

      function println
        (this : access Print'Class;
         arg2 : unsigned_char;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:81
      pragma Import (CPP, println, "_ZN5Print7printlnEhi");

      function println
        (this : access Print'Class;
         arg2 : int;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:82
      pragma Import (CPP, println, "_ZN5Print7printlnEii");

      function println
        (this : access Print'Class;
         arg2 : unsigned;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:83
      pragma Import (CPP, println, "_ZN5Print7printlnEji");

      function println
        (this : access Print'Class;
         arg2 : long;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:84
      pragma Import (CPP, println, "_ZN5Print7printlnEli");

      function println
        (this : access Print'Class;
         arg2 : unsigned_long;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:85
      pragma Import (CPP, println, "_ZN5Print7printlnEmi");

      function println
        (this : access Print'Class;
         arg2 : double;
         arg3 : int) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:86
      pragma Import (CPP, println, "_ZN5Print7printlnEdi");

      function println (this : access Print'Class; arg2 : access constant Printable_h.Class_Printable.Printable'Class) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:87
      pragma Import (CPP, println, "_ZN5Print7printlnERK9Printable");

      function println (this : access Print'Class) return stddef_h.size_t;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:88
      pragma Import (CPP, println, "_ZN5Print7printlnEv");

      procedure flush (this : access Print);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/Print.h:90
      pragma Import (CPP, flush, "_ZN5Print5flushEv");
   end;
   use Class_Print;
end Print_h;
