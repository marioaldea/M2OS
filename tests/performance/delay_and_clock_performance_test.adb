pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with Ada.Real_Time;
with M2.Direct_IO;

with Measurements;

procedure Delay_And_Clock_Performance_Test  is
   package DIO renames M2.Direct_IO;

   Loop_Counter : Integer := 0;

   Delay_In_Milliseconds : constant := 800;

   use type Ada.Real_Time.Time;
   Next_Time : Ada.Real_Time.Time;
   Period : constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (Delay_In_Milliseconds);

begin
   DIO.Put ("-Delay_And_Clock_Performance_Test-");

   Measurements.Init (Measurements.Long);
   Measurements.Start_Measurement;

   Next_Time := Ada.Real_Time.Clock;
   loop
      DIO.Put ("-Main-");

      if Loop_Counter = 1 then
         Measurements.End_Measurement;
         Measurements.Finish;
      end if;

      Loop_Counter := Loop_Counter + 1;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end loop;

end Delay_And_Clock_Performance_Test;
