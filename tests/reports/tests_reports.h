//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
#ifndef	_M2_TESTS_REPORTS_H_
#define _M2_TESTS_REPORTS_H_
#include <stdint.h>
#include <stdio.h>
#include <time.h>

// Convenience function to print timespecs.
// Implemented in 'm2_stdio.c'.
extern void putts(struct timespec ts);

extern void tests_reports__test_ok();

static inline void tests_reports__assert_equal_c(int32_t val1, int32_t val2,
						 int32_t margin) {
  if ((val1 > val2 && val1 - val2 > margin) ||
      (val2 > val1 && val2 - val1 > margin)) {
      puts("ASSERT FAILED\n");
      m2__end_execution__end_execution(-1);
    }
}

static inline void tests_reports__eat(struct timespec ts) {
  const hwtime_t hwt_end = TS2HWT(ts) + m2__kernel__api__get_time();
  while (hwt_end > m2__kernel__api__get_time());
}


#define tests_reports__assert(b) \
  if (!(b)) { \
    puts("ASSERT FAILED\n"); \
    puts(__FILE__":"); \
    putint(__LINE__); \
    puts("\n"); \
    m2__end_execution__end_execution(-1); \
  }

/*
 * Low resolution measurements.
 */
// Defined in 'measurements_lores.ads'
#if defined(M2_TARGET_Arduino_Uno)
typedef uint32_t lores_time_t;
typedef uint32_t lores_time_long_t;
typedef uint32_t lores_time_units_t; // ms
#define NS_PER_LORES_TIME_UNIT 1000000
#elif defined(M2_TARGET_stm32f4) || defined(M2_TARGET_microbit) || defined(M2_TARGET_riscv)
typedef uint64_t lores_time_t;       // 100 us/tick
typedef uint32_t lores_time_long_t;  // Store up to 5 days
typedef uint32_t lores_time_units_t; // us (store up to 1.2 hours)
#define NS_PER_LORES_TIME_UNIT 1000
#else
#  error Unexpected architecture
#endif
extern void measurements_lores__init();
extern void measurements_lores__reset();
extern void measurements_lores__start_measurement();
extern void measurements_lores__end_measurement();
extern lores_time_long_t measurements_lores__measurement_time();
extern lores_time_t measurements_lores__measurement_avg();
extern lores_time_units_t measurements_lores__to_time_units(lores_time_long_t t);
extern void measurements_lores__print_measures_data();

/*
 * High resolution measurements.
 */
// Defined in 'measurements_hires.ads'
#if defined(M2_TARGET_Arduino_Uno)
typedef uint8_t hires_time_t;
typedef uint32_t hires_time_long_t;
typedef uint32_t hires_time_units_t; // us
#define NS_PER_HIRES_TIME_UNIT 1000
#elif defined(M2_TARGET_stm32f4) || defined(M2_TARGET_microbit) || defined(M2_TARGET_riscv)
typedef uint32_t hires_time_t;  // 0..16799  (100 us)
typedef uint32_t hires_time_long_t; // Store up to 25.77 s
typedef uint64_t hires_time_units_t; // ns
#define NS_PER_HIRES_TIME_UNIT 1
#else
#  error Unexpected architecture
#endif
extern void measurements_hires__init();
extern void measurements_hires__reset();
extern void measurements_hires__start_measurement();
extern void measurements_hires__end_measurement();
extern lores_time_long_t measurements_hires__measurement_time();
extern hires_time_t measurements_hires__measurement_avg();
extern hires_time_units_t measurements_hires__measurement_avg_in_time_units();
extern hires_time_units_t measurements_hires__to_time_units(hires_time_long_t t);
extern void measurements_hires__print_measures_data();
extern void measurements_hires__finish();

#endif // _M2_TESTS_REPORTS_H_
