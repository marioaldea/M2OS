----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Time measurements with the high resolution clock provided by the HAL 
--  (HAL.Get_HiResTime).
with Interfaces;
with M2.HAL;

with Measurements_Generic;

package Measurements_HiRes is new
   Measurements_Generic (Time_T_Hz   => M2.HAL.HiResTime_Hz,
                         Time_T      => M2.HAL.HiResTime,
                         Time_Long_T => Interfaces.Unsigned_32,
                         Get_Time    => M2.HAL.Get_HiResTime,
#if M2_TARGET = "arduino_uno" then
                         Upward         => True,
                         Time_Units_T   => Interfaces.Unsigned_32,
                         Time_Unit_T_Hz => 1_000_000,
                         Time_Unit_Char => 'u');
   
#elsif M2_TARGET = "stm32f4" or M2_TARGET = "microbit" or M2_TARGET = "epiphany" or M2_TARGET = "avr_iot" then
                         Upward         => False,
                         Time_Units_T   => Interfaces.Unsigned_64,
                         Time_Unit_T_Hz => 1_000_000_000,
                         Time_Unit_Char => 'n'); 
   
#else
   pragma Compile_Time_Error (True, "Unexpected architecture");
#end if;
