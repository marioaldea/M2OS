----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
package Eat_CPU_Time is
   
   procedure Eat_Long (D : Duration);
   --  Eats the time with the resolution of the system clock and the
   --  granularity of Duration (20ms in Arduino Uno)
   
   procedure Eat_Milliseconds (MS : Natural);
   --  Eats the time with the resolution of the system clock
   
   procedure Eat_Microseconds (US : Natural);
   --  Eats time doing an active loop is supported in the architecture.
   --  It can exist a maximum value. For example, in Arduino architectue this
   --  limit is 16383us.
   
end Eat_CPU_Time;
