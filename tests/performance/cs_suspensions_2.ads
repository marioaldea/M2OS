pragma Warnings (Off);
with M2.Kernel.Scheduler;
pragma Elaborate_All (M2.Kernel.Scheduler);
pragma Warnings (On);

with Ada.Synchronous_Task_Control;

package CS_Suspensions_2 is

   Num_Of_Loops : constant := 20;

   type Suspension_Primitives is (Delay_Until_Past_Time,
                                  --Delay_Until,
                                  Suspension_Object,
                                  Yield,
                                  PO_Entry,
                                  PO_Entry_Param);
   Suspension_Primitive : Suspension_Primitives :=
     Suspension_Primitives'First;

   Loop_Counter : Integer := 0;

   SO : Ada.Synchronous_Task_Control.Suspension_Object;

   protected PO is
      entry Wait;

      procedure Open;

   private
      Is_Open : Boolean := False;
   end PO;

   protected PO_Param is
      entry Wait (Data_Out : access Integer);

      procedure Open;

   private
      Is_Open : Boolean := False;
      Counter : Integer := 0;
   end PO_Param;

   task Other_Task;

end CS_Suspensions_2;
