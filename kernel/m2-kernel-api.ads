----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with M2.Kernel.Initialization;
pragma Elaborate_All (M2.Kernel.Initialization);

with Interfaces.C;
with Ada.Synchronous_Task_Control;

with M2.Kernel.TCBs;

with M2.Kernel.Scheduler;
with M2.Kernel.Ready_Queue;
with M2.Kernel.Timing_Events;
with M2.HAL;
with M2.End_Execution;
with M2.Kernel.CPU_Time;

----------------------
-- MaRTE.Kernel.API --
----------------------

package M2.Kernel.API is
   use type Interfaces.Unsigned_8;

   package TCBs renames M2.Kernel.TCBs;

   subtype Thread_T is TCBs.TCB;

   subtype Thread_Ac is TCBs.TCB_Ac;

   subtype Time is HAL.HWTime;

   ----------
   -- Self --
   ----------

   function Self return Thread_Ac renames Ready_Queue.Running_Thread;

   -------------------
   -- Create_Thread --
   -------------------

   procedure Create_Thread (Th_Ac : Thread_Ac;
                            Th_Body_Ac : HAL.Code_Address;
                            Priority : M2.Kernel.Priority;
                            Args : System.Address)
                            renames Scheduler.New_Thread;
--     pragma Export (C, Create_Thread, "m2__kernel__api__create_thread");
--     pragma Export_Procedure
--       (Internal        => Create_Thread,
--        External        => "m2__kernel__api__create_thread",
--        Parameter_Types => (TCBs.TCB_Ac,
--                            Scheduler.Thread_Init_Body_Ac,
--                            M2.Kernel.Priority,
--                            System.Address),
--        Mechanism       => (Th_Ac    => Reference,
--                            Th_Body  => Value,
--                            Priority => Value,
--                            Args     => Value));

   ----------------------------
   -- Suspend_Running_Thread --
   ----------------------------

   procedure Suspend_Running_Thread (Until_Abs_Time : Time) renames
     Scheduler.Suspend_Running_Thread;

   --------------------------
   -- Block_Running_Thread --
   --------------------------

   procedure Block_Running_Thread renames
     Scheduler.Block_Running_Thread;

   -----------------------------------------------------
   -- Running_Thread_Ends_Activation_Without_Blocking --
   -----------------------------------------------------

   procedure Running_Thread_Ends_Job_Without_Blocking renames
     Scheduler.Running_Thread_Ends_Job_Without_Blocking;

   -----------------------------
   -- Activate_Blocked_Thread --
   -----------------------------

   procedure Activate_Blocked_Thread (Th_Ac : Thread_Ac) renames
     Scheduler.Activate_Blocked_Thread;

   --------------
   -- Get_Time --
   --------------

   function Get_Time return Time;
   --  function Get_Time return Time renames HAL.Get_HWTime;
   --  To avoid error:
   --  "sorry, unimplemented: nested function trampolines not supported on
   --  This target"

   --------------------------------
   -- Set_Body_Of_Running_Thread --
   --------------------------------

   procedure Set_Job_Body_Of_Running_Thread (Body_Ac : HAL.Code_Address);
   pragma Export (C, Set_Job_Body_Of_Running_Thread,
                  "m2__kernel__api__set_job_body_of_running_thread");
   pragma Export_Procedure (Set_Job_Body_Of_Running_Thread,
                            "m2__kernel__api__set_job_body_of_running_thread",
                            (HAL.Code_Address),
                            Mechanism => Value);

   ---------------------
   -- Yield_To_Higher --
   ---------------------

   procedure Yield_To_Higher
     renames M2.Kernel.Scheduler.Yield_To_Higher;

   -------------------
   -- End_Execution --
   -------------------

   procedure End_Execution  (Status : Interfaces.C.Int) renames
     M2.End_Execution.End_Execution;

   ---------
   -- Eat --
   ---------

   procedure Eat (Eat_Time : Duration);

   -------------------
   -- Timing Events --
   -------------------

   subtype Timing_Event is Timing_Events.Timing_Event;
   subtype Timing_Event_Ac is Timing_Events.Timing_Event_Ac;
   subtype Timing_Event_Handler is Timing_Events.Timing_Event_Handler;

   procedure Set_Handler
     (Event_Ac : Timing_Event_Ac;
      At_Time : Time;
      Handler : Timing_Event_Handler)
      renames Timing_Events.Set_Handler;

   --  Not allowed in Ravenscar (pragma Restrictions No_Relative_Delay)
--     procedure Set_Handler_Relative
--       (Event_Ac : Timing_Event_Ac;
--        In_Time : Time;
--        Handler : Timing_Event_Handler)
--        renames Timing_Events.Set_Handler_Relative;

   function Current_Handler (Event : Timing_Event)
                             return Timing_Event_Handler
                             renames Timing_Events.Current_Handler;

   subtype Boolean_C is Timing_Events.Boolean_C;

   procedure Cancel_Handler (Event_Ac  : Timing_Event_Ac;
                             Cancelled : out Boolean_C)
                             renames Timing_Events.Cancel_Handler;

   function Time_Of_Event (Event : Timing_Event) return Time
                           renames Timing_Events.Time_Of_Event;

   --------------------
   -- Execution Time --
   --------------------

   subtype CPU_Time is Kernel.CPU_Time.CPU_Time;

   function Thread_CPU_Time (Th_Ac : Thread_Ac) return CPU_Time
                             renames M2.Kernel.CPU_Time.Thread_CPU_Time;

   function Idle_CPU_Time return CPU_Time
     renames M2.Kernel.CPU_Time.Idle_CPU_Time;

   ----------------------
   -- Stack management --
   ----------------------

   subtype Stack_Address is HAL.Stack_Address;
   -- 32 bytes (ARM) / 16 bytes (Arduino Uno)

   function Global_Stack_Base return Stack_Address;

   function Current_Stack_Top return Stack_Address;

   ------------------------
   -- Suspension Objects --
   ------------------------

   SO_Size_In_Bytes : constant Interfaces.Unsigned_8 :=
     Ada.Synchronous_Task_Control.Suspension_Object'Size / 8;

end M2.Kernel.API;
