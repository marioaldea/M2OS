--  Measure the context switch when using a delay until with a
--  time in the past.

pragma Ravenscar;
pragma Task_Dispatching_Policy (FIFO_Within_Priorities);
pragma Locking_Policy(Ceiling_Locking);
pragma Queuing_Policy(Priority_Queuing);

with CS_Delay_Past_Time_1;
with CS_Delay_Past_Time_2;

with M2.Direct_IO;
with Measurements_LoRes;

procedure CS_Delay_Past_Time_Test  is
   package DIO renames M2.Direct_IO;
   package Measurements renames Measurements_LoRes;

begin
   DIO.Put (" CS_Delay_Past_Time_Test ");

   Measurements.Init;

end CS_Delay_Past_Time_Test;
