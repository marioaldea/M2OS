----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of evs_ev3ultrasoonic.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with EVShieldUART_h;
with SHDefines_h;
with Interfaces.C.Extensions;
with stdint_h;
with Arduino.EVShield; use Arduino.EVShield;

package Arduino.EVShield.EV3Ultrasonic is
   generic
      Bp : SHDefines_h.SH_BankPort;
     package Ultrasonic is
      -- MODE_Sonar, Modes supported by EV3 Sonar (Ultrasonic Sensor).
      subtype MODE_Sonar is char;
      MODE_Sonar_CM : constant MODE_Sonar := '0';  -- Choose for measurements in centimeters
      MODE_Sonar_Inches : constant MODE_Sonar := '1'; -- Choose measurements in inches
      MODE_Sonar_Presence : constant MODE_Sonar := '2';  -- Choose to Listen for other ultrasonic devices

      -- get the distance to obstacle (in cm or inches based on the mode.)
      -- use setMode() to change the mode as you need
      function Get_Dist return float;

      -- detect other ultrasonic devices
      function Detect return stdint_h.uint8_t;

      function Set_Mode ( New_Mode : MODE_Sonar ) return stdint_h.uint8_t;
   end Ultrasonic;


end Arduino.EVShield.EV3Ultrasonic;
