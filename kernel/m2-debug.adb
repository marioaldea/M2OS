----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with M2.Direct_IO;
with M2.End_Execution;
with Interfaces.C;

package body M2.Debug is

   use type Interfaces.C.Int;

   ------------
   -- Assert --
   ------------

   procedure Assert (Assertion : Boolean) is
   begin
      if not Assertion then
         Direct_IO.Put ("Assert fail");
         Direct_IO.New_Line;
         End_Execution.End_Execution (-2);
      end if;
   end Assert;

   ------------
   -- Assert --
   ------------

--     procedure Assert (Assertion : Boolean;
--                       Msg       : String := GNAT.Source_Info.Source_Location) is
--     begin
--        if not Assertion then
--           Direct_IO.Put ("Assert fail:");
--           Direct_IO.Put (Msg);
--           Direct_IO.New_Line;
--           End_Execution.End_Execution (-2);
--        end if;
--     end Assert;

   ---------
   -- Put --
   ---------

   procedure Put (Msg : String; Task_Id : Integer;
                  Level : M2.Trace_Levels := M2.Trace) is
   begin
      if not M2.Trace_Enabled (Level) then
         return;
      end if;

      Direct_IO.Put (Msg);
      Direct_IO.Put (Task_Id);
      Direct_IO.New_Line;
      --  Direct_IO.Put (" ");
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (Msg_1 : String; Task_Id_1 : Integer;
                  Msg_2 : String; Task_Id_2 : Integer;
                  Level : M2.Trace_Levels := M2.Trace)is
   begin
      if not M2.Trace_Enabled (Level) then
         return;
      end if;

      Direct_IO.Put (Msg_1);
      Direct_IO.Put (Task_Id_1);
      Direct_IO.Put (Msg_2);
      Direct_IO.Put (Task_Id_2);
      Direct_IO.New_Line;
      --  Direct_IO.Put (" ");
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (Msg : String;
                  Level : M2.Trace_Levels := M2.Trace) is
   begin
      if not M2.Trace_Enabled (Level) then
         return;
      end if;

      Direct_IO.Put (Msg);
      --  Direct_IO.Put (" ");
   end Put;

end M2.Debug;
