

#include "nrf.h"
#include "system_nrf51.h"

void m2_hal_init_timer (void)
{
  SystemInit();
  //Update SystemCoreClock variable according to Clock Register Values.
  SystemCoreClockUpdate();
  SysTick_Config(SystemCoreClock / 100U); // seconds

/*   SysTick->CTRL = 0; // Disable SysTick */
/*   SysTick->LOAD = 999; // Count down from 999 to 0 */
/*   SysTick->VAL = 0; // Clear current value to 0 */
/*   SysTick->CTRL = 0x7; // Enable SysTick enable SysTick // exception and use processor clock */
}
