----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Authors: David Garcia Villaescusa (garcia@unican.es)
--           Mario Aldea Rivas (aldeam@unican.es))
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

--  Epiphany version of this package

pragma Restrictions (No_Elaboration_Code);

with System;
with Interfaces;
with Ada.Unchecked_Conversion;

package M2.HAL is
   pragma Preelaborate;

   type Procedure_Ac is access procedure;

   subtype Thread_Context is Procedure_Ac;

   Events_Order_Counter : Interfaces.Unsigned_8 := 0;
   --  To keep FIFO order when threads have equal Activation_Time.
   --  Set to 0 in the timer handler.

   ---------------------
   -- Integer_Address --
   ---------------------

   subtype Integer_Address is System.Storage_Elements.Integer_Address;
   --  Shortcut for this extesively used type
   function To_Address (Value : Integer_Address) return System.Address
     renames System.Storage_Elements.To_Address;
   function To_Integer (Value : System.Address) return Integer_Address
     renames System.Storage_Elements.To_Integer;
   pragma Compile_Time_Error (Integer_Address'Size /= 32,
                             "Integer_Address'Size /= 32");

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch (New_Context : Thread_Context;
                             New_Stack : System.Address);

   --------------------------
   -- Push_Function_Status --
   --------------------------

   procedure Push_Function_Status;
   pragma Import (Asm, Push_Function_Status, "m2_hal_regs_push_status");
   --  defined in m2_hal_regs.S

   -------------------------
   -- Pop_Function_Status --
   -------------------------

   procedure Pop_Function_Status;
   pragma Import (Asm, Pop_Function_Status, "m2_hal_regs_pop_status");
   --  defined in m2_hal_regs.S

   -----------------------
   -- Current_Stack_Top --
   -----------------------

   function Current_Stack_Top return Integer_Address;
   pragma Import (Asm, Current_Stack_Top, "m2_hal_regs_get_sp_reg");
   --  From m2_hal_regs.S

   -----------------------
   -- Global_Stack_Base --
   -----------------------

   function Global_Stack_Base return System.Address with Inline_Always;

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

   procedure Set_Global_Stack_Limit;

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Natural;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Natural;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Natural;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Natural;

   -------------------------------
   -- Enable/disable interrupts --
   -------------------------------

   procedure Enable_Interrupts;
   pragma Inline_Always (Enable_Interrupts);

   procedure Disable_Interrupts;
   pragma Inline_Always (Disable_Interrupts);

   function Are_Interrupts_Enabled return Boolean;

   ------------
   -- HWTime --
   ------------

   HWTime_Hz : constant := 1_000;
   --  Should have the same value than OSI.Ticks_Per_Second

   type HWTime is new Interfaces.Unsigned_64;

   ----------------
   -- Get_HWTime --
   ----------------

   function Get_HWTime return HWTime;

   ------------------------
   -- OS_Tick_Handler_Ac --
   ------------------------

   type OS_Tick_Handler_Ac is access procedure (Now : HWTime);

   --------------------
   -- Initialization --
   --------------------

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac);

end M2.HAL;
