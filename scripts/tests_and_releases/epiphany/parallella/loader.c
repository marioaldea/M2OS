/****************************************************************************
*                                  M2OS
*
*                           Copyright (C) 2019
*                    Universidad de Cantabria, SPAIN
*
*  Authors: David Garcia Villaescusa (garciavd@unican.es)
*           Mario Aldea Rivas (aldeam@unican.es)
*
*  This file is part of M2OS.
*
*  M2OS is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  M2OS is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <e-hal.h>
#include <errno.h>

#define CONSOLE_SIZE 256
#define CONSOLE_ADDR 0x7500
#define N_CORES 16
#define INIT_CORE 0

int main()
{
  int i,j;
  char buf[CONSOLE_SIZE*sizeof(char)];
  
  //System initialization
  e_init(NULL);
  e_reset_system();

  //Workgroup definition
  e_epiphany_t dev;
  e_open(&dev, 0, 0, 4, 4);
  e_reset_group(&dev);

  char code_name[14] = "e_core_0x0.elf";

  //Loading the executables
  for (i = INIT_CORE; i < N_CORES; i++) {
    code_name[7] = i/4+'0'; // ASCII
    code_name[9] = i%4+'0'; // ASCII 
    if ( E_OK != e_load(code_name, &dev, i/4, i%4, E_FALSE) ) {
      fprintf(stderr, "Failed to load %s\n", code_name);
      return EXIT_FAILURE;
    }
  }

  //Starting the execution
  for (i = INIT_CORE; i < N_CORES; i++) {
    e_start (&dev, i/4, i%4);
  }

  //Waiting for the tests execution
  usleep(1000000);

  //Console reading
  for (i = INIT_CORE; i < N_CORES; i++) {
    e_read(&dev, i/4, i%4, CONSOLE_ADDR, &buf, sizeof(buf));
    printf("Core ***** %dx%d *****\n", i/4, i%4); 
    printf("%s\n", buf);
    printf("------------------------\n");
  }

  //End execution
  e_close(&dev);
  e_finalize();

  return 0;
}
