----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  pragma Restrictions (No_Elaboration_Code);

with M2.Kernel.TCBs;
with M2.Kernel.Scheduler;

package M2.Kernel.Ready_Queue is
   --  pragma No_Elaboration_Code_All;
   --  pragma Preelaborate;

   --------------------
   -- Running thread --
   --------------------

   Running_Ac : TCBs.TCB_Ac := null;

   function Running_Thread return TCBs.TCB_Ac is (Running_Ac);
   pragma Export (C, Running_Thread, "m2_self");

   procedure Enqueue_New_Thread (Th_Ac : TCBs.TCB_Ac);

   function Head_Of_Ready_Queue return TCBs.TCB_Ac;

   procedure Show (DBG_Level : M2.Trace_Levels);

end M2.Kernel.Ready_Queue;
