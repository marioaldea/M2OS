----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2020
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of Grove-16x2 LCD library
--  https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/
----------------------------------------------------------------------------

package body Arduino.LCD_I2C.Advanced is

   -----------------------------
   --  C++ imported functions --
   -----------------------------

   procedure Auto_Scroll (This : access LCD);
   pragma Import (CPP, Auto_Scroll, "_ZN7rgb_lcd10autoscrollEv");

   procedure No_Auto_Scroll (This : access LCD);
   pragma Import (CPP, No_Auto_Scroll, "_ZN7rgb_lcd12noAutoscrollEv");

   procedure Print_Double
     (This : access LCD;
      Num : Interfaces.C.Double;
      Digital_Palces: Integer := 2);
   pragma Import (CPP, Print_Double, "_ZN5Print5printEdi");

   -----------------
   -- Auto_Scroll --
   -----------------

   procedure Auto_Scroll is
   begin
      Auto_Scroll (Screen'Access);
   end Auto_Scroll;

   --------------------
   -- No_Auto_Scroll --
   --------------------

   procedure No_Auto_Scroll is
   begin
      No_Auto_Scroll (Screen'Access);
   end No_Auto_Scroll;

   ------------------
   -- Print_Double --
   ------------------

   procedure Print_Double (Num : Interfaces.C.double;
                           Digital_Places: Integer := 2) is
   begin
      Print_Double (Screen'Access, Num, Digital_Places);
   end Print_Double;

end Arduino.Lcd_I2c.Advanced;
