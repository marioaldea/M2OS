----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Task that control the robot based on the color measures.
----------------------------------------------------------------------------

pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Interfaces.C;

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Arduino.EVShield.EV3Touch;
with Arduino.EVShield.Motors;
with SHDefines_H; use SHDefines_H;
with Interfaces.C.Extensions;
with Stdint_H;
with EVShield_Light_Measure_Task; use EVShield_Light_Measure_Task;
with Ada.Real_Time;


package body EVShield_Light_Control_Task is
   package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;

   use type RT.Time_Span;

   ----------------
   -- Other_Task --
   ----------------
   Period : constant RT.Time_Span := RT.Milliseconds (1000);

   Next_Time : RT.Time;
   Loop_Count : Natural := 0;
   -- Program variables
   Count : Interfaces.C.Int;
--     Aux : Stdint_H.Uint8_T;
   Aux2 : Interfaces.C.Extensions.Bool;

   package Touch_Sensor is new Arduino.EVShield.EV3Touch.Touch (SHDefines_H.SH_BAS1);
   -- Bank A Motor 1
   package Motor_Right is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM2);
   -- Bank A Motor 2
   package Motor_Left is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM1);

   use type Interfaces.C.Int;
   use type Interfaces.C.Unsigned_Char;

   procedure Other_Task_Init is
   begin

      Next_Time := Ada.Real_Time.Clock;

      DIO.Put_Line ("-----------------------------");
      DIO.Put_Line ("Starting EVShield light demo.");
      DIO.Put_Line ("-----------------------------");

      -- reset motors.
      Aux2 := Motor_Right.Motor_Reset;
      Aux2 := Motor_Left.Motor_Reset;

   end Other_Task_Init;


   procedure Other_Task_Body is
   begin
      Count := Touch_Sensor.Get_Bump_Count;
      DIO.Put ("Luz por la izquierda: "); DIO.Put (Value_Left); DIO.New_Line;
      DIO.Put ("Luz por la derecha: "); DIO.Put_Line (Value_Right'Img);

      if Count mod 2 = 1 then
         if Value_Right >= 45 then
            DIO.Put_Line ("Luz por la derecha --> Girar a la derecha.");
            DIO.Put ("Luz por la derecha: "); DIO.Put_Line (Value_Right'Img);

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,15);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse, 50);
         elsif Value_Left >= 35.0 then
            DIO.Put_Line ("Luz por la izquierda --> Girar a la izquierda.");
            DIO.Put ("Luz por la izquierda: "); DIO.Put (Value_Left); DIO.New_Line;

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,50);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,15);
         elsif Value_Right >= 30 and Value_Right < 45 and Value_Left >= 10.0 and Value_Left < 35.0 then
            DIO.Put_Line ("Luz por ambos sentidos --> sigue recto.");
            DIO.Put ("Luz por la izquierda: "); DIO.Put (Value_Left); DIO.New_Line;
            DIO.Put ("Luz por la derecha: "); DIO.Put_Line (Value_Right'Img);

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,30);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,30);
         else
            DIO.Put_Line ("No hay luz --> Para.");

            Aux2 := Motor_Right.Motor_Stop(Motor_Right.SH_Next_Action_Brake);
            Aux2 := Motor_Left.Motor_Stop(Motor_Left.SH_Next_Action_Brake);
         end if;
      else
         Aux2 := Motor_Right.Motor_Stop(Motor_Right.SH_Next_Action_Brake);
         Aux2 := Motor_Left.Motor_Stop(Motor_Left.SH_Next_Action_Brake);
         DIO.Put_Line ("Demo is STOP. Press touch sensor to start.");
         -- wait for some time
         Arduino.C_Delay (1000);
      end if;

      Next_Time := Next_Time + Period;
      delay until Next_Time;
   end Other_Task_Body;

   Other_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => Other_Task_Init'Access,
      Body_Ac  => Other_Task_Body'Access,
      Priority => 4);

end EVShield_Light_Control_Task;
