----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--  Modified by: Mario Vicente Garcia (mvg424@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  RP2040 version of this package

with System;

with System.Storage_Elements;

with Ada.Unchecked_Conversion;
with System.Machine_Code; use System.Machine_Code;

with M2.Debug;
with M2.Direct_IO;

package body M2.HAL is
   package DBG renames M2.Debug;

   type UInt32 is new Interfaces.Unsigned_32; --NEW
   function To_UInt32 is new Ada.Unchecked_Conversion (System.Address, UInt32); --NEW
   type UInt7 is mod 2**7 with Size => 7; --NEW
   function To_Address is new Ada.Unchecked_Conversion (UInt32, System.Address); --NEW

   use type Interfaces.Unsigned_32;
   use type System.Address;

   Initialized : Boolean := False;

   Stack_Base_Linker_Script : Interfaces.Unsigned_32;
   pragma Import (C, Stack_Base_Linker_Script, "__stack_end");

   Stack_Base : Interfaces.Unsigned_32;
   pragma Volatile (Stack_Base);

   ----------------
   -- Get_SP_Reg --
   ----------------

   function Get_SP_Reg return Interfaces.Unsigned_32;
   pragma Import (Asm, Get_SP_Reg, "m2_hal_regs_get_sp_reg");
   --  From m2_hal_regs.S

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch
     (New_Context : Thread_Context)
   is
      procedure Jump_To_Context (Funct : Thread_Context;
                                 Stack_Top : Interfaces.Unsigned_32);
      pragma Import (C, Jump_To_Context, "m2_hal_regs_jump_to_context");
      --  From m2_hal_regs.S

   begin
      pragma Debug (Debug.Assert (New_Context /= null));
      pragma Debug (Debug.Assert (Stack_Base /= 0));

      --  Jump to the task body address (stored in New_Context)

      Jump_To_Context (New_Context, Stack_Base);

      --        Asm ("ldr sp, [%0]" & ASCII.LF & ASCII.HT &
      --               "ldr pc, [%1]",
      --             Inputs => (System.Address'Asm_Input ("r", Stack_Base'Address),
      --                        System.Address'Asm_Input ("r", New_Context'Address)),
      --             Volatile => True);

      --  Never returns

      pragma Debug (Debug.Assert (False));
   end Context_Switch;

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

   procedure Set_Global_Stack_Limit is
   begin
      Stack_Base := Get_SP_Reg;
   end Set_Global_Stack_Limit;

   --     procedure Set_Global_Stack_Limit is
   --        use System.Storage_Elements;
   --     begin
   --  --      pragma Debug (Debug.Assert (Stack_Base = 0, ""));
   --
   --        Asm ("str sp, [%0]",
   --             Inputs => System.Address'Asm_Input ("r", Stack_Base'Address),
   --             Volatile => True);
   --  --      Stack_Base := Stack_Base - 4;
   --     end Set_Global_Stack_Limit;

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Natural is
   begin
      pragma Compile_Time_Warning
        (False, "Stack management functions not implemented");
      return 1000;
   end Get_Stack_Max_Size_In_Bytes;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Natural is
   begin
      return 100;
   end Get_Current_Stack_Size_In_Bytes;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Natural is
   begin
      return 900;
   end Get_Current_Stack_Margin_In_Bytes;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area is
   begin
      null;
   end Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Natural is
   begin
      return 234;
   end Get_Max_Stack_Usage_In_Bytes;

   -----------------------
   -- Enable_Interrupts -- Checked
   -----------------------

   procedure Enable_Interrupts is
   begin
      --  System.Machine_Code.Asm ("cpsie i", Volatile => True);

      System.Machine_Code.Asm ("cpsie i"   & ASCII.LF & ASCII.HT
                               & "dsb"     & ASCII.LF & ASCII.HT
                               & "isb",
                               Clobber => "memory", Volatile => True);
   end Enable_Interrupts;

   ------------------------
   -- Disable_Interrupts -- Checked
   ------------------------

   procedure Disable_Interrupts is
   begin
      System.Machine_Code.Asm ("cpsid i", Volatile => True);
   end Disable_Interrupts;

   ----------------------------
   -- Are_Interrupts_Enabled --
   ----------------------------

   function Are_Interrupts_Enabled return Boolean is
      ICSR : Interfaces.Unsigned_32 with Volatile,
        Address => System.Storage_Elements.To_Address (16#E000_ED04#);
      ICSR_VECTACTIVE_Mask : constant := 16#FF#;
      Sys_Tick_Vector : constant := 15;
      PRIMASK_Reg : Interfaces.Unsigned_32;
   begin
      System.Machine_Code.Asm
        ("mrs %0, PRIMASK",
         Outputs => Interfaces.Unsigned_32'Asm_Output ("=r", PRIMASK_Reg),
         Volatile => True);
      return PRIMASK_Reg = 0 and
        (ICSR and ICSR_VECTACTIVE_Mask) /= Sys_Tick_Vector;
   end Are_Interrupts_Enabled;

   ----------
   -- Time --
   ----------

   Now : HWTime := 0; --  Current time

   ---------------------
   -- SysTick_Handler --
   ---------------------

   OS_Tick_Handler : OS_Tick_Handler_Ac;

   --  Handler for the timer interrupt.

   procedure Sys_Tick_Handler;
   pragma Export (C, Sys_Tick_Handler, "__gnat_irq_trap_rtc0");

   procedure Sys_Tick_Handler is
   begin
      Now := Now + 1;

      HAL.Events_Order_Counter := 0;

      if M2.Use_Timing_Events'First then
         OS_Tick_Handler.all (Now);
      end if;
   end Sys_Tick_Handler;

   ----------------
   -- Get_HWTime --
   ----------------

   function Get_HWTime return HWTime is
   begin
      pragma Debug (DBG.Assert (Initialized));

      return Now;
   end Get_HWTime;

   --------------------
   -- Initialize_CPU --
   --------------------

   --  Initialize vector table (in Cortex-M4) and faults
   --  Taken from Initialize_CPU (s-bbcppr.adb)

   procedure Initialize_CPU;

   procedure Initialize_CPU is

   begin
      pragma Compile_Time_Warning (True, "Initialize_CPU");

      null;
   end Initialize_CPU;

   ----------------------
   -- Initialize_Board --
   ----------------------

   --  Initialize and start timer.
   --  Taken from s-bbbosu.adb (microbit) and microbit-time.adb (ADL)

   procedure Initialize_Board;

   procedure Initialize_Board is

   begin
      pragma Compile_Time_Warning (True, "Initialize_Board");

      null;
   end Initialize_Board;

   --------------------
   -- Initialization --
   --------------------

   --  Part of the code is based on Initialize_CPU (s-bbcppr.adb)

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac)is
      procedure M2_HAL_Init_Timer (Period_In_Ticks : Interfaces.Unsigned_32);
      pragma Import (C, M2_HAL_Init_Timer, "m2_hal_init_timer");
      --  From m2_hal_init_timer.c

      function To_Unsigned_32 is
        new Ada.Unchecked_Conversion (System.Address, Interfaces.Unsigned_32);

   begin
      pragma Debug
        (Debug.Assert (System.Address'Size = Interfaces.Unsigned_32'Size));

      --  Initialize vector table (in Cortex-M0) and faults
      Initialize_CPU;

      HAL.Disable_Interrupts;

      Initialize_Board; --  Initialize and start timer (Microbit)

      HAL.OS_Tick_Handler := OS_Tick_Handler;

      Stack_Base := To_Unsigned_32 (Stack_Base_Linker_Script'Address);

      Initialized := True;
   end Initialization;

end M2.HAL;
