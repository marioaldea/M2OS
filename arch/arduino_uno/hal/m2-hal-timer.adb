----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Interfaces;

package body M2.HAL.Timer is

   use type Interfaces.Unsigned_8;

   ----------------------------------
   -- ATmega328p Timer 0 registers --
   ----------------------------------

   -- Timer/Counter Control Register A
   TCCR0A : Interfaces.Unsigned_8;
   for TCCR0A'Address use 16#44#;
   pragma Volatile (TCCR0A);

   -- Timer/Counter Control Register B
   TCCR0B : Interfaces.Unsigned_8;
   for TCCR0B'Address use 16#45#;
   pragma Volatile (TCCR0B);

   -- Timer/Counter Register
   TCNT0 : HAL.HiResTime;
   for TCNT0'Address use 16#46#;
   pragma Volatile (TCNT0);

   --  Output Compare Register A
   OCR0A : Interfaces.Unsigned_8;
   for OCR0A'Address use 16#47#;
   pragma Volatile (OCR0A);

   --  Timer/Counter Interrupt Mask Register
   TIMSK0 : Interfaces.Unsigned_8;
   for TIMSK0'Address use 16#6E#;
   pragma Volatile (TIMSK0);

   -------------------
   -- Timer_Handler --
   -------------------

   OS_Tick_Handler : HAL.OS_Tick_Handler_Ac;

   procedure Tick;
   pragma Machine_Attribute (Entity         => Tick,
                             Attribute_Name => "signal");
   pragma Export (C, Tick, "__vector_14");

   procedure Tick is
   begin
      Now := Now + 1;

      HAL.Events_Order_Counter := 0;

      if M2.Use_Timing_Events'First then
         OS_Tick_Handler.all (Now);
      end if;
   end Tick;



   -----------------
   -- Get_Counter --
   -----------------

   function Get_Counter return HAL.HiResTime is
   begin
      return TCNT0;
   end Get_Counter;

   ---------------
   -- Initilize --
   ---------------

   --  Timer 0 configuration for 1ms periodic interrupt:
   --    Core freq 16 MHz => Period = 0.000_000_062_5 s
   --    Prescaler = 64
   --    End of count (TOP): Prescaler * Period * 250 = 0.001 s = 1 ms

   procedure Initialize (OS_Tick_Handler : HAL.OS_Tick_Handler_Ac) is
   begin
      Timer.OS_Tick_Handler := OS_Tick_Handler;

      --  Set timer mode to  "Clear Timer on Compare" (CTC)
      --  Use OCR0A as "TOP" counter value

      TCCR0A := 2#0000_0010#;

      -- Set end of count (TOP) value

      OCR0A := 249;

      --  Enable Output Compare Match A Interrupt

      TIMSK0 := TIMSK0 or 2#0000_0010#;

      --  Set prescaler to 64

      TCCR0B := TCCR0B or 2#0000_0011#;

      --  Reset counter

      TCNT0 := 0;
   end Initialize;

end M2.HAL.Timer;
