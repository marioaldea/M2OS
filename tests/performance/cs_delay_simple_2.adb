pragma Warnings (Off);
with System.OS_Interface;
pragma Elaborate_All (System.OS_Interface);
pragma Warnings (On);

with Ada.Real_Time;

with M2.Direct_IO;
with M2.HAL;

with Measurements;

package body CS_Delay_Simple_2 is
   package DIO renames M2.Direct_IO;

   ----------------
   -- Other_Task --
   ----------------

   task body Other_Task is
   begin
      loop
         M2.HAL.Enable_Interrupts;

         --DIO.Put ("-Other-");

         Measurements.End_Measurement;

         Measurements.Assert (not Other_Task_Executed);

         Other_Task_Executed := True;

         delay until Ada.Real_Time.Time_First;
      end loop;
   end Other_Task;

end CS_Delay_Simple_2;
