----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2023
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--  Modified by: Mario Vicente Garcia (mvg424@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Initialize_CPU has been taken from GNAT file s-bbcppr.adb               --
--        Copyright (C) 1999-2002 Universidad Politecnica de Madrid         --
--             Copyright (C) 2003-2005 The European Space Agency            --
--                     Copyright (C) 2003-2017, AdaCore                     --
--                                                                          --
-- GNARL is free software; you can  redistribute it  and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 3,  or (at your option) any later ver- --
-- sion. GNARL is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.                                     --
----------------------------------------------------------------------------
--  ARM version of this package

with System;

pragma Warnings (Off);
with System.BB.Parameters;
with System.BB.Board_Parameters;
pragma Warnings (On);
with System.Storage_Elements;

--  Microbit
-- Comment this two lines in case inicialice board is commented
with Interfaces.NRF51.CLOCK;
with Interfaces.NRF51.RTC;

with Ada.Unchecked_Conversion;
with System.Machine_Code; use System.Machine_Code;

with M2.Debug;

package body M2.HAL is

   package DBG renames M2.Debug;
   package BBOPA renames System.BB.Board_Parameters;
   type UInt32 is new Interfaces.Unsigned_32; --NEW
   function To_UInt32 is new Ada.Unchecked_Conversion (System.Address, UInt32); --NEW
   type UInt7 is mod 2**7 with Size => 7; --NEW
   function To_Address is new Ada.Unchecked_Conversion (UInt32, System.Address); --NEW

   use type Interfaces.Unsigned_32;
   use type System.Address, Integer_Address;

   Initialized : Boolean := False;

   Stack_Base_Linker_Script : Interfaces.Unsigned_32;
   pragma Import (C, Stack_Base_Linker_Script, "__stack_end");

   Stack_Base : Integer_Address;
   pragma Volatile (Stack_Base);

   ----------------
   -- Get_SP_Reg --
   ----------------

   function Get_SP_Reg return Interfaces.Unsigned_32;
   pragma Import (Asm, Get_SP_Reg, "m2_hal_regs_get_sp_reg");
   --  From m2_hal_regs.S

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch (New_Context : Thread_Context;
                             New_Stack : System.Address) is
      procedure Jump_To_Context (Funct : Thread_Context;
                                 Stack_Top : System.Address);
      pragma Import (C, Jump_To_Context, "m2_hal_regs_jump_to_context");
      --  From m2_hal_regs.S

   begin
      pragma Debug (Debug.Assert (New_Context /= null));
      pragma Debug (Debug.Assert (To_Integer (New_Stack) /= 0));

      --  Jump to the task body address (stored in New_Context)

      Jump_To_Context (New_Context, New_Stack);

--        Asm ("ldr sp, [%0]" & ASCII.LF & ASCII.HT &
--               "ldr pc, [%1]",
--             Inputs => (System.Address'Asm_Input ("r", Stack_Base'Address),
--                        System.Address'Asm_Input ("r", New_Context'Address)),
--             Volatile => True);

      --  Never returns

      pragma Debug (Debug.Assert (False));
   end Context_Switch;

   ----------------------------
   -- Set_Global_Stack_Limit --
   ----------------------------

--     procedure Set_Global_Stack_Limit is
--     begin
--        Stack_Base := Get_SP_Reg;
--     end Set_Global_Stack_Limit;

--     procedure Set_Global_Stack_Limit is
--        use System.Storage_Elements;
--     begin
--  --      pragma Debug (Debug.Assert (Stack_Base = 0, ""));
--
--        Asm ("str sp, [%0]",
--             Inputs => System.Address'Asm_Input ("r", Stack_Base'Address),
--             Volatile => True);
--  --      Stack_Base := Stack_Base - 4;
--     end Set_Global_Stack_Limit;

   -----------------------
   -- Global_Stack_Base --
   -----------------------

   function Global_Stack_Base return System.Address is
      (To_Address (Stack_Base));

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Natural is
   begin
      pragma Compile_Time_Warning
        (True, "Stack management functions not implemented");
      return 1000;
   end Get_Stack_Max_Size_In_Bytes;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Natural is
   begin
      return 100;
   end Get_Current_Stack_Size_In_Bytes;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Natural is
   begin
      return 900;
   end Get_Current_Stack_Margin_In_Bytes;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area is
   begin
      null;
   end Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Natural is
   begin
      return 234;
   end Get_Max_Stack_Usage_In_Bytes;

   -----------------------
   -- Enable_Interrupts --
   -----------------------

   procedure Enable_Interrupts is
   begin
      --  System.Machine_Code.Asm ("cpsie i", Volatile => True);

      System.Machine_Code.Asm ("cpsie i"   & ASCII.LF & ASCII.HT
                               & "dsb"     & ASCII.LF & ASCII.HT
                               & "isb",
                               Clobber => "memory", Volatile => True);
   end Enable_Interrupts;

   ------------------------
   -- Disable_Interrupts --
   ------------------------

   procedure Disable_Interrupts is
   begin
      System.Machine_Code.Asm ("cpsid i", Volatile => True);
   end Disable_Interrupts;

   ----------------------------
   -- Are_Interrupts_Enabled --
   ----------------------------

   function Are_Interrupts_Enabled return Boolean is
      ICSR : Interfaces.Unsigned_32 with Volatile,
        Address => System.Storage_Elements.To_Address (16#E000_ED04#);
      ICSR_VECTACTIVE_Mask : constant := 16#FF#;
      Sys_Tick_Vector : constant := 15;
      PRIMASK_Reg : Interfaces.Unsigned_32;
   begin
      System.Machine_Code.Asm
        ("mrs %0, PRIMASK",
         Outputs => Interfaces.Unsigned_32'Asm_Output ("=r", PRIMASK_Reg),
         Volatile => True);
      return PRIMASK_Reg = 0 and
        (ICSR and ICSR_VECTACTIVE_Mask) /= Sys_Tick_Vector;
   end Are_Interrupts_Enabled;

   ----------
   -- Time --
   ----------

   Now : HWTime := 0; --  Current time

   ---------------------
   -- SysTick_Handler --
   ---------------------

   OS_Tick_Handler : OS_Tick_Handler_Ac;

   --  Handler for the timer interrupt.

   procedure Sys_Tick_Handler;
   pragma Export (C, Sys_Tick_Handler, "__gnat_irq_trap_rtc0");

   procedure Sys_Tick_Handler is
      use Interfaces.NRF51.CLOCK, Interfaces.NRF51.RTC;
      Reg : UInt32 with Address => System.Address (RTC0_Periph.EVENTS_COMPARE (0)'Address);
   begin
      RTC0_Periph.TASKS_STOP := 1;
      RTC0_Periph.TASKS_CLEAR := 1;
      RTC0_Periph.TASKS_START := 1;

      Reg := 0;

      Now := Now + 1;
      
      Events_Order_Counter := 0;

      if M2.Use_Timing_Events'First then
         OS_Tick_Handler.all (Now);
      end if;
   end Sys_Tick_Handler;

   ----------------
   -- Get_HWTime --
   ----------------

   function Get_HWTime return HWTime is
   begin
      pragma Debug (DBG.Assert (Initialized));

      return Now;
   end Get_HWTime;

   --------------------
   -- Initialize_CPU --
   --------------------

   --  Initialize vector table (in Cortex-M4) and faults
   --  Taken from Initialize_CPU (s-bbcppr.adb)

   procedure Initialize_CPU;

   procedure Initialize_CPU is
--        Interrupt_Stack_Table : array (System.Multiprocessors.CPU)
--          of System.Address;
--        pragma Import (Asm, Interrupt_Stack_Table, "interrupt_stack_table");
      --  Table containing a pointer to the top of the stack for each processor
      --  M2OS uses the same stack for interrupts and threads

      VTOR : System.Address with Volatile,
        Address => System.Storage_Elements.To_Address (16#E000_ED08#);
      --  Vec. Table Offset

      type Word is mod 2**System.Word_Size;

      AIRCR : Word with Volatile,  -- App Int/Reset Ctrl
        Address => System.Storage_Elements.To_Address (16#E000_ED0C#);
      CCR   : Word with Volatile, -- Config. Control
        Address => System.Storage_Elements.To_Address (16#E000_ED14#);
      SHPR1 : Word with Volatile, -- Sys Hand  4- 7 Prio
        Address => System.Storage_Elements.To_Address (16#E000_ED18#);
      SHPR2 : Word with Volatile, -- Sys Hand  8-11 Prio
        Address => System.Storage_Elements.To_Address (16#E000_ED1C#);
      SHPR3 : Word with Volatile, -- Sys Hand 12-15 Prio
        Address => System.Storage_Elements.To_Address (16#E000_ED20#);
      SHCSR : Word with Volatile, -- Sys Hand Ctrl/State
        Address => System.Storage_Elements.To_Address (16#E000_ED24#);

      System_Vectors : constant System.Address;
      pragma Import (Asm, System_Vectors, "__vectors");
      --  Defined in handler.S

   begin

      --  Not used in M2OS
--        if Has_OS_Extensions then
--           --  Switch the stack pointer to SP_process (PSP)
--
--           Asm ("mrs r0, MSP" & NL &
--                  "msr PSP, r0" & NL &
--                  "mrs r0, CONTROL" & NL &
--                  "movs r1, #2" & NL &
--                  "orr r0,r0,r1" & NL &
--                  "msr CONTROL,r0" & NL &
--                  "mrs r0, CONTROL",
--                Clobber => "r0,r1",
--                Volatile => True);
--
--           --  Initialize SP_main (MSP)
--
--           Asm ("msr MSP, %0",
--                Inputs => Address'Asm_Input ("r", Interrupt_Stack_Table (1)),
--                Volatile => True);
--        end if;

      if System.BB.Parameters.Has_VTOR then
         --  Replaces the initial vector table (set in start-rom.S) by this
         --  other (more complete) table.
         --  Not in Cortex-M0
         VTOR := System_Vectors'Address;
      end if;

      --  Set configuration: stack is 8 byte aligned, trap on divide by 0,
      --  no trap on unaligned access, can enter thread mode from any level.

      CCR := CCR or 16#211#;

      --  Set priorities of system handlers. The Pend_SV handler runs at the
      --  lowest priority, so context switching does not block higher priority
      --  interrupt handlers. All other system handlers run at the highest
      --  priority (0), so they will not be interrupted. This is also true for
      --  the SysTick interrupt, as this interrupt must be serviced promptly in
      --  order to avoid losing track of time.

      SHPR1 := 0;
      SHPR2 := 0;
      SHPR3 := 16#00_FF_00_00#;

      if not System.BB.Parameters.Is_ARMv6m then

         --  Write the required key (16#05FA#) and desired PRIGROUP value. We
         --  configure this to 3, to have 16 group priorities

         --  Not in Cortex-M0

         AIRCR := 16#05FA_0300#;
         pragma Assert (AIRCR = 16#FA05_0300#); --  Key value is swapped
      end if;

      --  Enable usage, bus and memory management fault

      SHCSR := SHCSR or 16#7_0000#;

      --  Call context switch hardware initialization
--        Initialize_Context_Switch;

      --  Unmask Fault

      Asm ("cpsie f", Volatile => True);
   end Initialize_CPU;

     ----------------------
     -- Initialize_Board --
     ----------------------

     --  Initialize and start timer.
     --  Taken from s-bbbosu.adb (microbit) and microbit-time.adb (ADL)

     procedure Initialize_Board;

     procedure Initialize_Board is
        use Interfaces.NRF51.CLOCK, Interfaces.NRF51.RTC;
        use type Interfaces.Nrf51.UInt12;
        use type Interfaces.NRF51.UInt32;
        use type Interfaces.NRF51.UInt24;

        Reg_Addr : constant UInt32 := To_UInt32 (System.Address (RTC0_Periph.EVENTS_COMPARE (0)'Address));
        Device_Base : constant UInt32 := Reg_Addr and 16#FFFF_F000#;
        Event_Index : constant UInt7 := UInt7 (Reg_Addr and 16#0000_007F#) / 4;
        Set_Register_Addr : constant UInt32 := Device_Base + 16#0000_0304#;
        Set_Register : UInt32 with Address => To_Address (Set_Register_Addr);

        NVIC_Base : constant System.Address := System'To_Address (16#E000E100#);
        type NVIC_Peripheral is record
        --  Interrupt Set-Enable Registers
        NVIC_ISER : aliased HAL.UInt32;
        end record
        with Volatile;

        NVIC_Periph : aliased NVIC_Peripheral
        with Import, Address => NVIC_Base;

        Value : constant Interfaces.NRF51.UInt24 := 33;

     begin
        --  Mask interrupts
        Disable_Interrupts;

        -- Timer --

        --  Configure the low frequency clock required for RTC0
        CLOCK_Periph.LFCLKSRC.SRC := Rc; -- Use internal RC oscillator

        --  Start the low frequency clock
        CLOCK_Periph.TASKS_LFCLKSTART := 1;

        --  Wait until the low frequency clock is started
        while CLOCK_Periph.EVENTS_LFCLKSTARTED = 0 loop
           null;
        end loop;

        -- Stop timer
        RTC0_Periph.TASKS_STOP := 1; --NEW

        --  We run the counter at 32.768KHz
        RTC0_Periph.PRESCALER.PRESCALER := 0;
        RTC0_Periph.CC (Integer (0)).COMPARE := Value; --NEW

        -- Enable Events
        RTC0_Periph.EVTEN.COMPARE.Arr (0) := Enabled; --NEW

        -- Enable Interrupt
        Set_Register := 2**Natural (Event_Index); --NEW

        NVIC_Periph.NVIC_ISER := Shift_Left (1, Natural (11)); --NEW

        --  Clear pending timer interrupt if any
        RTC0_Periph.EVENTS_TICK := 0;

        --  Now that the interrupt handler is attached, we can start the timer
        RTC0_Periph.TASKS_START := 1;
     end Initialize_Board;

   --------------------
   -- Initialization --
   --------------------

   --  Part of the code is based on Initialize_CPU (s-bbcppr.adb)

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac)is
      procedure M2_HAL_Init_Timer (Period_In_Ticks : Interfaces.Unsigned_32);
      pragma Import (C, M2_HAL_Init_Timer, "m2_hal_init_timer");
      --  From m2_hal_init_timer.c

      function To_Unsigned_32 is
        new Ada.Unchecked_Conversion (System.Address, Interfaces.Unsigned_32);

   begin
      pragma Debug
        (Debug.Assert (System.Address'Size = Interfaces.Unsigned_32'Size));

       --  Initialize vector table (in Cortex-M0) and faults
      Initialize_CPU;
      HAL.Disable_Interrupts;

      Initialize_Board; --  Initialize and start timer (Microbit)

      HAL.OS_Tick_Handler := OS_Tick_Handler;

      Stack_Base := To_Integer (Stack_Base_Linker_Script'Address);

      Initialized := True;
   end Initialization;

end M2.HAL;
