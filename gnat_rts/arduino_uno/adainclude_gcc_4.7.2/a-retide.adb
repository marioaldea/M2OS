with System.OS_Interface;

package body Ada.Real_Time.Delays is

   -----------------
   -- Delay_Until --
   -----------------

   procedure Delay_Until (T : Time) is
   begin
      --  pragma Detect_Blocking is mandatory in this run time, so that
      --  Program_Error must be raised if this delay (potentially blocking
      --  operation) is called from a protected operation.

      --  << MaRTE
--        if STPO.Self.Common.Protected_Action_Nesting > 0 then
--           raise Program_Error;
--        else
--           STPO.Delay_Until (STPO.Time (T));
--        end if;
      System.OS_Interface.Delay_Until (System.OS_Interface.Time (T));
      --  MaRTE >>
   end Delay_Until;

   -----------------
   -- To_Duration --
   -----------------

   --  This function is not supposed to be used by the Ravenscar run time and
   --  it is not supposed to be with'ed by the user either (because it is an
   --  internal GNAT unit). It is kept here (returning a junk value) just for
   --  sharing the same package specification with the regular run time.

   function To_Duration (T : Time) return Duration is
      pragma Unreferenced (T);
   begin
      return 0.0;
   end To_Duration;

end Ada.Real_Time.Delays;
