----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Binding of evs_ev3color.h library
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with stdint_h;
with Arduino.EVShield;
with EVs_EV3Color_h;


package Arduino.EVShield.EV3Color is
   generic
      Bp : Arduino.EVShield.SH_BankPort;
   package Color is

      subtype MODE_Color is char;
      MODE_Color_ReflectedLight : constant MODE_Color := '0';  -- Choose for measuring reflected light
      MODE_Color_AmbientLight : constant MODE_Color := '1';    -- Choose for measuring ambient light
      MODE_Color_MeasureColor : constant MODE_Color := '2';    -- Choose for measuring color

      -- get the color value
      function Get_Val return float;

      function Set_Mode ( New_Mode : MODE_Color ) return stdint_h.uint8_t;

   end Color;

end Arduino.EVShield.EV3Color;
