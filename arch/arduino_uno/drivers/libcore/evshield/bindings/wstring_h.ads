pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with System;
with Interfaces.C.Strings;

package WString_h is

   --  arg-macro: function F (string_literal)
   --    return reinterpret_cast<const __FlashStringHelper *>(PSTR(string_literal));
   --  skipped empty struct uu_FlashStringHelper

   type String;
   type StringIfHelperType is record
      uu_pfn : System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:50
      uu_delta : aliased int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:50
   end record;
   pragma Convention (C_Pass_By_Copy, StringIfHelperType);
   package Class_String is
      type String is limited record
         buffer : Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:196
         capacity : aliased unsigned;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:197
         len : aliased unsigned;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:198
      end record;
      pragma Import (CPP, String);

      procedure StringIfHelper (this : access constant String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:51
      pragma Import (CPP, StringIfHelper, "_ZNK6String14StringIfHelperEv");

      function New_String (cstr : Interfaces.C.Strings.chars_ptr) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:59
      pragma CPP_Constructor (New_String, "_ZN6StringC1EPKc");

      function New_String (str : System.Address) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:61
      pragma CPP_Constructor (New_String, "_ZN6StringC1EPK19__FlashStringHelper");

      function New_String (rval : System.Address) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:64
      pragma CPP_Constructor (New_String, "_ZN6StringC1EO15StringSumHelper");

      function New_String (c : char) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:66
      pragma CPP_Constructor (New_String, "_ZN6StringC1Ec");

      function New_String (arg1 : unsigned_char; base : unsigned_char) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:67
      pragma CPP_Constructor (New_String, "_ZN6StringC1Ehh");

      function New_String (arg1 : int; base : unsigned_char) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:68
      pragma CPP_Constructor (New_String, "_ZN6StringC1Eih");

      function New_String (arg1 : unsigned; base : unsigned_char) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:69
      pragma CPP_Constructor (New_String, "_ZN6StringC1Ejh");

      function New_String (arg1 : long; base : unsigned_char) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:70
      pragma CPP_Constructor (New_String, "_ZN6StringC1Elh");

      function New_String (arg1 : unsigned_long; base : unsigned_char) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:71
      pragma CPP_Constructor (New_String, "_ZN6StringC1Emh");

      function New_String (arg1 : float; decimalPlaces : unsigned_char) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:72
      pragma CPP_Constructor (New_String, "_ZN6StringC1Efh");

      function New_String (arg1 : double; decimalPlaces : unsigned_char) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:73
      pragma CPP_Constructor (New_String, "_ZN6StringC1Edh");

      procedure Delete_String (this : access String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:74
      pragma Import (CPP, Delete_String, "_ZN6StringD1Ev");

      function reserve (this : access String; size : unsigned) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:80
      pragma Import (CPP, reserve, "_ZN6String7reserveEj");

      function length (this : access constant String) return unsigned;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:81
      pragma Import (CPP, length, "_ZNK6String6lengthEv");

      function operator_as (this : access String; rhs : access constant String) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:86
      pragma Import (CPP, operator_as, "_ZN6StringaSERKS_");

      function operator_as (this : access String; cstr : Interfaces.C.Strings.chars_ptr) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:87
      pragma Import (CPP, operator_as, "_ZN6StringaSEPKc");

      function operator_as (this : access String; str : System.Address) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:88
      pragma Import (CPP, operator_as, "_ZN6StringaSEPK19__FlashStringHelper");

      function operator_as (this : access String; rval : access String) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:90
      pragma Import (CPP, operator_as, "_ZN6StringaSEOS_");

      function operator_as (this : access String; rval : System.Address) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:91
      pragma Import (CPP, operator_as, "_ZN6StringaSEO15StringSumHelper");

      function concat (this : access String; str : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:99
      pragma Import (CPP, concat, "_ZN6String6concatERKS_");

      function concat (this : access String; cstr : Interfaces.C.Strings.chars_ptr) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:100
      pragma Import (CPP, concat, "_ZN6String6concatEPKc");

      function concat (this : access String; c : char) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:101
      pragma Import (CPP, concat, "_ZN6String6concatEc");

      function concat (this : access String; c : unsigned_char) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:102
      pragma Import (CPP, concat, "_ZN6String6concatEh");

      function concat (this : access String; num : int) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:103
      pragma Import (CPP, concat, "_ZN6String6concatEi");

      function concat (this : access String; num : unsigned) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:104
      pragma Import (CPP, concat, "_ZN6String6concatEj");

      function concat (this : access String; num : long) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:105
      pragma Import (CPP, concat, "_ZN6String6concatEl");

      function concat (this : access String; num : unsigned_long) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:106
      pragma Import (CPP, concat, "_ZN6String6concatEm");

      function concat (this : access String; num : float) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:107
      pragma Import (CPP, concat, "_ZN6String6concatEf");

      function concat (this : access String; num : double) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:108
      pragma Import (CPP, concat, "_ZN6String6concatEd");

      function concat (this : access String; str : System.Address) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:109
      pragma Import (CPP, concat, "_ZN6String6concatEPK19__FlashStringHelper");

      function operator_pa (this : access String; rhs : access constant String) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:113
      pragma Import (CPP, operator_pa, "_ZN6StringpLERKS_");

      function operator_pa (this : access String; cstr : Interfaces.C.Strings.chars_ptr) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:114
      pragma Import (CPP, operator_pa, "_ZN6StringpLEPKc");

      function operator_pa (this : access String; c : char) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:115
      pragma Import (CPP, operator_pa, "_ZN6StringpLEc");

      function operator_pa (this : access String; num : unsigned_char) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:116
      pragma Import (CPP, operator_pa, "_ZN6StringpLEh");

      function operator_pa (this : access String; num : int) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:117
      pragma Import (CPP, operator_pa, "_ZN6StringpLEi");

      function operator_pa (this : access String; num : unsigned) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:118
      pragma Import (CPP, operator_pa, "_ZN6StringpLEj");

      function operator_pa (this : access String; num : long) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:119
      pragma Import (CPP, operator_pa, "_ZN6StringpLEl");

      function operator_pa (this : access String; num : unsigned_long) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:120
      pragma Import (CPP, operator_pa, "_ZN6StringpLEm");

      function operator_pa (this : access String; num : float) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:121
      pragma Import (CPP, operator_pa, "_ZN6StringpLEf");

      function operator_pa (this : access String; num : double) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:122
      pragma Import (CPP, operator_pa, "_ZN6StringpLEd");

      function operator_pa (this : access String; str : System.Address) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:123
      pragma Import (CPP, operator_pa, "_ZN6StringpLEPK19__FlashStringHelper");

      function operator_1 (this : access constant String) return StringIfHelperType;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:138
      pragma Import (CPP, operator_1, "_ZNK6StringcvMS_KFvvEEv");

      function compareTo (this : access constant String; s : access constant String) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:139
      pragma Import (CPP, compareTo, "_ZNK6String9compareToERKS_");

      function equals (this : access constant String; s : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:140
      pragma Import (CPP, equals, "_ZNK6String6equalsERKS_");

      function equals (this : access constant String; cstr : Interfaces.C.Strings.chars_ptr) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:141
      pragma Import (CPP, equals, "_ZNK6String6equalsEPKc");

      function operator_eq (this : access constant String; rhs : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:142
      pragma Import (CPP, operator_eq, "_ZNK6StringeqERKS_");

      function operator_eq (this : access constant String; cstr : Interfaces.C.Strings.chars_ptr) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:143
      pragma Import (CPP, operator_eq, "_ZNK6StringeqEPKc");

      function operator_ne (this : access constant String; rhs : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:144
      pragma Import (CPP, operator_ne, "_ZNK6StringneERKS_");

      function operator_ne (this : access constant String; cstr : Interfaces.C.Strings.chars_ptr) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:145
      pragma Import (CPP, operator_ne, "_ZNK6StringneEPKc");

      function operator_lt (this : access constant String; rhs : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:146
      pragma Import (CPP, operator_lt, "_ZNK6StringltERKS_");

      function operator_gt (this : access constant String; rhs : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:147
      pragma Import (CPP, operator_gt, "_ZNK6StringgtERKS_");

      function operator_le (this : access constant String; rhs : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:148
      pragma Import (CPP, operator_le, "_ZNK6StringleERKS_");

      function operator_ge (this : access constant String; rhs : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:149
      pragma Import (CPP, operator_ge, "_ZNK6StringgeERKS_");

      function equalsIgnoreCase (this : access constant String; s : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:150
      pragma Import (CPP, equalsIgnoreCase, "_ZNK6String16equalsIgnoreCaseERKS_");

      function startsWith (this : access constant String; prefix : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:151
      pragma Import (CPP, startsWith, "_ZNK6String10startsWithERKS_");

      function startsWith
        (this : access constant String;
         prefix : access constant String;
         offset : unsigned) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:152
      pragma Import (CPP, startsWith, "_ZNK6String10startsWithERKS_j");

      function endsWith (this : access constant String; suffix : access constant String) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:153
      pragma Import (CPP, endsWith, "_ZNK6String8endsWithERKS_");

      function charAt (this : access constant String; index : unsigned) return char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:156
      pragma Import (CPP, charAt, "_ZNK6String6charAtEj");

      procedure setCharAt
        (this : access String;
         index : unsigned;
         c : char);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:157
      pragma Import (CPP, setCharAt, "_ZN6String9setCharAtEjc");

      function operator_ob (this : access constant String; index : unsigned) return char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:158
      pragma Import (CPP, operator_ob, "_ZNK6StringixEj");

      function operator_ob (this : access String; index : unsigned) return access char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:159
      pragma Import (CPP, operator_ob, "_ZN6StringixEj");

      procedure getBytes
        (this : access constant String;
         buf : access unsigned_char;
         bufsize : unsigned;
         index : unsigned);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:160
      pragma Import (CPP, getBytes, "_ZNK6String8getBytesEPhjj");

      procedure toCharArray
        (this : access constant String;
         buf : Interfaces.C.Strings.chars_ptr;
         bufsize : unsigned;
         index : unsigned);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:161
      pragma Import (CPP, toCharArray, "_ZNK6String11toCharArrayEPcjj");

      function c_str (this : access constant String) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:163
      pragma Import (CPP, c_str, "_ZNK6String5c_strEv");

      function c_begin (this : access String) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:164
      pragma Import (CPP, c_begin, "_ZN6String5beginEv");

      function c_end (this : access String) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:165
      pragma Import (CPP, c_end, "_ZN6String3endEv");

      function c_begin (this : access constant String) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:166
      pragma Import (CPP, c_begin, "_ZNK6String5beginEv");

      function c_end (this : access constant String) return Interfaces.C.Strings.chars_ptr;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:167
      pragma Import (CPP, c_end, "_ZNK6String3endEv");

      function indexOf (this : access constant String; ch : char) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:170
      pragma Import (CPP, indexOf, "_ZNK6String7indexOfEc");

      function indexOf
        (this : access constant String;
         ch : char;
         fromIndex : unsigned) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:171
      pragma Import (CPP, indexOf, "_ZNK6String7indexOfEcj");

      function indexOf (this : access constant String; str : access constant String) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:172
      pragma Import (CPP, indexOf, "_ZNK6String7indexOfERKS_");

      function indexOf
        (this : access constant String;
         str : access constant String;
         fromIndex : unsigned) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:173
      pragma Import (CPP, indexOf, "_ZNK6String7indexOfERKS_j");

      function lastIndexOf (this : access constant String; ch : char) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:174
      pragma Import (CPP, lastIndexOf, "_ZNK6String11lastIndexOfEc");

      function lastIndexOf
        (this : access constant String;
         ch : char;
         fromIndex : unsigned) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:175
      pragma Import (CPP, lastIndexOf, "_ZNK6String11lastIndexOfEcj");

      function lastIndexOf (this : access constant String; str : access constant String) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:176
      pragma Import (CPP, lastIndexOf, "_ZNK6String11lastIndexOfERKS_");

      function lastIndexOf
        (this : access constant String;
         str : access constant String;
         fromIndex : unsigned) return int;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:177
      pragma Import (CPP, lastIndexOf, "_ZNK6String11lastIndexOfERKS_j");

      function substring (this : access constant String; beginIndex : unsigned) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:178
      pragma Import (CPP, substring, "_ZNK6String9substringEj");

      function substring
        (this : access constant String;
         beginIndex : unsigned;
         endIndex : unsigned) return String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:179
      pragma Import (CPP, substring, "_ZNK6String9substringEjj");

      procedure replace
        (this : access String;
         find : char;
         replace : char);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:182
      pragma Import (CPP, replace, "_ZN6String7replaceEcc");

      procedure replace
        (this : access String;
         find : access constant String;
         replace : access constant String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:183
      pragma Import (CPP, replace, "_ZN6String7replaceERKS_S1_");

      procedure remove (this : access String; index : unsigned);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:184
      pragma Import (CPP, remove, "_ZN6String6removeEj");

      procedure remove
        (this : access String;
         index : unsigned;
         count : unsigned);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:185
      pragma Import (CPP, remove, "_ZN6String6removeEjj");

      procedure toLowerCase (this : access String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:186
      pragma Import (CPP, toLowerCase, "_ZN6String11toLowerCaseEv");

      procedure toUpperCase (this : access String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:187
      pragma Import (CPP, toUpperCase, "_ZN6String11toUpperCaseEv");

      procedure trim (this : access String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:188
      pragma Import (CPP, trim, "_ZN6String4trimEv");

      function toInt (this : access constant String) return long;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:191
      pragma Import (CPP, toInt, "_ZNK6String5toIntEv");

      function toFloat (this : access constant String) return float;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:192
      pragma Import (CPP, toFloat, "_ZNK6String7toFloatEv");

      function toDouble (this : access constant String) return double;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:193
      pragma Import (CPP, toDouble, "_ZNK6String8toDoubleEv");

      procedure init (this : access String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:200
      pragma Import (CPP, init, "_ZN6String4initEv");

      procedure invalidate (this : access String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:201
      pragma Import (CPP, invalidate, "_ZN6String10invalidateEv");

      function changeBuffer (this : access String; maxStrLen : unsigned) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:202
      pragma Import (CPP, changeBuffer, "_ZN6String12changeBufferEj");

      function concat
        (this : access String;
         cstr : Interfaces.C.Strings.chars_ptr;
         length : unsigned) return unsigned_char;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:203
      pragma Import (CPP, concat, "_ZN6String6concatEPKcj");

      function copy
        (this : access String;
         cstr : Interfaces.C.Strings.chars_ptr;
         length : unsigned) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:206
      pragma Import (CPP, copy, "_ZN6String4copyEPKcj");

      function copy
        (this : access String;
         pstr : System.Address;
         length : unsigned) return access String;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:207
      pragma Import (CPP, copy, "_ZN6String4copyEPK19__FlashStringHelperj");

      procedure move (this : access String; rhs : access String);  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:209
      pragma Import (CPP, move, "_ZN6String4moveERS_");
   end;
   use Class_String;
   function operator_p (lhs : System.Address; rhs : access constant String) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:125
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperRK6String");

   function operator_p (lhs : System.Address; cstr : Interfaces.C.Strings.chars_ptr) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:126
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperPKc");

   function operator_p (lhs : System.Address; c : char) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:127
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperc");

   function operator_p (lhs : System.Address; num : unsigned_char) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:128
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperh");

   function operator_p (lhs : System.Address; num : int) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:129
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperi");

   function operator_p (lhs : System.Address; num : unsigned) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:130
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperj");

   function operator_p (lhs : System.Address; num : long) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:131
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperl");

   function operator_p (lhs : System.Address; num : unsigned_long) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:132
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperm");

   function operator_p (lhs : System.Address; num : float) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:133
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperf");

   function operator_p (lhs : System.Address; num : double) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:134
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperd");

   function operator_p (lhs : System.Address; rhs : System.Address) return System.Address;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:135
   pragma Import (CPP, operator_p, "_ZplRK15StringSumHelperPK19__FlashStringHelper");

   package Class_StringSumHelper is
      type StringSumHelper is limited record
         parent : aliased String;
      end record;
      pragma Import (CPP, StringSumHelper);

      function New_StringSumHelper (s : access constant String) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:216
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1ERK6String");

      function New_StringSumHelper (p : Interfaces.C.Strings.chars_ptr) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:217
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1EPKc");

      function New_StringSumHelper (c : char) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:218
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1Ec");

      function New_StringSumHelper (num : unsigned_char) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:219
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1Eh");

      function New_StringSumHelper (num : int) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:220
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1Ei");

      function New_StringSumHelper (num : unsigned) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:221
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1Ej");

      function New_StringSumHelper (num : long) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:222
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1El");

      function New_StringSumHelper (num : unsigned_long) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:223
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1Em");

      function New_StringSumHelper (num : float) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:224
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1Ef");

      function New_StringSumHelper (num : double) return StringSumHelper;  -- /home/daniel/arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:225
      pragma CPP_Constructor (New_StringSumHelper, "_ZN15StringSumHelperC1Ed");
   end;
   use Class_StringSumHelper;
end WString_h;
