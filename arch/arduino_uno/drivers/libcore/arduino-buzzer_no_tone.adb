----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with Ada.Synchronous_Task_Control;

with AdaX_Dispatching_Stack_Sharing;

--  with M2.Direct_IO;
with Arduino;

package body Arduino.Buzzer_No_Tone is
   --  package DIO renames M2.Direct_IO;
   package RT renames Ada.Real_Time;
   package STC renames Ada.Synchronous_Task_Control;

   use type Ada.Real_Time.Time;
   use type Ada.Real_Time.Time_Span;
   use type Arduino.Digital_Pin_Out_Value;

   Suspension_Object : STC.Suspension_Object;

   --  Current tone characteristics

   Pin_Level : Arduino.Digital_Pin_Out_Value := Arduino.Out_High;
   Tone_Semiperiod : RT.Time_Span;
   Tone_Duration : RT.Time_Span;
   Next_Tone_End : RT.Time;
   Tone_Repeat_Period : RT.Time_Span;
   Next_Period_End : RT.Time;
   Buzzer_Off : Boolean := True;

   --  Next activation time

   Next_Time : RT.Time;

   ----------
   -- Tone --
   ----------

   procedure Tone (Frequency_Hz : Positive;
                   Tone_Duration : RT.Time_Span;
                   Repeat_Period : RT.Time_Span := RT.Time_Span_Zero) is
   begin
      pragma Assert (Repeat_Period = RT.Time_Span_Zero or
                       Tone_Duration < Repeat_Period);
      pragma Assert (Tone_Duration > RT.Milliseconds (10));
      pragma Assert (Frequency_Hz >= 1);

      Tone_Semiperiod := RT.Milliseconds (500 / Frequency_Hz);
      Tone_Repeat_Period := Repeat_Period;
      Arduino.Buzzer.Tone_Duration := Tone_Duration;
      Buzzer_Off := False;
      Next_Time := Ada.Real_Time.Clock;
      Next_Tone_End := Next_Time + Tone_Duration;
      Next_Period_End := Next_Time + Tone_Repeat_Period;
      STC.Set_True (Suspension_Object);
   end Tone;

   ------------
   -- Silent --
   ------------

   procedure Silent is
   begin
      Buzzer_Off := True;
      Arduino.Digital_Write (Pin_Buzzer, Arduino.Out_Low);
      STC.Set_False (Suspension_Object);
   end Silent;

   ----------------------
   -- Buzzer_Task_Init --
   ----------------------

   procedure Buzzer_Task_Init is
   begin
      --DIO.Put (" BuzzerInit ");
      Arduino.Pin_Mode (Pin  => Pin_Buzzer,
                        Mode => Arduino.Output);
      STC.Suspend_Until_True (Suspension_Object);
   end Buzzer_Task_Init;

   ----------------------
   -- Buzzer_Task_Body --
   ----------------------

   procedure Buzzer_Task_Body is
   begin
      --DIO.Put (" BuzzerBody ");
      pragma Assert (Next_Tone_End <= Next_Period_End);

      if Buzzer_Off then
         --  Wait until next call to 'Tone'

         STC.Suspend_Until_True (Suspension_Object);

      elsif Next_Time < Next_Tone_End then
         --  Play sound in interval [T0, T0+Tone_Duration]

         Arduino.Digital_Write (Pin_Buzzer, Pin_Level);

         if Pin_Level = Arduino.Out_High then
            Pin_Level := Arduino.Out_Low;
         else
            Pin_Level := Arduino.Out_High;
         end if;

      elsif Next_Time >= Next_Period_End then
         if Tone_Repeat_Period /= RT.Time_Span_Zero then
            --  periodic sound: end of period

            Next_Tone_End := Next_Tone_End + Tone_Repeat_Period;
            Next_Period_End := Next_Period_End + Tone_Repeat_Period;

         else
            --  One shot sound: end of sound

            Buzzer_Off := True;
         end if;
      end if;

      --  Wait for next polling time

      Next_Time := Next_Time + Tone_Semiperiod;
      delay until Next_Time;
   end Buzzer_Task_Body;

   -----------------
   -- Buzzer_Task --
   -----------------

   Buzzer_Task : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac => Buzzer_Task_Init'Unrestricted_Access,
      Body_Ac => Buzzer_Task_Body'Unrestricted_Access,
      Priority  => Task_Prio);

end Arduino.Buzzer_No_Tone;
