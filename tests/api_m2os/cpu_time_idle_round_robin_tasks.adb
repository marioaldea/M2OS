----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2022
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------

with Ada.Real_Time;
with Ada.Task_Identification;
pragma Warnings (Off);
with System.Tasking;
pragma Warnings (On);
with Ada.Unchecked_Conversion;

with M2.Direct_IO;
with Adax_Dispatching_Stack_Sharing;
with M2.Kernel.API;

with Eat_CPU_Time;
with Tests_Reports;
with M2.HAL;

package body CPU_Time_Idle_Round_Robin_Tasks is
   package DIO renames M2.Direct_IO;
   package API renames M2.Kernel.API;
   package HAL renames M2.HAL;

   use type Ada.Real_Time.Time;
   
   Loops_Counter_Limit : constant := 10;
   
   Prio : constant := 3;
   Eat_MS : constant := 20;
   
   Period_MS_A : constant := Eat_MS * 4;
   Period_A: constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (Period_MS_A);
   Eat_MS_A : constant := Eat_MS / 2;
     
   Period_MS_B : constant := Eat_MS * 5;
   Period_B: constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (Period_MS_B);
   Eat_MS_B : constant := Eat_MS / 2;
     
   Period_MS_C : constant := Eat_MS * 6;
   Period_C: constant Ada.Real_Time.Time_Span :=
     Ada.Real_Time.Milliseconds (Period_MS_C);
   Eat_MS_C : constant := Eat_MS;
   
   --  One of the tasks could charge the system initialization time
   
   Init_CPU_Time_A : Integer;
   Init_CPU_Time_B : Integer;   
   Init_CPU_Time_C : Integer;
      
   --------------------------
   -- Task_Id_To_Thread_Ac --
   --------------------------

   function Task_Id_To_Thread_Ac (T_Id : Ada.Task_Identification.Task_Id)
                                  return API.Thread_Ac Is
      function To_STask_Id is
        new Ada.Unchecked_Conversion (Ada.Task_Identification.Task_Id,
                                      System.Tasking.Task_Id);
      pragma Warnings (Off);
      function To_Th_Ac is
        new Ada.Unchecked_Conversion (System.Address,
                                      API.Thread_Ac);
      pragma Warnings (On);
   begin
      return To_Th_Ac (To_STask_Id (T_Id).Thread'Address);
   end Task_Id_To_Thread_Ac;
         
   -----------
   -- TaskA --
   -----------

   Next_Time_A : Ada.Real_Time.Time := Ada.Real_Time.Time_First;
   A_Counter : Natural := 0;

   procedure TaskA_Init is
   begin
      DIO.Put (" -TA P:");
      DIO.Put (Integer (Period_MS_A));
      DIO.Put (" C:");
      DIO.Put (Integer (Eat_MS_A));      
      Init_CPU_Time_A := Integer (API.Thread_CPU_Time (API.Self));
   end TaskA_Init;

   procedure TaskA_Body is
   begin
      --  DIO.Put (" A ");
      Eat_CPU_Time.Eat_Milliseconds (Eat_MS_A);
      A_Counter := A_Counter + 1;

      Next_Time_A := Next_Time_A + Period_A;
      delay until Next_Time_A;
   end TaskA_Body;

   Task_A : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => TaskA_Init'Access,
      Body_Ac  => TaskA_Body'Access,
      Priority => Prio);

   -----------
   -- TaskB --
   -----------

   Next_Time_B : Ada.Real_Time.Time := Ada.Real_Time.Time_First;
   B_Counter : Natural := 0;

   procedure TaskB_Init is
   begin
      DIO.Put (" -TB P:");
      DIO.Put (Integer (Period_MS_B));
      DIO.Put (" C:");
      DIO.Put (Integer (Eat_MS_B));      
      Init_CPU_Time_B := Integer (API.Thread_CPU_Time (API.Self));
   end TaskB_Init;

   procedure TaskB_Body is
   begin
      --  DIO.Put (" B ");
      Eat_CPU_Time.Eat_Milliseconds (Eat_MS_B);  
      B_Counter := B_Counter + 1; 

      Next_Time_B := Next_Time_B + Period_B;
      delay until Next_Time_B;
   end TaskB_Body;

   Task_B : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => TaskB_Init'Access,
      Body_Ac  => TaskB_Body'Access,
      Priority => Prio);

   -----------
   -- TaskC --
   -----------

   Next_Time_C : Ada.Real_Time.Time := Ada.Real_Time.Time_First;
   C_Counter : Natural := 0;

   procedure TaskC_Init is
   begin
      DIO.Put (" -TC P:");
      DIO.Put (Integer (Period_MS_C));
      DIO.Put (" C:");
      DIO.Put (Integer (Eat_MS_C));
      DIO.New_Line;
      
      Init_CPU_Time_A :=
        Integer (API.Thread_CPU_Time (Task_Id_To_Thread_Ac (Task_A'Identity)));       
      Init_CPU_Time_B :=
        Integer (API.Thread_CPU_Time (Task_Id_To_Thread_Ac (Task_B'Identity)));      
      Init_CPU_Time_C := Integer (API.Thread_CPU_Time (API.Self));
      Tests_Reports.Assert (Init_CPU_Time_A = 0 or Init_CPU_Time_B = 0);
   end TaskC_Init;

   procedure TaskC_Body is
      CPU_Time_A : Integer;
      CPU_Time_B : Integer;
      CPU_Time_C : Integer;
      CPU_Idle : Integer;      
      CPU_Time_Total : Integer;
      Now : API.Time;
   begin
      Eat_CPU_Time.Eat_Milliseconds (Eat_MS_C);
      
      CPU_Time_A :=
        Integer (API.Thread_CPU_Time (Task_Id_To_Thread_Ac (Task_A'Identity)));       
      CPU_Time_B :=
        Integer (API.Thread_CPU_Time (Task_Id_To_Thread_Ac (Task_B'Identity)));     
      CPU_Time_C := Integer (API.Thread_CPU_Time (API.Self));
      CPU_Idle := Integer (API.Idle_CPU_Time);
      CPU_Time_Total := CPU_Time_A + CPU_Time_B + CPU_Time_C + CPU_Idle;
      Now := API.Get_Time;
      
      C_Counter := C_Counter + 1;
           
      Tests_Reports.Assert_Equal
        (Init_CPU_Time_A + A_Counter * Eat_MS_A * HAL.HWTime_Hz / 1_000,
         CPU_Time_A, 1 * HAL.HWTime_Hz / 1_000);           
      Tests_Reports.Assert_Equal
        (Init_CPU_Time_B + B_Counter * Eat_MS_B * HAL.HWTime_Hz / 1_000,
         CPU_Time_B, 1 * HAL.HWTime_Hz / 1_000);           
      Tests_Reports.Assert_Equal
        (Init_CPU_Time_C + C_Counter * Eat_MS_C * HAL.HWTime_Hz / 1_000,
         CPU_Time_C, 1 * HAL.HWTime_Hz / 1_000);
      Tests_Reports.Assert_Equal (Integer (Now), CPU_Time_Total, 1);
      if C_Counter = Loops_Counter_Limit then   
         DIO.New_Line;     
         DIO.Put ("CPU time A:");
         DIO.Put (CPU_Time_A);  
         DIO.Put ("  CPU time B:");
         DIO.Put (CPU_Time_B);  
         DIO.Put ("  CPU time C:");
         DIO.Put (CPU_Time_C);    
         DIO.Put ("  CPU Idle:");
         DIO.Put (CPU_Idle); 
         DIO.New_Line;
         DIO.Put ("Now:");
         DIO.Put (Integer (Now));  
         DIO.Put ("  CPU Total:");
         DIO.Put (CPU_Time_Total);  
         DIO.New_Line;
         
         Tests_Reports.Test_OK;
      end if;

      Next_Time_C := Next_Time_C + Period_C;
      delay until Next_Time_C;
   end TaskC_Body;

   Task_C : AdaX_Dispatching_Stack_Sharing.One_Shot_Task
     (Init_Ac  => TaskC_Init'Access,
      Body_Ac  => TaskC_Body'Access,
      Priority => Prio);
   
end CPU_Time_Idle_Round_Robin_Tasks;
