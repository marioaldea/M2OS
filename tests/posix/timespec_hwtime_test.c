//--------------------------------------------------------------------------
//                                  M2OS
//
//                           Copyright (C) 2024
//                    Universidad de Cantabria, SPAIN
//
//  Author: Mario Aldea Rivas (aldeam@unican.es)
//
//  This file is part of M2OS.
//
//  M2OS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  M2OS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
//--------------------------------------------------------------------------
// Test the timespec macros defined in <time.h>.
// The implementation of "struct timespec" used depends on if macro
// M2_USE_POSIX_TIMESPEC is defined or not.
#include <stdio.h>
#include <time.h>
#include "../reports/tests_reports.h"

int main (int argc, char **argv) {
  m2osinit();
  check_posix_api();
  puts("HWTIME_HZ:"); putint(HWTIME_HZ); puts("\n");

  // test assigment and gets
  puts("Test assigment and gets...\n");
  struct timespec ts = TS(35, 234000000);
  putts(ts); puts("\n");
  tests_reports__assert_equal_c(35, TS_SEC(ts), 0);
  tests_reports__assert_equal_c(234000000, TS_NSEC(ts), 0);
  puts("OK\n");

  // test add
  puts("Test TS_ADD...\n");
  {
    struct timespec ts1;
    struct timespec ts2 = TS(6, 511000000);
    struct timespec ts3 = TS(0, 234000000);
    TS_ADD(ts1, ts2, ts3);
    putts(ts1); puts("="); putts(ts2); puts("+"); putts(ts3); puts("\n");
    tests_reports__assert_equal_c(TS_SEC(ts1), 6, 0);
    tests_reports__assert_equal_c(TS_NSEC(ts1), 511000000 + 234000000, 0);

    TS_ADD(ts3, ts2, ts1); // carry one second
    putts(ts3); puts("="); putts(ts2); puts("+"); putts(ts1); puts("\n");
    tests_reports__assert_equal_c(TS_SEC(ts3), 13, 0);
    tests_reports__assert_equal_c(TS_NSEC(ts3),
				  511000000 + 234000000 + 511000000 - 1000000000,
				  0);
  }
  puts("OK\n");

  // test incr
  puts("Test TS_INC...\n");
  {
    struct timespec ts1_old;
    struct timespec ts1 = TS(1, 0);
    struct timespec ts2 = TS(6, 511000000);
    ts1_old = ts1;
    TS_INC(ts1, ts2);
    putts(ts1); puts("="); putts(ts2); puts("+"); putts(ts1_old); puts("\n");
    tests_reports__assert_equal_c(TS_SEC(ts1), 7, 0);
    tests_reports__assert_equal_c(TS_NSEC(ts1), 511000000, 0);

    ts1_old = ts1;
    TS_INC(ts1, ts2); // carry one second
    putts(ts1); puts("="); putts(ts2); puts("+"); putts(ts1_old); puts("\n");
    tests_reports__assert_equal_c(TS_SEC(ts1), 14, 0);
    tests_reports__assert_equal_c(TS_NSEC(ts1),
				  511000000 + 511000000 - 1000000000, 0);
  }
  puts("OK\n");
    
  // test TS2HWT and HWT2TS
  puts("Test TS2HWT and HWT2TS...\n");
  {
    const int sec = 5;
    const int32_t nsec = 765000000;
    hwtime_t hwt = sec * HWTIME_HZ + ((uint64_t) nsec * HWTIME_HZ) / 1000000000;
    struct timespec ts;

    HWT2TS(ts, hwt);
    putts(ts); puts("\n");
    tests_reports__assert_equal_c(TS_SEC(ts), sec, 0);
    tests_reports__assert_equal_c(TS_NSEC(ts), nsec, 0);
    
    hwtime_t hwt1 = TS2HWT(ts);
    tests_reports__assert_equal_c(hwt, hwt1, 0);
  }
  puts("OK\n");

  tests_reports__test_ok();
  return 0;
}
