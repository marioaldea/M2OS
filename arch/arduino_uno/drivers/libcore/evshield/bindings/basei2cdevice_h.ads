pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with stdint_h;
with Interfaces.C.Extensions;
with Interfaces.C.Strings;
with Arduino;


package BaseI2CDevice_h is

   function readIntFromBuffer (buf : access stdint_h.uint8_t) return stdint_h.uint16_t;  -- BaseI2CDevice.h:43
   pragma Import (CPP, readIntFromBuffer, "_Z17readIntFromBufferPh");

   function readLongFromBuffer (buf : access stdint_h.uint8_t) return stdint_h.uint32_t;  -- BaseI2CDevice.h:49
   pragma Import (CPP, readLongFromBuffer, "_Z18readLongFromBufferPh");

   procedure writeByteToBuffer (buf : access stdint_h.uint8_t; data : stdint_h.uint8_t);  -- BaseI2CDevice.h:59
   pragma Import (CPP, writeByteToBuffer, "_Z17writeByteToBufferPhh");

--     procedure writeByteToBuffer (buf : access stdint_h.uint8_t; data : stdint_h.int8_t);  -- BaseI2CDevice.h:64
--     pragma Import (CPP, writeByteToBuffer, "_Z17writeByteToBufferPha");

   procedure writeIntToBuffer (buf : access stdint_h.uint8_t; data : stdint_h.uint16_t);  -- BaseI2CDevice.h:70
   pragma Import (CPP, writeIntToBuffer, "_Z16writeIntToBufferPhj");

--     procedure writeIntToBuffer (buf : access stdint_h.uint8_t; data : stdint_h.int16_t);  -- BaseI2CDevice.h:76
--     pragma Import (CPP, writeIntToBuffer, "_Z16writeIntToBufferPhi");

   procedure writeLongToBuffer (buf : access stdint_h.uint8_t; data : stdint_h.uint32_t);  -- BaseI2CDevice.h:82
   pragma Import (CPP, writeLongToBuffer, "_Z17writeLongToBufferPhm");

--     procedure writeLongToBuffer (buf : access stdint_h.uint8_t; data : stdint_h.int32_t);  -- BaseI2CDevice.h:90
--     pragma Import (CPP, writeLongToBuffer, "_Z17writeLongToBufferPhl");

   package Class_BaseI2CDevice is
      type BaseI2CDevice is limited record
         u_device_address : aliased stdint_h.uint8_t;  -- BaseI2CDevice.h:214
         u_write_error_code : aliased stdint_h.uint8_t;  -- BaseI2CDevice.h:215
      end record;
      pragma Import (CPP, BaseI2CDevice);

      function New_BaseI2CDevice (i2c_address : stdint_h.uint8_t) return BaseI2CDevice;  -- BaseI2CDevice.h:105
      pragma CPP_Constructor (New_BaseI2CDevice, "_ZN13BaseI2CDeviceC1Eh");

      procedure initProtocol (this : access BaseI2CDevice);  -- BaseI2CDevice.h:108
      pragma Import (CPP, initProtocol, "_ZN13BaseI2CDevice12initProtocolEv");

      function readRegisters
        (this : access BaseI2CDevice;
         start_register : stdint_h.uint8_t;
         bytes_to_read : stdint_h.uint8_t;
         buffer : access stdint_h.uint8_t;
         buffer_length : stdint_h.uint8_t;
         clear_buffer : Arduino.Boolean8) return access stdint_h.uint8_t;  -- BaseI2CDevice.h:118
      pragma Import (CPP, readRegisters, "_ZN13BaseI2CDevice13readRegistersEhhPhhb");

      function readByte (this : access BaseI2CDevice; location : stdint_h.uint8_t) return stdint_h.uint8_t;  -- BaseI2CDevice.h:125
      pragma Import (CPP, readByte, "_ZN13BaseI2CDevice8readByteEh");

      function readInteger (this : access BaseI2CDevice; location : stdint_h.uint8_t) return stdint_h.int16_t;  -- BaseI2CDevice.h:131
      pragma Import (CPP, readInteger, "_ZN13BaseI2CDevice11readIntegerEh");

      function readLong (this : access BaseI2CDevice; location : stdint_h.uint8_t) return stdint_h.uint32_t;  -- BaseI2CDevice.h:137
      pragma Import (CPP, readLong, "_ZN13BaseI2CDevice8readLongEh");

      function readString
        (this : access BaseI2CDevice;
         location : stdint_h.uint8_t;
         bytes_to_read : stdint_h.uint8_t;
         buffer : access stdint_h.uint8_t;
         buffer_length : stdint_h.uint8_t) return Interfaces.C.Strings.chars_ptr;  -- BaseI2CDevice.h:146
      pragma Import (CPP, readString, "_ZN13BaseI2CDevice10readStringEhhPhh");

      function writeRegisters
        (this : access BaseI2CDevice;
         start_register : stdint_h.uint8_t;
         bytes_to_write : stdint_h.uint8_t;
         buffer : access stdint_h.uint8_t) return Extensions.bool;  -- BaseI2CDevice.h:155
      pragma Import (CPP, writeRegisters, "_ZN13BaseI2CDevice14writeRegistersEhhPh");

      function writeByte
        (this : access BaseI2CDevice;
         location : stdint_h.uint8_t;
         data : stdint_h.uint8_t) return Extensions.bool;  -- BaseI2CDevice.h:162
      pragma Import (CPP, writeByte, "_ZN13BaseI2CDevice9writeByteEhh");

      function writeInteger
        (this : access BaseI2CDevice;
         location : stdint_h.uint8_t;
         data : stdint_h.uint16_t) return Extensions.bool;  -- BaseI2CDevice.h:168
      pragma Import (CPP, writeInteger, "_ZN13BaseI2CDevice12writeIntegerEhj");

      function writeLong
        (this : access BaseI2CDevice;
         location : stdint_h.uint8_t;
         data : stdint_h.uint32_t) return Extensions.bool;  -- BaseI2CDevice.h:174
      pragma Import (CPP, writeLong, "_ZN13BaseI2CDevice9writeLongEhm");

      function checkAddress (this : access BaseI2CDevice) return Extensions.bool;  -- BaseI2CDevice.h:177
      pragma Import (CPP, checkAddress, "_ZN13BaseI2CDevice12checkAddressEv");

      function setAddress (this : access BaseI2CDevice; i2c_address : stdint_h.uint8_t) return Extensions.bool;  -- BaseI2CDevice.h:182
      pragma Import (CPP, setAddress, "_ZN13BaseI2CDevice10setAddressEh");

      function getAddress (this : access BaseI2CDevice) return stdint_h.uint8_t;  -- BaseI2CDevice.h:185
      pragma Import (CPP, getAddress, "_ZN13BaseI2CDevice10getAddressEv");

      function getWriteErrorCode (this : access BaseI2CDevice) return stdint_h.uint8_t;  -- BaseI2CDevice.h:188
      pragma Import (CPP, getWriteErrorCode, "_ZN13BaseI2CDevice17getWriteErrorCodeEv");

      function getFirmwareVersion (this : access BaseI2CDevice) return Interfaces.C.Strings.chars_ptr;  -- BaseI2CDevice.h:191
      pragma Import (CPP, getFirmwareVersion, "_ZN13BaseI2CDevice18getFirmwareVersionEv");

      function getVendorID (this : access BaseI2CDevice) return Interfaces.C.Strings.chars_ptr;  -- BaseI2CDevice.h:194
      pragma Import (CPP, getVendorID, "_ZN13BaseI2CDevice11getVendorIDEv");

      function getDeviceID (this : access BaseI2CDevice) return Interfaces.C.Strings.chars_ptr;  -- BaseI2CDevice.h:197
      pragma Import (CPP, getDeviceID, "_ZN13BaseI2CDevice11getDeviceIDEv");

      function getFeatureSet (this : access BaseI2CDevice) return Interfaces.C.Strings.chars_ptr;  -- BaseI2CDevice.h:200
      pragma Import (CPP, getFeatureSet, "_ZN13BaseI2CDevice13getFeatureSetEv");

      procedure setWriteErrorCode (this : access BaseI2CDevice; code : stdint_h.uint8_t);  -- BaseI2CDevice.h:211
      pragma Import (CPP, setWriteErrorCode, "_ZN13BaseI2CDevice17setWriteErrorCodeEh");



      b_initialized : aliased Extensions.bool;  -- BaseI2CDevice.h:206
      pragma Import (CPP, b_initialized, "_ZN13BaseI2CDevice13b_initializedE");
   end;
   use Class_BaseI2CDevice;
end BaseI2CDevice_h;
