----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
-- Concurrent version of lines demo
----------------------------------------------------------------------------
-- setup for this example:
-- attach demo robot
----------------------------------------------------------------------------

with Interfaces.C;

with Arduino.Wire;
with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Arduino.EVShield.EV3Touch;
with Arduino.EVShield.EV3Color;
with Arduino.EVShield.Motors;
with Arduino.EVShield.NXTColor;
with SHDefines_H; use SHDefines_H;
with Interfaces.C.Extensions;
with Stdint_H;

procedure EVShield_Demo_Lines_Concurrent is
   package DIO renames M2.Direct_IO;

   -- Program variables
   Aux1 : Stdint_H.Uint8_T;
   Aux2 : Interfaces.C.Extensions.Bool;
   Count : Interfaces.C.Int;

   Color_Left : Float;
   Color_Right : Stdint_H.Uint8_T;

   package Touch_Sensor is new Arduino.EVShield.EV3Touch.Touch (SHDefines_H.SH_BAS2);
   -- Bank A Motor 1
   package Motor_Left is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM2);
   -- Bank A Motor 2
   package Motor_Right is new Arduino.EVShield.Motors.Motor (Arduino.EVShield.Motors.SH_BAM1);
   package Light_Right is new Arduino.EVShield.NXTColor.Color (SHDefines_H.SH_BBS2);
   package Light_Left is new Arduino.EVShield.EV3Color.Color (Arduino.EVShield.SH_BAS1);
   use type Interfaces.C.Int;
   use type Interfaces.C.Unsigned_Char;
begin
   DIO.Put_Line ("-----------------------------");
   DIO.Put_Line ("Starting EVShield light demo.");
   DIO.Put_Line ("-----------------------------");

   Aux1 := Light_Left.Set_Mode(Light_Left.MODE_Color_MeasureColor);
   Aux2 := Light_Right.Set_Color(Light_Right.SH_Type_COLORFULL);

   loop
      Count := Touch_Sensor.Get_Bump_Count;

      if Count mod 2 = 1 then
         -- read the value from color sensors
         Color_Right := Light_Right.Read_Color;
         Color_Left := Light_Left.Get_Val;

         while Color_Right = 4 loop

            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            40);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           0);
            -- wait for some time
            Arduino.C_Delay (200);
            Color_Right := Light_Right.Read_Color;

         end loop;

         while Color_Left = 3.0 loop
            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            0);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           40);
            -- wait for some time
            Arduino.C_Delay (200);
            Color_Left := Light_Left.Get_Val;

         end loop;

         if Color_Left /= 3.0 and Color_Right /= 4 then
            Motor_Right.Motor_Run_Unlimited(Motor_Right.SH_Direction_Reverse,
                                            10);
            Motor_Left.Motor_Run_Unlimited(Motor_Left.SH_Direction_Reverse,
                                           10);
            -- wait for some time
            Arduino.C_Delay (200);
         end if;

      else
         DIO.Put_Line ("Demo is STOP. Press touch sensor to start.");
         Aux2 := Motor_Right.Motor_Stop(Motor_Right.SH_Next_Action_Brake);
         Aux2 := Motor_Left.Motor_Stop(Motor_Left.SH_Next_Action_Brake);
         -- wait for some time
         Arduino.C_Delay (1000);
      end if;



   end loop;

end EVShield_Demo_Lines_Concurrent;
