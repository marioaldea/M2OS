----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Arduino Uno version of this package

with System;
with Ada.Unchecked_Conversion;
with System.Machine_Code; use System.Machine_Code;
with Interfaces;

with M2.HAL.Timer;

with M2.Debug;

package body M2.HAL is

   package DBG renames M2.Debug;
   use type Interfaces.Unsigned_16;
   use type System.Address, Stack_Size;

   Initialized : Boolean := False;

   ---------------------------------
   --  ATmega328 SRAM memory map  --
   ---------------------------------

   Internal_SRAM_Start_Addr    : constant := 16#100#;
   Internal_SRAM_Size_In_Bytes : constant := 16#800#;

   --------------------
   -- Context_Switch --
   --------------------

   procedure Context_Switch (New_Addr  : Code_Address;
                             New_Stack : Stack_Address) is
      procedure Jump_To_Context (Funct : Code_Address;
                                 Stack_Top : Stack_Address);
      pragma Import (C, Jump_To_Context, "marte_hal_jump_to_context");
      --  defined in marte_hal_context_switch.S

   begin
      pragma Debug (Debug.Assert (System.Address (New_Addr) /=
                      System.Null_Address));
      pragma Debug (Debug.Assert (New_Stack /= 0));

      --  Jump to the task body address (stored in New_Context)

      Jump_To_Context (New_Addr, New_Stack);

      --  Never returns

      pragma Debug (Debug.Assert (False));
   end Context_Switch;

   --------------------------
   -- Stack related values --
   --------------------------

   --  Boundaries of the stack defined by the linker script file.
   --  The usable stack area is:
   --     SRAM_End_Addr down to Data_End'Address

   SRAM_End_Addr : constant := Internal_SRAM_Start_Addr +
     Internal_SRAM_Size_In_Bytes - 1;
   --  In Arduino Uno:
   --     SRAM_End_Addr = 0x100 + 0x800 - 1 =
   --                     0x8FF = 2303

   Data_End : Integer;
   pragma Import (C, Data_End, "__bss_end");
   --  end of .data section (defined in linker script).

   Stack_Not_Used_Magic : constant := 16#AB#;

   Stack_Base : constant := SRAM_End_Addr;

   function To_Address (Value : Stack_Size) return System.Address
     renames System.Storage_Elements.To_Address;
   function To_Size (Value : System.Address) return Stack_Size
     renames System.Storage_Elements.To_Integer;
   function To_Size is new Ada.Unchecked_Conversion (Stack_Address, Stack_Size);
   function To_Stack_Address is
     new Ada.Unchecked_Conversion (System.Address, Stack_Address);

   -----------------------
   -- Global_Stack_Base --
   -----------------------

   function Global_Stack_Base return Stack_Address is
      (Stack_Base);

   ---------------------------------
   -- Get_Stack_Max_Size_In_Bytes --
   ---------------------------------

   function Get_Stack_Max_Size_In_Bytes return Stack_Size is
   begin
      return SRAM_End_Addr - To_Size (Data_End'Address) + 1;
   end Get_Stack_Max_Size_In_Bytes;

   -------------------------------------
   -- Get_Current_Stack_Size_In_Bytes --
   -------------------------------------

   function Get_Current_Stack_Size_In_Bytes return Stack_Size is
   begin
      return SRAM_End_Addr - To_Size (Current_Stack_Top) + 1;
   end Get_Current_Stack_Size_In_Bytes;

   ---------------------------------------
   -- Get_Current_Stack_Margin_In_Bytes --
   ---------------------------------------

   function Get_Current_Stack_Margin_In_Bytes return Stack_Size is
   begin
      return To_Size (Current_Stack_Top) - To_Size (Data_End'Address);
   end Get_Current_Stack_Margin_In_Bytes;

   ---------------------
   -- Mark_Stack_Area --
   ---------------------

   procedure Mark_Stack_Area is
      Stack : array (1 .. Get_Stack_Max_Size_In_Bytes)
        of Interfaces.Unsigned_8;
      for Stack'Address use Data_End'Address;
   begin
      for I in Stack_Size range Stack'First ..
        Stack'Last - Get_Current_Stack_Size_In_Bytes loop
         Stack (I) := Stack_Not_Used_Magic;
      end loop;
   end Mark_Stack_Area;

   ----------------------------------
   -- Get_Max_Stack_Usage_In_Bytes --
   ----------------------------------

   function Get_Max_Stack_Usage_In_Bytes return Stack_Size is
      Stack : array (1 .. Get_Stack_Max_Size_In_Bytes)
        of Interfaces.Unsigned_8;
      for Stack'Address use Data_End'Address;
      I : Stack_Size := 1;
      use type Interfaces.Unsigned_8;
   begin
      loop
         exit when Stack (I) /= Stack_Not_Used_Magic;
         I := I + 1;
      end loop;
      return Stack'Last - I + 1;
   end Get_Max_Stack_Usage_In_Bytes;

   ----------------------------
   -- Are_Interrupts_Enabled --
   ----------------------------

   function Are_Interrupts_Enabled return Boolean is

      use type Interfaces.Unsigned_8;

      --  Status register

      SREG : Interfaces.Unsigned_8;
      for SREG'Address use 16#5f#;
      pragma Volatile (SREG);

      --  Interrupt Enable bit

      Interrupt_Flag : constant := 16#80#;

   begin
      return (SREG and Interrupt_Flag) = Interrupt_Flag;
   end Are_Interrupts_Enabled;

   ----------------
   -- Get_HWTime --
   ----------------

   function Get_HWTime return HWTime is
   begin
      pragma Assert (Initialized);

      if Are_Interrupts_Enabled then
         declare
            Now : HWTime;
         begin
            Disable_Interrupts;
            Now := Timer.Now;
            Enable_Interrupts;

            return Now;
         end;
      else
         return Timer.Now;
      end if;
   end Get_HWTime;

   -------------------
   -- Get_HiResTime --
   -------------------

   function Get_HiResTime return HiResTime renames M2.HAL.Timer.Get_Counter;

   --------------------
   -- Initialization --
   --------------------

   procedure Initialization (OS_Tick_Handler : OS_Tick_Handler_Ac) is
   begin
      pragma Assert (not Initialized);
      pragma Assert (To_Integer (Data_End'Address) /= 0);

      HAL.Timer.Initialize (OS_Tick_Handler);

      Initialized := True;
   end Initialization;

end M2.HAL;
