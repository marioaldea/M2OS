----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2021
--                    Universidad de Cantabria, SPAIN
--
--  Author: Daniel Fernandez Castillo (dfc915@alumnos.unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--  Example based on nxt_color_detect.ino from EVShield library.
--  https://github.com/mindsensors/EVShield
----------------------------------------------------------------------------
----------------------------------------------------------------------------
--  setup for this example:
--  attach NXT Color Sensor to Port BAS1
----------------------------------------------------------------------------

with Interfaces.C;

with M2.Direct_IO;
with AdaX_Dispatching_Stack_Sharing;
with Arduino.EVShield;
with Interfaces.C.Extensions;
with SHDefines_h; use SHDefines_h;
with Arduino.EVShield.NXTColor;
with stdint_h;

procedure evShield_nxt_color_detect is
   package DIO renames M2.Direct_IO;

   use type Arduino.Time_MS, Interfaces.C.Extensions.bool;
   -- Program variables
   value : stdint_h.uint8_t;
   aux : Interfaces.C.Extensions.bool;

   package Color_sensor is new Arduino.EVShield.NXTColor.Color (Arduino.EVShield.SH_BAS1);

begin

   DIO.Put_Line ("--------------------------------------");
   DIO.Put_Line ("Starting NXT Color Sensor Color Detect Test program");
   DIO.Put_Line ("--------------------------------------");

   aux := Color_sensor.Set_Color(Color_sensor.SH_Type_COLORFULL);

   DIO.Put_Line ("setup done");
   DIO.Put_Line ("hold a colored object in front of the color sensor");

   loop
      -- read the value from color sensor
      value := Color_sensor.Read_Color;

      -- print the value on serial terminal
      DIO.Put ("color sensor light value: "); DIO.Put_Line (value'Img);

      -- wait for one second
      Arduino.C_Delay (1000);

   end loop;
end evShield_nxt_color_detect;
