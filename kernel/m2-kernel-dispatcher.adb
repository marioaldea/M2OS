----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2024
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
with M2.Kernel.Ready_Queue;
with M2.HAL;
with M2.Kernel.TCBs;
with M2.Kernel.CPU_Time;
with M2.Debug;

with Ada.Unchecked_Conversion;
with System.Storage_Elements;

package body M2.Kernel.Dispatcher is
   package RQ renames Ready_Queue;
   package SSE renames System.Storage_Elements;
   use type TCBs.TCB_Ac;

   ------------------
   -- Show_Running --
   ------------------

   procedure Show_Running;

   procedure Show_Running is
   begin
      if RQ.Running_Ac /= TCBs.Null_TCB_Ac then
         pragma Debug (M2.Debug.Put ("RT"));
         TCBs.Show (RQ.Running_Ac.all);
      else
         pragma Debug (M2.Debug.Put ("RT(null)"));
      end if;
   end Show_Running;

   -------------
   -- Show_CS --
   -------------

   procedure Show_CS (Heir_Ac : TCBs.TCB_Ac);

   procedure Show_CS (Heir_Ac : TCBs.TCB_Ac) is
   begin
      pragma Debug (M2.Debug.Put ("CS->", Integer (Heir_Ac.Id)));
   end Show_CS;

   --------------
   -- Dispatch --
   --------------

   procedure Dispatch is
      Head_Ac : TCBs.TCB_Ac;
      Idle_Time : Boolean := False;
      use type HAL.Stack_Size, System.Address, HAL.Stack_Address;

   begin
      pragma Debug (M2.Debug.Put ("|Disp"));
      pragma Debug (Show_Running);
      pragma Debug (RQ.Show (M2.Trace));
      pragma Assert (RQ.Running_Ac.Stack_Base >= HAL.Current_Stack_Top);
      pragma Assert (HAL.Get_Current_Stack_Margin_In_Bytes > 50);
      pragma Assert (HAL.Get_Current_Stack_Size_In_Bytes <
                       HAL.Get_Max_Stack_Usage_In_Bytes);

      --  Account running thread CPU time

      if M2.Account_CPU_Time'First then
         CPU_Time.Update_CPU_Time_Of_Running_Thread;
      end if;

      --  Wait until there is some active task.

      loop
         Head_Ac := RQ.Head_Of_Ready_Queue;

         if Head_Ac /= TCBs.Null_TCB_Ac then
            Head_Ac.Stack_Base := RQ.Running_Ac.Stack_Base;
            RQ.Running_Ac := Head_Ac;
            exit;
         elsif M2.Account_CPU_Time'First then
            --  No ready threads: dispatching time must be accounted as "idle"

            Idle_Time := True;
         end if;
      end loop;

      --  Account idle CPU time

      if M2.Account_CPU_Time'First and then Idle_Time then
         CPU_Time.Update_Idle_CPU_Time;
      end if;

      --  Perform context switch to the Heir task

      pragma Debug (Show_CS (RQ.Running_Ac));
      HAL.Context_Switch (New_Addr  => RQ.Running_Ac.Ret_Addr,
                          New_Stack => RQ.Running_Ac.Stack_Base);
      pragma Assert (Standard.False); -- Never reached
   end Dispatch;

   ------------------------------------
   -- Dispatch_After_Yield_To_Higher --
   ------------------------------------

   procedure Dispatch_After_Yield_To_Higher is
      Head_Ac : TCBs.TCB_Ac;
      function To_Code_Address is
        new Ada.Unchecked_Conversion (SSE.Integer_Address, HAL.Code_Address);
      function To_Address is
        new Ada.Unchecked_Conversion (HAL.Stack_Address, System.Address);
      function To_Integer is
        new Ada.Unchecked_Conversion (HAL.Stack_Address, SSE.Integer_Address);
      function To_Stack_Address is
        new Ada.Unchecked_Conversion (System.Address, HAL.Stack_Address);
      use type System.Address, HAL.Stack_Address, HAL.Stack_Size;
   begin
      pragma Debug (M2.Debug.Put ("|Yld"));
      pragma Debug (Show_Running);
      pragma Debug (RQ.Show (M2.Trace));
      pragma Assert (RQ.Running_Ac.Stack_Base >= HAL.Current_Stack_Top);
      pragma Assert (HAL.Get_Current_Stack_Margin_In_Bytes > 50);
      pragma Assert (HAL.Get_Current_Stack_Size_In_Bytes <
                       HAL.Get_Max_Stack_Usage_In_Bytes);

      --  Yield to a thread with higher priority.

      Head_Ac := RQ.Head_Of_Ready_Queue;
      pragma Assert (RQ.Running_Ac /= null);
      pragma Assert (Head_Ac /= null);

      if Head_Ac /= RQ.Running_Ac then
         --  There is a more prioritary task to preempt the running task

         --  Account running thread CPU time
         if M2.Account_CPU_Time'First then
            CPU_Time.Update_CPU_Time_Of_Running_Thread;
         end if;

         --  Store task body and stack base and set address of next activation
         RQ.Running_Ac.Aux_Args := To_Address (RQ.Running_Ac.Stack_Base);
#if M2_TARGET = "stm32f4" or M2_TARGET = "epiphany" or M2_TARGET = "rp2040" or M2_TARGET = "microbit" then
         RQ.Running_Ac.Ret_Addr :=
           To_Code_Address (SSE.To_Integer (Ret_Label'Address) or 1);
         --  Set least significative bit to 1 to avoid change from thumb to ARM
#elsif M2_TARGET = "arduino_uno" or M2_TARGET = "avr_iot" then
         RQ.Running_Ac.Ret_Addr :=
           To_Code_Address (SSE.To_Integer (Ret_Label'Address));
#elsif  M2_TARGET = "riscv" then
         pragma Compile_Time_Warning (True, "TODO");
#else
         pragma Compile_Time_Error (True, "Unexpected architecture");
#end if;
         HAL.Push_Function_Status;

         --  Change ready thread
         Head_Ac.Stack_Base := HAL.Current_Stack_Top;
         RQ.Running_Ac := Head_Ac;

         pragma Debug (Show_CS (RQ.Running_Ac));
         HAL.Context_Switch (New_Addr  => RQ.Running_Ac.Ret_Addr,
                             New_Stack => RQ.Running_Ac.Stack_Base);
         pragma Assert (Standard.False); -- Never reached
#if M2_TARGET = "stm32f4" or M2_TARGET = "epiphany" or M2_TARGET = "rp2040" or M2_TARGET = "microbit" then
         -- To avoid reordering of <<Ret_Label>> when using optimization
         if RQ.Running_Ac = null then
            return;
         end if;
#end if;

         <<Ret_Label>> --  Return of preempted task

         --  restore task body and stack base for the preempted task
         HAL.Pop_Function_Status;
         RQ.Running_Ac.Ret_Addr := RQ.Running_Ac.Body_Ac;
         RQ.Running_Ac.Stack_Base := To_Stack_Address (RQ.Running_Ac.Aux_Args);
         pragma Assert (RQ.Running_Ac.Stack_Base >= HAL.Current_Stack_Top);
      end if;
   end Dispatch_After_Yield_To_Higher;

end M2.Kernel.Dispatcher;
