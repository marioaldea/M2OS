----------------------------------------------------------------------------
--                                  M2OS
--
--                           Copyright (C) 2019
--                    Universidad de Cantabria, SPAIN
--
--  Author: Mario Aldea Rivas (aldeam@unican.es)
--
--  This file is part of M2OS.
--
--  M2OS is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  M2OS is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with M2OS.  If not, see <https://www.gnu.org/licenses/>.
----------------------------------------------------------------------------
--
--  Binding of Arduino standard library Wire.h
--  #include <Wire.h> must appear in lib_core_generator.ino
--  See https://www.arduino.cc/en/reference/wire for functions
--  documentation.
--
with System;
with Interfaces.C;

package Arduino.Wire is

   type Dev_Address is new Interfaces.Unsigned_8 range 1 .. 127;
   for Dev_Address'Size use Interfaces.Unsigned_8'Size;

   type Transmission_Result is (OK, Data_Too_Long, NACK_Address,
                                NACK_Data, Other_Error);
   for Transmission_Result'Size use Interfaces.Unsigned_8'Size;

   Two_Wire_Size_In_Bytes : constant := 12;

   Sizeof_Two_Wire : Integer with
     Import, Convention => C,
     External_Name => "sizeof_TwoWire";
   --  Defined in 'arch/arduino_uno/drivers/libcore/sizes_arduino_types.cpp'
   --  Take the value of -1 if the library is not included.

   type Two_Wire_Filling is
     array(1 .. Two_Wire_Size_In_Bytes) of Interfaces.Unsigned_8;

   type Two_Wire is limited record
      Filling: Two_Wire_Filling;
   end record;
   pragma Import (CPP, Two_Wire);
   for Two_Wire'Size use Two_Wire_Size_In_Bytes * 8;

   function New_Two_Wire return Two_Wire;
   pragma CPP_Constructor (New_Two_Wire, "_ZN7TwoWireC1Ev");

   procedure Begin_Two_Wire (This : access Two_Wire);
   pragma Import (CPP, Begin_Two_Wire, "_ZN7TwoWire5beginEv");

   procedure Begin_Transmission (This : access Two_Wire;
                                 Address : Dev_Address);
   pragma Import (CPP, Begin_Transmission, "_ZN7TwoWire17beginTransmissionEh");

   function End_Transmission (This : access Two_Wire)
                              return Transmission_Result;
   pragma Import (CPP, End_Transmission, "_ZN7TwoWire15endTransmissionEv");

   function Request_From
     (This : access Two_Wire;
      Address : Dev_Address;
      Quantity : Interfaces.Unsigned_8)
      return Interfaces.C.Size_T;
   pragma Import (CPP, Request_From, "_ZN7TwoWire11requestFromEhh");

   function Request_From_With_Stop
     (This : access Two_Wire;
      Address : Dev_Address;
      Quantity : Interfaces.Unsigned_8;
      Stop : Arduino.Boolean8)
      return Interfaces.C.Size_T;
   pragma Import (CPP, Request_From_With_Stop, "_ZN7TwoWire11requestFromEhhh");

   function write (This : access Two_Wire;
                   Byte : Interfaces.Unsigned_8)
                   return Interfaces.C.Size_T;
   pragma Import (CPP, Write, "_ZN7TwoWire5writeEh");

   function Write_Data
     (This : access Two_Wire;
      Data : System.Address; --  access to an array of bytes
      Length : Interfaces.C.Size_T) --  length of the array of bytes
      return Interfaces.C.Size_T;
   pragma Import (CPP, Write_Data, "_ZN7TwoWire5writeEPKhj");

   function Available (This : access Two_Wire)
                       return Interfaces.C.Int;
   pragma Import (CPP, Available, "_ZN7TwoWire9availableEv");

   function Read (This : access Two_Wire)
                  return Interfaces.Integer_16;
   pragma Import (CPP, Read, "_ZN7TwoWire4readEv");

   Wire : aliased Two_Wire;
   pragma Import (C, Wire, "Wire");

end Arduino.Wire;
