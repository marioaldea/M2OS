
generic
   Pin : Arduino.Digital_Pin;
package Arduino.DHT.DHT11_Sensor is

   procedure Begin_DHT
     with Inline;

   function Read (TH : access Measurement) return Integer
     with Inline;

end Arduino.DHT.DHT11_Sensor;
