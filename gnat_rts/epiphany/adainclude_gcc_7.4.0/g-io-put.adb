--  MaRTE version of Put (C : Character)

separate (GNAT.IO)
procedure Put (C : Character) is
   procedure Putchar (C : Character);
   pragma Import (C, Putchar, "m2__direct_io__putchar");
begin
   Putchar (C);
end Put;
